<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Личный кабинет абитуриента на сайте</title>

    <!--Bootstrap -->
    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
        crossorigin="anonymous">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" type="text/css">

    <link href="css/uploadfile.css" rel="stylesheet">
    <link href="css/bootstrap-datepicker.css" rel="stylesheet">
    <link rel="stylesheet" href="css/multi-step-form.css" type="text/css">

    <!-- KLADR -->
    <link href="kladr/css/jquery.kladr.min.css" rel="stylesheet">
    <!-- <link href="kladr/css/one_string.css" rel="stylesheet"> -->
    <link href="kladr/css/form.css" rel="stylesheet">

    <link href="css/style.css" rel="stylesheet">


</head>

<body>

    <div id="wrapper">

        <div class="container body-content">
 
            <form class="form-registration" id="formreg" method=post action="actionreg.php">
                <div class="msf-formreg">
                    <div class="row">
                        <div class="col-md-12">
                            <h3>Регистрация</h3>
                        </div>
                        <div class="col-md-12">

                            <div class="form-group">
                                <label>Email</label> 
                                <input id="email" name="email" type="text" class="form-control" placeholder="Email" data-bind="value: Email" data-val="true" data-val-required="email is required" data-msg="Введите Email" required>
                            </div>
                            <div class="form-group inddocuments-step-1">
                                <label>Удостоверение личности</label> 
                            </div>
                            <div class="form-group series series-step-1">
                                <input type="text" class="form-control" id="PassportSeries" name="passportseries" placeholder="Серия" data-msg="Введите серию" required>
                            </div>
                            <div class="form-group number">
                                <input type="text" class="form-control" id="PassportNumber" name="passportnumber" placeholder="Номер" data-msg="Введите номер" required>
                            </div> 
                            <div class="form-group lastname_ch">
                                <label>Фамилия</label> <br>
                                <input id="lastname" name="lastname" type="text" class="form-control" placeholder="Фамилия" data-val="true" data-val-required="lastname is required" onkeyup="rusinput('ru', this );"  data-msg="Введите фамилию" required>  
                                <input type="checkbox" class="form-check-input" id="lastname_ch" value="Y" name="lastname_empty"><small>отсутствует</small> 
                            </div> 
                            <div class="form-group firstname_ch">
                                <label>Имя</label> <br>
                                <input id="firstname" name="firstname" type="text" class="form-control" placeholder="Имя" data-val="true" data-val-required="firstname is required" onkeyup="rusinput('ru', this );" data-msg="Введите имя" required>  
                                <input type="checkbox" class="form-check-input" id="firstname_ch"  value="Y" name="firstname_empty"><small>отсутствует</small>  
                            </div>
                            <div class="form-group patronymic_ch">
                                <label>Отчество</label> <br>
                                <input id="patronymic" name="patronymic" type="text" class="form-control" placeholder="Отчество" data-val="true" data-val-required="patronymic is required" onkeyup="rusinput('ru', this );" data-msg="Введите отчество" required>  
                                <input type="checkbox" class="form-check-input" id="patronymic_ch" value="Y"  name="patronymic_empty"><small>отсутствует</small> 
                            </div>
                            <div class="form-group form-date">
                                <label>Дата рождения</label>
                                <input id="birthdate" name="birthdate" type="text" class="form-control" placeholder="Дата рождения" data-val="true" data-msg="Введите дату рождения" required>
                                <input type="hidden" class="hidden-date" value="">
                            </div>
                            <div class="form-group">
                                <input type="submit" data-type="submit" id="registrationbtn" class="btn btn-primary" value="зарегистрироваться">
                            </div>

                        </div>
                    </div>
                </div> 
            </form>

            <div class="continue-block">
                <p>для продолжения ...</p>
            </div>

            

        </div>
    </div>


    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script type="text/javascript" src="js/jquery.min.js"></script>

    <!-- KLADR -->
    <script src="kladr/js/jquery.kladr.min.js" type="text/javascript"></script>
    <script src="kladr/js/form.js" type="text/javascript"></script>
    <!-- <script src="kladr/js/one_string.js" type="text/javascript"></script> -->

    <script type="text/javascript" src="js/knockout-min.js"></script>

    <script type="text/javascript" src="js/bootstrap.min.js"></script>

    <!-- multi-step-form validate -->
    <script type="text/javascript"  src="js/jquery.validate.js"></script>   
    <script type="text/javascript"  src="js/multi-step-form.js"></script>   

    <!-- upload file -->
    <script src="js/jquery.uploadfile.min.js"></script>
    <script src="js/bootstrap-datepicker.min.js"></script>
    <script src="js/bootstrap-datepicker.ru.min.js" charset="UTF-8"></script>

    <!-- fontawesome -->
    <script defer src="js/font-awesome.js"></script>

    <script src="js/jquery.maskedinput.min.js"></script>
    <script type="text/javascript"  src="js/script.js"></script>

</body>

</html>