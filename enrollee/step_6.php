<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Заявления");?>
<?global $USER;?>
<?
session_start();
$ip = $_SERVER['REMOTE_ADDR'];
//отработать-показать условия и текст для внешнего диапазона
if (($ip < '10.0.0.1') || ($ip > '10.255.255.254')){
    $localFl = false;
//ЛВС
}else{
    $localFl = true;
}

if($ip=='91.229.128.218'){
    $localFl = true;
}
?>
<?
$date = '2018-03-28';
$arErrors = array();
$arResult = array();
$arRslt = array();
$arResultAb = array();
$arResultApp = array();
$arCompGr = array();
$arCompGrGUID = array();
$arTrForm = array();
$arSpecialty = array();
$arSpecialtyGUID = array();
$arProfGUIDSpecGUID = array();
$arRezCompGr = array();
if(!$USER->IsAuthorized()){
    $arErrors[] = "Вы не авторизованы";
    //ShowError("Вы не авторизованы");
    //переадресация на авторизацию
    header('Location: /enrollee/step_0.php', true, 301);
    //die();
}else{
    $rsUser = CUser::GetByLogin($USER->GetLogin());
    $arUser = $rsUser->Fetch();
    //echo "<pre>"; print_r($arUser); echo "</pre>";
    if(!$arUser["XML_ID"]){
        $arErrors[] = "У Пользователя не заполнено поле XML_ID";
        //ShowError("У Пользователя не заполнено поле XML_ID");
        $USER->Logout();
        $err = urlencode("Ошибка получения данных (XML_ID) для ".$arUser["LOGIN"].", обратитесь к администратору");
        header('Location: /enrollee/?err='.$err, true, 301);
        die();
    }else{
        $client = getSoapClient(WSABITURS_URL);
        if($client){
            $params = array("ID" => $arUser["XML_ID"]);
            try{
                $result = $client->ПолучитьСписокАбитуриентов($params);
                $arRslt = json_decode(json_encode($result->return->Состав), TRUE);
                $arResultAb = json_decode(json_encode($result->return->Состав->Абитуриент), TRUE);
                //проверка на пустой ответ из 1С
                isEmptyArr1C($arResultAb, 6, true);
                $arResultBufApp = json_decode(json_encode($result->return->Состав->Заявления->Заявление), TRUE);
                if(is_array($arResultBufApp[0])){
                    $arResultApp = $arResultBufApp;
                }elseif(is_array($arResultBufApp)){
                    $arResultApp[0] = $arResultBufApp;
                }else{
                    $arResultApp = array();
                }

                /*
                if($arUser["XML_ID"] == 1988){
                    echo "<pre>";
                    print_r($arResultApp);
                    echo "</pre>";
                }
                */

            }catch (Exception $e){
                $arErrors[] = 'Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n";
                //ShowError('Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n");
            }
            $groups = getSoapClient(WSCOMPGROUPS_URL);
            if($groups){
                try{
                    //echo $date = date('Y-m-d',strtotime('28.03.2018'));
                    $rez = $groups->ПолучитьСписокКонкурсныхГрупп(array("ДатаНачала"=>$date));
                    $arCompGr = json_decode(json_encode($rez->return->КонкурснаяГруппа), TRUE);
                    foreach($arCompGr as $arItem){
                        $arCompGrGUID[$arItem["GUID"]] = $arItem;
                    }
                }catch (Exception $e){
                    $arErrors[] = 'Выброшено исключение. Метод "ПолучитьСписокКонкурсныхГрупп". '.$e->getMessage()."\n";
                    //ShowError('Выброшено исключение. Метод "ПолучитьСписокКонкурсныхГрупп". '.$e->getMessage()."\n");
                }
                try{
                    $rez = $groups->ПолучитьСписокТиповДокументов();
                    $arTypesDocs = json_decode(json_encode($rez->return->ТипДокумента), TRUE);
                }catch (Exception $e){
                    $arErrors[] = 'Выброшено исключение. Метод "ПолучитьСписокТиповДокументов". '.$e->getMessage()."\n";
                    //ShowError('Выброшено исключение. Метод "ПолучитьСписокТиповДокументов". '.$e->getMessage()."\n");
                }
                try{
                    $rez = $groups->ПолучитьСписокУровнейОбразования();
                    $arLvlvEdu = json_decode(json_encode($rez->return->УровеньОбразования), TRUE);
                }catch (Exception $e){
                    $arErrors[] = 'Выброшено исключение. Метод "ПолучитьСписокУровнейОбразования". '.$e->getMessage()."\n";
                    //ShowError('Выброшено исключение. Метод "ПолучитьСписокУровнейОбразования". '.$e->getMessage()."\n");
                }
                try{
                    $rez = $groups->ПолучитьСписокФормОбучения();
                    $arTrForm = json_decode(json_encode($rez->return->ФормаОбучения), TRUE);
                }catch (Exception $e){
                    $arErrors[] = 'Выброшено исключение. Метод "ПолучитьСписокФормОбучения". '.$e->getMessage()."\n";
                    //ShowError('Выброшено исключение. Метод "ПолучитьСписокФормОбучения". '.$e->getMessage()."\n");
                }
                try{
                    $rez = $groups->ПолучитьСписокСпециальностей(array("ДатаНачала"=>$date));
                    $arSpecialty = json_decode(json_encode($rez->return->Специальность), TRUE);
                    foreach($arSpecialty as $arItem){
                        $arSpecialtyGUID[$arItem["GUID"]] = $arItem;
                        if(is_array($arItem["Профили"][0])){
                            foreach($arItem["Профили"] as $arProfile){
                                $arProfGUIDSpecGUID[$arProfile["GUID"]] = $arItem["GUID"];
                            }
                        }elseif(is_array($arItem["Профили"])){
                            $arProfGUIDSpecGUID[$arItem["Профили"]["GUID"]] = $arItem["GUID"];
                        }
                        /*
                        foreach($arItem["Профили"] as $arProfile){
                            $arProfGUIDSpecGUID[$arProfile["GUID"]] = $arItem["GUID"];
                        }*/
                    }
                }catch (Exception $e){
                    $arErrors[] = 'Выброшено исключение. Метод "ПолучитьСписокСпециальностей". '.$e->getMessage()."\n";
                    //ShowError('Выброшено исключение. Метод "ПолучитьСписокСпециальностей". '.$e->getMessage()."\n");
                }
            }
        }
    }
}?>
<?if($arResultAb["СтатусЗаявки"] == "На доработке"){?>
    <form id="formstatus" method="post" action="">
        <input type="hidden" name="changeSt" value="Y">
    </form>
<?}?>
<div class="msf">
	<div class="msf-header">
		<div class="row msf-header__top">
			<div class="msf-header__column"><div class="msf-header__status">Статус: <span id="status"><?=$arResultAb["СтатусЗаявки"]?></span></div></div>
			<?if($arResultAb["СтатусЗаявки"]=="На доработке"){?>
				<div class="msf-header__column">
					<a href="#" id="changeStatusSend" class="btn btn-primary">Отправить данные на проверку</a>
				</div>
			<?}?>
			<div class="msf-header__column msf-header__column_logout"><a href="/enrollee/?logout=yes">Выход</a></div>
		</div>
		<div class="row text-left">
			<?if($arResultAb["СтатусЗаявки"]=="На доработке"){?>
				<div class="msf-step"><a href="step_1.php">Основные данные</a></div>
				<div class="msf-step"><a href="step_2.php">Семья</a></div>
				<div class="msf-step"><a href="step_3.php">Дополнительная информация</a></div>
				<div class="msf-step"><a href="step_4.php">Предоставленные документы</a></div>
				<div class="msf-step"><a href="step_5.php">Сведения о ЕГЭ</a></div>
				<div class="msf-step active msf-step-active"><a href="step_6.php">Заявления</a></div>
				<div class="msf-step"><a href="info.php">Инфопанель</a></div>
			<?}else{?>
				<div class="msf-step"><span>Основные данные</span></div>
				<div class="msf-step"><span>Семья</span></div>
				<div class="msf-step"><span>Дополнительная информация</span></div>
				<div class="msf-step"><span>Предоставленные документы</span></div>
				<div class="msf-step"><span>Сведения о ЕГЭ</span></div>
				<div class="msf-step active msf-step-active"><span>Заявления</span></div>
				<div class="msf-step"><a href="info.php">Инфопанель</a></div>
			<?}?>
		</div>
	</div>
	<div class="msf-content" id="top">
		<div class="msf-view" style="display: block">
			<div class="row">
				<div class="col-md-12">
					<?echo '<input id="status-inp" type="hidden" value="'.$arResultAb["СтатусЗаявки"].'">';?>
					<h3>Заявления</h3>
					<br />
					<p><a target="_blank" href="https://msal.ru/upload/struktura/cpk2018/pkdoc/%D0%98%D0%BD%D1%81%D1%82%D1%80%D1%83%D0%BA%D1%86%D0%B8%D0%B8%20%D0%BF%D0%BE%20%D0%BF%D0%BE%D0%B4%D0%B3%D0%BE%D1%82%D0%BE%D0%B2%D0%BA%D0%B5%20%D0%B8%20%D0%BE%D1%82%D0%BF%D1%80%D0%B0%D0%B2%D0%BA%D0%B5%20%D0%B4%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%BE%D0%B2%20%D0%B4%D0%BB%D1%8F%20%D0%BF%D0%BE%D0%B4%D0%B0%D1%87%D0%B8%20%D0%B7%D0%B0%D1%8F%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D1%8F%20%D1%87%D0%B5%D1%80%D0%B5%D0%B7%20%D0%BB%D0%B8%D1%87%D0%BD%D1%8B%D0%B9%20%D0%BA%D0%B0%D0%B1%D0%B8%D0%BD%D0%B5%D1%82%20%D0%B0%D0%B1%D0%B8%D1%82%D1%83%D1%80%D0%B8%D0%B5%D0%BD%D1%82%D0%B0.pdf">Инструкции по подготовке и отправке документов для подачи заявления через личный кабинет абитуриента</a> (открыть в новой вкладке)</p>
					<?foreach($arErrors as $error){
						ShowError($error);
					}?>
					<?if($_GET["msg"]=="wrngs"){
						ShowError("Данные не сохранены. Большая нагрузка на сервис. Введите данные еще раз.");
					}?>
					<?if($_GET["msg"]=="ok"){
						echo "<p class='ok'>Спасибо! Заявления отправлены на проверку модератору.<br>Вы можете перейти к просмотру страницы <a href='/enrollee/info.php'>Информационная панель абитуриента</a>.</p>";
						echo "<p class='ok'><a href='/enrollee/info.php'><b>Далее</b></a></p><br><br>";
					}?>
					<?if($_POST["changeSt"]=="Y"){?>
						<?require($_SERVER["DOCUMENT_ROOT"]."/enrollee/action/action_chng_st.php");?>
					<?}?>
					<?if($_POST["submit"]=="Y"){
						/*
						//отправляем в 1С отобранные заявления
						if($_POST["action"] == "send"){
							require($_SERVER["DOCUMENT_ROOT"]."/enrollee/action/action_step_6.php");
						}
						*/
						//сохраняем в сессию отобранные заявления
						if($_POST["action"] == "save"){
							$_SESSION['enrollee']['appGET'] = array();
							foreach($_POST["app"] as $arItem){
								if($arItem["take"] && !array_key_exists($arItem["GUID"], $_SESSION['enrollee']['appGET'])){
									$_SESSION['enrollee']['appGET'][$arItem["guid"]] = $arItem;
								}
							}
							if($_POST["app_main"]){
								$_SESSION['enrollee']["app_main"] = $_POST["app_main"];
							}
						}?>
					<?}?>
					<div class="document-examples">Все образцы документов (заявлений) можно скачать по ссылке: <a href="https://msal.ru/content/abiturientam/priemnaya-kampaniya/?hash=tab2622" target="_blank">Образцы документов</a></div>
				</div>

				<form method="get" action="<?php echo $_SERVER['SCRIPT_NAME'] ?>#formmsf6">
					<div class="clearfix">
						<p class="col-md-12"><b>Пожалуйста выберите значения в отборе для быстрого поиска заявления. При необходимости нажмите кнопку «сбросить значения отбора» и укажите новые параметры отбора. Внимание нужно в отборе указать все поля!</b></p><br>
						<div class="form-group">
							<?$arFin = array(
								"0" => array("КодСправочника"=>"Бюджетное", "Наименование"=>"Бюджетное"),
								"1" => array("КодСправочника"=>"С оплатой обучения", "Наименование"=>"С оплатой обучения"),
							);?>
							<label>Вид финансирования</label>
							<select name="financing" class="form-control">
								<?foreach($arFin as $arItem){?>
									<option value="<?=$arItem["КодСправочника"]?>" <?=($_GET["financing"] == $arItem["КодСправочника"]) ? 'selected' : ''?>><?=$arItem["Наименование"]?></option>
								<?}?>
							</select>
						</div>
						<div class="form-group">
							<label>Форма обучения</label>
							<select name="tr_form" class="form-control">
								<?foreach($arTrForm as $arItem){?>
									<option attr-code="<?=$arItem["GUID"]//$arItem["КодСправочника"]?>" value="<?=$arItem["GUID"]//$arItem["КодСправочника"]?>" <?=($_GET["tr_form"] == $arItem["GUID"]/*$arItem["КодСправочника"]*/) ? 'selected' : ''?>><?=$arItem["Наименование"]?></option>
								<?}?>
							</select>
						</div>
						<div class="form-group">
							<label>Специальность\направление (выбирайте только интересующие Вас направления)</label>
							<?
							#if($_GET[aa])
							#3{
							#	#echo "<pre>".print_r($arSpecialty,1)."</pre>";
							#}
							?>
							<select name="speciality" class="form-control">
								<?foreach($arSpecialty as $arItem){?>
									<option attr-code="<?=$arItem["GUID"]?>" value="<?=$arItem["GUID"]?>" <?=(($_GET["speciality"] == $arItem["GUID"]) && isset($_GET["speciality"]) && !empty($_GET["speciality"])) ? 'selected' : ''?>><?=$arItem["КодСпециальности"]?> <?=$arItem["Наименование"]?></option>
									<?/*
									<optgroup label="<?=$arItem["Наименование"]?>">
										<?if(is_array($arItem["Профили"][0])){
											foreach($arItem["Профили"] as $arProfile){?>
												<option attr-code="<?=$arItem["GUID"]//$arProfile["КодСправочника"]?>" value="<?=$arItem["GUID"]//$arProfile["КодСправочника"]?>" <?=(($_GET["speciality"] == $arItem["GUID"]/*$arItem["КодСправочника"]*//*)  && isset($_GET["speciality"]) && !empty($_GET["speciality"])) ? 'selected' : ''?>><?=$arProfile["Наименование"]?></option>
											<?}?>
										<?}else{?>
											<option attr-code="<?=$arItem["GUID"]//$arItem["Профили"]["КодСправочника"]?>" value="<?=$arItem["GUID"]//$arItem["Профили"]["КодСправочника"]?>" <?=(($_GET["speciality"] == $arItem["GUID"]/*$arItem["Профили"]["КодСправочника"]*//*) && isset($_GET["speciality"]) && !empty($_GET["speciality"])) ? 'selected' : ''?>><?=$arItem["Профили"]["Наименование"]?></option>
										<?}?>
									</optgroup>
									*/?>
								<?}?>
							</select>
						</div>
						<div class="form-group">
							<?$arTargetDirect = array(
								"0" => array("КодСправочника"=>"0", "Наименование"=>"Без признака"),
								"1" => array("КодСправочника"=>"1", "Наименование"=>"Целевое"),
								"2" => array("КодСправочника"=>"2", "Наименование"=>"Квота"),
							);?>
							<label>Отображать заявления по «целевому приему» (в случае, если у Вас есть целевое направление)</label>
							<select name="target_direction" class="form-control">
								<?foreach($arTargetDirect as $arItem){?>
									<option value="<?=$arItem["КодСправочника"]?>" <?=($_GET["target_direction"] == $arItem["КодСправочника"]) ? 'selected' : ''?>><?=$arItem["Наименование"]?></option>
								<?}?>
							</select>
						</div>
						<div class="form-group gender">
							<label>Отображать заявления по «квоте» (в случае, если у Вас есть документы об наличии инвалидности или признаны сиротой)</label>
							<div class="custom-control">
								<input type="radio" name="quota" value="Y" class="custom-control-input" <?=($_GET["quota"] == "Y") ? 'checked' : ''?>><span>Да</span>
								<input type="radio" name="quota" value="N" class="custom-control-input" <?=($_GET["quota"] != "Y") ? 'checked' : ''?>><span>Нет</span>
							</div>
						</div>
					</div>

					<input type="hidden" name="submit" value="Y">

					<div class="msf-navigation">
						<div class="text-left">
							<div class="nav-btn">
								<button type="reset" data-type="submit" class="btn btn-default msf-nav-button js-form-reset">сбросить значения отбора</button>
							</div>
							<div class="nav-btn">
								<button type="submit" data-type="submit" class="btn btn-primary msf-nav-button">показать</button>
							</div>
						</div>
					</div>
				</form>
				<br><br>
				<?//фильтр по группам - перебиравем все группы, если группа подходит по параметрам добавляем ее в новый массив
				foreach($arCompGr as $arGroup){
					$flag = true;
					//перебираем отправленные заявление в 1С, если такая группа попала в фильтр то игнорируем ее, потому что она уже есть
					foreach($arResultApp as $arApp){
						if($arGroup["GUID"] == $arApp["GUIDКГ"]){
							$flag = false;
							break;
						}
					}
					if($flag){
						//если совпали условия "Вид финансирования" и "Форма обучения" и "Специальность"
						if(($_GET["financing"] == $arGroup["ВидФинансирования"])
							&& ($_GET["tr_form"] == $arGroup["GUIDФормыОбучения"])
							&& ($_GET["speciality"] == $arGroup["GUIDНаправления"])
							//&& (($_GET["target_direction"] == 1) && $arGroup["ЦелевойПрием"])
						){
							//Без признака
							if($_GET["target_direction"] == 0){
								$arRezCompGr[$arGroup["GUID"]] = $arGroup;
								//Целевой прием
							}elseif($_GET["target_direction"] == 1){
								if($arGroup["ЦелевойПрием"]){
									$arRezCompGr[$arGroup["GUID"]] = $arGroup;
								}
								//Особая квота
							}elseif($_GET["target_direction"] == 2){
								//Отображать заявления по «квоте»
								if($_GET["quota"] == "Y"){
									if($arGroup["ОсобаяКвота"]){
										$arRezCompGr[$arGroup["GUID"]] = $arGroup;
									}
								}else{
									$arRezCompGr[$arGroup["GUID"]] = $arGroup;
								}
							}
							/*
						elseif(!$arGroup["ЦелевойПрием"]){
							$arRezCompGr[] = $arGroup;
						}
							*/
						}
					}
				}?>
				<?
				//перебиравем все группы сохраненные в сессии
				foreach($_SESSION['enrollee']['appGET'] as $arGroup){
                    $flag = true;
                    //перебираем отправленные заявление в 1С, если такая группа попала в фильтр то игнорируем ее, потому что она уже есть
                    foreach($arResultApp as $arApp){
                        if($arGroup["guid"] == $arApp["GUIDКГ"]){
                            $flag = false;
                            break;
                        }
                    }
                    if($flag){
                        if(!in_array($arGroup["guid"], $arRezCompGr)){
                            $arRezCompGr[$arGroup["guid"]] = $arCompGrGUID[$arGroup["guid"]];
                        }
                    }
				}?>
				<?
				$arSamples = array();
				$arRezCompGrGUID = array();
				//выбираем GUID для выборки из ИБ шаблонов заявлений
				foreach($arRezCompGr as $key => $arGroup){
					if(!in_array($arGroup["GUID"], $arRezCompGrGUID)){
						$arRezCompGrGUID[] = $arGroup["GUID"];
					}
				}
				if(!empty($arRezCompGrGUID)){
					//print_r($arRezCompGrGUID);
					$arSelect = Array("ID", "NAME", "IBLOCK_ID", "PROPERTY_*");
					$arFilter = Array("IBLOCK_ID" => IntVal(SAMPLES_CG_IB_ID), "PROPERTY_GUID_KG" => $arRezCompGrGUID, "ACTIVE_DATE" => "Y", "ACTIVE" => "Y");
					$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
					while($ob = $res->GetNextElement()){
						$arFields = $ob->GetFields();
						$arFields["PROPERTIES"] = $ob->GetProperties();
						$arSamples[trim($arFields["PROPERTIES"]["GUID_KG"]["VALUE"])] = $arFields;
					}
				}
				?>

                <?/*if($_GET['a']=='test'){?>
                    <pre>
                        <?//=count($arCompGrGUID)?>
                        <?//print_r($arCompGrGUID)?>
                        <?//print_r($arSpecialtyGUID)?>

                        <?//print_r($arSpecialty)?>
                        <?//print_r($arSpecialtyGUID)?>
                        <?//print_r($arProfGUIDSpecGUID)?>
                        <?print_r($arCompGr)?>
                        <?print_r($arResultApp)?>
                        <?print_r($arRezCompGr)?>
                    </pre>
                <?}*/?>

				<div class="col-md-12">
					<form class="form-horizontal msf js-form-address" id="formmsf6" method="post" action="" enctype="multipart/form-data">
						<p><b>Внимание!!!<br>
						Согласно правилам приемной комиссии «согласие о зачислении» можно поставить напротив одного заявления! Все выбранные заявления будут проверены модератором на основании прикрепленных Вами документов. Статусы, личные данные, результаты экзаменов и другие дополнительные сведения отслеживайте на вкладке «информационная панель абитуриента».</b><br>
						<br>Вы можете подать заявления на не более чем <b>три направления</b> подготовки</p>
						<br>
						<?if($_POST["submit"]=="Y"){
							//отправляем в 1С отобранные заявления
							if($_POST["action"] == "send"){
								require($_SERVER["DOCUMENT_ROOT"]."/enrollee/action/action_step_6.php");
							}
						}?>
						<br>
						<p><b>Таблица выбранных заявлений</b></p>
						<table class="table-apps">
							<thead>
								<tr>
									<th>Наименования конкурсных групп согласно указанных критериях выше</th>
									<th>Установить флаг &laquo;V&raquo;, если желаете участвовать в&nbsp;данной конкурсной группе</th>
									<th>Укажите флаг &laquo;V&raquo; для фиксации &laquo;согласия о&nbsp;зачислении&raquo; (приоритетное заявление)</th>
									<th>Выберите наименование профиля\программы напротив приоритетного заявления</th>
									<?/*<th>Выберите наименование института напротив приоритетного заявления</th>*/?>
									<th>Файл заявления</th>
								</tr>
							</thead>
							<tbody>
								<?$key=0;
								//приоритетное заявление, если выбрано, то все остальные заявления radio блокируются
								$radioFl = false;
								foreach($arResultApp as $k => $arGroup){
									if($arGroup["СогласиеНаЗачисление"]){
										$radioFl = true;
										break;
									}
								}

								/*
								echo "<pre>";
                                print_r($arResultApp);
                                echo "</pre>";*/


								foreach($arResultApp as $k => $arGroup){
									if($arGroup["GUIDКГ"]){
										$key++;?>
										<tr>
											<td>
												<?=$arCompGrGUID[$arGroup["GUIDКГ"]]["Наименование"]?>
												<input type="hidden" name="app[<?=$key?>][name]" value="<?=$arCompGrGUID[$arGroup["GUIDКГ"]]["Наименование"]?>">
												<input type="hidden" name="app[<?=$key?>][guid]" value="<?=$arGroup["GUIDКГ"]?>">
												<?if($arGroup["RequestStatus"] == "На доработке" || ($arGroup["RequestStatus"] != "Принята" && $arGroup["ПутьКФайлу"] == '')){?>
													<input type="hidden" name="app[<?=$key?>][status]" value="Y">
													<input type="hidden" name="app[<?=$key?>][take]" value="addcheck<?=$key?>">
												<?}else{?>
													<input type="hidden" name="app[<?=$key?>][status]" value="">
												<?}?>

											</td>
											<td><input type="checkbox" name="app[<?=$key?>][take]" value="addcheck<?=$key?>" <?if(!$localFl && ($arGroup["RequestStatus"] == "На доработке" || ($arGroup["RequestStatus"] != "Принята" && $arGroup["ПутьКФайлу"] == ''))){?>data-file="1"<?}?>  data-id="<?=$key?>" checked disabled></td>
											<td><input type="radio" name="app_main" value="<?=$arGroup["GUIDКГ"]//$k//?>" data-id="<?=$key?>" <?=($arGroup["СогласиеНаЗачисление"]=="Y") ? 'checked' : ''?> <?=($radioFl || $arGroup["RequestStatus"] == "Принята") ? 'class="is-disabled" onclick="return false"' : ''?>></td>
											<td>
												<?$arItem = $arSpecialtyGUID[$arProfGUIDSpecGUID[$arGroup["GUIDПрофиль"]]]; //GUIDНП?>
                                                <input type="hidden" name="app[<?=$key?>][guid_spec]" value="<?=$arItem["GUIDНП"]/*$arItem["GUID"]*/?>">
												<?#todo если нельзя менять, обязательно на эти селекты класс is-disabled ?>
												<select name="app[<?=$key?>][profile]" style="max-width: 250px" <?/**/?>class="is-disabled" onclick="return false"<?/**/?>>
												<?if(!empty($arItem)){?>
													<?if(is_array($arItem["Профили"][0])){
														foreach($arItem["Профили"] as $arProfile){?>
															<option attr-code="<?=$arProfile["GUID"]?>" value="<?=$arProfile["GUID"]?>" <?/*GUIDНП*/?><?=($arGroup["GUIDПрофиль"] == $arProfile["GUID"]) ? 'selected' : ''?> <?/*disabled  */?>><?=$arProfile["Наименование"]?></option>
														<?}?>
													<?}else{?>
														<option attr-code="<?=$arItem["Профили"]["GUID"]?>" value="<?=$arItem["Профили"]["GUID"]?>" selected><?=$arItem["Профили"]["Наименование"]?></option>
													<?}?>
												<?}?>
												</select>
											</td>
											<td>
												<?=$arGroup["RequestStatus"]?><br>
                                                <?if(!$localFl){?>
                                                    <?if($arGroup["RequestStatus"] == "На доработке" || ($arGroup["RequestStatus"] != "Принята" && $arGroup["ПутьКФайлу"] == '')){?>
                                                        <div class="input-file-container">
                                                            <input class="input-file-<?=$key?>" id="my-file-<?=$key?>" name="applctn[<?=$key?>]" type="file" accept=".jpg, .pdf,.rtf,.xls,.xlsx,.doc,.docx">
                                                            <label tabindex="0" for="my-file-<?=$key?>" class="input-file-trigger-<?=$key?>"><i class="fas fa-upload"></i><span>загрузить файл...</span></label>
                                                            <p class="file-return-1 file-result"><span class="is-error"><?/*=($_SESSION["enrollee"]["appGET"][$arGroup["GUID"]]["take"]) ? 'Загрузите файл' : ''*/?></span></p>
                                                        </div>
                                                    <?}?>
                                                <?}else{?>
                                                    <p>абитуриенты работающие из ЛВС не могут прикреплять файлы</p>
                                                <?}?>
												<?/*[Состояние] - <?=($arGroup["RequestStatus"]) ? $arGroup["RequestStatus"] : 'На&nbsp;проверке'?><br>*/?>
											</td>
										</tr>
									<?}?>
								<?}?>
								<?
								foreach($arRezCompGr as $k => $arGroup){
									$key++;?>
									<tr>
										<td>
											<?=$arCompGrGUID[$arGroup["GUID"]]["Наименование"]?>
											<?if(array_key_exists($arGroup["GUID"], $arSamples)){
												if($arSamples[$arGroup["GUID"]]["PROPERTIES"]["FILE"]["VALUE"]){?>
													<br><a target="_blank" href="<?=CFile::GetPath($arSamples[$arGroup["GUID"]]["PROPERTIES"]["FILE"]["VALUE"])?>" title="открыть/скачать файл">Скачать шаблон заявления</a>
												<?}?>
											<?}?>
											<input type="hidden" name="app[<?=$key?>][name]" value="<?=$arCompGrGUID[$arGroup["GUID"]]["Наименование"]?>">
											<input type="hidden" name="app[<?=$key?>][guid]" value="<?=$arGroup["GUID"]?>">
											<input type="hidden" name="app[<?=$key?>][status]" value="N">
										</td>
										<td><input type="checkbox" name="app[<?=$key?>][take]" value="addcheck<?=$key?>" <?if(!$localFl){?>data-file="1"<?}?> data-id="<?=$key?>" <?=($_SESSION["enrollee"]["appGET"][$arGroup["GUID"]]["take"]) ? 'checked' : ''?>></td>
										<td><input type="radio" name="app_main" value="<?=$arGroup["GUID"]/*$k?>addmain<?=$key*/?>" data-id="<?=$key?>" <?=($_SESSION['enrollee']["app_main"]==$arGroup["GUID"]) ? 'checked' : ''?> <?=($radioFl) ? 'class="is-disabled" onclick="return false"' : ''?>></td>
										<td>
											<?$arItem = $arSpecialtyGUID[$arGroup["GUIDНаправления"]];?>
                                            <input type="hidden" name="app[<?=$key?>][guid_spec]" value="<?=$arGroup["GUIDНаправления"]?>">
											<?if(!empty($arItem)){?>
												<?//=$_SESSION["enrollee"]["appGET"][$arGroup["GUID"]]["profile"]?>
												<select name="app[<?=$key?>][profile]" style="max-width: 200px">
													<option value="">Выберите профиль/программу</option>
													<?if(is_array($arItem["Профили"][0])){
														foreach($arItem["Профили"] as $arProfile){?>
															<option attr-code="<?=$arProfile["GUID"]?>" value="<?=$arProfile["GUID"]?>" <?=(($_SESSION["enrollee"]["appGET"][$arGroup["GUID"]]["profile"] == $arProfile["GUID"])) ? 'selected' : ''?> <?/*=(($_GET["speciality"] == $arProfile["GUID"]) && isset($_GET["speciality"]) && !empty($_GET["speciality"])) ? 'selected' : ''*/?>><?=$arProfile["Наименование"]?></option>
														<?}?>
													<?}else{?>
														<option attr-code="<?=$arItem["Профили"]["GUID"]?>" value="<?=$arItem["Профили"]["GUID"]?>" <?=(($_SESSION["enrollee"]["appGET"][$arGroup["GUID"]]["profile"] == $arItem["Профили"]["GUID"])) ? 'selected' : ''?>><?=$arItem["Профили"]["Наименование"]?></option>
													<?}?>
												</select>
											<?}?>
										</td>
										<td>
											<?if(!$localFl){?>
												<div class="input-file-container">
													<input class="input-file-<?=$key?>" id="my-file-<?=$key?>" name="applctn[<?=$key?>]" type="file" accept=".jpg, .pdf,.rtf,.xls,.xlsx,.doc,.docx">
													<label tabindex="0" for="my-file-<?=$key?>" class="input-file-trigger-<?=$key?>"><i class="fas fa-upload"></i><span>загрузить файл...</span></label>
													<p class="file-return-1 file-result"><span class="is-error"><?/*=($_SESSION["enrollee"]["appGET"][$arGroup["GUID"]]["take"]) ? 'Загрузите файл' : ''*/?></span></p>
												</div>
											<?}else{?>
												<p>абитуриенты работающие из ЛВС не могут прикреплять файлы</p>
											<?}?>
										</td>
									</tr>
								<?}?>
								<?if(count($arRezCompGr)==0 && count($arResultApp)==0){?>
									<tr>
										<td colspan="5" class="text-center">
											<br><b>По Вашему запросу ничего не найдено</b><br><br>
										</td>
									</tr>

								<?}?>
							</tbody>
						</table>
						<div class="msf-navigation">
							<div class="row">
								<div class="nav-btn">
									<button type="submit" data-type="submit" name="action" value="save" class="btn btn-primary msf-nav-button">Сохранить выбранные и продолжить подбор</button>&nbsp;&nbsp;
									<button type="submit" data-type="submit" name="action" value="send" class="btn btn-primary msf-nav-button">Далее</button>
									<?/*
									<button type="submit" data-type="submit" name="action" class="btn btn-primary msf-nav-button">отправить</button>
									*/?>
								</div>
							</div>
						</div>
						<input type="hidden" name="submit" value="Y">
					</form>
				</div>
				<?/*?>
					<pre>
						<?print_r($arRezCompGr)?>
					</pre>
				<?*/?>
				<br><br><br>
			</div>
		</div>
	</div>
</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>