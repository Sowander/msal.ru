<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Сведения о ЕГЭ");?>
<?global $USER;?>
<?session_start();?>
<?
$arErrors = array();
$arResult = array();
$arResultAb = array();
$arResultEge = array();
$arRslt = array();
if(!$USER->IsAuthorized()){
    //переадресация на авторизацию
    header('Location: https://msal.me/enrollee/step_0.php');
    //exit();
}else{
    $rsUser = CUser::GetByLogin($USER->GetLogin());
    $arUser = $rsUser->Fetch();
    //echo "<pre>"; print_r($arUser); echo "</pre>";
    if(!$arUser["XML_ID"]){
        $arErrors[] = "У Пользователя не заполнено поле XML_ID";
        //ShowError("У Пользователя не заполнено поле XML_ID");
        $USER->Logout();
        $err = urlencode("Ошибка получения данных (XML_ID) для ".$arUser["LOGIN"].", обратитесь к администратору");
        header('Location: /enrollee/?err='.$err, true, 301);
        die();
    }else{
        $client = getSoapClient(WSABITURS_URL);
        if($client){
            $params = array("ID" => $arUser["XML_ID"]);
            try{
                $result = $client->ПолучитьСписокАбитуриентов($params);
                $arRslt = json_decode(json_encode($result->return->Состав), TRUE);
                $arResultAb = json_decode(json_encode($result->return->Состав->Абитуриент), TRUE);
                //проверка на пустой ответ из 1С
                isEmptyArr1C($arResultAb, 5, true);
                $arResultBufEge = json_decode(json_encode($result->return->Состав->РезультатыЕГЭ), TRUE);
                if(is_array($arResultBufEge[0])){
                    $arResultEge = $arResultBufEge;
                }elseif(is_array($arResultBufEge)){
                    $arResultEge[0] = $arResultBufEge;
                }else{
                    $arResultEge = array();
                }
            }catch (Exception $e){
                $arErrors[] = 'Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n";
                //ShowError('Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n");
            }
            if($_POST["submit"] == "Y"){
                $arData = array();
                /*
                $arData[] = array("NAME_1C" => "ПаспортВыдан", "NAME_POST" => "doctwho");
                $arData[] = array("NAME_1C" => "ПаспортКодПодразделения", "NAME_POST" => "doctcode");
                */

                foreach($arResultEge as $key => $arItem){
                    if($arItem["Дисциплина"] == "Русский"){
                        if(isset($_POST["russian"]) && !empty(isset($_POST["russian"])) && intval($_POST["russian"])>0){
                            $arResult["Русский"] = intval($_POST["russian"]);
                            //$arResultEge[$key]["Отметка"] = intval($_POST["russian"]);
                        }
                    }
                    if($arItem["Дисциплина"] == "Обществознание"){
                        if(isset($_POST["socialscience"]) && !empty(isset($_POST["socialscience"])) && intval($_POST["socialscience"])>0){
                            $arResult["Обществознание"] = intval($_POST["socialscience"]);
                            //$arResultEge[$key]["Отметка"] = intval($_POST["socialscience"]);
                        }
                    }
                    if($arItem["Дисциплина"] == "История"){
                        if(isset($_POST["history"]) && !empty(isset($_POST["history"])) && intval($_POST["history"])>0){
                            $arResult["История"] = intval($_POST["history"]);
                            //$arResultEge[$key]["Отметка"] = intval($_POST["history"]);
                        }
                    }
                }
            }else{
                foreach($arResultEge as $key => $arItem){
                    if($arItem["Дисциплина"] == "Русский язык"){
                        $arItem["Дисциплина"] = "Русский";
                    }
                    $arResult[$arItem["Дисциплина"]] = $arItem["Отметка"];
                }
            }

        }
        /*
                                    echo "<pre>";
                                    //print_r($arResult);
                                    //print_r($arRslt);
                                    print_r($arResultAb);
                                    //print_r($arResultEge);
                                    //print_r(getAbitEgeFldsToMaindata($arUser["XML_ID"]));
                                    ////print_r($_POST);
                                    echo "</pre>";
        */
    }
}?>
<?if($arResultAb["СтатусЗаявки"] == "На доработке"){?>
    <form id="formstatus" method="post" action="">
        <input type="hidden" name="changeSt" value="Y">
    </form>
<?}?>
<form class="form-horizontal msf js-form-address<?if($arResult["СтатусЗаявки"]=="Принята"){?> is-disabled<?}?>" id="formmsf5" method="post" action="">
    <div class="msf-header">
        <div class="row msf-header__top">
            <div class="msf-header__column"><div class="msf-header__status">Статус: <span id="status"><?=$arResultAb["СтатусЗаявки"]?></span></div></div>
            <?if($arResultAb["СтатусЗаявки"]=="На доработке"){?>
                <div class="msf-header__column">
                    <a href="#" id="changeStatusSend" class="btn btn-primary">Отправить данные на проверку</a>
                </div>
            <?}?>
            <div class="msf-header__column msf-header__column_logout"><a href="/enrollee/?logout=yes">Выход</a></div>
        </div>
		<div class="row text-left">
            <?if($arResultAb["СтатусЗаявки"]=="На доработке"){?>
                <div class="msf-step"><a href="step_1.php">Основные данные</a></div>
                <div class="msf-step"><a href="step_2.php">Семья</a></div>
                <div class="msf-step"><a href="step_3.php">Дополнительная информация</a></div>
                <div class="msf-step"><a href="step_4.php">Предоставленные документы</a></div>
                <div class="msf-step active msf-step-active"><a href="step_5.php">Сведения о ЕГЭ</a></div>
                <div class="msf-step"><a href="step_6.php">Заявления</a></div>
                <div class="msf-step"><a href="info.php">Инфопанель</a></div>
            <?}else{?>
                <div class="msf-step"><span>Основные данные</span></div>
                <div class="msf-step"><span>Семья</span></div>
                <div class="msf-step"><span>Дополнительная информация</span></div>
                <div class="msf-step"><span>Предоставленные документы</span></div>
                <div class="msf-step active msf-step-active"><span>Сведения о ЕГЭ</span></div>
                <div class="msf-step"><span>Заявления</span></div>
                <div class="msf-step"><a href="info.php">Инфопанель</a></div>
            <?}?>
		</div>
    </div>
    <div class="msf-content">
        <div class="msf-view">
            <div class="row">
                <div class="col-md-12">
                    <h3>Сведения о наличии ЕГЭ, заявленных абитуриентом</h3>
                </div>
                <div class="col-md-12">
                    <?echo '<input id="status-inp" type="hidden" value="'.$arResultAb["СтатусЗаявки"].'">';?>
                    <p><b>Поступающие на программы магистратуры и лица, не сдававшие ЕГЭ, могут пропустить этот шаг, оставив в строках значения 0 - просто нажмите кнопку “Далее”.</b></p>
                    <br>
                    <?foreach($arErrors as $error){
                        ShowError($error);
                    }?>
                    <?if($_GET["msg"]=="wrngs"){
                        ShowError("Данные не сохранены. Большая нагрузка на сервис. Введите данные еще раз.");
                    }?>
                    <?if($_POST["changeSt"]=="Y"){?>
                        <?require($_SERVER["DOCUMENT_ROOT"]."/enrollee/action/action_chng_st.php");?>
                    <?}?>
                    <?if($_POST["submit"]=="Y"){?>
                        <?require($_SERVER["DOCUMENT_ROOT"]."/enrollee/action/action_step_5.php");?>
                    <?}?>
                    <div class="ege-ducument row">
                        <div class="col-sm-12 sub-information">В случае, если ЕГЭ сдавались по другому удостов. личности, просьба заполнить их реквизиты.</div>
                        <div class="col-md-4 series ege-series">
                            <input type="text" class="form-control" id="EgeDocSeries" name="EgeDocSeries" value="<?=$arResultAb["ПаспортСерия2"]?>" placeholder="Серия">
                        </div>
                        <div class="col-md-4 number ege-number">
                            <input type="text" class="form-control" id="EgeDocNumber" name="EgeDocNumber" value="<?=$arResultAb["ПаспортНомер2"]?>" placeholder="Номер">
                        </div>
                        <div class="col-md-4 date date-document-type">
                            <input type="text" class="form-control" id="DocDate" name="EgeDocDate" value="<?=date('d.m.Y', strtotime($arResultAb["ПаспортДата2"]))?>" placeholder="Дата выдачи" data-msg="Введите дату выдачи" required>
                        </div>
                    </div>

                    <div id="ege-group" class="ege-group">
                        <div class="form-group">
                            <label for="ege-russian" class="ege-label">Русский <span class="required">*</span></label>
                            <input type="text" class="form-control" id="russian" name="russian" placeholder="40" data-val="true" data-msg="Введите значение (от 40 до 100) или 0" min="40" max="100" onkeyUp="return onlynumber(this);" required value="<?=($arResult["Русский"])?$arResult["Русский"]:0?>">
                        </div>
                        <div class="form-group">
                            <label for="ege-socialscience" class="ege-label">Обществознание <span class="required">*</span></label>
                            <input type="text" class="form-control" id="socialscience" name="socialscience" placeholder="50" data-val="true" data-msg="Введите значение (от 50 до 100) или 0" min="50" max="100" onkeyUp="return onlynumber(this);" required value="<?=($arResult["Обществознание"])?$arResult["Обществознание"]:0?>">
                        </div>
                        <div class="form-group">
                            <label for="ege-history" class="ege-label">История <span class="required">*</span></label>
                            <input type="text" class="form-control" id="history" name="history" placeholder="36" data-val="true" data-msg="Введите значение (от 36 до 100) или 0" min="36" max="100" onkeyUp="return onlynumber(this);"  required value="<?=($arResult["История"])?$arResult["История"]:0?>">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="msf-navigation">
        <div class="row">
			<p class="text-left"><br><span class="required">*</span> &mdash; поля обязательные для заполнения</p>

            <div class="nav-btn">
                <button type="button" data-type="back" class="btn btn-default msf-nav-button"><i class="fa fa-chevron-left"></i> назад </button>
            </div>
            <div class="nav-btn">
                <button type="button" data-type="next" class="btn btn-default msf-nav-button">далее <i class="fa fa-chevron-right"></i></button>

                <button type="submit" data-type="submit" class="btn btn-primary msf-nav-button">далее</button>
            </div>
        </div>
    </div>
    <input type="hidden" name="submit" value="Y">
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>