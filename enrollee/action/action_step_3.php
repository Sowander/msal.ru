<?
//error_reporting(E_ALL ^ E_NOTICE);
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $USER;
$ip = $_SERVER['REMOTE_ADDR'];
//отработать-показать условия и текст для внешнего диапазона
if(($ip < '10.0.0.1') || ($ip > '10.255.255.254')){
    $localFl = false;
//ЛВС
}else{
    $localFl = true;
}?>
<?if(!$USER->IsAuthorized()){
    header('Location: /enrollee/step_0.php', true, 301);
    die();
}else{
    $rsUser = CUser::GetByLogin($USER->GetLogin());
    $arUser = $rsUser->Fetch();
    if(!$arUser["XML_ID"]){
        ShowError("У Пользователя не заполнено поле XML_ID");
        //sleep(10);
        //header('Location: /enrollee/step_0.php', true, 301);
        //die();
    }else{
        $client = getSoapClient(MAINDATA_URL);
        if($client){
            $params = array();
            $arAbitCur = getAbitMainFldsToMaindata($arUser["XML_ID"], 3);
            $arAbitNew  = array (
                "ID"            =>  $arUser["XML_ID"],
                "Online"        =>  ($localFl) ? false : true,
                "Name"          =>  trim($arUser["NAME"]), //Имя
                "Family"        =>  trim($arUser["LAST_NAME"]), //Фамилию
                "VisitedCourses"=>  $_POST["Сourses"],
                //CoursesOrganisation
                "Workplace"     =>  trim($_POST["workplace"]), //Место работы
                "Workpost"      =>  trim($_POST["employment"]), //Трудоустройство/должность
            );
            $params["Students"]["Student"] = array_merge ($arAbitCur, $arAbitNew);
            $params["Students"]["Student"]["FamilyMembers"] = getAbitFamilyFldsToMaindata($arUser["XML_ID"],3);
            if(is_array($_POST["languages"])){
                foreach($_POST["languages"] as $arItem){
                    $params["Students"]["Student"]["Languages"][] =
                        array(
                            "ID"    => "",
                            "Language" => $arItem["language"],
                            "IsMain" => ($arItem["main_language"]) ? true : false,
                            //"" => $arItem["check_language"]
                        );
                }
            }
            $params["Students"]["Student"]["EgeResults"] = getAbitEgeFldsToMaindata($arUser["XML_ID"], 3);
            $params["Students"]["Student"]["EntrantsFiles"] = getAbitFilesToMaindata($arUser["XML_ID"], false, 3);
            $params["Students"]["Student"]["AttachedDocuments"] = getAbitDocsToMaindata($arUser["XML_ID"],false,3);
            $params["Students"]["Student"]["Requests"] = getAbitAppToMaindata($arUser["XML_ID"], 3);
            saveToLog2("*********************************************************************",$arUser["XML_ID"],3);
            try{
                $result = $client->ToGetMainData($params);
                $savedId = $result->return->SavedID;
                if(!$savedId){
                    ShowError("Ошибка! ".$result->return->ErrorMessage);
                    //exit();
                }else{
                   // saveToLog("\n===STEP_3====\n".print_r($params,1));
					saveToLog(print_r($params,1),$arUser["XML_ID"],3);
                    echo "<p class='ok'>Ваши данные успешно сохранены. <br>Для продолжения перейдите по <a href='/enrollee/step_4.php'><b>ссылке</b></a></p>";
                    header('Location: /enrollee/step_4.php', true, 301);
                    die();
                }
            }catch (Exception $e){
                ShowError('Выброшено исключение: '.$e->getMessage()."\n");
            }
        }else{
            ShowError("Пользователь не найден");
        }
    }
}?>

