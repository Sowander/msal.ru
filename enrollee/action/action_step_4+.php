<?
//error_reporting(E_ALL ^ E_NOTICE);
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('PATH_TO_1C_REMOTE_FOLDER', '\\\\1CSERVICES');
$siteUrl = 'https://msal.me';
$REMOTE_FILE_PATH = '\public\pk2018\\';
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $USER;
$ip = $_SERVER['REMOTE_ADDR'];
//отработать-показать условия и текст для внешнего диапазона
if(($ip < '10.0.0.1') || ($ip > '10.255.255.254')){
    $localFl = false;
//ЛВС
}else{
    $localFl = true;
}?>
<?if(!$USER->IsAuthorized()){
    header('Location: /enrollee/step_0.php', true, 301);
    die();
}else{
    $rsUser = CUser::GetByLogin($USER->GetLogin());
    $arUser = $rsUser->Fetch();
    if(!$arUser["XML_ID"]){
        ShowError("У Пользователя не заполнено поле XML_ID");
        //sleep(10);
        //header('Location: /enrollee/step_0.php', true, 301);
        //die();
    }else{
        $client = getSoapClient(MAINDATA_URL);
        if($client){
            $params = array();
            $arAbitCur = getAbitMainFldsToMaindata($arUser["XML_ID"], 41);
            $arAbitNew  = array (
                "ID"            =>  $arUser["XML_ID"],
                "Online"        =>  ($localFl) ? false : true,
                "Name"          =>  trim($arUser["NAME"]),        //Имя
                "Family"        =>  trim($arUser["LAST_NAME"]),   //Фамилию
                "DateOfIssue"   =>  date('d.m.Y',strtotime($_POST["doctdate"])), //Дата выдачи
                "DepartmentCode"=>  trim($_POST["doctcode"]),     //Код подразделения
                "Department"    =>  trim($_POST["doctwho"]),      //Кем выдан
                //$_POST["document-education"];
                //$_POST["serieseducationdoc"];
                //$_POST["numbereducationdoc"];
                //$_POST["dateeducationdoc"];
            );





            $params["Students"]["Student"] = array_merge ($arAbitCur, $arAbitNew);
            $params["Students"]["Student"]["FamilyMembers"] = getAbitFamilyFldsToMaindata($arUser["XML_ID"], 41);
            $params["Students"]["Student"]["Languages"] = getAbitLangFldsToMaindata($arUser["XML_ID"], 41);
            //$params["Students"]["Student"]["EgeResults"] = getAbitEgeFldsToMaindata($arUser["XML_ID"]);


            $params["Students"]["Student"]["EgeResults"] = getAbitEgeFldsToMaindataTEST($arUser["XML_ID"],'4+');


            $params["Students"]["Student"]["Requests"] = getAbitAppToMaindata($arUser["XML_ID"], 41);
            $bufFiles = getAbitFilesToMaindata($arUser["XML_ID"], true, 41);
            $bufDocs = getAbitDocsToMaindata($arUser["XML_ID"], true, 41);

            saveToLog(print_r($params,1),$arUser["XML_ID"],41);
            LocalRedirect("/enrollee/step_4+.php?msg=ok");
            die();

            /*

            try{
                $result = $client->ToGetMainData($params);
                $savedId = $result->return->SavedID;
                if(!$savedId){
                    ShowError("Ошибка! ".$result->return->ErrorMessage);
                    //exit();
                }else{
                    //saveToLog("\n===STEP_4+====\n".print_r($params,1));
					saveToLog(print_r($params,1),$arUser["XML_ID"],41);
                    LocalRedirect("/enrollee/step_4+.php?msg=ok");
                    die();
                }
            }catch (Exception $e){
                ShowError('Выброшено исключение: '.$e->getMessage()."\n");
            }
            */
        }else{
            ShowError("Пользователь не найден");
        }
    }
}?>

