<?
//error_reporting(E_ALL ^ E_NOTICE);
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $USER;
$ip = $_SERVER['REMOTE_ADDR'];
//отработать-показать условия и текст для внешнего диапазона
if(($ip < '10.0.0.1') || ($ip > '10.255.255.254')){
    $localFl = false;
//ЛВС
}else{
    $localFl = true;
}?>
<?if(!$USER->IsAuthorized()){
    header('Location: /enrollee/step_0.php', true, 301);
    die();
}else{
    $rsUser = CUser::GetByLogin($USER->GetLogin());
    $arUser = $rsUser->Fetch();
    if(!$arUser["XML_ID"]){
        ShowError("У Пользователя не заполнено поле XML_ID");
        //header('Location: /enrollee/step_0.php', true, 301);
        //die();
    }else{
        $client = getSoapClient(MAINDATA_URL);
        if($client){
            $params = array();
            $arAbitCur = getAbitMainFldsToMaindata($arUser["XML_ID"], 1);
            $arAbitNew = array (
                "ID"            =>  $arUser["XML_ID"],
                "Online"        =>  ($localFl) ? 0 : 1,
                "Name"          =>  trim($_POST["firstname2"]),   //Имя
                "Family"        =>  trim($_POST["lastname2"]),    //Фамилию
                'Surname'       =>  trim($_POST["patronymic2"]),  //Отчество
                'DateOfBirth'   =>  date('Y-m-d',strtotime($_POST["birthdate2"])), //Дата рождения
                'Sex'           =>  1, //Пол
                'NeedHostel'    =>  '0', //нуждаюсь в общежитии
                'Foreigner'     =>  '0', //иностранный гражданин
                'PlaceOfBirth'  =>  $_POST["birthplace"], //Место рождения
                'Patriality'    =>  trim($_POST["countries-sel"]), //Гражданство
                'RegionFIS'     =>  trim($_POST["regions-sel"]),  //Регион (для ФИС)
                'PlaceTypeFIS'  =>  trim($_POST["settlement-sel"]), //Тип населенного пункта
                'IndexR'        =>  trim($_POST["zip"]), //Адрес регистрации - Индекс
                'CityR'         =>  trim($_POST["city"]), //Адрес регистрации - Город
                'RegionR'       =>  trim($_POST["region"]), //Адрес регистрации - Регион
                'LocalityR'     =>  trim($_POST["district"]), //Адрес регистрации - Населенный пункт
                'StreetR'       =>  trim($_POST["street"]), //Адрес регистрации - Улица
                'HouseR'        =>  trim($_POST["building"]), //Адрес регистрации - Дом
                'FlatR'         =>  trim($_POST["flat"]), //Адрес регистрации - Квартира
                'TelContact'    =>  $_POST["contactphone"], //Телефон контактный
                'TelHome'       =>  $_POST["homephone"], //Телефон домашний
                'TelWork'       =>  $_POST["workphone"], //Телефон служебный
            );
            if($_POST["usergender"]=="w"){
                $arAbitNew["Sex"] = '0';
            }
            if($_POST["needhostel"]){
                $arAbitNew["NeedHostel"] = 1;
            }
            if($_POST["user_patronymic_empty"]){
                $arAbitNew["Surname"] = '';
            }
            if($_POST["foreigncitizen"]){
                $arAbitNew["Foreigner"] = 1;
            }
            //адрес регистрации совпадает с адресом проживания
            if($_POST["sameresidenceaddress_check"]){
                $arAbitNew["IndexL"] = trim($_POST["zip"]); //Адрес регистрации - Индекс
                $arAbitNew["CityL"] = trim($_POST["city"]); //Адрес регистрации - Город
                $arAbitNew["RegionL"] = trim($_POST["region"]); //Адрес регистрации - Регион
                $arAbitNew["LocalityL"] = trim($_POST["district"]); //Адрес регистрации - Населенный пункт
                $arAbitNew["StreetL"] = trim($_POST["street"]); //Адрес регистрации- Улица
                $arAbitNew["HouseL"] = trim($_POST["building"]); //Адрес регистрации - Дом
                $arAbitNew["FlatL"] = trim($_POST["flat"]); //Адрес регистрации - Квартира
            }else{
                $arAbitNew["IndexL"] = trim($_POST["zipP"]); //Адрес проживания - Индекс
                $arAbitNew["CityL"] = trim($_POST["cityP"]); //Адрес проживания - Город
                $arAbitNew["RegionL"] = trim($_POST["regionP"]); //Адрес проживания - Регион
                $arAbitNew["LocalityL"] = trim($_POST["districtP"]); //Адрес проживания - Населенный пункт
                $arAbitNew["StreetL"] = trim($_POST["streetP"]); //Адрес проживания- Улица
                $arAbitNew["HouseL"] = trim($_POST["buildingP"]); //Адрес проживания - Дом
                $arAbitNew["FlatL"] = trim($_POST["flatP"]); //Адрес проживания - Квартира
            }
            /*
            if($_POST["documenttype2-checked"]){
                $arAbitNew["DocType2"] = $_POST["inddocuments3"]; //Тип удостоверения личности №2 - тип документа
                $arAbitNew["Series2"] = trim($_POST["passportseries3"]); //Тип удостоверения личности №2 - серия
                $arAbitNew["Number2"] = trim($_POST["passportnumber3"]); //Тип удостоверения личности №2 - номер
            }else{
                $arAbitNew["DocType2"] = ''; //Тип удостоверения личности №2 - тип документа
                $arAbitNew["Series2"] = ''; //Тип удостоверения личности №2 - серия
                $arAbitNew["Number2"] = ''; //Тип удостоверения личности №2 - номер
            }
            */
            $params["Students"]["Student"] = array_merge ($arAbitCur, $arAbitNew);
            $params["Students"]["Student"]["FamilyMembers"] = getAbitFamilyFldsToMaindata($arUser["XML_ID"],1);
            $params["Students"]["Student"]["Languages"] = getAbitLangFldsToMaindata($arUser["XML_ID"],1);
            $params["Students"]["Student"]["EgeResults"] = getAbitEgeFldsToMaindata($arUser["XML_ID"],1);
            $params["Students"]["Student"]["EntrantsFiles"] = getAbitFilesToMaindata($arUser["XML_ID"], false, 1);
            $params["Students"]["Student"]["AttachedDocuments"] = getAbitDocsToMaindata($arUser["XML_ID"], false, 1);
            $params["Students"]["Student"]["Requests"] = getAbitAppToMaindata($arUser["XML_ID"],1);
            saveToLog2("*********************************************************************",$arUser["XML_ID"],1);
/*
echo "<pre>";
print_r($params);
echo "</pre>";
*/

            try{
                $result = $client->ToGetMainData($params);
                $savedId = $result->return->SavedID;
                $status = $result->return->RequestStatus;

                //var_dump($status);
                if(!$savedId){
                    ShowError("Ошибка! ".$result->return->ErrorMessage);
                    //exit();
                }else{
                    saveToLog(print_r($params,1),$arUser["XML_ID"],1);
                    $user = new CUser;
                    $fields = Array(
                        "NAME"              => $_POST["firstname2"],
                        "SECOND_NAME"       => $_POST["patronymic2"],
                        "LAST_NAME"         => $_POST["lastname2"],
                        //"EMAIL"             => $_POST["email2"],
                        //"LOGIN"             => $_POST["email2"],
                        "PERSONAL_BIRTHDAY" => $_POST["birthdate2"]
                        //серия паспорта
                        //номер паспорта
                    );
                    $user->Update($arUser["ID"], $fields);
                    $strError = $user->LAST_ERROR;
                    if(strlen($strError)>0){
                        ShowError($strError);
                    }
                    LocalRedirect("/enrollee/step_1.php?msg=ok");
                    die();
                    //echo "<p class='ok'>Ваши основные данные успешно сохранены. <br>Для продолжения перейдите по <a href='/enrollee/step_2.php'><b>ссылке</b></a></p>";
                    //header('Location: /enrollee/step_2.php', true, 301);
                    //die();
                }
            }catch (Exception $e){
                ShowError('Выброшено исключение: '.$e->getMessage()."\n");
            }
        }else{
            ShowError("Пользователь не найден");
        }
    }
}?>

