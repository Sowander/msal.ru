<?
//error_reporting(E_ALL ^ E_NOTICE);
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('PATH_TO_1C_REMOTE_FOLDER', '\\\\1CSERVICES');
$siteUrl = 'https://msal.me';
$REMOTE_FILE_PATH = '\public\pk2018\\';
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $USER;
$ip = $_SERVER['REMOTE_ADDR'];
//отработать-показать условия и текст для внешнего диапазона
if(($ip < '10.0.0.1') || ($ip > '10.255.255.254')){
    $localFl = false;
//ЛВС
}else{
    $localFl = true;
}?>
<?if(!$USER->IsAuthorized()){
    header('Location: /enrollee/step_0.php', true, 301);
    die();
}else{
    $rsUser = CUser::GetByLogin($USER->GetLogin());
    $arUser = $rsUser->Fetch();
    if(!$arUser["XML_ID"]){
        ShowError("У Пользователя не заполнено поле XML_ID");
        //sleep(10);
        //header('Location: /enrollee/step_0.php', true, 301);
        //die();
    }else{
        $client = getSoapClient(MAINDATA_URL);
        if($client){
            $params = array();
            $arAbitCur = getAbitMainFldsToMaindata($arUser["XML_ID"], 4);
            $arAbitNew  = array (
                "ID"            =>  $arUser["XML_ID"],
                "Online"        =>  ($localFl) ? false : true,
                "Name"          =>  trim($arUser["NAME"]),        //Имя
                "Family"        =>  trim($arUser["LAST_NAME"]),   //Фамилию
                "DateOfIssue"   =>  date('d.m.Y',strtotime($_POST["doctdate"])), //Дата выдачи
                "DepartmentCode"=>  trim($_POST["doctcode"]),     //Код подразделения
                "Department"    =>  trim($_POST["doctwho"]),      //Кем выдан
/*
                "Email" => "ap@g-lab.ru",
                "DocType" => "Паспорт гражданина РФ",
                "Series" => "7777",
                "Number" => "9999",
*/

                //$_POST["document-education"];
                //$_POST["serieseducationdoc"];
                //$_POST["numbereducationdoc"];
                //$_POST["dateeducationdoc"];
            );

            $params["Students"]["Student"] = array_merge ($arAbitCur, $arAbitNew);
            $params["Students"]["Student"]["FamilyMembers"] = getAbitFamilyFldsToMaindata($arUser["XML_ID"], 4);
            $params["Students"]["Student"]["Languages"] = getAbitLangFldsToMaindata($arUser["XML_ID"], 4);
            $params["Students"]["Student"]["EgeResults"] = getAbitEgeFldsToMaindata($arUser["XML_ID"], 4);
            $bufFiles = getAbitFilesToMaindata($arUser["XML_ID"], true, 4);
            $bufDocs = getAbitDocsToMaindata($arUser["XML_ID"], true, 4);
            $params["Students"]["Student"]["Requests"] = getAbitAppToMaindata($arUser["XML_ID"], 4);
            saveToLog2("*********************************************************************",$arUser["XML_ID"],4);

            //отработать-показать условия и текст для внешнего диапазона
            if(!$localFl){
                $remotePath = $arUser["XML_ID"].'\\'; //папка с названием ИД пользователя
                if(isset($_POST["inddoc4code"]) && !empty($_POST["inddoc4code"])){
                    //файл - Удостоверение личности
                    $arrFile = Array(
                        "name" => $_FILES["identification-card"]["name"],
                        "size" => $_FILES["identification-card"]["size"],
                        "tmp_name" => $_FILES["identification-card"]["tmp_name"],
                        "type" => "",
                        "old_file" => "",
                        "del" => "Y",
                        "MODULE_ID" => "iblock"
                    );
                    $ext = substr(strrchr($arrFile["name"], '.'), 1);
                    if(strlen($ext) > 0){
                        $ext = ".".$ext;
                    }
                    if((strlen($arrFile["name"]) > 0 || strlen($arrFile["del"]) > 0) && $arrFile["error"] == 0 && $arrFile["size"] > 0){
                        $ftp1C = new Ftp1CDev();
                        $filePath1C = $ftp1C->filePut($arrFile["tmp_name"], $remotePath, "passport".$ext/*$arrFile['identification-card']["name"]*/);
                        $fileDir = "";
                        if($filePath1C){
                            if(empty($fileDir)){
                                $fileDir = str_replace($_FILES['identification-card']["name"], "", $filePath1C);
                            }
                            $subDir = '';
                            $fileName = '';
                            //вырезаем из строки $REMOTE_FILE_PATH
                            $filePath = str_replace($REMOTE_FILE_PATH, "", $filePath1C);
                            $bufFiles[trim($_POST["inddoc4code"])] = array(
                                "ID" => "",
                                "File" => "",
                                "FilePath" => $filePath,
                                "FileType" => trim($_POST["inddoc4code"]), //
                                "DateChange" => date('Y-m-d')
                            );
                            /*
                            //удаление файла
                            $arrPath = explode("\\", $filePath);
                            if(strlen($arrPath[0])>0 && strlen($arrPath[1])>0){
                                $subDir = $arrPath[0]."\\";
                                $fileName = $arrPath[1];
                            }
                            $filePath1C = $ftp1C->fileDelete($subDir,$fileName);
                            */
                        }else{
                            ShowError("Не удалось загрузить файл для Удостоверения личности");
                        }
                    }
                }

                if(isset($_POST["education"]) && !empty($_POST["education"])){
                    //файл - Документ об образовании
                    $arrFile = Array(
                        "name" => $_FILES["document-education"]["name"],
                        "size" => $_FILES["document-education"]["size"],
                        "tmp_name" => $_FILES["document-education"]["tmp_name"],
                        "type" => "",
                        "old_file" => "",
                        "del" => "Y",
                        "MODULE_ID" => "iblock"
                    );
                    $ext = substr(strrchr($arrFile["name"], '.'), 1);
                    if(strlen($ext) > 0){
                        $ext = ".".$ext;
                    }
                    if((strlen($arrFile["name"]) > 0 || strlen($arrFile["del"]) > 0) && $arrFile["error"] == 0 && $arrFile["size"] > 0){
                        $ftp1C = new Ftp1CDev();
                        $filePath1C = $ftp1C->filePut($arrFile["tmp_name"], $remotePath, "education_".trim($_POST["education"]).$ext);
                        $fileDir = "";
                        if($filePath1C){
                            if(empty($fileDir)){
                                $fileDir = str_replace($_FILES['document-education']["name"], "", $filePath1C);
                            }
                            $subDir = '';
                            $fileName = '';
                            //вырезаем из строки $REMOTE_FILE_PATH
                            $filePath = str_replace($REMOTE_FILE_PATH, "", $filePath1C);
                            $bufFiles[trim($_POST["education"])] = array(
                                "ID" => "",
                                "File" => "",
                                "FilePath" => $filePath,
                                "FileType" => trim($_POST["education"]), //  Документ об образовании - Тип
                                "DateChange" => date('Y-m-d')
                            );
                        }else{
                            ShowError("Не удалось загрузить файл Документа об образовании");
                        }
                    }
                    //AttachedDocuments туда все документы кроме паспортов
                    //Но это НЕ файлы, а реквизиты
                    if($_POST["education"] && ($_POST["serieseducationdoc"] || $_POST["numbereducationdoc"])){
                        $bufDocs[trim($_POST["education"])] = array(
                            "ID" => "",
                            "Code" => trim($_POST["education"]), //Документ об образовании - тип
                            "Serial" => trim($_POST["serieseducationdoc"]), //Серия документа
                            "Number" => trim($_POST["numbereducationdoc"]), //Номер документа
                            "Date" => date('Y-m-d', strtotime($_POST["dateeducationdoc"])) //Дата
                        );
                    }
                    /*
                    Файлы у нас все сливались в EntrantsFiles
                    (кроме файлов заявлений)
                    */
                }

                //файлы - Документ другие
                $arrFiles = array();
                foreach($_FILES["other_documents"] as $k=>$v){
                    foreach($v as $key=>$val){
                        $arrFiles[$key][$k] = $val;
                    }
                }
                foreach($arrFiles as $k => $arrFile){
                    if($_POST["other_documents"][$k]["type"]){
                        $ext = substr(strrchr($arrFile["name"], '.'), 1);
                        if(strlen($ext) > 0){
                            $ext = ".".$ext;
                        }
                        $filename = $arrFile["name"];
                        $p = strrpos($filename, '.');
                        if ($p > 0) $filename = substr($filename, 0, $p);

                        if((strlen($arrFile["name"]) > 0 || strlen($arrFile["del"]) > 0) && $arrFile["error"] == 0 && $arrFile["size"] > 0){
                            $ftp1C = new Ftp1CDev();
                            //$nameFile = "other_doc_".trim($_POST["other_documents"][$k]["type"]).$ext;
                            $nameFile = trim($_POST["other_documents"][$k]["type"]).'_'.Cutil::translit($filename,"ru",array()).'_'.date('dmHis').$ext;
                            $filePath1C = $ftp1C->filePut($arrFile["tmp_name"], $remotePath, $nameFile);
                            $fileDir = "";
                            if($filePath1C){
                                if(empty($fileDir)){
                                    $fileDir = str_replace($nameFile, "", $filePath1C);
                                }
                                $subDir = '';
                                $fileName = '';
                                //вырезаем из строки $REMOTE_FILE_PATH
                                $filePath = str_replace($REMOTE_FILE_PATH, "", $filePath1C);
                                $bufFiles[trim($_POST["other_documents"][$k]["type"])] = array(
                                    "ID" => "",
                                    "File" => "",
                                    "FilePath" => $filePath,
                                    "FileType" => trim($_POST["other_documents"][$k]["type"]), //  Документ - Тип
                                    "DateChange" => date('Y-m-d')
                                );
                            }else{
                                //ShowError("Не удалось загрузить файл документа, тип документа - ".trim($_POST["other_documents"][$k]["type"]));
                            }
                        }
                    }
                }

                //AttachedDocuments туда все документы кроме паспортов
                //Но это НЕ файлы, а реквизиты
                foreach($_POST["other_documents"] as $k => $arrItem){
                    if($arrItem["type"] && ($arrItem["series"] || $arrItem["number"])){
                        $bufDocs[trim($arrItem["type"])] = array(
                            "ID" => "",
                            "Code" => trim($arrItem["type"]), //Документ об образовании - тип
                            "Serial" => trim($arrItem["series"]), //Серия документа
                            "Number" => trim($arrItem["number"]), //Номер документа
                            "Date" => date('Y-m-d') //Дата
                            //"Date" => date('Y-m-d', strtotime($_POST["dateeducationdoc"])) //Дата
                        );
                    }
                }
                /*
                echo "<pre>";
                print_r($bufFiles);
                print_r($bufDocs);
                echo "</pre>";
                */

                $params["Students"]["Student"]["EntrantsFiles"] = array_values($bufFiles);
                $params["Students"]["Student"]["AttachedDocuments"] = array_values($bufDocs);

            //отработать-показать условия и текст для ЛВС
            }else{
                //Документ об образовании
                if(isset($_POST["education"]) && !empty($_POST["education"]) && ($_POST["serieseducationdoc"] || $_POST["numbereducationdoc"])){
                    //AttachedDocuments туда все документы кроме паспортов
                    $bufDocs[trim($_POST["education"])] = array(
                        "ID" => "",
                        "Code" => trim($_POST["education"]), //Документ об образовании - тип
                        "Serial" => trim($_POST["serieseducationdoc"]), //Серия документа
                        "Number" => trim($_POST["numbereducationdoc"]), //Номер документа
                        "Date" => date('Y-m-d', strtotime($_POST["dateeducationdoc"])) //Дата
                    );
                }
                //Документы другие
                foreach($_POST["other_documents"] as $k => $arrItem){
                    if($arrItem["type"] && ($arrItem["series"] || $arrItem["number"])){
                        $bufDocs[trim($arrItem["type"])] = array(
                            "ID" => "",
                            "Code" => trim($arrItem["type"]), //Документ об образовании - тип
                            "Serial" => trim($arrItem["series"]), //Серия документа
                            "Number" => trim($arrItem["number"]), //Номер документа
                            "Date" => date('Y-m-d') //Дата
                            //"Date" => date('Y-m-d', strtotime($_POST["dateeducationdoc"])) //Дата
                        );
                    }
                }
                $params["Students"]["Student"]["AttachedDocuments"] = array_values($bufDocs);
            }
/*
if($arUser["XML_ID"]==2237){
    //            echo "<div style='display: none'>";
                echo "<pre>";
                print_r($_POST);
                print_r($_FILES);
                print_r($params);
                echo "</pre>";
    //            echo "</div>";
                //die();
}
*/
            try{
                $result = $client->ToGetMainData($params);
                $savedId = $result->return->SavedID;
                if(!$savedId){
                    ShowError("Ошибка! ".$result->return->ErrorMessage);
                    //exit();
                }else{
                    //saveToLog("\n===STEP_4+====\n".print_r($params,1));
					saveToLog(print_r($params,1),$arUser["XML_ID"],4);
                    LocalRedirect("/enrollee/step_4.php?msg=ok");
                    die();
                }
            }catch (Exception $e){
                ShowError('Выброшено исключение: '.$e->getMessage()."\n");
            }
        }else{
            ShowError("Пользователь не найден");
        }
    }
}?>

