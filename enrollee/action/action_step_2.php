<?
//error_reporting(E_ALL ^ E_NOTICE);
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $USER;
$ip = $_SERVER['REMOTE_ADDR'];
//отработать-показать условия и текст для внешнего диапазона
if(($ip < '10.0.0.1') || ($ip > '10.255.255.254')){
    $localFl = false;
//ЛВС
}else{
    $localFl = true;
}?>
<?if(!$USER->IsAuthorized()){
    header('Location: /enrollee/step_0.php', true, 301);
    die();
}else{
    $rsUser = CUser::GetByLogin($USER->GetLogin());
    $arUser = $rsUser->Fetch();
    if(!$arUser["XML_ID"]){
        ShowError("У Пользователя не заполнено поле XML_ID");
        //sleep(10);
        //header('Location: /enrollee/step_0.php', true, 301);
        //die();
    }else{
        $client = getSoapClient(MAINDATA_URL);
        if($client){
            $params = array();
            $arAbitCur = getAbitMainFldsToMaindata($arUser["XML_ID"], 2);
            $arAbitNew = array(
                "ID"            =>  $arUser["XML_ID"],
                "Online"        =>  ($localFl) ? false : true,
                "Name"          =>  trim($arUser["NAME"]),   //Имя
                "Family"        =>  trim($arUser["LAST_NAME"]),    //Фамилию
                "MaritalStatus" =>  $_POST["maritalstatus"] //Семейное положение
                //'DateOfBirth'   =>  date('Y-m-d',strtotime($_POST["birthdate2"])), //Дата рождения
            );
            $params["Students"]["Student"] = array_merge ($arAbitCur, $arAbitNew);
            if(is_array($_POST["family"])){
                foreach($_POST["family"] as $arItem){
                    $params["Students"]["Student"]["FamilyMembers"][] =
                        array(
                            "ID"    => "",
                            "RelationType"  => $arItem["family-degree"],
                            "FIO" => trim($arItem["family-fio"]),
                            "DateOfBirth" => date('Y-m-d',strtotime($arItem["family-bdate"])),
                            "WorkPlace" => "",
                            "Tel" => $arItem["family-phone"],
                            "Address" => trim($arItem["family-member-address"])
                        );

                }
            }
            $params["Students"]["Student"]["Languages"] = getAbitLangFldsToMaindata($arUser["XML_ID"], 2);
            $params["Students"]["Student"]["EgeResults"] = getAbitEgeFldsToMaindata($arUser["XML_ID"], 2);
            $params["Students"]["Student"]["EntrantsFiles"] = getAbitFilesToMaindata($arUser["XML_ID"], false,2);
            $params["Students"]["Student"]["AttachedDocuments"] = getAbitDocsToMaindata($arUser["XML_ID"], false, 2);
            $params["Students"]["Student"]["Requests"] = getAbitAppToMaindata($arUser["XML_ID"], 2);
            saveToLog2("*********************************************************************",$arUser["XML_ID"],2);
            try{
                $result = $client->ToGetMainData($params);
                $savedId = $result->return->SavedID;
                if(!$savedId){
                    ShowError("Ошибка! ".$result->return->ErrorMessage);
                    //exit();
                }else{
                   // saveToLog("\n===STEP_2====\n".print_r($params,1));
					saveToLog(print_r($params,1),$arUser["XML_ID"],2);
                    echo "<p class='ok'>Ваши данные успешно сохранены. <br>Для продолжения перейдите по <a href='/enrollee/step_3.php'><b>ссылке</b></a></p>";
                    header('Location: /enrollee/step_3.php', true, 301);
                    die();
                }
            }catch (Exception $e){
                ShowError('Выброшено исключение: '.$e->getMessage()."\n");
            }
        }else{
            ShowError("Пользователь не найден");
        }
    }
}?>

