<?
//error_reporting(E_ALL ^ E_NOTICE);
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
$siteUrl = 'https://msal.me';
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $USER;
$ip = $_SERVER['REMOTE_ADDR'];
//отработать-показать условия и текст для внешнего диапазона
if(($ip < '10.0.0.1') || ($ip > '10.255.255.254')){
    $localFl = false;
//ЛВС
}else{
    $localFl = true;
}?>
<?if(!$USER->IsAuthorized()){
    header('Location: /enrollee/step_0.php', true, 301);
    die();
}else{
    $rsUser = CUser::GetByLogin($USER->GetLogin());
    $arUser = $rsUser->Fetch();
    if(!$arUser["XML_ID"]){
        ShowError("У Пользователя не заполнено поле XML_ID");
        //sleep(10);
        //header('Location: /enrollee/step_0.php', true, 301);
        //die();
    }else{
        $client = getSoapClient(MAINDATA_URL);
        if($client){
            $params = array();
            $arAbitCur = getAbitMainFldsToMaindata($arUser["XML_ID"], 5);
            $arAbitNew  = array (
                "ID"            =>  $arUser["XML_ID"],
                "Online"        =>  ($localFl) ? false : true,
                "Name"          =>  trim($arUser["NAME"]),        //Имя
                "Family"        =>  trim($arUser["LAST_NAME"]),   //Фамилию
                "Series2"       =>  trim($_POST["EgeDocSeries"]), //Тип удостоверения личности №2 - серия
                "Number2"       =>  trim($_POST["EgeDocNumber"]), //Тип удостоверения личности №2 - номер
                "DateOfIssue2"  =>  date('d.m.Y',strtotime($_POST["EgeDocDate"]))
            );
            $params["Students"]["Student"] = array_merge ($arAbitCur, $arAbitNew);
            $params["Students"]["Student"]["FamilyMembers"] = getAbitFamilyFldsToMaindata($arUser["XML_ID"], 5);
            $params["Students"]["Student"]["Languages"] = getAbitLangFldsToMaindata($arUser["XML_ID"], 5);
            $params["Students"]["Student"]["EntrantsFiles"] = getAbitFilesToMaindata($arUser["XML_ID"], false, 5);
            $params["Students"]["Student"]["AttachedDocuments"] = getAbitDocsToMaindata($arUser["XML_ID"], false,5);
            $params["Students"]["Student"]["Requests"] = getAbitAppToMaindata($arUser["XML_ID"], 5);
            saveToLog2("*********************************************************************",$arUser["XML_ID"],5);
            $errFlag = false;
            //Русский
            if(isset($_POST["russian"]) && !empty(isset($_POST["russian"])) && intval($_POST["russian"])>0){
                //Русский язык 40-100
                if(intval($_POST["russian"])>=40 && intval($_POST["russian"])<=100){
                    $params["Students"]["Student"]["EgeResults"][] = array(
                        "ID"        =>	"",
                        "Subject"   =>  "Русский",
                        "Mark"      =>  intval($_POST["russian"])
                    );
                }else{
                    $errFlag = true;
                    ShowError("Ошибка! Значение поля 'Русский язык' должно быть от 40 до 100");
                }
            }
            //Обществознание
            if(isset($_POST["socialscience"]) && !empty(isset($_POST["socialscience"])) && intval($_POST["socialscience"])>0){
                //Обществознание 50-100
                if(intval($_POST["socialscience"])>=50 && intval($_POST["socialscience"])<=100){
                    $params["Students"]["Student"]["EgeResults"][] = array(
                        "ID"        =>	"",
                        "Subject"   =>  "Обществознание",
                        "Mark"      =>  intval($_POST["socialscience"])
                    );
                }else{
                    $errFlag = true;
                    ShowError("Ошибка! Значение поля 'Обществознание' должно быть от 50 до 100");
                }
            }
            //История
            if(isset($_POST["history"]) && !empty(isset($_POST["history"])) && intval($_POST["history"])>0){
                //История 36-100
                if(intval($_POST["history"])>=36 && intval($_POST["history"])<=100){
                    $params["Students"]["Student"]["EgeResults"][] = array(
                        "ID"        =>	"",
                        "Subject"   =>  "История",
                        "Mark"      =>  intval($_POST["history"])
                    );
                }else{
                    $errFlag = true;
                    ShowError("Ошибка! Значение поля 'История' должно быть от 36 до 100");
                }
            }
/*
            echo "<pre>";
            //print_r($arUser);
            print_r($params);
            //print_r($_POST);
            echo "</pre>";
*/
            try{
                $result = $client->ToGetMainData($params);
                $savedId = $result->return->SavedID;
                if(!$savedId){
                    ShowError("Ошибка! ".$result->return->ErrorMessage);
                    //exit();
                }else{
                    if(!$errFlag){
                       // saveToLog("\n===STEP_5====\n".print_r($params,1));
						saveToLog(print_r($params,1),$arUser["XML_ID"],5);
                        echo "<p class='ok'>Ваши данные успешно сохранены. <br>Для продолжения перейдите по <a href='/enrollee/step_6.php'><b>ссылке</b></a></p>";
                        header('Location: /enrollee/step_6.php', true, 301);
                        die();
                    }/*else{
                        echo "Error!";
                    }*/
                }
            }catch (Exception $e){
                ShowError('Выброшено исключение: '.$e->getMessage()."\n");
            }


        }else{
            ShowError("Пользователь не найден");
        }
    }
}?>

