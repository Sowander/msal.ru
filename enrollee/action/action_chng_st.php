<?
//error_reporting(E_ALL ^ E_NOTICE);
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
$siteUrl = 'https://msal.me';
$errFlag = false;
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $USER;
$ip = $_SERVER['REMOTE_ADDR'];
//отработать-показать условия и текст для внешнего диапазона
if(($ip < '10.0.0.1') || ($ip > '10.255.255.254')){
    $localFl = false;
//ЛВС
}else{
    $localFl = true;
}?>
<?if(!$USER->IsAuthorized()){
    header('Location: /enrollee/step_0.php', true, 301);
    die();
}else{
    $rsUser = CUser::GetByLogin($USER->GetLogin());
    $arUser = $rsUser->Fetch();
    if(!$arUser["XML_ID"]){
        ShowError("У Пользователя не заполнено поле XML_ID");
        //sleep(10);
        //header('Location: /enrollee/step_0.php', true, 301);
        //die();
    }else{
        $client = getSoapClient(MAINDATA_URL);
        if($client){
            $params = array();
            $arAbitCur = getAbitMainFldsToMaindata($arUser["XML_ID"], 'st');
            if($arAbitCur["RequestStatus"] == "На доработке"){
                $arAbitNew = array(
                    "ID" => $arUser["XML_ID"],
                    "Online" => ($localFl)?false:true,
                    "Name" => trim($arUser["NAME"]),        //Имя
                    "Family" => trim($arUser["LAST_NAME"]),   //Фамилию
                    "RequestStatus" => "На рассмотрении"
                );
                $params["Students"]["Student"] = array_merge ($arAbitCur, $arAbitNew);
                $params["Students"]["Student"]["FamilyMembers"] = getAbitFamilyFldsToMaindata($arUser["XML_ID"], 'st');
                $params["Students"]["Student"]["Languages"] = getAbitLangFldsToMaindata($arUser["XML_ID"], 'st');
                $params["Students"]["Student"]["EntrantsFiles"] = getAbitFilesToMaindata($arUser["XML_ID"], false, 'st');
                $params["Students"]["Student"]["AttachedDocuments"] = getAbitDocsToMaindata($arUser["XML_ID"], false,'st');
                $params["Students"]["Student"]["Requests"] = getAbitAppToMaindata($arUser["XML_ID"], 'st');
            }else{
                $errFlag = true;
            }
            saveToLog2("*********************************************************************",$arUser["XML_ID"],'st');
            if($errFlag){
                ShowError('Вы не можете изменить статус заявки');
            }else{
                try{
                    $result = $client->ToGetMainData($params);
                    $savedId = $result->return->SavedID;
                    if(!$savedId){
                        ShowError("Ошибка! ".$result->return->ErrorMessage);
                    }else{
                        saveToLog(print_r($params, 1), $arUser["XML_ID"], 'st');
                        //echo "<p class='ok'>Ваши данные успешно сохранены. <br>Для продолжения перейдите по <a href='/enrollee/step_6.php'><b>ссылке</b></a></p>";
                        LocalRedirect('/enrollee/info.php');
                        die();
                    }
                }catch(Exception $e){
                    ShowError('Выброшено исключение: '.$e->getMessage()."\n");
                }
            }
        }else{
            ShowError("Пользователь не найден");
        }
    }
}?>

