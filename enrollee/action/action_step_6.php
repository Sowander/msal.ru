<?
session_start();
//error_reporting(E_ALL ^ E_NOTICE);
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
define('PATH_TO_1C_REMOTE_FOLDER', '\\\\1CSERVICES');
$siteUrl = 'https://msal.me';
$REMOTE_FILE_PATH = '\public\pk2018\\';
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
global $USER;
$errFlag = true;
$ip = $_SERVER['REMOTE_ADDR'];
//отработать-показать условия и текст для внешнего диапазона
if(($ip < '10.0.0.1') || ($ip > '10.255.255.254')){
    $localFl = false;
//ЛВС
}else{
    $localFl = true;
}

if($ip=='91.229.128.218'){
    $localFl = true;
}

?>
<?if(!$USER->IsAuthorized()){
    header('Location: /enrollee/step_0.php', true, 301);
    die();
}else{
    $rsUser = CUser::GetByLogin($USER->GetLogin());
    $arUser = $rsUser->Fetch();
    if(!$arUser["XML_ID"]){
        ShowError("У Пользователя не заполнено поле XML_ID");
        //sleep(10);
        //header('Location: /enrollee/step_0.php', true, 301);
        //die();
    }else{
        $client = getSoapClient(MAINDATA_URL);
        if($client){
            $params = array();
            $arAbitCur = getAbitMainFldsToMaindata($arUser["XML_ID"], 6);
            $arAbitNew  = array (
                "ID"            =>  $arUser["XML_ID"],
                "Online"        =>  ($localFl) ? false : true,
                "Name"          =>  trim($arUser["NAME"]),        //Имя
                "Family"        =>  trim($arUser["LAST_NAME"]),   //Фамилию
            );
            $params["Students"]["Student"] = array_merge ($arAbitCur, $arAbitNew);
            $params["Students"]["Student"]["FamilyMembers"] = getAbitFamilyFldsToMaindata($arUser["XML_ID"], 6);
            $params["Students"]["Student"]["Languages"] = getAbitLangFldsToMaindata($arUser["XML_ID"], 6);
            $params["Students"]["Student"]["EgeResults"] = getAbitEgeFldsToMaindata($arUser["XML_ID"], 6);
            $params["Students"]["Student"]["EntrantsFiles"] = getAbitFilesToMaindata($arUser["XML_ID"], false,6);
            $params["Students"]["Student"]["AttachedDocuments"] = getAbitDocsToMaindata($arUser["XML_ID"], false, 6);
            $bufApps = getAbitAppToMaindata($arUser["XML_ID"], 6);
            saveToLog2("*********************************************************************",$arUser["XML_ID"],6);


            $params["Students"]["Student"]["Requests"] = $bufApps;
            $mainAppFl = false; //выбрал ли пользователь приоритетное заявление
            $mainApp1CFl = false; //отмечено ли приоритетное заявление в 1С
            $isNewApps = false; //новое заявление
            $mainAppCheckFl = false; //омечено ли приоритетное заявление для отправки в 1С
            foreach($params["Students"]["Student"]["Requests"] as $arItem){
                if($arItem["Priority"]){
                    $mainApp1CFl = true;
                    break;
                }
            }
            $arModifyApp = array();
            //проверка на 3 заявления на специальность
            $arSelGUIDKG = array();
            $arSelSpecCount = array();
            $arCompGr = array();
            $arCompGrGUID = array();
            foreach($bufApps as $key => $arItem){
                if(!in_array($arItem["GUIDKG"], $arSelGUIDKG)){
                    $arSelGUIDKG[] = $arItem["GUIDKG"];
                }
                //если статус «На доработке» или статус отличный от «Принята» и пустая ссылка на файл
                if($arItem["Status"] == "На доработке" || ($arItem["Status"] != "Принята" && $arItem["RequestPath"] == '')){
                    $arModifyApp[$arItem["GUIDKG"]] = $key;
                }
            }
            $groups = getSoapClient(WSCOMPGROUPS_URL);
            if($groups){
                try{
                    $date = '2018-03-28';
                    $rez = $groups->ПолучитьСписокКонкурсныхГрупп(array("ДатаНачала"=>$date));
                    $arCompGr = json_decode(json_encode($rez->return->КонкурснаяГруппа), TRUE);
                    foreach($arCompGr as $arItem){
                        $arCompGrGUID[$arItem["GUID"]] = $arItem;
                    }
                }catch (Exception $e){
                    ShowError('Выброшено исключение. Метод "ПолучитьСписокКонкурсныхГрупп". '.$e->getMessage()."\n");
                }
            }
            foreach($arSelGUIDKG as $GUIDKG){
                $arSelSpecCount[$arCompGrGUID[$GUIDKG]["GUIDНаправления"]][] = '+';
            }

/*
            if($arUser["XML_ID"] == 3177){
                echo "<pre>";
                print_r($bufApps);
                print_r($arModifyApp);
                print_r($_FILES);
                print_r($_POST);
                print_r($params["Students"]["Student"]["Requests"]);
                echo "</pre>";
                die();
            }
*/

            // # todo В случаи если заявления были добавлены ранее, и абитуриент пытается добавить их, то портал должен добавлять новые заявления, а по принятым игнорировать.
            //отработать-показать условия и текст для внешнего диапазона
            if(!$localFl){
                $remotePath = $arUser["XML_ID"].'\\'; //папка с названием ИД пользователя
                $arrFiles = array();
                foreach($_FILES["applctn"] as $k=>$v){
                    foreach($v as $key=>$val){
                        $arrFiles[$key][$k] = $val;
                    }
                }
                foreach($arrFiles as $k=> $arrFile){
                    if($_POST["app"][$k]["guid"] == $_POST["app_main"]){
                        $mainAppFl = true;
                        break;
                    }
                }

/*
                if($arUser["XML_ID"] == 1988){
                    echo "<pre>";
                    print_r($arrFiles);
                    echo "</pre>";
                    //die();
                }
*/

                if($mainAppFl || $mainApp1CFl){
                    foreach($arrFiles as $k => $arrFile){
                        //если выбрано заявления для участия и оно новое, то прикрепляем
                        if($_POST["app"][$k]["take"] && ($_POST["app"][$k]["status"] == "N" || $_POST["app"][$k]["status"] == "Y")){

                            //проверка на 3 завяления на 1 специальность
                            //if(count($arSelSpecCount[$arCompGrGUID[trim($_POST["app"][$k]["guid"])]["GUIDНаправления"]]) <= 3){
                                $arSelSpecCount[trim($_POST["app"][$k]["guid"])][] = '+';
                                $ext = substr(strrchr($arrFile["name"], '.'), 1);
                                if(strlen($ext) > 0){
                                    $ext = ".".$ext;
                                }
                                $filename = $arrFile["name"];
                                $p = strrpos($filename, '.');
                                if ($p > 0) $filename = substr($filename, 0, $p);
                                if((strlen($arrFile["name"]) > 0 || strlen($arrFile["del"]) > 0) && $arrFile["error"] == 0 && $arrFile["size"] > 0){
                                    $ftp1C = new Ftp1CDev();
                                    //timestamp
                                    //$nameFile = trim($_POST["app"][$k]["guid"]).time().$arrFile["name"];
                                    $nameFile = trim($_POST["app"][$k]["guid"]).'_'.Cutil::translit($filename,"ru",array()).'_'.date('dmHis').$ext;
                                    $filePath1C = $ftp1C->filePut($arrFile["tmp_name"], $remotePath, $nameFile/*$arrFile['identification-card']["name"]*/);
                                    //echo $filePath1C;
                                    $fileDir = "";
                                    if($filePath1C){
                                        $FilePathPublic = PATH_TO_1C_REMOTE_FOLDER.$filePath1C;
                                        if(empty($fileDir)){
                                            $fileDir = str_replace($_FILES['identification-card']["name"], "", $filePath1C);
                                        }
                                        $subDir = '';
                                        $fileName = '';
                                        //вырезаем из строки $REMOTE_FILE_PATH
                                        $filePath = str_replace($REMOTE_FILE_PATH, "", $filePath1C);
                                        //старое заявление На доработке
                                        if($_POST["app"][$k]["status"] == "Y"){
                                            echo $appID = $arModifyApp[trim($_POST["app"][$k]["guid"])];
                                            //обновляем файл, дату и статус
                                            if(!empty($params["Students"]["Student"]["Requests"][$appID])){
                                                if($filePath){
                                                    $params["Students"]["Student"]["Requests"][$appID]["RequestPath"] = $filePath;
                                                    $params["Students"]["Student"]["Requests"][$appID]["DateChange"] = date('Y-m-d');
                                                    $params["Students"]["Student"]["Requests"][$appID]["Status"] = "";
                                                    $params["Students"]["Student"]["Requests"][$appID]["Priority"] = (!$mainApp1CFl && !$mainAppCheckFl && ($_POST["app"][$k]["guid"] == $_POST["app_main"])) ? true : false;
                                                }
                                            }
                                            /*
                                            echo "<pre>";
                                            echo trim($_POST["app"][$k]["guid"])."<br>";
                                            echo $appID."<br>";
                                            print_r($params["Students"]["Student"]["Requests"][$appID]);
                                            echo "</pre>";
                                            */
                                        //Новое заявление
                                        }else{
                                            $isNewApps = true;
                                            $params["Students"]["Student"]["Requests"][] = array(
                                                "ID" => "",
                                                "GUIDKG" => trim($_POST["app"][$k]["guid"]),
                                                "Level" => ""/*trim($_POST["app"][$k]["name"])*/,
                                                "Specialty" => trim($_POST["app"][$k]["guid_spec"]),
                                                "GUIDProfile" => trim($_POST["app"][$k]["profile"]),
                                                "Request" => "",
                                                "RequestPath" => $filePath,
                                                "Priority" => (!$mainApp1CFl && !$mainAppCheckFl && ($_POST["app"][$k]["guid"] == $_POST["app_main"]))?true:false,
                                                "DateChange" => date('Y-m-d'),
                                                "Status" => ""
                                            );
                                        }
                                        //если
                                        if(!$mainApp1CFl && !$mainAppCheckFl && ($_POST["app"][$k]["guid"] == $_POST["app_main"])){
                                            $mainAppCheckFl = true;
                                        }
                                        /*
                                        //удаление файла
                                        $arrPath = explode("\\", $filePath);
                                        if(strlen($arrPath[0])>0 && strlen($arrPath[1])>0){
                                            $subDir = $arrPath[0]."\\";
                                            $fileName = $arrPath[1];
                                        }
                                        $filePath1C = $ftp1C->fileDelete($subDir,$fileName);
                                        */
                                    }else{
                                        $errFlag = false;
                                        ShowError("Не удалось загрузить заявление для КГ - ".$_POST["app"][$k]["name"]);
                                        $FilePathPublic = "";
                                    }
                                }
                            /*
                            }else{
                                $errFlag = false;
                                ShowError("Вы не можете учавстовать в КГ '".$_POST["app"][$k]["name"]."'. Вы уже подали 3 заявления на данную специальность");
                            }
                            */
                        }
                    }
                }else{
                    $errFlag = false;
                    ShowError("Не выбрано приоритетное заявление");
                }


            //отработать-показать условия и текст для ЛВС
            }else{
                foreach($_POST["app"] as $arItem){
                    if($arItem["take"] && ($arItem["status"] == "N" || $arItem["status"] == "Y")){
                        //проверка на 3 завяления на 1 специальность
                        //if(count($arSelSpecCount[$arCompGrGUID[$arItem["guid"]]["GUIDНаправления"]]) <= 3){
                            $arSelSpecCount[$arItem["guid"]][] = '+';
                            //старое заявление
                            if($arItem["status"] == "Y"){
                                $appID = $arModifyApp[trim($arItem["guid"])];
                                //обновляем дату и статус
                                if(!empty($params["Students"]["Student"]["Requests"][$appID])){
                                    $params["Students"]["Student"]["Requests"][$appID]["DateChange"] = date('Y-m-d');
                                    $params["Students"]["Student"]["Requests"][$appID]["Status"] = "";
                                    $params["Students"]["Student"]["Requests"][$appID]["Priority"] = (!$mainApp1CFl && !$mainAppCheckFl && ($arItem["guid"] == $_POST["app_main"])) ? true : false;
                                }
                            //Новое заявление
                            }else{
                                $isNewApps = true;
                                $params["Students"]["Student"]["Requests"][] = array(
                                    "ID" => "",
                                    "GUIDKG" => trim($arItem["guid"]),
                                    "Level" => ""/*trim($_POST["app"][$k]["name"])*/,
                                    "Specialty" => trim($arItem["guid_spec"]),
                                    "GUIDProfile" => trim($arItem["profile"]),
                                    "Request" => "",
                                    "RequestPath" => "",
                                    "Priority" => (!$mainApp1CFl && !$mainAppCheckFl && ($arItem["guid"] == $_POST["app_main"]))?true:false,
                                    "DateChange" => date('Y-m-d'),
                                    "Status" => ""
                                );
                                if(!$mainApp1CFl && !$mainAppCheckFl && ($arItem["guid"] == $_POST["app_main"])){
                                    $mainAppCheckFl = true;
                                }
                            }
                        /*
                        }else{
                            $errFlag = false;
                            ShowError("Вы не можете учавстовать в КГ '".$arItem["name"]."'. Вы уже подали 3 заявления на данную специальность");
                        }
                        */
                    }
                }
                /*
                foreach($_POST["app"] as $arItem){
                    $params["Students"]["Student"]["Requests"][] = array(
                        "ID" => "",
                        "Level" => trim($arItem["name"]),
                        "Specialty" => trim($arItem["profile"]),
                        "Request" => "",
                        "RequestPath" => "",
                        "Priority" => "",
                    );
                }
                */
            }
            //передавать в 1С статусы заявлений. НО Статус “На доработке” заменять на пустой, чтобы можно было модератору его рассматривать (Остальные статусы передавать как есть).
            foreach($params["Students"]["Student"]["Requests"] as &$arItem){
                if($arItem["Status"] == "На доработке"){
                    $arItem["Status"] = '';
                }
            }

            //если текущий статус = «Принята» и добавлены новые заЯвления, необходимо общий статус менять на «Частично принята»"
            if($params["Students"]["Student"]["RequestStatus"] == "Принята" && $isNewApps){
                $params["Students"]["Student"]["RequestStatus"] = "Частично принята";
            }


            //die();

/*
            if($arUser["XML_ID"] ==  10647){
                echo "<pre>";
                //print_r($arrFiles);
                print_r($_FILES);
                print_r($_POST);
                //print_r($arUser);
                print_r($params);
                echo "</pre>";
                die();
            }
*/


            //$params["Students"]["Student"]["Requests"] = array();
            try{
                $result = $client->ToGetMainData($params);
                $savedId = $result->return->SavedID;
                if(!$savedId){
                    ShowError("Ошибка! ".$result->return->ErrorMessage);
                    //exit();
                }else{
                    if($errFlag){
                        //session_destroy();
						saveToLog(print_r($params,1),$arUser["XML_ID"],6);
                        unset($_SESSION['enrollee']);
                        //echo "<p class='ok'>Спасибо! Заявления отправлены на проверку модератору.<br>Вы можете перейти к просмотру страницы “Информационная панель абитуриента”.</p>";
                        //echo "<p class='ok'><a href='/enrollee/step_7.php'><b>Далее</b></a></p>";
                        //header('Location: /enrollee/step_6+.php?msg=ok#top', true, 301);
                        LocalRedirect("/enrollee/step_6.php?msg=ok#top");
                        die();
                    }
                }
            }catch (Exception $e){
                ShowError('Выброшено исключение: '.$e->getMessage()."\n");
            }

        }else{
            ShowError("Пользователь не найден");
        }
    }
}?>

