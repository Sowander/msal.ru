<?
global $USER;
//error_reporting(E_ALL ^ E_NOTICE);
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true); 
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php"); 
session_start();
CModule::IncludeModule("iblock");
$ip = $_SERVER['REMOTE_ADDR'];
//отработать-показать условия и текст для внешнего диапазона
if(($ip < '10.0.0.1') || ($ip > '10.255.255.254')){
    $localFl = false;
//ЛВС
}else{
    $localFl = true;
}?>
<?
//провера существования юзера BX
$PSERIES = (str_replace(' ','',trim($_POST[passportseries])));//серия паспорта
$PNUMBER = (str_replace(' ','',trim($_POST[passportnumber])));//номер паспорта
/*
$passwd = (trim($_POST[email]));
$passwd = ''.(trim($_POST[passportseries])+0).''.(trim($_POST[passportnumber])+0).'';
*/
$passwd = $PSERIES.$PNUMBER;
$login = trim($_POST[email]);
$rsUser = CUser::GetByLogin($login);
$arUser = $rsUser->Fetch();
if($arUser[ID]){
	ShowError("Пользователь с такие адресом электронной почты уже зарегистрирован");
	//echo "Пользователь успешно добавлен. Логин и пароль отправлены на адрес электронной почты. Для продолжения перейдите по <a href='form.php'>ссылке</a>";
	//exit();
}else{

    //фильтр на существование серии и номера поспорта
    $filter = Array(
        "UF_PSERIES"    => $PSERIES,
        "UF_PNUMBER"    => $PNUMBER,
    );
    $rsPUsers = CUser::GetList(($by = "NAME"), ($order = "desc"), $filter);
    if($arPUser = $rsPUsers->Fetch()){
        ShowError("Пользователь с такими номером и серией паспорта уже зарегистрирован");
        //echo "Пользователь успешно добавлен. Логин и пароль отправлены на адрес электронной почты. Для продолжения перейдите по <a href='form.php'>ссылке</a>";
        //exit();
    }else{
        //шлем инфу в 1С
        //$soapUrl = 'http://1cservices/priem_2017_test/ws/maindata.1cws?wsdl';
        if(!function_exists('is_soap_fault')){
            print 'Не настроен web сервер. Не найден модуль php-soap.';
            return false;
        }
        ini_set('soap.wsdl_cache_enabled', 0);
        ini_set('soap.wsdl_cache_ttl', 0);
        try{
            $client = new SoapClient(MAINDATA_URL,
                array(
                    "login" => "Avrobus",
                    "password" => "avrobus",
                    'soap_version' => SOAP_1_2,
                    'trace' => true,
                    'exceptions' => true,
                    'cache_wsdl' => WSDL_CACHE_NONE
                )
            );
        }catch(SoapFault $e){
            trigger_error('Ошибка подключения или внутренняя ошибка сервера. Не удалось связаться с базой 1С.', E_ERROR);
            echo "<pre>".print_r($e, 1)."</pre>";
        }
        if(is_soap_fault($client)){
            trigger_error('Ошибка подключения или внутренняя ошибка сервера. Не удалось связаться с базой 1С.', E_ERROR);
            return false;
        }

        $params[Students][Student][0] = array(
            "ID" => "",
            "Online" =>  ($localFl) ? false : true,
            "Name" => trim($_POST[firstname]),
            "Family" => trim($_POST[lastname]),
            'Surname' => trim($_POST[patronymic]),
            'DateOfBirth' => date('Y-m-d', strtotime($_POST[birthdate])),
            'DocType' => trim($_POST[inddocuments_other]),
            'Series' => $PSERIES,
            'Number' => $PNUMBER,
            'Email' => $login,
        );



        try{
            $result = $client->ToGetMainData($params);
            $savedId = intval($result->return->SavedID);
            $status = $result->return->RequestStatus;

            $err = '';
            //var_dump($status);
            if(!$savedId){
                if($savedId == 0){
                    ShowError("Ошибка! Отправьте данные еще раз");
                }else{
                    ShowError("Ошибка! ".$result->return->ErrorMessage);
                }
                //exit();
            }else{
                if($result->return->ErrorMessage){
                    $err = $result->return->ErrorMessage;
                    ShowError("Ошибка! ".str_replace("Main Data failed", "Учетная запись с такой фамилией и такими паспортными данными уже зарегистрирована. Если Вы выпускник МГЮа, войдите с учетной записью МГЮА (s+номерстуденческого, например s0011223)", $result->return->ErrorMessage));
                }else{
                    //регисрируем
                    $user = new CUser;
                    $arFields = Array(
                        "NAME" => trim($_POST[firstname]),
                        "SECOND_NAME" => trim($_POST["patronymic"]),
                        "LAST_NAME" => trim($_POST[lastname]),
                        "EMAIL" => $login,
                        "LOGIN" => $login,
                        "PERSONAL_BIRTHDAY" => $_POST[birthdate],
                        "LID" => "ru",
                        "ACTIVE" => "Y",
                        "PASSWORD" => $passwd,
                        "CONFIRM_PASSWORD" => $passwd,
                        "GROUP_ID" => array(3, 4, 23),
                        "XML_ID" => $savedId,
                        "UF_PSERIES" => $PSERIES,//серия паспорта
                        "UF_PNUMBER" => $PNUMBER,//номер паспорта
                    );
                    $ID = $user->Add($arFields);
                    if($ID){
                        $USER->Authorize($ID);
                        echo "Пользователь успешно добавлен. Логин и пароль отправлены на адрес электронной почты. <br>Для продолжения перейдите по <a href='/enrollee/step_1.php'>ссылке</a>";
                        CEvent::Send("NEW_ENROLLEE", 's1', array("EMAIL" => $login, "PASSWORD" => $passwd, "FIO" => $_POST["lastname"]." ".$_POST["firstname"]." ".$_POST["patronymic"], "ID" => $savedId));
                        //header('Location: /enrollee/step_1.php', true, 301);
                        //die();
                    }else{
                        $err = $user->LAST_ERROR;
                        ShowError($user->LAST_ERROR);
                    }
                }
            }
            //логирование если пришло айди=1
            if($savedId == 1){
                $err .= "\n Этому юзеру присвоен XML_ID=1 \n";
                saveToLog3(print_r($result,1), intval($savedId));
            }
            saveToLog(print_r($params,1).'\n'.$err,$arUser["XML_ID"],0);
        }catch (Exception $e){
            ShowError('Выброшено исключение: '.$e->getMessage()."\n");
        }

        /*
        $result = $client->ToGetMainData($params);
        $savedId = $result->return->SavedID;
        if(!$savedId){
            ShowError("Ошибка! ".$result->return->ErrorMessage);
            //exit();
        }else{
            //регисрируем
            $user = new CUser;
            $arFields = Array(
                "NAME" => $_POST[firstname],
                "SECOND_NAME" => $_POST["patronymic"],
                "LAST_NAME" => $_POST[lastname],
                "EMAIL" => $login,
                "LOGIN" => $login,
                "PERSONAL_BIRTHDAY" => $_POST[birthdate],
                "LID" => "ru",
                "ACTIVE" => "Y",
                "PASSWORD" => $passwd,
                "CONFIRM_PASSWORD" => $passwd,
                "GROUP_ID" => array(3, 4, 23),
                "XML_ID" => $savedId,
                "UF_PSERIES" => $PSERIES,//серия паспорта
                "UF_PNUMBER" => $PNUMBER,//номер паспорта
            );
            $ID = $user->Add($arFields);
            if($ID){
                $USER->Authorize($ID);
                echo "Пользователь успешно добавлен. Логин и пароль отправлены на адрес электронной почты. <br>Для продолжения перейдите по <a href='/enrollee/step_1.php'>ссылке</a>";
                CEvent::Send("NEW_ENROLLEE", 's1', array("EMAIL" => $login, "PASSWORD" => $passwd));
                //header('Location: /enrollee/step_1.php', true, 301);
                //die();
            }else{
                ShowError($user->LAST_ERROR);
            }
        }
        */
    }
}?>