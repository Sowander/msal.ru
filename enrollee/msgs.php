<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Информационная панель абитуриента");?>
<?global $USER;?>
<?
$groupID = 1; //группа пользователей которая может просматривать струницу
?>
<div class="msf-header">
    <div class="row msf-header__top">
        <?if($USER->IsAuthorized()){?>
            <div class="msf-header__column"><a href="/enrollee/?logout=yes">Выход</a></div>
        <?}?>
    </div>
</div>
<div class="msf-content">
    <div class="msf-view" style="display: block">
        <div class="row">
            <div class="col-md-12">
                <h3>МГЮА Отправленные сообщения ПК</h3>
                <?$arResultMsgs = array();
                if(!in_array($groupID, $USER->GetUserGroupArray())){
                    ShowError("У вас не достаточно прав для просмотра данной страницы");
                    die();
                    ?>
                <?}else{
                    $client = getSoapClient(WSABITURS_URL);
                    if($client){
                        if($_POST["submit"] == "Y"){
                            if((intval($_POST["id"]) > 0 || strlen($_POST["email"])) > 0){
                                try{
                                    $params2 = array();
                                    if(intval($_POST["id"]) > 0){
                                        $params2["ID"] = intval($_POST["id"]);
                                    }
                                    if(strlen($_POST["email"]) > 0){
                                        $params2["EMail"] = $_POST["email"];
                                    }
                                    if(!empty($params2)){
                                        $result = $client->ПолучитьСообщенияАбитуриенту($params2);
                                        $arBufMsgs = json_decode(json_encode($result->return->Состав), TRUE);
                                        if(is_array($arBufMsgs[0])){
                                            $arResultMsgs = $arBufMsgs;
                                        }elseif(is_array($arBufMsgs)){
                                            $arResultMsgs[0] = $arBufMsgs;
                                        }else{
                                            $arResultMsgs = array();
                                        }
                                    }
                                }catch(Exception $e){
                                    ShowError('Выброшено исключение. Метод "ПолучитьСообщенияАбитуриенту". '.$e->getMessage()."\n");
                                }
                            }else{
                                ShowError('Введите ID или E-mail абитуриента');
                            }
                        }
                    }
                }?>
            </div>
            <div class="col-md-12">
                <div class="col-md-12">
                    <form class="form-horizontal" method="post">
                        <div class="form-group">
                            <label>ID</label><br>
                            <input class="form-control" type="text" name="id" value="<?=$_POST['id']?>">
                        </div>
                        <div class="form-group">
                            <label>или E-mail</label>
                            <input class="form-control" type="text" name="email" value="<?=$_POST['email']?>">
                        </div>
                        <div class="form-group">
                            <button class="btn btn-primary" name="submit" type="submit" value="Y">Найти</button>
                        </div>
                    </form>
                    <br>
                </div>
                <div class="row">
                    <?if(count($arResultMsgs)>0){?>
                        <?foreach($arResultMsgs as $arItem){?>
                            <table>
                                <tr>
                                    <th width="100">Адресат</th>
                                    <td><?=$arItem["Адресат"]?></td>
                                </tr>
                                <tr>
                                    <th width="100">Дата Отправки</th>
                                    <td><?=$arItem["ДатаОтправки"]?></td>
                                </tr>
                                <tr>
                                    <th width="100">Тема</th>
                                    <td><?=$arItem["Тема"]?></td>
                                </tr>
                                <tr>
                                    <th width="100">Сообщение</th>
                                    <td><?=nl2br(TruncateText($arItem["ТелоПисьма"], 1500))?></td>
                                </tr>
                            </table>
                            <br>
                        <?}?>
                    <?}else{?>
                        <?if($_POST["submit"]=="Y"){?>
                            <br><p>По Вашему запросу ничего не найдено!</p>
                        <?}?>
                    <?}?>
                </div>
            </div>
        </div>
    </div>
</div>
<style>
    table{
        width: 100%;
        border-collapse: collapse;
        border: 1px solid black;
    }
    table td,
    table th{
        padding: 5px;
        border: 1px solid black;
    }
</style>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>