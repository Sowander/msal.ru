<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Авторизация абитуриента на сайте");?>
<?
//error_reporting(E_ALL ^ E_NOTICE);
define("NO_KEEP_STATISTIC", true);
define("NOT_CHECK_PERMISSIONS",true);
require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
session_start();
CModule::IncludeModule("iblock");
global $USER;
$ip = $_SERVER['REMOTE_ADDR'];
//отработать-показать условия и текст для внешнего диапазона
if (($ip < '10.0.0.1') || ($ip > '10.255.255.254')){
    $localFl = false;
//ЛВС
}else{
    $localFl = true;
}
//дата окончания регистрации
$dateClose = "21.07.2018";
if($USER->isAuthorized()){
    header('Location: /enrollee/step_1.php');
}else{?>
    <form class="form-registration" name="form_auth" method="post" target="_top" action="">
        <div class="msf-formreg">
            <div class="row">
                <div class="col-md-12">
                    <h3>Пожалуйста, авторизуйтесь</h3>
                    <br>
					<p style="font-size:13px;color:maroon"><b>Уважаемые абитуриенты, регистрация личных кабинетов для удаленной подачи документов окончена 21/07/2018 года в 23:59:59<br />
				Документы на направления бакалавриата/специалитета можно будет подать очно до 17:00 26 июля включительно, на направления магистратуры - до 17:00 31 июля включительно.</b></p>
                    <p><b>Уважаемые абитуриенты, для уверенной работы с личным кабинетом и отправки файлов рекомендуется использовать браузер Google Chrome и проводной интернет, не менее 1Мб/сек.</b></p>
                    <p><b>Передача данных через личный кабинет с использованием мобильной сети может быть нарушена низким качеством или нестабильной связью с точкой доступа Вашего провайдера.</b></p>
					<p><a target="_blank" href="https://msal.ru/upload/struktura/cpk2018/pkdoc/%D0%98%D0%BD%D1%81%D1%82%D1%80%D1%83%D0%BA%D1%86%D0%B8%D0%B8%20%D0%BF%D0%BE%20%D0%BF%D0%BE%D0%B4%D0%B3%D0%BE%D1%82%D0%BE%D0%B2%D0%BA%D0%B5%20%D0%B8%20%D0%BE%D1%82%D0%BF%D1%80%D0%B0%D0%B2%D0%BA%D0%B5%20%D0%B4%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%BE%D0%B2%20%D0%B4%D0%BB%D1%8F%20%D0%BF%D0%BE%D0%B4%D0%B0%D1%87%D0%B8%20%D0%B7%D0%B0%D1%8F%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D1%8F%20%D1%87%D0%B5%D1%80%D0%B5%D0%B7%20%D0%BB%D0%B8%D1%87%D0%BD%D1%8B%D0%B9%20%D0%BA%D0%B0%D0%B1%D0%B8%D0%BD%D0%B5%D1%82%20%D0%B0%D0%B1%D0%B8%D1%82%D1%83%D1%80%D0%B8%D0%B5%D0%BD%D1%82%D0%B0.pdf">Инструкции по подготовке и отправке документов для подачи заявления через личный кабинет абитуриента</a> (открыть в новой вкладке)</p>
					<p>Логин для выпускников МГЮА - <b>буква s + номер студенческого</b> (пример <b>s0011223</b>, пароль - как от учетной записи)</p>
                    <br>
                </div>
                <div class="col-md-12">
                    <?if($_GET["err"]){
                        ShowError(urldecode($_GET["err"]));
                    };
                    if($_REQUEST[USER_LOGIN] and $_REQUEST[USER_PASSWORD]){
                        if (!is_object($USER)) $USER = new CUser;
                        $arAuthResult = $USER->Login($_REQUEST[USER_LOGIN], $_REQUEST[USER_PASSWORD]); //, "Y"
                        $APPLICATION->arAuthResult = $arAuthResult;
                        if($arAuthResult[MESSAGE]){
                            ShowError($arAuthResult[MESSAGE]);
                            //exit();
                        }else{
                            print_r($arAuthResult);
                            //localRedirect('./step_1.php');
                        }
                    }
                    if($USER->isAuthorized()){
                        header('Location: /enrollee/step_1.php');
                    }
                    ?>
                    <div class="form-group">
                        <label>Логин</label><br>
                        <input class="form-control" type="text" name="USER_LOGIN" maxlength="255" placeholder="Логин" value="" required>
                    </div>
                    <div class="form-group">
                        <label>Пароль</label><br>
                        <input class="form-control" type="password" name="USER_PASSWORD" maxlength="255" autocomplete="off" placeholder="Пароль"  required>
                    </div>
                    <div class="form-group">
                        <input type="submit" name="Login" value="Войти" class="btn btn-primary">
                    </div>

                    <?//c 22 июля регистрация закрыта
                    $result = $DB->CompareDates($dateClose, date("d.m.Y"));
                    if(($result == -1 && $localFl) || $result != -1){?>
                        <a href="/enrollee/step_0.php">Регистрация</a>
                    <?}?>
                </div>
            </div>
        </div>
    </form>
<?}?>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>
