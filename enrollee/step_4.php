<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Предоставленные документы");?>
<?global $USER;?>
<?//error_reporting(E_ALL);?>
<?
session_start();
$ip = $_SERVER['REMOTE_ADDR'];
//отработать-показать условия и текст для внешнего диапазона
if (($ip < '10.0.0.1') || ($ip > '10.255.255.254')){
    $localFl = false;
//ЛВС
}else{
    $localFl = true;
}
?>
<?
$arErrors = array();
$arResult = array();
$arResultAb = array();
$arResultDoc = array();
$arTypesDocs = array();
$arLvlvEdu = array();
$arrDocs = array();
$arResultAbFiles = array();
if(!$USER->IsAuthorized()){
    //переадресация на авторизацию
    header('Location: https://msal.me/enrollee/step_0.php');
    //exit();
}else{
    $rsUser = CUser::GetByLogin($USER->GetLogin());
    $arUser = $rsUser->Fetch();
    //echo "<pre>"; print_r($arUser); echo "</pre>";
    if(!$arUser["XML_ID"]){
        $arErrors[] = "У Пользователя не заполнено поле XML_ID";
        //ShowError("У Пользователя не заполнено поле XML_ID");
        $USER->Logout();
        $err = urlencode("Ошибка получения данных (XML_ID) для ".$arUser["LOGIN"].", обратитесь к администратору");
        header('Location: /enrollee/?err='.$err, true, 301);
        die();
    }else{
        $client = getSoapClient(WSABITURS_URL);
        if($client){
            $params = array("ID" => $arUser["XML_ID"]);
            try{
                $result = $client->ПолучитьСписокАбитуриентов($params);
                $arRslt = json_decode(json_encode($result->return->Состав), TRUE);
                $arResultAb = json_decode(json_encode($result->return->Состав->Абитуриент), TRUE);
                //проверка на пустой ответ из 1С
                isEmptyArr1C($arResultAb, 4, true);
                /*
                echo "<pre>";
                print_r($arResultAb);
                echo "</pre>";
                */
                $arResultBufDoc = json_decode(json_encode($result->return->Состав->ПоданныеДокументы), TRUE);
                if(is_array($arResultBufDoc[0])){
                    $arResultDoc = $arResultBufDoc;
                }elseif(is_array($arResultBufDoc)){
                    $arResultDoc[0] = $arResultBufDoc;
                }else{
                    $arResultDoc = array();
                }
                $arResultBufFiles = json_decode(json_encode($result->return->Состав->ФайлыАбитуриента), TRUE);
                if(is_array($arResultBufFiles[0])){
                    $arResultAbFiles = $arResultBufFiles;
                }elseif(is_array($arResultBufFiles)){
                    $arResultAbFiles[0] = $arResultBufFiles;
                }else{
                    $arResultAbFiles = array();
                }
            }catch (Exception $e){
                $arErrors[] = 'Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n";
                //ShowError('Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n");
            }
            $groups = getSoapClient(WSCOMPGROUPS_URL);
            if($groups){
                try{
                    $rez = $groups->ПолучитьСписокТиповДокументов();
                    $arTypesDocs = json_decode(json_encode($rez->return->ТипДокумента), TRUE);
                }catch (Exception $e){
                    $arErrors[] = 'Выброшено исключение. Метод "ПолучитьСписокТиповДокументов". '.$e->getMessage()."\n";
                    //ShowError('Выброшено исключение. Метод "ПолучитьСписокТиповДокументов". '.$e->getMessage()."\n");
                }
                try{
                    $rez = $groups->ПолучитьСписокУровнейОбразования();
                    $arLvlvEdu = json_decode(json_encode($rez->return->УровеньОбразования), TRUE);
                }catch (Exception $e){
                    $arErrors[] = 'Выброшено исключение. Метод "ПолучитьСписокУровнейОбразования". '.$e->getMessage()."\n";
                    //ShowError('Выброшено исключение. Метод "ПолучитьСписокУровнейОбразования". '.$e->getMessage()."\n");
                }
            }
            foreach($arTypesDocs as $arType){
                if(strlen($arType["Категория"])>0){
                    $arrDocs[$arType["Категория"]][] = $arType;
                }else{
                    $arrDocs["Без категории"][]= $arType;
                }
            }

            if($_POST["submit"] == "Y"){
                $arData = array();
                $arData[] = array("NAME_1C" => "ПаспортВыдан", "NAME_POST" => "doctwho");
                $arData[] = array("NAME_1C" => "ПаспортКодПодразделения", "NAME_POST" => "doctcode");

                //$arData[] = array("NAME_1C" => "", "NAME_POST" => "document-education");
                //$arData[] = array("NAME_1C" => "", "NAME_POST" => "serieseducationdoc");
                //$arData[] = array("NAME_1C" => "", "NAME_POST" => "numbereducationdoc");
                //$arData[] = array("NAME_1C" => "", "NAME_POST" => "dateeducationdoc");

                foreach($arData as $arField){
                    checkPostField($arField["NAME_1C"], $arField["NAME_POST"], $_POST, $arResultAb);
                }
                if(isset($_POST["doctdate"]) && !empty($_POST["doctdate"])){
                    $arResultAb["ПаспортДата"] = date('Y-m-d', strtotime($_POST["doctdate"]));
                }
                foreach($_POST["doc"] as $key => $val){
                    $arResult["doc"][$key]["ВидДокумента"] = $val["otherdocuments-sel"];
                    $arResult["doc"][$key]["Серия"] = $val["otherdoc-seriya"];
                    $arResult["doc"][$key]["Номер"] = $val["otherdoc-number"];
                    $arResult["doc"][$key]["Дата"] = date('Y-m-d', strtotime($val["otherdoc-date"]));
                }
            }else{
                $arResult["Docs"] = $arResultDoc;
            }
        }
    }
}?>
<?if($arResultAb["СтатусЗаявки"] == "На доработке"){?>
    <form id="formstatus" method="post" action="">
        <input type="hidden" name="changeSt" value="Y">
    </form>
<?}?>
<form class="form-horizontal msf js-form-address<?if($arResultAb["СтатусЗаявки"]=="Принята"){?> is-disabled<?}?>" id="formmsf4" method="post" action="" enctype="multipart/form-data">
    <div class="msf-header">
        <div class="row msf-header__top">
            <div class="msf-header__column"><div class="msf-header__status">Статус: <span id="status"><?=$arResultAb["СтатусЗаявки"]?></span></div></div>
            <?if($arResultAb["СтатусЗаявки"]=="На доработке"){?>
                <div class="msf-header__column">
                    <a href="#" id="changeStatusSend" class="btn btn-primary">Отправить данные на проверку</a>
                </div>
            <?}?>
            <div class="msf-header__column msf-header__column_logout"><a href="/enrollee/?logout=yes">Выход</a></div>
        </div>
		<div class="row text-left">
            <?if($arResultAb["СтатусЗаявки"]=="На доработке" || $arResultAb["СтатусЗаявки"]=="Принята"){?>
                <div class="msf-step"><a href="step_1.php">Основные данные</a></div>
                <div class="msf-step"><a href="step_2.php">Семья</a></div>
                <div class="msf-step"><a href="step_3.php">Дополнительная информация</a></div>
                <div class="msf-step active msf-step-active"><a href="step_4.php">Предоставленные документы</a></div>
                <div class="msf-step"><a href="step_5.php">Сведения о ЕГЭ</a></div>
                <div class="msf-step"><a href="step_6.php">Заявления</a></div>
                <div class="msf-step"><a href="info.php">Инфопанель</a></div>
            <?}else{?>
                <div class="msf-step"><span>Основные данные</span></div>
                <div class="msf-step"><span>Семья</span></div>
                <div class="msf-step"><span>Дополнительная информация</span></div>
                <div class="msf-step active msf-step-active"><span>Предоставленные документы</span></div>
                <div class="msf-step"><span>Сведения о ЕГЭ</span></div>
                <div class="msf-step"><span>Заявления</span></div>
                <div class="msf-step"><a href="info.php">Инфопанель</a></div>
            <?}?>
		</div>
    </div>
    <div class="msf-content">
        <div class="msf-view">
            <div class="row">
                <div class="col-md-12">
                    <h3>Предоставленные документы</h3>
					<br />
					<p><a target="_blank" href="https://msal.ru/upload/struktura/cpk2018/pkdoc/%D0%98%D0%BD%D1%81%D1%82%D1%80%D1%83%D0%BA%D1%86%D0%B8%D0%B8%20%D0%BF%D0%BE%20%D0%BF%D0%BE%D0%B4%D0%B3%D0%BE%D1%82%D0%BE%D0%B2%D0%BA%D0%B5%20%D0%B8%20%D0%BE%D1%82%D0%BF%D1%80%D0%B0%D0%B2%D0%BA%D0%B5%20%D0%B4%D0%BE%D0%BA%D1%83%D0%BC%D0%B5%D0%BD%D1%82%D0%BE%D0%B2%20%D0%B4%D0%BB%D1%8F%20%D0%BF%D0%BE%D0%B4%D0%B0%D1%87%D0%B8%20%D0%B7%D0%B0%D1%8F%D0%B2%D0%BB%D0%B5%D0%BD%D0%B8%D1%8F%20%D1%87%D0%B5%D1%80%D0%B5%D0%B7%20%D0%BB%D0%B8%D1%87%D0%BD%D1%8B%D0%B9%20%D0%BA%D0%B0%D0%B1%D0%B8%D0%BD%D0%B5%D1%82%20%D0%B0%D0%B1%D0%B8%D1%82%D1%83%D1%80%D0%B8%D0%B5%D0%BD%D1%82%D0%B0.pdf">Инструкции по подготовке и отправке документов для подачи заявления через личный кабинет абитуриента</a> (открыть в новой вкладке)</p>
                    <br>
                </div>
                <div class="col-md-12" id="upload-documents">
                    <?echo '<input id="status-inp" type="hidden" value="'.$arResultAb["СтатусЗаявки"].'">';?>
                    <?foreach($arErrors as $error){
                        ShowError($error);
                    }?>
                    <?if($_GET["msg"]=="wrngs"){
                        ShowError("Данные не сохранены. Большая нагрузка на сервис. Введите данные еще раз.");
                    }?>
                    <?if($_GET["msg"]=="ok"){
                        echo "<p class='ok'>Ваши данные успешно сохранены. <br>Для продолжения перейдите по <a href='/enrollee/step_5.php'><b>ссылке</b></a></p>";
                    }?>
                    <?if($_POST["changeSt"]=="Y"){?>
                        <?require($_SERVER["DOCUMENT_ROOT"]."/enrollee/action/action_chng_st.php");?>
                    <?}?>
                    <?if($_POST["submit"]=="Y"){?>
                        <?
                        //echo "<pre>";
                        //print_r($arRslt);
                        //print_r($arResultAb);
                        //print_r($arResultDoc);
                        //print_r($_REQUEST);
                        //print_r($_FILES);
                        //echo "</pre>";
                        ?>
                        <?require($_SERVER["DOCUMENT_ROOT"]."/enrollee/action/action_step_4.php");?>
                    <?}?>
                    <?
                    $arIndDocuments = array();
                    $arIndDocuments = $arrDocs["Документы удостоверяющие личность"];
                    // Получение списка столбцов
                    foreach ($arIndDocuments as $key => $row) {
                        $data[$key]  = $row['КодСправочника'];
                    }
                    // Сортируем массив по полю КодСправочника
                    array_multisort($data, SORT_ASC, $arIndDocuments);
                    unset($data);
                    $arIndDocumentsCode = array();
                    foreach ($arIndDocuments as $key => $row) {
                        $arIndDocumentsCode[$row['КодСправочника']] = $row;
                    }


/*
                    echo "<pre>";
                    //print_r($arResultAb);
                    //print_r($bufDocs = getAbitDocsToMaindata($arUser["XML_ID"], true));
                    print_r($arResultDoc);
                    print_r($arResultAbFiles);
                    echo "</pre>";
*/
                    ?>
                    <div class="person-document-block col-md-12">
                        <div id="document-3-type" class="form-group" attr-select="<?=$arResultAb["ПаспортВид"]?>">
                            <label>Удостоверение личности</label>
                            <select disabled name="inddocuments4" class="inddocuments form-control" onchange="GetSelndex4(this);">
                                <?
                                $curCode = '';
                                foreach($arIndDocuments as $arItem){
                                    if($arResultAb["ПаспортВид"]==$arItem["КодСправочника"] || $arResultAb["ПаспортВид"]==$arItem["Наименование"]){
                                        $curCode = $arItem["КодСправочника"];
                                    }?>
                                    <option attr-code="<?=$arItem["КодСправочника"]?>" attr-seriya="Да" attr-number="Да" value="<?=$arItem["КодСправочника"]?>" <?=($arResultAb["ПаспортВид"]==$arItem["КодСправочника"] || $arResultAb["ПаспортВид"]==$arItem["Наименование"])?'selected':''?>><?=$arItem["Наименование"]?></option>
                                <?}?>
                            </select>
                        </div>
                        <input type="hidden" name="inddoc4code" value="<?=$curCode?>">
						<div class="form-group">
							<div class="col-md-2 series series-document-type">
								<input type="text" class="form-control" id="DocSeries" name="docseries" value="<?=$arResultAb["ПаспортСерия"]?>" placeholder="Серия" data-msg="Введите серию" required disabled>
							</div>
							<div class="col-md-2 number number-document-type">
								<input type="text" class="form-control" id="DocNumber" name="doctnumber" value="<?=$arResultAb["ПаспортНомер"]?>" placeholder="Номер" data-msg="Введите номер" required disabled>
							</div>
							<div class="col-md-2 date date-document-type">
								<input type="text" class="form-control" id="DocDate" name="doctdate" value="<?=date('d.m.Y', strtotime($arResultAb["ПаспортДата"]))?>" placeholder="Дата выдачи" data-msg="Введите дату выдачи" required>
							</div>
							<div class="col-md-3 date who-document-type">
								<input type="text" class="form-control" id="DocWho" name="doctwho" value="<?=$arResultAb["ПаспортВыдан"]?>" placeholder="Кем выдан" data-msg="Заполните поле" required>
							</div>
							<div class="col-md-3 date code-document-type">
								<input type="text" class="form-control" id="DocCode" name="doctcode" value="<?=$arResultAb["ПаспортКодПодразделения"]?>" placeholder="Код подразделения" data-msg="Введите код подразделения" required>
							</div>
						</div>
                        <ul>
                            <?foreach($arIndDocuments as $arItem){
                                foreach($arResultAbFiles as $arFile){
                                    if($arItem["КодСправочника"] == $arFile["ТипДокумента"]){?>
                                        <li><span><?=$arItem["Наименование"]?></span> - <b>загружен</b> <span><?=date('d.m.Y', strtotime($arFile["ДатаИзменения"]))?></span></li>
                                    <?}
                                }?>
                            <?}?>
                        </ul>
                        <?if(!$localFl){?>
                            <div class="input-file-container">
                                <input class="input-file-3" id="my-file-3" name="identification-card" type="file" accept=".jpg, .pdf,.rtf,.xls,.xlsx,.doc,.docx">
                                <label tabindex="0" for="my-file-3" class="input-file-trigger-3"><i class="fas fa-upload"></i><span>загрузить файл...</span></label>
                                <p class="file-return-3 file-result"></p>
                            </div>
                        <?}?>
                    </div>

                    <?// до 20 июня скрываем
                    $arEduDocuments = array();
                    $arEduDocuments = $arrDocs["Документы об образовании"];
                    /*
                    // Получение списка столбцов
                    foreach ($arIndDocuments as $key => $row) {
                        $data[$key]  = $row['КодСправочника'];
                    }
                    // Сортируем массив по полю КодСправочника
                    array_multisort($data, SORT_ASC, $arIndDocuments);
                    unset($data);
                    $arIndDocumentsCode = array();
                    foreach ($arIndDocuments as $key => $row) {
                        $arIndDocumentsCode[$row['КодСправочника']] = $row;
                    }
                    */
                    ?>
                    <div class="education-document-block col-md-12">
                        <?/*
                        <div id="education-doc" class="form-group" attr-select="">
                            <label>Документ об образовании</label>
                        </div>
                        */?>
                        <div class="form-group" attr-select="">
                            <label>Документ об образовании</label>
                            <select name="education" class="inddocuments form-control valid" onchange="changeAttrSel(this); changeEducationDocuments(this)" aria-invalid="false">
                                <option value="">Выберите документ об образовании</option>
                            <?
                            $curCode = '';
                            foreach($arEduDocuments as $arItem){
                                $sel = '';
                                foreach($arResultAbFiles as $arFile){
                                    if($arItem["КодСправочника"] == $arFile["ТипДокумента"]){
                                        $sel = 'selected';
                                        break;
                                    }
                                }?>
                                <option <?=$sel?> attr-code="<?=$arItem["КодСправочника"]?>" attr-seriya="Да" attr-number="Да" value="<?=$arItem["КодСправочника"]?>" <?/*=($arResultAb["ПаспортВид"]==$arItem["КодСправочника"] || $arResultAb["ПаспортВид"]==$arItem["Наименование"])?'selected':''*/?>><?=$arItem["Наименование"]?></option>
                            <?}?>
                            </select>
                        </div>
                        <?if(!$localFl){?>
                            <?if(!empty($arResultAbFiles)){?>
                                <p><b>Файлы:</b></p>
                                <ul>
                                    <?$fl = false;
                                    foreach($arEduDocuments as $arItem){
                                        foreach($arResultAbFiles as $arFile){
                                            if($arItem["КодСправочника"] == $arFile["ТипДокумента"]){
                                                $fl = true;?>
                                                <li><span><?=$arItem["Наименование"]?></span> - <b>загружен</b> <span><?=date('d.m.Y', strtotime($arFile["ДатаИзменения"]))?></span></li>
                                            <?}
                                        }?>
                                    <?}?>
                                    <?if(!$fl){?>
                                        <li>Не загружены</li>
                                    <?}?>
                                </ul>
                            <?}?>
                            <?if(!empty($arResultDoc)){?>
                                <p><b>Реквизиты:</b></p>
                                <ul>
                                    <?$fl = false;
                                    foreach($arEduDocuments as $arItem){
                                        foreach($arResultDoc as $arFile){
                                            if($arItem["КодСправочника"] == $arFile["ВидДокумента"]){
                                                $fl = true;?>
                                                <li><span><?=$arItem["Наименование"]?>. Серия: <?=$arFile["Серия"]?>. Номер: <?=$arFile["Номер"]?></span> - <b>от</b> <span><?=date('d.m.Y', strtotime($arFile["Дата"]))?></span></li>
                                            <?}
                                        }?>
                                    <?}?>
                                    <?if(!$fl){?>
                                        <li>Не загружены</li>
                                    <?}?>
                                </ul>
                            <?}?>
                            <div class="input-file-container">
                                <input class="input-file-4" id="my-file-4" name="document-education" type="file" accept=".jpg, .pdf,.rtf,.xls,.xlsx,.doc,.docx">
                                <label tabindex="0" for="my-file-4" class="input-file-trigger-4"><i class="fas fa-upload"></i><span>загрузить файл...</span></label>
                                <p class="file-return-4 file-result"></p>
                            </div>
                        <?}else{?>
                            <?if(!empty($arResultDoc)){?>
                                <p><b>Реквизиты:</b></p>
                                <ul>
                                    <?$fl = false;
                                    foreach($arEduDocuments as $arItem){
                                        foreach($arResultDoc as $arFile){
                                            if($arItem["КодСправочника"] == $arFile["ВидДокумента"]){
                                                $fl = true;?>
                                                <li><span><?=$arItem["Наименование"]?>. Серия: <?=$arFile["Серия"]?>. Номер: <?=$arFile["Номер"]?></span> - <b>от</b> <span><?=date('d.m.Y', strtotime($arFile["Дата"]))?></span></li>
                                            <?}
                                        }?>
                                    <?}?>
                                    <?if(!$fl){?>
                                        <li>Не загружены</li>
                                    <?}?>
                                </ul>
                            <?}?>
                        <?}?>
                        <div class="education-doc-fields">
                            <div class="col-md-4"><input id="series-education-doc" name="serieseducationdoc" type="text" class="form-control" data-msg="Введите серию" placeholder="Серия документа"></div>
                            <div class="col-md-4"><input id="number-education-doc" name="numbereducationdoc" type="text" class="form-control" data-msg="Введите номер" placeholder="Номер документа" <?/*required*/?>></div>
                            <div class="col-md-4"><input id="date-education-doc" name="dateeducationdoc" type="text" class="form-control" data-msg="Укажите дату" placeholder="Дата" <?/*required*/?>></div>
                            <?/*
                            <div class="col-md-4"><input id="place-education-doc" name="placeeducationdoc" type="text" class="form-control" data-msg="Введите место выдачи" placeholder="Место выдачи" required></div>
                            <div class="col-md-4"><input id="where-education-doc" name="whereeducationdoc" type="text" class="form-control" data-msg="Укажите где выдан" placeholder="Где выдан" required></div>
                            <div class="col-md-4"><input id="speciality-education-doc" name="specialityeducationdoc" type="text" class="form-control" data-msg="Укажите специальность" placeholder="Специальность"></div>
                            <div class="col-md-4">
                                <div id="education-lvl-doc" attr-select="значение 2">
                                    <select name="lvleducation" class="inddocuments form-control valid" aria-invalid="false">
                                        <option>Уровень образования</option>
                                        <?foreach($arLvlvEdu as $arLvl){?>
                                            <option attr-educode="<?=$arLvl["КодСправочника"]?>"><?=$arLvl["Наименование"]?></option>
                                        <?}?>
                                    </select>
                                </div>
                            </div>
                            <div class="col-md-4"><input id="avgscore-education-doc" name="avgscoreeducationdoc" type="text" class="form-control" data-msg="Укажите средний балл" placeholder="Средний балл"></div>
                            */?>
                            <div id="full"></div>
                        </div>
                    </div>

                    <?
                    //$arrDocs["Без категории"]
                    //не вывел сюда массив, потому что не заполнены поля [КодСправочника]
                    ?>
                    <?/*
                    <div class="education-document-block  col-md-12">
                        <label>Иные документы</label>
                        <?if(!$localFl){?>
                            <?if(!empty($arResultAbFiles)){?>
                                <p><b>Файлы:</b></p>
                                <ul>
                                    <?$fl = false;
                                    foreach($arrDocs as $key => $arCategory){
                                        if($key != "Документы удостоверяющие личность" && $key != "НЕ ИСПОЛЬЗОВАТЬ" && $key != "Документы об образовании"){?>
                                            <?foreach($arCategory as $arItem){
                                                foreach($arResultAbFiles as $arFile){
                                                    if($arItem["КодСправочника"] == $arFile["ТипДокумента"]){
                                                        $fl = true;?>
                                                        <li><span><?=$arItem["Наименование"]?></span> - <b>загружен</b> <span><?=date('d.m.Y', strtotime($arFile["ДатаИзменения"]))?></span></li>
                                                        <?
                                                    }
                                                }?>
                                            <?}?>
                                        <?}?>
                                    <?}?>
                                    <?if(!$fl){?>
                                        <li>Не загружены</li>
                                    <?}?>
                                </ul>
                            <?}?>
                        <?}?>
                        <?if(!empty($arResultDoc)){?>
                            <p><b>Реквизиты:</b></p>
                            <ul>
                                <?$fl = false;
                                foreach($arrDocs as $key => $arCategory){
                                    if($key != "Документы удостоверяющие личность" && $key != "НЕ ИСПОЛЬЗОВАТЬ" && $key != "Документы об образовании"){?>
                                        <?foreach($arCategory as $arItem){
                                            foreach($arResultDoc as $arFile){
                                                if($arItem["КодСправочника"] == $arFile["ВидДокумента"]){
                                                    $fl = true;
                                                    ?>
                                                    <li><span><?=$arItem["Наименование"]?></span> - <b>загружен</b> <span><?=date('d.m.Y', strtotime($arFile["Дата"]))?></span></li>
                                                <?
                                                }
                                            }?>
                                        <?}?>
                                    <?}?>
                                <?}?>
                                <?if(!$fl){?>
                                    <li>Не загружены</li>
                                <?}?>
                            </ul>
                        <?}?>
                    </div>
                    */?>
                    <div class="other-documents-types form-group">
						<select>
                            <option data-series="0" data-number="0" data-date="0" value="">Выберите тип документа</option>
							<?foreach($arrDocs as $key => $arCategory){
								if($key != "Документы удостоверяющие личность" && $key != "НЕ ИСПОЛЬЗОВАТЬ" && $key != "Документы об образовании"){?>
									<?foreach($arCategory as $arItem){
										if($arItem["КодСправочника"]){?>
											<option data-series="<?=($arItem["НужнаСерия"]) ? 1 : 0?>" data-number="<?=($arItem["НуженНомер"]) ? 1 : 0?>" data-date="<?=($arItem["НуженаДата"]) ? 1 : 0?>" value="<?=$arItem["КодСправочника"]?>"><?=$arItem["Наименование"]?></option>
										<?}?>
									<?}?>
								<?}?>
							<?}?>
						</select>
					</div>
                    <div class="other-documents-types form-group"></div>
                    <div id="other-documents" class="col-md-12">
                        <?$arOtherDocs = array();
                        $arOtherDocsRequis = array();
                        foreach($arrDocs as $key => $arCategory){
                            if($key != "Документы удостоверяющие личность" && $key != "НЕ ИСПОЛЬЗОВАТЬ" && $key != "Документы об образовании"){?>
                                <?foreach($arCategory as $arItem){
                                    foreach($arResultAbFiles as $arFile){
                                        if($arItem["КодСправочника"] == $arFile["ТипДокумента"]){
                                            $arOtherDocs[$arFile["ТипДокумента"]] = $arFile;
                                        }
                                    }
                                    foreach($arResultDoc as $arItm){
                                        if($arItem["КодСправочника"] == $arItm["ВидДокумента"]){
                                            $arOtherDocsRequis[$arItm["ВидДокумента"]] = $arItm;
                                        }
                                    }
                                    ?>
                                <?}?>
                            <?}?>
                        <?}?>
                        <?/*
                        <pre>
                            <?print_r(array_merge_recursive($arOtherDocs, $arOtherDocsRequis))?>
                        </pre>
                        */?>
                        <?$num = 0;
                        //if(!$localFl){?>
                            <?//Другие документы - файлы
                            foreach($arOtherDocs as $arFile){
                                $arCur = array();?>
                                <div class="OtherDocumentBlock" data-id="<?=$num?>">
                                    <?/*<input value="удалить" class="DeleteOtherDoc" type="button">*/?>
                                    <div class="" attr-select="<?=$arFile["ТипДокумента"]?>">
                                        <select class="form-control other-documents" id="otherdocuments<?=$num?>" name="other_documents[<?=$num?>][type]" disabled>
                                            <?foreach($arrDocs as $key => $arCategory){
                                                if($key != "Документы удостоверяющие личность" && $key != "НЕ ИСПОЛЬЗОВАТЬ" && $key != "Документы об образовании"){?>
                                                    <?foreach($arCategory as $arItem){
                                                        if($arItem["КодСправочника"]){
                                                            if(($arItem["КодСправочника"] == $arFile["ТипДокумента"])){
                                                                $arCur = array_merge($arItem, $arFile);
                                                            }?>
                                                            <option value="<?=$arItem["КодСправочника"]?>" <?=($arItem["КодСправочника"] == $arFile["ТипДокумента"]) ? 'selected':''?>><?=$arItem["Наименование"]?></option>
                                                            <?/*
                                                            <option data-series="<?=($arItem["НужнаСерия"]) ? 1 : 0?>" data-number="<?=($arItem["НуженНомер"]) ? 1 : 0?>" data-date="<?=($arItem["НуженаДата"]) ? 1 : 0?>" value="<?=$arItem["КодСправочника"]?>"><?=$arItem["Наименование"]?></option>
                                                            */?>
                                                        <?}?>
                                                    <?}?>
                                                <?}?>
                                            <?}?>
                                        </select>
                                        <input type="hidden" name="other_documents[<?=$num?>][type]" value="<?=$arCur["ТипДокумента"]?>">
                                    </div>
                                    <?if($arCur["НужнаСерия"]){?>
                                        <div class="col-md-4 series-otherdoc otherdocuments<?=$num?>">
                                            <input class="form-control otherdoc-seriya-field" name="other_documents[<?=$num?>][series]" id="otherdoc-seriya<?=$num?>" placeholder="Серия документа *" data-msg="Введите серию" value="<?=$arOtherDocsRequis[$arCur["ТипДокумента"]]["Серия"]?>" <?/*=($arOtherDocsRequis[$arCur["ТипДокумента"]]["Серия"]) ? 'disabled':''*/?> required="">
                                        </div>
                                    <?}?>
                                    <?if($arCur["НуженНомер"]){?>
                                        <div class="col-md-4 number-otherdoc otherdocuments<?=$num?>">
                                            <input class="form-control otherdoc-number-field" name="other_documents[<?=$num?>][number]" id="otherdoc-number<?=$num?>" placeholder="Номер документа *" data-msg="Введите номер" value="<?=$arOtherDocsRequis[$arCur["ТипДокумента"]]["Номер"]?>" <?/*=($arOtherDocsRequis[$arCur["ТипДокумента"]]["Номер"]) ? 'disabled':''*/?> required="">
                                        </div>
                                    <?}?>
                                    <div class="col-md-12 input-file-container otherdocuments<?=$num?>">
                                        <p><?/*<span><?=$arCur["Наименование"]?></span> - */?><b>Файл загружен</b> - <span><?=date('d.m.Y', strtotime($arCur["ДатаИзменения"]))?></span></p>
                                        <?if(!$localFl){?>
                                            <input class="input-file-<?=($num+4)?>" id="my-file-<?=($num+4)?>" name="other_documents[<?=$num?>]" accept=".png,.jpg,.pdf,.rtf,.xls,.xlsx,.doc,.docx" type="file">
                                            <label for="my-file-<?=($num+4)?>" class="input-file-trigger-3<?=($num+4)?>">
                                                <svg class="svg-inline--fa fa-upload fa-w-16" aria-hidden="true" data-prefix="fas" data-icon="upload" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M296 384h-80c-13.3 0-24-10.7-24-24V192h-87.7c-17.8 0-26.7-21.5-14.1-34.1L242.3 5.7c7.5-7.5 19.8-7.5 27.3 0l152.2 152.2c12.6 12.6 3.7 34.1-14.1 34.1H320v168c0 13.3-10.7 24-24 24zm216-8v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V376c0-13.3 10.7-24 24-24h136v8c0 30.9 25.1 56 56 56h80c30.9 0 56-25.1 56-56v-8h136c13.3 0 24 10.7 24 24zm-124 88c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20zm64 0c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20z"></path></svg>
                                                <!-- <i class="fas fa-upload"></i> --><span>загрузить файл...</span>
                                            </label>
                                            <p class="file-return-<?=($num+4)?> file-result"></p>
                                        <?}?>
                                    </div>
                                </div>
                                <?
                                unset($arOtherDocsRequis[$arCur["ТипДокумента"]]);
                                $num++;
                            }?>
                            <?//Другие документы - реквизиты без файлов
                            foreach($arOtherDocsRequis as $arRequis){
                                $arCur = array();?>
                                <div class="OtherDocumentBlock" data-id="<?=$num?>">
                                    <?/*<input value="удалить" class="DeleteOtherDoc" type="button">*/?>
                                    <div class="" attr-select="<?=$arRequis["ВидДокумента"]?>">
                                        <select class="form-control other-documents" id="otherdocuments<?=$num?>" name="other_documents[<?=$num?>][type]" disabled>
                                            <?foreach($arrDocs as $key => $arCategory){
                                                if($key != "Документы удостоверяющие личность" && $key != "НЕ ИСПОЛЬЗОВАТЬ" && $key != "Документы об образовании"){?>
                                                    <?foreach($arCategory as $arItem){
                                                        if($arItem["КодСправочника"]){
                                                            if(($arItem["КодСправочника"] == $arRequis["ВидДокумента"])){
                                                                $arCur = array_merge($arItem, $arRequis);
                                                            }?>
                                                            <option value="<?=$arItem["КодСправочника"]?>" <?=($arItem["КодСправочника"] == $arRequis["ВидДокумента"]) ? 'selected':''?>><?=$arItem["Наименование"]?></option>
                                                        <?}?>
                                                    <?}?>
                                                <?}?>
                                            <?}?>
                                        </select>
                                        <input type="hidden" name="other_documents[<?=$num?>][type]" value="<?=$arCur["ВидДокумента"]?>">
                                    </div>
                                    <?if($arCur["НужнаСерия"]){?>
                                        <div class="col-md-4 series-otherdoc otherdocuments<?=$num?>">
                                            <input class="form-control otherdoc-seriya-field" name="other_documents[<?=$num?>][series]" id="otherdoc-seriya<?=$num?>" placeholder="Серия документа *" data-msg="Введите серию" value="<?=$arRequis["Серия"]?>" <?/*=($arRequis["Серия"]) ? 'disabled':''*/?> required="">
                                        </div>
                                    <?}?>
                                    <?if($arCur["НуженНомер"]){?>
                                        <div class="col-md-4 number-otherdoc otherdocuments<?=$num?>">
                                            <input class="form-control otherdoc-number-field" name="other_documents[<?=$num?>][number]" id="otherdoc-number<?=$num?>" placeholder="Номер документа *" data-msg="Введите номер" value="<?=$arRequis["Номер"]?>" <?/*=($arRequis["Номер"]) ? 'disabled':''*/?> required="">
                                        </div>
                                    <?}?>
                                    <?if(!$localFl){?>
                                        <div class="col-md-12 input-file-container otherdocuments<?=$num?>">
                                            <input class="input-file-<?=($num+4)?>" id="my-file-<?=($num+4)?>" name="other_documents[<?=$num?>]" accept=".png,.jpg,.pdf,.rtf,.xls,.xlsx,.doc,.docx" type="file">
                                            <label for="my-file-<?=($num+4)?>" class="input-file-trigger-3<?=($num+4)?>">
                                                <svg class="svg-inline--fa fa-upload fa-w-16" aria-hidden="true" data-prefix="fas" data-icon="upload" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 512 512" data-fa-i2svg=""><path fill="currentColor" d="M296 384h-80c-13.3 0-24-10.7-24-24V192h-87.7c-17.8 0-26.7-21.5-14.1-34.1L242.3 5.7c7.5-7.5 19.8-7.5 27.3 0l152.2 152.2c12.6 12.6 3.7 34.1-14.1 34.1H320v168c0 13.3-10.7 24-24 24zm216-8v112c0 13.3-10.7 24-24 24H24c-13.3 0-24-10.7-24-24V376c0-13.3 10.7-24 24-24h136v8c0 30.9 25.1 56 56 56h80c30.9 0 56-25.1 56-56v-8h136c13.3 0 24 10.7 24 24zm-124 88c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20zm64 0c0-11-9-20-20-20s-20 9-20 20 9 20 20 20 20-9 20-20z"></path></svg>
                                                <!-- <i class="fas fa-upload"></i> --><span>загрузить файл...</span>
                                            </label>
                                            <p class="file-return-<?=($num+4)?> file-result"></p>
                                        </div>
                                    <?}?>
                                </div>
                                <?
                                $num++;
                            }?>
                        <?//}?>
                    </div>
                    <div class="AddOtherDocuments col-md-12">
                        <button type="button" class="btn btn-default" id="addotherdocs">добавить иные документы</button>
                    </div>
                    <?/*
                    echo "<pre>";
                    //print_r($arResultAb);
                    //print_r($arOtherDocs);
                    //print_r($arLvlvEdu);
                    print_r($arrDocs);
                    //print_r($arrDocs["Без категории"]);
                    echo "</pre>";
                   */?>
                </div>
            </div>
        </div>
    </div>
    <div class="msf-navigation">
        <div class="row">
            <br>
            <div class="nav-btn">
                <button type="button" data-type="back" class="btn btn-default msf-nav-button"><i class="fa fa-chevron-left"></i> назад </button>
            </div>
            <div class="nav-btn">
                <?if($arResultAb["СтатусЗаявки"]=="Принята"){?>
                    <a href="/enrollee/step_5.php" class="btn btn-primary msf-nav-button">далее</a>
                <?}else{?>
                    <button type="button" data-type="next" class="btn btn-default msf-nav-button">далее <i class="fa fa-chevron-right"></i></button>
                    <button type="submit" data-type="submit" class="btn btn-primary msf-nav-button">далее</button>
                <?}?>
            </div>
        </div>
    </div>
    <input type="hidden" name="submit" value="Y">
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>