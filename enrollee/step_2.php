<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Семья");?>
<?global $USER;?>
<?session_start();?>
<?
$arErrors = array();
$arResult = array();
$arResultAb = array();
$arResultFamily = array();
if(!$USER->IsAuthorized()){
    //переадресация на авторизацию
    header('Location: /enrollee/step_0.php', true, 301);
    die();
}else{
    $rsUser = CUser::GetByLogin($USER->GetLogin());
    $arUser = $rsUser->Fetch();
    //echo "<pre>"; print_r($arUser); echo "</pre>";
    if(!$arUser["XML_ID"]){
        $arErrors[] = "У Пользователя не заполнено поле XML_ID";
        //ShowError("У Пользователя не заполнено поле XML_ID");
        $USER->Logout();
        $err = urlencode("Ошибка получения данных (XML_ID) для ".$arUser["LOGIN"].", обратитесь к администратору");
        header('Location: /enrollee/?err='.$err, true, 301);
        die();
    }else{
        $client = getSoapClient(WSABITURS_URL);
        if($client){
            $params = array("ID"=>$arUser["XML_ID"]);
            try{
                $result = $client->ПолучитьСписокАбитуриентов($params);
                $arResultAb = object_to_array($result->return->Состав->Абитуриент);
                //проверка на пустой ответ из 1С
                isEmptyArr1C($arResultAb, 2, true);
                $arResultBufFamily = json_decode(json_encode($result->return->Состав->СоставСемьи),TRUE);
                if(is_array($arResultBufFamily[0])){
                    $arResultFamily = $arResultBufFamily;
                }elseif(is_array($arResultBufFamily)){
                    $arResultFamily[0] = $arResultBufFamily;
                }else{
                    $arResultFamily = array();
                }
            }catch (Exception $e){
                $arErrors[] = 'Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n";
                //ShowError('Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n");
            }
            $arResult["СемейноеПоложение"] = $arResultAb["СемейноеПоложение"];
            if($_POST["submit"]=="Y"){
                foreach($_POST["family"] as $key => $val){
                    $arResult["Family"][$key]["СтепеньРодства"] = $val["family-degree"];
                    $arResult["Family"][$key]["ФИО"] = $val["family-fio"];
                    $arResult["Family"][$key]["ДатаРождения"] = $val["family-bdate"];
                    $arResult["Family"][$key]["МестоРаботы"] = "";
                    $arResult["Family"][$key]["Телефон"] =$val["family-phone"];
                    $arResult["Family"][$key]["Адрес"] = $val["family-member-address"];
                }
                if(isset($_POST["maritalstatus"])&&!empty($_POST["maritalstatus"])){
                    $arResult["СемейноеПоложение"] = $_POST["maritalstatus"];
                }
            }else{
                $arResult["Family"] = $arResultFamily;
            }
        }
    }
}?>
<?if($arResultAb["СтатусЗаявки"]=="На доработке"){?>
    <form id="formstatus" method="post" action="">
        <input type="hidden" name="changeSt" value="Y">
    </form>
<?}?>

<form class="form-horizontal msf js-form-address<?if($arResultAb["СтатусЗаявки"]=="Принята"){?> is-disabled<?}?>" id="formmsf2" method="post" action="">
    <div class="msf-header">
        <div class="row msf-header__top">
            <div class="msf-header__column"><div class="msf-header__status">Статус: <span id="status"><?=$arResultAb["СтатусЗаявки"]?></span></div></div>
            <?if($arResultAb["СтатусЗаявки"]=="На доработке"){?>
                <div class="msf-header__column">
                    <a href="#" id="changeStatusSend" class="btn btn-primary">Отправить данные на проверку</a>
                </div>
            <?}?>
            <div class="msf-header__column msf-header__column_logout"><a href="/enrollee/?logout=yes">Выход</a></div>
        </div>
		<div class="row text-left">
            <?if($arResultAb["СтатусЗаявки"]=="На доработке" || $arResultAb["СтатусЗаявки"]=="Принята"){?>
                <div class="msf-step"><a href="step_1.php">Основные данные</a></div>
                <div class="msf-step active msf-step-active"><a href="step_2.php">Семья</a></div>
                <div class="msf-step"><a href="step_3.php">Дополнительная информация</a></div>
                <div class="msf-step"><a href="step_4.php">Предоставленные документы</a></div>
                <div class="msf-step"><a href="step_5.php">Сведения о ЕГЭ</a></div>
                <div class="msf-step"><a href="step_6.php">Заявления</a></div>
                <div class="msf-step"><a href="info.php">Инфопанель</a></div>
            <?}else{?>
                <div class="msf-step"><span>Основные данные</span></div>
                <div class="msf-step active msf-step-active"><span>Семья</span></div>
                <div class="msf-step"><span>Дополнительная информация</span></div>
                <div class="msf-step"><span>Предоставленные документы</span></div>
                <div class="msf-step"><span>Сведения о ЕГЭ</span></div>
                <div class="msf-step"><span>Заявления</span></div>
                <div class="msf-step"><a href="info.php">Инфопанель</a></div>
            <?}?>
		</div>
    </div>
    <div class="msf-content">
        <div class="msf-view">
            <div class="row">
                <div class="col-md-12">
                    <h3>Семья</h3>
                </div>
                <div class="col-md-6">
                    <?echo '<input id="status-inp" type="hidden" value="'.$arResultAb["СтатусЗаявки"].'">';?>
                    <?foreach($arErrors as $error){
                        ShowError($error);
                    }?>
                    <?if($_GET["msg"]=="wrngs"){
                        ShowError("Данные не сохранены. Большая нагрузка на сервис. Введите данные еще раз.");
                    }?>
                    <?if($_POST["changeSt"]=="Y"){?>
                        <?require($_SERVER["DOCUMENT_ROOT"]."/enrollee/action/action_chng_st.php");?>
                    <?}?>
                    <?if($_POST["submit"]=="Y"){?>
                        <?require($_SERVER["DOCUMENT_ROOT"]."/enrollee/action/action_step_2.php");?>
                    <?}
                    $arMaterialStatus = array("Холост/Не замужем", "Женат/замужем", "Разведен(-а)", "Вдовец/вдова");
                    /*
                    echo "<pre>";
                    //print_r($arResultAb);
                    print_r($_POST);
                    print_r($arResult);
                    echo "</pre>";
                    */
                    ?>
                    <div class="form-group" attr-select="<?=$arResult["СемейноеПоложение"]?>">
                        <label>Семейное положение</label>
                        <select class="form-control" id="MaritalStatus" name="maritalstatus" required>
                            <?/*foreach($arMaterialStatus as $status){?>
                                <option value="<?=$status?>" <?=($status==$arResult["СемейноеПоложение"])?'selected':''?>><?=$status?></option>
                            <?}*/?>
                        </select>
						<div class="marital-status-CSV" attr-select="<?=$arResult["СемейноеПоложение"]?>"></div>
                        <span>Укажите семейное положение</span>
                    </div>
                    <div class="select-buffer"></div>
					
                    <div id="AddFamilyMemberContainer">
                        <?foreach($arResult["Family"] as $key => $val){?>
                            <div class="DynamicExtraField" data-id="<?=$key?>">
                                <input value="удалить" type="button" class="DeleteDynamicExtraField">
                                <div class="form-group" attr-select="<?=$val["СтепеньРодства"]?>">
                                    <?/*
                                    <input class="form-control family-degree-field"
                                           name="family[<?=$key?>][family-degree]"
                                           value="<?=$val["СтепеньРодства"]?>" id="family-degree<?=$key?>"
                                           placeholder="Степень родства"
                                           data-msg="Введите степень родства">
                                    */?>
                                    <select attr-select="<?=$val["СтепеньРодства"]?>" data-name="family-degree" class="form-control valid" <?/*onchange="changeAttrSel(this)"*/?> name="family[<?=$key?>][degree]" data-id="<?=$key?>" id="family-degree2" title="Степень родства" data-msg="Введите степень родства" aria-invalid="false">
                                    </select>

                                </div>
                                <div class="form-group">
                                    <input onkeyup="rusinput(&quot;ru&quot;, this );"
                                           class="form-control family-fio-field"
                                           name="family[<?=$key?>][family-fio]" value="<?=$val["ФИО"]?>"
                                           id="family-FIO<?=$key?>"
                                           placeholder="ФИО" data-msg="Введите ФИО">
                                </div>
                                <div class="form-group">
                                    <input class="form-control family-bdate-field bdate-picker"
                                           name="family[<?=$key?>][family-bdate]"
                                           value="<?=date('d.m.Y', strtotime($val["ДатаРождения"]))?>"
                                           id="family-bdate<?=$key?>"
                                           placeholder="Дата рождения">
                                </div>
                                <div class="form-group">
                                    <input class="form-control family-phone-field phone-mask"
                                           name="family[<?=$key?>][family-phone]"
                                           value="<?=$val["Телефон"]?>" id="family-phone<?=$key?>"
                                           placeholder="Телефон" data-msg="Введите телефон">
                                </div>
                                <div class="form-group">
                                    <input class="form-control family-address-field"
                                           name="family[<?=$key?>][family-member-address]"
                                           value="<?=$val["Адрес"]?>" id="family-member-address<?=$key?>"
                                           placeholder="Адрес" data-msg="Введите адрес">
                                </div>
                            </div>
                        <?}?>
                    </div>
                    <div class="form-group">
                        <button id="addfamilymember" type="button" class="btn btn-default">добавить члена семьи</button>
                    </div>
                    <div class="message-parent">
                        <span><i class="fas fa-exclamation-circle"></i>Необходимо добавить родителя</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="msf-navigation">
        <div class="row">
			<p class="text-left"><br><span class="required">*</span> &mdash; поля обязательные для заполнения</p>
            <div class="nav-btn">
                <button type="button" data-type="back" class="btn btn-default msf-nav-button"><i class="fa fa-chevron-left"></i> назад </button>
            </div>
            <div class="nav-btn">
                <?if($arResultAb["СтатусЗаявки"]=="Принята"){?>
                    <a href="/enrollee/step_3.php" class="btn btn-primary msf-nav-button">далее</a>
                <?}else{?>
                    <button type="button" data-type="next" class="btn btn-default msf-nav-button">далее <i class="fa fa-chevron-right"></i></button>
                    <button type="submit" data-type="submit" class="btn btn-primary msf-nav-button">далее</button>
                <?}?>
            </div>
        </div>
    </div>
    <input type="hidden" name="submit" value="Y">
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>