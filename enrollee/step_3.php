<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Дополнительная информация");?>
<?global $USER;?>
<?session_start();?>
<?
$arErrors = array();
$arResult = array();
$arResultAb = array();
$arResultLang = array();
if(!$USER->IsAuthorized()){
    //переадресация на авторизацию
    header('Location: https://msal.me/enrollee/step_0.php');
    //exit();
}else{
    $rsUser = CUser::GetByLogin($USER->GetLogin());
    $arUser = $rsUser->Fetch();
    //echo "<pre>"; print_r($arUser); echo "</pre>";
    if(!$arUser["XML_ID"]){
        $arErrors[] = "У Пользователя не заполнено поле XML_ID";
        //ShowError("У Пользователя не заполнено поле XML_ID");
        $USER->Logout();
        $err = urlencode("Ошибка получения данных (XML_ID) для ".$arUser["LOGIN"].", обратитесь к администратору");
        header('Location: /enrollee/?err='.$err, true, 301);
        die();
    }else{
        $client = getSoapClient(WSABITURS_URL);
        if($client){
            $params = array("ID"=>$arUser["XML_ID"]);
            try{
                $result = $client->ПолучитьСписокАбитуриентов($params);
                $arResultAb = object_to_array($result->return->Состав->Абитуриент);
                //проверка на пустой ответ из 1С
                isEmptyArr1C($arResultAb, 3, true);
                $arResultBufLang = json_decode(json_encode($result->return->Состав->ИностранныеЯзыки),TRUE);
                if(is_array($arResultBufLang[0])){
                    $arResultLang = $arResultBufLang;
                }elseif(is_array($arResultBufLang)){
                    $arResultLang[0] = $arResultBufLang;
                }else{
                    $arResultLang= array();
                }
            }catch (Exception $e){
                $arErrors[] = 'Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n";
                //ShowError('Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n");
            }
            /*
            $arResultLang
            ИностранныеЯзыки
            ИностранныйЯзык
            */
            $arResult["ПосещениеПодготовительныхКурсов"] = $arResultAb["ПосещениеПодготовительныхКурсов"];
            $arResult["Должность"] = $arResultAb["Должность"];
            $arResult["МестоРаботы"] = $arResultAb["МестоРаботы"];
            if($_POST["submit"]=="Y"){
                $arResultLang = array();
                foreach($_POST["languages"] as $key => $val){
                    $arResultLang[$key]["ИностранныйЯзык"] = $val["language"];
                    $arResultLang[$key]["Основной"] = $val["main_language"];
                    //$arResultLang[$key][""] = $val["check_language"];
                }
                if(isset($_POST["Сourses"])&&!empty($_POST["Сourses"])){
                    $arResult["ПосещениеПодготовительныхКурсов"] = $_POST["Сourses"];
                }
                if(isset($_POST["employment"])&&!empty($_POST["employment"])){
                    $arResult["Должность"] = $_POST["employment"];
                }
                if(isset($_POST["workplace"])&&!empty($_POST["workplace"])){
                    $arResult["МестоРаботы"] = $_POST["workplace"];
                }
            }
        }
        //echo "<pre>";
        //print_r($result);
        //print_r($arResultAb);
        //print_r($arResultLang);
        //print_r($_POST);
        //print_r($arResult);
        //echo "</pre>";

    }
}?>
<?if($arResultAb["СтатусЗаявки"] == "На доработке"){?>
    <form id="formstatus" method="post" action="">
        <input type="hidden" name="changeSt" value="Y">
    </form>
<?}?>
<form class="form-horizontal msf js-form-address<?if($arResultAb["СтатусЗаявки"]=="Принята"){?> is-disabled<?}?>" id="formmsf3" method="post" action="">
    <div class="msf-header">
        <div class="row msf-header__top">
            <div class="msf-header__column"><div class="msf-header__status">Статус: <span id="status"><?=$arResultAb["СтатусЗаявки"]?></span></div></div>
            <?if($arResultAb["СтатусЗаявки"]=="На доработке"){?>
                <div class="msf-header__column">
                    <a href="#" id="changeStatusSend" class="btn btn-primary">Отправить данные на проверку</a>
                </div>
            <?}?>
            <div class="msf-header__column msf-header__column_logout"><a href="/enrollee/?logout=yes">Выход</a></div>
        </div>
		<div class="row text-left">
            <?if($arResultAb["СтатусЗаявки"]=="На доработке" || $arResultAb["СтатусЗаявки"]=="Принята"){?>
                <div class="msf-step"><a href="step_1.php">Основные данные</a></div>
                <div class="msf-step"><a href="step_2.php">Семья</a></div>
                <div class="msf-step active msf-step-active"><a href="step_3.php">Дополнительная информация</a></div>
                <div class="msf-step"><a href="step_4.php">Предоставленные документы</a></div>
                <div class="msf-step"><a href="step_5.php">Сведения о ЕГЭ</a></div>
                <div class="msf-step"><a href="step_6.php">Заявления</a></div>
                <div class="msf-step"><a href="info.php">Инфопанель</a></div>
            <?}else{?>
                <div class="msf-step"><span>Основные данные</span></div>
                <div class="msf-step"><span>Семья</span></div>
                <div class="msf-step active msf-step-active"><span>Дополнительная информация</span></div>
                <div class="msf-step"><span>Предоставленные документы</span></div>
                <div class="msf-step"><span>Сведения о ЕГЭ</span></div>
                <div class="msf-step"><span>Заявления</span></div>
                <div class="msf-step"><a href="info.php">Инфопанель</a></div>
            <?}?>
		</div>
    </div>
    <div class="msf-content">
        <div class="msf-view">
            <div class="row">
                <div class="col-md-12">
                    <h3>Дополнительная информация</h3>
                </div>
                <div class="col-md-6">
                    <?echo '<input id="status-inp" type="hidden" value="'.$arResultAb["СтатусЗаявки"].'">';?>
                    <?foreach($arErrors as $error){
                        ShowError($error);
                    }?>
                    <?if($_GET["msg"]=="wrngs"){
                        ShowError("Данные не сохранены. Большая нагрузка на сервис. Введите данные еще раз.");
                    }?>
                    <?if($_POST["changeSt"]=="Y"){?>
                        <?require($_SERVER["DOCUMENT_ROOT"]."/enrollee/action/action_chng_st.php");?>
                    <?}?>
                    <?if($_POST["submit"]=="Y"){?>
                        <?require($_SERVER["DOCUMENT_ROOT"]."/enrollee/action/action_step_3.php");?>
                    <?}?>
                    <label>Добавить иностранные языки</label>
                    <div class="lang-buffer"></div>
                    <div id="AddLangContainer">
                        <?foreach($arResultLang as $key => $val){?>
                            <div class="AdditionalLang" data-id="<?=$key?>">
                                <input value="удалить" class="deleteLang" type="button">
								<div class="language-wrap" attr-select="<?=$val["ИностранныйЯзык"]?>">
									<select class="form-control add-lang" name="languages[<?=$key?>][language]" id="lang<?=$key?>" attr-select="<?=$val["ИностранныйЯзык"]?>"></select>
								</div>
                                <input class="form-check-input" value="addlang<?=$key?>" id="addlang<?=$key?>" name="languages[<?=$key?>][main_language]" type="radio" <?=($val["Основной"])?'checked':''?>><label class="form-check-label lang-check" for="addlang<?=$key?>">Хочу изучать этот язык</label><br>
                                <input class="form-check-input" value="addcheck<?=$key?>" id="addcheck<?=$key?>" name="languages[<?=$key?>][check_language]" type="checkbox"><label class="form-check-label lang-check" for="addcheck<?=$key?>">Аттестован (Аттестат/Диплом)</label>
                            </div>
                        <?}?>
                    </div>
                    <div class="form-group">
                        <button id="addlanguage" type="button" class="btn btn-default">
                            добавить иностранный язык
                        </button>
                    </div>
                    <div class="form-group">
                        <label>Подготовительные курсы</label>
                        <?$arCourses = array("посещал(-а) в МГЮА", "не посещал(-а)", "посещал(-а) в другом месте");
                        $num = 1;
                        foreach($arCourses as $key=>$arItem){?>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="Сourses" id="Сourses<?=$num?>"
                                       value="<?=$arItem?>" <?=($arResult["ПосещениеПодготовительныхКурсов"]==$arItem)?'checked':''?>>
                                <span class="Сourses-label" for="Сourses<?=$num?>"><?=$arItem?></span>
                            </div>
                            <?$num++;?>
                        <?}?>
                        <?/*
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="Сourses" id="Сourses1"
                                   value="посещал в МГЮА" checked>
                            <span class="Сourses-label" for="Сourses1"></span>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="Сourses" id="Сourses2"
                                   value="не посещал">
                            <span class="Сourses-label" for="Сourses2"></span>
                        </div>
                        <div class="form-check">
                            <input class="form-check-input" type="radio" name="Сourses" id="Сourses3"
                                   value="посещал в другом месте">
                            <span class="Сourses-label" for="Сourses3">посещал(-а) в другом месте</span>
                        </div>
                        */?>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="employment" value="<?=$arResult["Должность"]?>" id="employment" placeholder="Трудоустройство/должность" data-val="true" data-msg='Укажите трудоустройство/должность'>
                    </div>
                    <div class="form-group">
                        <input type="text" class="form-control" name="workplace" value="<?=$arResult["МестоРаботы"]?>" id="workplace" placeholder="Место работы" data-val="true" data-msg='Укажите место работы'>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="msf-navigation">
        <div class="row">
            <br>
            <div class="nav-btn">
                <button type="button" data-type="back" class="btn btn-default msf-nav-button"><i class="fa fa-chevron-left"></i> назад </button>
            </div>
            <div class="nav-btn">
                <?if($arResultAb["СтатусЗаявки"]=="Принята"){?>
                    <a href="/enrollee/step_4.php" class="btn btn-primary msf-nav-button">далее</a>
                <?}else{?>
                    <button type="button" data-type="next" class="btn btn-default msf-nav-button">далее <i class="fa fa-chevron-right"></i></button>
                    <button type="submit" data-type="submit" class="btn btn-primary msf-nav-button">далее</button>
                <?}?>
            </div>
        </div>
    </div>
    <input type="hidden" name="submit" value="Y">
</form>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>