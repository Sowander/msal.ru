<?php
include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Diag\Debug;

//если поступил лзапрос на обнавление расписания от 1с
if (intval($_REQUEST['start']) === 1 && $_POST['ZdesJSON']) { // && $_SERVER['REMOTE_ADDR'] == Lk::$host
    $time = date('d.m.Y-H:i:s');
    $result['TIME'] = date('c');
    $result['REQUEST'] = $_REQUEST;
    $result['REMOTE_ADDR'] = $_SERVER['REMOTE_ADDR'];
    try {
        Debug::writeToFile($result, '', START_SH_FILE);
        Import1C::updatePreopd($_REQUEST['ZdesJSON']);
    } catch (Exception $e) {
        $error = ' ОШИБКА обновления данных ' . $e->getMessage();
        echo $error;
        $result['RESULT'] = $time . $error;
        $result['REQUEST'] = $_REQUEST;
        Debug::writeToFile($result, '', SH_LOG);
    }
}