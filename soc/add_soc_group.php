<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php"); ?>
<?
/*
 * A (Только владелец группы),
 * E (Владелец группы и модераторы группы),
 * K (Все члены группы ).
 * L (Авторизованные пользователи).
 * N (все все даже не авторизованые).
 * https://dev.1c-bitrix.ru/api_help/socialnetwork/classes/csocnetgroup/creategroup.php
 *      [Operations] => Array
                        (
                            [view_post] => L
                            [premoderate_post] => K
                            [write_post] => K
                            [moderate_post] => E
                            [full_post] => E
                            [view_comment] => K
                            [premoderate_comment] => K
                            [write_comment] => K
                            [moderate_comment] => E
                            [full_comment] => E
                        )

Тип объекта:
SONET_ENTITY_GROUP - группа,
SONET_ENTITY_USER - пользователь.
 */
global $USER;
if (!CModule::IncludeModule("socialnetwork")) {
    die("Модуль не подключен socialnetwork");
}
if (!$USER->IsAdmin()) {
    die("Только для админа");
}

function addSocGroup($idMaster, $groupName, $visible = 'Y', $inPerms = 'E', $spamPerms = "K")
{
    $errorMessage = '';
    $groupId = false;
    $groupName = trim($groupName);

    $arGroup = CSocNetGroup::GetList(
        array("ID" => "DESC"),
        $arFilter = array('NAME' => $groupName),
        $arGroupBy = false,
        $arNavStartParams = false,
        $arSelectFields = ['ID']
    )->fetch();

    if (!$arGroup['ID']) {
        $arFields = array(
            "SITE_ID" => SITE_ID,
            "NAME" => $groupName,
            "DESCRIPTION" => "Описание",
            "VISIBLE" => $visible === 'Y'?'Y':'N',
            "OPENED" => "N",
            "CLOSED" => "N",
            "SUBJECT_ID" => 1,
            "INITIATE_PERMS" => $inPerms,
            "SPAM_PERMS" => $spamPerms,
        );
        $groupId = CSocNetGroup::CreateGroup($idMaster, $arFields);
        if (!$groupId) {
            if ($e = $GLOBALS["APPLICATION"]->GetException())
                $errorMessage .= $e->GetString() . "<br>";
            echo "группа $groupName <br>";
            echo "id  user $idMaster <br>";
            echo $errorMessage;
        } else {
            echo "группа $groupName создана <br>";
        }
    } else {
        echo "группа $groupName уже существует! <br>";
    }
    return $groupId;
}


    $filter = array('UF_STUDENT_STAR' => 1,'ACTIVE'=>'Y');
    $by = "id";
    $order = 'asc';
    $select = array('FIELDS' => array('ID'), 'SELECT' => ['UF_STUDENT_GROUP']);
    $rsUsers = \CUser::GetList($by, $order, $filter, $select);
    while ($arUser = $rsUsers->Fetch()) {
        $arStarList[] = $arUser;
    }

    $feature = 'blog';
    $arOper = [
        'write_post',
        'view_post',
        'view_comment',
        'write_comment',
    ];
    if(is_array($arStarList) && !empty($arStarList)){
        foreach ($arStarList as $arStar){
            if($arStar['ID'] && $arStar['UF_STUDENT_GROUP']){
                $groupId = addSocGroup($arStar['ID'],trim($arStar['UF_STUDENT_GROUP']));
                if($groupId){
                    $idTmp = CSocNetFeatures::setFeature(
                        SONET_ENTITY_GROUP,
                        $groupId,
                        $feature,
                        "Y",
                        false
                    );
                    foreach ($arOper as $oper){
                        $id1Tmp = CSocNetFeaturesPerms::SetPerm(
                            $idTmp,
                            $oper,
                            'L'
                        );
                    }
                    unset($id1Tmp);
                    unset($idTmp);
                }
            }
        }
    }

?>

