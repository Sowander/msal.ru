<?php require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

if (!CModule::IncludeModule("socialnetwork")) {
    die("Модуль не подключен socialnetwork");
}
if (!CModule::IncludeModule("main")) {
    die('Модуль не подключен main');
}
global $USER;
if (!$USER->IsAdmin()){
    die('Только для админа');
}
$tempFilter = [];
$filterGroupName = [];
$arUserList = [];

$obRes = CSocNetGroup::GetList([], [], false, false, ['ID', 'NAME','OWNER_ID']);
while ($res = $obRes->fetch()) {
    $filterGroupName[] = trim($res['NAME']);
    $tempFilter[trim($res['NAME'])]['ID'] = $res['ID'];
    $tempFilter[trim($res['NAME'])]['OWNER_ID'] = $res['OWNER_ID'];
}

if (!empty($filterGroupName)) {

    $filter = array('%UF_STUDENT_GROUP' => $filterGroupName, "ACTIVE" => "Y", "GROUPS_ID" => [ID_STUNENT_GROUP]);
    $by = "id";
    $order = 'asc';
    $select = array('FIELDS' => ['ID'], 'SELECT' => ['UF_STUDENT_GROUP']);
    $rsUsers = \CUser::GetList($by, $order, $filter, $select);
    while ($arUser = $rsUsers->NavNext()) {
        $arUserList[trim($arUser['UF_STUDENT_GROUP'])][] = $arUser['ID'];
    }

    if (!empty($arUserList) && is_array($arUserList)) {
        foreach ($arUserList as $group => $arUsers) {
            if (!empty($arUsers) && is_array($arUsers))
                foreach ($arUsers as $userId) {
                    if ($userId && $tempFilter[$group]['ID']){
                        $resAdd=CSocNetUserToGroup::Add(
                            array(
                                "USER_ID" => $userId,
                                "GROUP_ID" => $tempFilter[$group]['ID'],
                                "ROLE" => SONET_ROLES_USER,
                                "=DATE_CREATE" => $GLOBALS["DB"]->CurrentTimeFunction(),
                                "=DATE_UPDATE" => $GLOBALS["DB"]->CurrentTimeFunction(),
                                "INITIATED_BY_TYPE" => SONET_INITIATED_BY_GROUP,
                                "INITIATED_BY_USER_ID" => $tempFilter[$group]['OWNER_ID'],
                                "MESSAGE" => false,
                            )
                        );
                        $arResAdd[] = $resAdd;
                    }
                }
        }
    }
}
printr('Добавлено '.count($arResAdd). 'юзероа, в '. count($arUserList). 'групп');