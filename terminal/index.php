<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/header.php');?>
	<section class="msal-main">
		<div class="msal-main__text">
			<p class="msal-main__text-title">Добрый день!</p>
			<p class="msal-main__text-subtitle">Выберите интересующий вас пункт меню, и следуйте подсказкам на экране</p>
		</div>
		<div class="msal-main__homepage-sections">
			<div class="homepage-sections">
				<div class="homepage-sections__item">
					<a href="schedule/index.php?id=123&step=1">Расписание</a>
				</div>
				<div class="homepage-sections__item">
					<a href="teachers.php">Преподаватели</a>
				</div>
				<div class="homepage-sections__item">
					<a href="kafedry.php">Кафедры</a>
				</div>
				<div class="homepage-sections__item">
					<a href="news/index.php">Новости</a>
				</div>
			</div>
		</div>
	</section>
<?require($_SERVER['DOCUMENT_ROOT'].'/bitrix/footer.php');?>