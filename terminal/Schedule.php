<?php

namespace Terminal;

/**
 * Created by PhpStorm.
 * User: Sowander
 * Date: 20.02.2018
 * Time: 15:14
 */
use Bitrix\Main\Diag\Debug;

require_once('Common.php');

class Schedule extends Common
{
	static function getListSchedule()
	{
		$function = 'ПолучитьСписокРасписаний';
		$wsdl = 'http://' . self::$host . '/UV/ws/wsreportschedulegroup.1cws?wsdl';
		return self::getSoapClient($wsdl, $function);
	}

	static function getkListScheduleBitrix($active = false)
	{
		self::setBitrixSystems();

		$arResult = NULL;
		$arSelect = Array("ID", "NAME", "ACTIVE", "PROPERTY_SCH_GUID");
		$arFilter = Array("IBLOCK_ID" => IntVal(self::$ListScheduleIblockID), 'ACTIVE' => $active);
		$res = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNext()) {
			$arResult[] = $ob;
		}
		return $arResult;
	}

	static function setListSchedule()
	{

		self::setBitrixSystems();
		$ob1C = self::getListSchedule();
		$arBitrix = self::getkListScheduleBitrix();

		global $USER;
		$el = new \CIBlockElement;
		foreach ($ob1C as $ob) {

			unset($arLoadProductArray);
			unset($PROP);
			unset($PRODUCT_ID);
			unset($ACTIVE);

			$PROP['SCH_GUID'] = trim($ob->Расписание->GUIDРасписания);
			$PROP['SCH_YEAR_NAME'] = trim($ob->УчебныйГодРасписания->Наименование);
			$PROP['SCH_YEAR_GUID'] = trim($ob->УчебныйГодРасписания->GUIDУчебногоГодаРасписания);
			$PROP['SCH_RING_NAME'] = trim($ob->ПланЗвонковПоУмолчаниюРасписания->Наименование);
			$PROP['SCH_RING_GUID'] = trim($ob->ПланЗвонковПоУмолчаниюРасписания->GUIDПланаЗвонковПоУмолчаниюРасписания);

			foreach ($arBitrix as $arItem) {
				if (trim($arItem['PROPERTY_SCH_GUID_VALUE']) == trim($ob->Расписание->GUIDРасписания)) {
					$PRODUCT_ID = $arItem['ID'];
					$ACTIVE = $arItem['ACTIVE'];
				}
			}
			$arLoadProductArray = Array(
				"MODIFIED_BY" => $USER->GetID(), // элемент изменен текущим пользователем
				"IBLOCK_SECTION_ID" => false,
				"IBLOCK_ID" => self::$ListScheduleIblockID,
				"PROPERTY_VALUES" => $PROP,
				"NAME" => trim($ob->Расписание->Наименование),
				"ACTIVE" => $ACTIVE ? $ACTIVE : 'N',
			);
			if ($PRODUCT_ID) { //Обновим элемент
				if (!$res = $el->Update($PRODUCT_ID, $arLoadProductArray)) {
					Debug::writeToFile($el->LAST_ERROR . '/n', '', self::$logpath . 'setListScheduleErrorUpdate.log');
				}
			} else { //Добавим элемент
				if (!$PRODUCT_ID = $el->Add($arLoadProductArray)) {
					Debug::writeToFile($el->LAST_ERROR . '/n', '', self::$logpath . 'setListScheduleErrorAdd.log');
				}
			}
		}
	}

	static function getNumberLection($timeStart)
	{
		switch ($timeStart) {
			case 9:
				return 1;
				break;
			case 10:
				return 2;
				break;
			case 12:
				return 3;
				break;
			case 13:
				return 3;
				break;
			case 14:
				return 4;
				break;
			case 15:
				return 5;
				break;
			case 16:
				return 6;
				break;
			case 17:
				return 6;
				break;
			case 18:
				return 7;
				break;
			case 19:
				return 7;
				break;
			case 20:
				return 8;
				break;
			default:
				return false;
		}
	}

	static function getNumberLectionPrepod($timeStart)
	{
		switch ($timeStart) {
			case 9:
				return 1;
				break;
			case 10:
				return 2;
				break;
			case 12:
				return 3;
				break;
			case 13:
				return 4;
				break;
			case 14:
				return 4;
				break;
			case 15:
				return 5;
				break;
			case 16:
				return 6;
				break;
			case 17:
				return 6;
				break;
			case 18:
				return 7;
				break;
			case 19:
				return 7;
				break;
			case 20:
				return 8;
				break;
			default:
				return false;
		}
	}


	static function getListGroups()
	{
		$result = [];
		$entityDataClassGr = self::GetEntityDataClass(HL_GROUPS_ID);
		$obRes = $entityDataClassGr::getList(['select' => ['*']]);
		while ($res = $obRes->fetch()) {
			if ($res['UF_NAME'] && $res['UF_XML_ID']) {
				$result[$res['UF_INST']][] = [
					'UF_NAME' => $res['UF_NAME'],
					'UF_XML_ID' => $res['UF_XML_ID'],
					'UF_COURSE' => $res['UF_COURSE'],
					'UF_FORM' => $res['UF_FORM'],
				];
			}
		}
		return $result;
	}

}