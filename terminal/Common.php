<?php
namespace Terminal;
include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Loader;
use Bitrix\Highloadblock\HighloadBlockTable as HLBT;

class Common
{
	protected static $login = 'webservices';
	protected static $password = 'WebTest17';
	protected static $host = '10.0.0.53';
	protected static $logpath = '/logs/';
	protected static $ListScheduleIblockID = 17;
	protected static $ListRingIblockID = 18;
	public static $arInst = [
		'ИПР МАГ'=>'',
		'ИПР'=>'ИНСТИТУТ ПРОКУРАТУРЫ',
		'ИАДМАГ'=>'ИНСТИТУТ МАГИСТРАТУРЫ',
		'ИАД'=>'ИНСТИТУТ АДВОКАТУРЫ',
		'ЮЗИ(СП)'=>'ЮРИДИЧЕСКИЙ ЗАОЧНЫЙ ИНСТИТУТ (ПСП)',
		'ЮЗИ'=>'ЮРИДИЧЕСКИЙ ЗАОЧНЫЙ ИНСТИТУТ',
		'ИППУ'=>'ИНСТИТУТ ПУБЛИЧНОГО ПРАВА И УПРАВЛЕНИЯ',
		'ИСПП'=>'ИНСТИТУТ СОВРЕМЕННОГО ПРИКЛАДНОГО ПРАВА',
		'ИФБП'=>'ИНСТИТУТ ФИНАНСОВОГО И БАНКОВСКОГО ПРАВА',
		'ИЧП'=>'ИНСТИТУТ ЧАСТНОГО ПРАВА',
		'МПИ'=>'МЕЖДУНАРОДНО-ПРАВОВОЙ ИНСТИТУТ',
		'ИСЭ'=>'ИНСТИТУТ СУДЕБНЫХ ЭКСПЕРТИЗ',
		'ИПВ'=>'',
		'ИБП'=>'ИНСТИТУТ БИЗНЕС ПРАВА',
		'ИЭП'=>'ИНСТИТУТ ЭНЕРГЕТИЧЕСКОГО ПРАВА',
		'ИНО'=>'ИНСТИТУТ НЕПРЕРЫВНОГО ОБРАЗОВАНИЯ',

	];


	static function setBitrixSystems()
	{
		require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
		Loader::includeModule("iblock");
	}


	static function getSoapClient($wsd, $functionName, $params = NULL, $obj = false)
	{
		if (!function_exists('is_soap_fault')) {
			print 'Не настроен web сервер. Не найден модуль php-soap.';
			return false;
		}

		try {
			//кеширование wsdl
			$cache = static::wsdlCahe(1);
			$client = new \SoapClient($wsd,
				array("login" => self::$login,
					"password" => self::$password,
					'soap_version' => SOAP_1_2,
					'trace' => true,
					'exceptions' => true,
					'cache_wsdl' => $cache
				)
			);
		} catch (SoapFault $e) {
			trigger_error('Ошибка подключения или внутренняя ошибка сервера. Не удалось связаться с базой 1С.', E_ERROR);
			var_dump($e);
		}
		if (is_soap_fault($client)) {
			trigger_error('Ошибка подключения или внутренняя ошибка сервера. Не удалось связаться с базой 1С.', E_ERROR);
			return false;
		}

		if ($obj === false) {
			$obj = 'Состав';
		}

		if ($obj === null) {
			$result = $client->$functionName($params)->return;
		} else {
			$result = $client->$functionName($params)->return->$obj;
		}
		return $result;
	}

	static function wsdlCahe($bool)
	{
		$timeout = 300;
		$time = 24*3600;
		ini_set('default_socket_timeout', intval($timeout));
		ini_set('soap.wsdl_cache_enabled', intval($bool));
		ini_set('soap.wsdl_cache_ttl', intval($time));
		return intval($bool);
	}

	static function dateToMS($curDate = false, $format = false, $plus = false)
	{
		setlocale(LC_TIME, 'ru_RU.UTF-8');

		if ($curDate === false) {
			$now = time();
		} else {
			$now = strtotime($curDate);
		}


		if ($format === false) {
			$date['monday'] = strftime('%F', strtotime('this week monday', $now));
			$date['sunday'] = strftime('%F', strtotime('this week sunday', $now));
		} else {
			$date['monday'] = strftime($format, strtotime('this week monday', $now));
			$date['sunday'] = strftime($format, strtotime('this week sunday', $now));
		}
		if ($plus !== false) {
			$date['monday'] = strftime($format, strtotime($plus . ' week monday', $now));
			$date['sunday'] = strftime($format, strtotime($plus . ' week sunday', $now));
		}

		return $date;
	}


	static function dateToDay($date = false, $format = false, $plus = false)
	{
		setlocale(LC_TIME, 'ru_RU.UTF-8');
		if ($format === false) {
			$format = '%A';
		}
		if ($date === false) {
			$day = strftime($format);
		} else {
			$day = strftime($format, strtotime($date));
		}
		if ($plus !== false && $format !== false) {
			$date = strtotime($date);
			$day = strftime($format, strtotime($plus, $date));
		}

		return $day;
	}

	static function stdClassInArray($object)
	{
		return json_decode(json_encode($object), true);
	}

	/**
	 * Функция получения экземпляра класса: для HL блоков
	 * @param $HlBlockId
	 * @return \Bitrix\Main\Entity\DataManager|bool
	 */
	static function GetEntityDataClass($HlBlockId)
	{
		Loader::includeModule("highloadblock");
		if (empty($HlBlockId) || $HlBlockId < 1) {
			return false;
		}
		$hlblock = HLBT::getById($HlBlockId)->fetch();
		$entity = HLBT::compileEntity($hlblock);
		$entityDataClass = $entity->getDataClass();
		return $entityDataClass;
	}


}