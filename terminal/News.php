<?php

namespace Terminal;

require_once('Common.php');

class News extends Common
{
    static $newsDomen = 'https://msal.ru';
    public static $nMaxName = 70; //максимальное кол символов в названии
    public static $nMaxPrev = 178; //максимальное кол символов в превью тексте

    static function startExNews()
    {
        $tempRes = self::getNewsJson();
        return self::updateNews($tempRes);
    }

    /**
     * @param bool $url
     * @return array|mixed
     * получаем новости с дальнего сайта
     */
    static function getNewsJson($url = false)
    {
        $result = [];
        if ($url === false) {
            $url = self::$newsDomen . '/stest.php';
        }
        $json = file_get_contents($url);
        if ($json) {
            $result = json_decode($json, true);
        }
        return $result;
    }

    /**
     * @param $tempRes
     * @return bool
     */
    static function updateNews($tempRes)
    {
        $result = false;
        self::setBitrixSystems();
        $Elem = new \CIBlockElement();
        if (is_array($tempRes['NEWS']) && !empty($tempRes['NEWS'])) {
            foreach ($tempRes['NEWS'] as &$news) {
                $news['IBLOCK_ID'] = ID_IBLOCK_NEWS;
                $news['IBLOCK_SECTION_ID'] = false;
                $news['DETAIL_TEXT'] = self::changeImgUrl(urldecode($news['DETAIL_TEXT']));
                $news['NAME'] = urldecode($news['NAME']);
                $news['PREVIEW_TEXT'] = urldecode($news['PREVIEW_TEXT']);
                if ($news['PREVIEW_PICTURE']['SRC']) {
                    $news['PREVIEW_PICTURE'] = \CFile::MakeFileArray(self::$newsDomen . $news['PREVIEW_PICTURE']['SRC']);
                }
                $news['CODE'] = $news['XML_ID'];
                if (!$Elem->Add($news)) {
//                    echo $Elem->LAST_ERROR;
                    continue;
                }

            }

            $result = true;
        }


        /*if (is_array($tempRes['EVENT']) && !empty($tempRes['EVENT']))
            foreach ($tempRes['EVENT'] as $event) {

            }*/
        return $result;
    }

    /**
     * @param $stNews
     * @return string
     * подставляем msal.ru к картинкам в тексте новостей
     */
    private static function changeImgUrl($stNews)
    {
        return strval(preg_replace('/"\/upload/', '"' . self::$newsDomen . '/upload/', $stNews));
    }

}