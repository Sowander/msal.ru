$(document).ready( function(){

$("#formmsf").submit(function(){

 	var form = $("#formmsf");
    var formdata = false;
    if (window.FormData){
        formdata = new FormData(form[0]);
    }

    var formAction = form.attr('action');
    $.ajax({
        url         : 'obrabotcik.php',
        data        : formdata ? formdata : form.serialize(),
        cache       : false,
        contentType : false,
        processData : false,
        type        : 'POST',
        success     : function(data, textStatus, jqXHR){
            console.log(data);
        }
    });
	return false;
});


// --------------- add-other-documents -------------------

$("#addotherdocs").click(function(event){
    addotherdocs();
    FileUploadInit();
    return false;
});

var index_m = 0;
function addotherdocs() {
    index_m = index_m + 1;
    index_n = index_m + 4;
    var div = $('<div/>', {
        'class' : 'OtherDocumentBlock'
    }).appendTo($('#other-documents'));
    var input = $('<input/>', {
        value : '',
        type : 'button',
        'class' : 'DeleteOtherDoc'
    }).appendTo(div);
    var label = $('<select onchange="GetSelndexOther(this);" name="otherdocuments-sel'+index_m+'" class="form-control other-documents" id="otherdocuments'+index_m+'"/>').appendTo(div);
    var select = $('.other-documents-types select>').clone().appendTo("#otherdocuments"+index_m+"");

    var dseriya = $('<div/>', {
        'class' : 'form-group col-md-3 series-otherdoc otherdocuments'+index_m+''
    }).html("<input class='form-control otherdoc-seriya-field' name='otherdoc-seriya"+index_m+"' id='otherdoc-seriya"+index_m+"' placeholder='Серия' data-msg='Введите серию' required/>").appendTo($(div));

    var dnumber = $('<div/>', {
        'class' : 'form-group col-md-3 number-otherdoc otherdocuments'+index_m+''
    }).html("<input class='form-control otherdoc-number-field' name='otherdoc-number"+index_m+"' id='otherdoc-number"+index_m+"' placeholder='Номер' data-msg='Введите номер' required/>").appendTo($(div));
    
    var ddate = $('<div/>', {
        'class' : 'form-group col-md-3 date-otherdoc otherdocuments'+index_m+''
    }).html("<input class='form-control otherdoc-date-field' name='otherdoc-date"+index_m+"' id='otherdoc-date"+index_m+"' placeholder='Дата' data-msg='Введите дату' required/>").appendTo($(div));
	
    var dfile = $('<div/>', {
        'class' : 'form-group col-md-12 input-file-container otherdocuments'+index_m+''
    }).html("<input class='input-file-"+index_n+"' id='my-file-"+index_n+"' name='other-document-"+index_n+
    "' type='file' accept='.pdf,.rtf,.xls,.xlsx,.doc,.docx'/><label for='my-file-"+index_n+"' class='input-file-trigger-3"+index_n+
    "'><i class='fas fa-upload'></i><span>загрузить файл...</span></label><p class='file-return-"+index_n+" file-result'></p>").appendTo($(div));

	$('input#otherdoc-date'+index_m+'').datepicker({
		language: "ru"
	});

    input.click(function() {
        $(this).parent().remove();
    });


}

//Для удаления первого поля
$('.DeleteOtherDoc').click(function(event) {
    $(this).parent().remove();
    return false;
});

// --------------- addfamilymember -------------------

$("#addfamilymember").click(function(event){
    addDynamicExtraField();
    return false;
});

var index_i = 0;
function addDynamicExtraField() {
    index_i = index_i + 1;
    var div = $('<div/>', {
        'class' : 'DynamicExtraField'
    }).appendTo($('#AddFamilyMemberContainer'));
    var input = $('<input/>', {
        value : '',
        type : 'button',
        'class' : 'DeleteDynamicExtraField'
    }).appendTo(div);
    var label = $('<select class="form-control maritalstatus" name="maritalstatus-sel'+index_i+'" id="maritalstatus'+index_i+'"/>').appendTo(div);
    var select = $('.maritalstatus-buffer select>').clone().appendTo("#maritalstatus"+index_i+"");

    var fio = $('<div/>', {
        'class' : 'form-group'
    }).html("<input class='form-control family-fio-field' name='family-fio"+index_i+"' id='family-FIO"+index_i+"' placeholder='ФИО' data-msg='Введите ФИО'/>").appendTo($(div));

    var phone = $('<div/>', {
        'class' : 'form-group'
    }).html("<input class='form-control family-phone-field' name='family-phone"+index_i+"' id='family-phone"+index_i+"' placeholder='Телефон' data-msg='Введите телефон'/>").appendTo($(div));

    var bdate = $('<div/>', {
        'class' : 'form-group'
    }).html("<input class='form-control family-bdate-field' name='family-bdate"+index_i+"' id='family-bdate"+index_i+"' placeholder='Дата рождения' data-msg='Укажите дату рождения'/>").appendTo($(div));

	$('input#family-bdate'+index_i+'').datepicker({
		language: "ru"
	});

    input.click(function() {
        $(this).parent().remove();
    });
}

//Для удаления первого поля
$('.DeleteDynamicExtraField').click(function(event) {
    $(this).parent().remove();
    return false;
});

// ---------------// addlanguage ---------------------

$("#addlanguage").click(function(event){
    addlanguage();
    return false;
});

var index_k = 0;
function addlanguage() {
    index_k = index_k + 1;
    var div = $('<div/>', {
        'class' : 'AdditionalLang'
    }).appendTo($('#AddLangContainer'));
    var input = $('<input/>', {
        value : '',
        type : 'button',
        'class' : 'deleteLang'
    }).appendTo(div);
    var label = $('<select class="form-control add-lang" name="addlang'+index_k+'" id="lang'+index_k+'"/>').appendTo(div);
    var select = $('.lang-buffer select>').clone().appendTo("#lang"+index_k+"");
    var checkmain = $("<input class='form-check-input' type='radio' value='addlang"+index_k+"' id='addlang"+index_k+"' name='mainlang' ><label class='form-check-label lang-check' for='addlang1'>основной</label>").appendTo(div);

    input.click(function() {
        $(this).parent().remove();
    });
}

//Для удаления первого поля
$('.deleteLang').click(function(event) {
    $(this).parent().remove();
    return false;
});

// ----------------- addlanguage ---------------------

// -------------// addfamilymember -------------------


//------------------------------------ upload file --------------------------------------------

	$("#fileuploader-1").uploadFile({
		url:"file",
		fileName:"myfile",
		multiple:false,
		maxFileCount:1,
		dragDropStr: "<span>или перетяни файл сюда</span>",
		uploadStr:"загрузить"
	});

	$("#fileuploader-2").uploadFile({
		url:"file",
		fileName:"myfile",
		multiple:false,
		maxFileCount:1,
		dragDropStr: "<span>или перетяни файл сюда</span>",
		uploadStr:"загрузить"
	});

	$("#fileuploader-3").uploadFile({
		url:"file",
		fileName:"myfile",
		multiple:false,
		maxFileCount:1,
		dragDropStr: "<span>или перетяни файл сюда</span>",
		uploadStr:"загрузить"
	});

	$("#fileuploader-4").uploadFile({
		url:"file",
		fileName:"myfile",
		multiple:false,
		maxFileCount:1,
		dragDropStr: "<span>или перетяни файл сюда</span>",
		uploadStr:"загрузить"
	});

	$("#fileuploader-5").uploadFile({
		url:"file",
		fileName:"myfile",
		multiple:false,
		maxFileCount:1,
		dragDropStr: "<span>или перетяни файл сюда</span>",
		uploadStr:"загрузить"
	});

//----------------------------------// upload file --------------------------------------------

// ------------------------------------- csv files ----------------------------------------------

	$.ajax({
	  url: 'documents/otherdocuments.csv',
	  dataType: 'text',	}).done(otherdocumentstypes);

	function otherdocumentstypes(data) {
	  var allRows = data.split(/\r?\n|\r/);
	  var select = '<select name="otherdocumentstypes" class="form-control">';
	  for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
	    var rowCells = allRows[singleRow].split(';');
	    if (singleRow !== 0 && allRows[singleRow] !='') {
	         select += '<option attr-othercode="'+rowCells[1]+'" attr-seriya="'+rowCells[2]+'" attr-number="'+rowCells[3]+'" attr-date="'+rowCells[4]+'">';
	         select += rowCells[0];
	         select += '</option>';
	    }
	  } 
	  select += '</select>';
	  $('.form-group.other-documents-types').append(select);
	}

	$.ajax({
	  url: 'documents/education.csv',
	  dataType: 'text',	}).done(education);

	function education(data) {
	  var allRows = data.split(/\r?\n|\r/);
	  var select = '<select name="education" class="inddocuments form-control">';
	  for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
	    var rowCells = allRows[singleRow].split(';');
	    if (singleRow !== 0 && allRows[singleRow] !='') {
	         select += '<option attr-educode="'+rowCells[1]+'"  attr-seriya="'+rowCells[2]+'" attr-number="'+rowCells[3]+'">';
	         select += rowCells[0];
	         select += '</option>';
	    }
	  } 
	  select += '</select>';
	  $('.form-group#education-doc').append(select);
	}

	$.ajax({
	  url: 'documents/documents.csv',
	  dataType: 'text',	}).done(inddocuments,inddocuments2,inddocuments3,inddocuments4);

	function inddocuments(data) {
	  var allRows = data.split(/\r?\n|\r/);
	  var select = '<select name="inddocuments_other" class="inddocuments form-control" onchange="GetSelndex(this);">';
	  for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
	    var rowCells = allRows[singleRow].split(';');
	    if (singleRow !== 0 && allRows[singleRow] !='') {
	         select += '<option attr-code="'+rowCells[1]+'"  attr-seriya="'+rowCells[2]+'" attr-number="'+rowCells[3]+'">';
	         select += rowCells[0];
	         select += '</option>';
	    }
	  } 
	  select += '</select>';
	  $('.form-group.inddocuments-step-1').append(select);
	}

	function inddocuments4(data) {
	  var allRows = data.split(/\r?\n|\r/);
	  var select = '<select name="inddocuments4" class="inddocuments form-control" onchange="GetSelndex4(this);">';
	  for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
	    var rowCells = allRows[singleRow].split(';');
	    if (singleRow !== 0 && allRows[singleRow] !='') {
	         select += '<option attr-code="'+rowCells[1]+'"  attr-seriya="'+rowCells[2]+'" attr-number="'+rowCells[3]+'">';
	         select += rowCells[0];
	         select += '</option>';
	    }
	  } 
	  select += '</select>';
	  $('.form-group#document-3-type').append(select);
	}

	function inddocuments2(data) {
	  var allRows = data.split(/\r?\n|\r/);
	  var select = '<select name="inddocuments2" class="inddocuments form-control" onchange="GetSelndex2(this);">';
	  for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
	    var rowCells = allRows[singleRow].split(';');
	    if (singleRow !== 0 && allRows[singleRow] !='') {
	         select += '<option attr-code="'+rowCells[1]+'"  attr-seriya="'+rowCells[2]+'" attr-number="'+rowCells[3]+'">';
	         select += rowCells[0];
	         select += '</option>';
	    }
	  } 
	  select += '</select>';
	  $('.form-group.inddocuments-step-2').append(select);
	}

	function inddocuments3(data) {
	  var allRows = data.split(/\r?\n|\r/);
	  var select = '<select name="inddocuments3" class="inddocuments form-control" onchange="GetSelndex3(this);">';
	  for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
	    var rowCells = allRows[singleRow].split(';');
	    if (singleRow !== 0 && allRows[singleRow] !='') {
	         select += '<option attr-code="'+rowCells[1]+'"  attr-seriya="'+rowCells[2]+'" attr-number="'+rowCells[3]+'">';
	         select += rowCells[0];
	         select += '</option>';
	    }
	  } 
	  select += '</select>';
	  $('.form-group.documenttype2').append(select);
	}

	$.ajax({
	  url: 'documents/countries.csv',
	  dataType: 'text',	}).done(countries);

	function countries(data) {
	  var allRows = data.split(/\r?\n|\r/);
	  var select = '<select name="countries-sel" class="form-control" onchange="">';
	  for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
	    var rowCells = allRows[singleRow].split(';');
	         select += '<option attr-countriescode="'+rowCells[1]+'">';
	         select += rowCells[0];
	         select += '</option>';
	  } 
	  select += '</select>';
	  
	  $('.form-group.сitizenship').append(select);
	}

	$.ajax({
	  url: 'documents/regions.csv',
	  dataType: 'text',	}).done(regions);

	function regions(data) {
	  var allRows = data.split(/\r?\n|\r/);
	  var select = '<select name="regions-sel" class="form-control" onchange="">';
	  for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
	    var rowCells = allRows[singleRow].split(';');
	         select += '<option attr-regionscode="'+rowCells[1]+'">';
	         select += rowCells[0];
	         select += '</option>';
	  } 
	  select += '</select>';

	  $('.form-group.regions').append(select);
	  $('.maritalstatus').append(select);
	}

	$.ajax({
	  url: 'documents/regions.csv',
	  dataType: 'text',	}).done(maritalstatus);

	function maritalstatus(data) {
	  var allRows = data.split(/\r?\n|\r/);
	  var select = '<select name="maritalstatus-sel" class="form-control" onchange="">';
	  for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
	    var rowCells = allRows[singleRow].split(';');
	         select += '<option attr-maritalcode="'+rowCells[1]+'">';
	         select += rowCells[0];
	         select += '</option>';
	  } 
	  select += '</select>';
	  $('.maritalstatus-buffer').append(select);
	}

	$.ajax({
	  url: 'documents/settlement.csv',
	  dataType: 'text',	}).done(settlement);

	function settlement(data) {
	  var allRows = data.split(/\r?\n|\r/);
	  var select = '<select name="settlement-sel" class="form-control" onchange="">';
	  for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
	    var rowCells = allRows[singleRow].split(';');
	         select += '<option attr-settlementcode="'+rowCells[1]+'">';
	         select += rowCells[0];
	         select += '</option>';
	  } 
	  select += '</select>';

	  $('.form-group.settlement').append(select);
	}

	$.ajax({
	  url: 'documents/languages.csv',
	  dataType: 'text',	}).done(languages);

	function languages(data) {
	  var allRows = data.split(/\r?\n|\r/);
	  var select = '<select name="languages-sel" class="form-control" onchange="">';
	  for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
	    var rowCells = allRows[singleRow].split(';');
	    if (singleRow !== 0 && allRows[singleRow] !='') {
	         select += '<option attr-languagescode="'+rowCells[0]+'">';
	         select += rowCells[1];
	         select += '</option>';
	    }
	  } 
	  select += '</select>';

	  $('.lang-buffer').append(select);
	}

// -----------------------------------// csv files ----------------------------------------------	

	$(".form-check-input").change(function(){
		if($(this).is(':checked')){
			$(".form-group."+$(this).attr("id")+" input").val("");
		}
    	$(".form-group."+$(this).attr("id")+"").toggle();
	});

	$("#documenttype2").change(function(){

    	$(".number-documenttype2").toggle();

		if ($('#documenttype2').is(':checked')){
	    	var SeriesFlagDoc2 = $(".documenttype2 .inddocuments :selected").attr("attr-seriya");    
		    if(SeriesFlagDoc2 == "Да"){
		    	$(".series-documenttype2.series").show();
		    	$(".series-documenttype2.series input").attr('required', true);
		    }   
		   	else{ 
		   		$(".series-documenttype2.series").hide();
		    	$(".series-documenttype2.series input").attr('required', false);
		    	$(".series-documenttype2.series input").val('');
		    }
		    
		}else{
			$(".series-documenttype2").hide();
		}

	});

	$('#birthdate').datepicker({
		language: "ru"
	});

	$('input#birthdate2').datepicker({
		language: "ru"
	});

	$('input#BirthDate').datepicker({
		language: "ru"
	});

	$('input#date-education-doc').datepicker({
		language: "ru"
	});

	$('input#otherdoc-date1').datepicker({
		language: "ru"
	});



// ------------------------------------- multiStepForm ----------------------------------------------

    function ViewModel() {
        var self = this;
        self.Name = ko.observable('');
        self.Email = ko.observable('');
        self.Details = ko.observable('');

        self.AdditionalDetails = ko.observable('');
        self.availableTypes = ko.observableArray(['New', 'Open', 'Closed']);
        self.chosenType = ko.observable('');

        self.availableCountries = ko.observableArray(['France', 'Germany', 'Spain', 'United States', 'Mexico']),
        self.chosenCountries = ko.observableArray([]) 

    }

    var viewModel = new ViewModel();

    ko.applyBindings(viewModel);

    $(document).on("msf:viewChanged", function(event, data){
        var progress = Math.round((data.completedSteps / data.totalSteps)*100);

        $(".progress-bar").css("width", progress + "%").attr('aria-valuenow', progress);   ;
    });

    $(".msf:first").multiStepForm({
        activeIndex: 0,
        hideBackButton : false,
        allowUnvalidatedStep : false,
        allowClickNavigation : false,
        validate: {}
    });

    
    $("#formmsf").validate({
    	rules: {
			email: {required: true,email: true},
			email2: {required: true,email: true},
			passportseries: {required: true},
			passportseries2: {required: true},
			passportseries3: {required: true},
			passportnumber: {required: true},			
			passportnumber2: {required: true},
			passportnumber3: {required: true},
			lastname:{required: true},
			lastname2:{required: true},
			firstname:{required: true},
			firstname2:{required: true},
			patronymic:{required: true},
			patronymic2:{required: true},
			birthdate:{required: true},
			registrationaddress:{required: true},
			contactphone:{required: true},
			sameresidenceaddress:{required: true},
			fio:{required: true},
			socialscience:{required: true},
			russian:{required: true},
			history:{required: true}
		},		
		messages: {
			email: "Введите корректный email",
			email2: "Введите корректный email",
			passportseries: "Введите серию документа",
			passportseries2: "Введите серию документа",
			passportseries3: "Введите серию документа",
			passportnumber: "Введите номер документа",
			passportnumber2: "Введите номер документа",
			passportnumber3: "Введите номер документа",
			lastname: "Введите фамилию",
			lastname2: "Введите фамилию",
			firstname: "Введите имя",
			firstname2: "Введите имя",
			patronymic: "Введите отчество",
			patronymic2: "Введите отчество",
			birthdate: "Введите дату рождения (возраст не менее 16 лет)",
			// birthdate2: "Введите дату рождения (возраст не менее 16 лет)",
			// birthplace: "Введите дату рождения (возраст не менее 16 лет)",
			registrationaddress: "Укажите адрес регистрации",
			contactphone: "Введите телефон",
			sameresidenceaddress: "Укажите адрес проживания",
			fio: "Введите ФИО" 
		}
    });


	$.validator.addClassRules("family-fio-field", {
		  	required: true
		}
	);

	$.validator.addClassRules("family-phone-field", {
		  	required: true
		}
	);

	$.validator.addClassRules("family-bdate-field", {
		  	required: true
		}
	);


// -----------------------------------// multiStepForm ----------------------------------------------

});

//--------------------------------------- show Series -----------------------------------------------

function GetSelndex(sel){

    var SeriesFlagStep1 = $(".inddocuments-step-1 .inddocuments :selected").attr("attr-seriya");    
    if(SeriesFlagStep1 == "Да"){
    	$(".series-step-1.series").show();
    	$(".series-step-1.series input").attr('required', true);
    }   
   	else{ 
   		$(".series-step-1.series").hide();
    	$(".series-step-1.series input").attr('required', false);
    	$(".series-step-1.series input").val('');
    }

    var SeriesFlagIndex = $(".inddocuments :selected").index()+1;
    $(".inddocuments-step-2 .inddocuments option:nth-child("+SeriesFlagIndex+")").attr("selected", "selected");

}

function GetSelndex2(sel2){
	var SeriesFlagStep2 = $(".inddocuments-step-2 .inddocuments :selected").attr("attr-seriya");    
    if(SeriesFlagStep2 == "Да"){
    	$(".series-step-2.series").show();
    	$(".series-step-2.series input").attr('required', true);
    }   
   	else{ 
   		$(".series-step-2.series").hide();
    	$(".series-step-2.series input").attr('required', false);
    	$(".series-step-2.series input").val('');
    }
}

function GetSelndex3(sel3){
    var SeriesFlagDoc2 = $(".documenttype2 .inddocuments :selected").attr("attr-seriya");    
    if(SeriesFlagDoc2 == "Да"){
    	$(".series-documenttype2.series").show();
    	$(".series-documenttype2.series input").attr('required', true);
    }   
   	else{ 
   		$(".series-documenttype2.series").hide();
    	$(".series-documenttype2.series input").attr('required', false);
    	$(".series-documenttype2.series input").val('');
    }
}

function GetSelndex4(sel4){
    var SeriesFlagDoc4 = $("#document-3-type .inddocuments :selected").attr("attr-seriya");    
    if(SeriesFlagDoc4 == "Да"){
    	$(".series-document-type.series").show();
    	$(".series-document-type.series input").attr('required', true);
    }   
   	else{ 
   		$(".series-document-type.series").hide();
    	$(".series-document-type.series input").attr('required', false);
    	$(".series-document-type.series input").val('');
    }
}

function GetSelndexOther(sel5){

	var ChangeSelctId = $(sel5).attr("id");

    var DocFlagSeries = $(".OtherDocumentBlock #"+ChangeSelctId+" :selected").attr("attr-seriya");  
    var NumberSeries = $(".OtherDocumentBlock #"+ChangeSelctId+" :selected").attr("attr-number");  
    var DateSeries = $(".OtherDocumentBlock #"+ChangeSelctId+" :selected").attr("attr-date");  

	console.log(ChangeSelctId);
    
    if(DocFlagSeries == "Да"){
    	$(".series-otherdoc."+ChangeSelctId+"").show();
    	$(".series-otherdoc."+ChangeSelctId+" input").attr('required', true);
    }   
   	else{ 
   		$(".series-otherdoc."+ChangeSelctId+"").hide();
    	$(".series-otherdoc."+ChangeSelctId+" input").attr('required', false);
    	$(".series-otherdoc."+ChangeSelctId+" input").val('');
    } 
    
    if(NumberSeries == "Да"){
    	$(".number-otherdoc."+ChangeSelctId+"").show();
    	$(".number-otherdoc."+ChangeSelctId+" input").attr('required', true);
    }   
   	else{ 
   		$(".number-otherdoc."+ChangeSelctId+"").hide();
    	$(".number-otherdoc."+ChangeSelctId+" input").attr('required', false);
    	$(".number-otherdoc."+ChangeSelctId+" input").val('');
    } 

    if(DateSeries == "Да"){
    	$(".date-otherdoc."+ChangeSelctId+"").show();
    	$(".date-otherdoc."+ChangeSelctId+" input").attr('required', true);
    }   
   	else{ 
   		$(".date-otherdoc."+ChangeSelctId+"").hide();
    	$(".date-otherdoc."+ChangeSelctId+" input").attr('required', false);
    	$(".date-otherdoc."+ChangeSelctId+" input").val('');
    }
}

//-------------------------------------// show Series -----------------------------------------------

$("#PassportSeries").keyup(function(){$("#PassportSeries2").val($("#PassportSeries").val());});
$("#PassportNumber").keyup(function(){$("#PassportNumber2").val($("#PassportNumber").val());});
$("#patronymic").keyup(function(){$("#patronymic2").val($("#patronymic").val());});
$("#firstname").keyup(function(){$("#firstname2").val($("#firstname").val());});
$("#lastname").keyup(function(){$("#lastname2").val($("#lastname").val());});
$("#birthdate").change(function(){$("#birthdate2").val($("#birthdate").val());});
$("#birthdate2").change(function(){$("#birthdate").val($("#birthdate2").val());});

// ---------------- addstatement ---------------------

$("#addstatement").click(function(){
	$("#addstatement_form").show("fast");
});

// --------------// addstatement ---------------------

$(".msf-nav-button").click(function(){
	if($(".gender .custom-control-input").hasClass("valid")){
		$(".gender span.error").hide();
	}	
});

$("#birthdate,#birthdate2").change(function(){

	var userAge = getAge($(this).val());
	$(".form-date .hidden-date").val(userAge);

	if (userAge >= 16){
		$(".form-date .hide-message").hide();
		$(".message-parent").show();
	}
	if (userAge < 16){
		$("#birthdate").val("");
		$("#birthdate2").val("");
		$(".form-date .hide-message").show();
	}
	if (userAge > 18){
		$(".message-parent").hide();
	}
});

// дата рождения в формате '22.05.1990'

function getAge(dateString) {
  var day = parseInt(dateString.substring(0,2));
  var month = parseInt(dateString.substring(3,5));
  var year = parseInt(dateString.substring(6,10));

  var today = new Date();
  var birthDate = new Date(year, month - 1, day); // 'month - 1' т.к. нумерация месяцев начинается с 0 
  var age = today.getFullYear() - birthDate.getFullYear();
  var m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) { 
      age--;
  }
  return age;
}

// -------------------- ege ----------------------

$("#ege_chek").click(function(){
	$("#ege-group").toggle("fast");
});

// ------------------// ege ----------------------


function FileUploadInit(){
	
	document.querySelector("html").classList.add('js');	

	var fileInput = new Array();      
	var button = new Array();      
	var the_return = new Array();      

	var CountBlock = $("#upload-documents .input-file-container").length;
	for(var st = 0; st < CountBlock; st++){
		fileInput[st]  = document.querySelector( ".input-file-"+st+"" ),  

	    button[st] = document.querySelector( ".input-file-trigger-"+st+"" ),
	    the_return[st] = document.querySelector(".file-return-"+st+"");


		// button[st].addEventListener( "keydown", function( event ) {  
		//     if ( event.keyCode == 13 || event.keyCode == 32 ) {  
		//         fileInpu[st].focus();  
		//     }  
		// }); 
		
		$(".input-file-"+st+"").change(function(){
			var this_val = $(this).val();
			var this_id = $(this).attr("id");
			if(this_val){			
				$(this).siblings(".file-result").text("Файл: "+this_val+" загружен");
				$(this).siblings("label").children("span").text("Изменить");
			}else{
				$(this).siblings(".file-result").text("");
				$(this).siblings("label").children("span").text("загрузить файл...");
			}
		});

	}
}

FileUploadInit();
