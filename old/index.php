<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Старый Личный кабинет");


?>
<h3 style="font-size: 30px">Старый Личный кабинет</h3>
<?
global $USER;
$isPrepod = in_array(ID_PREPOD_GROUP, $USER->GetUserGroupArray());
if($isPrepod){
    $APPLICATION->IncludeComponent(
        "glab:main.profile.prepodpagin",
        "",
        Array(
            "CHECK_RIGHTS" => "N",
            "SEND_INFO" => "N",
            "SET_TITLE" => "Y",
            "USER_PROPERTY" => array(),
            "USER_PROPERTY_NAME" => "",
            "CACHE_TIME" => 3600
        )
    );
}
?>
<style>
    .form-loader {
        display: block;
    }
</style>
<script>
    $(document).ready(function ($) {
        $(window).load(function () {
            $('.form-loader').fadeOut('slow', function () {});

        });
    });
</script>


<script>
    $('[name="form_auth"]').submit(function () {
        $('.form-loader').fadeIn(300);
    });


    <?if($_GET[t]){?>
    $(".t<?=$_GET[t]?>").trigger('click');
    <?}?>

</script>
<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
