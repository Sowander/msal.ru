<?
/*
 
Метод:
ToGetMainData
Тестовая публикация:
http://1cservices/priem_2017_test/ws/maindata.1cws?wsdl

Логин – Avrobus
Пароль – avrobus
 
 
 * */
include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
if (!function_exists('is_soap_fault')) {
    print 'Не настроен web сервер. Не найден модуль php-soap.';
    return false;
}
ini_set('soap.wsdl_cache_enabled', 0 );
ini_set('soap.wsdl_cache_ttl', 0);

try {
    //1// $client = new SoapClient('http://1cservices/priem_2017_test/ws/maindata.1cws?wsdl',
	/* 2 */// $client = new SoapClient('http://1cservices/priem_2017_test/ws/wsabiturs.1cws?wsdl',
	/* 3 $client = new SoapClient('http://1cservices/UV/ws/wsportfolio.1cws?wsdl',*/
	 $client = new SoapClient('http://1cservices/UV/ws/wsportfoliostud.1cws?wsdl',
	
        array(
		//"login" => "Avrobus",
         //   "password" => "avrobus",
		"login" => "webservices",
            "password" => "webTest17",		 
		 
            'soap_version' => SOAP_1_2,
            'trace' => true,
            'exceptions' => true,
            'cache_wsdl' => WSDL_CACHE_NONE
        )
    );
} catch (SoapFault $e) {
    trigger_error('Ошибка подключения или внутренняя ошибка сервера. Не удалось связаться с базой 1С.', E_ERROR);
    var_dump($e);
}
if (is_soap_fault($client)){
    trigger_error('Ошибка подключения или внутренняя ошибка сервера. Не удалось связаться с базой 1С.', E_ERROR);
    return false;
}
//$time = date("c", time());
//printr($time);

/*
$params = array('Студент'=>'9e785092-7d10-11e7-810e-005056b32020');
$params2 = array('НаДату'=>'');
$params3 = array();*/

//$params = array('Student' => array( 0 => array ("Name"=>"Ivan","Family"=>"Женат") ));

$params = array( 'Students' => array('Student' => array( 0 => array ("ID"=>"901","Name"=>"Ivan","Family"=>"Женат","ID"=>"",'Surname'=>"Ivanov1",'DateOfBirth'=>'1980-01-01','Sex'=>'1','PlaceOfBirth'=>'',
'Patriality'=>'',
'IndexR'=>'',
'RegionR'=>"",
"CityR"=>"",
"LocalityR"=>"",
"StreetR"=>"",
"HouseR"=>"",
"HousingR"=>"",
"FlatR"=>"",
"IndexL"=>"",
"RegionL"=>"",
"CityL"=>"",
"LocalityL"=>"",
"StreetL"=>"",
"HouseL"=>"",
"HousingL"=>"",
"FlatL"=>"",
"TelContact"=>"",
"TelHome"=>"",
"TelWork"=>"",
"Email"=>"",
"NeedHostel"=>"",
"FileStatement"=>"",
"FilePassport"=>"",
"FileCertificate"=>"",
"FileFoto"=>"",
"Ref"=>"",
"FamilyMembers"=>array(0=>array("ID"=>"","RelationType"=>"","FIO"=>"",'DateOfBirth'=>'1980-01-01',"WorkPlace"=>"","Tel"=>"","Address"=>"")),
"Requests"=>array(0=>array("ID"=>"","Level"=>"","Specialty"=>"",'Request'=>'',"RequestPath"=>"")),
"FilePassportPath"=>"",
"FileCertificatePath"=>"",
"FileFotoPath"=>"",
"DocType"=>"",
"Series"=>"",
"Number"=>"",
"DateOfIssue"=>"",
"Department"=>"",
"DepartmentCode"=>"",
"AttachedDocuments"=>array(0=>array("ID"=>"","Code"=>"","Serial"=>"",'Number'=>'','Date'=>'1980-01-01')),
) )));

// 1
//ответ object(stdClass)#123 (1) { ["return"]=> object(stdClass)#125 (2) { ["ErrorMessage"]=> string(0) "" ["SavedID"]=> string(8) "000 901" } }
#$result = $client->ToGetMainData($params);
#echo "<pre>";
#print_r($result);

//2
#$params = array("ID"=>901);
#$result = $client->ПолучитьСписокАбитуриентов($params);
#echo "<pre>";
#print_r($result);

//3 ПолучитьСправочникПортфолио
#$result = $client->ПолучитьСправочникПортфолио();
#echo "<pre>";
#print_r($result);

//4 ПолучитьСправочникПортфолио
$params = array('НомерСтуденческого'=>'0103177');
$result = $client->ПолучитьСписокСтудентов($params);
echo "<pre>";
print_r($result);

?>