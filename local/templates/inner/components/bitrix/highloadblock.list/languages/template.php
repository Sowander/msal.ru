<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
?>
<li class="langs">
<?foreach($arResult["rows"] as $lang):?>
	<a<?if($lang["UF_LANG_ID"] == LANGUAGE_ID):?> class="active"<?endif;?>
        href="<?=$lang["UF_LINK"]?>"
        >
        <?=$lang["UF_NAME"]?>
    </a>
<?endforeach;?>
</li>