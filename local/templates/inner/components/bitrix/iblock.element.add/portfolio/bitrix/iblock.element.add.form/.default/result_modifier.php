<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
foreach ($arResult["ELEMENT_FILES"] as $key => $value) {
	if($value["IS_IMAGE"]){
		$file = CFile::ResizeImageGet($value['ID'], array('width'=>150, 'height'=>150), BX_RESIZE_IMAGE_PROPORTIONAL_ALT , true);
		$arResult["ELEMENT_FILES"][$key]["SRC"] = $file["src"];
	}
}

				    