<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
if (count($arResult["ELEMENTS"]) > 0){
	CModule::IncludeModule('iblock');
	foreach ($arResult["ELEMENTS"] as $key=>$arElement){
		//if($arElement["XML_ID"]!="" && $arElement["IBLOCK_CODE"]=="personaloffice"){
			$arSelect = Array("ID", "NAME", "PROPERTY_DATE","PROPERTY_CATEGORY","PROPERTY_INCATEGORY");
			$arFilter = Array("IBLOCK_ID" => $arElement["IBLOCK_ID"], "ACTIVE"=>"Y","=ID"=>$arElement["ID"]);
			$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
			while($ob = $res->GetNextElement())
			{
				$arFields = $ob->GetFields();  
					$LINKS = array();
				    $reslink = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement["XML_ID"], "sort", "asc", array("CODE" => "LINKS"));
				    while ($oblink = $reslink->GetNext())
				    {
				        $LINKS[] = "<a href='".$oblink['VALUE']."' target='_blank'>".$oblink['VALUE']."</a>";
				    }
					$FILES = array();
				    $resfile = CIBlockElement::GetProperty($arElement["IBLOCK_ID"], $arElement["XML_ID"], "sort", "asc", array("CODE" => "FILES"));
				    while ($obfile = $resfile->GetNext())
				    {
				    	$rsFile = CFile::GetByID($obfile['VALUE']);
				    	$arFile = $rsFile->Fetch();
				    	$arFile["PATH"]=CFile::GetPath($obfile['VALUE']);
				    	$arFile["SMALL"]="";
				    	if (strpos($arFile["CONTENT_TYPE"], "image") !== false){
					    	$file = CFile::ResizeImageGet($obfile['VALUE'], array('width'=>150, 'height'=>150), BX_RESIZE_IMAGE_PROPORTIONAL_ALT , true);
					    	$arFile["SMALL"]=$file["src"];
					    }
				        $FILES[] = $arFile;
				    }
				    $arResult["ELEMENTS"][$key]["FILES"]=$FILES;
				    $arResult["ELEMENTS"][$key]["LINKS"]=$LINKS;
				    $arResult["ELEMENTS"][$key]["TYPE"]=$arFields["PROPERTY_CATEGORY_VALUE"]."&nbsp;/&nbsp;".$arFields["PROPERTY_INCATEGORY_VALUE"];
				    $arResult["ELEMENTS"][$key]["DATE_PROP"]=$arFields["PROPERTY_DATE_VALUE"];
			}//*/
		//}
	}
}
