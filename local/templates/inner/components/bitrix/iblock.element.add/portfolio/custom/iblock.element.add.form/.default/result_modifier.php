<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
foreach ($arResult["ELEMENT_FILES"] as $key => $value) {
	if($value["IS_IMAGE"]){
		$file = CFile::ResizeImageGet($value['ID'], array('width'=>150, 'height'=>150), BX_RESIZE_IMAGE_PROPORTIONAL_ALT , true);
		$arResult["ELEMENT_FILES"][$key]["SRC"] = $file["src"];
	}
}
$arResult["category"]=array(
	0=>"Категория 1",
	1=>"Категория 2",
	2=>"Категория 3",
	3=>"Категория 4",
);
$arResult["innercategory"]=array(
	"0" => array(
		"подкатегория 1_1",
		"подкатегория 1_2",
		"подкатегория 1_3",
		"подкатегория 1_4",
		"подкатегория 1_5"
	),
	"1" => array(
		"подкатегория 2_1",
		"подкатегория 2_2",
		"подкатегория 2_3",
		"подкатегория 2_4",
		"подкатегория 2_5"
	),
	"2" => array(
		"подкатегория 3_1",
		"подкатегория 3_2",
		"подкатегория 3_3",
		"подкатегория 3_4",
		"подкатегория 3_5"
	),
	"3" => array(
		"подкатегория 4_1",
		"подкатегория 4_2",
		"подкатегория 4_3",
		"подкатегория 4_4",
		"подкатегория 4_5"
	),
);
				    