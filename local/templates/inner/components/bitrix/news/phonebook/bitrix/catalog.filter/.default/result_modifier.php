<?php
/**
 * (c) Bit24, http://studiobit.ru
 * User: Bondarenko Georg
 * Date: 10.08.2016 13:51
 */

$structureIblockId = 3;
$arStructureParentSectionIds = array(12, 11, 10, 13, 33, 448, 39);

if($GLOBALS[$arParams["FILTER_NAME"]]["?NAME"]){
	$name = $GLOBALS[$arParams["FILTER_NAME"]]["?NAME"];
	$GLOBALS[$arParams["FILTER_NAME"]] = array(
		array(
			"LOGIC" => "OR",
			array("PROPERTY" => array("?POSITION"=>$name)),
			array("?NAME" => $name)
		)
	);
}
if(intval($_GET["DEPARTMENT"]) > 0)
{
	$GLOBALS[$arParams["FILTER_NAME"]]["PROPERTY"]["?DEPARTMENT"] = intval($_GET["DEPARTMENT"]);
}

/** Структура */
$arResult['SECTIONS'] = array();
if(\Bitrix\Main\Loader::includeModule("iblock"))
{
	$rsSections = CIBlockSection::GetList(
		array("SORT"=>"ASC"),
		array(
			"IBLOCK_ID" =>  $structureIblockId,
			"ACTIVE"    =>  "Y",
			"SECTION_ID" => $arStructureParentSectionIds
		),
		false,
		array("ID", "IBLOCK_ID", "SECTION_ID", "UF_NAME_RU"),
		false
	);
	unset($arParentSectionsFilter);
	while($arSection = $rsSections->fetch())
	{
		$arResult['SECTIONS'][] = array(
			"NAME"  =>  $arSection["UF_NAME_RU"],
			"URL"   =>  $APPLICATION->GetCurPageParam("DEPARTMENT=".$arSection["ID"], array("DEPARTMENT")),
			"ID"    =>  $arSection["ID"]
		);
	}
}