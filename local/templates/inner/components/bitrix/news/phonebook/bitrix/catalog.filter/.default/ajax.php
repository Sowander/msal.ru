<?php
/**
 * (c) Bit24, http://studiobit.ru
 * User: Bondarenko Georg
 * Date: 10.08.2016 14:31
 */
define("NO_KEEP_STATISTIC", true);
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");
define("AJAX_IBLOCK_ID", 16);
define("AJAX_SITE_DIR", "");

$arElements = array();
if(strlen($_REQUEST["NAME"]) > 3)
{
	$arFilter = array();
	if($_REQUEST["NAME"])
	{
		$name = $_REQUEST["NAME"];
		$arFilter = array(
			"IBLOCK_ID" =>  AJAX_IBLOCK_ID,
			array(
				"LOGIC" => "OR",
				array("?PROPERTY_POSITION"=>$name),
				array("?NAME" => $name)
			)
		);
		if(intval($_REQUEST["DEPARTMENT"]) > 0)
		{
			$arFilter["?PROPERTY_DEPARTMENT"] = intval($_REQUEST["DEPARTMENT"]);
		}
		if(\Bitrix\Main\Loader::includeModule("iblock"))
		{
			$rsElements = CIBlockElement::GetList(
					array("SORT"=>"ASC"),
					$arFilter,
					false,
					false,
					array("ID", "NAME", "PROPERTY_DEPARTMENT", "PROPERTY_POSITION", "DETAIL_PAGE_URL")
				);
			while($arElement = $rsElements->fetch())
			{
				$arElements[] = $arElement;
			}
		}
	}
}

if(!empty($arElements))
{
	?>
	<ul>
		<?foreach($arElements as $arElement):?>
			<li>
				<a href="<?=str_replace(array("#SITE_DIR#", "#ELEMENT_ID#"), array(AJAX_SITE_DIR, $arElement["ID"]), $arElement["DETAIL_PAGE_URL"])?>">
					<span class="fio"><?=$arElement["PROPERTY_POSITION_VALUE"]?> <?=$arElement["NAME"]?></span>
				</a>
			</li>
		<?endforeach;?>
	</ul>
	<?
}

die();