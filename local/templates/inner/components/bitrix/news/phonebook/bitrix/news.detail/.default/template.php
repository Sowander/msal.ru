<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? echo "<style>.left_sidebar{display:none;} #employers.inner_page_content{width: 775px;} .inner_page_content table.working-boss tbody tr:nth-child(1n) td:nth-child(1) {
    padding: .2em .5em 0.2em .5em;   vertical-align: middle;    font-weight: 700;}</style>"; ?>

<div id="employers" class="inner_page_content">
	<div class="square_container">
		<table class="working-boss" style="border: none; margin-bottom: 15px; background: white;" id="<?=$this->GetEditAreaId($arResult['ID']);?>">
			<tbody>
			<tr>
				<td style="text-align: center; vertical-align: middle;" rowspan="<?=count($arResult["DISPLAY_PROPERTIES"])+3?>" width="100px">
					<strong>
						<img
							style="width: 100px;"
							src="<?if($arResult["PREVIEW_PICTURE"]["SRC"]):?><?= $arResult["PREVIEW_PICTURE"]["SRC"]?><?else:?>/upload/main/6cc/6cc619c57347976c1f5071274b62adb9.jpg<?endif;?>"
						>
					</strong>
				</td>
				<td style="background: #800000; padding: .5em;" colspan="3">
					<span style="color: white; ">
							<strong><?=$arResult["DISPLAY_PROPERTIES"]["POSITION"]["DISPLAY_VALUE"]?></strong>
					</span>
				</td>
			</tr>
			<tr>
				<td class="name" style="vertical-align: middle; font-size: 20px; height: 30px;" colspan="3">
					<?= $arResult["NAME"]?>
				</td>
			</tr>
			<?foreach($arResult["DISPLAY_PROPERTIES"] as $pid=> $arProperty):?>
				<tr>
					<td><?=$arProperty["NAME"]?>:</td>
					<td>
						<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
							<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
						<?else:?>
							<?=$arProperty["DISPLAY_VALUE"];?>
						<?endif?>
					</td>
				</tr>
			<?endforeach;?>
			<tr>
				<td colspan="2" style="text-align:right">
					<a href="<?=$arParams["BACK_URL"]?>">
						<?=$arParams["BACK_URL_NAME_LINK"]?>
					</a>
				</td>
			</tr>
			</tbody>
		</table>
	</div>
</div>