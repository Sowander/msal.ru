<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
use Bitrix\Main\Page\Asset;
?>
<?Asset::getInstance()->addJs("/content/online/js/jquery-ui.js");?>
<?Asset::getInstance()->addJs("/content/online/js/chosen.jquery.min.js");?>
<?Asset::getInstance()->addCss("/bitrix/templates/furniture_pale-blue/css/chosen.css"); ?>
<script>
	$(".niceSel").chosen({disable_search_threshold: 2000});
</script>
<div class="big_search_block clearfix">
    <form name="<?echo $arResult["FILTER_NAME"]."_form"?>" action="<?echo $arResult["FORM_ACTION"]?>" method="get">
	    <?foreach($arResult["ITEMS"] as $arItem):
		    if(array_key_exists("HIDDEN", $arItem)):
			    echo $arItem["INPUT"];
		    endif;
	    endforeach;?>
		<div class="input_container search_dropdown">
			<input class="search" type="text"
			       name="arrFilter_ff[NAME]"
			       placeholder="<?=Bitrix\Main\Localization\Loc::getMessage("CUESL_FILTER_NAME_PLACEHOLDER")?>" id=""
			       value="<?if($arResult["ITEMS"]["NAME"]["INPUT_VALUE"]):?><?=$arResult["ITEMS"]["NAME"]["INPUT_VALUE"]?><?endif;?>"
			       onkeyup="BX.ajax.insertToNode('<?=$this->GetFolder()?>/ajax.php?ID=custom_employee_search_result&IS_IE11='+BX.browser.IsIE11()+'&AJAX=Y&DEPARTMENT=<?=intval($_REQUEST["DEPARTMENT"])?>&NAME='+this.value,BX('custom_employee_search_result'));return false;"
			>
			<button type="submit" name="set_filter" class="fa fa-search" value="<?=GetMessage("IBLOCK_SET_FILTER")?>" ></button>
			<div class="dropdown" id="custom_employee_search_result"><?=Bitrix\Main\Localization\Loc::getMessage("CUESL_FILTER_AJAX_NO_RESULTS")?></div>
		</div>
    </form>
</div>


<?if(count($arResult['SECTIONS']) > 0):?>
	<div class="right_sidebar">
		<div class="categories">
			<div class="info_title">
				<?=Bitrix\Main\Localization\Loc::getMessage("CUESL_RIGHT_MENU_TITLE")?>
			</div>
			<form method="get">
				<div class="grSelect">
					<select name="DEPARTMENT" class="width-100 program_name niceSel req  peru" name="personal-edulevel" id="personal-edulevel">
						<option value="">
							<?=Bitrix\Main\Localization\Loc::getMessage("CUESL_RIGHT_MENU_ALL")?>
						</option>
						<?foreach ($arResult['SECTIONS'] as $arSection):?>
							<option
								value="<?=$arSection["ID"];?>"
								<?if($GLOBALS[$arParams["FILTER_NAME"]]["PROPERTY"]["?DEPARTMENT"] == intval($arSection["ID"])):?>selected="selected"<?endif;?>
							>
								<?=$arSection["NAME"];?>
							</option>
						<?endforeach;?>
					</select>
				</div>
				<div class="red_button low">
					<button type="submit"><?=\Bitrix\Main\Localization\Loc::getMessage("CUESL_FILTER_SHOW_BUTTON")?></button>
				</div>
			</form>
		</div>
	</div>
<?endif;?>