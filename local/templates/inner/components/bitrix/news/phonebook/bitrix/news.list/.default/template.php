<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>

<? echo "<style>.left_sidebar{display:none;} #employers.inner_page_content{width: 775px;} .inner_page_content table.working-boss tbody tr:nth-child(1n) td:nth-child(1) {
    padding: .2em .5em 0.2em .5em;   vertical-align: middle;    font-weight: 700;}</style>"; ?>


<?if(!empty($arResult["ITEMS"])):?>
<div id="employers" class="inner_page_content">
	<div>
		<h2><?=$arParams['SECTION_NAME']?></h2>
	</div>
	<hr/>
	<div class="square_container">
		<?foreach($arResult["ITEMS"] as $arItem):?>
			<?
			$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
			$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
			?>
			<table class="working-boss" style="border: none; margin-bottom: 15px; background: white;" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
				<tbody>
				<tr>
					<td style="text-align: center; vertical-align: middle;" rowspan="<?=count($arItem["DISPLAY_PROPERTIES"])+3?>" width="100px">
						<strong>
							<img
								style="width: 100px;"
								src="<?if($arItem["PREVIEW_PICTURE"]["SRC"]):?><?=$arItem["PREVIEW_PICTURE"]["SRC"]?><?else:?>/upload/main/6cc/6cc619c57347976c1f5071274b62adb9.jpg<?endif;?>"
							>
						</strong>
					</td>
					<td style="background: #800000; padding: .5em;" colspan="3">
						<span style="color: white; ">
							<strong><?=$arItem["DISPLAY_PROPERTIES"]["POSITION"]["DISPLAY_VALUE"]?></strong>
						</span>
					</td>
				</tr>
				<tr>
					<td class="name" style="vertical-align: middle; font-size: 20px; height: 30px;" colspan="3">
						<?=$arItem["NAME"]?>
					</td>
				</tr>
				<?foreach($arItem["DISPLAY_PROPERTIES"] as $pid=>$arProperty):?>
					<tr>
						<td><?=$arProperty["NAME"]?>:</td>
						<td>
							<?if(is_array($arProperty["DISPLAY_VALUE"])):?>
								<?=implode("&nbsp;/&nbsp;", $arProperty["DISPLAY_VALUE"]);?>
							<?else:?>
								<?=$arProperty["DISPLAY_VALUE"];?>
							<?endif?>
						</td>
					</tr>
				<?endforeach;?>
				<tr>
					<td colspan="2" style="text-align:right">
						<a href="<?=$arItem["DETAIL_PAGE_URL"]?>">
							Перейти к полному профилю
						</a>
					</td>
				</tr>
				</tbody>
			</table>
		<?endforeach;?>
	</div>
	<?if($arParams["DISPLAY_BOTTOM_PAGER"]):?>
		<br /><?=$arResult["NAV_STRING"]?>
	<?endif;?>
</div>
<?endif;?>