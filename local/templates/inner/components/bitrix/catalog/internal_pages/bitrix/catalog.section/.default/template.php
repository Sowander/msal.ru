<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?
/** Filter fix by sections with need to filter by target group */
if(isset($arResult['ITEMS'][0]["PROPERTIES"]["UF_TARGET_GROUP"]["VALUE"]) && $arResult['ITEMS'][0]["PROPERTIES"]["UF_TARGET_GROUP"]["VALUE"] && !empty($arResult['ITEMS'][0]["PROPERTIES"]["UF_TARGET_GROUP"]["VALUE"]))
{
    $GLOBALS["arNewsAndEventsPreFilter"]["PROPERTY_UF_TARGET_GROUP"] = $arResult['ITEMS'][0]["PROPERTIES"]["UF_TARGET_GROUP"]["VALUE"];
}
/**
 * Check show blocks
*/

?>
<?if($GLOBALS["UF_L_R_B_NOT_SHOW"]):?>
	<?if($GLOBALS["NOT_SHOW_EVENTS_ON_THIS_SECTION"] || $GLOBALS["NOT_SHOW_NEWS_ON_THIS_SECTION"] || $GLOBALS["NOT_SHOW_IMP_INFO_ON_THIS_SECTION"]):?>
		<?if($GLOBALS["UF_NEWS_SHOW_IN"]=="8" || $GLOBALS["UF_EVENT_SHOW_IN"]=="6" || $GLOBALS["UF_IMP_INFO_SHOW_IN"]=="10"):?>
		    <div class="right_sidebar">
		        <?if($GLOBALS["NOT_SHOW_NEWS_ON_THIS_SECTION"] && $GLOBALS["UF_NEWS_SHOW_IN"]=="8"):?>
		            <?$APPLICATION->IncludeComponent(
		            "bitrix:news.list",
		            "little_events",
		            array(
		                "DISPLAY_DATE" => "N",
		                "DISPLAY_NAME" => "Y",
		                "DISPLAY_PICTURE" => "N",
		                "DISPLAY_PREVIEW_TEXT" => "N",
		                "AJAX_MODE" => "Y",
		                "IBLOCK_TYPE" => "content",
		                "IBLOCK_ID" => "8",
		                "NEWS_COUNT" => "2",
		                "SORT_BY1" => "ACTIVE_FROM",
		                "SORT_ORDER1" => "DESC",
		                "SORT_BY2" => "SORT",
		                "SORT_ORDER2" => "ASC",
		                "FILTER_NAME" => "arNewsAndEventsPreFilter",
		                "FIELD_CODE" => array(
		                    0 => "ID",
		                    1 => "CODE",
		                    2 => "SORT",
		                    3 => "PREVIEW_TEXT",
		                    4 => "DATE_ACTIVE_FROM",
		                    5 => "ACTIVE_FROM",
		                    6 => "DATE_ACTIVE_TO",
		                    7 => "ACTIVE_TO",
		                    8 => "DATE_CREATE",
		                    9 => "CREATED_USER_NAME",
		                    10 => "",
		                ),
		                "PROPERTY_CODE" => array(
		                    0 => "UF_AUTOR",
		                    1 => "UF_LANGUAGE",
		                    2 => "UF_IS_IMPORTANT",
		                    3 => "UF_TARGET_GROUP",
		                ),
		                "CHECK_DATES" => "Y",
		                "DETAIL_URL" => "",
		                "PREVIEW_TRUNCATE_LEN" => "",
		                "ACTIVE_DATE_FORMAT" => "d.m.Y",
		                "SET_TITLE" => "N",
		                "SET_BROWSER_TITLE" => "N",
		                "SET_META_KEYWORDS" => "Y",
		                "SET_META_DESCRIPTION" => "Y",
		                "SET_LAST_MODIFIED" => "Y",
		                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		                "ADD_SECTIONS_CHAIN" => "N",
		                "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
		                "PARENT_SECTION" => "",
		                "PARENT_SECTION_CODE" => "",
		                "INCLUDE_SUBSECTIONS" => "N",
		                "CACHE_TYPE" => "N",
		                "CACHE_TIME" => "3600",
		                "CACHE_FILTER" => "Y",
		                "CACHE_GROUPS" => "Y",
		                "DISPLAY_TOP_PAGER" => "N",
		                "DISPLAY_BOTTOM_PAGER" => "N",
		                "PAGER_TITLE" => "Новости",
		                "PAGER_SHOW_ALWAYS" => "N",
		                "PAGER_TEMPLATE" => "",
		                "PAGER_DESC_NUMBERING" => "Y",
		                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		                "PAGER_SHOW_ALL" => "N",
		                "PAGER_BASE_LINK_ENABLE" => "N",
		                "SET_STATUS_404" => "N",
		                "SHOW_404" => "N",
		                "MESSAGE_404" => "",
		                "PAGER_BASE_LINK" => "",
		                "PAGER_PARAMS_NAME" => "arrPager",
		                "AJAX_OPTION_JUMP" => "N",
		                "AJAX_OPTION_STYLE" => "Y",
		                "AJAX_OPTION_HISTORY" => "N",
		                "AJAX_OPTION_ADDITIONAL" => "",
		                "COMPONENT_TEMPLATE" => "little_events"
		            ),
		            false
		        );?>
		        <?endif;?>
		        <?if($GLOBALS["NOT_SHOW_EVENTS_ON_THIS_SECTION"] && $GLOBALS["UF_EVENT_SHOW_IN"]=="6"):?>
		            <?$APPLICATION->IncludeComponent(
		            "bitrix:news.list",
		            "little_events",
		            array(
		                "DISPLAY_DATE" => "N",
		                "DISPLAY_NAME" => "Y",
		                "DISPLAY_PICTURE" => "N",
		                "DISPLAY_PREVIEW_TEXT" => "N",
		                "AJAX_MODE" => "Y",
		                "IBLOCK_TYPE" => "content",
		                "IBLOCK_ID" => "6",
		                "NEWS_COUNT" => "20",
		                "SORT_BY1" => "ACTIVE_FROM",
		                "SORT_ORDER1" => "DESC",
		                "SORT_BY2" => "SORT",
		                "SORT_ORDER2" => "ASC",
		                "FILTER_NAME" => "arNewsAndEventsPreFilter",
		                "FIELD_CODE" => array(
		                    0 => "ID",
		                    1 => "CODE",
		                    2 => "SORT",
		                    3 => "PREVIEW_TEXT",
		                    4 => "DATE_ACTIVE_FROM",
		                    5 => "ACTIVE_FROM",
		                    6 => "DATE_ACTIVE_TO",
		                    7 => "ACTIVE_TO",
		                    8 => "DATE_CREATE",
		                    9 => "CREATED_USER_NAME",
		                    10 => "",
		                ),
		                "PROPERTY_CODE" => array(
		                    0 => "UF_AUTOR",
		                    1 => "UF_LOCATION",
		                    2 => "UF_LANGUAGE",
		                    3 => "UF_TARGET_GROUP",
		                    3 => "UF_TARGET_GROUP",
		                ),
		                "CHECK_DATES" => "Y",
		                "DETAIL_URL" => "",
		                "PREVIEW_TRUNCATE_LEN" => "",
		                "ACTIVE_DATE_FORMAT" => "d.m.Y",
		                "SET_TITLE" => "N",
		                "SET_BROWSER_TITLE" => "N",
		                "SET_META_KEYWORDS" => "Y",
		                "SET_META_DESCRIPTION" => "Y",
		                "SET_LAST_MODIFIED" => "Y",
		                "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		                "ADD_SECTIONS_CHAIN" => "N",
		                "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
		                "PARENT_SECTION" => "",
		                "PARENT_SECTION_CODE" => "",
		                "INCLUDE_SUBSECTIONS" => "N",
		                "CACHE_TYPE" => "N",
		                "CACHE_TIME" => "3600",
		                "CACHE_FILTER" => "Y",
		                "CACHE_GROUPS" => "Y",
		                "DISPLAY_TOP_PAGER" => "N",
		                "DISPLAY_BOTTOM_PAGER" => "N",
		                "PAGER_TITLE" => "Новости",
		                "PAGER_SHOW_ALWAYS" => "N",
		                "PAGER_TEMPLATE" => "",
		                "PAGER_DESC_NUMBERING" => "Y",
		                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		                "PAGER_SHOW_ALL" => "N",
		                "PAGER_BASE_LINK_ENABLE" => "N",
		                "SET_STATUS_404" => "N",
		                "SHOW_404" => "N",
		                "MESSAGE_404" => "",
		                "PAGER_BASE_LINK" => "",
		                "PAGER_PARAMS_NAME" => "arrPager",
		                "AJAX_OPTION_JUMP" => "N",
		                "AJAX_OPTION_STYLE" => "Y",
		                "AJAX_OPTION_HISTORY" => "N",
		                "AJAX_OPTION_ADDITIONAL" => "",
		                "COMPONENT_TEMPLATE" => "little_events"
		            ),
		            false
		        );?>
		        <?endif;?>
		        <?if($GLOBALS["NOT_SHOW_IMP_INFO_ON_THIS_SECTION"] && $GLOBALS["UF_IMP_INFO_SHOW_IN"]=="10"):?>
		            <?$APPLICATION->IncludeComponent(
		                "bitrix:news.list",
		                "little_events_with_modal",
		                array(
		                    "DISPLAY_DATE" => "N",
		                    "DISPLAY_NAME" => "Y",
		                    "DISPLAY_PICTURE" => "N",
		                    "DISPLAY_PREVIEW_TEXT" => "N",
		                    "AJAX_MODE" => "Y",
		                    "IBLOCK_TYPE" => "content",
		                    "IBLOCK_ID" => "9",
		                    "NEWS_COUNT" => "2",
		                    "SORT_BY1" => "ACTIVE_FROM",
		                    "SORT_ORDER1" => "DESC",
		                    "SORT_BY2" => "SORT",
		                    "SORT_ORDER2" => "ASC",
		                    "FILTER_NAME" => "arFilterLanguage",
		                    "FIELD_CODE" => array(
		                        0 => "ID",
		                        1 => "CODE",
		                        2 => "SORT",
		                        3 => "PREVIEW_TEXT",
		                        4 => "DATE_ACTIVE_FROM",
		                        5 => "ACTIVE_FROM",
		                        6 => "DATE_ACTIVE_TO",
		                        7 => "ACTIVE_TO",
		                        8 => "DATE_CREATE",
		                        9 => "CREATED_USER_NAME",
		                        10 => "",
		                    ),
		                    "PROPERTY_CODE" => array(
		                        0 => "UF_AUTOR",
		                        1 => "UF_LANGUAGE",
		                        2 => "",
		                    ),
		                    "CHECK_DATES" => "Y",
		                    "DETAIL_URL" => "",
		                    "PREVIEW_TRUNCATE_LEN" => "",
		                    "ACTIVE_DATE_FORMAT" => "d.m.Y",
		                    "SET_TITLE" => "N",
		                    "SET_BROWSER_TITLE" => "N",
		                    "SET_META_KEYWORDS" => "Y",
		                    "SET_META_DESCRIPTION" => "Y",
		                    "SET_LAST_MODIFIED" => "Y",
		                    "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		                    "ADD_SECTIONS_CHAIN" => "N",
		                    "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
		                    "PARENT_SECTION" => "",
		                    "PARENT_SECTION_CODE" => "",
		                    "INCLUDE_SUBSECTIONS" => "N",
		                    "CACHE_TYPE" => "N",
		                    "CACHE_TIME" => "3600",
		                    "CACHE_FILTER" => "Y",
		                    "CACHE_GROUPS" => "Y",
		                    "DISPLAY_TOP_PAGER" => "N",
		                    "DISPLAY_BOTTOM_PAGER" => "N",
		                    "PAGER_TITLE" => "Новости",
		                    "PAGER_SHOW_ALWAYS" => "N",
		                    "PAGER_TEMPLATE" => "",
		                    "PAGER_DESC_NUMBERING" => "Y",
		                    "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		                    "PAGER_SHOW_ALL" => "N",
		                    "PAGER_BASE_LINK_ENABLE" => "N",
		                    "SET_STATUS_404" => "N",
		                    "SHOW_404" => "N",
		                    "MESSAGE_404" => "",
		                    "PAGER_BASE_LINK" => "",
		                    "PAGER_PARAMS_NAME" => "arrPager",
		                    "AJAX_OPTION_JUMP" => "N",
		                    "AJAX_OPTION_STYLE" => "Y",
		                    "AJAX_OPTION_HISTORY" => "N",
		                    "AJAX_OPTION_ADDITIONAL" => "",
		                    "COMPONENT_TEMPLATE" => "little_events"
		                ),
		                false
		            );?>
		        <?endif;?>
		    </div>
		<?endif;?>
	<?endif;?>
<?endif;?>
<?
if (!empty($arResult['ITEMS']))
{
	$strElementEdit = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_EDIT");
	$strElementDelete = CIBlock::GetArrayByID($arParams["IBLOCK_ID"], "ELEMENT_DELETE");
	$arElementDeleteParams = array("CONFIRM" => GetMessage('CT_BCS_TPL_ELEMENT_DELETE_CONFIRM'));
    foreach ($arResult['ITEMS'] as $key => $arItem)
    {
        /**
         *  Check language version
         *  This must be the same like site language
         * */
        if($arItem["PROPERTIES"]["UF_LANGUAGE"]["VALUE"] == $GLOBALS["arFilterLanguage"]["PROPERTY_UF_LANGUAGE"])
        {
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], $strElementEdit);
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], $strElementDelete, $arElementDeleteParams);
            $strMainID = $this->GetEditAreaId($arItem['ID']);

            /** Get tabs by element id */
            $ar_tabs_list = \BIT\ORM\BITTabsList::GetListByIBElementID($arItem['ID']);
            $ar_tabs_types = \BIT\ORM\BITTypeTab::GetAllTypes();

            if(isset($_GET["hash"]) && strlen($_GET["hash"])>0)
            {
                $tab_hash_numb = $_GET["hash"];
            }
            else
            {
                $ar_temp_tabs = $ar_tabs_list;
                $tab_hash_numb = "tab".array_shift($ar_temp_tabs)["ID"];
                unset($ar_temp_tabs);
            }
	        $bool_hash_isset = false;
	        foreach($ar_tabs_list as $ar_cur_tab)
	        {
		        if("tab".$ar_cur_tab["ID"] == $tab_hash_numb
			        &&
			        (
				        $ar_cur_tab["UF_SHOW_TYPE"] != 13
				        &&
				        (
					        $ar_cur_tab["UF_SHOW_TYPE"] == 11
					        ||
					        ($ar_cur_tab["UF_SHOW_TYPE"] == 12 && intval($USER->GetID())>0)
				        )
			        )
		        )
		        {
			        $bool_hash_isset = true;
			        break;
		        }
	        }
	        if(!$bool_hash_isset)
	        {
		        global $USER;
		        foreach($ar_tabs_list as $ar_cur_tab)
		        {
			        if(
				        $ar_cur_tab["UF_SHOW_TYPE"] != 13
				        &&
				        (
					        $ar_cur_tab["UF_SHOW_TYPE"] == 11
					        ||
					        ($ar_cur_tab["UF_SHOW_TYPE"] == 12 && intval($USER->GetID())>0)
				        )
			        )
			        {
				        $tab_hash_numb = "tab".$ar_cur_tab["ID"];
				        break;
			        }
		        }
	        }
            ?>
			<?if(count($ar_tabs_list)>1):?>
				<?if($GLOBALS["UF_L_R_B_NOT_SHOW"]):?>
					<div class="
					tabs_menu
						<?if(!$GLOBALS["NOT_SHOW_EVENTS_ON_THIS_SECTION"] && !$GLOBALS["NOT_SHOW_NEWS_ON_THIS_SECTION"] && !$GLOBALS["NOT_SHOW_IMP_INFO_ON_THIS_SECTION"]
							||
							!($GLOBALS["UF_NEWS_SHOW_IN"]=="8" || $GLOBALS["UF_EVENT_SHOW_IN"]=="6" || $GLOBALS["UF_IMP_INFO_SHOW_IN"]=="10")
						):?>
						cols_2
						<?endif;?>
						<?if($GLOBALS["NOT_SHOW_EVENTS_ON_THIS_SECTION"] && $GLOBALS["NOT_SHOW_NEWS_ON_THIS_SECTION"] && $GLOBALS["NOT_SHOW_IMP_INFO_ON_THIS_SECTION"]
					        ||
					        ($GLOBALS["UF_NEWS_SHOW_IN"]=="8" || $GLOBALS["UF_EVENT_SHOW_IN"]=="6" || $GLOBALS["UF_IMP_INFO_SHOW_IN"]=="10")
				        ):?>
				        one_col
				        <?endif;?>
					" id="<?=$this->GetEditAreaId($arItem['ID']);?>" style="padding-bottom: 0px;">
						<div class="block_headers">
							<ul class="links">
								<?
								foreach($ar_tabs_list as $ar_cur_tab)
								{
									if($ar_cur_tab["UF_SHOW_TYPE"] != 13):
										if($ar_cur_tab["UF_SHOW_TYPE"] == 11 || ($ar_cur_tab["UF_SHOW_TYPE"] == 12 && intval($USER->GetID())>0)):
											?><li class="
											<?if("tab".$ar_cur_tab["ID"] == $tab_hash_numb):?>
											active
											<?endif;?>
											"><a href="?hash=tab<?=$ar_cur_tab["ID"]?>"  data-hash="#tab<?=$ar_cur_tab["ID"]?>"><?=$ar_cur_tab["UF_NAME"]?></a></li><?
										endif;
									endif;
								}
								?>
							</ul>
						</div>
					</div>
				<?endif;?>
			<?endif;?>
            <?
            foreach($ar_tabs_list as $ar_cur_tab)
            {
				if($ar_cur_tab["UF_SHOW_TYPE"] != 13)
				{
					if ($ar_cur_tab["UF_SHOW_TYPE"] == 11 || ($ar_cur_tab["UF_SHOW_TYPE"] == 12 && intval($USER->GetID()) > 0))
					{
						?>
						<? if ($GLOBALS["UF_L_R_B_NOT_SHOW"]): ?>
							<div id="tab<?= $ar_cur_tab["ID"] ?>" class="
	                inner_page_content
	                <? if ("tab" . $ar_cur_tab["ID"] != $tab_hash_numb): ?>
	                hide_el
	                <? endif; ?>
	                <? if (!$GLOBALS["NOT_SHOW_EVENTS_ON_THIS_SECTION"] && !$GLOBALS["NOT_SHOW_NEWS_ON_THIS_SECTION"] && !$GLOBALS["NOT_SHOW_IMP_INFO_ON_THIS_SECTION"]
								||
								!($GLOBALS["UF_NEWS_SHOW_IN"] == "8" || $GLOBALS["UF_EVENT_SHOW_IN"] == "6" || $GLOBALS["UF_IMP_INFO_SHOW_IN"] == "10")
							): ?>
	                cols_2
	                <? endif; ?>
	                ">
						<? endif; ?>
						<!--	            --><?//if(strlen($ar_cur_tab["UF_NAME"])>1):
						?>
						<!--	                <div>-->
						<!--	                    <h2>--><?//=$ar_cur_tab["UF_NAME"]
						?><!--</h2>-->
						<!--	                </div>-->
						<!--	                <hr/>-->
						<!--	            --><?//endif;
						?>
						<?
						if ($ar_tabs_types[$ar_cur_tab["UF_TAB_TYPE_ID"]]["UF_CODE"] == "TEXT") {
							/**
							 *  This is hack, wich unclude components and show html
							 */
							eval("?>" . htmlspecialcharsBack($ar_cur_tab["UF_HTML_TEXT"]) . "<?");

						} elseif ($ar_tabs_types[$ar_cur_tab["UF_TAB_TYPE_ID"]]["UF_CODE"] == "LIST") {
							foreach ($ar_cur_tab["UF_LIST"] as $ar_list_elem) {
								?>
								<div class="bayan" id="<?= $ar_list_elem["ID"] ?>">
									<h3><span><?= $ar_list_elem["UF_NAME"] ?></span></h3>

									<div>
										<p>
											<?
											/**
											 *  This is hack, wich unclude components and show html
											 */
											eval("?>" . htmlspecialcharsBack($ar_list_elem["UF_TEXT"]) . "<?");
											?>
										</p>
									</div>
								</div>
								<?
							}
						} elseif ($ar_tabs_types[$ar_cur_tab["UF_TAB_TYPE_ID"]]["UF_CODE"] == "EMPLOYEES") {
							?>
							

							<div class="square_container"><?
							foreach ($ar_cur_tab["UF_EMPLOYEES"] as $contact) {
								$employee_photo_path = CFile::GetPath($contact["PERSONAL_PHOTO"]);
								?>
								<div class="square_item">
<!-- РЕКТОРАТ -->
<table class="working-boss" style="border: none; margin-bottom: 15px; background: white;">
<tbody>
<tr>
<td style="text-align: center; vertical-align: middle;" rowspan="6" width="100px"><strong><img style="width: 100px;" src="<?= $employee_photo_path ?>"></strong></td>
<td style="background: #800000;" colspan="3"><span style="color: white; "><strong>

  <? if ($contact["PERSONAL_PROFESSION"] && strlen($contact["PERSONAL_PROFESSION"]) && isset($contact["PERSONAL_PROFESSION"])): ?>
											<?= $contact["PERSONAL_PROFESSION"] ?>
											<?
										elseif ($contact["WORK_POSITION"] && strlen($contact["WORK_POSITION"]) && isset($contact["WORK_POSITION"])):?>
											<?= $contact["WORK_POSITION"] ?>
										<? endif; ?>
										</strong></span>
</td>
</tr>
<tr>

<td class="name" style="vertical-align: middle; font-size: 20px; height: 30px;" colspan="3"><?= $contact["LAST_NAME"] ?> <?= $contact["NAME"] ?> <?= $contact["SECOND_NAME"] ?></td>
</tr>
<? if ($contact["UF_ACADEMIC_DEGREE"]): ?>
<tr>
<td width="120px">Ученая степень:<br />Ученое звание:</td>
<td>
<?foreach($contact["UF_ACADEMIC_DEGREE"] as $academic_degree):?>
                        <?=$academic_degree?><br />
                    <?endforeach;?>
		<?foreach($contact["UF_ACADEMIC_TITLE"] as $academic_title):?>
                        <?=$academic_title?><br />
                    <?endforeach;?>
					
					</td>
</tr>
<? endif; ?>

<? if ($contact["UF_DISCIPLINES"]): ?>

<tr>
<td>Преподаваемые дисциплины</td>
<td>


<?foreach($contact["UF_DISCIPLINES"] as $disciplines):?>
                        <?=$disciplines?><br />
                    <?endforeach;?>
</td>
</tr>
<? endif; ?>

<? if ($contact["PERSONAL_STREET"]): ?>
<tr>
<td>Контакты</td>
<td>
                        <?=$contact["PERSONAL_STREET"]?><br />
</td>
</tr>
<? endif; ?>

<tr>
<td colspan="2" style="text-align:right"><a href="/users/<?= $contact["ID"] ?>/">Перейти к полному профилю</a></td>
</tr>

<tbody></table>
					
					
					<div style="display:none;">
									<a href="/users/<?= $contact["ID"] ?>/">
										<div class="picture">
											<src="<?= $employee_photo_path ?>" alt=""/>
										</div>
										<div class="square_title">
                                    <span>
                                        <? if ($contact["PERSONAL_PROFESSION"] && strlen($contact["PERSONAL_PROFESSION"]) && isset($contact["PERSONAL_PROFESSION"])): ?>
											<?= $contact["PERSONAL_PROFESSION"] ?>
											<?
										elseif ($contact["WORK_POSITION"] && strlen($contact["WORK_POSITION"]) && isset($contact["WORK_POSITION"])):?>
											<?= $contact["WORK_POSITION"] ?>
										<? endif; ?>
                                    </span>
										</div>
										<div class="square_name">
											<span><?= $contact["LAST_NAME"] ?> <?= $contact["NAME"] ?> <?= $contact["SECOND_NAME"] ?></span>
										</div>
									</a>
								</div>
								</div>		
								<?
								unset($employee_photo_path);
							}
							?>
							
							</div>
					

							
							
							
							<?
						} elseif ($ar_tabs_types[$ar_cur_tab["UF_TAB_TYPE_ID"]]["UF_CODE"] == "EMPLOYEES_SOAP") {
							?>


							<div class="square_container"><?
								if ($ar_cur_tab["UF_EMPLOYEES"] && function_exists("sortUsersByMainProfession"))
								{
									$ar_cur_tab["UF_EMPLOYEES"] = sortUsersByMainProfession(
										$ar_cur_tab["UF_EMPLOYEES"],
										$arItem["IBLOCK_SECTION_ID"]
									);
								}

								foreach ($ar_cur_tab["UF_EMPLOYEES"] as $contact) {
									$employee_photo_path = CFile::GetPath($contact["PERSONAL_PHOTO"]);
									?>
									<div class="square_item">

										<table class="working-boss" style="border: none; margin-bottom: 15px; background: white;">
											<tbody>
											<tr>
												<!-- td style="text-align: center; vertical-align: middle;" rowspan="5" width="100px"><strong><img style="width: 100px;" src="< ?= $employee_photo_path ?>"></strong></td -->
												<td style="text-align: center; vertical-align: middle;" rowspan="20" width="100px"><strong><img style="width: 100px;" src="/upload/main/020/813272jazqnvae82m1ivw5ddv6aw.png"></strong></td>
												<td style="background: #800000;" colspan="3"><span style="color: white; "><strong>

															<?
															$profession = "";
															if (function_exists("getDepartmentProfession")) {
																$profession = getDepartmentProfession(
																	$contact["UF_PROFESSION"],
																	$contact["UF_DEP_UTIL"],
																	$arItem["IBLOCK_SECTION_ID"]
																);
															}
															if ($profession): ?>
																<?= $profession ?>
																<?
															elseif ($contact["WORK_POSITION"] && strlen($contact["WORK_POSITION"]) && isset($contact["WORK_POSITION"])):?>
																<?= $contact["WORK_POSITION"] ?>
															<? endif; ?>
														</strong></span>
												</td>
											</tr>
											<tr>

												<td class="name" style="vertical-align: middle; font-size: 20px; height: 30px;" colspan="3"><?= $contact["LAST_NAME"] ?> <?= $contact["NAME"] ?> <?= $contact["SECOND_NAME"] ?></td>
											</tr>
											<tr>
												<td width="120px">Ученая степень:<br />Ученое звание:</td>
												<td>
													<?foreach($contact["UF_ACADEMIC_DEGREE"] as $academic_degree):?>
														<?=$academic_degree?><br />
													<?endforeach;?>
													<!-- ?foreach($contact["UF_ACADEMIC_TITLE"] as $academic_title):?>
														<!-- ?=$academic_title?><br />
													<!-- ?endforeach;? -->

												</td>
											<? if (!empty($contact["UF_DISCIPLINES"])) { ?>
											<tr>
												<td>Преподаваемые дисциплины</td>
												<td>


													<?foreach($contact["UF_DISCIPLINES"] as $disciplines):?>
														<?=$disciplines?><br />
													<?endforeach;?>
												</td>
											</tr>
											<? } ?>
											<tr>
												<td colspan="2" style="text-align:right"><b><a href="/users/<?= $contact["ID"] ?>/">Перейти к полному профилю</a></b></td>
											</tr>

											<tbody></table>


										<div style="display:none;">
											<a href="/users/<?= $contact["ID"] ?>/">
												<div class="picture">
													<img src="<?= $employee_photo_path ?>" alt=""/>
												</div>
												<div class="square_title">
                                    <span>
                                        <? if ($contact["PERSONAL_PROFESSION"] && strlen($contact["PERSONAL_PROFESSION"]) && isset($contact["PERSONAL_PROFESSION"])): ?>
	                                        <?= $contact["PERSONAL_PROFESSION"] ?>
	                                        <?
                                        elseif ($contact["WORK_POSITION"] && strlen($contact["WORK_POSITION"]) && isset($contact["WORK_POSITION"])):?>
	                                        <?= $contact["WORK_POSITION"] ?>
                                        <? endif; ?>
                                    </span>
												</div>
												<div class="square_name">
													<span><?= $contact["LAST_NAME"] ?> <?= $contact["NAME"] ?> <?= $contact["SECOND_NAME"] ?></span>
												</div>
											</a>
										</div>
									</div>
									<?
									unset($employee_photo_path);
								}
								?>

							</div>





							<?
						} elseif ($ar_tabs_types[$ar_cur_tab["UF_TAB_TYPE_ID"]]["UF_CODE"] == "PHONEBOOK") {
							?>

							<div class="square_container"><?
								foreach ($ar_cur_tab["UF_PHONEBOOK"] as $contact) {
									?>
									<div class="square_item">

										<table class="working-boss" style="border: none; margin-bottom: 15px; background: white;">
											<tbody>
											<tr>
												<td style="text-align: center; vertical-align: middle;" rowspan="<?=count($contact["PROPERTY"])+2?>" width="100px">
													<strong>
														<img style="width: 100px;" src="<?=CFile::GetPath($contact["PREVIEW_PICTURE"])?>">
													</strong>
												</td>
												<td style="background: #800000;" colspan="3"><span style="color: white; "><strong>

															<? if ($contact["PROPERTY"]["POSITION"]["VALUE"] && strlen($contact["PROPERTY"]["POSITION"]["VALUE"])>0): ?>
																<?= $contact["PROPERTY"]["POSITION"]["VALUE"] ?>
															<? endif; ?>
														</strong></span>
												</td>
											</tr>
											<tr>

												<td class="name" style="vertical-align: middle; font-size: 20px; height: 30px;" colspan="3"><?= $contact["LAST_NAME"] ?> <?= $contact["NAME"] ?> <?= $contact["SECOND_NAME"] ?></td>
											</tr>
											<?foreach($contact["PROPERTY"] as $contactProp):?>
												<?if($contactProp["CODE"] == "POSITION") continue;?>
												<tr>
													<td>
														<?=$contactProp["NAME"]?>
													</td>
													<td>
														<?=$contactProp["VALUE"]?>
													</td>
												</tr>
											<?endforeach;?>

											<tr>
												<td colspan="2" style="text-align:right"><a href="<?=$contact["DETAIL_PAGE_URL"]?>/">Перейти к полному профилю</a></td>
											</tr>

											<tbody></table>


										<div style="display:none;">
											<a href="/users/<?= $contact["ID"] ?>/">
												<div class="picture">
													<img src="<?= $employee_photo_path ?>" alt=""/>
												</div>
												<div class="square_title">
                                    <span>
                                        <? if ($contact["PERSONAL_PROFESSION"] && strlen($contact["PERSONAL_PROFESSION"]) && isset($contact["PERSONAL_PROFESSION"])): ?>
	                                        <?= $contact["PERSONAL_PROFESSION"] ?>
	                                        <?
                                        elseif ($contact["WORK_POSITION"] && strlen($contact["WORK_POSITION"]) && isset($contact["WORK_POSITION"])):?>
	                                        <?= $contact["WORK_POSITION"] ?>
                                        <? endif; ?>
                                    </span>
												</div>
												<div class="square_name">
													<span><?= $contact["LAST_NAME"] ?> <?= $contact["NAME"] ?> <?= $contact["SECOND_NAME"] ?></span>
												</div>
											</a>
										</div>
									</div>
									<?
									unset($employee_photo_path);
								}
								?>

							</div>





							<?
						}elseif ($ar_tabs_types[$ar_cur_tab["UF_TAB_TYPE_ID"]]["UF_CODE"] == "GALLERY") {
							?>
							<div class="mix_gallery justifyfix">
								<?
								foreach ($ar_cur_tab["UF_VIDEO"] as $cur_video) {
									$video_obj = new VideoThumb($cur_video);
									$video_obj_title = $video_obj->getTitle();
									$video_obj_img = $video_obj->getImage();
									$video_obj_path = $video_obj->getVideo();
									?>
									<a class="gallery_to_modal video" data-fancybox-type="iframe"
									   data-fancybox-group="group<?= $ar_cur_tab["ID"] ?>"
									   href="<?= $video_obj_path ?>">
										<img src="<?= $video_obj_img ?>" alt="<?= $video_obj_title ?>"/>
									</a>
									<?
									unset($video_obj_img);
								}
								foreach ($ar_cur_tab["UF_GALLERY"] as $ar_cur_image) {
									$gallery_image_path = CFile::GetPath($ar_cur_image);
									?>
									<a class="gallery_to_modal photo" data-fancybox-type="image"
									   data-fancybox-group="group<?= $ar_cur_tab["ID"] ?>"
									   href="<?= $gallery_image_path ?>">
										<img src="<?= $gallery_image_path ?>" alt=""/>
									</a>
									<?
									unset($gallery_image_path);
								}
								?>
							</div>
							<?
						} elseif ($ar_tabs_types[$ar_cur_tab["UF_TAB_TYPE_ID"]]["UF_CODE"] == "STRUCTURE") {
							$ar_cur_tab["UF_STRUCTURE"] = array_reverse($ar_cur_tab["UF_STRUCTURE"]);
							foreach ($ar_cur_tab["UF_STRUCTURE"] as $structure_elem) {
								?>
								<h3>
									<a href="<?= $structure_elem["UF_SECTION_PAGE_URL"] ?>">
										<?= $structure_elem["UF_SECTION_NAME"] ?>
									</a>
								</h3>
								<?
								if (!empty($structure_elem["UF_SELECTED_TABS"])) {
									?>
									<ul><?
									$tabs_list = \BIT\ORM\BITTabsList::GetListByArrID($structure_elem["UF_SELECTED_TABS"]);
									foreach ($tabs_list as $temp_tab) {
										?>
										<li>
											<a href="<?= $structure_elem["UF_SECTION_PAGE_URL"] ?>?hash=tab<?= $temp_tab["ID"] ?>"><?= $temp_tab["UF_NAME"] ?></a>
										</li>
										<?
									}
									?></ul><?
								}
							}
						} elseif ($ar_tabs_types[$ar_cur_tab["UF_TAB_TYPE_ID"]]["UF_CODE"] == "STRUCTURE_BAYAN")
						{

							$ar_cur_tab_srtucture_bayan = \BIT\ORM\BITTabsList::GetStructureInfoByanByArID($ar_cur_tab["UF_STRUCTURE_BAYAN"]);
							if($ar_cur_tab_srtucture_bayan)
							{
								foreach ($ar_cur_tab_srtucture_bayan as $ar_byan_elem) {
									?>
									<div class="bayan" id="<?= $ar_byan_elem["ID"] ?>">
										<h3><span><?= $ar_byan_elem["NAME"] ?></span></h3>

										<div>
											<p>
												<?
												/**
												 *  This is hack, wich unclude components and show html
												 */
												eval("?>" . htmlspecialcharsBack($ar_byan_elem["DESCRIPTION"]) . "<?");
												?>
											</p>
										</div>
									</div>
									<?
								}
							}
						} elseif ($ar_tabs_types[$ar_cur_tab["UF_TAB_TYPE_ID"]]["UF_CODE"] == "CONSULTATIONS") {

							?>
							<?if ($ar_cur_tab["UF_TABLE_DATA"]):?>
								<table border="1" cellspacing="0" cellpadding="0">
									<thead>
										<tr>
											<?foreach ($ar_cur_tab["UF_TABLE_DATA"]["HEAD"] as $headCell):?>
												<th><p><?=$headCell?></p></th>
											<?endforeach;?>
										</tr>
									</thead>
									<tbody>
										<?foreach ($ar_cur_tab["UF_TABLE_DATA"]["BODY"] as $row):?>
											<tr>
												<?foreach ($row as $rowCell):?>
													<td><p><?=$rowCell?></p></td>
												<?endforeach;?>
											</tr>
										<?endforeach;?>
									</tbody>
								</table>
							<?endif;?>
							<?
						}

						?>
						<? if ($GLOBALS["UF_L_R_B_NOT_SHOW"]): ?>
							</div>
						<? endif; ?>
						<?
					}
				}

            }
        }
    }
}
else{
    ?>
    <div class="inner_page_content">
    </div>
    <?
}
?>
<?unset($GLOBALS["NOT_SHOW_EVENTS_ON_THIS_SECTION"], $GLOBALS["NOT_SHOW_NEWS_ON_THIS_SECTION"], $GLOBALS["NOT_SHOW_IMP_INFO_ON_THIS_SECTION"]);?>