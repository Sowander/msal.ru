<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();

$arViewModeList = array('LIST', 'LINE', 'TEXT', 'TILE');

$arDefaultParams = array(
	'VIEW_MODE' => 'LIST',
	'SHOW_PARENT_NAME' => 'Y',
	'HIDE_SECTION_NAME' => 'N'
);

$arParams = array_merge($arDefaultParams, $arParams);

if (!in_array($arParams['VIEW_MODE'], $arViewModeList))
	$arParams['VIEW_MODE'] = 'LIST';
if ('N' != $arParams['SHOW_PARENT_NAME'])
	$arParams['SHOW_PARENT_NAME'] = 'Y';
if ('Y' != $arParams['HIDE_SECTION_NAME'])
	$arParams['HIDE_SECTION_NAME'] = 'N';

$arResult['VIEW_MODE_LIST'] = $arViewModeList;

if (0 < $arResult['SECTIONS_COUNT'])
{
	if ('LIST' != $arParams['VIEW_MODE'])
	{
		$boolClear = false;
		$arNewSections = array();
		foreach ($arResult['SECTIONS'] as &$arOneSection)
		{
			if (1 < $arOneSection['RELATIVE_DEPTH_LEVEL'])
			{
				$boolClear = true;
				continue;
			}
			$arNewSections[] = $arOneSection;
		}
		unset($arOneSection);
		if ($boolClear)
		{
			$arResult['SECTIONS'] = $arNewSections;
			$arResult['SECTIONS_COUNT'] = count($arNewSections);
		}
		unset($arNewSections);
	}
}

if (0 < $arResult['SECTIONS_COUNT'])
{
	$boolPicture = false;
	$boolDescr = false;
	$arSelect = array('ID');
	$arMap = array();
	if ('LINE' == $arParams['VIEW_MODE'] || 'TILE' == $arParams['VIEW_MODE'])
	{
		reset($arResult['SECTIONS']);
		$arCurrent = current($arResult['SECTIONS']);
		if (!isset($arCurrent['PICTURE']))
		{
			$boolPicture = true;
			$arSelect[] = 'PICTURE';
		}
		if ('LINE' == $arParams['VIEW_MODE'] && !array_key_exists('DESCRIPTION', $arCurrent))
		{
			$boolDescr = true;
			$arSelect[] = 'DESCRIPTION';
			$arSelect[] = 'DESCRIPTION_TYPE';
		}
	}
	if ($boolPicture || $boolDescr)
	{
		foreach ($arResult['SECTIONS'] as $key => $arSection)
		{
			$arMap[$arSection['ID']] = $key;
		}
		$rsSections = CIBlockSection::GetList(array(), array('ID' => array_keys($arMap)), false, $arSelect);
		while ($arSection = $rsSections->GetNext())
		{
			if (!isset($arMap[$arSection['ID']]))
				continue;
			$key = $arMap[$arSection['ID']];
			if ($boolPicture)
			{
				$arSection['PICTURE'] = intval($arSection['PICTURE']);
				$arSection['PICTURE'] = (0 < $arSection['PICTURE'] ? CFile::GetFileArray($arSection['PICTURE']) : false);
				$arResult['SECTIONS'][$key]['PICTURE'] = $arSection['PICTURE'];
				$arResult['SECTIONS'][$key]['~PICTURE'] = $arSection['~PICTURE'];
			}
			if ($boolDescr)
			{
				$arResult['SECTIONS'][$key]['DESCRIPTION'] = $arSection['DESCRIPTION'];
				$arResult['SECTIONS'][$key]['~DESCRIPTION'] = $arSection['~DESCRIPTION'];
				$arResult['SECTIONS'][$key]['DESCRIPTION_TYPE'] = $arSection['DESCRIPTION_TYPE'];
				$arResult['SECTIONS'][$key]['~DESCRIPTION_TYPE'] = $arSection['~DESCRIPTION_TYPE'];
			}
		}
	}
}

/**
 *  Get Language names by site language
*/
foreach($arResult['SECTIONS'] as $key=>$arSection)
{
    $ar_temp_filter = Array('IBLOCK_ID'=>$arSection["IBLOCK_ID"], 'ID'=>$arSection["ID"]);
    $ar_temp_select = array("IBLOCK_ID", "UF_*");
    $db_temp_list = CIBlockSection::GetList(array(), $ar_temp_filter, false, $ar_temp_select, false);
    unset($ar_temp_filter);
    unset($ar_temp_select);
    if($ar_temp_result = $db_temp_list->Fetch())
    {
        $arResult['SECTIONS'][$key]["UF_NAME"] = $ar_temp_result["UF_NAME_".strtoupper(LANGUAGE_ID)];
        unset($ar_temp_result);
    }
    unset($db_temp_list);
}
/**
 *  Get language name fot current section by site language
*/
if($arResult['SECTION']["ID"] && $arResult['SECTION']["IBLOCK_ID"])
{
    $ar_temp_filter = Array('IBLOCK_ID'=>$arResult['SECTION']["IBLOCK_ID"], 'ID'=>$arResult['SECTION']["ID"]);
    $ar_temp_select = array("IBLOCK_ID", "UF_*");
    $db_temp_list = CIBlockSection::GetList(array(), $ar_temp_filter, false, $ar_temp_select, false);
    unset($ar_temp_filter);
    unset($ar_temp_select);
    if($ar_temp_result = $db_temp_list->Fetch())
    {
        $arResult['SECTION']["UF_NAME"] = $ar_temp_result["UF_NAME_".strtoupper(LANGUAGE_ID)];
        unset($ar_temp_result);
    }
    unset($db_temp_list);
}

?>