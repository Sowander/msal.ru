<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="helper inner_page_content" style="display: inline-block;">
<?if(!empty($arResult['ANSWER'])):?>

        <div class="helper">
            <div class="main_header">
                <a href="javascript:history.back();" class="left"><img src="/local/templates/main/img/left_arrow.png" style="width: 50px;"><!-- &laquo;&nbsp;<!-- ?=GetMessage("CCS_BACK");? --></a>
				<h4><?=$arResult["UF_NAME"]?></h4>
                <hr>
            </div>
            <div class="helper_in_main">
                            <span>
                                <p>
                                    <?if(strlen($arResult["ANSWER"]["DETAIL_TEXT"])>0):?>
                                        <?=trim($arResult["ANSWER"]["DETAIL_TEXT"]);?>
                                    <?elseif(strlen($arResult["ANSWER"]["PREVIEW_TEXT"])>0):?>
                                        <?=trim($arResult["ANSWER"]["PREVIEW_TEXT"]);?>
                                    <?else:?>
                                        <?=GetMessage("CCS_NO_ANSWER");?>
                                    <?endif?>
                                </p>
                            </span>
            </div>
            <?$APPLICATION->IncludeComponent("bit:answers.custom.counter", "", Array(
                    "IBLOCK_ID"        => $arResult['ANSWER']["IBLOCK_ID"],
                    "ELEMENT_ID"       => $arResult['ANSWER']["ID"],
                    "AJAX_MODE"        => "Y"
                ),
                false
            );?>
            <div class="go_back">
                <?if(!empty($arResult["PREV"]) && strlen($arResult["PREV"]["UF_NAME"])>0):?>
                    <a href="<?=substr($GLOBALS["arSiteLanguageParams"]["UF_LINK"], 0, -1)?><?=$arResult["PREV"]["SECTION_PAGE_URL"]?>" class="left"><< <?=$arResult["PREV"]["UF_NAME"]?></a>
                <?endif;?>
                <?if(!empty($arResult["NEXT"]) && strlen($arResult["NEXT"]["UF_NAME"])>0):?>
                    <a href="<?=substr($GLOBALS["arSiteLanguageParams"]["UF_LINK"], 0, -1)?><?=$arResult["NEXT"]["SECTION_PAGE_URL"]?>" class="right"><?=$arResult["NEXT"]["UF_NAME"]?>  >></a>
                <?endif;?>
            </div>
            <hr>
			

<a class="widget-tab widget-tab-left" id="copiny-widget-tab" title="Техническая поддержка" href="#" style="border-color: rgb(248, 242, 215);"><img class="widget-img" src="http://widget.copiny.com/image.php?text=b4d210c6877974eec9c5bad747f458af/b4d210c6877974eec9c5bad747f458af/ejOwVXUxUHU0ApFOpqouhqoWpmC2C5i0AIuYI8kagtmOYNIALOKmDea4gUlXMGmCREK0GYDZZsia1Qxt/izLTM0oA"></a>			
			

            <div class="last_helper" style="display:none;">
                <h2><?=GetMessage("CCS_NO_ANSWER_RIGHT");?></h2>
                            <span>
                                <p>
                                    <?=GetMessage("CCS_NO_ASK_RIGHT");?>
                                </p>
                            </span>
                <?$APPLICATION->IncludeComponent(
	"bit:custom.feedback", 
	".default", 
	array(
		"USE_CAPTCHA" => "N",
		"OK_TEXT" => "Спасибо, ваше сообщение отправлено.",
		"EMAIL_TO" => "pvryabov@msal.ru",
		"REQUIRED_FIELDS" => array(
			0 => "UF_NAME",
			1 => "UF_EMAIL",
			2 => "UF_QUESTION",
		),
		"EVENT_MESSAGE_ID" => array(
			0 => "5",
		),
		"COMPONENT_TEMPLATE" => ".default",
		"IBLOCK_TYPE" => "tools",
		"IBLOCK_ID" => "13",
		"IBLOCK_ELEM" => "192",
		"NAME_TO_RU" => "Техническая поддержка",
		"NAME_TO_EN" => "Helpdesk"
	),
	false
);?>
            </div>
        </div>
<?endif;?>
</div>