<?
$MESS ['MAIN_UL_TPL_INFO_CLOSE'] = "Закрыть";
$MESS ['MAIN_UL_TPL_INFO_LOADING'] = "Загрузка...";
$MESS ['MAIN_UL_CULED_DISCILPINES'] = "Преподаваемые дисциплины";
$MESS ['MAIN_UL_CULED_EDUCATION'] = "Образование";
$MESS ['MAIN_UL_CULED_SPICIALITY'] = "Специальность";
$MESS ['MAIN_UL_CULED_EXPERTISE'] = "Квалификация";
$MESS ['MAIN_UL_CULED_TRANING'] = "Повышение квалификации";
$MESS ['MAIN_UL_CULED_TOTAL_EXPERIENCE'] = "Общий стаж";
$MESS ['MAIN_UL_CULED_SPEC_EXPERIENCE'] = "Стаж по специальности";
$MESS ['MAIN_UL_CULED_TOTAL_HONORS'] = "Награды";
$MESS ['MAIN_UL_CULED_DETAIL_INFORMATION'] = "Подробная информация";
?>