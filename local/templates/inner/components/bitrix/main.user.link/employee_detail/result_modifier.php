<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

if (function_exists("getDepartmentProfession"))
{
	$arResult["User"]["PERSONAL_PROFESSION"] = getDepartmentProfession(
		$arResult["User"]["UF_PROFESSION"],
		$arResult["User"]["UF_DEP_UTIL"]
	);
}