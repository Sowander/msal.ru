<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(strlen($arResult["FatalError"])>0) {}
else
{?>
    <div class="teacher inner_page_content" style="display: inline-block;">
        <div class="teacher">
            <div class="main_header">
                <h4><?=$arResult["User"]["NAME_FORMATTED"]?></h4>
            </div>
            <div class="about_teacher">
                <hr>
                <h5><?=$arResult["User"]["PERSONAL_PROFESSION"]?></h5>
                <span class="phd">
                    <?foreach($arResult["User"]["UF_ACADEMIC_DEGREE"] as $academic_degree):?>
                        <?=$academic_degree?>,
                    <?endforeach;?>
                    <?foreach($arResult["User"]["UF_ACADEMIC_TITLE"] as $academic_title):?>
                        <?=$academic_title?>,
                    <?endforeach;?>
                </span>
                <div class="ph_ab">
                    <div class="teacher_photo">
                        <img src="<?=CFile::GetPath($arResult["User"]["PERSONAL_PHOTO"]);?>" alt="">
                        <hr>
						<?if(!empty($arResult["User"]["UF_DISCIPLINES"])):?>
	                        <h6><?=GetMessage("MAIN_UL_CULED_DISCILPINES")?></h6>
	                        <span>
	                            <?foreach($arResult["User"]["UF_DISCIPLINES"] as $discipline):?>
	                                <?=$discipline?>,
	                            <?endforeach;?>
	                        </span>
	                        <hr>
						<?endif;?>
						<?if(
							!empty($arResult["User"]["UF_SPECIALITY"]) ||
							!empty($arResult["User"]["UF_EXERITIES"]) ||
							!empty($arResult["User"]["UF_RETRAINING"]) ||
							strlen($arResult["User"]["UF_TOTAL_EXPERIENCE"])>0 ||
							!empty($arResult["User"]["UF_AWARDS"])
							):?>
	                        <h6><?=GetMessage("MAIN_UL_CULED_EDUCATION")?></h6>
	                        <table>
								<?if(!empty($arResult["User"]["UF_EDUCATION"])):?>
									<tr>
										<td><?=GetMessage("MAIN_UL_CULED_EDUCATION")?></td>
										<td>
											<?foreach($arResult["User"]["UF_EDUCATION"] as $edu):?>
												<?=$edu?><br>
											<?endforeach;?>
										</td>
									</tr>
								<?endif;?>
		                        <?if(!empty($arResult["User"]["UF_SPECIALITY"])):?>
		                            <tr>
		                                <td><?=GetMessage("MAIN_UL_CULED_SPICIALITY")?></td>
		                                <td>
		                                    <?foreach($arResult["User"]["UF_SPECIALITY"] as $specility):?>
		                                        <?=$specility?><br>
		                                    <?endforeach;?>
		                                </td>
		                            </tr>
								<?endif;?>
								<?if(!empty($arResult["User"]["UF_EXERITIES"])):?>
		                            <tr>
		                                <td><?=GetMessage("MAIN_UL_CULED_EXPERTISE")?></td>
		                                <td>
		                                    <?foreach($arResult["User"]["UF_EXERITIES"] as $exerities):?>
		                                        <?=$exerities?><br>
		                                    <?endforeach;?>
		                                </td>
		                            </tr>
								<?endif;?>
								<?if(!empty($arResult["User"]["UF_RETRAINING"])):?>
		                            <tr>
		                                <td><?=GetMessage("MAIN_UL_CULED_TRANING")?></td>
		                                <td>
		                                    <?foreach($arResult["User"]["UF_RETRAINING"] as $retraining):?>
		                                        <?=$retraining?><br>
		                                    <?endforeach;?>
		                                </td>
		                            </tr>
								<?endif;?>
		                        <?if(strlen($arResult["User"]["UF_TOTAL_EXPERIENCE"])>0):?>
		                            <tr>
		                                <td><?=GetMessage("MAIN_UL_CULED_TOTAL_EXPERIENCE")?></td>
		                                <td><?=$arResult["User"]["UF_TOTAL_EXPERIENCE"]?></td>
		                            </tr>
								<?endif;?>
								<?if(strlen($arResult["User"]["UF_CPEC"])>0):?>
									<tr>
										<td><?=GetMessage("MAIN_UL_CULED_SPEC_EXPERIENCE")?></td>
										<td><?=$arResult["User"]["UF_CPEC"]?></td>
									</tr>
								<?endif;?>
		                        <?if(!empty($arResult["User"]["UF_AWARDS"])):?>
		                            <tr>
		                                <td><?=GetMessage("MAIN_UL_CULED_TOTAL_HONORS")?></td>
		                                <td>
		                                    <?foreach($arResult["User"]["UF_AWARDS"] as $awards):?>
		                                        <?=$awards?><br>
		                                    <?endforeach;?>
		                                </td>
		                            </tr>
								<?endif;?>
	                        </table>
						<?endif;?>
	                    <?if(strlen($arResult["User"]["PERSONAL_NOTES"])):?>
	                        <hr>
	                        <h6><?=GetMessage("MAIN_UL_CULED_DETAIL_INFORMATION")?></h6>
	                        <div class="about_teacher">
	                            <p>
	                                <?=$arResult["User"]["PERSONAL_NOTES"]?>
	                            </p>
	                        </div>
						<?endif;?>
                    </div>
                </div>
            </div>
        </div>
    </div>
<?
}
?>