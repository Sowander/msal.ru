<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>
<?
if(strlen($arResult["FatalError"])>0) {}
else
{?>
    <div class="teacher inner_page_content" style="display: inline-block;   width: 79%; " >
        
<style>
.inner_page_content table.working-boss tbody tr:nth-child(1n) td:nth-child(1) {
    padding: .2em .5em 0.2em .5em;
    vertical-align: middle;
    font-weight: 700;}
</style>	
		
		
		<table class="working-boss" style="border: none; margin-bottom: 15px; background: white;">
<tbody>
<tr>

<?$arrgh=CFile::GetPath($arResult["User"]["PERSONAL_PHOTO"]);?>
<!-- td style="text-align: center; vertical-align: middle;" rowspan="20" width="100px"><strong><img style="width: 100px;" src="< ? if ($arrgh == NULL) { echo "/upload/main/6cc/6cc619c57347976c1f5071274b62adb9.jpg"; } else { echo "$arrgh"; } ? >"></strong></td -->
<td style="text-align: center; vertical-align: middle;" rowspan="20" width="100px"><strong><img style="width: 100px;" src="/upload/main/020/813272jazqnvae82m1ivw5ddv6aw.png"></strong></td>
<td style="background: #800000;" colspan="3"><span style="color: white; "><strong><?=$arResult["User"]["PERSONAL_PROFESSION"]?></strong></span>
</td>
</tr>
<tr>
<td class="name" style="vertical-align: middle; font-size: 20px; height: 30px;" colspan="3"><?=$arResult["User"]["NAME_FORMATTED"]?></td>
</tr>
<tr>
<td width="120px"><b>Ученая степень:<br />Ученое звание:</b></td>
<td><?foreach($arResult["User"]["UF_ACADEMIC_DEGREE"] as $academic_degree):?>
                        <?=$academic_degree?><br />
                    <?endforeach;?>
                    <!-- ?foreach($arResult["User"]["UF_ACADEMIC_TITLE"] as $academic_title):?>
                        <!-- ?=$academic_title?><br />
                    <!-- ?endforeach;? -->
</td>
</tr>
<? if (!empty($arResult["User"]["UF_DISCIPLINES"])) { ?>
<tr>
<td><b>Преподаваемые дисциплины:</b></td>
<td>
<?if(!empty($arResult["User"]["UF_DISCIPLINES"])):?>
	                        <span>
							<ul>
	                            <?foreach($arResult["User"]["UF_DISCIPLINES"] as $discipline):?>
	                                <li><?=$discipline?></li>
	                            <?endforeach;?>
								</ul>
	                        </span>
						<?endif;?>
</td>
</tr>
<? } ?>
  <!-- остальные поля -->
  
 
<?if(
							!empty($arResult["User"]["UF_SPECIALITY"]) ||
							!empty($arResult["User"]["UF_EXERITIES"]) ||
							!empty($arResult["User"]["UF_RETRAINING"]) ||
							strlen($arResult["User"]["UF_TOTAL_EXPERIENCE"])>0 ||
							!empty($arResult["User"]["UF_AWARDS"])
							):?>
					<?if(!empty($arResult["User"]["UF_EDUCATION"])):?>
									<tr>
										<td><b><?=GetMessage("MAIN_UL_CULED_EDUCATION")?>:<b></td>
										<td>
											<?foreach($arResult["User"]["UF_EDUCATION"] as $edu):?>
												<?=$edu?><br>
											<?endforeach;?>
										</td>
									</tr>
								<?endif;?>
		                        <?if(!empty($arResult["User"]["UF_SPECIALITY"])):?>
		                            <tr>
		                                <td><b><?=GetMessage("MAIN_UL_CULED_SPICIALITY")?>:</b></td>
		                                <td>
		                                    <?foreach($arResult["User"]["UF_SPECIALITY"] as $specility):?>
		                                        <?=$specility?><br>
		                                    <?endforeach;?>
		                                </td>
		                            </tr>
								<?endif;?>
								<?if(!empty($arResult["User"]["UF_EXERITIES"])):?>
		                            <tr>
		                                <td><b><?=GetMessage("MAIN_UL_CULED_EXPERTISE")?>:</b></td>
		                                <td>
		                                    <?foreach($arResult["User"]["UF_EXERITIES"] as $exerities):?>
		                                        <?=$exerities?><br>
		                                    <?endforeach;?>
		                                </td>
		                            </tr>
								<?endif;?>
								<?if(!empty($arResult["User"]["UF_RETRAINING"])):?>
		                            <tr>
		                                <td><b><?=GetMessage("MAIN_UL_CULED_TRANING")?>:</b></td>
		                                <td>
		                                    <?foreach($arResult["User"]["UF_RETRAINING"] as $retraining):?>
		                                        <?=$retraining?><br>
		                                    <?endforeach;?>
		                                </td>
		                            </tr>
								<?endif;?>
								
								<?if(!empty($arResult["User"]["UF_AWARDS"])):?>
		                            <tr>
		                                <td><b><?=GetMessage("MAIN_UL_CULED_TOTAL_HONORS")?>:</b></td>
		                                <td>
		                                    <ul><?foreach($arResult["User"]["UF_AWARDS"] as $awards):?>
		                                        <li><?=$awards?></li>
		                                    <?endforeach;?>
											</ul>
		                                </td>
		                            </tr>
								<?endif;?>
								
		                        <?if(strlen($arResult["User"]["UF_TOTAL_EXPERIENCE"])>0):?>
		                            <tr>
		                                <td><b><?=GetMessage("MAIN_UL_CULED_TOTAL_EXPERIENCE")?>:</b></td>
		                                <td><?=$arResult["User"]["UF_TOTAL_EXPERIENCE"]?></td>
		                            </tr>
								<?endif;?>
								<?if(strlen($arResult["User"]["UF_CPEC"])>0):?>
									<tr>
										<td><b><?=GetMessage("MAIN_UL_CULED_SPEC_EXPERIENCE")?>:</b></td>
										<td><?=$arResult["User"]["UF_CPEC"]?></td>
									</tr>
								<?endif;?>
		                        
	                     
						<?endif;?>

   </tbody>
</table>						
						<?if(strlen($arResult["User"]["PERSONAL_NOTES"])):?>
	                        <hr>
	                        <h3><b><?=GetMessage("MAIN_UL_CULED_DETAIL_INFORMATION")?>:<b></h3>
	                        <div class="about_teacher" style="width:100%">
	                            <p>
	                                <?=$arResult["User"]["PERSONAL_NOTES"]?>
	                            </p>
	                        </div>
						<?endif;?>
		
		
    </div>
<?
}
?>