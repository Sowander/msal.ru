<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<?if(!empty($arResult["ITEMS"])):?>
    <div class="info_title"><?=GetMessage("UPCOMING_".$arParams["IBLOCK_ID"])?></div>

    <div class="news-line">
        <?foreach($arResult["ITEMS"] as $arItem):?>
            <?
            $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
            $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
            ?>
<!--            --><?//pre_dump($arItem["PROPERTIES"])?>
            <div class="short_information" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                <div class="date">
	                <?if($arItem["IBLOCK_ID"]=="8"):?>
		                <?=explode(" ",$arItem["DATE_CREATE"])[0]?>
					<?elseif($arItem["IBLOCK_ID"]=="6"):?>
		                <?=explode(" ",$arItem["PROPERTIES"]["UF_START_EVENT"]["VALUE"])[0]?>
	                <?endif;?>
                </div>
                <div class="info_text">
                    <p>
                        <?echo $arItem["NAME"]?>
                    </p>
                </div>
                <?if(isset($arItem["CREATED_USER_NAME"]) && strlen($arItem["CREATED_USER_NAME"])>0 && isset($arItem["PROPERTIES"]["UF_AUTOR"]["VALUE"]) && strlen($arItem["PROPERTIES"]["UF_AUTOR"]["VALUE"])>0):?>
                <div class="info_press_sl">
                    <?if($arItem["PROPERTIES"]["UF_AUTOR"]["VALUE"] && isset($arItem["PROPERTIES"]["UF_AUTOR"]["VALUE"]) && strlen($arItem["PROPERTIES"]["UF_AUTOR"]["VALUE"])>0):?>
                        <?echo $arItem["PROPERTIES"]["UF_AUTOR"]["VALUE"]?>
                    <?else:?>
                        <?echo $arItem["CREATED_USER_NAME"]?>
                    <?endif;?>
                </div>
                <?endif;?>
                <div class="readmore">
                    <a href="<?echo $arItem["DETAIL_PAGE_URL"]?>"><?=GetMessage("DETAILS")?></a>
                </div>
            </div>
        <?endforeach;?>
    </div>

    <div class="red_button low">
        <a href="<?=$GLOBALS["arSiteLanguageParams"]["UF_LINK"]?><?=GetMessage("SECTION_".$arParams["IBLOCK_ID"]."_URL")?>"><?=GetMessage("ALL_".$arParams["IBLOCK_ID"])?></a>
    </div>
<?endif;?>