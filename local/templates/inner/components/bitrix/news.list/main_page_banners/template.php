<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="bg_slider">
    <div class="awesome_container">
        <div class="awesome_slider">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <div class="awesome_slide">
        <img
            src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" class="cut"
            alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
            title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
            />
        <div class="left_side">
            <div class="content">
                <h3>
                    <?foreach($arItem["DISPLAY_PROPERTIES"]["UF_TITLES"]["VALUE"] as $key=>$elem_title):?>
                        <?=trim($elem_title)?>
                        <?if(next($arItem["DISPLAY_PROPERTIES"]["UF_TITLES"]["VALUE"])):?>
                            <br />
                        <?endif;?>
                    <?endforeach;?>
                </h3>
                <p>
                </p>
                <div class="red_button">
                    <a href="<?=$arItem["DISPLAY_PROPERTIES"]["UF_LINK"]["VALUE"]?>"><?=GetMessage("READ_MORE");?></a>
                </div>
            </div>
        </div>
    </div>
<?endforeach;?>
        </div>
        <div class="next_button_container">
            <div class="next_button"></div>
        </div>
    </div>
</section>


<script type="text/javascript" language="javascript">

    // Мешалка анимаций. Возвращает массив с двумя значениями, первое "входная" анимация слайда, второе "выходная" анимация.
    function shuffleTypeOfAnimations () {
        var arrStringNameAnimations = [];

        var animationsIn = [
            'bounceIn',
            'bounceInDown',
            'bounceInLeft',
            'bounceInRight',
            'bounceInUp',
            'fadeIn',
            'fadeInDown',
            'fadeInDownBig',
            'fadeInLeft',
            'fadeInLeftBig',
            'fadeInRight',
            'fadeInRightBig',
            'fadeInUp',
            'fadeInUpBig',
            'flipInX',
            'flipInY',
            'lightSpeedIn',
            'rotateIn',
            'rotateInDownLeft',
            'rotateInDownRight',
            'rotateInUpLeft',
            'rotateInUpRight',
            'rollIn',
            'zoomIn',
            'zoomInDown',
            'zoomInLeft',
            'zoomInRight',
            'zoomInUp',
            'slideInDown',
            'slideInLeft',
            'slideInRight',
            'slideInUp'
        ];
        var animationsOut = [
            'bounceOut',
            'bounceOutDown',
            'bounceOutLeft',
            'bounceOutRight',
            'bounceOutUp',
            'fadeOut',
            'fadeOutDown',
            'fadeOutDownBig',
            'fadeOutLeft',
            'fadeOutLeftBig',
            'fadeOutRight',
            'fadeOutRightBig',
            'fadeOutUp',
            'fadeOutUpBig',
            'flipOutX',
            'flipOutY',
            'lightSpeedOut',
            'rotateOut',
            'rotateOutDownLeft',
            'rotateOutDownRight',
            'rotateOutUpLeft',
            'rotateOutUpRight',
            'rollOut',
            'zoomOut',
            'zoomOutDown',
            'zoomOutLeft',
            'zoomOutRight',
            'zoomOutUp',
            'slideOutDown',
            'slideOutLeft',
            'slideOutRight',
            'slideOutUp'
        ];

        var maxLengthOfAnim = animationsIn.length-1;

        var randDigit =  Math.floor(Math.random() * maxLengthOfAnim);

        arrStringNameAnimations[0] = animationsIn[Math.floor(Math.random() * maxLengthOfAnim)];
        arrStringNameAnimations[1] = animationsOut[Math.floor(Math.random() * maxLengthOfAnim)];

        return arrStringNameAnimations;
    }

    var oldAnim = []; // Глобальный массив для запоминания "предыдущих" анимаций (юзается дальше в коллбэке)
    $(document).ready(function() {
        var mainCarousel = $(".awesome_slider");

        // Инициализатор для браузеров, поддерживающих css3 animation
        if (animation.supported) {
            mainCarousel.owlCarousel({
                items : 1,
                loop : true,
                dots : true,
                animateIn : true,
                animateOut : true,
                autoplay : true,
                nav : true,
                autoplayHoverPause : true,
                callbacks : true,
                mouseDrag : false,
                touchDrag : false,
                onChange : function(){
                    var owl = mainCarousel.data('owlCarousel');
                    var diffAnim = shuffleTypeOfAnimations();
                    owl._plugins.animate.core.settings.animateIn = diffAnim[0]; // Никакого более способа, кроме как прям ВНУТРИ ЭТО УЖАСНОГО плагина, прописать анимацию не нашёл. РАБОТАЕТ!
                    owl._plugins.animate.core.settings.animateOut = diffAnim[1]; // только надо подчищать классы, ниже в коллбэке это и происходит
                    owl.options.animateIn = diffAnim[0]; // Эти две строчки чтоб уж наверняка!!!
                    owl.options.animateOut = diffAnim[1];

                    oldAnim[0] = diffAnim[0]; // Запоминает данную анимацию.
                    oldAnim[1] = diffAnim[1]; // В след. коллбэке чистит классы у слайдов.
                },
                onChanged : function () {
                    $(".owl-item").removeClass(oldAnim[0]); // Чистка слайдов от классов, после анимации
                    $(".owl-item").removeClass(oldAnim[1]); // Если этого не делать, то классы остаются и анимация перестаёт работать (т.е. классы всё время разные подставляются)
                }
            });
        } else {
            // Иницализатор для браузеров, не поддерживающих css3 animation
            mainCarousel.owlCarousel({
                items : 1,
                loop : true,
                dots : true,
                autoplay : true,
                nav : true,
                autoplayHoverPause : true,
                callbacks : true,
                mouseDrag : false,
                touchDrag : false
            });
        }


        var owl = mainCarousel.data('owlCarousel'); // Объект-слайдер. Для дальнейших операций.



    });
</script>