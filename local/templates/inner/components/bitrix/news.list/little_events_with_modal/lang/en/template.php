<?
$MESS["CT_BNL_ELEMENT_DELETE_CONFIRM"] = "All the information linked to this record will be deleted. Continue anyway?";
$MESS["UPCOMING_6"] = "Upcoming Events";
$MESS["UPCOMING_8"] = "PREVIEW NEWS";
$MESS["DETAILS"] = "Read more";
$MESS["ALL_EVENTS"] = "All events";
$MESS["ALL_6"] = "All events";
$MESS["ALL_8"] = "All news";
$MESS["ALL_9"] = "Reference Information";
$MESS["SECTION_6_URL"] = "/events/";
$MESS["SECTION_8_URL"] = "/news/";
?>