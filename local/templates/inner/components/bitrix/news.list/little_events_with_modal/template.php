<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<div class="info_title"><?=GetMessage("UPCOMING_".$arParams["IBLOCK_ID"])?></div>

<div class="news-line">
    <?foreach($arResult["ITEMS"] as $arItem):?>
        <?
        $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
        $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
        ?>
<!--        --><?//pre_dump($arItem["PROPERTIES"])?>
        <div class="short_information" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
            <div class="date"><?echo $arItem["DATE_CREATE"]?></div>
            <div class="info_text">
                <p>
                    <?echo $arItem["NAME"]?>
                </p>
            </div>
            <?if(isset($arItem["CREATED_USER_NAME"]) && strlen($arItem["CREATED_USER_NAME"])>0 && isset($arItem["PROPERTIES"]["UF_AUTOR"]["VALUE"]) && strlen($arItem["PROPERTIES"]["UF_AUTOR"]["VALUE"])>0):?>
            <div class="info_press_sl">
                <?if($arItem["PROPERTIES"]["UF_AUTOR"]["VALUE"] && isset($arItem["PROPERTIES"]["UF_AUTOR"]["VALUE"]) && strlen($arItem["PROPERTIES"]["UF_AUTOR"]["VALUE"])>0):?>
                    <?echo $arItem["PROPERTIES"]["UF_AUTOR"]["VALUE"]?>
                <?else:?>
                    <?echo $arItem["CREATED_USER_NAME"]?>
                <?endif;?>
            </div>
            <?endif;?>
            <div class="readmore">
                <a
                    class="various"
                    href="#important_news_all_<?=$arItem["ID"]?>"
                    onclick="impotant_data_set_cookie('<?=$arItem['ID']?>');">
                    <?=GetMessage("DETAILS")?>
                </a>
            </div>
        </div>
    <?endforeach;?>
</div>


<?$APPLICATION->IncludeComponent(
    "bitrix:news.list",
    "important_information_all",
    array(
        "DISPLAY_DATE" => "N",
        "DISPLAY_NAME" => "Y",
        "DISPLAY_PICTURE" => "N",
        "DISPLAY_PREVIEW_TEXT" => "N",
        "AJAX_MODE" => "N",
        "IBLOCK_TYPE" => "content",
        "IBLOCK_ID" => "9",
        "NEWS_COUNT" => "3",
        "SORT_BY1" => "ACTIVE_FROM",
        "SORT_ORDER1" => "DESC",
        "SORT_BY2" => "SORT",
        "SORT_ORDER2" => "ASC",
        "FILTER_NAME" => "arFilterLanguage",
        "FIELD_CODE" => array(
            0 => "ID",
            1 => "CODE",
            2 => "SORT",
            3 => "PREVIEW_TEXT",
            4 => "DATE_ACTIVE_FROM",
            5 => "ACTIVE_FROM",
            6 => "DATE_ACTIVE_TO",
            7 => "ACTIVE_TO",
            8 => "DATE_CREATE",
            9 => "CREATED_USER_NAME",
            10 => "",
        ),
        "PROPERTY_CODE" => array(
            0 => "UF_IMPORTANT",
            1 => "UF_LANGUAGE",
            2 => "",
            3 => "",
        ),
        "CHECK_DATES" => "Y",
        "DETAIL_URL" => "",
        "PREVIEW_TRUNCATE_LEN" => "",
        "ACTIVE_DATE_FORMAT" => "d.m.Y",
        "SET_TITLE" => "N",
        "SET_BROWSER_TITLE" => "N",
        "SET_META_KEYWORDS" => "Y",
        "SET_META_DESCRIPTION" => "Y",
        "SET_LAST_MODIFIED" => "Y",
        "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
        "ADD_SECTIONS_CHAIN" => "N",
        "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
        "PARENT_SECTION" => "",
        "PARENT_SECTION_CODE" => "",
        "INCLUDE_SUBSECTIONS" => "N",
        "CACHE_TYPE" => "N",
        "CACHE_TIME" => "3600",
        "CACHE_FILTER" => "Y",
        "CACHE_GROUPS" => "Y",
        "DISPLAY_TOP_PAGER" => "N",
        "DISPLAY_BOTTOM_PAGER" => "N",
        "PAGER_TITLE" => "Новости",
        "PAGER_SHOW_ALWAYS" => "N",
        "PAGER_TEMPLATE" => "",
        "PAGER_DESC_NUMBERING" => "Y",
        "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
        "PAGER_SHOW_ALL" => "N",
        "PAGER_BASE_LINK_ENABLE" => "N",
        "SET_STATUS_404" => "N",
        "SHOW_404" => "N",
        "MESSAGE_404" => "",
        "PAGER_BASE_LINK" => "",
        "PAGER_PARAMS_NAME" => "arrPager",
        "AJAX_OPTION_JUMP" => "N",
        "AJAX_OPTION_STYLE" => "Y",
        "AJAX_OPTION_HISTORY" => "N",
        "AJAX_OPTION_ADDITIONAL" => "",
        "COMPONENT_TEMPLATE" => "important_information"
    ),
    false
);?>