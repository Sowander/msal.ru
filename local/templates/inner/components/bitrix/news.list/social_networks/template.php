<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<?if($arParams["SHOW_H1"]=="Y"):?>
    <h4><?=GetMessage('SOCIAL_NETWORKS')?></h4>
<?endif;?>
<div class="foot_info">
    <ul class="socials">
<?foreach($arResult["ITEMS"] as $arItem):?>
	<?
	$this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
	$this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
	?>
    <li class="vk">
        <a href="<?=$arItem["DISPLAY_PROPERTIES"]["UF_URL_LINK"]["VALUE"]?>">
            <img src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>" alt=""/>
            <img class="to-hover" src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>" alt="<?=$arItem["NAME"]?>"/>
        </a>
    </li>

<?endforeach;?>
    </ul>
</div>
