<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 08.09.2015
 * Time: 13:13
 */

foreach($arResult["ITEMS"] as &$arItem)
{
    $lang_name_field = "UF_NAME_".strtoupper(LANGUAGE_ID);
    $arItem["UF_NAME"] = $arItem["PROPERTIES"][$lang_name_field]["VALUE"];
    unset($lang_name_field);
}

?>