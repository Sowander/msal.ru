<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>


<div class="helper inner_page_content" style="display: inline-block;">
    <div class="helper">
        <div class="main_header" style="margin-bottom:1em;width:100% !important;">
		<!-- h4>Справочная система<!-- ?=GetMessage("CHFL_HELPFROM_STUDENTS")?></h4><br / -->
		<span style="font-size:14px;">Введите запрос в поисковую строку или выберите категорию.<br />Используя уточняющие вопросы, получите необходимый Вам ответ.</span>		
		</div>
        <div class="edu_act_container">
            <?foreach($arResult["ITEMS"] as $arItem):?>
                <?
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                ?>
                    <div class="edu_act_item" id="<?=$this->GetEditAreaId($arItem['ID']);?>">
                        <div class="edu_act_img">
                            <a href="/handbook/?UF_THEMES_DIRECTORY=<?=$arItem["ID"]?>">
                                <?if(strlen($arItem["PREVIEW_PICTURE"]["SRC"])>0):?>
                                    <img
                                        src="<?=$arItem["PREVIEW_PICTURE"]["SRC"]?>"
                                        alt="<?=$arItem["PREVIEW_PICTURE"]["ALT"]?>"
                                        title="<?=$arItem["PREVIEW_PICTURE"]["TITLE"]?>"
                                        />
                                <?endif;?>
                                <?if(strlen($arItem["DETAIL_PICTURE"]["SRC"])>0):?>
                                    <img
                                        class="to-hover"
                                        src="<?=$arItem["DETAIL_PICTURE"]["SRC"]?>"
                                        alt="<?=$arItem["DETAIL_PICTURE"]["ALT"]?>"
                                        title="<?=$arItem["DETAIL_PICTURE"]["TITLE"]?>"
                                        />
                                <?endif;?>
                                <span><?=$arItem["UF_NAME"]?></span>
                            </a>
                        </div>
                    </div>
            <?endforeach;?>
        </div>
    </div>
</div>