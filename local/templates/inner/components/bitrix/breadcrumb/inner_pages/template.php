<?php
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

/**
 * @global CMain $APPLICATION
 */

global $APPLICATION;

//delayed function must return a string
if(empty($arResult))
	return "";

$itemSize = count($arResult);

$strReturn .= '<div class="breadcrumbs no_margin"><ul>';
for($index = 0; $index < $itemSize; $index++)
{
    $title = htmlspecialcharsex($arResult[$index]["TITLE"]);
    if(in_array($title, $GLOBALS["breadcrumbsNameExclusion"]))
    {
        continue;
    }
    if(isset($GLOBALS["breadcrumbsChange"][$title]) && strlen($GLOBALS["breadcrumbsChange"][$title])>0)
    {
        $strReturn .= '<li><a href="'.$GLOBALS["breadcrumbsChange"][$title].'">'.$title.'</a></li>';
    }
    else
    {
        $strReturn .= '<li><a href="'.$arResult[$index]["LINK"].'">'.$title.'</a></li>';
    }
}

$strReturn .= '</ul></div>';
return $strReturn;
