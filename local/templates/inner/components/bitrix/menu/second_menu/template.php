<?if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();?>

<?if (!empty($arResult)):?>
<menu class="small_menu">
    <ul class="wrapper_container justifyfix">

<?
foreach($arResult as $arItem):
	if($arParams["MAX_LEVEL"] == 1 && $arItem["DEPTH_LEVEL"] > 1) 
		continue;
?>
    <li<?if($arItem["TEXT"] == "Личный кабинет" || $arItem["TEXT"] == "Personal Area"):?> class="modal_windows"<?endif;?>>
	<?if($arItem["SELECTED"]):?>
		<a class="<?=$arItem["PARAMS"]["dif_class"]?>" href="<?=$arItem["LINK"]?>" class="selected"><span><?=$arItem["TEXT"]?></span></a>
	<?else:?>
		<a class="<?=$arItem["PARAMS"]["dif_class"]?>" href="<?=$arItem["LINK"]?>"><span><?=$arItem["TEXT"]?></span></a>
	<?endif?>
    </li>
<?endforeach?>
        <!-- Languages -->
        <?$APPLICATION->IncludeComponent("bitrix:highloadblock.list", "languages", Array(
                "BLOCK_ID" => "3",	// ID инфоблока
            ),
            false
        );?>
    </ul>
</menu>
<?endif?>