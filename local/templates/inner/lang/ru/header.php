<?php
/**
 * Created by PhpStorm.
 * User: Bondarenko Georg
 * Date: 31.07.2015
 * Time: 19:41
 */

$MESS['CONTACT_INFO'] = "Контактная информация";
$MESS['HANDBOOK_INFO_NAME'] = "Справочная информация";
$MESS['VERSION_VISUALLY_IMPAIRED'] = "Версия для слабовидящих";
$MESS['VERSION_VISUALLY_IMPAIRED_FULL'] = "Обычная версия";
?>