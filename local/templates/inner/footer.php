<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
IncludeTemplateLangFile(__FILE__);
?>

</div>
</section>
<footer>


    <section class="bottom">
        <div class="wrapper_container">
            <!-- Copyright -->
            <div class="copy left">
                <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => "/includes/".LANGUAGE_ID."/copyright.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    false
                );?>
            </div>
           
        </div>
    </section>
</footer>

<!-- Подключаемые файлы для IE, скрипты, плагины и тд. -->
<!--[if lt IE 9]>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/libs/html5shiv/es5-shim.min.js");?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/libs/html5shiv/html5shiv.min.js");?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/libs/html5shiv/html5shiv-printshiv.min.js");?>
<![endif]-->
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/libs/jquery/jquery-1.11.1.min.js");?>
<!-- Skitter JS -->
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/libs/jquery-mousewheel/jquery.mousewheel.min.js");?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/libs/fancybox/jquery.fancybox.pack.js");?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/libs/scrollto/jquery.scrollTo.min.js");?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/libs/owl-carousel2/owl.carousel.js");?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/common.js");?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/gallery.js");?>

		

<!-- /Подключаемые файлы для IE, скрипты, плагины и тд. -->
<link href="/local/templates/inner/css/mgua.css" rel="stylesheet" type="text/css">

	
<div class="form-loader">
	<div class="form-loader__inner">Пожалуйста, подождите, идёт загрузка данных...</div>
</div>
	

	
</body>
</html>