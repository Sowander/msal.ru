<?php header('X-UA-Compatible: IE=edge'); ?>
<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
IncludeTemplateLangFile(__FILE__);
?><!DOCTYPE html>
<!--[if lt IE 7]><html lang="ru" class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html lang="ru" class="lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html lang="ru" class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="ru">
<!--<![endif]-->
	<head>
		<?$APPLICATION->ShowHead();?>
		<title><?$APPLICATION->ShowTitle();?></title>
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

		
        <!-- Skitter Styles -->
        <link href='//fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>

        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/libs/font-awesome-4.2.0/css/font-awesome.min.css");?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/libs/fancybox/jquery.fancybox.css");?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/libs/owl-carousel2/owl.carousel.css");?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/reset.css");?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/fonts.css");?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/animate.css");?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/main.css");?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/disable_1.css");?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/mgua.css");?>



<!-- lk-cbox -->
		<link rel="stylesheet" href="" />
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/libs/colorbox/colorbox.css");?>
	<!-- script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script -->
		<script src="/local/templates/main/libs/colorbox/jquery.colorbox.js"></script>
		<script>
			$(document).ready(function(){
				//Examples of how to assign the Colorbox event to elements
				$(".iframe").colorbox({iframe:true, width:"50%", height:"60%"});
				$(".inlinecol").colorbox({inline:true, width:"50%"});
				$(".callbacks").colorbox({
					onOpen:function(){ alert('onOpen: colorbox is about to open'); },
					onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
					onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
					onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
					onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
				});
				//Example of preserving a JavaScript event for inline calls.
				$("#click").click(function(){ 
					$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
					return false;
				});
			});
		</script>

		
		<script>
$(document).ready(function() {
	$(".variousss").fancybox({
		maxWidth	: 1024,
		maxHeight	: 950,
		fitToView	: false,
		width		: '100%',
		height		: '100%',
		autoSize	: false,
		closeClick	: false,
		openEffect	: 'none',
		closeEffect	: 'none'
	});
});
</script>


<script type="text/javascript" src="/common/js/jquery.cycle.js"></script><script type="text/javascript">
$(document).ready(function() {
    $('.slideshow').cycle({
        fx: 'fade',
        speed: 2000, 
        timeout: 5000
    });
});
</script><script type="text/javascript">
$(document).ready(function() {
    $('.slideshow1').cycle({
        fx: 'fade',
        speed: 1, 
        timeout: 4000
    });
});
</script><script type="text/javascript" src="/common/js/fan/fancybox/jquery.mousewheel-3.0.4.pack.js"></script><script>
$(document).ready(function() {
	$(".fancybox-thumb").fancybox({
		prevEffect	: 'none',
		nextEffect	: 'none',
		helpers	: {
			title	: {
				type: 'outside'
			},
			thumbs	: {
				width	: 50,
				height	: 50
			}
		}
	});
});
</script>


<!-- lk-cbox -->


<!-- стилевые правки МГЮА тест широкой таблицы -->
<link rel="stylesheet" href="/common/js/responsive-tables.css">
<script type="text/javascript" src="/common/js/responsive-tables.js"></script>
<!-- /стилевые правки МГЮА тест широкой таблицы -->

<!-- скрипт спойлера 1 МГЮА -->
<script type="text/javascript">
function displ(ddd)
{
if (document.getElementById(ddd).style.display == 'none') {document.getElementById(ddd).style.display = 'block'}
else {document.getElementById(ddd).style.display = 'none'}
}
</script>
<!-- /скрипт спойлера 1 МГЮА -->

<!-- /скрипт спойлера 2 МГЮА -->
<script type="text/javascript">
  $(document).ready(function(){
    $('.splLink').click(function() {
      cont = $(this).parent().children('.splCont');
      $('.splCont').each(function() {
        if ($(this)[0] != cont[0] && $(this).hasClass('show'))
          $(this).hide('normal', function() { $(this).removeClass('show'); });
      });
      if (cont.hasClass('show'))
        cont.hide('normal', function() { $(this).removeClass('show'); });
      else
        cont.show('normal', function() { $(this).addClass('show'); });
      return false;
    });
  });
</script>
<!-- /скрипт спойлера 2 МГЮА -->

<!-- скрипт pdf МГЮА -->
<script type="text/javascript" src="/common/js/fan/pdf/jquery.gdocsviewer.min.js"></script> 
<script type="text/javascript"> 
/*<![CDATA[*/
$(document).ready(function() {
	$('a.embed').gdocsViewer({width: 750, height: 800});
	$('#embedURL').gdocsViewer();
});
/*]]>*/
</script> 
<!-- /скрипт pdf МГЮА -->

<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter50644546 = new Ya.Metrika({
                    id:50644546,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "https://mc.yandex.ru/metrika/watch.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks");
</script>
<noscript><div><img src="https://mc.yandex.ru/watch/50644546" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->


	</head>
    <body>
		<div id="panel">
			<?$APPLICATION->ShowPanel();?>
		</div>
        <header>

          
            <section class="top_head">
                <ul class="wrapper_container">
                    <li class="logo">
                        <a href="<?if(isset($arSiteLanguageParams) && strlen($arSiteLanguageParams["UF_LINK"])>0):?><?=$arSiteLanguageParams["UF_LINK"]?><?else:?>/<?endif;?>">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/logo.png" alt="МГЮА"/>
                        </a>
                        <i class="corner"></i>
                    </li>
                    <li class="logo_text">
                        <p>
                            <!-- University name -->
                            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                                    "AREA_FILE_SHOW" => "file",
                                    "PATH" => "/includes/".LANGUAGE_ID."/university_name.php",
                                    "EDIT_TEMPLATE" => ""
                                ),
                                false
                            );?>
                        </p>
                        <span class="little_text">Non scholae sed vitae discimus</span>
                    </li>


                   <li class="important_links" style="margin-top:1em;width: 150px;">
                        <ul>
                            <li>
								<div class="modal_windows">
									<a target="_blank" href="/common/">
										<img src="/local/templates/main/img/top_screen.gif" alt="">
										<span>Обратная связь</span>
									</a>
								</div>
							</li>
						</ul>
					</li>
					<li class="logo_text" style="width: 40%;">
    <!-- noindex --><noindex>                    <p style="font-size: .9em; color: #d22593;">
                       Уважаемые пользователи!<br>
    Ежедневно с 21:30 до 22:30 на серверах проводятся профилактические наладочные работы.<br>Просим Вас в этот период воздержаться от использования личного кабинета</p>
                        <span class="little_text" style="font-size: .6em;animation: 4s linear 0s infinite alternate move_eye;">С уважением, разработчики</span></noindex><!-- /noindex -->
                    </li>

                </ul>
            </section>

           

        </header>


      

        <?if(strripos($APPLICATION->GetCurDir(),"/events/") !== false || strripos($APPLICATION->GetCurDir(),"/news/") !== false):?>
            <?
                /** Sorry by this hard fix */
                if(strripos($APPLICATION->GetCurDir(),"/events/") !== false)
                {
                    $calendar_iblock_id = "6";
                    $sort_field = "PROPERTY_UF_START_EVENT";
                }
                if(strripos($APPLICATION->GetCurDir(),"/news/") !== false)
                {
                    $calendar_iblock_id = "8";
                    $sort_field = "DATE_ACTIVE_FROM";
                }
            ?>
            <?$APPLICATION->IncludeComponent(
                "bit:content_calendar",
                "",
                array(
                    "IBLOCK_ID"     =>  $calendar_iblock_id,
                    "SORT_FIELD"    =>  $sort_field
                ),
                false
            );?>
            <div class="grey_separator"></div>
        <?endif;?>
        <section class="inner_page white">
            <div class="wrapper_container clearfix">