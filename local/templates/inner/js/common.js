// Disable version. Font-size increase functions

var resetDisableScriptStyles = function () {
    $('*').each(function(){
        var redSize = "" ; //here, you can give the percentage( now it is reduced to 90%)
        $(this).css('font-size',redSize);

    });
};

var increaseFonts = function (howMuchPercent) {
    resetDisableScriptStyles();
    var increaseVar = 100 +  howMuchPercent;
    $('a, span, p, h1, h2, h3, h4, h5, h6, i, s').each(function(){
        var k =  parseInt($(this).css('font-size'));
        //console.log(this.nodeName + "." + this.className + " fz:" + k);
        var redSize = ((k*increaseVar)/100) ; //here, you can give the percentage( now it is reduced to 90%)
        $(this).css('font-size',redSize);

    });
};



if(getCookie("disable_vers") == "true") {
    enableDisableVersion();

    if(getCookie("font") == "bigger") {
        resetDisableScriptStyles();
        setTimeout(increaseFonts(15),0);
    } else if (getCookie("font") == "biggest"){
        resetDisableScriptStyles();
        setTimeout(increaseFonts(30),0);
    }

    if(getCookie("background") == "white"){
        $("body").addClass("white_bg");
    } else if(getCookie("background") == "black"){
        $("body").addClass("black_bg");
    } else if(getCookie("background") == "blue"){
        $("body").addClass("blue_bg");
    }

    if(getCookie("letter_space") == "bigger"){
        $("body").removeClass("letspace2").addClass("letspace1");
    } else if(getCookie("letter_space") == "biggest") {
        $("body").removeClass("letspace1").addClass("letspace2");
    }
}

function enableDisableVersion() {
    $("#disable_vers").addClass("enabled");
    $("body").addClass("disable_vers");
}

function disableChecker(link) {
    if(!link.hasClass("enabled")) {
        link.addClass("enabled");
        $("body").addClass("disable_vers");

        setCookie("disable_vers", "true", {
            path: "/"
        });
    } else {
        link.removeClass("enabled");
        $("body").removeClass("disable_vers");
        resetDisableScriptStyles();
        deleteCookie("disable_vers");
        deleteCookie("background");
        deleteCookie("letter_space");
        deleteCookie("font");
    }
}

function getCookie(name) {
    var matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
}

function deleteCookie(name) {
    setCookie(name, "", {
        expires: -1
    })
}

function setCookie(name, value, options) {
    options = options || {};

    var expires = options.expires;

    if (typeof expires == "number" && expires) {
        var d = new Date();
        d.setTime(d.getTime() + expires * 1000);
        expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
        options.expires = expires.toUTCString();
    }

    value = encodeURIComponent(value);

    var updatedCookie = name + "=" + value;

    for (var propName in options) {
        updatedCookie += "; " + propName;
        var propValue = options[propName];
        if (propValue !== true) {
            updatedCookie += "=" + propValue;
        }
    }
    
    console.log(updatedCookie)

    document.cookie = updatedCookie;
}





$(".default_a").on("click", function (e) {
    e.preventDefault();
    resetDisableScriptStyles();
    deleteCookie("font");
});

$(".bigger_a").on("click", function (e) {
    e.preventDefault();
    deleteCookie("font");
    resetDisableScriptStyles();
    setTimeout(increaseFonts(15),0);
    setCookie("font", "bigger", {
        path: "/"
    });
});

$(".biggest_a").on("click", function (e) {
    e.preventDefault();
    deleteCookie("font");
    resetDisableScriptStyles();
    setTimeout(increaseFonts(30),0);
    setCookie("font", "biggest", {
        path: "/"
    });
});

// letter space

$(".default_letter").on("click", function (e) {
    e.preventDefault();
    deleteCookie("letter_space");
    $("body").removeClass("letspace2").removeClass("letspace1");
});

$(".bigger_letter").on("click", function (e) {
    e.preventDefault();
    deleteCookie("letter_space");
    $("body").removeClass("letspace2").addClass("letspace1");
    setCookie("letter_space", "bigger", {
        path: "/"
    });
});

$(".biggest_letter").on("click", function (e) {
    e.preventDefault();
    deleteCookie("letter_space");
    $("body").removeClass("letspace1").addClass("letspace2");
    setCookie("letter_space", "biggest", {
        path: "/"
    });
});

// Bg

$(".white_square").on("click", function (e) {
    e.preventDefault();
    deleteCookie("background");
    $("body").removeClass("black_bg")
    $("body").removeClass("blue_bg")
    $("body").addClass("white_bg");
    setCookie("background", "white", {
        path: "/"
    });
});

$(".black_square").on("click", function (e) {
    e.preventDefault();
    deleteCookie("background");
    $("body").removeClass("blue_bg")
    $("body").removeClass("white_bg")
    $("body").addClass("black_bg");
    setCookie("background", "black", {
        path: "/"
    });
});

$(".blue_square").on("click", function (e) {
    e.preventDefault();
    deleteCookie("background");
    $("body").removeClass("black_bg")
    $("body").removeClass("white_bg")
    $("body").addClass("blue_bg");
    setCookie("background", "blue", {
        path: "/"
    });
});



var isSafari = /^((?!chrome).)*safari/i.test(navigator.userAgent);

function CSSAnimation(){
    /*
    Проверка поддержки css анимации

    webkitAnimationName => Safari/Chrome
    MozAnimationName => Mozilla Firefox
    OAnimationName => Opera
    animationName => compliant browsers (inc. IE10)
     */
    var supported = false;
    var prefixes = ['webkit', 'Moz', 'O', ''];
    var limit = prefixes.length;
    var doc = document.documentElement.style;
    var prefix, start, end;

    while (limit--) {
        // If the compliant browser check (in this case an empty string value) then we need to check against different string (animationName and not prefix + AnimationName)
        if (!prefixes[limit]) {
            // If not undefined then we've found a successful match
            if (doc['animationName'] !== undefined) {
                prefix = prefixes[limit];
                start = 'animationstart';
                end = 'animationend';
                supported = true;
                break;
            }
        }
        // Other brower prefixes to be checked
        else {
            // If not undefined then we've found a successful match
            if (doc[prefixes[limit] + 'AnimationName'] !== undefined) {
                prefix = prefixes[limit];

                switch (limit) {
                    case 0:
                        //  webkitAnimationStart && webkitAnimationEnd
                        start = prefix.toLowerCase() + 'AnimationStart';
                        end = prefix.toLowerCase() + 'AnimationEnd';
                        supported = true;
                        break;

                    case 1:
                        // animationstart && animationend
                        start = 'animationstart';
                        end = 'animationend';
                        supported = true;
                        break;

                    case 2:
                        // oanimationstart && oanimationend
                        start = prefix.toLowerCase() + 'animationstart';
                        end = prefix.toLowerCase() + 'animationend';
                        supported = true;
                        break;
                }

                break;
            }
        }
    }

    return {
        supported: supported,
        prefix: prefix,
        start: start,
        end: end
    };
}

var animation = CSSAnimation();

var tabsDefaults = $.extend( {
    tabsContentContainer : $(".slided_tabs"),
    tabsContentWrapper : $(".slided_tabs .slided_tabs_wrapper"),
    tabContentClass : "slided_tab",
    tabMenuItemClass : "link_tab",
    clickedLink : null
});

var tabsOptions;

function animatedTabs (params) {

    tabsOptions = $.extend({}, tabsDefaults, tabsOptions, params);
    options = tabsOptions;

    tabsContentContainer = options.tabsContentContainer;
    tabsContentWrapper = options.tabsContentWrapper;
    tabContentClass  = options.tabContentClass;
    tabMenuItemClass = options.tabMenuItemClass;
    $this = options.clickedLink;

    //var numOfContentTabs = tabsContentContainer.find(tabContentClass).length; // Кол-во табов с контентом
    //var lastContentTabIndex = numOfContentTabs - 1; //Индекс последнего

    var thisLinkParent = $this.closest("li");

    var listOfTabsObj = tabsContentContainer.find(tabContentClass); //Выборка табов

    var idNeededContentTab ="#" + $this.data("tabid"); // айди таба, к которому переключаемся.
    //var emptyLeftOffset = tabsContentContainer.offset().left; // Ненужный отступ с пустотой до левого края экрана. (его будем отнимать от других оффсетов)

    var wrapperOffset = tabsContentWrapper.offset().left;
    var taboffset = $(idNeededContentTab).offset().left;

    var diff =  wrapperOffset - taboffset;

    // Move start

    if(!thisLinkParent.hasClass("active")) {
        $this.closest("ul").find("." + tabMenuItemClass).removeClass("active");
        listOfTabsObj.removeClass("current");
        thisLinkParent.addClass("active");

        if (animation.supported) {
            tabsContentWrapper.css({left: diff + "px"});
        } else {
            tabsContentWrapper.animate({left: diff + "px"});
        }
        $(idNeededContentTab).addClass("current");
    } else {
        return false;
    }
}

var mainCarousel;

// Обработчик клика и переключатель версии для слабовидящих
$("#disable_vers").on("click", function (e) {
    e.preventDefault();

    disableChecker($(this));
    if(mainCarousel.length) {
    mainCarousel.data("owlCarousel").onResize();
    }
});

$(document).ready(function() {

    mainCarousel = $(".awesome_slider");
    mainCarousel.owlCarousel({
        items : 2,
        loop : true,
        dots : true,
        autoplay : true,
        nav : true,
        autoWidth: true,
        autoplayHoverPause : true,
        callbacks : true,
        mouseDrag : true,
        touchDrag : true,
        center: true
    });

    $(".tabs li a").on("click", function(e){
        e.preventDefault();
        e.stopPropagation();
        var tabsContentContainer = $(this).closest(".content").find(".slided_tabs");
        var tabsContentWrapper = tabsContentContainer.find(".slided_tabs_wrapper");
        var tabContentClass = "slided_tab";
        var tabMenuItemClass = "link_tab";
        animatedTabs({
            tabsContentContainer : tabsContentContainer,
            tabsContentWrapper : tabsContentWrapper,
            tabContentClass : tabContentClass,
            tabMenuItemClass : tabMenuItemClass,
            clickedLink : $(this) //Обязательный параметр
        });
        //console.log(animatedTabs(tabsContentContainer, tabsContentWrapper, tabContentClass, tabMenuItemClass, $(this)))
    })
	//Попап менеджер FancyBox
	//Документация: http://fancybox.net/howto
	//<a class="fancybox"><img src="image.jpg" /></a>
	//<a class="fancybox" data-fancybox-group="group"><img src="image.jpg" /></a>
	$(".fancybox").fancybox();


    $(document).on("click", ".bayan h3", function(){
        var that = $(this);
        var bayan = that.closest(".bayan");
        var slidedDiv = that.next();

         if(bayan.hasClass("active")) {
             slidedDiv.slideUp(500);
             bayan.removeClass("active");
         } else {
             slidedDiv.slideDown(500);
             bayan.addClass("active");
         }
    });


	//Кнопка "Наверх"
	//Документация:
	//http://api.jquery.com/scrolltop/
	//http://api.jquery.com/animate/

    if($("body").offset().top >= 300) {
        $("#top_arrow").show();
    } else {
        $("#top_arrow").hide();
    }
	$("#top_arrow").click(function () {
		$("body, html").animate({
			scrollTop: 0
		}, 800);
		return false;
	});

    //менюшка-раскладушка

    $(".has_child > a").on("click", function(e){
        e.preventDefault();
        var parentLi = $(this).closest("li");

        if(parentLi.hasClass("active")) {
            parentLi.find("ul").hide();
        } else {
            parentLi.find("ul").show();
        }
        parentLi.toggleClass("active");
    });

    // Show_hide calendar
    $(".hide_show_calendar").on("click", function (e) {
        e.preventDefault();

        var that = $(this);
        $(".events_calendar").toggle();
        that.toggleClass("active");

    });
    //modal-windows



    var fancyObj = $(".various").fancybox({
        maxWidth: 800,
        width: 800,
        height: "auto",
        afterShow: function (e) {
            if(isSafari) {
                setTimeout(function () {
                    $.fancybox.update();
                }, 300)
            }
        },
        afterLoad   : function (e) {
            if(isSafari) {
                $.fancybox.update();
            }

        }
        //closeClick	: false,
    });



    // search dropdown

    $(".input_container.search_dropdown input[type=text], .input_container.search_dropdown").on("focus", function (e) {
        console.log("Its focusing NOW!");

        var parent = $(this).closest(".input_container.search_dropdown");
        parent.addClass("focus");
    });

    $(".input_container.search_dropdown input[type=text], .input_container.search_dropdown").on("blur", function (e) {
        console.log("Its bluring NOW!");
        var parent = $(this).closest(".input_container.search_dropdown");

        setTimeout(function (e) {
            parent.removeClass("focus");
        }, 200)
    });

});

