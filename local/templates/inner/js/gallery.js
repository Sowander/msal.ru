$(".gallery_to_modal").fancybox({
    openEffect: 'none',
    closeEffect: 'none',
    nextEffect: 'none',
    prevEffect: 'none',
    padding: 0,
    autoSize: false,
    maxWidth: 800,
    margin: [20, 0, 20, 0],
    wrapCSS: "mgua_gallery",
    afterShow: function (e) {
        if(isSafari) {
            setTimeout(function () {
                $.fancybox.update();
            }, 300)
        }
    },
    afterLoad   : function (e) {
        if(isSafari) {
            $.fancybox.update();
        }

    }
});