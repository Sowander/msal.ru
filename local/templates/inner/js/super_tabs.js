// Функция для добавления хэша в адресную строку
//function setLocationBrowserString(curLoc){
//    try {
//        history.pushState(null, null, curLoc);
//history.pushState(null, null, "?page=asdasda");
//        return;
//    } catch(e) {}
//    location.hash +=  curLoc;
//}


// Проверка строки на хэш для таба.
function checkHashTabs() {
    var nowHash = location.search.split("hash=").splice(1).join("").split("&")[0];

    if (nowHash) {
        nowHash = "#" + nowHash;
    } else {
        nowHash = undefined;
    }

    var searchNeededLink = $(".inner_page .tabs_menu .links a[data-hash=" + nowHash +"]");

    if(nowHash != undefined) {
        showNeededTab(nowHash, searchNeededLink);
    } else {
        showNeededTab();
    }
}

function showNeededTab(tabId, linkOfActiveTab) {
    tabId = (tabId) ? tabId : ("#" + $(".inner_page_content")[0].id); // Tab id

    var searchNeededLink = $(".inner_page .tabs_menu .links a[data-hash=" + tabId +"]"); // Link of this tab

    linkOfActiveTab = (linkOfActiveTab) ? linkOfActiveTab : searchNeededLink; // From arg or remember the Link

    // Start changing tabs

    var tabObject = $(tabId);

    $(".inner_page_content").fadeOut(500);
    setTimeout(function () {
        tabObject.fadeIn(500);
        tabObject.css({"display":"inline-block"});
    },510);

    // End changing tabs

    menuActiveSwitcher(linkOfActiveTab);
}

function menuActiveSwitcher(linkObj) {
    linkObj.closest(".links").find("li").removeClass("active");
    linkObj.closest("li").addClass("active");

    var hash = linkObj.data("hash");
    var splitHash = hash.split("#");

    history.pushState(null, null, "?hash=" + splitHash[1]); // Podstanovka v stroke get zaprosa
    return false;
}

$(document).on("click", ".inner_page .tabs_menu .links a", function (e) {
    e.preventDefault();
    var that = $(this);
    var dataHash = that.data("hash");
    showNeededTab(dataHash, that);
})

checkHashTabs();
