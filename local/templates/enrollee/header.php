<?php header('X-UA-Compatible: IE=edge'); ?>
<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ru" class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html lang="ru" class="lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html lang="ru" class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="ru">
<!--<![endif]-->
	<head>
		<?$APPLICATION->ShowHead();?>
		<title><?$APPLICATION->ShowTitle();?></title>
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />
        <!-- Skitter Styles -->
        <link href='//fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>


        <!--Bootstrap -->
        <!-- Latest compiled and minified CSS -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous">
        <!-- Font Awesome -->
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" type="text/css">

        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/fonts.css");?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/main.css");?>

        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/uploadfile.css");?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/bootstrap-datepicker.css");?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/multi-step-form.css");?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/uploadfile.css");?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/kladr/css/jquery.kladr.min.css");?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/kladr/css/form.css");?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/style.css");?>

	</head>
    <body>
		<div id="panel">
			<?$APPLICATION->ShowPanel();?>
		</div>
        <header>
            <section class="top_head">
                <ul class="wrapper_container">
                    <li class="logo">
                        <a href="<?if(isset($arSiteLanguageParams) && strlen($arSiteLanguageParams["UF_LINK"])>0):?><?=$arSiteLanguageParams["UF_LINK"]?><?else:?>/<?endif;?>">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/logo.png" alt="МГЮА"/>
                        </a>
                        <i class="corner"></i>
                    </li>
                    <li class="logo_text">
                        <p>
                            <!-- University name -->
                            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                                    "AREA_FILE_SHOW" => "file",
                                    "PATH" => "/includes/".LANGUAGE_ID."/university_name.php",
                                    "EDIT_TEMPLATE" => ""
                                ),
                                false
                            );?>
                        </p>
                        <span class="little_text">Non scholae sed vitae discimus</span>
                    </li>
					
                </ul>
            </section>
        </header>

        <section class="inner_page white">
            <?/*
            <div class="wrapper_container clearfix">
                */?>
                <div id="wrapper">
                    <div class="container body-content">
