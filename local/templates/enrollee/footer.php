<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
IncludeTemplateLangFile(__FILE__);
?>

                    </div>
                </div>
<?/*
            </div>
*/?>
        </section>

        <footer>
            <section class="bottom">
                <div class="wrapper_container">
                    <!-- Copyright -->
                    <div class="copy left">
                        <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => "/includes/".LANGUAGE_ID."/copyright.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            false
                        );?>
                    </div>

                </div>
            </section>
        </footer>
        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.min.js");?>
        <!-- KLADR -->
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/kladr/js/jquery.kladr.min.js");?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/kladr/js/form.js");?>
        <?/*<script src="kladr/js/one_string.js" type="text/javascript"></script>*/?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/knockout-min.js");?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/bootstrap.min.js");?>
        <!-- multi-step-form validate -->
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.validate.js");?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/multi-step-form.js");?>
        <!-- upload file -->
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.uploadfile.min.js");?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/bootstrap-datepicker.min.js");?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/bootstrap-datepicker.ru.min.js");?>
        <!-- fontawesome -->
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/font-awesome.js");?>

        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/jquery.maskedinput.min.js");?>
        <?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/script.js");?>
        <?/*
        <div class="form-loader">
            <div class="form-loader__inner">Пожалуйста, подождите, идёт загрузка данных...</div>
        </div>
        */?>

    <style>
        .errortext {color:red;}
        .ok {color: green; margin: 0 0 15px;}
    </style>

	
	<!-- Yandex.Metrika counter -->
<script type="text/javascript" >
    (function (d, w, c) {
        (w[c] = w[c] || []).push(function() {
            try {
                w.yaCounter50644546 = new Ya.Metrika2({
                    id:50644546,
                    clickmap:true,
                    trackLinks:true,
                    accurateTrackBounce:true
                });
            } catch(e) { }
        });

        var n = d.getElementsByTagName("script")[0],
            s = d.createElement("script"),
            f = function () { n.parentNode.insertBefore(s, n); };
        s.type = "text/javascript";
        s.async = true;
        s.src = "//mc.yandex.ru/metrika/tag.js";

        if (w.opera == "[object Opera]") {
            d.addEventListener("DOMContentLoaded", f, false);
        } else { f(); }
    })(document, window, "yandex_metrika_callbacks2");
</script>
<noscript><div><img src="//mc.yandex.ru/watch/50644546" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
<!-- /Yandex.Metrika counter -->

    </body>
</html>