$(document).ready( function(){

$("#formreg").validate({
	submitHandler: function() { 
		//$(".continue-block").show();
		return true;
	}
});

// --phone mask

$("#ContactPhone").mask("+7(999) 999-99-99");
$("#HomePhone").mask("+7(999) 999-99-99");
$("#WorkPhone").mask("+7(999) 999-99-99");

// //phone mask


$("#formmsf").submit(function(){

 	var form = $("#formmsf");
    var formdata = false;
    if (window.FormData){
        formdata = new FormData(form[0]);
    }

    var formAction = form.attr('action');
    $.ajax({
        url         : 'obrabotcik.php',
        data        : formdata ? formdata : form.serialize(),
        cache       : false,
        contentType : false,
        processData : false,
        type        : 'POST',
        success     : function(data, textStatus, jqXHR){
            console.log(data);
        }
    });
	return false;
});


// --------------- add-other-documents -------------------

$("#addotherdocs").click(function(event){
    addotherdocs();
    FileUploadInit();
    return false;
});

var index_m = 0;
function addotherdocs() {
    index_m = index_m + 1;
    index_n = index_m + 4;
    var div = $('<div/>', {
        'class' : 'OtherDocumentBlock'
    }).appendTo($('#other-documents'));
    var input = $('<input/>', {
        value : 'удалить',
        type : 'button',
        'class' : 'DeleteOtherDoc'
    }).appendTo(div);
    var label = $('<select onchange="GetSelndexOther(this);" name="otherdocuments-sel'+index_m+'" class="form-control other-documents" id="otherdocuments'+index_m+'"/>').appendTo(div);
    var select = $('.other-documents-types select>').clone().appendTo("#otherdocuments"+index_m+"");

    var dseriya = $('<div/>', {
        'class' : 'form-group col-md-4 series-otherdoc otherdocuments'+index_m+''
    }).html("<input class='form-control otherdoc-seriya-field' name='otherdoc-seriya"+index_m+"' id='otherdoc-seriya"+index_m+"' placeholder='Серия документа' data-msg='Введите серию' required/>").appendTo($(div));

    var dnumber = $('<div/>', {
        'class' : 'form-group col-md-4 number-otherdoc otherdocuments'+index_m+''
    }).html("<input class='form-control otherdoc-number-field' name='otherdoc-number"+index_m+"' id='otherdoc-number"+index_m+"' placeholder='Номер документа' data-msg='Введите номер' required/>").appendTo($(div));
    
    var ddate = $('<div/>', {
        'class' : 'form-group col-md-4 date-otherdoc otherdocuments'+index_m+''
    }).html("<input class='form-control otherdoc-date-field' name='otherdoc-date"+index_m+"' id='otherdoc-date"+index_m+"' placeholder='Дата' data-msg='Введите дату' required/>").appendTo($(div));
	
    var dfile = $('<div/>', {
        'class' : 'form-group col-md-12 input-file-container otherdocuments'+index_m+''
    }).html("<input class='input-file-"+index_n+"' id='my-file-"+index_n+"' name='other-document-"+index_n+
    "' type='file' accept='.pdf,.rtf,.xls,.xlsx,.doc,.docx'/><label for='my-file-"+index_n+"' class='input-file-trigger-3"+index_n+
    "'><i class='fas fa-upload'></i><span>загрузить файл...</span></label><p class='file-return-"+index_n+" file-result'></p>").appendTo($(div));

	// $('input#otherdoc-date'+index_m+'').datepicker({
	// 	language: "ru",
	// 	startDate: '01-01-1950',
	// 	endDate: '31-12-2003'
	// });

	var fullYear =  new Date().getFullYear();
	var fullMonth =  new Date().getMonth();
	var fullMonth = fullMonth+1;
	var fullDay =  new Date().getDate();

	$('input#otherdoc-date'+index_m+'').datepicker({
		language: "ru",
		dateFormat:"yy/mm/dd",
		endDate: ''+fullDay+'-'+fullMonth+'-'+fullYear+''
	});

    input.click(function() {
        $(this).parent().remove();
    });


}

//Для удаления первого поля
$('.DeleteOtherDoc').click(function(event) {
    $(this).parent().remove();
    return false;
});

// --------------- addfamilymember -------------------

$("#addfamilymember").click(function(event){
    addDynamicExtraField();
    return false;
});

var index_i = 0;
function addDynamicExtraField() {
    index_i = index_i + 1;
    var div = $('<div/>', {
        'class' : 'DynamicExtraField'
    }).appendTo($('#AddFamilyMemberContainer'));
    var input = $('<input/>', {
        value : 'удалить',
        type : 'button',
        'class' : 'DeleteDynamicExtraField'
    }).appendTo(div);
    // var label = $('<select class="form-control maritalstatus" name="maritalstatus-sel'+index_i+'" id="maritalstatus'+index_i+'"/>').appendTo(div);
    // var select = $('.maritalstatus-buffer select>').clone().appendTo("#maritalstatus"+index_i+"");

    var degree = $('<div/>', {
        'class' : 'form-group'
    }).html("<input class='form-control family-degree-field' name='family-degree"+index_i+"' id='family-degree"+index_i+"' placeholder='Степень родства' data-msg='Введите степень родства'/>").appendTo($(div));

    var fio = $('<div/>', {
        'class' : 'form-group'
    }).html("<input onkeyup='rusinput(\"ru\", this );' class='form-control family-fio-field' name='family-fio"+index_i+"' id='family-FIO"+index_i+"' placeholder='ФИО' data-msg='Введите ФИО'/>").appendTo($(div));

    var bdate = $('<div/>', {
        'class' : 'form-group'
    }).html("<input class='form-control family-bdate-field' name='family-bdate"+index_i+"' id='family-bdate"+index_i+"' placeholder='Дата рождения'/>").appendTo($(div));
    
    var phone = $('<div/>', {
        'class' : 'form-group'
    }).html("<input class='form-control family-phone-field' name='family-phone"+index_i+"' id='family-phone"+index_i+"' placeholder='Телефон' data-msg='Введите телефон'/>").appendTo($(div));
    
    var faddress = $('<div/>', {
        'class' : 'form-group'
    }).html("<input class='form-control family-address-field' name='family-member-address"+index_i+"' id='family-member-address"+index_i+"' placeholder='Адрес' data-msg='Введите адрес'/>").appendTo($(div));


	$('input#family-bdate'+index_i+'').datepicker({
		language: "ru",
		startDate: '01-01-1950',
		endDate: '31-12-2003'
	});

	$("#family-phone"+index_i+"").mask("+7(999) 999-99-99");

    input.click(function() {
        $(this).parent().remove();
    });
}

//Для удаления первого поля
$('.DeleteDynamicExtraField').click(function(event) {
    $(this).parent().remove();
    return false;
});

// ---------------// addlanguage ---------------------

$("#addlanguage").click(function(event){
    addlanguage();
    return false;
});

var index_k = 0;
function addlanguage() {
    index_k = index_k + 1;
    var div = $('<div/>', {
        'class' : 'AdditionalLang'
    }).appendTo($('#AddLangContainer'));
    var input = $('<input/>', {
        value : 'удалить',
        type : 'button',
        'class' : 'deleteLang'
    }).appendTo(div);
    var label = $('<select class="form-control add-lang" name="addlang'+index_k+'" id="lang'+index_k+'"/>').appendTo(div);
    var select = $('.lang-buffer select>').clone().appendTo("#lang"+index_k+"");
    var checkmain = $("<input class='form-check-input' type='radio' value='addlang"+index_k+"' id='addlang"+index_k+"' name='mainlang' ><label class='form-check-label lang-check' for='addlang1'>основной</label>").appendTo(div);
    var check = $("<input class='form-check-input' type='checkbox' value='addcheck"+index_k+"' id='addcheck"+index_k+"' name='checklang"+index_k+"' ><label class='form-check-label lang-check' for='addcheck'>Аттестован</label>").appendTo(div);

// <input type="checkbox" class="form-check-input" id="firstname_ch2" name="user_firstname_empty">отсутствует               

    input.click(function() {
        $(this).parent().remove();
    });
}

//Для удаления первого поля
$('.deleteLang').click(function(event) {
    $(this).parent().remove();
    return false;
});

// ----------------- addlanguage ---------------------

// -------------// addfamilymember -------------------


//------------------------------------ upload file --------------------------------------------

	$("#fileuploader-1").uploadFile({
		url:"file",
		fileName:"myfile",
		multiple:false,
		maxFileCount:1,
		dragDropStr: "<span>или перетяни файл сюда</span>",
		uploadStr:"загрузить"
	});

	$("#fileuploader-2").uploadFile({
		url:"file",
		fileName:"myfile",
		multiple:false,
		maxFileCount:1,
		dragDropStr: "<span>или перетяни файл сюда</span>",
		uploadStr:"загрузить"
	});

	$("#fileuploader-3").uploadFile({
		url:"file",
		fileName:"myfile",
		multiple:false,
		maxFileCount:1,
		dragDropStr: "<span>или перетяни файл сюда</span>",
		uploadStr:"загрузить"
	});

	$("#fileuploader-4").uploadFile({
		url:"file",
		fileName:"myfile",
		multiple:false,
		maxFileCount:1,
		dragDropStr: "<span>или перетяни файл сюда</span>",
		uploadStr:"загрузить"
	});

	$("#fileuploader-5").uploadFile({
		url:"file",
		fileName:"myfile",
		multiple:false,
		maxFileCount:1,
		dragDropStr: "<span>или перетяни файл сюда</span>",
		uploadStr:"загрузить"
	});

//----------------------------------// upload file --------------------------------------------

// ------------------------------------- csv files ----------------------------------------------

	$.ajax({
	  url: 'documents/otherdocuments.csv',
	  dataType: 'text',	}).done(otherdocumentstypes);

	function otherdocumentstypes(data) {
	  var allRows = data.split(/\r?\n|\r/);
	  var select = '<select name="otherdocumentstypes" class="form-control">';
	  for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
	    var rowCells = allRows[singleRow].split(';');
	    if (singleRow !== 0 && allRows[singleRow] !='') {
	         select += '<option attr-othercode="'+rowCells[1]+'" attr-seriya="'+rowCells[2]+'" attr-number="'+rowCells[3]+'" attr-date="'+rowCells[4]+'">';
	         select += rowCells[0];
	         select += '</option>';
	    }
	  } 
	  select += '</select>';
	  $('.form-group.other-documents-types').append(select);
	}

	$.ajax({
	  url: 'documents/education.csv',
	  dataType: 'text',	}).done(education);

	function education(data) {
	  var allRows = data.split(/\r?\n|\r/);
	  var select = '<select name="education" class="inddocuments form-control">';
	  for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
	    var rowCells = allRows[singleRow].split(';');
	    if (singleRow !== 0 && allRows[singleRow] !='') {
	         select += '<option attr-educode="'+rowCells[1]+'"  attr-seriya="'+rowCells[2]+'" attr-number="'+rowCells[3]+'">';
	         select += rowCells[0];
	         select += '</option>';
	    }
	  } 
	  select += '</select>';
	  $('.form-group#education-doc').append(select);
	}

	$.ajax({
	  url: 'documents/documents.csv',
	  dataType: 'text',	}).done(inddocuments,inddocuments2,inddocuments3,inddocuments4);

	function inddocuments(data) {
	  var allRows = data.split(/\r?\n|\r/);
	  var select = '<select name="inddocuments_other" class="inddocuments form-control" onchange="GetSelndex(this);">';
	  for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
	    var rowCells = allRows[singleRow].split(';');
	    // if (singleRow !== 0 && allRows[singleRow] !='') {
	         select += '<option attr-code="'+rowCells[1]+'"  attr-seriya="'+rowCells[2]+'" attr-number="'+rowCells[3]+'">';
	         select += rowCells[0];
	         select += '</option>';
	    // }
	  } 
	  select += '</select>';
	  $('.form-group.inddocuments-step-1').append(select);
	}

	function inddocuments4(data) {
	  var allRows = data.split(/\r?\n|\r/);
	  var select = '<select disabled name="inddocuments4" class="inddocuments form-control" onchange="GetSelndex4(this);">';
	  for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
	    var rowCells = allRows[singleRow].split(';');
	    // if (singleRow !== 0 && allRows[singleRow] !='') {
	         select += '<option attr-code="'+rowCells[1]+'"  attr-seriya="'+rowCells[2]+'" attr-number="'+rowCells[3]+'">';
	         select += rowCells[0];
	         select += '</option>';
	    // }
	  } 
	  select += '</select>';
	  $('.form-group#document-3-type').append(select);
	}

	function inddocuments2(data) {
	  var allRows = data.split(/\r?\n|\r/);
	  var select = '<select name="inddocuments2" class="inddocuments form-control" onchange="GetSelndex2(this);">';
	  for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
	    var rowCells = allRows[singleRow].split(';');
	    // if (singleRow !== 0 && allRows[singleRow] !='') {
	         select += '<option attr-code="'+rowCells[1]+'"  attr-seriya="'+rowCells[2]+'" attr-number="'+rowCells[3]+'">';
	         select += rowCells[0];
	         select += '</option>';
	    // }
	  } 
	  select += '</select>';
	  $('.form-group.inddocuments-step-2').append(select);
	}

	function inddocuments3(data) {
	  var allRows = data.split(/\r?\n|\r/);
	  var select = '<select name="inddocuments3" class="inddocuments form-control" onchange="GetSelndex3(this);">';
	  for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
	    var rowCells = allRows[singleRow].split(';');
	    // if (singleRow !== 0 && allRows[singleRow] !='') {
	         select += '<option attr-code="'+rowCells[1]+'"  attr-seriya="'+rowCells[2]+'" attr-number="'+rowCells[3]+'">';
	         select += rowCells[0];
	         select += '</option>';
	    // }
	  } 
	  select += '</select>';
	  $('.form-group.documenttype2').append(select);
	}

	$.ajax({
	  url: 'documents/countries.csv',
	  dataType: 'text',	}).done(countries);

	function countries(data) {
	  var allRows = data.split(/\r?\n|\r/);
	  var select = '<select name="countries-sel" class="form-control" onchange="">';
	  for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
	    var rowCells = allRows[singleRow].split(';');
	         select += '<option attr-countriescode="'+rowCells[1]+'">';
	         select += rowCells[0];
	         select += '</option>';
	  } 
	  select += '</select>';
	  
	  $('.form-group.сitizenship').append(select);
	}

	$.ajax({
	  url: 'documents/regions.csv',
	  dataType: 'text',	}).done(regions);

	function regions(data) {
	  var allRows = data.split(/\r?\n|\r/);
	  var select = '<select name="regions-sel" id="regionssel" class="form-control" onchange="CityType(this);">';
	  for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
	    var rowCells = allRows[singleRow].split(';');
	         select += '<option attr-regionscode="'+rowCells[1]+'">';
	         select += rowCells[0];
	         select += '</option>';
	  } 
	  select += '</select>';

	  $('.form-group.regions').append(select);
	  $('.maritalstatus').append(select);
	}

	$.ajax({
	  url: 'documents/regions.csv',
	  dataType: 'text',	}).done(maritalstatus);

	function maritalstatus(data) {
	  var allRows = data.split(/\r?\n|\r/);
	  var select = '<select name="maritalstatus-sel" class="form-control" onchange="">';
	  for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
	    var rowCells = allRows[singleRow].split(';');
	         select += '<option attr-maritalcode="'+rowCells[1]+'">';
	         select += rowCells[0];
	         select += '</option>';
	  } 
	  select += '</select>';
	  $('.maritalstatus-buffer').append(select);
	}

	$.ajax({
	  url: 'documents/settlement.csv',
	  dataType: 'text',	}).done(settlement);

	function settlement(data) {
	  var allRows = data.split(/\r?\n|\r/);
	  var select = '<select name="settlement-sel" id="settlementsel" class="form-control" onchange="">';
	  for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
	    var rowCells = allRows[singleRow].split(';');
	         select += '<option attr-settlementcode="'+rowCells[1]+'" value="citytype'+rowCells[1]+'">';
	         select += rowCells[0];
	         select += '</option>';
	  } 
	  select += '</select>';

	  $('.form-group.settlement').append(select);
	}

	$.ajax({
	  url: 'documents/languages.csv',
	  dataType: 'text',	}).done(languages);

	function languages(data) {
	  var allRows = data.split(/\r?\n|\r/);
	  var select = '<select name="languages-sel" class="form-control" onchange="">';
	  for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
	    var rowCells = allRows[singleRow].split(';');
	    if (singleRow !== 0 && allRows[singleRow] !='') {
	         select += '<option attr-languagescode="'+rowCells[0]+'">';
	         select += rowCells[1];
	         select += '</option>';
	    }
	  } 
	  select += '</select>';

	  $('.lang-buffer').append(select);
	}

// -----------------------------------// csv files ----------------------------------------------	

	$(".form-check-input").change(function(){
		if($(this).is(':checked')){
			$(".form-group."+$(this).attr("id")+" input").val("");
		}
    	$(".form-group."+$(this).attr("id")+" input[type='text']").toggle();

    	$(".form-group."+$(this).attr("id")+" input[type='text']").toggle();    	
    	$(".form-group."+$(this).attr("id")+" input[type='text']").toggle();
	});



	$("#sameresidenceaddress_ch").change(function(){
		$(".sameresidenceaddress").toggle();
	});

	$("#documenttype2").change(function(){

    	$(".documenttype2-block").toggle();
    	// $(".number-documenttype2").toggle();

		if ($('#documenttype2').is(':checked')){
	    	var SeriesFlagDoc2 = $(".documenttype2 .inddocuments :selected").attr("attr-seriya");    
		    if(SeriesFlagDoc2 == "Да"){
		    	$(".series-documenttype2.series").show();
		    	$(".series-documenttype2.series input").attr('required', true);
		    }   
		   	else{ 
		   		$(".series-documenttype2.series").hide();
		    	$(".series-documenttype2.series input").attr('required', false);
		    	$(".series-documenttype2.series input").val('');
		    }
		    
		}else{
			$(".series-documenttype2").hide();
		}

	});

	$('#birthdate').datepicker({
		language: "ru",
		startDate: '01-01-1950',
		endDate: '31-12-2003'
	});

	var fullYear =  new Date().getFullYear();
	var fullMonth =  new Date().getMonth();
	var fullMonth = fullMonth+1;
	var fullDay =  new Date().getDate();

	$('#DocDate').datepicker({
		// language: "ru",
		// startDate: '01-01-1950',
		// endDate: '31-12-2003'
		language: "ru",
		dateFormat:"yy/mm/dd",
		endDate: ''+fullDay+'-'+fullMonth+'-'+fullYear+''
	});

	$('input#birthdate2').datepicker({
		language: "ru",
		startDate: '01-01-1950',
		endDate: '31-12-2003'
	});

	$('input#BirthDate').datepicker({
		language: "ru",
		startDate: '01-01-1950',
		endDate: '31-12-2003'
	});

	$('input#date-education-doc').datepicker({
		language: "ru",
		dateFormat:"yy/mm/dd",
		endDate: ''+fullDay+'-'+fullMonth+'-'+fullYear+''
	});

	$('input#otherdoc-date1').datepicker({
		language: "ru",
		startDate: '01-01-1950',
		endDate: '31-12-2003'
	});



// ------------------------------------- multiStepForm ----------------------------------------------
    

    function ViewModel() {
        var self = this;
        self.Name = ko.observable('');
        self.Email = ko.observable('');
        self.Details = ko.observable('');

        self.AdditionalDetails = ko.observable('');
        self.availableTypes = ko.observableArray(['New', 'Open', 'Closed']);
        self.chosenType = ko.observable('');

        self.availableCountries = ko.observableArray(['France', 'Germany', 'Spain', 'United States', 'Mexico']),
        self.chosenCountries = ko.observableArray([]) 

    }

    var viewModel = new ViewModel();

    ko.applyBindings(viewModel);

    $(document).on("msf:viewChanged", function(event, data){
        var progress = Math.round((data.completedSteps / data.totalSteps)*100);

        $(".progress-bar").css("width", progress + "%").attr('aria-valuenow', progress);   ;
    });

    $(".msf:first").multiStepForm({
        activeIndex: 0,
        hideBackButton : false,
        allowUnvalidatedStep : false,
        allowClickNavigation : false,
        validate: {}
    });

    
    
    $("#formmsf").validate({
    	rules: {
			email: {required: true,email: true},
			email2: {required: true,email: true},
			passportseries: {required: true},
			passportseries2: {required: true},
			passportseries3: {required: true},
			passportnumber: {required: true},			
			passportnumber2: {required: true},
			passportnumber3: {required: true},
			lastname:{required: true},
			lastname2:{required: true},
			firstname:{required: true},
			firstname2:{required: true},
			patronymic:{required: true},
			patronymic2:{required: true},
			birthdate:{required: true},
			registrationaddress:{required: true},
			contactphone:{required: true},
			sameresidenceaddress:{required: true},
			fio:{required: true},
			socialscience:{required: true},
			russian:{required: true},
			history:{required: true}
		},		
		messages: {
			email: "Введите корректный email",
			email2: "Введите корректный email",
			passportseries: "Введите серию документа",
			passportseries2: "Введите серию документа",
			passportseries3: "Введите серию документа",
			passportnumber: "Введите номер документа",
			passportnumber2: "Введите номер документа",
			passportnumber3: "Введите номер документа",
			lastname: "Введите фамилию",
			lastname2: "Введите фамилию",
			firstname: "Введите имя",
			firstname2: "Введите имя",
			patronymic: "Введите отчество",
			patronymic2: "Введите отчество",
			birthdate: "Введите дату рождения (возраст не менее 16 лет)",
			// birthdate2: "Введите дату рождения (возраст не менее 16 лет)",
			// birthplace: "Введите дату рождения (возраст не менее 16 лет)",
			registrationaddress: "Укажите адрес регистрации",
			contactphone: "Введите телефон",
			sameresidenceaddress: "Укажите адрес проживания",
			fio: "Введите ФИО" 
		}
    });


	$.validator.addClassRules("family-fio-field", {
		  	required: true
		}
	);

	$.validator.addClassRules("family-phone-field", {
		  	required: true
		}
	);

	// $.validator.addClassRules("family-bdate-field", {
	// 	  	required: true
	// 	}
	// );

	$('.kladr-block input').keyup(function(){
		var this_element = $(this);
		setTimeout(function() {
		// console.log('aaa');

		if(this_element.hasClass('error')){
			this_element.siblings('.important-label').show();			
		}
		if(!this_element.hasClass('kladr-error') && this_element.val()!=''){
		// 	console.log('222');
		// 	$(this).siblings('label').show();
		// 	data_msg = $(this).attr('data-msg');
		// 	$(this).siblings('label').text(data_msg);
		// 	// console.log($(this));
		// }
		// else{
			// console.log('333');
			this_element.siblings('.important-label').hide();
			if(  this_element.attr('name') =='zip'  ){
				$('.kladr-block input').each(function(){
					if($(this).val() !=''){
						$(this).siblings('.important-label').hide();
						$(this).removeClass('kladr-error');
						$(this).removeClass('error');
					}
				});
			}
		}
		if(this_element.hasClass('kladr-error')){
			// console.log('555555555');
		}
		// if($(this).val()==''){
		// 	console.log('eeeeeeeeeeeeeeeee');

		// }
		}, 300);
	});

	$(".msf-nav-button").click(function(){
		$('.kladr-block input').each(function(){
			if($(this).val() ==''){
				$(this).siblings('.important-label').show();
			}
		});
	});

// -----------------------------------// multiStepForm ----------------------------------------------

});

//--------------------------------------- show Series -----------------------------------------------

function GetSelndex(sel){

    var SeriesFlagStep1 = $(".inddocuments-step-1 .inddocuments :selected").attr("attr-seriya");    
    if(SeriesFlagStep1 == "Да"){
    	$(".series-step-1.series").show();
    	$(".series-step-1.series input").attr('required', true);
    	
    	$(".series.series-document-type").show();
    	$(".series.series-document-type input").attr('required', true);

    	$(".series.series series-step-2").show();
    	$(".series.series series-step-2 input").attr('required', true);
    }   
   	else{ 
   		$(".series-step-1.series").hide();
    	$(".series-step-1.series input").attr('required', false);
    	$(".series-step-1.series input").val('');

   		$(".series-document-type.series").hide();
    	$(".series-document-type.series input").attr('required', false);
    	$(".series-document-type.series input").val('');

   		$(".series-step-2.series").hide();
    	$(".series-step-2.series input").attr('required', false);
    	$(".series-step-2.series input").val('');
    }

    var SeriesFlagIndex = $(".inddocuments :selected").index()+1;

    $(".inddocuments-step-2 .inddocuments option:nth-child("+SeriesFlagIndex+")").attr("selected", "selected");
    $(".person-document-block .inddocuments option:nth-child("+SeriesFlagIndex+")").attr("selected", "selected");

}

function GetSelndex2(sel2){
	var SeriesFlagStep2 = $(".inddocuments-step-2 .inddocuments :selected").attr("attr-seriya");    
    if(SeriesFlagStep2 == "Да"){
    	$(".series-step-2.series").show();
    	$(".series-step-2.series input").attr('required', true);
    }   
   	else{ 
   		$(".series-step-2.series").hide();
    	$(".series-step-2.series input").attr('required', false);
    	$(".series-step-2.series input").val('');
    }
}

function GetSelndex3(sel3){
    var SeriesFlagDoc2 = $(".documenttype2 .inddocuments :selected").attr("attr-seriya");    
    if(SeriesFlagDoc2 == "Да"){
    	$(".series-documenttype2.series").show();
    	$(".series-documenttype2.series input").attr('required', true);
    }   
   	else{ 
   		$(".series-documenttype2.series").hide();
    	$(".series-documenttype2.series input").attr('required', false);
    	$(".series-documenttype2.series input").val('');
    }
}

function GetSelndex4(sel4){
    var SeriesFlagDoc4 = $("#document-3-type .inddocuments :selected").attr("attr-seriya");    
    if(SeriesFlagDoc4 == "Да"){
    	$(".series-document-type.series").show();
    	$(".series-document-type.series input").attr('required', true);
    }   
   	else{ 
   		$(".series-document-type.series").hide();
    	$(".series-document-type.series input").attr('required', false);
    	$(".series-document-type.series input").val('');
    }
}

function GetSelndexOther(sel5){

	var ChangeSelctId = $(sel5).attr("id");

    var DocFlagSeries = $(".OtherDocumentBlock #"+ChangeSelctId+" :selected").attr("attr-seriya");  
    var NumberSeries = $(".OtherDocumentBlock #"+ChangeSelctId+" :selected").attr("attr-number");  
    var DateSeries = $(".OtherDocumentBlock #"+ChangeSelctId+" :selected").attr("attr-date");  

	// console.log(ChangeSelctId);
    
    if(DocFlagSeries == "Да"){
    	$(".series-otherdoc."+ChangeSelctId+"").show();
    	$(".series-otherdoc."+ChangeSelctId+" input").attr('required', true);
    }   
   	else{ 
   		$(".series-otherdoc."+ChangeSelctId+"").hide();
    	$(".series-otherdoc."+ChangeSelctId+" input").attr('required', false);
    	$(".series-otherdoc."+ChangeSelctId+" input").val('');
    } 
    
    if(NumberSeries == "Да"){
    	$(".number-otherdoc."+ChangeSelctId+"").show();
    	$(".number-otherdoc."+ChangeSelctId+" input").attr('required', true);
    }   
   	else{ 
   		$(".number-otherdoc."+ChangeSelctId+"").hide();
    	$(".number-otherdoc."+ChangeSelctId+" input").attr('required', false);
    	$(".number-otherdoc."+ChangeSelctId+" input").val('');
    } 

    if(DateSeries == "Да"){
    	$(".date-otherdoc."+ChangeSelctId+"").show();
    	$(".date-otherdoc."+ChangeSelctId+" input").attr('required', true);
    }   
   	else{ 
   		$(".date-otherdoc."+ChangeSelctId+"").hide();
    	$(".date-otherdoc."+ChangeSelctId+" input").attr('required', false);
    	$(".date-otherdoc."+ChangeSelctId+" input").val('');
    }
}

function CityType(city){
	var SelectCity = $("#regionssel :selected").attr("attr-regionscode");
	if(SelectCity == 78 || SelectCity == 77 || SelectCity == 92){
		$("#settlementsel").val("citytype00-000001").change();
	}
}

//-------------------------------------// show Series -----------------------------------------------

$("#PassportSeries").keyup(function(){$("#PassportSeries2").val($("#PassportSeries").val());});
$("#PassportNumber").keyup(function(){$("#PassportNumber2").val($("#PassportNumber").val());});

$("#PassportSeries").keyup(function(){$("#DocSeries").val($("#PassportSeries").val());});
$("#PassportNumber").keyup(function(){$("#DocNumber").val($("#PassportNumber").val());});

$("#DocSeries").keyup(function(){$("#PassportSeries").val($("#DocSeries").val());});
$("#DocNumber").keyup(function(){$("#PassportNumber").val($("#DocNumber").val());});


$("#patronymic").keyup(function(){$("#patronymic2").val($("#patronymic").val());});
$("#firstname").keyup(function(){$("#firstname2").val($("#firstname").val());});
$("#lastname").keyup(function(){$("#lastname2").val($("#lastname").val());});
$("#birthdate").change(function(){$("#birthdate2").val($("#birthdate").val());});
$("#birthdate2").change(function(){$("#birthdate").val($("#birthdate2").val());});

// ---------------- addstatement ---------------------

$("#addstatement").click(function(){
	$("#addstatement_form").show("fast");
});

// --------------// addstatement ---------------------

$(".msf-nav-button").click(function(){
	if($(".gender .custom-control-input").hasClass("valid")){
		$(".gender span.error").hide();
	}	
});

$("#birthdate,#birthdate2").change(function(){

	var userAge = getAge($(this).val());
	$(".form-date .hidden-date").val(userAge);

	if (userAge >= 16){
		$(".form-date .hide-message").hide();
		$(".message-parent").show();
	}
	if (userAge < 16){
		$("#birthdate").val("");
		$("#birthdate2").val("");
		$(".form-date .hide-message").show();
	}
	if (userAge > 18){
		$(".message-parent").hide();
	}
});

// дата рождения в формате '22.05.1990'

function getAge(dateString) {
  var day = parseInt(dateString.substring(0,2));
  var month = parseInt(dateString.substring(3,5));
  var year = parseInt(dateString.substring(6,10));

  var today = new Date();
  var birthDate = new Date(year, month - 1, day); // 'month - 1' т.к. нумерация месяцев начинается с 0 
  var age = today.getFullYear() - birthDate.getFullYear();
  var m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) { 
      age--;
  }
  return age;
}

function FileUploadInit(){
	
	document.querySelector("html").classList.add('js');	

	var fileInput = new Array();      
	var button = new Array();      
	var the_return = new Array();      

	var CountBlock = $("#upload-documents .input-file-container").length;
	for(var st = 0; st < CountBlock; st++){
		fileInput[st]  = document.querySelector( ".input-file-"+st+"" ),  

	    button[st] = document.querySelector( ".input-file-trigger-"+st+"" ),
	    the_return[st] = document.querySelector(".file-return-"+st+"");

		
		$(".input-file-"+st+"").change(function(){
			var size = this.files[0].size; 
			if(3000000<size){
				$(this).siblings(".file-result").text("Файл должен быть менее 3 Мбайт");
			}else{
				var this_val = $(this).val().substr(12);
				var this_id = $(this).attr("id");
				if(this_val){			
					$(this).siblings(".file-result").text("Выбран файл: "+this_val+"");
					$(this).siblings("label").children("span").text("Изменить");
				}else{
					$(this).siblings(".file-result").text("");
					$(this).siblings("label").children("span").text("загрузить файл...");
				}
			}
		});

	}
}

FileUploadInit();

function onlynumber(input) {
    ch = input.value.replace(/[^\d,]/g, ''); //разрешаем вводить только числа и запятую
    pos = ch.indexOf(','); // проверяем, есть ли в строке запятая
    if(pos != -1){ // если запятая есть
        if((ch.length-pos)>2){ // проверяем, сколько знаков после запятой, если больше 1го то
            ch = ch.slice(0, -1); // удаляем лишнее
        }
    }
    input.value = ch; // приписываем в инпут новое значение
};

function rusinput( k, obj ) {
	switch( k ){
	    case 'ru':
	    obj.value = obj.value.replace(/[^а-яА-ЯёЁ0-9 -]/ig,'');
	    break;
	}
}

$(".continue-block").click(function(){
	$("#formreg").hide();
	$(this).hide();
	$("#formmsf").show();
});

$(".registration-block .field input").change(function(){
	// if($(this).hasClass("kladr-error")){
	// 	console.log("1");
	// 	// console.log('111');
	// 	$(this).val("");
	// 	$(this).siblings("span").addClass("active-label");
	// }
	// }else{
	// 	$(this).siblings("span.error").removeClass("active-label");		
	// }

	// if($(".registration-block .field input").val() == ""){
	// 	$(this).addClass("kladr-error");
	// 	$(this).addClass("error");
	// }else{
	// 	$(this).removeClass("kladr-error");
	// 	$(this).removeClass("error");

	// }

	// if($(".registration-block .field input").hasClass("kladr-error")){
	// 	$(this).parent(".field").children("span.error").addClass("active-label");	
	// 	console.log('1');
	// }else{
	// 	$(".registration-block .field input.valid").parent(".field").children("span").removeClass("active-label");
	// 	console.log('2');

	// }

});


