$(document).ready( function(){
	if ($('.form-loader').length < 1) {
		$('body').append('<div class="form-loader" style="display: none;"><div class="form-loader__inner">Пожалуйста, подождите, идёт загрузка данных...</div></div>');
	}
	
	if ($('form.is-disabled').length > 0) {
		$('form.is-disabled input, form.is-disabled textarea, form.is-disabled select, form.is-disabled button').attr('disabled', true);
	}
	
	$("#formreg, #formmsf1, #formmsf2, #formmsf3, #formmsf4, #formmsf5, #formmsf6").validate({
		submitHandler: function(form) {			
			//проверка полей в 6ом шаге			
			if (form == $('#formmsf6')[0]) {
				var dataSubmit = $(form).serializeArray();
				for (var key in dataSubmit) {
					dataSubmit[dataSubmit[key]['name']] = dataSubmit[key]['value'];
				}
				
				if (!addValidateStep6(dataSubmit)) {
					return false;
				}
			}
			
			$('.form-loader').fadeIn(300);
			form.submit();
		}	
	});

	$('.is-disabled:not(form)').on('click', function(e) {
		e.preventDefault();
		return false;
	});
	
	// --phone mask
	$(".phone-mask, #ContactPhone, #HomePhone, #WorkPhone").mask("+7(999) 999-99-99");
	// //phone mask


	$("#formmsf1").submit(function(){
		var form = $("#formmsf1");
		var formdata = false;
		if (window.FormData){
			formdata = new FormData(form[0]);
		}
		
		var formAction = form.attr('action');
		$.ajax({
			url         : 'obrabotcik.php',
			data        : formdata ? formdata : form.serialize(),
			cache       : false,
			contentType : false,
			processData : false,
			type        : 'POST',
			success     : function(data, textStatus, jqXHR){}
		});
		return false;
	});
	
	// сброс фильтра
	$('body').on('click', '.js-form-reset', function(e) {
		e.preventDefault();
		
		$('.form-loader').fadeIn(300);
		var newLocation = window.location.href.replace(window.location.search, '');
		newLocation = window.location.href.replace(window.location.hash, '');
		window.location = newLocation;
		
		return false;
	});

	// set status
	if ($('#status-inp').length > 0) {
		var val = ($('#status-inp').val()) ? $('#status-inp').val() : '';
		$('#status').html(val);
	}
	
	// отправка статуса об изменении данных
	$('#changeStatusSend').click(function() {
		if ($('form#formstatus').length > 0) {
			$('form#formstatus').submit();
		}
	});

	$('body')
		.on('change', 'input[class*="input-file-"]', function() {
			var size = $(this)[0].files[0]['size'];
			
			if (size > 3000000) {
				$(this).siblings(".file-result").html("Файл должен быть менее 3&nbsp;Мбайт");
			} else {
				var this_val = $(this).val().substr(12);
				var this_id = $(this).attr("id");
				
				if (this_val) {			
					$(this).siblings(".file-result").text("Выбран файл: "+this_val+"");
					$(this).siblings("label").children("span").text("Изменить");
				} else {
					$(this).siblings(".file-result").text("");
					$(this).siblings("label").children("span").text("загрузить файл...");
				}
			}
		})
		// step 6
		.on('change', '[name="app_main"]', function() {
			var id = $(this).attr('data-id') ? parseInt($(this).attr('data-id')) : $(this).val().replace('addmain', '');
			
			if ($(this).closest('table').siblings('.js-form-error').length > 0) {
				$(this).closest('table').siblings('.js-form-error').remove();
				$(this).closest('table').find('tr:first-child th').eq($(this).closest('td').index()).removeClass('is-error');
			}
			
			if (id !== NaN && id !== undefined && id !== null) {
			
				if (!$(this).closest('tr').find('[name="app['+ id +'][take]"]').prop('checked')) {
					$(this).closest('tr').find('[name="app['+ id +'][take]"]').prop('checked', true);
				}
				
				$(this).closest('table').find('.step-alert.is-error').remove();
				$(this).closest('table').find('[name^="app"][name$="[profile]"].is-error').removeClass('is-error');
				
				var $selectProfile = $(this).closest('tr').find('[name="app['+ id +'][profile]"]:not(.is-disabled)');
				if ($selectProfile.length > 0 && 
					(($selectProfile.attr('data-change') == undefined || $selectProfile.attr('data-change') != 1) &&
					($selectProfile.find('option[selected]').length < 1)
				)) {
					$selectProfile.addClass('is-error');
					if ($selectProfile.siblings('.step-alert.is-error').length < 1) {
						$selectProfile.before('<div class="step-alert is-error">Пожалуйста, выберите программу</div>');	
					}
				}
				
			}
		})
		.on('change', '[name^="app"][name$="[take]"]', function() {
			var id = $(this).attr('data-id') ? parseInt($(this).attr('data-id')) : $(this).val().replace('addcheck', '');
			
			if (id != NaN && id != undefined && id !== null) {
			
				if ($(this).closest('tr').find('[name="app_main"][data-id="'+ id +'"]').length > 0 && $(this).closest('tr').find('[name="app_main"][data-id="'+ id +'"]').prop('checked')) {
					$(this).prop('checked', true);
				}
				
			}
		})
		.on('change', '[name^="app"][name$="[profile]"]', function() {
			if ($(this).val() != '') {			
				$(this).attr('data-change', 1);
				$(this).removeClass('is-error');
				$(this).closest('tr').find('.step-alert.is-error').remove();	
			}
		})
		// end spet 6
		// step 5
		.on('change', '#russian, #socialscience, #history', function() {
			if ($(this).val() == 0) {
				$(this).rules("add", {
					min: 0,
					max: ($(this).attr('max') !== undefined) ? parseInt($(this).attr('max')) : 0,
				});
			} else {
				$(this).rules("add", {
					min: ($(this).attr('min') !== undefined) ? parseInt($(this).attr('min')) : 0,
					max: ($(this).attr('max') !== undefined) ? parseInt($(this).attr('max')) : 0,
				});
			}
		})
		// end step 5
		.on('change', '[name^="passportseries"], [name^="passportnumber"]', function() {
			onlyNumber($(this)[0]);
		});
	
// --------------- add-other-documents -------------------

	// добавление иных документов - 4 шаг
	$("#addotherdocs").click(function(event){
		addotherdocs();
		return false;
	});

	//Для удаления первого поля
	$('.DeleteOtherDoc').click(function(event) {
		$(this).parent().remove();
		return false;
	});

// --------------- addfamilymember -------------------

	$("#addfamilymember").click(function(event){
		addDynamicExtraField();
		return false;
	});
	
	var index_i = 0;
	function addDynamicExtraField() {
		var DynamicExtraFieldIndexses = [];
		
		if ($("#AddFamilyMemberContainer .DynamicExtraField").length > 0) {
			$("#AddFamilyMemberContainer .DynamicExtraField").each(function() {
				if ($(this).attr('data-id') !== undefined) {
					DynamicExtraFieldIndexses.push($(this).attr('data-id'));
				}
			});
		
			if (DynamicExtraFieldIndexses.length > 0) {
				index_i = Math.max.apply(null, DynamicExtraFieldIndexses);
				index_i++;
			}
		} else {
			index_i = 0;
		}
		
		var div = $('<div/>', {
			'class' : 'DynamicExtraField',
			'data-id': index_i
		}).appendTo($('#AddFamilyMemberContainer'));
		
		var input = $('<input/>', {
			value : 'удалить',
			type : 'button',
			'class' : 'DeleteDynamicExtraField'
		}).appendTo(div);
		
		var degreeSelect = $('.select-buffer').find('[data-name="family-degree"]').clone();
		degreeSelect.attr('name', 'family['+index_i+'][family-degree]');
		degreeSelect.attr('id', 'family-degree'+index_i);
		degreeSelect.attr('title', 'Степень родства');
		degreeSelect.attr('data-msg', 'Введите степень родства');
		
		var degree = $('<div/>', {
			'class' : 'form-group',
			'attr-select' : (degreeSelect.find('option').length > 0) ? degreeSelect.find('option').first().attr('value') : '',
		}).html('<br><br><label>Степень родства</label><br>').append(degreeSelect).appendTo($(div));

		var fio = $('<div/>', {
			'class' : 'form-group'
		}).html("<input onkeyup='rusinput(\"ru\", this );' class='form-control family-fio-field' name='family["+index_i+"][family-fio]' id='family-FIO"+index_i+"' placeholder='ФИО *' data-msg='Введите ФИО'/>").appendTo($(div));

		var bdate = $('<div/>', {
			'class' : 'form-group'
		}).html("<input class='form-control family-bdate-field' name='family["+index_i+"][family-bdate]' id='family-bdate"+index_i+"' placeholder='Дата рождения'/>").appendTo($(div));
		
		var phone = $('<div/>', {
			'class' : 'form-group'
		}).html("<input class='form-control family-phone-field' name='family["+index_i+"][family-phone]' id='family-phone"+index_i+"' placeholder='Телефон *' data-msg='Введите телефон'/>").appendTo($(div));
		
		var faddress = $('<div/>', {
			'class' : 'form-group'
		}).html("<input class='form-control family-address-field' name='family["+index_i+"][family-member-address]' id='family-member-address"+index_i+"' placeholder='Адрес' data-msg='Введите адрес'/>").appendTo($(div));


		$('input#family-bdate'+index_i+'').datepicker({
			language: "ru",
			startDate: '01-01-1950',
			endDate: '31-12-2003'
		});

		$("#family-phone"+index_i+"").mask("+7(999) 999-99-99");

		input.click(function() {
			$(this).parent().remove();
		});
	}

	//Для удаления первого поля
	$('.DeleteDynamicExtraField').click(function(event) {
		$(this).parent().remove();
		return false;
	});

// -------------// addfamilymember -------------------


// --------------- addlanguage ---------------------

	$("#addlanguage").click(function(event){
		addlanguage();
		return false;
	});

	var index_k = 0;
	function addlanguage() {		
		var DynamicExtraFieldIndexses = [];
		
		if ($("#AddLangContainer .AdditionalLang").length > 0) {
			$("#AddLangContainer .AdditionalLang").each(function() {
				if ($(this).attr('data-id') !== undefined) {
					DynamicExtraFieldIndexses.push($(this).attr('data-id'));
				} else if ($(this).find('[id^="lang"]')) {
					DynamicExtraFieldIndexses.push($(this).find('[id^="lang"]').attr('id').replace('lang', ''));
				}
			});
		
			if (DynamicExtraFieldIndexses.length > 0) {
				index_k = Math.max.apply(null, DynamicExtraFieldIndexses);
				index_k++;
			}
		} else {
			index_k = 0;
		}
		
		if ($('.lang-buffer').length < 1) {
			return;
		}
		
		if ($('.lang-buffer').find('[data-name="languages"]').length < 1) {
			return;
		}
		
		var languageSelect = $('.lang-buffer').find('[data-name="languages"]').clone();
		languageSelect.attr('name', 'languages['+index_k+'][language]');
		languageSelect.attr('class', 'form-control add-lang');
		languageSelect.attr('id', 'lang'+index_k);
		languageSelect.attr('title', 'Выберите язык');
		languageSelect.attr('data-msg', 'Выберите язык');
		
		var div = $('<div/>', {
			'class' : 'AdditionalLang',
			'data-id': index_k,
		}).appendTo($('#AddLangContainer'));
		
		var input = $('<input/>', {
			value : 'удалить',
			type : 'button',
			'class' : 'deleteLang'
		}).appendTo(div);
		
		var language = $('<div/>', {
			'class' : 'language-wrap',
			'attr-select' : (languageSelect.find('option').length > 0) ? languageSelect.find('option').first().attr('value') : '',
		}).append(languageSelect).appendTo($(div));
		
		var checkmain = $("<input class='form-check-input' type='radio' value='addlang"+index_k+"' id='addlang"+index_k+"' name='languages["+index_k+"][main_language]'><label class='form-check-label lang-check' for='addlang"+ index_k +"'>Хочу изучать этот язык</label><br>").appendTo(div);
		var check = $("<input class='form-check-input' type='checkbox' value='addcheck"+index_k+"' id='addcheck"+index_k+"' name='languages["+index_k+"][check_language]'><label class='form-check-label lang-check' for='addcheck"+index_k+"'>Аттестован (Аттестат/Диплом)</label>").appendTo(div);

		input.click(function() {
			$(this).parent().remove();
		});
	}

	//Для удаления первого поля
	$('.deleteLang').click(function(event) {
		$(this).parent().remove();
		return false;
	});

// -----------------// addlanguage ---------------------

//------------------------------------ upload file --------------------------------------------
	$("#fileuploader-1, #fileuploader-2, #fileuploader-3, #fileuploader-4, #fileuploader-5").each(function() {
		$(this).uploadFile({
			url:"file",
			fileName:"myfile",
			multiple:false,
			maxFileCount:1,
			dragDropStr: "<span>или перетяни файл сюда</span>",
			uploadStr:"загрузить"
		});
	});

//----------------------------------// upload file --------------------------------------------

// ------------------------------------- csv files ----------------------------------------------

	function setCsvSelect(data, attrs, selectAttrs, $appendBlock, nameDocument, valueKey) {
		if (valueKey === undefined) {
			valueKey = 0;
		}
		
		var rows = data.split(/\r?\n|\r/);
		
		var options = '';
		
		for (var i = 0; i < rows.length; i++) {
			var rowCells = rows[i].split(';');
			var rowAttrs = '';
			
			for (key in attrs) {
				rowAttrs += key + rowCells[attrs[key]] + '" ';
			}
			
			if (((nameDocument == 'otherdocuments' || nameDocument == 'education') && (i === 0)) || rows[i] == '') {
				continue;
			}
			
			selected = ($appendBlock.attr('attr-select') !== undefined && ($appendBlock.attr('attr-select') == rowCells[0] || $appendBlock.attr('attr-select').indexOf(rowCells[1]) >= 0)) ? ' selected' : '';
			
			options += '<option ' + rowAttrs + selected + '>' + rowCells[valueKey] + '</option>';
		}
			
		$appendBlock.append('<select' + selectAttrs + '>' + options + '</select>');
	}
	
	if ($('.form-group#education-doc').length > 0) {
		$.ajax({
			url: 'documents/education.csv',
			dataType: 'text',	
		})
		.done(function(data) {
			var attrs = {
				'attr-educode="': 1,
				'attr-seriya="': 2,
				'attr-number="': 3
			};
			
			setCsvSelect(data, attrs, ' name="education" class="inddocuments form-control" onchange="changeAttrSel(this); changeEducationDocuments(this)"', $('.form-group#education-doc'), 'education');
		});
	}

	/* step 1 */
	if ($('.form-group.сitizenship').length > 0) {
		$.ajax({
			url: 'documents/countries.csv',
			dataType: 'text'
		})
		.done(function(data) {
			var attrs = {
				'attr-countriescode="': 1,
			};
			
			setCsvSelect(data, attrs, ' name="countries-sel" class="form-control" onchange="changeAttrSel(this)"', $('.form-group.сitizenship'), 'countries');
		});
	}
	
	if ($('.form-group.regions').length > 0) {	
		$.ajax({
			url: 'documents/regions.csv',
			dataType: 'text'
		})
		.done(function(data) {
			var attrs = {
				'attr-regionscode="': 1,
			};
			
			setCsvSelect(data, attrs, ' name="regions-sel" id="regionssel" class="form-control" onchange="CityType(this);"', $('.form-group.regions'), 'regions');
		});
	}
	
	if ($('.form-group.settlement').length > 0) {
		$.ajax({
			url: 'documents/settlement.csv',
			dataType: 'text',	
		})
		.done(function(data) {
			var attrs = {
				'attr-settlementcode="': 1,
				'value="citytype': 1
			};
			
			setCsvSelect(data, attrs, ' name="settlement-sel" id="settlementsel" class="form-control"', $('.form-group.settlement'), 'settlement');
		});
	}

	if ($('.form-group.documenttype2').length > 0 || $('.form-group.inddocuments-step-1').length > 0 || $('.form-group.inddocuments-step-2').length > 0) {
		$.ajax({
			url: 'documents/documents.csv',
			dataType: 'text',	
		})
		.done(inddocuments,inddocuments2,inddocuments3);
	}

	function inddocuments(data) {
		if ($('.form-group.inddocuments-step-1').length > 0) {
		  var allRows = data.split(/\r?\n|\r/);
		  var select = '<select name="inddocuments_other" class="inddocuments form-control" onchange="GetSelndex(this);">';
		  for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
			var rowCells = allRows[singleRow].split(';');
			select += '<option attr-code="'+rowCells[1]+'"  attr-seriya="'+rowCells[2]+'" attr-number="'+rowCells[3]+'">';
			select += rowCells[0];
			select += '</option>';
		  } 
		  select += '</select>';
		  $('.form-group.inddocuments-step-1').append(select);
		}
	}

	function inddocuments2(data) {
		if ($('.form-group.inddocuments-step-2').length > 0) {
			var selectBlock = $('.form-group.inddocuments-step-2').attr("attr-select");	

			var allRows = data.split(/\r?\n|\r/);
			var select = '<select name="inddocuments2" class="inddocuments form-control" disabled="" onchange="GetSelndex2(this);">';
			
			for (var singleRow = 0; singleRow < allRows.length; singleRow++) {

				var rowCells = allRows[singleRow].split(';');

				stringSelect = '';
				if(selectBlock == rowCells[0]){
					stringSelect = 'selected="selected"';
				}

				select += '<option '+stringSelect+' attr-code="'+rowCells[1]+'"  attr-seriya="'+rowCells[2]+'" attr-number="'+rowCells[3]+'">';
				select += rowCells[0];
				select += '</option>';
			}
			
			select += '</select>';
			$('.form-group.inddocuments-step-2').append(select);
		}
	}

	function inddocuments3(data) {
		if ($('.form-group.documenttype2').length > 0) {
			var allRows = data.split(/\r?\n|\r/);
			var select = '<select name="inddocuments3" class="inddocuments form-control" onchange="GetSelndex3(this);">';
			for (var singleRow = 0; singleRow < allRows.length; singleRow++) {
				var rowCells = allRows[singleRow].split(';');
				selected = ($('.form-group.documenttype2').attr('attr-select') !== undefined && $('.form-group.documenttype2').attr('attr-select') == rowCells[0]) ? 'selected' : '';
				
				select += '<option attr-code="'+rowCells[1]+'" attr-seriya="'+rowCells[2]+'" attr-number="'+rowCells[3]+'" ' + selected + '>';
				select += rowCells[0];
				select += '</option>';
			} 
			select += '</select>';
			$('.form-group.documenttype2').append(select);
		}
	}
	/* end step 1 */

	/* step 2 */
	if ($('.marital-status-CSV').length > 0) {
		$.ajax({
			url: 'documents/marital_status.csv',
			dataType: 'text',	
		})
		.done(function(data) {
			var attrs = {
				'attr-statuscode="': 0,
				'value="': 1,
			};
			
			setCsvSelect(data, attrs, ' name="maritalstatus" id="MaritalStatus" required class="form-control" onchange="changeAttrSel(this)"', $('.marital-status-CSV'), 'marital_status', 1);
			setMaritalStatus();
		});
	}

	if ($('.select-buffer').length > 0) {
		$.ajax({
			url: 'documents/family_member.csv',
			dataType: 'text',	
		})
		.done(function(data) {
			var attrs = {
				'attr-membercode="': 0,
				'value="': 0,
			};
			
			setCsvSelect(data, attrs, ' data-name="family-degree" class="form-control" onchange="changeAttrSel(this)"', $('.select-buffer'), 'family_member', 1);
			setFamilyDegrees();
		});
	}
	/* end step 2 */
	
	/* step 3 */
	if ($('.lang-buffer').length > 0) {
		$.ajax({
			url: 'documents/languages.csv',
			dataType: 'text',	
		})
		.done(function(data) {
			var attrs = {
				'attr-languagescode="': 0,
				'value="': 0,
			};
			
			setCsvSelect(data, attrs, ' data-name="languages" class="form-control" onchange="changeAttrSel(this)"', $('.lang-buffer'), 'languages', 1);
			setLanguages();
		});
	}
	/* end step 3 */

// -----------------------------------// csv files ----------------------------------------------	

	if ($(".form-check-input").length > 0) {
		$(".form-check-input").each(function() {
			if ($(this).is(':checked')){
				$(".form-group."+$(this).attr("id")+" input").val("");
				$(".form-group."+$(this).attr("id")+" input").not(this).prop("disabled", true);
				$(".form-group."+$(this).attr("id")+" input").not(this).hide();
				$(".form-group."+$(this).attr("id")+" input").not(this).rules("add", {
					required: false,
				});
			} else {
				$(".form-group."+$(this).attr("id")+" input").not(this).prop("disabled", false);
				$(".form-group."+$(this).attr("id")+" input").not(this).show();
				$(".form-group."+$(this).attr("id")+" input").not(this).rules("add", {
					required: true,
				});
			}
		});
		
		$(".form-check-input").change(function(){
			if ($(this).is(':checked')){
				$(".form-group."+$(this).attr("id")+" input").val("");
				$(".form-group."+$(this).attr("id")+" input").not(this).prop("disabled", true);
				$(".form-group."+$(this).attr("id")+" input").not(this).hide();
				$(".form-group."+$(this).attr("id")+" input").not(this).rules("add", {
					required: false,
				});
			} else {
				$(".form-group."+$(this).attr("id")+" input").not(this).prop("disabled", false);
				$(".form-group."+$(this).attr("id")+" input").not(this).show();
				$(".form-group."+$(this).attr("id")+" input").not(this).rules("add", {
					required: true,
				});
			}
		});
	}

	$("#sameresidenceaddress_ch").change(function(){
		$(".sameresidenceaddress").toggle();
	});

	$("#documenttype2").change(function(){

    	$(".documenttype2-block").toggle();
    	// $(".number-documenttype2").toggle();

		if ($('#documenttype2').is(':checked')){
	    	var SeriesFlagDoc2 = $(".documenttype2 .inddocuments :selected").attr("attr-seriya");    
		    if(SeriesFlagDoc2 == "Да"){
		    	$(".series-documenttype2.series").show();
		    	$(".series-documenttype2.series input").attr('required', true);
		    }   
		   	else{ 
		   		$(".series-documenttype2.series").hide();
		    	$(".series-documenttype2.series input").attr('required', false);
		    	$(".series-documenttype2.series input").val('');
		    }
		    
		} else {
			$(".series-documenttype2").hide();
		}

	});

	var fullYear =  new Date().getFullYear();
	var fullMonth =  new Date().getMonth();
	var fullMonth = fullMonth+1;
	var fullDay =  new Date().getDate();

	if ($('#DocDate').length > 0) {
		$('#DocDate').datepicker({
			// language: "ru",
			// startDate: '01-01-1950',
			// endDate: '31-12-2003'
			language: "ru",
			dateFormat:"yy/mm/dd",
			endDate: ''+fullDay+'-'+fullMonth+'-'+fullYear+''
		});
	}

	if ($('.bdate-picker').length > 0) {
		$('.bdate-picker').datepicker({
			language: "ru"
		});
	}
	
	if ($('input#date-education-doc').length > 0) {
		$('input#date-education-doc').datepicker({
			language: "ru",
			dateFormat:"yy/mm/dd",
			endDate: ''+fullDay+'-'+fullMonth+'-'+fullYear+''
		});
	}

	if ($('input#birthdate2').length > 0) {
		$('input#birthdate2').datepicker({
			language: "ru",
			startDate: '01-01-1950',
			endDate: '31-12-2003'
		});
	}

	if ($('#birthdate').length > 0) {
		$('#birthdate').datepicker({
			language: "ru",
			startDate: '01-01-1950',
			endDate: '31-12-2003'
		});
	}

	if ($('input#BirthDate').length > 0) {
		$('input#BirthDate').datepicker({
			language: "ru",
			startDate: '01-01-1950',
			endDate: '31-12-2003'
		});
	}

	if ($('input#otherdoc-date1').length > 0) {
		$('input#otherdoc-date1').datepicker({
			language: "ru",
			startDate: '01-01-1950',
			endDate: '31-12-2003'
		});
	}

// ------------------------------------- multiStepForm ----------------------------------------------
    
    function ViewModel() {
        var self = this;
        self.Name = ko.observable('');
        self.Email = ko.observable('');
        self.Details = ko.observable('');

        self.AdditionalDetails = ko.observable('');
        self.availableTypes = ko.observableArray(['New', 'Open', 'Closed']);
        self.chosenType = ko.observable('');

        self.availableCountries = ko.observableArray(['France', 'Germany', 'Spain', 'United States', 'Mexico']),
        self.chosenCountries = ko.observableArray([]) 

    }

    var viewModel = new ViewModel();

    ko.applyBindings(viewModel);

    $(document).on("msf:viewChanged", function(event, data){
        var progress = Math.round((data.completedSteps / data.totalSteps)*100);

        $(".progress-bar").css("width", progress + "%").attr('aria-valuenow', progress);   ;
    });

	if ($(".msf").length > 0 && typeof(multiStepForm) !== 'undefined') {
		$(".msf:first").multiStepForm({
			activeIndex: 0,
			hideBackButton : false,
			allowUnvalidatedStep : false,
			allowClickNavigation : false,
			validate: {}
		});
	}
    
    $("form").validate({
    	rules: {
			email: {email: true},
		},
		messages: {
			email: "Введите корректный email",
		}
	});
	
    $("#formmsf1").validate({
    	rules: {
			email: {required: true,email: true},
			email2: {required: true,email: true},
			passportseries: {required: true, number: true},
			passportseries2: {required: true, number: true},
			passportseries3: {required: true, number: true},
			passportnumber: {required: true, number: true},			
			passportnumber2: {required: true, number: true},
			passportnumber3: {required: true, number: true},
			lastname:{required: true},
			lastname2:{required: true},
			firstname:{required: true},
			firstname2:{required: true},
			patronymic:{required: true},
			patronymic2:{required: true},
			birthdate:{required: true},
			registrationaddress:{required: true},
			contactphone:{required: true},
			sameresidenceaddress:{required: true},
			fio:{required: true},
			socialscience:{required: true},
			russian:{required: true},
			history:{required: true},
			placeeducationdoc:{required: true},
			whereeducationdoc:{required: true},
			avgscoreeducationdoc: { number: true },
		},		
		messages: {
			email: "Введите корректный email",
			email2: "Введите корректный email",
			passportseries: "Введите серию документа",
			passportseries2: "Введите серию документа",
			passportseries3: "Введите серию документа",
			passportnumber: "Введите номер документа",
			passportnumber2: "Введите номер документа",
			passportnumber3: "Введите номер документа",
			lastname: "Введите фамилию",
			lastname2: "Введите фамилию",
			firstname: "Введите имя",
			firstname2: "Введите имя",
			patronymic: "Введите отчество",
			patronymic2: "Введите отчество",
			birthdate: "Введите дату рождения (возраст не менее 16 лет)",
			russian: "Введите значение (от 40 до 100) или 0",
			socialscience: "Введите значение (от 50 до 100) или 0",
			history: "Введите значение (от 50 до 100) или 0",
			// birthdate2: "Введите дату рождения (возраст не менее 16 лет)",
			// birthplace: "Введите дату рождения (возраст не менее 16 лет)",
			registrationaddress: "Укажите адрес регистрации",
			contactphone: "Введите телефон",
			sameresidenceaddress: "Укажите адрес проживания",
			fio: "Введите ФИО" 
		}
    });

	$.validator.addClassRules("family-fio-field", {
		  	required: true
		}
	);

	$.validator.addClassRules("family-phone-field", {
		  	required: true
		}
	);

	$('.kladr-block input').keyup(function(){
		var this_element = $(this);
		setTimeout(function() {
		// console.log('aaa');

		if(this_element.hasClass('error')){
			this_element.siblings('.important-label').show();			
		}
		if(!this_element.hasClass('kladr-error') && this_element.val()!=''){
		// 	console.log('222');
		// 	$(this).siblings('label').show();
		// 	data_msg = $(this).attr('data-msg');
		// 	$(this).siblings('label').text(data_msg);
		// 	// console.log($(this));
		// }
		// else{
			// console.log('333');
			this_element.siblings('.important-label').hide();
			if(  this_element.attr('name') =='zip'  ){
				$('.kladr-block input').each(function(){
					if($(this).val() !=''){
						$(this).siblings('.important-label').hide();
						$(this).removeClass('kladr-error');
						$(this).removeClass('error');
					}
				});
			}
		}
		}, 300);
	});

	$("[type='submit'].msf-nav-button").click(function(){
		if ($('.kladr-block input').length > 0) {
			$('.kladr-block input').each(function(){
				if($(this).val() ==''){
					$(this).siblings('.important-label').show();
				}
			});
		}
		
		if ($(".gender .custom-control-input").length > 0 && $(".gender .custom-control-input").hasClass("valid")) {
			$(".gender span.error").hide();
		}
	});
	
// -----------------------------------// multiStepForm ----------------------------------------------

	// переключение шага
	setTimeout(function() {
		changeStep();
	}, 500);

	//-------------------------------------// show Series -----------------------------------------------
	$('[id^="PassportSeries"], [id^="PassportNumber"]').each(function() {
		$(this).change(function() {
			$(this).val($(this).val().replace(/\s+/g, ''));
		});
	});

	$('[name^="avgscoreeducationdoc"]').each(function() {	
		$(this).change(function() {
			onlynumber2($(this));
		});
	});

	$("#PassportSeries").keyup(function(){$("#PassportSeries2").val($("#PassportSeries").val());});
	$("#PassportNumber").keyup(function(){$("#PassportNumber2").val($("#PassportNumber").val());});

	$("#PassportSeries").keyup(function(){$("#DocSeries").val($("#PassportSeries").val());});
	$("#PassportNumber").keyup(function(){$("#DocNumber").val($("#PassportNumber").val());});

	$("#DocSeries").keyup(function(){$("#PassportSeries").val($("#DocSeries").val());});
	$("#DocNumber").keyup(function(){$("#PassportNumber").val($("#DocNumber").val());});


	$("#patronymic").keyup(function(){$("#patronymic2").val($("#patronymic").val());});
	$("#firstname").keyup(function(){$("#firstname2").val($("#firstname").val());});
	$("#lastname").keyup(function(){$("#lastname2").val($("#lastname").val());});
	$("#birthdate").change(function(){$("#birthdate2").val($("#birthdate").val());});
	$("#birthdate2").change(function(){$("#birthdate").val($("#birthdate2").val());});

	// ---------------- addstatement ---------------------

	$("#addstatement").click(function(){
		$("#addstatement_form").show("fast");
	});

	// --------------// addstatement ---------------------

	$("#birthdate,#birthdate2").change(function(){

		var userAge = getAge($(this).val());
		$(".form-date .hidden-date").val(userAge);

		if (userAge >= 16){
			$(".form-date .hide-message").hide();
			$(".message-parent").show();
		}
		if (userAge < 16){
			$("#birthdate").val("");
			$("#birthdate2").val("");
			$(".form-date .hide-message").show();
		}
		if (userAge > 18){
			$(".message-parent").hide();
		}
	});
	
	setTimeout(function() {
		if ($('#russian, #socialscience, #history').length > 0) {
			$('#russian, #socialscience, #history').each(function() {
				if ($(this).val() == 0) {
					$(this).rules("add", {
						min: 0,
						max: ($(this).attr('max') !== undefined) ? parseInt($(this).attr('max')) : 0,
					});
				} else {
					$(this).rules("add", {
						min: ($(this).attr('min') !== undefined) ? parseInt($(this).attr('min')) : 0,
						max: ($(this).attr('max') !== undefined) ? parseInt($(this).attr('max')) : 0,
					});
				}
			});
		}
	}, 200);

	$(".continue-block").click(function(){
		$("#formreg").hide();
		$(this).hide();
		$("#formmsf").show();
	});
});

//--------------------------------------- show Series -----------------------------------------------

function setMaritalStatus() {
	if ($('.marital-status-CSV').prev('select#MaritalStatus').length > 0) {
		$('.marital-status-CSV').prev('select#MaritalStatus').remove();
	}
}

function setFamilyDegrees() {
	$("#AddFamilyMemberContainer .DynamicExtraField").each(function() {
		var selected;
		
		var $inputDegree = $(this).find('input[id^="family-degree"], select[id^="family-degree"]');
		
		
		if ($inputDegree.length > 0) {
			var index = ($(this).attr('data-id') !== undefined) ? $(this).attr('data-id') : $inputDegree.attr('id').replace('family-degree', '');
			var selected = ($inputDegree.closest('[attr-select]').length > 0) ? $inputDegree.attr('attr-select') : '';
		
			var degreeSelect = $('.select-buffer').find('[data-name="family-degree"]').clone();
			degreeSelect.attr('name', 'family[' + index + '][family-degree]');
			degreeSelect.attr('id', 'family-degree' + index);
			degreeSelect.attr('title', 'Степень родства');
			degreeSelect.attr('data-msg', 'Введите степень родства');
			
			if (degreeSelect.find('option[value="' + selected + '"]').length > 0) {
				degreeSelect.find('option[value="' + selected + '"]').prop('selected', true);
			}
			$inputDegree.closest('.form-group').html('<br><br><label>Степень родства</label><br>').append(degreeSelect);
		}
	});
}

function setLanguages() {
	$("#AddLangContainer .AdditionalLang").each(function() {
		var selected;
		
		var $inputLanguage = $(this).find('input[name^="languages"].add-lang, select[name^="languages"].add-lang');
		
		if ($inputLanguage.length > 0) {
			var index = ($(this).attr('data-id') !== undefined) ? $(this).attr('data-id') : $inputLanguage.attr('id').replace('family-degree', '');
			var selected = ($inputLanguage.closest('[attr-select]').length > 0) ? $inputLanguage.attr('attr-select') : '';
			
			var languageSelect = $('.lang-buffer').find('[data-name="languages"]').clone();
			languageSelect.attr('name', 'languages['+index+'][language]');
			languageSelect.attr('class', 'form-control add-lang');
			languageSelect.attr('id', 'lang'+index);
			languageSelect.attr('title', 'Выберите язык');
			languageSelect.attr('data-msg', 'Выберите язык');
			
			if (languageSelect.find('option[value="' + selected + '"]').length > 0) {
				languageSelect.find('option[value="' + selected + '"]').prop('selected', true);
			}
			
			$inputLanguage.closest('.language-wrap').append(languageSelect);
			$inputLanguage.remove();
		}
	});
}

function changeAttrSel(self){	
    $(self).closest('[attr-select]').attr("attr-select", $(self).val());
}

function changeEducationDocuments(select) {
	if ($(select).val() != '') {
		var $selectedDocument = $(select).find('option:selected');
		
		if ($selectedDocument.attr('attr-seriya') == 'Да') {
			$('#series-education-doc').attr('required', true).show();
			if ($('#series-education-doc').attr('placeholder').indexOf('*') < 0) {
				$('#series-education-doc').attr('placeholder', $('#series-education-doc').attr('placeholder') + '*');	
			}
		} else {
			$('#series-education-doc').attr('required', false);
			$('#series-education-doc').attr('placeholder', $('#series-education-doc').attr('placeholder').replace(/\*/g, ''));
		}
		
		if ($selectedDocument.attr('attr-number') == 'Да') {
			$('#number-education-doc').attr('required', true).show();	
			if ($('#number-education-doc').attr('placeholder').indexOf('*') < 0) {
				$('#number-education-doc').attr('placeholder', $('#number-education-doc').attr('placeholder') + '*');
			}	
		} else {
			$('#number-education-doc').attr('required', false);
			$('#number-education-doc').attr('placeholder', $('#number-education-doc').attr('placeholder').replace(/\*/g, ''));
		}
		
		if ($selectedDocument.attr('attr-date') == undefined || $selectedDocument.attr('attr-date') == 'Да') {
			$('#date-education-doc').attr('required', true).show();		
			if ($('#date-education-doc').attr('placeholder').indexOf('*') < 0) {
				$('#date-education-doc').attr('placeholder', $('#date-education-doc').attr('placeholder') + '*');		
			}
		} else {
			$('#date-education-doc').attr('required', false);
			$('#date-education-doc').attr('placeholder', $('#date-education-doc').attr('placeholder').replace(/\*/g, ''));
		}
	} else {
		$('#number-education-doc').attr('required', false).attr('placeholder', $('#number-education-doc').attr('placeholder').replace(/\*/g, ''));
		$('#series-education-doc').attr('required', false).attr('placeholder', $('#series-education-doc').attr('placeholder').replace(/\*/g, ''));
		$('#date-education-doc').attr('required', false).attr('placeholder', $('#date-education-doc').attr('placeholder').replace(/\*/g, ''));
		clearValidation($('#series-education-doc, #number-education-doc, #date-education-doc'));
	}
}

function GetSelndex(sel){

    var SeriesFlagStep1 = $(".inddocuments-step-1 .inddocuments :selected").attr("attr-seriya");    
    if(SeriesFlagStep1 == "Да"){
    	$(".series-step-1.series").show();
    	$(".series-step-1.series input").attr('required', true);
    	
    	$(".series.series-document-type").show();
    	$(".series.series-document-type input").attr('required', true);

    	$(".series.series series-step-2").show();
    	$(".series.series series-step-2 input").attr('required', true);
    }   
   	else{ 
   		$(".series-step-1.series").hide();
    	$(".series-step-1.series input").attr('required', false);
    	$(".series-step-1.series input").val('');

   		$(".series-document-type.series").hide();
    	$(".series-document-type.series input").attr('required', false);
    	$(".series-document-type.series input").val('');

   		$(".series-step-2.series").hide();
    	$(".series-step-2.series input").attr('required', false);
    	$(".series-step-2.series input").val('');
    }

    var SeriesFlagIndex = $(".inddocuments :selected").index()+1;

    $(".inddocuments-step-2 .inddocuments option:nth-child("+SeriesFlagIndex+")").attr("selected", "selected");
    $(".person-document-block .inddocuments option:nth-child("+SeriesFlagIndex+")").attr("selected", "selected");

    $(".inddocuments-step-1").attr("attr-select",$(".inddocuments :selected").val());

}

function GetSelndex2(sel2){
	var SeriesFlagStep2 = $(".inddocuments-step-2 .inddocuments :selected").attr("attr-seriya");    
    if(SeriesFlagStep2 == "Да"){
    	$(".series-step-2.series").show();
    	$(".series-step-2.series input").attr('required', true);
    }   
   	else{ 
   		$(".series-step-2.series").hide();
    	$(".series-step-2.series input").attr('required', false);
    	$(".series-step-2.series input").val('');
    }

    $(".inddocuments-step-2").attr("attr-select",$(".inddocuments-step-2 .inddocuments :selected").val());
}

function GetSelndex3(sel3){
    var SeriesFlagDoc2 = $(".documenttype2 .inddocuments :selected").attr("attr-seriya");    
    if(SeriesFlagDoc2 == "Да"){
    	$(".series-documenttype2.series").show();
    	$(".series-documenttype2.series input").attr('required', true);
    } else{ 
   		$(".series-documenttype2.series").hide();
    	$(".series-documenttype2.series input").attr('required', false);
    	$(".series-documenttype2.series input").val('');
    }

    $(sel3).closest('[attr-select]').attr("attr-select", $(sel3).val());
}

function GetSelndex4(sel4){
    var SeriesFlagDoc4 = $("#document-3-type .inddocuments :selected").attr("attr-seriya");    
    if(SeriesFlagDoc4 == "Да"){
    	$(".series-document-type.series").show();
    	$(".series-document-type.series input").attr('required', true);
    }   
   	else{ 
   		$(".series-document-type.series").hide();
    	$(".series-document-type.series input").attr('required', false);
    	$(".series-document-type.series input").val('');
    }

    $(sel4).closest('[attr-select]').attr("attr-select", $(sel4).val());
}

function GetSelndexOther(sel5){
	var ChangeSelctId = $(sel5).attr("id");
	var $selectedDocument = $(".OtherDocumentBlock #"+ChangeSelctId+" :selected");

    var flagSeries = $selectedDocument.attr("data-series");  
    var flagNumber = $selectedDocument.attr("data-number");  
    var flagDate = $selectedDocument.attr("data-date");  
    
    if (flagSeries == "Да" || flagSeries == 1) {
    	$(".series-otherdoc."+ChangeSelctId+"").show();
    	$(".series-otherdoc."+ChangeSelctId+" input").attr('required', true);
    } else{ 
   		$(".series-otherdoc."+ChangeSelctId+"").hide();
    	$(".series-otherdoc."+ChangeSelctId+" input").attr('required', false);
    	$(".series-otherdoc."+ChangeSelctId+" input").val('');
    } 
    
    if (flagNumber == "Да" || flagNumber == 1) {
    	$(".number-otherdoc."+ChangeSelctId+"").show();
    	$(".number-otherdoc."+ChangeSelctId+" input").attr('required', true);
    } else { 
   		$(".number-otherdoc."+ChangeSelctId+"").hide();
    	$(".number-otherdoc."+ChangeSelctId+" input").attr('required', false);
    	$(".number-otherdoc."+ChangeSelctId+" input").val('');
    } 

    if (flagDate == "Да" || flagDate == 1) {
    	$(".date-otherdoc."+ChangeSelctId+"").show();
    	$(".date-otherdoc."+ChangeSelctId+" input").attr('required', true);
    } else{ 
   		$(".date-otherdoc."+ChangeSelctId+"").hide();
    	$(".date-otherdoc."+ChangeSelctId+" input").attr('required', false);
    	$(".date-otherdoc."+ChangeSelctId+" input").val('');
    }
	
    $(sel5).closest('[attr-select]').attr("attr-select", $(sel5).val());
}

function CityType(city){
	var SelectCity = $("#regionssel :selected").attr("attr-regionscode");
	if(SelectCity == 78 || SelectCity == 77 || SelectCity == 92){
		$("#settlementsel").val("citytype00-000001").change();
	}
	
    $(city).closest('[attr-select]').attr("attr-select", $(city).val());
}

// дата рождения в формате '22.05.1990'

function getAge(dateString) {
  var day = parseInt(dateString.substring(0,2));
  var month = parseInt(dateString.substring(3,5));
  var year = parseInt(dateString.substring(6,10));

  var today = new Date();
  var birthDate = new Date(year, month - 1, day); // 'month - 1' т.к. нумерация месяцев начинается с 0 
  var age = today.getFullYear() - birthDate.getFullYear();
  var m = today.getMonth() - birthDate.getMonth();
  if (m < 0 || (m === 0 && today.getDate() < birthDate.getDate())) { 
      age--;
  }
  return age;
}

function onlyNumber(input) {
    var ch = input.value.replace(/[^\d]/g, ''); //разрешаем вводить только числа
    input.value = ch; //приписываем в инпут новое значение
};

function onlynumber(input) {
    ch = input.value.replace(/[^\d,]/g, ''); //разрешаем вводить только числа и запятую
    pos = ch.indexOf(','); // проверяем, есть ли в строке запятая
    if(pos != -1){ // если запятая есть
        if((ch.length-pos)>2){ // проверяем, сколько знаков после запятой, если больше 1го то
            ch = ch.slice(0, -1); // удаляем лишнее
        }
    }
    input.value = ch; // приписываем в инпут новое значение
};

function onlynumber2(input) {
    ch = input.val().replace(/[^\d,]/g, ''); //разрешаем вводить только числа и запятую
    pos = ch.indexOf(','); // проверяем, есть ли в строке запятая
	
    if (pos != -1){ // если запятая есть
        if((ch.length - pos) >= 2){ // проверяем, сколько знаков после запятой, если больше 1го то
            ch = ch.slice(0, 4); // удаляем лишнее
        }
    }
	
    input.val(ch); // приписываем в инпут новое значение
};

function rusinput( k, obj ) {
	switch( k ){
	    case 'ru':
	    obj.value = obj.value.replace(/[^а-яА-ЯёЁ0-9 -]/ig,'');
	    break;
	}
}

function addValidateStep6(dataSubmit) {
	var noValidate = false;
	
	if (dataSubmit !== undefined && dataSubmit['action'] == 'send') {	
		if ($('[name^="app"][name$="[profile]"]:not(.is-disabled)').length > 0) {
			$('[name^="app"][name$="[profile]"]:not(.is-disabled)').each(function() {
				var $selectProfile = $(this);
				
				if ($selectProfile.closest('tr').find('[name^="app"][name$="[take]"]:checked').length > 0) {
					if ($selectProfile.length > 0 && ($selectProfile.find('option').filter(':selected').length < 1 || $selectProfile.val() == '')) {
						$selectProfile.addClass('is-error');
						if ($selectProfile.siblings('.step-alert.is-error').length < 1) {
							$selectProfile.before('<div class="step-alert is-error">Пожалуйста, выберите программу</div>');	
						}
						noValidate = true;
					} else {
						$selectProfile.removeClass('is-error');
						$selectProfile.siblings('.step-alert.is-error').remove();
					}
				} else {
					$selectProfile.removeClass('is-error');
					$selectProfile.siblings('.step-alert.is-error').remove();
				}
			});
		}
	
		if ($('[name="app_main"]').length > 0 && $('[name="app_main"]:checked').length < 1) {
			if ($('[name="app_main"]').closest('table').siblings('.js-form-error').length < 1) {
				$('[name="app_main"]').closest('table').before('<p class="js-form-error"><b><span class="is-error">Обязательно должно быть указано &laquo;согласие о зачислении&raquo; (приоритетное заявление)</span></b></p>');
			}
			
			$('[name="app_main"]').closest('table').find('tr:first-child th').eq($('[name="app_main"]').closest('td').index()).addClass('is-error');
			noValidate = true;
		}
	
		if ($('[name^="app"][name$="[take]"]').length > 0) {
			$('[name^="app"][name$="[take]"]').each(function() {
				if ($(this).attr('data-file') !== undefined && $(this).attr('data-file') == 1 && $(this).closest('tr').find('[name^="applctn"]').length > 0) {
					var $file = $(this).closest('tr').find('[name^="applctn"]');
					
					if ($(this).prop('checked')) {
						if ($file[0].files == undefined || $file[0].files.length < 1 || $file.siblings(".file-result").text().indexOf("Выбран файл: ") < 0) {
							$file.siblings(".file-result").html("<span class='is-error'>Загрузите файл</span>");
							noValidate = true;
						}
					} else if (!$(this).prop('checked') && $file.siblings(".file-result").find('.is-error').length > 0) {
						$file.siblings(".file-result").html("");
					}
				}
			});
		}
	} else {
		$('[name^="app"][name$="[profile]"]:not(.is-disabled)').removeClass('is-error');
		$('[name^="app"][name$="[profile]"]:not(.is-disabled)').closest('tr').find('.step-alert.is-error').remove();
	}
	
	if ($('[name="app_main"]').length > 0 && $('[name="app_main"]:checked').length > 0) {
		
		if ($('[name="app_main"]:checked').closest('tr').find('[name^="app"][name$="[profile]"]:not(.is-disabled)').length > 0) {
			var $selectProfile = $('[name="app_main"]:checked').closest('tr').find('[name^="app"][name$="[profile]"]:not(.is-disabled)');
				
			if ($selectProfile.length > 0 && 
				(($selectProfile.attr('data-change') == undefined || $selectProfile.attr('data-change') != 1) &&
				($selectProfile.find('option[selected]').length < 1)
			)) {
				$selectProfile.addClass('is-error');
				if ($selectProfile.siblings('.step-alert.is-error').length < 1) {
					$selectProfile.before('<div class="step-alert is-error">Пожалуйста, выберите программу</div>');	
				}
				noValidate = true;
			}
		}
		
	}
	
	if (noValidate) {
		return false;
	}
	
	return true;
}

function changeStep() {	
	if ($('.js-form-address').length > 0) {
		var startFormData = $('.js-form-address').serialize();
	}

	$('.msf-step')
		.on('click', 'a', function(e) {
		
			if (startFormData !== undefined && startFormData !== $('.js-form-address').serialize()) {
				
				e.preventDefault();
				
				$('body').append('<div class="msf-popup js-msf-popup"><div class="msf-popup__overlay"></div>' +
				'<div class="msf-popup__content"><div class="msf-popup__close js-msf-popup-close"></div><p>Введенные данные будут потеряны, продолжить?</p>'+
				'<div class="msf-popup__btns text-center"><a class="btn btn-sm btn-primary js-msf-popup-close" href="#">Нет</a><a class="btn btn-sm btn-primary" href="'+ $(this).attr('href') +'">Да</a></div></div></div>');
				
				return false;
			}
			
			return true;
		})
		.on('click', '.js-msf-popup-close', function(e) {
		
			$('.js-msf-popup').remove();
			
		})
		.on('click', function() {
			$('.js-msf-popup-close').click();
		})
		.keydown(function(e) {
			if (e.keyCode == 27) {
				$('.js-msf-popup-close').click();
			}
		})
		.on('click', '.js-msf-popup', function(e) {
			e.stopPropagation();
		});
}

// добавление иных документов - 4 шаг
function addotherdocs() {	
	var index_m = 0;	
	var DynamicExtraFieldIndexses = [];
	var OtherDocumentBlockCount = $("#other-documents .OtherDocumentBlock").length;		
	
	if ($(".OtherDocumentBlock").length > 0) {
		$(".OtherDocumentBlock").each(function() {
			if ($(this).attr('data-id') !== undefined) {
				DynamicExtraFieldIndexses.push($(this).attr('data-id'));
			}
		});
	
		if (DynamicExtraFieldIndexses.length > 0) {
			index_m = Math.max.apply(null, DynamicExtraFieldIndexses);
			index_m++;
		}
	} else {
		index_m = 0;
	}
	
	index_n = index_m + 4;
	
	var div = $('<div/>', {
		'class' : 'OtherDocumentBlock',
		'data-id' : index_m,
	}).appendTo($('#other-documents'));
	
	var input = $('<input/>', {
		value : 'удалить',
		type : 'button',
		'class' : 'DeleteOtherDoc'
	}).appendTo(div);
	
	var documentSelect = $('.other-documents-types').find('select').clone();
	documentSelect.attr('class', 'form-control other-documents');
	documentSelect.attr('id', 'otherdocuments'+index_m);
	documentSelect.attr('name', 'other_documents['+index_m+'][type]');
	documentSelect.attr('onchange', 'GetSelndexOther(this);');
	
	var select = $('<div/>', {
		'class' : '',
		'attr-select' : (documentSelect.find('option').length > 0) ? documentSelect.find('option').first().attr('value') : '',
	}).append(documentSelect).appendTo($(div));
	
	/*var label = $('<select onchange="" id="otherdocuments'+index_m+'" name="other_documents[' + index_m + '][type]" class="form-control other-documents"/>').appendTo(div);
	var select = $('.other-documents-types select>').clone().appendTo("#otherdocuments"+index_m+"");*/

	if (documentSelect.find('option[data-series="1"]').length > 0) {
		$('<div/>', {
			'class' : 'col-md-4 series-otherdoc otherdocuments'+index_m,
			'style': (documentSelect.find('option:first-child').attr('data-series') == 1) ? '' : 'display: none;',
		}).html("<input class='form-control otherdoc-seriya-field' name='other_documents[" + index_m + "][series]' id='otherdoc-seriya"+index_m+"' placeholder='Серия документа *' data-msg='Введите серию' required/>").appendTo($(div));
	}
	
	if (documentSelect.find('option[data-number="1"]').length > 0) {
		$('<div/>', {
			'class' : 'col-md-4 number-otherdoc otherdocuments'+index_m,
			'style': (documentSelect.find('option:first-child').attr('data-number') == 1) ? '' : 'display: none;',
		}).html("<input class='form-control otherdoc-number-field' name='other_documents[" + index_m + "][number]' id='otherdoc-number"+index_m+"' placeholder='Номер документа *' data-msg='Введите номер' required/>").appendTo($(div));
	}
	
	if (documentSelect.find('option[data-date="1"]').length > 0) {
		$('<div/>', {
			'class' : 'col-md-4 date-otherdoc otherdocuments'+index_m+''
		}).html("<input class='form-control otherdoc-date-field' name='other_documents[" + index_m + "][date]' id='otherdoc-date"+index_m+"' placeholder='Дата *' data-msg='Введите дату' required/>").appendTo($(div));
	}
	
	var dfile = $('<div/>', {
		'class' : 'col-md-12 input-file-container otherdocuments'+index_m+''
	}).html(
		"<input class='input-file-"+index_n+"' id='my-file-"+index_n+"' name='other_documents[" + index_m + "]' type='file' accept='.png,.jpg,.pdf,.rtf,.xls,.xlsx,.doc,.docx'/>"+
		"<label for='my-file-"+index_n+"' class='input-file-trigger-3"+index_n+"'><i class='fas fa-upload'></i><span>загрузить файл...</span></label><p class='file-return-"+index_n+" file-result'></p>").appendTo($(div)
	);
	
	var todayDate = new Date();
	var todayDateArray = {};
	todayDateArray['fullYear'] = todayDate.getFullYear();
	todayDateArray['fullMonth'] = todayDate.getMonth() + 1;
	todayDateArray['fullDay'] = todayDate.getDate();

	$('input#otherdoc-date'+index_m+'').datepicker({
		language: "ru",
		dateFormat:"yy/mm/dd",
		endDate: todayDateArray['fullDay'] + '-' + todayDateArray['fullMonth'] + '-' + todayDateArray['fullYear']
	});

	input.click(function() {
		$(this).parent().remove();
	});
}

function clearValidation($inputs) {
	//Internal $.validator is exposed through $(form).validate()
	var validator = $inputs.closest('form').validate();
	
	//Iterate through named elements inside of the form, and mark them as error free
	$inputs.each(function(){
		$(this).blur();
		$(this).siblings('label.error').remove();
		$(this).removeClass('error');
		$(this).attr('aria-invalid', false);
		validator.successList.push(this); //mark as error free
		validator.showErrors(); //remove error messages if present
	});
}