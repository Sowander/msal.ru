<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();?>
<? if (!$isMain): ?>
	<footer class="msal-footer msal-secondary-footer">
		<div class="msal-footer__btns">
					<?
					$home = '/terminal/';
                    $backName = 'ВЕРНУТЬСЯ НАЗАД';
					$step = intval($_GET['step']);
					$arDell = ["step", "page"];
					if ($step === 5) {
						$arDell = array_merge($arDell, ['name', 'guid']);
					} elseif ($step === 4) {
						$arDell = array_merge($arDell, ['inst']);
					} elseif ($step === 3) {
						$arDell = array_merge($arDell, ['form']);
					} elseif ($step === 2) {
						$arDell = array_merge($arDell, ['course']);
					}

					$back = $APPLICATION->GetCurPageParam("step=" . strval($step - 1) . '&page=1', $arDell);

					if ($step - 1 <= 0) {
						$back = $home;
					}
					if($isNewsDetail){
                        $backName = 'ВЕРНУТЬСЯ К НОВОСТЯМ';
                        $back = $home.'news/index.php';
                    }
					?>
			<a href="<?= $back; ?>" class="msal-footer__btns-left"><?=$backName?></a>
			<a href="/terminal/" class="msal-footer__btns-right">ВЕРНУТЬСЯ НА ГЛАВНУЮ</a>
		</div>
	</footer>
<? endif; ?>
</div></body></html>