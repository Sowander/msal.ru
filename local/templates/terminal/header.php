<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Localization\Loc,
    Bitrix\Main\Page\Asset;

Loc::loadMessages(__FILE__);
global $APPLICATION;
$isMain = $APPLICATION->GetCurPage(true) === '/terminal/index.php';
$isNewsDetail = stripos($APPLICATION->GetCurPage(true), '/detail.php') !== false;
if ($isMain) {
    $logoPath = '/images/logo.png';
} else {
    $logoPath = '/images/logo-sm.png';

}

?>
<!doctype html>
<html lang="<?= LANG ?>">
<head>
    <title><? $APPLICATION->ShowTitle() ?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
    <? $APPLICATION->ShowHead();
    Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/libs/jquery/jquery.jscrollpane.css");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/libs/jquery/jquery-3.3.1.min.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH ."/libs/jquery/jquery.jscrollpane.min.js");
    Asset::getInstance()->addJs("/common/js/jquery.cycle.js");
    Asset::getInstance()->addJs(SITE_TEMPLATE_PATH ."/script.js");

    ?>
</head>
<body>
<? $APPLICATION->ShowPanel() ?>
<div class="msal-page <?= $isNewsDetail ? ' single-new-open' : ''; ?>">
    <header class="msal-header <?= $isMain ? 'msal-homepage-header' : 'msal-secondary-header'; ?>">
        <? if (!$isNewsDetail): ?>
            <div class="msal-header__logo">
                <div class="msal-logo">
                    <a href="/terminal/"><img class="msal-logo__img" src="<?= SITE_TEMPLATE_PATH . $logoPath ?>"/></a>
                </div>
            </div>
        <? endif; ?>
    </header>
