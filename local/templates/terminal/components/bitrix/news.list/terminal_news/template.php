<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
$nMaxName = Terminal\News::$nMaxName;
$nMaxPrev = Terminal\News::$nMaxPrev;

//printr(iconv_strlen('Всероссийский конкурс по конституционному правосудию среди студенческих команд «Хрустальная Фемида» проходил в Москве уже в восьмой раз под эгидой Ассоциации юристов России. Е'));
?>
    <section class="msal-main msal-secondary-main msal-news-section">
        <div class="msal-main__text">
            <h1 class="msal-main__text-title">Календарь новостей</h1>
        </div>
        <div class="msal-main__news-sections">
            <div class="news-sections">


                <? foreach ($arResult["ITEMS"] as $arItem): ?>
                    <?
                    $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                    $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));
                    //ограничиваем название превью
                    if (iconv_strlen($arItem["NAME"]) > $nMaxName + 1) {
                        $arItem["NAME"] = substr($arItem["NAME"], 0, $nMaxName) . '...';
                    }
                    //ограничиваем текст превью
                    $arItem["PREVIEW"] = strip_tags($arItem["~DETAIL_TEXT"]);
                    if (iconv_strlen($arItem["PREVIEW"]) > $nMaxPrev + 1) {
                        $arItem["PREVIEW"] = substr($arItem["PREVIEW"], 0, $nMaxPrev) . '...';
                    }
                    ?>

                        <div class="news-sections__item">
                            <a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">
                            <div class="news__item-title"><?= $arItem["NAME"] ?></div>
                            <div class="news__item-date"><?= $arItem["DISPLAY_ACTIVE_FROM"] ?></div>
                            <div class="news__item-text">
                                <div class="news__item-text-preview">
                                    <div class="news__item-image">
                                        <img src="<?= $arItem["PREVIEW_PICTURE"]["SRC"] ?>"
                                             width="187px"
                                             height="187px"
                                        />
                                    </div>
                                    <p>

                                        <?= $arItem["PREVIEW"] ?>
                                    </p>
                                </div>
                                <div class="clearfix"></div>
                            </div>
                            <? /*
      <div class="news__item-link"><a href="<?= $arItem["DETAIL_PAGE_URL"] ?>">читать весь текст
                                новости</a></div>
                        */ ?>
                            </a>
                        </div>

                <? endforeach; ?>


            </div>
            <div class="msal-main__navigation">
                <?=$arResult["NAV_STRING"]?>
            </div>
        </div>
    </section>