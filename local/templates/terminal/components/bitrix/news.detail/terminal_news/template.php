<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);
?>
<section class="msal-main msal-secondary-main msal-news-section onenews">
    <div class="msal-main__news-sections">
        <div class="news-sections">
            <div class="news-sections__item">
                <div class="news__item-title"><?= $arResult["NAME"] ?></div>
                <div class="news__item-date"><?= $arResult["DISPLAY_ACTIVE_FROM"] ?></div>
                <div class="news__item-text">
                    <div class="news__item-text-preview">
                        <div class="news__item-image">
                            <img src="<?= $arResult["PREVIEW_PICTURE"]["SRC"] ?>"
                                 width="225px"
                                 height="225px"
                            />
                        </div>
                    </div>
                    <div class="news__item-text-full scroll-pane">
                        <div class="news__item-image">
                            <img src="<?= $arResult["PREVIEW_PICTURE"]["SRC"] ?>" alt="">
                        </div>
                        <p><?= preg_replace('/justify/','',$arResult["~DETAIL_TEXT"]) ?></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>