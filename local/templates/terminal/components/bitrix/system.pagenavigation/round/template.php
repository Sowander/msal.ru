<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var array $arParams */
/** @var array $arResult */
/** @var CBitrixComponentTemplate $this */

$this->setFrameMode(true);

if (!$arResult["NavShowAlways"]) {
    if ($arResult["NavRecordCount"] == 0 || ($arResult["NavPageCount"] == 1 && $arResult["NavShowAll"] == false))
        return;
}

$strNavQueryString = ($arResult["NavQueryString"] != "" ? $arResult["NavQueryString"] . "&amp;" : "");
$strNavQueryStringFull = ($arResult["NavQueryString"] != "" ? "?" . $arResult["NavQueryString"] : "");

?>

<div class="msal-navigation">
    <ul class="msal-navigation__list">
        <? if ($arResult["bDescPageNumbering"] === true): ?>

            <? if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]): ?>
                <? if ($arResult["bSavePage"]): ?>
                    <li class="msal-navigation__list-item bx-pag-prev">
                        <div class="msal-navigation__nav-prev">
                            <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>">
                                <img src="/local/templates/terminal/images/arr-prev.png" alt="">
                            </a>
                        </div>
                    </li>
                    <li class="msal-navigation__list-item">
                        <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>"><span>1</span></a>
                    </li>
                <? else: ?>
                    <? if (($arResult["NavPageNomer"] + 1) == $arResult["NavPageCount"]): ?>
                        <li class="msal-navigation__list-item bx-pag-prev">
                            <div class="msal-navigation__nav-prev">
                                <a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">
                                    <img src="/local/templates/terminal/images/arr-prev.png" alt="">
                                </a>
                            </div>
                        </li>
                    <? else: ?>
                        <li class="msal-navigation__list-item bx-pag-prev">
                            <div class="msal-navigation__nav-prev">
                                <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>">
                                    <img src="/local/templates/terminal/images/arr-prev.png" alt="">
                                </a>
                            </div>
                        </li>
                    <? endif ?>
                    <li class="msal-navigation__list-item">
                        <a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><span>1</span></a></li>
                <? endif ?>
            <? else: ?>
                <li class="msal-navigation__list-item bx-pag-prev">
                    <div class="msal-navigation__nav-prev">
                        <a href="">
                            <img src="/local/templates/terminal/images/arr-prev.png" alt="">
                        </a>
                    </div>
                </li>
                <li class="msal-navigation__list-item active"><span>1</span></li>
            <? endif ?>

            <?
            $arResult["nStartPage"]--;
            while ($arResult["nStartPage"] >= $arResult["nEndPage"] + 1):
                ?>
                <? $NavRecordGroupPrint = $arResult["NavPageCount"] - $arResult["nStartPage"] + 1; ?>

                <? if ($arResult["nStartPage"] == $arResult["NavPageNomer"]): ?>
                <li class="msal-navigation__list-item active"><span><?= $NavRecordGroupPrint ?></span></li>
            <? else: ?>
                <li class="msal-navigation__list-item">
                    <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"><span><?= $NavRecordGroupPrint ?></span></a>
                </li>
            <? endif ?>

                <? $arResult["nStartPage"]-- ?>
            <? endwhile ?>

            <? if ($arResult["NavPageNomer"] > 1): ?>
                <? if ($arResult["NavPageCount"] > 1): ?>
                    <li class="msal-navigation__list-item">
                        <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1"><span><?= $arResult["NavPageCount"] ?></span></a>
                    </li>
                <? endif ?>
                <li class="msal-navigation__list-item bx-pag-next">
                    <div class="msal-navigation__nav-next">
                        <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>">
                            <img src="/local/templates/terminal/images/arr-next.png" alt="">
                        </a>
                    </div>
                </li>
            <? else: ?>
                <? if ($arResult["NavPageCount"] > 1): ?>
                    <li class="msal-navigation__list-item active"><span><?= $arResult["NavPageCount"] ?></span></li>
                <? endif ?>
                <li class="msal-navigation__list-item bx-pag-next">
                    <div class="msal-navigation__nav-next">
                        <a href="">
                            <img src="/local/templates/terminal/images/arr-next.png" alt="">
                        </a>
                    </div>
                </li>
            <? endif ?>

        <? else: ?>

            <? if ($arResult["NavPageNomer"] > 1): ?>
                <? if ($arResult["bSavePage"]): ?>
                    <li class="msal-navigation__list-item bx-pag-prev">
                        <div class="msal-navigation__nav-prev">
                            <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>">
                                <img src="/local/templates/terminal/images/arr-prev.png" alt="">
                            </a>
                        </div>
                    </li>
                    <li class="">
                        <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=1"><span>1</span></a>
                    </li>
                <? else: ?>
                    <? if ($arResult["NavPageNomer"] > 2): ?>
                        <li class="msal-navigation__list-item bx-pag-prev">
                            <div class="msal-navigation__nav-prev">
                                <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] - 1) ?>">
                                    <img src="/local/templates/terminal/images/arr-prev.png" alt="">
                                </a>
                            </div>
                        </li>
                    <? else: ?>
                        <li class="msal-navigation__list-item bx-pag-prev">
                            <div class="msal-navigation__nav-prev">
                                <a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>">
                                    <img src="/local/templates/terminal/images/arr-prev.png" alt="">
                                </a>
                            </div>
                        </li>
                    <? endif ?>
                    <li class="msal-navigation__list-item">
                        <a href="<?= $arResult["sUrlPath"] ?><?= $strNavQueryStringFull ?>"><span>1</span></a></li>
                <? endif ?>
            <? else: ?>
                <li class="msal-navigation__list-item bx-pag-prev">
                    <div class="msal-navigation__nav-prev">
                        <a href="">
                            <img src="/local/templates/terminal/images/arr-prev.png" alt="">
                        </a>
                    </div>
                </li>
                <li class="msal-navigation__list-item active"><span>1</span></li>
            <? endif ?>

            <?
            $arResult["nStartPage"]++;
            while ($arResult["nStartPage"] <= $arResult["nEndPage"] - 1):
                ?>
                <? if ($arResult["nStartPage"] == $arResult["NavPageNomer"]): ?>
                <li class="msal-navigation__list-item active"><span><?= $arResult["nStartPage"] ?></span></li>
            <? else: ?>
                <li class="msal-navigation__list-item">
                    <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["nStartPage"] ?>"><span><?= $arResult["nStartPage"] ?></span></a>
                </li>
            <? endif ?>
                <? $arResult["nStartPage"]++ ?>
            <? endwhile ?>

            <? if ($arResult["NavPageNomer"] < $arResult["NavPageCount"]): ?>
                <? if ($arResult["NavPageCount"] > 1): ?>
                    <li class="msal-navigation__list-item">
                        <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= $arResult["NavPageCount"] ?>"><span><?= $arResult["NavPageCount"] ?></span></a>
                    </li>
                <? endif ?>
                <li class="msal-navigation__list-item bx-pag-next">
                    <div class="msal-navigation__nav-next">
                        <a href="<?= $arResult["sUrlPath"] ?>?<?= $strNavQueryString ?>PAGEN_<?= $arResult["NavNum"] ?>=<?= ($arResult["NavPageNomer"] + 1) ?>">
                            <img src="/local/templates/terminal/images/arr-next.png" alt="">
                        </a>
                    </div>
                </li>
            <? else: ?>
                <? if ($arResult["NavPageCount"] > 1): ?>
                    <li class="msal-navigation__list-item active"><span><?= $arResult["NavPageCount"] ?></span></li>
                <? endif ?>
                <li class="msal-navigation__list-item bx-pag-next">
                    <div class="msal-navigation__nav-next">
                        <a href="">
                            <img src="/local/templates/terminal/images/arr-next.png" alt="">
                        </a>
                    </div>
                </li>
            <? endif ?>
        <? endif ?>
    </ul>
</div>


