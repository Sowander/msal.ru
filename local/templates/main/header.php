<?php header('X-UA-Compatible: IE=edge'); ?>
<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
IncludeTemplateLangFile(__FILE__);
?>
<!DOCTYPE html>
<!--[if lt IE 7]><html lang="ru" class="lt-ie9 lt-ie8 lt-ie7"><![endif]-->
<!--[if IE 7]><html lang="ru" class="lt-ie9 lt-ie8"><![endif]-->
<!--[if IE 8]><html lang="ru" class="lt-ie9"><![endif]-->
<!--[if gt IE 8]><!-->
<html lang="ru">
<!--<![endif]-->
	<head>
		<?$APPLICATION->ShowHead();?>
		<title><?$APPLICATION->ShowTitle();?></title>
		<link rel="shortcut icon" type="image/x-icon" href="/favicon.ico" />
        <meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width, initial-scale=1.0" />

        <!-- Skitter Styles -->
        <link href='//fonts.googleapis.com/css?family=PT+Sans:400,700,400italic,700italic&subset=latin,cyrillic-ext' rel='stylesheet' type='text/css'>

<meta name="yandex-verification" content="8c96d55255431f06" />

        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/libs/font-awesome-4.2.0/css/font-awesome.min.css");?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/libs/fancybox/jquery.fancybox.css");?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/libs/owl-carousel2/owl.carousel.css");?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/reset.css");?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/fonts.css");?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/animate.css");?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/main.css");?>
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/css/disable_1.css");?>



<!-- lk-cbox -->
		<link rel="stylesheet" href="" />
        <?$APPLICATION->SetAdditionalCSS(SITE_TEMPLATE_PATH."/libs/colorbox/colorbox.css");?>
	<!-- script src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script -->
		<script src="/local/templates/main/libs/colorbox/jquery.colorbox.js"></script>
		<script>
			$(document).ready(function(){
				//Examples of how to assign the Colorbox event to elements
				$(".iframe").colorbox({iframe:true, width:"50%", height:"60%"});
				$(".inlinecol").colorbox({inline:true, width:"50%"});
				$(".callbacks").colorbox({
					onOpen:function(){ alert('onOpen: colorbox is about to open'); },
					onLoad:function(){ alert('onLoad: colorbox has started to load the targeted content'); },
					onComplete:function(){ alert('onComplete: colorbox has displayed the loaded content'); },
					onCleanup:function(){ alert('onCleanup: colorbox has begun the close process'); },
					onClosed:function(){ alert('onClosed: colorbox has completely closed'); }
				});
				//Example of preserving a JavaScript event for inline calls.
				$("#click").click(function(){ 
					$('#click').css({"background-color":"#f00", "color":"#fff", "cursor":"inherit"}).text("Open this window again and this message will still be here.");
					return false;
				});
			});
		</script>


<!-- lk-cbox -->

<link href="/local/templates/inner/css/mgua.css" rel="stylesheet" type="text/css">
<!-- СНЕГ -->
<link href="/local/templates/main/css/snow_msal.css" rel="stylesheet" type="text/css">

	</head>
    <body class="index_page">
		<div id="panel">
			<?$APPLICATION->ShowPanel();?>
		</div>
        <header>
        <!-- Second menu -->
        <?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"second_menu", 
	array(
		"ROOT_MENU_TYPE" => "second_".LANGUAGE_ID,
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "top",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "Y",
		"MENU_CACHE_TIME" => "36000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"COMPONENT_TEMPLATE" => "second_menu"
	),
	false
);?>
            <section class="top_head">
                <ul class="wrapper_container">
                    <li class="logo">
                        <a href="<?if(isset($arSiteLanguageParams) && strlen($arSiteLanguageParams["UF_LINK"])>0):?><?=$arSiteLanguageParams["UF_LINK"]?><?else:?>/<?endif;?>">
                            <img src="<?=SITE_TEMPLATE_PATH?>/img/logo.png" alt="МГЮА"/>
                        </a>
                        <i class="corner"></i>
                    </li>
                    <li class="logo_text">
                        <p>
                            <!-- University name -->
                            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                                    "AREA_FILE_SHOW" => "file",
                                    "PATH" => "/includes/".LANGUAGE_ID."/university_name.php",
                                    "EDIT_TEMPLATE" => ""
                                ),
                                false
                            );?>
                        </p>
                        <span class="little_text">Non scholae sed vitae discimus</span>
                    </li>


                    <li class="contacts">
                        <div>

                            <!-- Social networks -->
                            <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"social_networks", 
	array(
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "4",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "PREVIEW_TEXT",
			1 => "PREVIEW_PICTURE",
			2 => "DETAIL_PICTURE",
			3 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "UF_URL_LINK",
			1 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_LAST_MODIFIED" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "360000000000",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"PAGER_BASE_LINK" => "",
		"PAGER_PARAMS_NAME" => "arrPager",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "social_networks",
		"SHOW_H1" => "N"
	),
	false
);?>

                            <!-- Hot line header -->
                            <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                                    "AREA_FILE_SHOW" => "file",
                                    "PATH" => "/includes/".LANGUAGE_ID."/hot_line_header.php",
                                    "EDIT_TEMPLATE" => ""
                                ),
                                false
                            );?>
                        </div>
                    </li>
                    <li class="important_links">
                        <ul>
                            <li>
                                <?$APPLICATION->IncludeComponent(
                                    "bit:custom.feedback",
                                    "rector",
                                    array(
                                        "USE_CAPTCHA" => "Y",
                                        "OK_TEXT" => "Спасибо, ваше сообщение принято.",
                                        "EMAIL_TO" => "pvryabov@msal.ru",
                                        "REQUIRED_FIELDS" => array(
                                            0 => "UF_NAME",
                                            1 => "UF_EMAIL",
                                            2 => "UF_QUESTION",
                                        ),
                                        "EVENT_MESSAGE_ID" => array(
                                            0 => "5",
                                        ),
                                        "COMPONENT_TEMPLATE" => ".default",
                                        "IBLOCK_TYPE" => "tools",
                                        "IBLOCK_ID" => "13",
                                        "IBLOCK_ELEM" => "138",
                                        "NAME_TO_RU" => "Блажееву Виктору Владимировичу",
                                        "NAME_TO_EN" => "Блажееву Виктору Владимировичу"
                                    ),
                                    false
                                );?>
                            </li>
                            <li>
                                <a href="#" id="disable_vers">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/img/top_glasses.gif" alt=""/>
                                    <span><?=GetMessage("VERSION_VISUALLY_IMPAIRED")?></span>
                                    <span class="full_vers"><?=GetMessage("VERSION_VISUALLY_IMPAIRED_FULL")?></span>
                                </a>
                            </li>
                            <li>
                                <a href="<?=$GLOBALS["arSiteLanguageParams"]["UF_LINK"]?>handbook/?THEMES_LIST=Y">
                                    <img src="<?=SITE_TEMPLATE_PATH?>/img/top_book.gif" alt="handbook"/>
                                    <span><?=GetMessage("HANDBOOK_INFO_NAME")?></span>
                                </a>
                            </li>
							
						  
                            <?$arrImportantInformationFilter = array_merge($arFilterLanguage, array("!PROPERTY_UF_IMPORTANT"=>false))?>
                            <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"important_information", 
	array(
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "9",
		"NEWS_COUNT" => "10",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "arrImportantInformationFilter",
		"FIELD_CODE" => array(
			0 => "ID",
			1 => "CODE",
			2 => "SORT",
			3 => "PREVIEW_TEXT",
			4 => "DATE_ACTIVE_FROM",
			5 => "ACTIVE_FROM",
			6 => "DATE_ACTIVE_TO",
			7 => "ACTIVE_TO",
			8 => "DATE_CREATE",
			9 => "CREATED_USER_NAME",
			10 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "UF_IMPORTANT",
			1 => "UF_LANGUAGE",
			2 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_LAST_MODIFIED" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "N",
		"CACHE_TYPE" => "Y",
		"CACHE_TIME" => "3600",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"PAGER_BASE_LINK" => "",
		"PAGER_PARAMS_NAME" => "arrPager",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "important_information"
	),
	false
);?>

                            <li class="disable_panel">
                                <section>

                                    <a href="#" class="default_letter"><span>[ ]</span></a>
                                    <a href="#" class="bigger_letter"><span>[ ]</span></a>
                                    <a href="#" class="biggest_letter"><span>[  ]</span></a>
                                     
                                    <a href="#" class="default_a"><span>A</span></a>
                                    <a href="#" class="bigger_a"><span>A</span></a>
                                    <a href="#" class="biggest_a"><span>A</span></a>
                                     
                                    <a href="#" class="white_square">
                                        <span> </span>
                                    </a>
                                    <a href="#" class="black_square">
                                        <span> </span>
                                    </a>
                                    <a href="#" class="blue_square">
                                        <span> </span>
                                    </a>
                                </section>
                            </li>
                        </ul>
<!-- ПРИЕМКА -->
		<!-- p class="trans_button low" style="/*margin-left:4em;margin-top: -1.8em;*/text-align: center;margin-left: 1em;/* margin-top: 0.3em; */"><a style="font-size: 14px;color:#980000;font-weight: 700;" href="https://msal.ru/content/online/">ОНЛАЙН-РЕГИСТРАЦИЯ</a>
		</p -->
		<!-- p class="trans_button low" style="/*margin-left:4em;margin-top: -1.8em;*/text-align: center;margin: .6em .1em -.6em 12em;/* margin-top: 0.3em; */"><a style="font-size: 14px;color:#980000;font-weight: 700;" target="_blank" href="http://msal.ru/content/abiturientam/monitoring-elektronnoy-ocheredi/">Электронная очередь</a>
		</p -->
<!-- ПРИЕМКА -->		
<!-- MSAL-TV -->						
						<p class="trans_button low">
			<a target="_blank" href="/msaltv/">MSAL-TV</a>
		</p>
<!-- MSAL-TV -->	

<p class="trans_button low" style="
    margin-left: 6em;
">
			<a target="_blank" href="https://vk.com/legal_fm">LEGAL-FM</a>
		</p>							
                    </li>
                </ul>
				
											
<!-- div class="radtv" style="    float: right;    position: relative;    margin: -1.4em 20em 0 0; color:gray; font-size: 25px;                          "><a href="" style="color:gray" alt="МГЮА-FM" title="МГЮА-FM">&#127911;</a> | <a style="color:gray" href=""  alt="МГЮА-TV" title="МГЮА-TV">📺</a></div -->


            </section>

            <!-- Main menu -->
            <?$APPLICATION->IncludeComponent(
	"bitrix:menu", 
	"main_menu", 
	array(
		"ROOT_MENU_TYPE" => "main_".LANGUAGE_ID,
		"MAX_LEVEL" => "1",
		"CHILD_MENU_TYPE" => "top",
		"USE_EXT" => "Y",
		"DELAY" => "N",
		"ALLOW_MULTI_SELECT" => "Y",
		"MENU_CACHE_TYPE" => "Y",
		"MENU_CACHE_TIME" => "36000000000",
		"MENU_CACHE_USE_GROUPS" => "Y",
		"MENU_CACHE_GET_VARS" => array(
		),
		"COMPONENT_TEMPLATE" => "main_menu"
	),
	false
);?>

            <!-- Banners -->
            <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"main_page_banners", 
	array(
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "N",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "2",
		"NEWS_COUNT" => "5",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "arFilterLanguage",
		"FIELD_CODE" => array(
			0 => "PREVIEW_TEXT",
			1 => "PREVIEW_PICTURE",
			2 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "UF_TITLES",
			1 => "UF_LINK",
			2 => "UF_LANGUAGE",
			3 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "N",
		"SET_META_DESCRIPTION" => "N",
		"SET_LAST_MODIFIED" => "N",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "36000000000000",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"PAGER_BASE_LINK" => "",
		"PAGER_PARAMS_NAME" => "arrPager",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "N",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "main_page_banners"
	),
	false
);?>


        </header>