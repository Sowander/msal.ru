<?
if(!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true)
	die();
IncludeTemplateLangFile(__FILE__);
?>

<footer>
    <section class="top">
        <div class="wrapper_container">
            <ul>
                <!-- Contact info -->
                <li class="col_1">
                    <h4><?=GetMessage("CONTACT_INFO")?></h4>

                    <div class="foot_info">

                        <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => "/includes/".LANGUAGE_ID."/contact_info.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            false
                        );?>
                        <?$APPLICATION->IncludeComponent(
							"bit:custom.feedback",
							"usially_feedback_with_map",
							array(
								"USE_CAPTCHA" => "Y",
								"OK_TEXT" => "Спасибо, ваше сообщение принято.",
								"EMAIL_TO" => "pvryabov@msal.ru",
								"REQUIRED_FIELDS" => array(
									0 => "UF_QUESTION",
								),
								"EVENT_MESSAGE_ID" => array(
									0 => "5",
								),
								"COMPONENT_TEMPLATE" => "usially_feedback_with_map",
								"IBLOCK_TYPE" => "tools",
								"IBLOCK_ID" => "13",
								"IBLOCK_ELEM" => "201",
								"NAME_TO_RU" => "Администратору сайта МГЮА",
								"NAME_TO_EN" => " "
							),
							false
						);?>
	                    <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
		                    "AREA_FILE_SHOW" => "file",
		                    "PATH" => "/includes/".LANGUAGE_ID."/phonebook.php",
		                    "EDIT_TEMPLATE" => ""
	                    ),
		                    false
	                    );?>
                    </div>
                </li>
                <!-- Press service -->
                <li class="col_2">
                    <h4><?=GetMessage("PRESS_SERVICE")?></h4>

                    <div class="foot_info">
                        <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => "/includes/".LANGUAGE_ID."/press_service.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            false
                        );?>
                    </div>
                    <!-- Selection committee -->
                    <h4><?=GetMessage("SELECTION_COMMITTEE")?></h4>

                    <div class="foot_info">
                        <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => "/includes/".LANGUAGE_ID."/selection_committee.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            false
                        );?>
                    </div>
                </li>
                <li class="col_3">
                    <!-- Hot line footer -->
                    <h4><?=GetMessage("HOT_LINE_FOOTER")?></h4>

                    <div class="foot_info">
                        <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                                "AREA_FILE_SHOW" => "file",
                                "PATH" => "/includes/".LANGUAGE_ID."/hot_line_footer.php",
                                "EDIT_TEMPLATE" => ""
                            ),
                            false
                        );?>
                    </div>

                    <!-- Social networks -->
                    <?$APPLICATION->IncludeComponent(
	"bitrix:news.list", 
	"social_networks", 
	array(
		"DISPLAY_DATE" => "N",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "N",
		"DISPLAY_PREVIEW_TEXT" => "N",
		"AJAX_MODE" => "N",
		"IBLOCK_TYPE" => "content",
		"IBLOCK_ID" => "4",
		"NEWS_COUNT" => "20",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_ORDER1" => "DESC",
		"SORT_BY2" => "SORT",
		"SORT_ORDER2" => "ASC",
		"FILTER_NAME" => "",
		"FIELD_CODE" => array(
			0 => "PREVIEW_TEXT",
			1 => "PREVIEW_PICTURE",
			2 => "DETAIL_PICTURE",
			3 => "",
		),
		"PROPERTY_CODE" => array(
			0 => "UF_URL_LINK",
			1 => "",
		),
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"SET_TITLE" => "N",
		"SET_BROWSER_TITLE" => "N",
		"SET_META_KEYWORDS" => "Y",
		"SET_META_DESCRIPTION" => "Y",
		"SET_LAST_MODIFIED" => "Y",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "N",
		"ADD_SECTIONS_CHAIN" => "N",
		"HIDE_LINK_WHEN_NO_DETAIL" => "Y",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"INCLUDE_SUBSECTIONS" => "N",
		"CACHE_TYPE" => "A",
		"CACHE_TIME" => "360000000000",
		"CACHE_FILTER" => "Y",
		"CACHE_GROUPS" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"DISPLAY_BOTTOM_PAGER" => "N",
		"PAGER_TITLE" => "Новости",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "",
		"PAGER_DESC_NUMBERING" => "Y",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"SET_STATUS_404" => "N",
		"SHOW_404" => "N",
		"MESSAGE_404" => "",
		"PAGER_BASE_LINK" => "",
		"PAGER_PARAMS_NAME" => "arrPager",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"COMPONENT_TEMPLATE" => "social_networks",
		"SHOW_H1" => "Y"
	),
	false
);?>



                </li>
                <li class="col_4">

                    <!-- Our partners -->
                    <?$APPLICATION->IncludeComponent(
                        "bitrix:news.list",
                        "our_partners",
                        array(
                            "DISPLAY_DATE" => "N",
                            "DISPLAY_NAME" => "Y",
                            "DISPLAY_PICTURE" => "N",
                            "DISPLAY_PREVIEW_TEXT" => "N",
                            "AJAX_MODE" => "Y",
                            "IBLOCK_TYPE" => "content",
                            "IBLOCK_ID" => "1",
                            "NEWS_COUNT" => "20",
                            "SORT_BY1" => "ACTIVE_FROM",
                            "SORT_ORDER1" => "DESC",
                            "SORT_BY2" => "SORT",
                            "SORT_ORDER2" => "ASC",
                            "FILTER_NAME" => "arFilterLanguage",
                            "FIELD_CODE" => array(
                                0 => "",
                                1 => "",
                            ),
                            "PROPERTY_CODE" => array(
                                0 => "UF_URL_LINK",
                                1 => "UF_LANGUAGE",
                                2 => "",
                                3 => "",
                            ),
                            "CHECK_DATES" => "Y",
                            "DETAIL_URL" => "",
                            "PREVIEW_TRUNCATE_LEN" => "",
                            "ACTIVE_DATE_FORMAT" => "d.m.Y",
                            "SET_TITLE" => "N",
                            "SET_BROWSER_TITLE" => "N",
                            "SET_META_KEYWORDS" => "Y",
                            "SET_META_DESCRIPTION" => "Y",
                            "SET_LAST_MODIFIED" => "Y",
                            "INCLUDE_IBLOCK_INTO_CHAIN" => "N",
                            "ADD_SECTIONS_CHAIN" => "N",
                            "HIDE_LINK_WHEN_NO_DETAIL" => "Y",
                            "PARENT_SECTION" => "",
                            "PARENT_SECTION_CODE" => "",
                            "INCLUDE_SUBSECTIONS" => "N",
                            "CACHE_TYPE" => "A",
                            "CACHE_TIME" => "3600000000",
                            "CACHE_FILTER" => "Y",
                            "CACHE_GROUPS" => "Y",
                            "DISPLAY_TOP_PAGER" => "N",
                            "DISPLAY_BOTTOM_PAGER" => "N",
                            "PAGER_TITLE" => "Новости",
                            "PAGER_SHOW_ALWAYS" => "N",
                            "PAGER_TEMPLATE" => "",
                            "PAGER_DESC_NUMBERING" => "Y",
                            "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                            "PAGER_SHOW_ALL" => "N",
                            "PAGER_BASE_LINK_ENABLE" => "N",
                            "SET_STATUS_404" => "N",
                            "SHOW_404" => "N",
                            "MESSAGE_404" => "",
                            "PAGER_BASE_LINK" => "",
                            "PAGER_PARAMS_NAME" => "arrPager",
                            "AJAX_OPTION_JUMP" => "N",
                            "AJAX_OPTION_STYLE" => "Y",
                            "AJAX_OPTION_HISTORY" => "N",
                            "AJAX_OPTION_ADDITIONAL" => "",
                            "COMPONENT_TEMPLATE" => "our_partners"
                        ),
                        false
                    );?>


                </li>
            </ul>
        </div>
    </section>

    <section class="bottom">
        <div class="wrapper_container">
            <!-- Copyright -->
            <div class="copy left">
                <?$APPLICATION->IncludeComponent("bitrix:main.include", ".default", array(
                        "AREA_FILE_SHOW" => "file",
                        "PATH" => "/includes/".LANGUAGE_ID."/copyright.php",
                        "EDIT_TEMPLATE" => ""
                    ),
                    false
                );?>
            </div>
            <?$APPLICATION->IncludeComponent(
                "bitrix:search.form",
                "mini_search_form",
                array(
                    "USE_SUGGEST" => "N",
                    "PAGE" => "#SITE_DIR#search/index.php",
                    "COMPONENT_TEMPLATE" => "mini_search_form"
                ),
                false
            );?>
        </div>
    </section>
</footer>

<!-- Подключаемые файлы для IE, скрипты, плагины и тд. -->
<!--[if lt IE 9]>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/libs/html5shiv/es5-shim.min.js");?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/libs/html5shiv/html5shiv.min.js");?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/libs/html5shiv/html5shiv-printshiv.min.js");?>
<![endif]-->
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/libs/jquery/jquery-1.11.1.min.js");?>
<!-- Skitter JS -->
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/libs/jquery-mousewheel/jquery.mousewheel.min.js");?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/libs/fancybox/jquery.fancybox.pack.js");?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/libs/scrollto/jquery.scrollTo.min.js");?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/libs/owl-carousel2/owl.carousel.js");?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/common.js");?>
<?$APPLICATION->AddHeadScript(SITE_TEMPLATE_PATH."/js/index_page.js");?>


<!-- /Подключаемые файлы для IE, скрипты, плагины и тд. -->

<script type="text/javascript">
       (function(d, t, p) {
           var j = d.createElement(t); j.async = true; j.type = "text/javascript";
           j.src = ("https:" == p ? "https:" : "http:") + "//stat.sputnik.ru/cnt.js";
           var s = d.getElementsByTagName(t)[0]; s.parentNode.insertBefore(j, s);
       })(document, "script", document.location.protocol);
    </script>
	
</body>
</html>