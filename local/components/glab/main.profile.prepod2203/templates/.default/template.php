<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();
use Bitrix\Main\Page\Asset;

Asset::getInstance()->addString("<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/bootstrap.min.css");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/bootstrap.min.js");
$ruWeek = array('Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье');
$arJSlection = [];
global $APPLICATION;
$request = \Bitrix\Main\Context::getCurrent()->getRequest();


function ru_date($format, $date = false) {
    if ($date === false) {
        $date = time();
    }
    if ($format === '') {
        $format = '%e&nbsp;%bg&nbsp;%Y&nbsp;г.';
    }
    $months = explode("|", '|января|февраля|марта|апреля|мая|июня|июля|августа|сентября|октября|ноября|декабря');
    $format = preg_replace("~\%bg~", $months[date('n', $date)], $format);
    $res = strftime($format, $date);
    return $res;
}
?>

<div>
    <br>
    <div class="prepod-info">
        <div class="col-md-6" style="padding:0;">
            <h3><?= $arResult['PREPOD_INFO']['FIO']; ?></h3>
        </div>
        <div class="col-md-6">
            <? if ($arResult["STUDENT"]["PERSONAL_PHOTO"]): ?>
                <img class="student-img" src="<?= $arResult["PREPOD_INFO"]["PERSONAL_PHOTO"] ?>" alt="">
            <? endif; ?>
        </div>
    </div>
    <br> <br>
    <!-- Навигация -->
    <ul class="nav nav-tabs" role="tablist">
        <li class="">
            <a class="thome" href="#home" aria-controls="home" role="tab" data-toggle="tab">Журнал успеваемости старая
                версия</a> <? //todo удалить сж ?>
        </li>
        <li class="prepod-shedule active">
            <a class="" href="#shedule" aria-controls="shedule" role="tab" data-toggle="tab">Расписание</a>
        </li>
        <li class="">
            <a class="" href="#dorab" aria-controls="dorab" role="tab" data-toggle="tab">Доработка</a>
        </li>
        <li>
            <a onclick="$('.form-loader').fadeIn(100);"
               href="<?= 'http://' . $_SERVER['HTTP_HOST'] . substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], '?')) . '?logout=yes' ?>">Выход</a>
        </li>
    </ul>
    <!-- Содержимое вкладок -->
    <div class="tab-content">
        <!--        старый журнал--> <? //todo удалить сж ?>
        <div role="tabpanel" class="vedomosti tab-pane" id="home">
            <?

            if ($arResult["Ведомости"]):?>
                <div class="">
                    <table class="vedomosti__table">
                        <thead>

                        <tr>
                            <th>Дисциплина</th>
                            <th>Институт</th>
                            <th>Группа</th>
                            <th>Вид нагрузки</th>
                        </tr>
                        </thead>
                        <tbody>
                        <? foreach ($arResult["Ведомости"] as $item): ?>
                            <tr>

                                <td>
                                    <a data-guid="<?= $item['НомерВедомости'] ?>" data-toggle="modal" data-target="#discModal"
                                       href="#discModal">
                                        <?= $item['Дисциплина'] ?>
                                    </a>
                                </td>
                                <td><?= $item['Институт'] ?></td>
                                <td><?= $item['ИмяГруппы'] ?></td>
                                <td><?= $item['ВидНагрузки'] ?></td>
                            </tr>
                        <? endforeach; ?>
                        </tbody>
                    </table>
                    <nav style="text-align: center">
                        <ul class="pagination">
                            <li class="page-item <?= intval($arResult['CUR_PAGIN'] - 1) <= 0 ? ' disabled' : ''; ?>">
                                <a class="page-link"
                                   href="<?= intval($arResult['CUR_PAGIN'] - 1) <= 0 ? 'javascript:void(0);' : $arResult['CUR_PAGE'] . '?page=' . intval($arResult['CUR_PAGIN'] - 1) ?>"
                                   aria-label="Previous"> <span aria-hidden="true">&laquo;</span> <span class="sr-only">Предыдущая</span>
                                </a>
                            </li>
                            <? for ($i = 1; $i <= $arResult['PAGIN']; $i++):
                                $activep = false;
                                if ($arResult['CUR_PAGIN'] == $i) {
                                    $activep = true;
                                }
                                ?>
                                <li class="page-item <?= $activep ? ' active' : ''; ?>">
                                    <a class="page-link" href="<?= $arResult['CUR_PAGE'] . '?page=' . $i ?>"><?= $i ?></a>
                                </li>
                            <? endfor; ?>
                            <li class="page-item <?= intval($arResult['CUR_PAGIN'] + 1) > $arResult['PAGIN'] ? ' disabled' : ''; ?>">
                                <a class="page-link"
                                   href="<?= intval($arResult['CUR_PAGIN'] + 1) > $arResult['PAGIN'] ? 'javascript:void(0);' : $arResult['CUR_PAGE'] . '?page=' . intval($arResult['CUR_PAGIN'] + 1); ?>"
                                   aria-label="Next"> <span aria-hidden="true">&raquo;</span> <span class="sr-only">Следующая</span>
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>
            <? else: ?>
                <p class="alert-danger">Ведомости отсутствуют</p>
            <? endif; ?>
        </div>
        <div role="tabpanel" class="tab-pane active" id="shedule">
            <div class="prep-scheule__btn">
                <button type="text" class="btn btn-default btnprev"
                        value="<?= $arResult['SCHEDULE']['SYS_DATE_PREV'] ?>">
                    Пред. Неделя
                </button>
                <input type="text" class="datems"
                       value="<?= $arResult['SCHEDULE']['DATE_START'] . " - " . $arResult['SCHEDULE']['DATE_END']; ?>"
                       name="date" onclick="BX.calendar({node: this, field: this, bTime: false});">
                <button type="text" class="btn btn-default btnnext"
                        value="<?= $arResult['SCHEDULE']['SYS_DATE_NEXT'] ?>">
                    След. Неделя
                </button>
            </div>
            <div class="">
                <table class="prepod_schedule__table">
                    <thead>
                    <tr>
                        <th>№</th>
                        <th>Время</th>
                        <? foreach ($ruWeek as $plus => $day): ?>
                            <th><?= $day . '<br>' . ru_date('%e %bg', strtotime('+' . $plus . ' day', strtotime($arResult['SCHEDULE']['DATE_START']))) ?></th>
                        <? endforeach; ?>
                    </tr>
                    </thead>
                    <tbody>
                    <? $rows = 8; // количество строк, tr
                    $cols = 9;
                    for ($tr = 1;
                    $tr <= $rows;
                    $tr++): ?>
                    <tr>
                        <? for ($td = 1;
                        $td <= $cols;
                        $td++):
                        if ($td != 1 && $td != 2) {
                            echo '<td>';
                            $arLection = [];
                            if (!empty($arResult['ITEMS'][$tr][$td - 2][0]['subject'])) {
                                $arLection = $arResult['ITEMS'][$tr][$td - 2][0];
                                $arJSlection[$arLection['id']] = $arLection;
                                echo '<a class="js-lectionhref"  data-lectionid="' . trim($arLection['id']) . '" href="' . $APPLICATION->GetCurPageParam("opwin=" . trim($arLection['id']), array("opwin", "svod")) . '">';
                                echo '<span class="js-date" style="display: none">' . $arLection['date'] . '</span>';
                                echo '<p class="schedule__name js-name"><b>' . $arLection['subject'] . '</b></p>';
                                echo '<span class="js-type"><i>' . $arLection['view'] . '</i></span><br>';
                                echo '<span class="js-gr">' . $arLection['group'] . '</span><br>';
                                echo $arLection['korpus'] . '<br>';
                                echo '<span style="font-weight: bold;">' . $arLection['place'] . '</span><br>';
                                echo '</a>';
                            }
                            echo '</td>';

                        } elseif ($td === 2) {
                            echo '<td>';
                            echo $arResult['RINGS'][$tr];
                            echo '</td>';
                        } elseif ($td === 1) {
                            echo '<td>';
                            echo $arResult['RINGS'][$tr] ? $tr : '';
                            echo '</td>';
                        }
                        if (!empty($arLection)):
                        ?>
                        <!--нововая модал ведомость-->

                        <td class="modal" id="<?= $arLection['id'] ?>" tabindex="-1" role="dialog"
                            aria-labelledby="<?= $arLection['id'] ?>" aria-hidden="true">
                            <div class="modal-dialog">
                                <form action="" class="jsFormUpdate">
                                    <div class="modal-content res<?= $arLection['id'] ?>">

                                        <div class="modal-body">


                                            <? if (is_array($arLection['JOURNAL']) && !empty($arLection['JOURNAL']) && !($arLection['ERROR'])): ?>
                                                <input type="submit" value="Сохранить"
                                                       class="js-button js-save btn btn-success">
                                            <? endif; ?>

                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span class="close-btn" aria-hidden="true">ЗАКРЫТЬ</span>
                                            </button>
                                            <div>
                                                <h3 class="js-type-modal"><?= $arLection['view'] ?></h3>
                                            </div>
                                            <? if (!$arLection['ERROR']): ?>
                                                <div class="tema">
                                                    <select
                                                            class="js-tema"
                                                            required
                                                            name="tema"
                                                            title="Тема"
                                                        <?
                                                        $kind = trim(strtolower($arLection['view']));
                                                        if ($kind === 'семинар' or $kind === 'лабораторная работа') {
                                                            echo ' disabled'; //запрет редактировать Тему занятий в семенарах
                                                        }
                                                        ?>
                                                    >
                                                        <?
                                                        $tema1c = '';
                                                        foreach ($arLection['JOURNAL'] as $gname => $ar) {
                                                            $tema1c = trim($ar[0]['ItemTheme']['Name']);
                                                        }
                                                        ?>
                                                        <option <?= strlen($tema1c) > 2 ? '' : 'selected'; ?> disabled value>
                                                            Выберите тему
                                                        </option>
                                                        <? foreach ($arLection['RPD']['Modules'] as $arMod): ?>
                                                            <optgroup label="<?= trim($arMod['Name']) ?>">
                                                                <? foreach ($arMod['ItemsTheme'] as $arTema):
                                                                    $temavalue = trim($arMod['GUID']) . 'DELIM' . trim($arTema['Name']) . 'DELIM' . trim($arMod['Name']) . 'DELIM' . trim($arTema['MaxBall']);
                                                                    $selected = '';
                                                                    if ($tema1c === trim($arTema['Name'])) {
                                                                        $selected = 'selected';
                                                                    }
                                                                    ?>
                                                                    <option <?= $selected ?> value="<?= $temavalue ?>"><?= $arTema['Name'] ?></option>
                                                                <? endforeach; ?>
                                                            </optgroup>
                                                        <? endforeach; ?>
                                                    </select>
                                                </div>
                                            <? endif; ?>

                                            <div class="alert alert-success" style="display:none;">
                                            </div>
                                            <div class="alert alert-danger" <?= $arLection['ERROR'] ? '' : ' style="display:none;"'; ?> >
                                                <?
                                                if ($arLection['ERROR']) {
                                                    echo $arLection['ERROR'];
                                                }
                                                ?>
                                            </div>

                                            <ul class="nav nav-tabs" role="tablist">
                                                <?
                                                $i = 1;
                                                if (is_array($arLection['JOURNAL']) && !empty($arLection['JOURNAL']))
                                                    foreach ($arLection['JOURNAL'] as $gname => $ar):?>
                                                        <li class="<?= $i === 1 ? 'active' : ''; ?>">
                                                            <a class="chgroup"
                                                               href="#<?= $ar[0]['Group']['GUID'] ?>"
                                                               aria-controls="<?= $ar[0]['Group']['GUID'] ?>"
                                                               role="tab"
                                                               data-toggle="tab"><?= $gname ?></a>
                                                        </li>
                                                        <?
                                                        $i++;
                                                    endforeach;
                                                unset($i);

                                                $kind = trim(strtolower($arLection['view']));
                                                if (($kind === 'семинар' or $kind === 'лабораторная работа') && !$arLection['ERROR']):?>
                                                    <li>
                                                        <a class="js-svod" href="<?= $APPLICATION->GetCurPageParam("opwin=" . trim($arLection['id']) . '&svod=Y', array("opwin", "svod")) ?>">Сводная
                                                            таблица</a>
                                                    </li>
                                                <? endif; ?>
                                            </ul>
                                            <!-- Содержимое вкладок окна -->
                                            <div class="tab-content">
                                                <?
                                                $i = 1;
                                                foreach ($arLection['JOURNAL'] as $gname => $ar):?>
                                                    <div role="tabpanel"
                                                         class="tab-pane <?= $i === 1 ? 'active' : ''; ?>"
                                                         id="<?= $ar[0]['Group']['GUID'] ?>">
                                                        <? $i++ ?>
                                                        <div class="row">

                                                            <? if (strlen(trim($ar['ItemTheme']['Name'])) > 2): ?>
                                                                <span class="js-tema-name" style="display: none"><?= trim($ar['ItemTheme']['Name']) ?></span>
                                                            <? endif; ?>

                                                            <ul class="col-md-6">
                                                                <li>Дата занятия: <span><?= $arLection['date'] ?></span>
                                                                </li>
                                                                <li>Учебный год:
                                                                    <span><?= $arLection['uchYear'] ?></span>
                                                                </li>
                                                                <li>Семестр: <span><?= $arLection['semestr'] ?></span>
                                                                </li>
                                                                <li>Курс: <span><?= $arLection['curse'] ?></span></li>
                                                            </ul>
                                                            <ul class="col-md-6">
                                                                <li>Институт: <span><?= $arLection['inst'] ?></span>
                                                                </li>
                                                                <li>Форма обучения:
                                                                    <span><?= $arLection['form'] ?></span>
                                                                </li>
                                                                <li>Модуль:
                                                                    <span class="js-show-module"><?= $ar[0]['ItemModule']['Name'] ?></span>
                                                                </li>
                                                            </ul>
                                                        </div>

                                                        <ul class="nav nav-tabs" role="tablist">
                                                            <li class="active">
                                                                <a href="#<?= $arLection['subjectGuid'] ?>"
                                                                   aria-controls="<?= $arLection['subjectGuid'] ?>"
                                                                   role="tab"
                                                                   data-toggle="tab"><?= $arLection['subject'] ?></a>
                                                            </li>
                                                        </ul>
                                                        <div class="tab-content">
                                                            <div role="tabpanel" class="tab-pane active"
                                                                 id="<?= $arLection['subjectGuid'] ?>">

                                                                <div class="liketable">

                                                                    <div class="likerow">
                                                                        <div class="likecell">Студент</div>
                                                                        <div class="likecell">Балл/Отсутствие</div>
                                                                        <div class="likecell">Итого/max</div>
                                                                    </div><? foreach ($ar as $arStudent):
                                                                        $backcol = '';
                                                                        if (0) {
                                                                            $backcol = 'style="background-color: rgba(68, 157, 68, 0.5);"'; //зелен
                                                                        } elseif ($arStudent['Status'] == 2) {
                                                                            $backcol = 'style="background-color: rgba(178, 0, 0, 0.5);"';//красн
                                                                        } elseif ($arStudent['Status'] == 3) {
                                                                            $backcol = 'style="background-color: rgb(100%, 86%, 20%, 0.5);"';//желт
                                                                        } ?>
                                                                        <div class="likerow" <?= $backcol ?>
                                                                             data-stuid="<?= trim($arStudent['Student']['GUID']) ?>">
                                                                            <div class="likecell js-stname"><?= trim($arStudent['Student']['Name']) ?></div>
                                                                            <div class="likecell">
                                                                                <input class="js-ball"
                                                                                       data-groupGuid="<?= trim($arStudent['Group']['GUID']) ?>"
                                                                                       data-groupName="<?= trim($arStudent['Group']['Name']) ?>"
                                                                                       data-idLection="<?= trim($arLection['id']) ?>"
                                                                                    <?= intval($arStudent['ItemTheme']['MaxBall']) > 0 ? 'max=' . intval($arStudent['ItemTheme']['MaxBall']) : 'max=' . intval($arStudent['ItemModule']['MaxBall']); ?>
                                                                                    <? if ($arStudent['Missed']): ?>
                                                                                        type="text" value="Н"
                                                                                    <? else: ?>
                                                                                        type="number" value="<?= $arStudent['Ball'] ?>" min="0"
                                                                                    <? endif; ?>>
                                                                                <label for="">
                                                                                    <input class="js-out" type="checkbox" <?= $arStudent['Missed'] ? 'checked' : ''; ?>>
                                                                                    Отс.
                                                                                </label>
                                                                            </div>
                                                                            <div class="likecell"><?= '<span class="js-sumBaDisc">' . intval($arStudent['SumBalls']) . '</span>/' . intval($arLection['RPD']['MaxBall']) ?></div>
                                                                        </div>
                                                                    <? endforeach; ?>

                                                                </div>

                                                            </div>
                                                        </div>
                                                    </div>
                                                <? endforeach; ?>
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </div>
                        </td>
            </div>
            <? endif;
            endfor;
            echo '</tr>';
            endfor; ?>
            </tbody>
            </table>
        </div>
    </div>

    <div role="tabpanel" class="tab-pane" id="dorab">
        <div class="search">
            <div class="row">
                <div class="col-sm-12">
                <p>Введите номер студенческого билета/зачетной книжки</p>
                    <br>
                </div>
                <div class="col-sm-6">
                    <form action="" class="search_student">

                        <input name="loginStudent" type="number">
                        <input name="loginPrepod" type="hidden" value="<?= $arResult['PREPOD_INFO']['LOGIN'] ?>">
                        <button type="submit">Найти</button>
                    </form>
                </div>
                <div class="col-sm-6">
                    <div class="TextMessageSave alert"></div>
                </div>
            </div>
            <div class="dor-result">

            </div>
        </div>
        <div class="search_result"></div>
    </div>
</div>
</div>

<?
if ($arResult["Ведомости"]): //todo удалить сж
    ?>
    <div class="modal" id="discModal" tabindex="-1" role="dialog" aria-labelledby="discModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close-btn"
                                                                                                      aria-hidden="true">ЗАКРЫТЬ</span>
                    </button>
                    <h4 class="modal-title" id="myModalLabel"></h4>
                    <ul class="col-md-6">
                        <li>Номер ведомости: <span id="numberVed"></span></li>
                        <li>Дата ведомости: <span id="dateVed"></span></li>
                        <li>Учебный год: <span id="yearLern"></span></li>
                        <li>Семестр: <span id="semestr"></span></li>
                    </ul>
                    <ul class="col-md-6">
                        <li>Направление: <span id="spec"></span></li>
                        <li>Форма обучения: <span id="formlern"></span></li>
                        <li>Курс: <span id="curse"></span></li>
                        <li>Год набора: <span id="nabor"></span></li>
                    </ul>

                </div>
                <div class="modal-body">
                    <?
                    foreach ($arResult["Ведомости"] as $arItems):

                        $date = DateTime::createFromFormat('Y-m-d', $arItems['ДатаВедомости']);
                        //$arJSvedomost[$arItems['НомерВедомости']]['dateVed'] = $date->format('d/m/Y');
                        $arJSvedomost[$arItems['НомерВедомости']]['dateVed'] = date('d/m/Y', $date);
                        $arJSvedomost[$arItems['НомерВедомости']]['numberVed'] = $arItems['НомерВедомости'];
                        $arJSvedomost[$arItems['НомерВедомости']]['yearLern'] = $arItems['УчебныйГод'] ? $arItems['УчебныйГод'] : false;
                        $arJSvedomost[$arItems['НомерВедомости']]['semestr'] = $arItems['НомерСеместра'] ? $arItems['НомерСеместра'] : false;
                        $arJSvedomost[$arItems['НомерВедомости']]['spec'] = $arItems['Специальность'] ? $arItems['Специальность'] : false;
                        $arJSvedomost[$arItems['НомерВедомости']]['formlern'] = $arItems['ФормаОбучения'] ? $arItems['ФормаОбучения'] : false;
                        $arJSvedomost[$arItems['НомерВедомости']]['curse'] = $arItems['НомерКурса'] ? $arItems['НомерКурса'] : false;
                        $arJSvedomost[$arItems['НомерВедомости']]['nabor'] = $arItems['ГодНабора'] ? $arItems['ГодНабора'] : false;
                        ?>
                    <div class="onemodule" id="modal<?= $arItems['НомерВедомости'] ?>" style="display: none">
                        <table>
                            <tr>
                                <td>Студент/Модуль</td><?
                                foreach ($arItems['РабочаяПрограммаДисциплины']['СписокМодулей'] as $arModule):
                                    ?>
                                    <td>
                                    <a href="#moduleBm" data-guid="<?= $arModule['УИД'] . $arItems['НомерВедомости'] ?>"
                                       data-toggle="modal" data-target="#moduleBm"><?= $arModule['Наименование'] ?></a>
                                    </td><?
                                endforeach;
                                ?>
                                <td>Итого</td>
                            </tr><?
                            foreach ($arItems['СписокСтудентов'] as $student):
                                ?>
                                <tr>
                                <td><?= $student ['ФИО'] ?></td><? $maxball = 0;
                                foreach ($arItems['РабочаяПрограммаДисциплины']['СписокМодулей'] as $arModule):
                                    $arIDlections = [];
                                    foreach ($arModule['СписокТем'] as $arTema) {
                                        foreach ($arTema['СписокЗанятий'] as $lections) {
                                            $arIDlections[] = $lections['УИД'];
                                        }
                                    }
                                    $ball = 0;
                                    foreach ($arItems['СписокОценок'] as $arBall) {
                                        if ($arBall['УИДСтудента'] === $student['УИД'] && in_array($arBall['УИДЗанятия'], $arIDlections)) {
                                            $ball += intval($arBall['Оценка']);
                                        }
                                    }
                                    foreach ($arItems['СписокОценокДоработки'] as $arBallDop) {
                                        if ($arBallDop['УИДСтудента'] === $student['УИД'] && $arBallDop['УИДМодуля'] === $arModule['УИД']) {
                                            $ball += intval($arBallDop['Оценка']);
                                        }
                                    }
                                    ?>
                                    <td><?= $ball . '/' . $arModule['МаксимальныйБалл'] ?></td><?
                                    $maxball += $arModule['МаксимальныйБалл'];
                                endforeach;
                                $itog = 0;
                                foreach ($arItems['СписокОценок'] as $arBall) {
                                    if ($arBall['УИДСтудента'] === $student['УИД']) {
                                        $itog += intval($arBall['Оценка']);
                                    }
                                }
                                foreach ($arItems['СписокОценокДоработки'] as $arBallDop) {
                                    if ($arBallDop['УИДСтудента'] === $student['УИД']) {
                                        $itog += intval($arBallDop['Оценка']);
                                    }
                                }

                                ?>
                                <td><?= $itog . '/' . $maxball ?></td></tr><?
                            endforeach; ?></table></div><?
                    endforeach; ?>
                </div>
            </div>
        </div>
    </div>
    <div class="modal" id="moduleBm" tabindex="-1" role="dialog" aria-labelledby="moduleBm" aria-hidden="true">
    <div class="modal-dialog">
        <div class="wrapper1">
            <div class="div1"></div>
        </div>
        <div class="modal-content table-responsive" style="overflow-y: hidden">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close-btn"
                                                                                                  aria-hidden="true">ЗАКРЫТЬ</span>
                </button>

                <h4 class="modal-title" id="myModalLabelBm"></h4>
                <div class="alert alert-danger">Баллы вводятся через расписание. Смотрите инструкцию под ссылкой Доступ
                    в личный кабинет преподавателя.
                </div>
            </div>
            <div class="modal-body">
                <? foreach ($arResult["Ведомости"] as $arItems): ?><? foreach ($arItems['РабочаяПрограммаДисциплины']['СписокМодулей'] as $arModule): ?>
                <div class="onemodule" id="bm<?= $arModule['УИД'] . $arItems['НомерВедомости'] ?>" style="display: none">

                    <form action="" class="jsFormUpdate" data-nv="<?= $arItems['НомерВедомости']; ?>">
                        <table id="$arModule['УИД']" class="table table-striped"
                               data-nved="<?= $arItems['НомерВедомости'] ?>" data-uidmodule="<?= $arModule['УИД'] ?>">
                            <thead>
                            <tr class="trbmheader">
                                <td style="min-width: 200px">Студент/Тема</td><?
                                foreach ($arModule['СписокТем'] as $arTem):
                                    ?>
                                <td class="bmheader"
                                    colspan="<?= count($arTem['СписокЗанятий']); ?>">
                                    <div style="max-height: 100px; overflow: hidden; text-overflow: ellipsis"><?= trim($arTem['Наименование']); ?></div>
                                    </td><?
                                endforeach; ?>
                                <td class="">Доработка</td>
                                <td class="" style="min-width: 70px;">Итог</td>
                            </tr>
                            </thead><?
                            foreach ($arItems['СписокСтудентов'] as $student):
                                ?>
                            <tr data-uidstudent="<?= $student['УИД'] ?>">
                                <td class="tdname"><?= $student ['ФИО'] ?></td><?
                                $itog = 0;
                                foreach ($arModule['СписокТем'] as $arTema):
                                    $ball = 0;
                                    $itogPoTem = 0;
                                    foreach ($arTema['СписокЗанятий'] as $nL => $lections):
                                        $arIDlections = $lections['УИД'];

                                        $maxball = $arModule['МаксимальныйБалл'];
                                        foreach ($arItems['СписокОценок'] as $arBall):
                                            if ($arBall['УИДСтудента'] === $student['УИД'] && $lections['УИД'] === $arBall['УИДЗанятия']):
                                                $ball = intval($arBall['Оценка']);
                                                $itog += intval($arBall['Оценка']);
                                                $itogPoTem += intval($arBall['Оценка']);
                                                if ($ball === 0 && $arBall['ПропустилЗанятие']) {
                                                    $ball = 'Н';
                                                }
                                                $uidLesson = $arBall['УИДЗанятия'];
                                                if (count($arTema['СписокЗанятий']) == 1) {
                                                    $maxb = $arTema['БаллПоТеме'];
                                                } elseif (count($arTema['СписокЗанятий']) > 1) {
                                                    if ($nL == 0) {
                                                        $maxb = $arTema['БаллПоТеме'];
                                                    } else {
                                                        $maxb = $arTema['БаллПоТеме'] - $itogPoTem;
                                                        if ($maxb < 0) {
                                                            $maxb = 0;
                                                        }
                                                        if ($maxb < $ball) {
                                                            $maxb = $ball;
                                                        }
                                                    }

                                                }

                                                ?>
                                                <td>
                                                <input id="<?= $student['УИД'] . $uidLesson ?>"
                                                       disabled
                                                       data-id="<?= $student['УИД'] . $uidLesson ?>" data-uidlesson="<?= $uidLesson ?>"
                                                       data-maxtemball="<?= $arTema['БаллПоТеме'] ?>"
                                                       data-studentuid="<?= $student['УИД'] ?>" data-itogpoteme="<?= $itogPoTem ?>"
                                                       data-nl="<?= $nL ?>" name="bal-<?= $student['УИД'] ?>" value="<?= $ball ?>"
                                                       autocomplete="off" class="js-ball" min="0" max="<?= abs($maxb) ?>" step="1"
                                                       <? if ($ball !== 'Н'): ?>type="number" <? else: ?>type="text" value="Н" <? endif; ?>><label
                                                        id="out-<?= $student['УИД'] . $uidLesson ?>" class="js-out">
                                                    <input type="checkbox" hidden value="1" <?= $arBall['ПропустилЗанятие'] ? ' checked ' : ''; ?>></label>
                                                </td><?
                                            endif;
                                        endforeach;
                                    endforeach;
                                endforeach;
                                foreach ($arItems['СписокОценокДоработки'] as $arBallDop):
                                    if ($arBallDop['УИДСтудента'] === $student['УИД'] && $arBallDop['УИДМодуля'] === $arModule['УИД']):
                                        $DopBall = intval($arBallDop['Оценка']);
                                        $itog += intval($arBallDop['Оценка']);
                                        ?>
                                        <td>
                                        <input id="<?= $student['УИД'] . $arModule['УИД'] ?>" name="dopbal-<?= $student['УИД'] ?>"
                                               disabled
                                               type="number" data-maxball="<?= $arModule['МаксимальныйБалл'] ?>"
                                               data-studentuid="<?= $student['УИД'] ?>" data-moduleuid="<?= $arModule['УИД'] ?>"
                                               data-dopball="1" autocomplete="off" class="js-dopball" min="0"
                                               max="<?= abs(intval($arModule['МаксимальныйБалл']) - intval($itog) + intval($DopBall)) ?>"
                                               value="<?= $DopBall ?>"></td><?
                                    endif; endforeach;
                                ?>
                                <td><span
                                            id="itog-<?= $student['УИД'] . $arModule['УИД'] ?>"><?= $itog ?></span><?= '/' . $arModule['МаксимальныйБалл']; ?>
                                </td></tr><?
                            endforeach;
                            ?></table>
                    </form>
                    </div><? endforeach;
                endforeach;
                ?></div>
        </div>
    </div></div><?
endif;

foreach ($arJSlection as &$arlec) {
    unset($arlec['RPD']);
}

//сводная таблица
if (is_array($arResult['SVOD']) && !empty($arResult['SVOD'])): ?>
    <div class="modal" id="svodModal" tabindex="-1" role="dialog" aria-labelledby="svodModal" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content table-responsive">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close-btn"
                                                                                                      aria-hidden="true">ЗАКРЫТЬ</span>
                    </button>
                    <h4 class="modal-title">Сводная таблица</h4>
                </div>
                <div class="modal-body">
                    <ul class="nav nav-tabs" role="tablist">
                        <?
                        $i = 0;
                        foreach ($arResult['SVOD']['HEADER'] as $bm => $arHead): ?>

                            <li class="<?= $i === 0 ? ' active' : '' ?>">
                                <a class="thome" href="#<?= $bm ?>" aria-controls="home" role="tab" data-toggle="tab"><?= $bm ?></a>
                            </li>
                            <?
                            $i++;
                        endforeach; ?>
                    </ul>

                    <div class="tab-content">
                        <?
                        $i = 0;
                        foreach ($arResult['SVOD']['HEADER'] as $bm => $arHead): ?>
                            <div role="tabpanel" class="tab-pane<?= $i === 0 ? ' active' : '' ?>" id="<?= $bm ?>">
                                <table class="table table-striped">
                                    <thead>
                                    <tr>
                                        <th>ФИО/Тема</th>
                                        <? foreach ($arHead as $arTema): ?>
                                            <th>
                                                <div style="max-height: 71px; overflow: hidden;word-break: break-word;font-size: 13px">
                                                    <span style="word-break: keep-all"><?= trim($arTema['date']) . '</span><br>' . $arTema['tema'] ?>
                                                </div>
                                            </th>
                                        <? endforeach; ?>
                                        <th>
                                            Доработка
                                        </th>
                                        <th style="min-width: 100px">
                                            Итог/<br>МаксМодуль/<br>МаксДисц
                                        </th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <? foreach ($arResult['SVOD']['ITEMS'][$bm] as $student => $arItems):
                                        $backcol = '';
                                        if (0) {
                                            $backcol = 'background-color: rgba(68, 157, 68, 0.5);'; //зелен
                                        } elseif ($arItems[0]['Status'] == 2) {
                                            $backcol = 'background-color: rgba(178, 0, 0, 0.5);';//красн
                                        } elseif ($arItems[0]['Status'] == 3) {
                                            $backcol = 'background-color: rgb(100%, 86%, 20%, 0.5);';//желт
                                        } ?>
                                        <tr style="<?= $backcol ?>">
                                            <td>

                                                <div style="width: 215px; overflow: hidden; white-space: nowrap;"><?= $arItems[0]['Student']['Name']?></div>
                                            </td>
                                            <? foreach ($arItems as $arItem): ?>
                                                <td><?= $arItem['Missed'] ? 'Н' : $arItem['Ball'] ?></td>
                                            <? endforeach; ?>
                                            <td><?= intval($arItems[0]['BallR']) ?></td>
                                            <td><?= intval($arItems[0]['SumBalls']) . '/' . intval($arItems[0]['Module']['MaxBall']) . '/' . $arResult['SVOD']['MAXBALL'] ?></td>

                                        </tr>
                                    <? endforeach; ?>
                                    </tbody>
                                </table>
                            </div>
                            <?
                            $i++;
                        endforeach; ?>
                    </div>


                </div>
            </div>
        </div>
    </div>
<? endif;

if (IsModuleInstalled("im") && CBXFeatures::IsFeatureEnabled('WebMessenger')) {
    $APPLICATION->IncludeComponent("glab:im.messenger", "", [], false);
}
?>
<script>
  //объект для js, без него все умрет
  let arLection = '<?=str_replace("\\", "\\\\", \Bitrix\Main\Web\Json::encode($arJSlection)) ?>';
  let arResult = {}; //удалить сж
  arResult.vedomosti = '<?=\Bitrix\Main\Web\Json::encode($arJSvedomost);?>';//удалить сж
</script>
<style>
    #moduleBm .modal-dialog {
        width: 100%;
        height: 100%;
        padding: 0;
        margin: 0;
    }

    input:disabled {
        background-color: white;
    }
</style>
