function intval(num) {
  if (typeof num == 'number' || typeof num == 'string') {
    num = num.toString();
    let dotLocation = num.indexOf('.');
    if (dotLocation > 0) {
      num = num.substr(0, dotLocation);
    }
    if (isNaN(Number(num))) {
      num = parseInt(num);
    }
    if (isNaN(num)) {
      return 0;
    }
    return Number(num);
  }
  else if (typeof num == 'object' && num.length != null && num.length > 0) {
    return 1;
  }
  else if (typeof num == 'boolean' && num === true) {
    return 1;
  }
  return 0;
}

function countProperties(obj) {
  let count = 0;

  for (let prop in obj) {
    if (obj.hasOwnProperty(prop))
      ++count;
  }

  return count;
}

function getParams() {
  return window.location.search.replace('?', '').split('&').reduce(
    function (p, e) {
      let a = e.split('=');
      p[decodeURIComponent(a[0])] = decodeURIComponent(a[1]);
      return p;
    },
    {}
  );
}

const LOADER = '.form-loader';

$(function () {
  const DELIM = 'DELIM';  //параметр для разделение Темы занятия и Guid модуля
  const opwin = 'opwin';  //get параметр для открытия журнала
  const svod = 'svod';  //get параметр для открытия сводной таблицы
  const STATUSOK = 'Успех';  //статусы ответа 1с
  const STATUSWARN = 'Предупреждение';  //статусы ответа 1с
  const STATUSERROR = 'Ошибка';  //статусы ответа 1с
  const KEYMESSAGE = 'TextMessage';  //ключ соробщения
  const KEYSTATUS = 'Status';  //ключ статуса
  let scaleNum = 1.8;


  //объект из php, без него все умрет
  if (arLection) {
    arLection = $.parseJSON(arLection);
  }
  //date

  $('input.datems').change(function () {
    let curDate = $(this).val().replace(/\./g, '-');
    let data = {
      'curDate': curDate,
    };
    let hashSedule = '#shedule';
    $(LOADER).fadeIn(300);
    $.ajax({
      type: 'POST',
      url: '',
      data: data,
      success: function (res) {
        res = $.parseJSON(res);
        // window.location.href = url;
        location.href = location.origin + location.pathname + '?datereq=' + res.monday + hashSedule;
      },
    });
  });
  $('.btnprev, .btnnext').click(function () {
    let curDate = $(this).val();
    let data = {
      'curDate': curDate,
    };
    let hashSedule = '#shedule';
    $(LOADER).fadeIn(300);
    $.ajax({
      type: 'POST',
      url: '',
      data: data,
      success: function (res) {
        res = $.parseJSON(res);
        // window.location.href = url;
        location.href = location.origin + location.pathname + '?datereq=' + curDate + hashSedule;
      },
    });
  });

// табы при перезагрузке
  $(document).ready(function () {
    if (location.hash) {
      $('a[href="' + location.hash + '"]').tab('show');
    }
    $(document.body).on('click', 'a[data-toggle=\'tab\']', function (event) {
      location.hash = this.getAttribute('href');
    });
  });
  // $(window).on('popstate', function () {
  //   let anchor = location.hash || $('a[data-toggle=\'tab\']').first().attr('href');
  //   $('a[href=\'' + anchor + '\']').tab('show');
  // });

  //лоадер на пагинацию
  $('.page-item').not('.active, .disabled').click(function () {
    $(LOADER).fadeIn(100);
  });

  //новая ведомость

  // //скролл в модале здец
  // function changeHi(id, liket) {
  //   var $liketable = '';
  //   var windowHeight = $(window).height();
  //   if (liket === false || liket === undefined) {
  //     $liketable = $('.liketable');
  //   } else {
  //     $liketable = liket;
  //   }
  //
  //   var modalcontent = document.getElementsByClassName('modal-content')[0];
  //   console.log(modalcontent);
  //   if (intval(modalcontent.clientHeight) > intval(windowHeight)) {
  //     var hepx = intval(windowHeight) / scaleNum;
  //     $liketable.css({'height': hepx.toString() + 'px'});
  //   }
  // }


  function eventBootsrap(id) {
    $('#' + id).on('shown.bs.modal', function (e) { //событие открытия окна ведомости
      $(LOADER).fadeOut(300);
    }).on('hide.bs.modal', function (e) {//событие закрытия окна ведомости
      window.location.hash = '';
      if (location.search.indexOf(opwin) !== -1) {
        let newurl = window.location.href.replace(window.location.search, '');
        window.history.replaceState('object or string', 'Title', newurl);
      }
    });
  }

  //запуск событий отк зак мод окна + скрыть баллы в лекциях
  Object.keys(arLection).forEach(function (key) {
    eventBootsrap(arLection[key]['id']);
    if (arLection[key]['view'] === 'Лекция') {
      $('#' + arLection[key]['id'] + ' .js-ball').hide();
    }
  });

  $('#svodModal').on('shown.bs.modal', function (e) { //событие открытия окна сводной

  }).on('hidden.bs.modal', function () {//при закрытии сводной таблицы, пересчитываем размер для скрола
    let id = $('td.modal.in').attr('id');
    $('#' + id).modal('hide').modal('show');
  });

  // //событие переключения таба группы
  // $('.chgroup').on('shown.bs.tab', function (e) {
  //   $('.new-modal-j').modal('handleUpdate');
  //   // changeHi($($(this).attr('href') + ' .liketable'));
  // });

  //функционал ведомости

  //клик по лекции
  $('td').on('click', '.js-lectionhref', function () {
    $(LOADER).fadeIn(300);
  }).on('click', '.js-svod', function () {
    $(LOADER).fadeIn(300);
  });


  //клик по чекбоксу отсутствует
  $('.js-out').change(function () {
    let $inp = $(this).closest('.likecell').find('input.js-ball');
    if ($(this).is(':checked')) {
      $inp.attr({
        'type': 'text',
        'value': 'Н',
        'readonly': true,
      }).val('Н');
    } else {
      $inp.attr({
        'type': 'number',
        'readonly': false,
        'value': '0',
      }).val('0');
    }
  });

  //меняем макс бал и прописываем назв модуля при изменении темы
  $('.js-tema').change(function () {
    /* arMod
     * массив в [
     * 0 => ГуидМодуля,
     * 1 => НазваниеТемы,
     * 2=>Название модуля,
     * 3=>ТемаМаксимальныйБалл]
     * */
    let arMod = $(this).val().split(DELIM);
    let moduleName = arMod[2];
    let maxTemaBall = arMod[3];
    $(this).closest('form').find('.js-ball[type="number"]').attr({'max': intval(maxTemaBall)});
    $('.js-show-module').text(moduleName);
  });



  //показываем подгруппу для ин-яза
  $('body').on('click', '.js-show-sub', function () {
    let subGroup = $(this).text();
    $('div[data-stshow]').hide();
    $('div[data-stshow="' + subGroup + '"]').show();
  });
  //если подгруппы есть фильтруем ибо нельзя сохранять в инязе всю группу
  if($('.js-show-sub').text()){
    $('.js-show-sub')[0].click();
  }

  console.log(arLection);
  //сохранение оценок
  $('form.jsFormUpdate').submit(function (e) {
    e.preventDefault();
    $(LOADER).fadeIn();
    let $form = $(this);
    let arBalls = {save: []};
    let data = {};
    let isEn = $form.find('input.js-isen').val();
    let selectr = '';

    if(isEn){
      selectr = ':visible';
    }
    data.arModGuidTema = $form.find('select').val().split(DELIM);
    $form.find('input.js-ball'+selectr).each(function (i, input) {
      let $thisInput = $(input);
      data.ball = $thisInput.val();
      data.stuid = $thisInput.closest('[data-stuid]').data('stuid');
      data.stname = $thisInput.closest('[data-stuid]').find('.js-stname').text().trim();
      data.groupGuid = $thisInput.attr('data-groupGuid');
      data.groupName = $thisInput.attr('data-groupName');
      data.subGroupName = $thisInput.attr('data-subGroupName');
      data.subGroupGuid = $thisInput.attr('data-subGroupGuid');
      data.idLection = $thisInput.attr('data-idLection');
      data.moduleName = $('.js-show-module').text().trim();
      data.nn = 0;

      if ($.trim(data.ball) === 'н' || $.trim(data.ball) === 'Н') {
        data.ball = 0;
        data.nn = 1;
      }
      arBalls.save.push({
        'idLection': data.idLection,
        'subject': arLection[data.idLection]['subject'],
        'subjectGuid': arLection[data.idLection]['subjectGuid'],
        'teacher': arLection[data.idLection]['teacher'],
        'teacherGuid': arLection[data.idLection]['teacherGuid'],
        'timeStart': arLection[data.idLection]['timeStart'],
        'timeEnd': arLection[data.idLection]['timeEnd'],
        'view': arLection[data.idLection]['view'],
        'uchYear': arLection[data.idLection]['uchYear'],
        'uchYearGuid': arLection[data.idLection]['uchYearGuid'],
        'semestr': intval(arLection[data.idLection]['semestr']),
        'date': arLection[data.idLection]['dateExp'],
        'tema': data.arModGuidTema[1],
        'temaMaxBall': intval(data.arModGuidTema[3]),
        'rpd': arLection[data.idLection]['rpd'],
        'rpdGuid': arLection[data.idLection]['rpdGuid'],
        'module': data.moduleName,
        'moduleGuid': data.arModGuidTema[0],
        'ball': intval(data.ball),
        'stname': data.stname,
        'stuid': data.stuid,
        'groupName': data.groupName,
        'groupGuid': data.groupGuid,
        'potokGuid': arLection[data.idLection]['potokGuid'],
        'potok': arLection[data.idLection]['potok'],
        'subGroupGuid': data.subGroupGuid,
        'subGroupName': data.subGroupName,
        'nn': data.nn,
      });
    });

    $.ajax({ //обработкаа в component.php
      type: 'POST',
      url: '',
      data: arBalls,
    }).done(function (res) {
      let message = false;
      res = $.parseJSON(res);
      if (res[KEYSTATUS] === STATUSOK) {
        message = 'Данные успешно сохранены';
        $('.alert-success').show().text(message);

        //обновляем сумму балов студентов
        let dataUpadeBall = {
          'updateBall': 1,
          'par1': arLection[data.idLection]['arFilterJgrE1'],
          'par2': arLection[data.idLection]['arFilterJgrE2'],
        };
        console.log(dataUpadeBall);
        $.ajax({
          type: 'POST',
          url: '',
          data: dataUpadeBall,
        }).done(function (res) {
          res = $.parseJSON(res);
          if (res !== false) {
            Object.keys(res).forEach(function (idStudent) {
              $('[data-stuid="' + idStudent + '"]').find('.js-sumBaDisc').text(intval(res[idStudent]));
            });
          }
        });


        setTimeout(function () {
          $('.alert-success').hide();
        }, 5000);
      } else {
        message = 'Ошибка. Не удалось сохранить данные в 1С';
        $('.alert-danger').show().text(message);
      }
    }).always(function () {
      $(LOADER).fadeOut();
    });

  });

  //открываем журнал по гет параметру
  if (location.search.indexOf(opwin) !== -1) {
    setTimeout(function () {
      $('#' + getParams().opwin).modal();
      //открываем сводную таблицу по гет параметру
      if (location.search.indexOf(svod) !== -1) {
        $('#svodModal').modal();
      }
    }, 1)
  }

  /*доработка*/
  let arDor = {};

  /*поиск по номеру студента*/
  $('.search_student').submit(function (e) {
    e.preventDefault();
    $(LOADER).fadeIn(300);
    let data = {
      'loginStudent': $(this).find('input[name="loginStudent"]').val(),
      'loginPrepod': $(this).find('input[name="loginPrepod"]').val(),
    };

    $.ajax({
      type: 'POST',
      url: '',
      data: data,
      success: function (res) {
        $(LOADER).fadeOut(300);
        res = JSON.parse(res);
        arDor = res.JS;
        $('.dor-result').html(res.HTML);
      },
    });
  });


  /*сохранение доработки*/
  $('body').on('submit', '.dor-save-ball', function (e) {
    e.preventDefault();

    let data = {};
    let $form = $(this);
    let $inputball = $form.find('input');
    let $button = $form.find('button');
    data = arDor[$button.attr('data-mguid')];
    data.Ball = intval($inputball.val());

    $(LOADER).fadeIn(300);
    $.ajax({
      type: 'POST',
      url: '',
      data: data,
      success: function (res) {
        $(LOADER).fadeOut(300);

        res = JSON.parse(res);
        let divclass = '';
        if (res.Status === 'Успех') {
          divclass = 'alert-success';
        } else {
          divclass = 'alert-danger';
        }
        if (res.TextMessage) {
          $('.TextMessageSave').addClass(divclass).text(res.TextMessage).show();
          setTimeout(function () {
            $('.TextMessageSave').hide();
          }, 5000);
        }

      },
    });

  });
  /*поиск по номеру студента END*/
  /*доработка END*/

  // var oldURL = Location;
  // var index = 0;
  // var newURL = oldURL;
  // index = oldURL.indexOf('?');
  // if(index == -1){
  //   index = oldURL.indexOf('#');
  // }
  // if(index != -1){
  //   newURL = oldURL.substring(0, index);
  // }
//////////////////////////////////////////////////////// Удалить сж start

  if (arResult.vedomosti) {
    arResult.vedomosti = JSON.parse(arResult.vedomosti);
  }
  $('.js-out>input').change(function () {
    var id = $(this).closest('label').attr('id').substring(4);

    if ($(this).is(':checked')) {
      $('input[data-id=' + id + ']').attr({
        'type': 'text',
        'value': 'Н',
        'readonly': true,
      }).val('Н');
    } else {
      //$("#" + id).attr({
      $('input[data-id=' + id + ']').attr({
        'type': 'number',
        'readonly': false,
        'value': '0',
      }).val('0');
    }
  });

  $('a[data-target="#discModal"]').click(function () {

    let guidSubject = $(this).data('guid');
    $('#modal' + guidSubject).show();
    $('#myModalLabel').html($(this).text());
    if (arResult.vedomosti[guidSubject].dateVed) {
      $('#dateVed').html(arResult.vedomosti[guidSubject].dateVed);
    }
    if (arResult.vedomosti[guidSubject].numberVed) {
      $('#numberVed').html(arResult.vedomosti[guidSubject].numberVed);
    }
    if (arResult.vedomosti[guidSubject].yearLern) {
      $('#yearLern').html(arResult.vedomosti[guidSubject].yearLern);
    }
    if (arResult.vedomosti[guidSubject].semestr) {
      $('#semestr').html(arResult.vedomosti[guidSubject].semestr);
    }
    if (arResult.vedomosti[guidSubject].spec) {
      $('#spec').html(arResult.vedomosti[guidSubject].spec);
    }
    if (arResult.vedomosti[guidSubject].formlern) {
      $('#formlern').html(arResult.vedomosti[guidSubject].formlern);
    }
    if (arResult.vedomosti[guidSubject].curse) {
      $('#curse').html(arResult.vedomosti[guidSubject].curse);
    }
    if (arResult.vedomosti[guidSubject].nabor) {
      $('#nabor').html(arResult.vedomosti[guidSubject].nabor);
    }
  });
  $('#discModal').on('hide.bs.modal', function (event) {
    $('#discModal .onemodule').each(function () {
      $(this).hide();
    });
  });

  $('a[data-target="#moduleBm"]').click(function () {
    let guidSubject = $(this).data('guid');
    $('#bm' + guidSubject).show();
    $('#myModalLabelBm').html($(this).text());
  });
  $('#moduleBm').on('hide.bs.modal', function (event) {
    let nved = $('#moduleBm .onemodule:visible .jsFormUpdate').data('nv');
    $('#moduleBm .onemodule:visible').hide();

  });

  $('#moduleBm').on('shown.bs.modal', function (event) {
    //для двойной прокрутки
    $('.wrapper1 .div1').css({'width': $('#moduleBm .modal-content').prop('scrollWidth')});
    $('html').css({'overflow': 'hidden'});
  });

  $(window).load(function () {
    let params = window
      .location
      .search
      .replace('?', '')
      .split('&')
      .reduce(
        function (p, e) {
          var a = e.split('=');
          p[decodeURIComponent(a[0])] = decodeURIComponent(a[1]);
          return p;
        },
        {}
      );
    $(LOADER).fadeOut(300);
    if ('nved' in params) {
      $('[data-guid="' + params['nved'] + '"]').click();
      delete  params['nved'];
    }
    let urlget = location.origin + location.pathname + '?';
    if (countProperties(params) > 0) {
      for (let key in params) {
        if (params[key] !== 'undefined') {
          urlget += key + '=' + params[key] + '&';
        }
      }
    }
    urlget = urlget.substr(0, urlget.length - 1);
    history.replaceState(null, '', urlget);
  });

//////////////////////////////////////////////////////// Удалить сж end

});