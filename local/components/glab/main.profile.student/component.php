<?
/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @var CBitrixComponent $this
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$arResult["ID"] = $USER->GetID();

$obCache = new CPHPCache;
$lifeTime = intval($arParams['CACHE_TIME']) * 24;
$cachePath = '/';
$cacheIDstudent = 'cstd' . strval($arResult["ID"]);
if ($obCache->InitCache($lifeTime, $cacheIDstudent, $cachePath)) {
	$arResult['STUDENT'] = $obCache->GetVars()['STUDENT'];
} elseif ($obCache->StartDataCache($lifeTime, $cacheIDstudent, $cachePath)) {
	$student = new Student();
	$arResult["STUDENT"] = $student->getStudentInfo();
	$obCache->EndDataCache(['STUDENT' => $arResult['STUDENT']]);
}

if (!$arResult["STUDENT"]["UF_STUDENT_GUID"] || $arResult["ID"] <= 0) {
	if ($arResult["ID"] > 0) {
		echo "<p class='alert alert-success'>Ошибка значения guid " . $arResult["STUDENT"]["LOGIN"] . "</p>";
		global $USER;
		$USER->Logout();
	}
	$APPLICATION->ShowAuthForm("");
	return;
}

$arResult["ID"] = intval($USER->GetID());

define("NO_KEEP_STATISTIC", true);
CJSCore::Init(array('popup', 'date'));
function processAjax()
{
	$context = \Bitrix\Main\Context::getCurrent();
	$request = $context->getRequest();
	if ($request->isAjaxRequest() && $request->get('curDate')) {
		global $APPLICATION;
		$APPLICATION->RestartBuffer();
		$now = date('c');
		if ($request->get('curDate')) {
			$now = $request->get('curDate');
		}
		$obDate = new Lk();
		$arDate = $obDate::dateToMS($now, '%d.%m.%Y');
		echo json_encode($arDate);
		exit;
	}
}

$context = \Bitrix\Main\Context::getCurrent();
$request = $context->getRequest();
if ($request->get('datereq')) {
	$datereq = $request->get('datereq');
} else {
	$datereq = date('d-m-Y');

}

$obCache = new CPHPCache;
$lifeTime = $arParams['CACHE_TIME'];
$cacheIDSCHEDULE = 'cach' . strval($datereq) . strval($arResult["STUDENT"]['UF_GROUP_GUID']);
$cacheIDprog = 'cach' . strval($USER->GetID());
$arDate = Lk::dateToMS($datereq);

if ($obCache->InitCache($lifeTime, $cacheIDprog, $cachePath)) {

	$arResult = $obCache->GetVars();
} elseif ($obCache->StartDataCache($lifeTime, $cacheIDprog, $cachePath)) {
	$arResult['PROGRESS'] = Progress::getOneProgress();
	if (strlen($arResult["STUDENT"]["FIO"]) < 5) {
		$arResult["STUDENT"]["FIO"] = $arResult['PROGRESS']["ITEMS"][0]["FIO"];
	}
	$arResult['arPortfolio'] = Student::getStudentPortfolio();
	$arResult['STUDENT_INFO'] = $arResult['arPortfolio']['COMMON'];
	unset($arResult['arPortfolio']['COMMON']);

	$endSh = microtime(true);
	$final = round(floatval($endSh) - floatval($startSh), 4);

	$obCache->EndDataCache($arResult);
}

$nk = 1;
$arResult['PAGIN'] = intval($arResult['STUDENT_INFO']['KURS']);
if ($arResult['PAGIN']) {
	$nk = $arResult['PAGIN'];
}

if ($request->get('nk')) {
	$nk = intval($request->get('nk'));
}

$arResult['CUR_PAGIN'] = $nk;
$arResult['CUR_PAGE'] = $APPLICATION->GetCurPage();
$cacheIDprogOnline = 'cach' . strval($nk) . strval($USER->GetID());

if ($obCache->InitCache($lifeTime, $cacheIDprogOnline, $cachePath)) {

	$arResult['PROGRESS_ONLINE'] = $obCache->GetVars()['PROGRESS_ONLINE'];

} elseif ($obCache->StartDataCache($lifeTime, $cacheIDprogOnline, $cachePath)) {
	//успеваемость

	$arResult['PROGRESS_ONLINE'] = Student::getCurProgressOnline($arResult["STUDENT"]["UF_STUDENT_GUID"], intval($nk))['РасширеннаяВедомость'];
	if ($arResult['PROGRESS_ONLINE']['НомерВедомости']) {
		$arResult['PROGRESS_ONLINE'] = [$arResult['PROGRESS_ONLINE']];
	}
	if (!$arResult['PROGRESS_ONLINE'][0]['НомерВедомости']) {
		unset($arResult['PROGRESS_ONLINE']);
	}

	if (is_array($arResult['PROGRESS_ONLINE']))
		foreach ($arResult['PROGRESS_ONLINE'] as &$arVed) {
			if ($arVed['РабочаяПрограммаДисциплины']['СписокМодулей']['УИД']) {
				$arVed['РабочаяПрограммаДисциплины']['СписокМодулей'] = [$arVed['РабочаяПрограммаДисциплины']['СписокМодулей']];
			}
			if ($arVed['СписокСтудентов']['УИД']) {
				$arVed['СписокСтудентов'] = [$arVed['СписокСтудентов']];
			}
			if ($arVed['СписокОценок']['УИДСтудента']) {
				$arVed['СписокОценок'] = [$arVed['СписокОценок']];
			}
			if ($arVed['СписокОценокДоработки']['УИДСтудента']) {
				$arVed['СписокОценокДоработки'] = [$arVed['СписокОценокДоработки']];
			}

			if (is_array($arVed['РабочаяПрограммаДисциплины']['СписокМодулей']))
				foreach ($arVed['РабочаяПрограммаДисциплины']['СписокМодулей'] as &$arModule) {
					if ($arModule['СписокТем']['УИД']) {
						$arModule['СписокТем'] = [$arModule['СписокТем']];
					}
					if (is_array($arModule['СписокТем']))
						foreach ($arModule['СписокТем'] as &$arTem) {
							if ($arTem['СписокЗанятий']['УИД']) {
								$arTem['СписокЗанятий'] = [$arTem['СписокЗанятий']];
							}
						}
				}
		}

	$obCache->EndDataCache(['PROGRESS_ONLINE' => $arResult['PROGRESS_ONLINE']]);
}

if ($obCache->InitCache($lifeTime, $cacheIDSCHEDULE, $cachePath)) {

    $arResult["SCHEDULE"] = $obCache->GetVars()['SCHEDULE'];

} elseif ($obCache->StartDataCache($lifeTime, $cacheIDSCHEDULE, $cachePath)) {

	//$arResult["SCHEDULEOlD"] = Schedule::getOneSchedule($arDate['monday'], $arDate['sunday']);
	$arResult["SCHEDULE"]['YEAR'] = Schedule::getkListScheduleBitrix('Y');
	$arDate = Lk::dateToMS($datereq, '%d.%m.%Y');
	$arResult["SCHEDULE"]['DATE_START'] = $arDate['monday'];
	$arResult["SCHEDULE"]['DATE_END'] = $arDate['sunday'];
	$arDateLast = Lk::dateToMS($datereq, '%d.%m.%Y', 'last');
	$arDateNext = Lk::dateToMS($datereq, '%d.%m.%Y', 'next');
	$arResult['SCHEDULE']['SYS_DATE_NEXT'] = $arDateNext['monday'];
	$arResult['SCHEDULE']['SYS_DATE_PREV'] = $arDateLast['monday'];

	$entityDataClassSh = Lk::GetEntityDataClass(HL_SHEDULE_ID);
	$entityDataClassGr = Lk::GetEntityDataClass(HL_GROUPS_ID);
	$groupHlId = $entityDataClassGr::getList(['select' => ['ID'], 'filter' => ['=UF_XML_ID' => trim($arResult["STUDENT"]['UF_GROUP_GUID'])]])->fetch()['ID'];
	$shOb = $entityDataClassSh::getList(['select' => ['*'], 'filter' => [
		'LOGIC' => 'AND',
		[
			'>=UF_DAY' => $arDate['monday'],
			'UF_LNK_GROUPS' => $groupHlId,
		],
		[
			'<=UF_DAY' => $arDate['sunday'],
			'UF_LNK_GROUPS' => $groupHlId,
		]
	]]);
	while ($shRes = $shOb->fetch()){
		$arResult["SCHEDULE"]['ITEMS'][$shRes['UF_N_PARA']][$shRes['UF_DAY']->format('N')][] = array(
			'date' => $shRes['UF_DAY']->format('d.m.Y'),
			'ndate' => $shRes['UF_DAY']->format('N'),
			'timeStart' => $shRes['UF_START'],
			'timeEnd' => $shRes['UF_END'],
			'subject' => $shRes['UF_DISCIPLIN'],
			'place' =>  $shRes['UF_AUDIT'],
			'view' => $shRes['UF_TYPE'],
			'korpus' => $shRes['UF_KORPUS'],
			'teacher' => $shRes['UF_PREPOD'],
		);
		$arResult["SCHEDULE"]['RINGS'][$shRes['UF_N_PARA']] = $shRes['UF_START'] . '-' . $shRes['UF_END'];
	}
	foreach ($arResult["SCHEDULE"]['ITEMS'] as $nlect => $arDay) {
		foreach ($arDay as $nday => $arTempLect) {
			if (count($arTempLect) === 1) {
				$arResult["SCHEDULE"]['ITEMS'][$nlect][$nday] = $arTempLect[0];
			}
		}
	}
	ksort($arResult["SCHEDULE"]['ITEMS']);
	ksort($arResult["SCHEDULE"]['RINGS']);

    $obCache->EndDataCache(['SCHEDULE' => $arResult['SCHEDULE']]);
}

processAjax();
$this->IncludeComponentTemplate();