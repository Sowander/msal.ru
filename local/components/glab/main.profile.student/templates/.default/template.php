<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
use Bitrix\Main\Page\Asset;

Asset::getInstance()->addString("<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/bootstrap.min.css");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/bootstrap.min.js");
$ruWeek = array('Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье');
?>
	<div>
		<br>
		<div class="student-info">
			<div class="row">
				<div class="col-md-6">
					<h2><?= $arResult['STUDENT']['UF_STUDENT_FAK']; ?></h2>
					<h3><?= $arResult['STUDENT_INFO']['FIO']; ?></h3>
					<h4 style="padding-top:10px">Ваша группа: <?= $arResult['STUDENT']['UF_STUDENT_GROUP']; ?></h4>
					<h4 style="padding-top:10px">Ваш телефон: <?= $arResult['STUDENT_INFO']['PHONE']; ?></h4>
					<h4 style="padding-top:10px">E-mail: <?= $arResult['STUDENT_INFO']['EMAIL']; ?></h4>
					<h4 style="padding-top:10px">Дата рождения: <?= $arResult['STUDENT_INFO']['DATE_BIRTH']; ?></h4>
				</div>
				<div class="col-md-6" style="height: 100%"><? if ($arResult["STUDENT"]["PERSONAL_PHOTO"]): ?>
						<img class="student-img" src="<?= $arResult["STUDENT"]["PERSONAL_PHOTO"] ?>" alt="">
									<? endif; ?>
				</div>
			</div>
		</div>
		<br>
		<!-- Навигация -->
		<ul class="nav nav-tabs" role="tablist">
			<li class="active"><a class="thome" href="#home" aria-controls="home" role="tab" data-toggle="tab">Расписание</a>
			</li>
			<li><a class="tprofile" href="#profile" aria-controls="profile" role="tab" data-toggle="tab">Зачетная
					книжка</a></li>
			<li><a class="tprofile" href="#progress" aria-controls="progress" role="tab" data-toggle="tab">Журнал
					успеваемости</a></li>
			<li><a href="#portfolio" aria-controls="portfolio" role="tab" data-toggle="tab">Портфолио</a></li>
			<li>
				<a onclick="$('.form-loader').fadeIn(100);" href="<?= 'http://' . $_SERVER['HTTP_HOST'] . substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], '?')) . '?logout=yes' ?>">Выход</a>
			</li>
		</ul>
		<div class="clearfix"></div>
		<!-- Содержимое вкладок -->
		<div class="tab-content">
			<div role="tabpanel" class="schedule tab-pane active" id="home">
				<h2 style="padding:10px 0 10px 0">Расписание занятий на <?= $arResult["SCHEDULE"]['YEAR'][0]['NAME'] ?>
					учебный год</h2>

				<button type="text" class="btn btn-default btnprev"
				        value="<?= $arResult['SCHEDULE']['SYS_DATE_PREV'] ?>">Пред. Неделя
				</button>
				<input type="text" class="datems"
				       value="<?= $arResult['SCHEDULE']['DATE_START'] . " - " . $arResult['SCHEDULE']['DATE_END']; ?>"
				       name="date" onclick="BX.calendar({node: this, field: this, bTime: false});">
				<button type="text" class="btn btn-default btnnext"
				        value="<?= $arResult['SCHEDULE']['SYS_DATE_NEXT'] ?>">След. Неделя
				</button>


				<div class="">
					<table class="schedule__table">
						<thead>

						<tr>
							<th>№</th>
							<th>Время</th>
													<? foreach ($ruWeek as $day): ?>
							  <th><?= $day ?></th>
													<? endforeach; ?>
						</tr>
						</thead>
						<tbody>
												<? $rows = 8; // количество строк, tr
												$cols = 9;
												for ($tr = 1; $tr <= $rows; $tr++):
													?>
													<?= '</tr>' ?><? for ($td = 1; $td <= $cols; $td++): ?>

													<?
													if ($td != 1 && $td != 2) {
														//если два заниятия
														if ($arResult["SCHEDULE"]['ITEMS'][$tr][$td - 2][1]) {
															echo '<td>';
															foreach ($arResult["SCHEDULE"]['ITEMS'][$tr][$td - 2] as $ki => $navibor) {
																if ($ki !== 0) {
																	echo '<hr>';
																}
																echo $navibor['view'] . '<br>';
																echo '<p class="schedule__name">' . $navibor['subject'] . '</p>';
																echo '<p class="schedule__lector">' . $navibor['teacher'] . '</p>';
																echo $navibor['place'] . '<br>';
															}
														} else {
															echo '<td>';
															echo $arResult["SCHEDULE"]['ITEMS'][$tr][$td - 2]['view'] . '<br>';
															echo '<p class="schedule__name">' . $arResult["SCHEDULE"]['ITEMS'][$tr][$td - 2]['subject'] . '</p>';
															echo '<p class="schedule__lector">' . $arResult["SCHEDULE"]['ITEMS'][$tr][$td - 2]['teacher'] . '</p>';
															echo $arResult["SCHEDULE"]['ITEMS'][$tr][$td - 2]['place'] . '<br>';
															echo '</td>';
														}
													} elseif ($td === 2) {
														echo '<td>';
														echo $arResult["SCHEDULE"]['RINGS'][$tr];
														echo '</td>';
													} elseif ($td === 1) {
														echo '<td>';
														echo $arResult["SCHEDULE"]['RINGS'][$tr] ? $tr : '';
														echo '</td>';
													}
													?>

												<? endfor; ?>
													<?= '</tr>' ?><? endfor; ?>
						</tbody>
					</table>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="profile">


				<div role="tabpanel" class="tab-pane" id="zachet">

					<div class="table-responsive">
						<table class="table">
													<? foreach ($arResult['PROGRESS']['ITEMS'] as $key => $item): ?>

													<? if ($key == 0): ?>
							<thead>
							<tr>
								<th>№</th>
								<th>Наименование дисциплины</th>
								<th>Преподаватель</th>
								<th>Семестр</th>
								<th>Курс</th>
								<th>Вид</th>
								<th>Оценка</th>
								<th>Баллы</th>
							</tr>
							</thead>
							<tbody>
														<? endif; ?>
							<tr>
								<td><?= $key + 1; ?></td>
								<td>
																	<? if ($item['BALS']): ?>
									  <a data-guid="<?= $item['GUID_SUBJECT'] ?>" data-toggle="modal"
									     data-target="#moduleModal"><?= $item['SUBJECT'] ? $item['SUBJECT'] : $item['VIEW']; ?></a>
																	<? else: ?>
									  <span><?= $item['SUBJECT'] ? $item['SUBJECT'] : $item['VIEW']; ?></span>
																	<? endif; ?>
								</td>
								<td><?= $item['PREPOD'] ?></td>
								<td><?= $item['SEMESTR'] ?></td>
								<td><?= $item['COURSE'] ?></td>
								<td><?= $item['VIEW'] ?></td>
								<td><?= $item['VALUE'] ?></td>
								<td><?= $item['BALS'] ?></td>
							</tr>
														<? endforeach; ?>
							</tbody>
						</table>
					</div>
				</div>

			</div>
			<div role="tabpanel" class="tab-pane" id="portfolio">
				<div class="inner_page_content">
					<div class="portfolio-group">
						<div class="panel-group">
													<? $i = 1; ?>
													<? foreach ($arResult['arPortfolio'] as $title => $arItem): ?>
							  <div class="panel panel-default">
								  <div class="panel-heading" data-bootId="collapse<?= $i ?>">
									  <p class="panel-title">
																				<?= $title ?>
									  </p>
								  </div>
								  <div id="collapse<?= $i ?>" class="panel-collapse collapse">
									  <div class="panel-body">
										  <table class="table table-bordered">
																						<? foreach ($arItem as $type => $value): ?>

												<tr>
													<td><?= $type ?></td>
													<td><?= $value ?></td>
												</tr>
																						<? endforeach; ?>
										  </table>
									  </div>
								  </div>
							  </div>
														<? $i++; ?><? endforeach; ?>
													<? $i++; ?>
							<div class="panel panel-default">
								<div class="panel-heading" data-bootId="collapse<?= $i ?>">
									<p class="panel-title">
																			<?= GetMessage('MY_ELEMENTS') ?>
									</p>
								</div>
								<div id="collapse<?= $i ?>" class="panel-collapse collapse in">
									<div class="panel-body"><? $APPLICATION->IncludeComponent(
																			 "bitrix:iblock.element.add",
																			 "portfolio",
																			 array(
																				"AJAX_MODE" => "N",
																				"AJAX_OPTION_ADDITIONAL" => "",
																				"AJAX_OPTION_HISTORY" => "N",
																				"AJAX_OPTION_JUMP" => "Y",
																				"AJAX_OPTION_STYLE" => "Y",
																				"ALLOW_DELETE" => "N",
																				"ALLOW_EDIT" => "Y",
																				"CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
																				"CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
																				"CUSTOM_TITLE_DETAIL_PICTURE" => "",
																				"CUSTOM_TITLE_DETAIL_TEXT" => "",
																				"CUSTOM_TITLE_IBLOCK_SECTION" => "",
																				"CUSTOM_TITLE_NAME" => "Название работы",
																				"CUSTOM_TITLE_PREVIEW_PICTURE" => "",
																				"CUSTOM_TITLE_PREVIEW_TEXT" => "",
																				"CUSTOM_TITLE_TAGS" => "",
																				"DEFAULT_INPUT_SIZE" => "30",
																				"DETAIL_TEXT_USE_HTML_EDITOR" => "N",
																				"ELEMENT_ASSOC" => "CREATED_BY",
																				"GROUPS" => array(
																				 0 => "1",
																				 1 => "3",
																				 2 => "4",
																				 3 => "5",
																				 4 => "6",
																				 5 => "7",
																				 6 => "8",
																				 7 => "9",
																				 8 => "10",
																				 9 => "11",
																				 10 => "12",
																				 11 => "13",
																				 12 => "14",
																				 13 => "15",
																				 14 => "16",
																				 15 => "17",
																				 16 => "18",
																				 17 => "19",
																				 18 => "20",
																				 19 => "21",
																				 20 => "22",
																				),
																				"IBLOCK_ID" => "19",
																				"IBLOCK_TYPE" => "personaloffice",
																				"LEVEL_LAST" => "N",
																				"MAX_FILE_SIZE" => "0",
																				"MAX_LEVELS" => "100000",
																				"MAX_USER_ENTRIES" => "100000",
																				"NAV_ON_PAGE" => "10",
																				"PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
																				"PROPERTY_CODES" => array(
																				 0 => "83",
																				 1 => "84",
																				 2 => "85",
																				 3 => "86",
																				 4 => "87",
																				 5 => "NAME",
																				),
																				"PROPERTY_CODES_REQUIRED" => array(
																				 0 => "83",
																				 1 => "84",
																				 2 => "NAME",
																				),
																				"RESIZE_IMAGES" => "N",
																				"SEF_MODE" => "N",
																				"STATUS" => "ANY",
																				"STATUS_NEW" => "N",
																				"USER_MESSAGE_ADD" => "",
																				"USER_MESSAGE_EDIT" => "",
																				"USE_CAPTCHA" => "N",
																				"COMPONENT_TEMPLATE" => "portfolio"
																			 ),
																			 false
																			);
																			?></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="progress"><?

							if ($arResult['PROGRESS_ONLINE']):?>
				  <div class="">
					  <table class="vedomosti__table">
						  <thead>
						  <tr>
							  <th style="width: 300px;">Дисциплина</th>
							  <th>Вид нагрузки</th>
							  <th>Семестр</th>
							  <th>Курс</th>
							  <th>Преподаватель</th>
						  </tr>
						  </thead>
						  <tbody>
													<? foreach ($arResult['PROGRESS_ONLINE'] as $item): ?>
							  <tr>

								  <td>
									  <a data-guid="<?= $item['НомерВедомости'] ?>" data-toggle="modal"
									     data-target="#discModal" href="#discModal">
																				<?= $item['Дисциплина'] ?>
									  </a>
								  </td>
								  <td><?= $item['ВидНагрузки'] ?></td>
								  <td><?= $item['НомерСеместра'] ?></td>
								  <td><?= $item['НомерКурса'] ?></td>
								  <td></td>
							  </tr>
													<? endforeach; ?>
						  </tbody>
					  </table>
				  </div>
							<? else: ?>
				  <p class="alert-danger">Ведомости отсутствуют</p>
							<? endif; ?>

				<nav style="text-align: center">
					<ul class="pagination">
						<li class="page-item <?= intval($arResult['CUR_PAGIN'] - 1) <= 0 ? ' disabled' : ''; ?>">
							<a class="page-link"
							   href="<?= intval($arResult['CUR_PAGIN'] - 1) <= 0 ? 'javascript:void(0);' : $arResult['CUR_PAGE'] . '?nk=' . intval($arResult['CUR_PAGIN'] - 1) . '#progress' ?>"
							   aria-label="Previous"> <span aria-hidden="true">&laquo;</span> <span class="sr-only">Предыдущая</span>
							</a>
						</li>
											<? for ($i = 1; $i <= $arResult['PAGIN']; $i++):
												$activep = false;
												if ($arResult['CUR_PAGIN'] == $i) {
													$activep = true;
												}
												?>
						  <li class="page-item <?= $activep ? ' active' : ''; ?>">
							  <a class="page-link" href="<?= $arResult['CUR_PAGE'] . '?nk=' . $i ?>#progress">
																<?= $i ?>
							  </a>
						  </li>
											<? endfor; ?>
						<li class="page-item <?= intval($arResult['CUR_PAGIN'] + 1) > $arResult['PAGIN'] ? ' disabled' : ''; ?>">
							<a class="page-link"
							   href="<?= intval($arResult['CUR_PAGIN'] + 1) > $arResult['PAGIN'] ? 'javascript:void(0);' : $arResult['CUR_PAGE'] . '?nk=' . intval($arResult['CUR_PAGIN'] + 1) . '#progress'; ?>"
							   aria-label="Next"> <span aria-hidden="true">&raquo;</span> <span class="sr-only">Следующая</span>
							</a>
						</li>
					</ul>
				</nav>
			</div>
		</div>
	</div>

	<div class="modal" id="moduleModal" tabindex="-1" role="dialog" aria-labelledby="moduleModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close">
						<span class="close-btn" aria-hidden="true">ЗАКРЫТЬ</span></button>
					<h4 class="modal-title" id="myModalLabel">Модули</h4>
				</div>
				<div class="modal-body">
									<?
									foreach ($arResult['PROGRESS']['MODULES'] as $guid => $arItems): ?>
					  <div class="onemodule" id="<?= $guid ?>" style="display: none">
						  <table>
							  <tr>
								  <td>Модуль</td>
								  <td>Баллы</td>
								  <td>Семестр</td>
							  </tr>
														<?
														foreach ($arItems as $item):
															?>
								<tr>
									<td><?= $item['MODULE'] ?></td>
																	<? if ($item["RPD"]['0']["UID_MODULE"]): ?>
									  <td class="points" data-toggle="modal" data-target="#moduleModalRPD"
									      data-titlebm="<?= $item["MODULE"] ?>"
									      data-guidrpd="rpd<?= $guid . $item["RPD"][0]["UID_MODULE"] ?>"><?= $item['POINTS'] ?></td>
																	<? else: ?>
									  <td><?= $item['POINTS'] ?></td>
																	<? endif; ?>
									<td><?= $item['SEMESTR'] ?></td>
								</tr>

														<? endforeach; ?>
						  </table>

					  </div>
									<? endforeach; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="moduleModalRPD" tabindex="-1" role="dialog" aria-labelledby="moduleModalRPD"
	     aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
							aria-hidden="true">×</span></button>
					<h4 class="modal-title" id="titlebm">RPD</h4>
				</div>
				<div class="modal-body">
									<? foreach ($arResult['PROGRESS']['MODULES'] as $guid => $arItems): ?><? foreach ($arItems as $keyi => $arRpd):
										if ($arRpd["RPD"][0]["UID_MODULE"]):
											?>
						<div class="onemodulerpd" id="rpd<?= $guid . $arRpd["RPD"][0]["UID_MODULE"] ?>"
						     style="display: none">
							<table>
								<tr>
									<td>Тема</td>
									<td>Баллы</td>

									<td>MAX</td>
									<td>Семестр</td>
								</tr>
															<? foreach ($arRpd["RPD"] as $key => $rpd): ?>
								  <tr>
									  <td><?= $rpd['TEMA'] ?></td>
									  <td><?= $rpd['TEMA_POINTS'] ?></td>
									  <td><?= $rpd['MAX_POINTS'] ?></td>
									  <td><?= $rpd['SEMESTR'] ?></td>
								  </tr>
															<? endforeach; ?>
							</table>
						</div>
										<? endif; ?><? endforeach; ?><? endforeach; ?>
				</div>
			</div>
		</div>
	</div>

<? if ($arResult['PROGRESS_ONLINE']): ?>
	<div class="modal" id="discModal" tabindex="-1" role="dialog" aria-labelledby="discModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close-btn"
					                                                                                  aria-hidden="true">ЗАКРЫТЬ</span>
					</button>
					<h4 class="modal-title" id="myModalLabelPg"></h4>
					<div class="row">
						<ul class="col-md-6">
							<li>Номер ведомости: <span id="numberVed"></span></li>
							<li>Дата ведомости: <span id="dateVed"></span></li>
							<li>Учебный год: <span id="yearLern"></span></li>
							<li>Семестр: <span id="semestr"></span></li>
						</ul>
						<ul class="col-md-6">
							<li>Направление: <span id="spec"></span></li>
							<li>Форма обучения: <span id="formlern"></span></li>
							<li>Курс: <span id="curse"></span></li>
							<li>Год набора: <span id="nabor"></span></li>
						</ul>
					</div>

				</div>
				<div class="modal-body">
									<?
									foreach ($arResult['PROGRESS_ONLINE'] as $arItems):

										$date = DateTime::createFromFormat('Y-m-d', $arItems['ДатаВедомости']);
										//$arJSvedomost[$arItems['НомерВедомости']]['dateVed'] = $date->format('d/m/Y');
										$arJSvedomost[$arItems['НомерВедомости']]['dateVed'] = date('d/m/Y', $date);
										$arJSvedomost[$arItems['НомерВедомости']]['numberVed'] = $arItems['НомерВедомости'];
										$arJSvedomost[$arItems['НомерВедомости']]['yearLern'] = $arItems['УчебныйГод'] ? $arItems['УчебныйГод'] : false;
										$arJSvedomost[$arItems['НомерВедомости']]['semestr'] = $arItems['НомерСеместра'] ? $arItems['НомерСеместра'] : false;
										$arJSvedomost[$arItems['НомерВедомости']]['spec'] = $arItems['Специальность'] ? $arItems['Специальность'] : false;
										$arJSvedomost[$arItems['НомерВедомости']]['formlern'] = $arItems['ФормаОбучения'] ? $arItems['ФормаОбучения'] : false;
										$arJSvedomost[$arItems['НомерВедомости']]['curse'] = $arItems['НомерКурса'] ? $arItems['НомерКурса'] : false;
										$arJSvedomost[$arItems['НомерВедомости']]['nabor'] = $arItems['ГодНабора'] ? $arItems['ГодНабора'] : false;
										?>
					  <div class="onemodule" id="modal<?= $arItems['НомерВедомости'] ?>" style="display: none">
						  <table>
							  <tr>
								  <td>Студент/Модуль</td>
																<? foreach ($arItems['РабочаяПрограммаДисциплины']['СписокМодулей'] as $arModule): ?>
									<td><a href="#moduleBm"
									       data-guid="<?= $arModule['УИД'] . $arItems['НомерВедомости'] ?>"
									       data-toggle="modal"
									       data-target="#moduleBm"><?= $arModule['Наименование'] ?></a></td>
																<? endforeach; ?>
								  <td>Итого</td>
							  </tr>
														<? foreach ($arItems['СписокСтудентов'] as $student): ?>
								<tr>
									<td><?= $student ['ФИО'] ?></td>
																	<? $maxball = 0; ?>
																	<? foreach ($arItems['РабочаяПрограммаДисциплины']['СписокМодулей'] as $arModule):

																		$arIDlections = [];
																		foreach ($arModule['СписокТем'] as $arTema) {
																			foreach ($arTema['СписокЗанятий'] as $lections) {
																				$arIDlections[] = $lections['УИД'];
																			}
																		}
																		$ball = 0;
																		foreach ($arItems['СписокОценок'] as $arBall) {
																			if ($arBall['УИДСтудента'] === $student['УИД'] && in_array($arBall['УИДЗанятия'], $arIDlections)) {
																				$ball += intval($arBall['Оценка']);
																			}
																		}
																		foreach ($arItems['СписокОценокДоработки'] as $arBallDop) {
																			if ($arBallDop['УИДСтудента'] === $student['УИД'] && $arBallDop['УИДМодуля'] === $arModule['УИД']) {
																				$ball += intval($arBallDop['Оценка']);
																			}
																		}
																		?>
									  <td><?= $ball . '/' . $arModule['МаксимальныйБалл'] ?></td>
																		<? $maxball += $arModule['МаксимальныйБалл']; ?><? endforeach; ?>
																	<?
																	$itog = 0;
																	foreach ($arItems['СписокОценок'] as $arBall) {
																		if ($arBall['УИДСтудента'] === $student['УИД']) {
																			$itog += intval($arBall['Оценка']);
																		}
																	}
																	foreach ($arItems['СписокОценокДоработки'] as $arBallDop) {
																		if ($arBallDop['УИДСтудента'] === $student['УИД']) {
																			$itog += intval($arBallDop['Оценка']);
																		}
																	}
																	?>
									<td><?= $itog . '/' . $maxball ?></td>
								</tr>
														<? endforeach; ?>
						  </table>
					  </div>
									<? endforeach; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="moduleBm" tabindex="-1" role="dialog" aria-labelledby="moduleBm" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content table-responsive" style="overflow-y: hidden">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close-btn"
					                                                                                  aria-hidden="true">ЗАКРЫТЬ</span>
					</button>
					<h4 class="modal-title" id="myModalLabelBm"></h4>
				</div>
				<div class="modal-body">
									<? foreach ($arResult['PROGRESS_ONLINE'] as $arItems): ?><? foreach ($arItems['РабочаяПрограммаДисциплины']['СписокМодулей'] as $arModule): ?>
					  <div class="onemodule" id="bm<?= $arModule['УИД'] . $arItems['НомерВедомости'] ?>"
					       style="display: none">
						  <div class="jsFormUpdate" data-nv="<?= $arItems['НомерВедомости']; ?>">
							  <table id="$arModule['УИД']" class="table table-striped"
							         data-nved="<?= $arItems['НомерВедомости'] ?>"
							         data-uidmodule="<?= $arModule['УИД'] ?>">
								  <tr>
									  <td style="min-width: 200px;">Студент/Тема</td>
																		<? foreach ($arModule['СписокТем'] as $arTem): ?>
										<td colspan="<?= count($arTem['СписокЗанятий']) ?>"><?= $arTem['Наименование'] ?></td>
																		<? endforeach; ?>
									  <td>Доработка</td>
									  <td style="min-width: 70px;">Итог</td>
								  </tr>
																<? foreach ($arItems['СписокСтудентов'] as $student): ?>
									<tr data-uidstudent="<?= $student['УИД'] ?>">

										<td class="tdname">
																					<?= $student ['ФИО'] ?>
										</td>
																			<?
																			$itog = 0;
																		 foreach ($arModule['СписокТем'] as $arTema):
																				$ball = 0;
																				$itogPoTem = 0;
																				foreach ($arTema['СписокЗанятий'] as $nL => $lections):
																					$arIDlections = $lections['УИД'];
																					$maxball = $arModule['МаксимальныйБалл'];
																					foreach ($arItems['СписокОценок'] as $arBall):
																						if ($arBall['УИДСтудента'] === $student['УИД'] && $lections['УИД'] === $arBall['УИДЗанятия']):
																							$ball = intval($arBall['Оценка']);
																							$itog += intval($arBall['Оценка']);
																							$itogPoTem += intval($arBall['Оценка']);
																							if ($ball === 0 && $arBall['ПропустилЗанятие']) {
																								$ball = 'Н';
																							}
																							$uidLesson = $arBall['УИДЗанятия'];
																							if (count($arTema['СписокЗанятий']) == 1) {
																								$maxb = $arTema['БаллПоТеме'];
																							} elseif (count($arTema['СписокЗанятий']) > 1) {
																								if ($nL == 0) {
																									$maxb = $arTema['БаллПоТеме'];
																								} else {
																									$maxb = $arTema['БаллПоТеме'] - $itogPoTem;
																									if ($maxb < 0) {
																										$maxb = 0;
																									}
																									if ($maxb < $ball) {
																										$maxb = $ball;
																									}
																								}

																							}
																							?>
												<td>
												<input id="<?= $student['УИД'] . $uidLesson ?>" value="<?= $ball ?>"
												       readonly type="text">
												</td><? endif; ?><?
																					endforeach;
																				endforeach;
																			endforeach;
																			foreach ($arItems['СписокОценокДоработки'] as $arBallDop):
																				if ($arBallDop['УИДСтудента'] === $student['УИД'] && $arBallDop['УИДМодуля'] === $arModule['УИД']):
																					$DopBall = intval($arBallDop['Оценка']);
																					$itog += intval($arBallDop['Оценка']); ?>
											<td>
											<input id="<?= $student['УИД'] . $arModule['УИД'] ?>" type="text" readonly
											       value="<?= $DopBall ?>">
											</td><?
																				endif;
																			endforeach;


																			?>
										<td>
											<span
												id="itog-<?= $student['УИД'] . $arModule['УИД'] ?>"> <?= $itog ?></span><?= '/' . $arModule['МаксимальныйБалл']; ?>
										</td>

									</tr>
																<? endforeach; ?>
							  </table>
						  </div>
					  </div>
									<? endforeach; ?><? endforeach; ?>
				</div>
			</div>
		</div>
	</div>
<? endif; ?>
	<script>
      var arResult = {};
      arResult.vedomosti = '<?=json_encode($arJSvedomost);?>';
	</script>
<?
if (IsModuleInstalled("im") && CBXFeatures::IsFeatureEnabled('WebMessenger')) {
	$APPLICATION->IncludeComponent("glab:im.messenger", "", [], false);
}
?>