$(function () {
//date
    $(".btnprev, .btnnext, input.datems").change(function () {
        $('.form-loader').fadeIn(300);
        var curDate = $(this).val().replace(/\./g, "-");
        console.log(curDate);
        var data = {
            "curDate": curDate
        };
        $.ajax({
            type: 'POST',
            url: '',
            data: data,
            success: function (res) {
                res = $.parseJSON(res);
                // window.location.href = url;
                window.location.href = window.location.origin + window.location.pathname + '?datereq=' + res.monday;
            }
        });
    });
    $(".btnprev, .btnnext").click(function () {
        $('.form-loader').fadeIn(300);
        var curDate = $(this).val().replace(/\./g, "-");
        console.log(curDate);
        var data = {
            "curDate": curDate
        };
        $.ajax({
            type: 'POST',
            url: '',
            data: data,
            success: function (res) {
                res = $.parseJSON(res);
                // window.location.href = url;
                window.location.href = window.location.origin + window.location.pathname + '?datereq=' + res.monday;
            }
        });
    });
//modal
    $('a[data-target="#moduleModal"]').click(function () {
        var guidSubject = $(this).data("guid");
        $("#" + guidSubject).show();

    });
    $('#moduleModal').on('hide.bs.modal', function (event) {
        $('.onemodule').each(function () {
            $(this).hide();
        });
    });

    $('[data-target="#moduleModalRPD"]').click(function () {
        var guidSubjectrpd = $(this).data("guidrpd");
        var titlebm = $(this).data("titlebm");
        $("#" + guidSubjectrpd).show();
        $("#titlebm").text(titlebm);

    });
    $('#moduleModalRPD').on('hide.bs.modal', function (event) {
        $('.onemodulerpd').each(function () {
            $(this).hide();
        });
    });
// табы при перезагрузке
    $(document).ready(function () {

        if (location.hash) {
            $("a[href='" + location.hash + "']").tab("show");
        }
        $(document.body).on("click", "a[data-toggle]", function (event) {
            location.hash = this.getAttribute("href");
        });
    });
    $(window).on("popstate", function () {
        var anchor = location.hash || $("a[data-toggle='tab']").first().attr("href");
        $("a[href='" + anchor + "']").tab("show");
    });
    //акардеон в портфолио
    $('.panel-heading').click(function () {
        var id = $(this).attr('data-bootId');
        $('#' + id).collapse('toggle');
    });
    //успеваемость

    if (arResult.vedomosti) {
        arResult.vedomosti = JSON.parse(arResult.vedomosti);
    }

    $('a[data-target="#discModal"]').click(function () {

        var guidSubject = $(this).data("guid");
        $("#modal" + guidSubject).show();
        $("#myModalLabelPg").html($(this).text());
        if (arResult.vedomosti[guidSubject].dateVed) {
            $("#dateVed").html(arResult.vedomosti[guidSubject].dateVed);
        }
        if (arResult.vedomosti[guidSubject].numberVed) {
            $("#numberVed").html(arResult.vedomosti[guidSubject].numberVed);
        }
        if (arResult.vedomosti[guidSubject].yearLern) {
            $("#yearLern").html(arResult.vedomosti[guidSubject].yearLern);
        }
        if (arResult.vedomosti[guidSubject].semestr) {
            $("#semestr").html(arResult.vedomosti[guidSubject].semestr);
        }
        if (arResult.vedomosti[guidSubject].spec) {
            $("#spec").html(arResult.vedomosti[guidSubject].spec);
        }
        if (arResult.vedomosti[guidSubject].formlern) {
            $("#formlern").html(arResult.vedomosti[guidSubject].formlern);
        }
        if (arResult.vedomosti[guidSubject].curse) {
            $("#curse").html(arResult.vedomosti[guidSubject].curse);
        }
        if (arResult.vedomosti[guidSubject].nabor) {
            $("#nabor").html(arResult.vedomosti[guidSubject].nabor);
        }
    });
    $('#discModal').on('hide.bs.modal', function (event) {
        $('#discModal .onemodule').each(function () {
            $(this).hide();
        });

    });

    $('a[data-target="#moduleBm"]').click(function () {
        var guidSubject = $(this).data("guid");
        $("#bm" + guidSubject).show();
        $("#myModalLabelBm").html($(this).text());
    });
    $('#moduleBm').on('hide.bs.modal', function (event) {
        $('#moduleBm .onemodule:visible').hide();
    });

    $('ul.pagination li:not(.disabled)').click(function () {
        $('.form-loader').fadeIn(300);
    })

});
$(window).load(function () {
    $('.form-loader').fadeOut(300);
});
