<?
/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @var CBitrixComponent $this
 * @var CBitrixComponent $arParams
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();

$this->setFrameMode(false);
define("NO_KEEP_STATISTIC", true);
CJSCore::Init(array('popup', 'date'));

$context = \Bitrix\Main\Context::getCurrent();
$request = $context->getRequest();
$prepod = new Prepod();

$arResult["ID"] = intval($USER->GetID());
$arResult["PREPOD_INFO"] = $prepod->getPrepodInfo();

if (!$arResult["PREPOD_INFO"]["LOGIN"] || $arResult["ID"] <= 0) {
	if ($arResult["ID"] > 0) {
		echo "<p class='alert alert-success'>Ошибка значения login " . $arResult["PREPOD_INFO"]["LOGIN"] . "</p>";
		global $USER;
		$USER->Logout();
	}
	$APPLICATION->ShowAuthForm("");
	return;
}
$arResult["GROUP_POLICY"] = CUser::GetGroupPolicy($arResult["ID"]);

function processAjax()
{
	$context = \Bitrix\Main\Context::getCurrent();
	$request = $context->getRequest();
	if ($request->isAjaxRequest() && $request->get('typeaction') === 'allball') {
		global $APPLICATION;
		$APPLICATION->RestartBuffer();
		$arballs = $request->get('arballs');
		$arDopBalls = $request->get('arDopBalls');
		$nved = $request->get('nved');
		$date = date('c', time());
		foreach ($arballs as &$ball) {
			$ball['ДатаОценки'] = $date;
		}
		foreach ($arDopBalls as &$dopball) {
			$dopball['ДатаОценки'] = $date;
		}
		if (!empty($nved)) {
			$result = Prepod::updateAllBall($nved, ['СтрокаОценок' => $arballs, 'СтрокаОценокДоработки' => $arDopBalls]);
		} else {
			$result['ОценкаНеБылаОбновлена'] = 1;
			$result['ТекстОшибки'] = 'Пустой параметр запроса';
		}
		echo json_encode($result);
		exit;
	}
	if ($request->isAjaxRequest() && $request->get('curDate')) {
		global $APPLICATION;
		$APPLICATION->RestartBuffer();
		$now = date('c');
		if ($request->get('curDate')) {
			$now = $request->get('curDate');
		}
		$obDate = new Lk();
		$reult = $obDate::dateToMS($now, '%d.%m.%Y');
		echo json_encode($reult);
		exit;
	}
}

$cachePath = '/';
$itemInpage = 5;
$page = intval($request->get('page'));
if ($page < 1) {
	$page = 1;
}
if ($request->get('datereq')) {
	$datereq = Lk::dateToDay($request->get('datereq'), '%Y-%m-%d');
	$datereqID = Lk::dateToDay($request->get('datereq'), '%Y-%m-%d');
} else {
	$datereq = date('c');
}
//расписание
$obCache = new CPHPCache;
$lifeTime = $arParams['CACHE_TIME'];
$lifeTimeVed = $lifeTime;
if (!$datereqID) {
	$datereqID = date('Ymd');
}
$cacheIDSCHEDULE = 'cach' . strval($datereqID) . strval($USER->GetID());
$cacheID = 'cach' . strval($USER->GetID());
if ($obCache->InitCache($lifeTime, $cacheIDSCHEDULE, $cachePath)) {
	$arResult = $obCache->GetVars();
} elseif ($obCache->StartDataCache($lifeTime, $cacheIDSCHEDULE, $cachePath)) {
	$arDate = Lk::dateToMS($datereq, '%d.%m.%Y');
	$arResult["SCHEDULE"]['DATE_START'] = $arDate['monday'];
	$arResult["SCHEDULE"]['DATE_END'] = $arDate['sunday'];
	$arResult["SCHEDULE"]['SYS_DATE_NEXT'] = Lk::dateToDay($datereq, '%Y-%m-%d', '+1 week');
	$arResult["SCHEDULE"]['SYS_DATE_PREV'] = Lk::dateToDay($datereq, '%Y-%m-%d', '1 week ago');

	$entityDataClassSh = Lk::GetEntityDataClass(HL_SHEDULE_ID);
	$entityDataClassGr = Lk::GetEntityDataClass(HL_GROUPS_ID);

	$shOb = $entityDataClassSh::getList(['select' => ['*'], 'filter' => [
		'LOGIC' => 'AND',
		[
			'>=UF_DAY' => $arDate['monday'],
			'=UF_PREPOD_LOGIN' => trim($arResult["PREPOD_INFO"]['LOGIN']),
		],
		[
			'<=UF_DAY' => $arDate['sunday'],
			'=UF_PREPOD_LOGIN' => trim($arResult["PREPOD_INFO"]['LOGIN']),
		]
	]]);
	while ($shRes = $shOb->fetch()) {
		$groupNames = [];
		if (!empty($shRes['UF_LNK_GROUPS']) && is_array($shRes['UF_LNK_GROUPS']))
			foreach ($shRes['UF_LNK_GROUPS'] as $gId) {
				$groupNames[] = $entityDataClassGr::getList(['select' => ['UF_NAME'], 'filter' => ['ID' => intval($gId)]])->fetch()['UF_NAME'];

			}
		$groupNames = implode(' ',$groupNames);

		$arResult['ITEMS'][$shRes['UF_N_PARA']][$shRes['UF_DAY']->format('N')][] = array(
			'date' => $shRes['UF_DAY']->format('d.m.Y'),
			'ndate' => $shRes['UF_DAY']->format('N'),
			'timeStart' => $shRes['UF_START'],
			'timeEnd' => $shRes['UF_END'],
			'subject' => $shRes['UF_DISCIPLIN'],
			'place' => $shRes['UF_AUDIT'],
			'view' => $shRes['UF_TYPE'],
			'korpus' => $shRes['UF_KORPUS'],
			'teacher' => $shRes['UF_PREPOD'],
			'group' => $groupNames,
		);
		$arResult['RINGS'][$shRes['UF_N_PARA']] = $shRes['UF_START'] . '-' . $shRes['UF_END'];
	}

	$obCache->EndDataCache($arResult);
}
//ведомости

$tempResult = Prepod::getVedomostiForPrepod($arResult["ID"]);
if ($tempResult['НомерВедомости']) {
	$tempResult = [$tempResult];
}
$arResult['PAGIN'] = intval(ceil(count($tempResult) / $itemInpage));
$arResult['CUR_PAGIN'] = $page;
$arResult['CUR_PAGE'] = $APPLICATION->GetCurPage();

$cacheIDVED = 'cach' . strval($USER->GetID()) . $page . $itemInpage;
//сброс кеша при сохранении ведомости
if ($request->get('nved')) {
	$lifeTimeVed = 1;
}
if ($obCache->InitCache($lifeTimeVed, $cacheIDVED, $cachePath)) {
	$arResult["Ведомости"] = $obCache->GetVars();
} elseif ($obCache->StartDataCache($lifeTimeVed, $cacheIDVED, $cachePath)) {

	$arResult["Ведомости"] = $prepod->getVedomostRpdPagin($page, $itemInpage, $tempResult);
	if ($arResult["Ведомости"]['НомерВедомости']) {
		$arResult["Ведомости"] = [$arResult["Ведомости"]];
	}

	foreach ($arResult["Ведомости"] as &$arVed) {
		if ($arVed['СписокСтудентов']['УИД']) {
			$arVed['СписокСтудентов'] = [$arVed['СписокСтудентов']];
		}
		if ($arVed['СписокОценок']['УИДСтудента']) {
			$arVed['СписокОценок'] = [$arVed['СписокОценок']];
		}

		if ($arVed['РабочаяПрограммаДисциплины']['СписокМодулей']['УИД']) {
			$arVed['РабочаяПрограммаДисциплины']['СписокМодулей'] = [$arVed['РабочаяПрограммаДисциплины']['СписокМодулей']];
		}
		foreach ($arVed['РабочаяПрограммаДисциплины']['СписокМодулей'] as &$arModule) {
			if ($arModule['СписокТем']['УИД']) {
				$arModule['СписокТем'] = [$arModule['СписокТем']];
			}
			foreach ($arModule['СписокТем'] as &$arTem) {
				if ($arTem['СписокЗанятий']['УИД']) {
					$arTem['СписокЗанятий'] = [$arTem['СписокЗанятий']];
				}
			}
		}
	}
	if (!($arResult["Ведомости"][0])) {
		$arResult["Ведомости"] = false;
	}

	$obCache->EndDataCache($arResult["Ведомости"]);
}

processAjax();
$this->IncludeComponentTemplate();