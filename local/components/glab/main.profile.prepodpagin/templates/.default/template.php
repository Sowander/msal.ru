<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
use Bitrix\Main\Page\Asset;

Asset::getInstance()->addString("<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/bootstrap.min.css");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/bootstrap.min.js");
$ruWeek = array('Понедельник', 'Вторник', 'Среда', 'Четверг', 'Пятница', 'Суббота', 'Воскресенье');
?>

<div>
	<br>
	<div class="prepod-info">
		<div class="col-md-6" style="padding:0;">
			<h3><?= $arResult['PREPOD_INFO']['FIO']; ?></h3>
		</div>
		<div class="col-md-6">
					<? if ($arResult["STUDENT"]["PERSONAL_PHOTO"]): ?>
			  <img class="student-img" src="<?= $arResult["PREPOD_INFO"]["PERSONAL_PHOTO"] ?>" alt="">
					<? endif; ?>
		</div>
	</div>
	<br> <br>
	<!-- Навигация -->
	<ul class="nav nav-tabs" role="tablist">
		<li class="active">
			<a class="thome" href="#home" aria-controls="home" role="tab" data-toggle="tab">Электронный журнал
				успеваемости</a>
		</li>
		<li class="prepod-shedule">
			<a class="thome" href="#shedule" aria-controls="shedule" role="tab" data-toggle="tab">Расписание</a>
		</li>
		<li>
			<a onclick="$('.form-loader').fadeIn(100);"
			   href="<?= 'http://' . $_SERVER['HTTP_HOST'] . substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], '?')) . '?logout=yes' ?>">Выход</a>
		</li>
	</ul>
	<!-- Содержимое вкладок -->
	<div class="tab-content">
		<div role="tabpanel" class="vedomosti tab-pane active" id="home">
					<?

					if ($arResult["Ведомости"]):?>
			  <div class="">
				  <table class="vedomosti__table">
				  <thead>

				  <tr>
				  <th>Дисциплина</th>
				  <th>Институт</th>
				  <th>Группа</th>
				  <th>Вид нагрузки</th>
				  </tr>
				  </thead>
				  <tbody>
									<? foreach ($arResult["Ведомости"] as $item): ?>
					  <tr>

					  <td>
					  <a data-guid="<?= $item['НомерВедомости'] ?>" data-toggle="modal" data-target="#discModal"
						 href="#discModal">
												<?= $item['Дисциплина'] ?>
					  </a>
					  </td>
					  <td><?= $item['Институт'] ?></td>
					  <td><?= $item['ИмяГруппы'] ?></td>
					  <td><?= $item['ВидНагрузки'] ?></td>
					  </tr>
									<? endforeach; ?>
				  </tbody>
				  </table>
				  <nav style="text-align: center">
					  <ul class="pagination">
						  <li class="page-item <?= intval($arResult['CUR_PAGIN'] - 1) <= 0 ? ' disabled' : ''; ?>">
							  <a class="page-link"
								 href="<?= intval($arResult['CUR_PAGIN'] - 1) <= 0 ? 'javascript:void(0);' : $arResult['CUR_PAGE'] . '?page=' . intval($arResult['CUR_PAGIN'] - 1) ?>"
								 aria-label="Previous"> <span aria-hidden="true">&laquo;</span> <span class="sr-only">Предыдущая</span>
							  </a>
						  </li>
												<? for ($i = 1; $i <= $arResult['PAGIN']; $i++):
													$activep = false;
													if ($arResult['CUR_PAGIN'] == $i) {
														$activep = true;
													}
													?>
							<li class="page-item <?= $activep ? ' active' : ''; ?>">
								<a class="page-link" href="<?= $arResult['CUR_PAGE'] . '?page=' . $i ?>"><?= $i ?></a>
							</li>
												<? endfor; ?>
						  <li class="page-item <?= intval($arResult['CUR_PAGIN'] + 1) > $arResult['PAGIN'] ? ' disabled' : ''; ?>">
							  <a class="page-link"
								 href="<?= intval($arResult['CUR_PAGIN'] + 1) > $arResult['PAGIN'] ? 'javascript:void(0);' : $arResult['CUR_PAGE'] . '?page=' . intval($arResult['CUR_PAGIN'] + 1); ?>"
								 aria-label="Next"> <span aria-hidden="true">&raquo;</span> <span class="sr-only">Следующая</span>
							  </a>
						  </li>
					  </ul>
				  </nav>
			  </div>
					<? else: ?>
			  <p class="alert-danger">Ведомости отсутствуют</p>
					<? endif; ?>
		</div>
		<div role="tabpanel" class="tab-pane" id="shedule">
			<div class="prep-scheule__btn">
				<button type="text" class="btn btn-default btnprev"
						value="<?= $arResult['SCHEDULE']['SYS_DATE_PREV'] ?>">Пред. Неделя
				</button>
				<input type="text" class="datems"
					   value="<?= $arResult['SCHEDULE']['DATE_START'] . " - " . $arResult['SCHEDULE']['DATE_END']; ?>"
					   name="date" onclick="BX.calendar({node: this, field: this, bTime: false});">
				<button type="text" class="btn btn-default btnnext"
						value="<?= $arResult['SCHEDULE']['SYS_DATE_NEXT'] ?>">След. Неделя
				</button>
			</div>
			<div class="">
				<table class="prepod_schedule__table">
				<thead>
				<tr>
				<th>№</th>
				<th>Время</th>
								<? foreach ($ruWeek as $day): ?>
					<th><?= $day ?></th>
								<? endforeach; ?>
				</tr>
				</thead>
				<tbody>
								<? $rows = 8; // количество строк, tr
								$cols = 9;
								for ($tr = 1; $tr <= $rows; $tr++):?><tr>
									<? for ($td = 1; $td <= $cols; $td++):
										if ($td != 1 && $td != 2) {
											echo '<td>';
											echo '<p class="schedule__name"><b>' . $arResult['ITEMS'][$tr][$td - 2][0]['subject'] . '</b></p>';
											echo $arResult['ITEMS'][$tr][$td - 2][0]['group'] . '<br>';
											echo $arResult['ITEMS'][$tr][$td - 2][0]['korpus'] . '<br>';
											echo '<span style="font-weight: bold;">' . $arResult['ITEMS'][$tr][$td - 2][0]['place'] . '</span><br>';
											echo '</td>';

										} elseif ($td === 2) {
											echo '<td>';
											echo $arResult['RINGS'][$tr];
											echo '</td>';
										} elseif ($td === 1) {
											echo '<td>';
											echo $arResult['RINGS'][$tr] ? $tr : '';
											echo '</td>';
										}
										?>

									<? endfor; ?>
									<?= '</tr>' ?><? endfor; ?>
				</tbody>
				</table>
			</div>
		</div>
	</div>
</div>

<? if ($arResult["Ведомости"]): ?>
	<div class="modal" id="discModal" tabindex="-1" role="dialog" aria-labelledby="discModal" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close-btn"
																									  aria-hidden="true">ЗАКРЫТЬ</span>
					</button>
					<h4 class="modal-title" id="myModalLabel"></h4>
					<ul class="col-md-6">
						<li>Номер ведомости: <span id="numberVed"></span></li>
						<li>Дата ведомости: <span id="dateVed"></span></li>
						<li>Учебный год: <span id="yearLern"></span></li>
						<li>Семестр: <span id="semestr"></span></li>
					</ul>
					<ul class="col-md-6">
						<li>Направление: <span id="spec"></span></li>
						<li>Форма обучения: <span id="formlern"></span></li>
						<li>Курс: <span id="curse"></span></li>
						<li>Год набора: <span id="nabor"></span></li>
					</ul>

				</div>
				<div class="modal-body">
									<?
									foreach ($arResult["Ведомости"] as $arItems):

										$date = DateTime::createFromFormat('Y-m-d', $arItems['ДатаВедомости']);
										//$arJSvedomost[$arItems['НомерВедомости']]['dateVed'] = $date->format('d/m/Y');
										$arJSvedomost[$arItems['НомерВедомости']]['dateVed'] = date('d/m/Y', $date);
										$arJSvedomost[$arItems['НомерВедомости']]['numberVed'] = $arItems['НомерВедомости'];
										$arJSvedomost[$arItems['НомерВедомости']]['yearLern'] = $arItems['УчебныйГод'] ? $arItems['УчебныйГод'] : false;
										$arJSvedomost[$arItems['НомерВедомости']]['semestr'] = $arItems['НомерСеместра'] ? $arItems['НомерСеместра'] : false;
										$arJSvedomost[$arItems['НомерВедомости']]['spec'] = $arItems['Специальность'] ? $arItems['Специальность'] : false;
										$arJSvedomost[$arItems['НомерВедомости']]['formlern'] = $arItems['ФормаОбучения'] ? $arItems['ФормаОбучения'] : false;
										$arJSvedomost[$arItems['НомерВедомости']]['curse'] = $arItems['НомерКурса'] ? $arItems['НомерКурса'] : false;
										$arJSvedomost[$arItems['НомерВедомости']]['nabor'] = $arItems['ГодНабора'] ? $arItems['ГодНабора'] : false;
										?>
				  <div class="onemodule" id="modal<?= $arItems['НомерВедомости'] ?>" style="display: none">
					  <table>
					  <tr>
					  <td>Студент/Модуль</td><?
											foreach ($arItems['РабочаяПрограммаДисциплины']['СписокМодулей'] as $arModule):
												?>
						  <td><a href="#moduleBm" data-guid="<?= $arModule['УИД'] . $arItems['НомерВедомости'] ?>"
								 data-toggle="modal" data-target="#moduleBm"><?= $arModule['Наименование'] ?></a></td><?
											endforeach;
											?>
					  <td>Итого</td>
					  </tr><?
											foreach ($arItems['СписокСтудентов'] as $student):
												?>
						  <tr>
					  <td><?= $student ['ФИО'] ?></td><? $maxball = 0;
											foreach ($arItems['РабочаяПрограммаДисциплины']['СписокМодулей'] as $arModule):
												$arIDlections = [];
												foreach ($arModule['СписокТем'] as $arTema) {
													foreach ($arTema['СписокЗанятий'] as $lections) {
														$arIDlections[] = $lections['УИД'];
													}
												}
												$ball = 0;
												foreach ($arItems['СписокОценок'] as $arBall) {
													if ($arBall['УИДСтудента'] === $student['УИД'] && in_array($arBall['УИДЗанятия'], $arIDlections)) {
														$ball += intval($arBall['Оценка']);
													}
												}
												foreach ($arItems['СписокОценокДоработки'] as $arBallDop) {
													if ($arBallDop['УИДСтудента'] === $student['УИД'] && $arBallDop['УИДМодуля'] === $arModule['УИД']) {
														$ball += intval($arBallDop['Оценка']);
													}
												}
												?>
						  <td><?= $ball . '/' . $arModule['МаксимальныйБалл'] ?></td><?
												$maxball += $arModule['МаксимальныйБалл'];
											endforeach;
											$itog = 0;
											foreach ($arItems['СписокОценок'] as $arBall) {
												if ($arBall['УИДСтудента'] === $student['УИД']) {
													$itog += intval($arBall['Оценка']);
												}
											}
											foreach ($arItems['СписокОценокДоработки'] as $arBallDop) {
												if ($arBallDop['УИДСтудента'] === $student['УИД']) {
													$itog += intval($arBallDop['Оценка']);
												}
											}

											?>
					  <td><?= $itog . '/' . $maxball ?></td></tr><?
											endforeach; ?></table></div><?
									endforeach; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="modal" id="moduleBm" tabindex="-1" role="dialog" aria-labelledby="moduleBm" aria-hidden="true">
	<div class="modal-dialog">
		<div class="wrapper1">
			<div class="div1"></div>
		</div>
		<div class="modal-content table-responsive" style="overflow-y: hidden">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span class="close-btn"
																								  aria-hidden="true">ЗАКРЫТЬ</span>
				</button>

				<h4 class="modal-title" id="myModalLabelBm"></h4>
			</div>
			<div class="modal-body">
							<? foreach ($arResult["Ведомости"] as $arItems): ?><? foreach ($arItems['РабочаяПрограммаДисциплины']['СписокМодулей'] as $arModule): ?>
			  <div class="onemodule" id="bm<?= $arModule['УИД'] . $arItems['НомерВедомости'] ?>" style="display: none">

				  <form action="" class="jsFormUpdate" data-nv="<?= $arItems['НомерВедомости']; ?>">
					  <input type="submit" value="Сохранить" class="js-button js-save btn btn-success">
					  <table id="$arModule['УИД']" class="table table-striped"
							 data-nved="<?= $arItems['НомерВедомости'] ?>" data-uidmodule="<?= $arModule['УИД'] ?>">
					  <thead>
					  <tr class="trbmheader">
					  <td style="min-width: 200px">Студент/Тема</td><?
											foreach ($arModule['СписокТем'] as $arTem):
												?>
						  <td class="bmheader"
							  colspan="<?= count($arTem['СписокЗанятий']); ?>">
											  <div style="max-height: 100px; overflow: hidden; text-overflow: ellipsis"><?= trim($arTem['Наименование']); ?></div>
												</td><?
											endforeach; ?>
					  <td class="">Доработка</td>
					  <td class="" style="min-width: 70px;">Итог</td>
					  </tr> </thead><?
											foreach ($arItems['СписокСтудентов'] as $student):
												?>
						  <tr data-uidstudent="<?= $student['УИД'] ?>">
					  <td class="tdname"><?= $student ['ФИО'] ?></td><?
											$itog = 0;
											foreach ($arModule['СписокТем'] as $arTema):
												$ball = 0;
												$itogPoTem = 0;
												foreach ($arTema['СписокЗанятий'] as $nL => $lections):
													$arIDlections = $lections['УИД'];

													$maxball = $arModule['МаксимальныйБалл'];
													foreach ($arItems['СписокОценок'] as $arBall):
														if ($arBall['УИДСтудента'] === $student['УИД'] && $lections['УИД'] === $arBall['УИДЗанятия']):
															$ball = intval($arBall['Оценка']);
															$itog += intval($arBall['Оценка']);
															$itogPoTem += intval($arBall['Оценка']);
															if ($ball === 0 && $arBall['ПропустилЗанятие']) {
																$ball = 'Н';
															}
															$uidLesson = $arBall['УИДЗанятия'];
															if (count($arTema['СписокЗанятий']) == 1) {
																$maxb = $arTema['БаллПоТеме'];
															} elseif (count($arTema['СписокЗанятий']) > 1) {
																if ($nL == 0) {
																	$maxb = $arTema['БаллПоТеме'];
																} else {
																	$maxb = $arTema['БаллПоТеме'] - $itogPoTem;
																	if ($maxb < 0) {
																		$maxb = 0;
																	}
																	if ($maxb < $ball) {
																		$maxb = $ball;
																	}
																}

															}

															?>
								<td>
							<input id="<?= $student['УИД'] . $uidLesson ?>"
								   data-id="<?= $student['УИД'] . $uidLesson ?>" data-uidlesson="<?= $uidLesson ?>"
								   data-maxtemball="<?= $arTema['БаллПоТеме'] ?>"
								   data-studentuid="<?= $student['УИД'] ?>" data-itogpoteme="<?= $itogPoTem ?>"
								   data-nl="<?= $nL ?>" name="bal-<?= $student['УИД'] ?>" value="<?= $ball ?>"
								   autocomplete="off" class="js-ball" min="0" max="<?= abs($maxb) ?>" step="1"
														       <? if ($ball !== 'Н'): ?>type="number" <? else: ?>type="text" value="Н" <? endif; ?>><label
								id="out-<?= $student['УИД'] . $uidLesson ?>" class="js-out"><input type="checkbox"
																								   value="1" <?= $arBall['ПропустилЗанятие'] ? ' checked ' : ''; ?>>Отс.</label>
								</td><?
														endif;
													endforeach;
												endforeach;
											endforeach;
											foreach ($arItems['СписокОценокДоработки'] as $arBallDop):
												if ($arBallDop['УИДСтудента'] === $student['УИД'] && $arBallDop['УИДМодуля'] === $arModule['УИД']):
													$DopBall = intval($arBallDop['Оценка']);
													$itog += intval($arBallDop['Оценка']);
													?>
							<td>
						<input id="<?= $student['УИД'] . $arModule['УИД'] ?>" name="dopbal-<?= $student['УИД'] ?>"
							   type="number" data-maxball="<?= $arModule['МаксимальныйБалл'] ?>"
							   data-studentuid="<?= $student['УИД'] ?>" data-moduleuid="<?= $arModule['УИД'] ?>"
							   data-dopball="1" autocomplete="off" class="js-dopball" min="0"
							   max="<?= abs(intval($arModule['МаксимальныйБалл']) - intval($itog) + intval($DopBall)) ?>"
							   value="<?= $DopBall ?>"></td><?
												endif; endforeach;
											?>
					  <td><span
						  id="itog-<?= $student['УИД'] . $arModule['УИД'] ?>"><?= $itog ?></span><?= '/' . $arModule['МаксимальныйБалл']; ?>
					  </td></tr><?
											endforeach;
											?></table>
				  </form>
				  </div><? endforeach;
							endforeach;
							?></div>
		</div>
	</div></div><?
endif;
if (IsModuleInstalled("im") && CBXFeatures::IsFeatureEnabled('WebMessenger')) {
	$APPLICATION->IncludeComponent("glab:im.messenger", "", [], false);
}
?>
<script>
  var arResult = {};
  arResult.vedomosti = '<?=json_encode($arJSvedomost);?>';
</script>