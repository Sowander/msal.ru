function intval(num) {
    if (typeof num == 'number' || typeof num == 'string') {
        num = num.toString();
        var dotLocation = num.indexOf('.');
        if (dotLocation > 0) {
            num = num.substr(0, dotLocation);
        }
        if (isNaN(Number(num))) {
            num = parseInt(num);
        }
        if (isNaN(num)) {
            return 0;
        }
        return Number(num);
    }
    else if (typeof num == 'object' && num.length != null && num.length > 0) {
        return 1;
    }
    else if (typeof num == 'boolean' && num === true) {
        return 1;
    }
    return 0;
}

function countProperties(obj) {
    var count = 0;

    for(var prop in obj) {
        if(obj.hasOwnProperty(prop))
            ++count;
    }

    return count;
}

var ved = function () {

};

ved.reset = function () {
    $("form.jsFormUpdate").trigger("reset");
};

$(function () {
    //date

    $("input.datems").change(function () {
        var curDate = $(this).val().replace(/\./g, "-");
        var data = {
            "curDate": curDate
        };
        var hashSedule = '#shedule';
        $('.form-loader').fadeIn(300);
        $.ajax({
            type: 'POST',
            url: '',
            data: data,
            success: function (res) {
                res = $.parseJSON(res);
                // window.location.href = url;
                location.href = location.origin + location.pathname + '?datereq=' + res.monday + hashSedule;
            }
        });
    });
    $(".btnprev, .btnnext").click(function () {
        var curDate = $(this).val();
        var data = {
            "curDate": curDate
        };
        var hashSedule = '#shedule';
        $('.form-loader').fadeIn(300);
        $.ajax({
            type: 'POST',
            url: '',
            data: data,
            success: function (res) {
                res = $.parseJSON(res);
                // window.location.href = url;
                location.href = location.origin + location.pathname + '?datereq=' + curDate + hashSedule;
            }
        });
    });
    //updute ball
    $("form.jsFormUpdate").submit(function (e) {
        e.preventDefault();
        var typeAction = 'allball';
        var form = $(this);
        var nved = form.data('nv');
        var arBalls = [];
        var arDopBalls = [];
        form.find('input.js-ball').each(function (i,input) {
            var thisInput = $(input);
            var ball = thisInput.val();
            var studentuid = thisInput.data('studentuid');
            var uidlesson = thisInput.data('uidlesson');
            var nn = 0;
            if(ball == 'н' || ball == 'Н'){
                ball = 0;
                nn = 1;
            }
            arBalls.push({
                'Оценка': intval(ball),
                'УИДСтудента':studentuid,
                'УИДЗанятия':uidlesson,
                'ПропустилЗанятие': nn,
                'ДатаОценки':0
            })
        });
        form.find('input.js-dopball').each(function (i,input) {
            var thisInput = $(input);
            var ball = thisInput.val();
            var studentuid = thisInput.data('studentuid');
            var moduleuid = thisInput.data('moduleuid');
            arDopBalls.push({
                'Оценка': intval(ball),
                'УИДСтудента':studentuid,
                'УИДМодуля':moduleuid,
                'ДатаОценки':0
            })
        });

        if (e.target.checkValidity()) {
            e.preventDefault();
            // ved.changeFields(id);
            $('.form-loader').fadeIn(300);
        } else {
            return false;
        }

        $.ajax({
            type: "POST",
            data: {
                'arballs':arBalls,
                'arDopBalls':arDopBalls,
                'nved':nved,
                'typeaction': typeAction
            },
            success: function (data) {
                data = JSON.parse(data);
               if(data.ОценкиНеБылиОбновлены === false){
               }else{
                   alert(data.ТекстОшибки);
               }
            },
            error: function () {
                $('.form-loader').fadeOut(300);
                alert('Ошибка запроса');
            },
            complete: function () {
                $('.form-loader').fadeOut(300);
            }
        });
    });

//modal
    if (arResult.vedomosti) {
        arResult.vedomosti = JSON.parse(arResult.vedomosti);
    }
    $(".js-out>input").change(function () {
        var id = $(this).closest('label').attr('id').substring(4);
		
        if ($(this).is(":checked")) {
		
		//console.log(1);
            //$("#" + id).attr({
			$("input[data-id=" + id + "]").attr({
               'type': 'text',
                'value': 'Н',
				'readonly': true
            }).val('Н');
        } else {
            //$("#" + id).attr({
			$("input[data-id=" + id + "]").attr({
               'type': 'number',
				'readonly': false,
                'value': '0'
            }).val('0');
        }
    });

    $('a[data-target="#discModal"]').click(function () {

        var guidSubject = $(this).data("guid");
        $("#modal" + guidSubject).show();
        $("#myModalLabel").html($(this).text());
        if (arResult.vedomosti[guidSubject].dateVed) {
            $("#dateVed").html(arResult.vedomosti[guidSubject].dateVed);
        }
        if (arResult.vedomosti[guidSubject].numberVed) {
            $("#numberVed").html(arResult.vedomosti[guidSubject].numberVed);
        }
        if (arResult.vedomosti[guidSubject].yearLern) {
            $("#yearLern").html(arResult.vedomosti[guidSubject].yearLern);
        }
        if (arResult.vedomosti[guidSubject].semestr) {
            $("#semestr").html(arResult.vedomosti[guidSubject].semestr);
        }
        if (arResult.vedomosti[guidSubject].spec) {
            $("#spec").html(arResult.vedomosti[guidSubject].spec);
        }
        if (arResult.vedomosti[guidSubject].formlern) {
            $("#formlern").html(arResult.vedomosti[guidSubject].formlern);
        }
        if (arResult.vedomosti[guidSubject].curse) {
            $("#curse").html(arResult.vedomosti[guidSubject].curse);
        }
        if (arResult.vedomosti[guidSubject].nabor) {
            $("#nabor").html(arResult.vedomosti[guidSubject].nabor);
        }
    });
    $('#discModal').on('hide.bs.modal', function (event) {
        $('#discModal .onemodule').each(function () {
            $(this).hide();
        });
    });

    $('a[data-target="#moduleBm"]').click(function () {
        var guidSubject = $(this).data("guid");
        $("#bm" + guidSubject).show();
        $("#myModalLabelBm").html($(this).text());
    });
    $('#moduleBm').on('hide.bs.modal', function (event) {

        $('.form-loader').fadeIn(300);
        var nved = $('#moduleBm .onemodule:visible .jsFormUpdate').data('nv');
        $('#moduleBm .onemodule:visible').hide();
        var newurl;
        if(location.search){
            newurl = location.origin + location.pathname + location.search + '&nved='+nved + location.hash;
        }else{
            newurl = location.origin + location.pathname + '?nved='+nved + location.hash;
        }
        if(newurl){
            window.location.replace(newurl);
        }
    });

  $('#moduleBm').on('shown.bs.modal', function (event) {
    //для двойной прокрутки
    $(".wrapper1 .div1").css({'width':$("#moduleBm .modal-content").prop("scrollWidth")});
    $('html').css({'overflow':'hidden'});
  });



  //симуляция прокрутки
  $(".wrapper1").scroll(function() {
    // $(this).next(".modal-content").scrollLeft($(this).scrollLeft());
    $(this).next(".modal-content").scrollLeft($(this).scrollLeft());
  });
  $(".modal-content").scroll(function() {
    $(".wrapper1").scrollLeft($(this).scrollLeft());
  });


// табы при перезагрузке
    $(document).ready(function () {
        if (location.hash) {
            $("a[href='" + location.hash + "']").tab("show");
        }
        $(document.body).on("click", "a[data-toggle='tab']", function (event) {
            location.hash = this.getAttribute("href");
        });
    });
    $(window).on("popstate", function () {
        var anchor = location.hash || $("a[data-toggle='tab']").first().attr("href");
        $("a[href='" + anchor + "']").tab("show");
    });

    //лоадер на пагинацию
  $('.page-item').not('.active, .disabled').click(function () {
    $('.form-loader').fadeIn(100);
  });



});

$(window).load(function () {
    var params = window
        .location
        .search
        .replace('?','')
        .split('&')
        .reduce(
            function(p,e){
                var a = e.split('=');
                p[ decodeURIComponent(a[0])] = decodeURIComponent(a[1]);
                return p;
            },
            {}
        );
    $('.form-loader').fadeOut(300);
    if('nved' in params){
        $('[data-guid="'+params["nved"]+'"]').click();
        delete  params['nved'];
    }
    var urlget = location.origin+location.pathname +'?';
    if(countProperties(params)>0){
        for(var key in params){
            if(params[key]!== "undefined"){
                urlget += key+ '='+params[key]+'&';
            }
        }
    }
    urlget = urlget.substr(0,urlget.length -1);
    history.replaceState(null,'',urlget);
});

