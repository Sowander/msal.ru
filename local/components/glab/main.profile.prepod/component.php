<?
/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @var CBitrixComponent $this
 * @var CBitrixComponent $arParams
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$this->setFrameMode(false);
define("NO_KEEP_STATISTIC", true);
CJSCore::Init(array('popup', 'date'));

$context = \Bitrix\Main\Context::getCurrent();
$request = $context->getRequest();
$prepod = new Prepod();

$arResult["ID"] = intval($USER->GetID());
$arResult["PREPOD_INFO"] = $prepod->getPrepodInfo();

if (!$arResult["PREPOD_INFO"]["LOGIN"] || $arResult["ID"] <= 0) {
    if ($arResult["ID"] > 0) {
        echo "<p class='alert alert-success'>Ошибка значения login " . $arResult["PREPOD_INFO"]["LOGIN"] . "</p>";
        global $USER;
        $USER->Logout();
    }
    $APPLICATION->ShowAuthForm("");
    return;
}

function processAjax()
{
    $context = \Bitrix\Main\Context::getCurrent();
    $request = $context->getRequest();
    global $APPLICATION;

    //листание расписания
    if ($request->isAjaxRequest() && $request->get('curDate')) {
        $APPLICATION->RestartBuffer();
        $now = date('c');
        if ($request->get('curDate')) {
            $now = $request->get('curDate');
        }
        $obDate = new Lk();
        $reult = $obDate::dateToMS($now, '%d.%m.%Y');
        echo json_encode($reult);
        exit;
    }
    //сохранение балов за занятие
    if ($request->isAjaxRequest() && $request->get('save')) {
        $APPLICATION->RestartBuffer();
        $arRes = $request->get('save');
        $arRParams = [];
        if (is_array($arRes) && !empty($arRes)) {
            foreach ($arRes as $item) {
                $arRParams[] = [
                    'Discipline' => [
                        'Name' => trim($item['subject']),
                        'GUID' => trim($item['subjectGuid']) ? trim($item['subjectGuid']) : Lk::$null,
                    ],
                    'LoadType' => [
                        'Name' => trim($item['view']),
                    ],
                    'Teacher' => [
                        'Name' => trim($item['teacher']),
                        'GUID' => trim($item['teacherGuid']) ? trim($item['teacherGuid']) : Lk::$null,
                    ],
                    'RPD' => [
                        'Name' => trim($item['rpd']),
                        'GUID' => trim($item['rpdGuid']) ? trim($item['rpdGuid']) : Lk::$null,
                    ],
                    'Module' => [
                        'Name' => trim($item['module']) ?? '',
                        'GUID' => trim($item['moduleGuid']) ? trim($item['moduleGuid']) : Lk::$null,
                    ],
                    'Student' => [
                        'Name' => trim($item['stname']) ,
                        'GUID' => trim($item['stuid']) ? trim($item['stuid']) : Lk::$null,
                    ],
                    'STime' => [
                        'Val' => trim($item['timeStart']),
                    ],
                    'ETime' => [
                        'Val' => trim($item['timeEnd']),
                    ],
                    'ItemTheme' => [
                        'Name' => trim($item['tema']),
                        'MaxBall' => intval($item['temaMaxBall'])
                    ],
                    'AcademicYear' => [
                        'Name' => trim($item['uchYear']),
                        'GUID' => trim($item['uchYearGuid']) ? trim($item['uchYearGuid']) : Lk::$null,
                    ],
                    'Group' => [
                        'Name' => trim($item['groupName']),
                        'GUID' => trim($item['groupGuid']) ? trim($item['groupGuid']) : Lk::$null,
                    ],
                    'SubGroup' => [
                        'Name' => trim($item['subGroupName']) ? trim($item['subGroupName']) : '',
                        'GUID' => trim($item['subGroupGuid']) ? trim($item['subGroupGuid']) : Lk::$null,
                    ],
                    'Flow' => [
                        'Name' => trim($item['potok']),
                          'GUID' => trim($item['potokGuid']) ? trim($item['potokGuid']) : Lk::$null,
                    ],
                    'Ball' => intval($item['ball']),
                    'Semester' => intval($item['semestr']),
                    'Missed' => intval($item['nn']),
                    'BallR' => intval($item['dorBall']), //TODO добавить
                    'Date' => $item['date'],
                ];
            }
        }

        if (is_array($arRParams) && !empty($arRParams)) {
            $answer = Prepod::saveDataLectionE($arRParams);
            echo json_encode($answer, JSON_UNESCAPED_UNICODE);
        }
        exit;
    }

    //обновление суммы балов студента
    if ($request->isAjaxRequest() && $request->get('updateBall')) {
        $APPLICATION->RestartBuffer();
        $par1 = $request->get('par1');
        $par2 = $request->get('par2');
        $result = false;

        if ($par1 && $par2) {
            $tempResult = Prepod::getLectionE($par1, $par2);
            if (is_array($tempResult) && !empty($tempResult)) {
                foreach ($tempResult as $arGroup) {
                    foreach ($arGroup as $student) {
                        $result[trim($student['Student']['GUID'])] = intval($student['SumBalls']);
                    }
                }
            }
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        exit;
    }

    //ajax поиск студента для доработки
    $loginStudent = trim($request->get('loginStudent'));
    $loginPrepod = trim($request->get('loginPrepod'));
    if ($request->isAjaxRequest() && $loginStudent && $loginPrepod) {
        $APPLICATION->RestartBuffer();
        $listDisc = Prepod::getListDiscDorab($loginStudent, $loginPrepod);
        $result = [];
        $resdor = [];
        $reJs = [];
        if ($listDisc['Status'] !== 'Ошибка') {
            if ($listDisc['MatrixListR']['Discipline'])
                $listDisc['MatrixListR'] = [$listDisc['MatrixListR']];

            if (is_array($listDisc['MatrixListR']) && !empty($listDisc['MatrixListR']))
                foreach ($listDisc['MatrixListR'] as $key => $arDorItem) {
                    $reJs[$arDorItem['Module']['GUID'] . $key] = $arDorItem;

                    $resdor[$arDorItem['Discipline']['Name']]['TABLE']['HEAD'][] = trim($arDorItem['Module']['Name']);
                    $resdor[$arDorItem['Discipline']['Name']]['TABLE']['BODY'][] = '<form class="dor-save-ball" action=""><input size="3" type="number" min="0"  max="' . (intval($arDorItem['Module']['MaxBall']) - intval($arDorItem['SumBalls'])) . '" value="' . $arDorItem['Ball'] . '"><button class="dor-save" data-mguid="' . $arDorItem["Module"]["GUID"] . $key . '" type="submit">Сохранить</button></form>';
                    $resdor[$arDorItem['Discipline']['Name']]['TABLE']['BODY2'][] = '<span>' . $arDorItem['SumBalls'] . '/' . trim($arDorItem['Module']['MaxBall']) . '</span>';
                }
            $resdor2 = '<div class="dor-fio">' . $listDisc['MatrixListR'][0]['Student']['Name'] . '</div>';
            $resdor2 .= '<div class="dor-group">' . $listDisc['MatrixListR'][0]['Group']['Name'] . '</div>';
            foreach ($resdor as $disc => $arDor) {
                $resdor2 .= '<table class="table table-striped">';
                $resdor2 .= "<tr><td>Модуль</td><td>" . implode('</td><td>', $arDor['TABLE']['HEAD']) . "</td></tr>";
                $resdor2 .= "<tr><td>Баллы за семинарские занятия/Макс модуль</td><td>" . implode('</td><td>', $arDor['TABLE']['BODY2']) . "</td></tr>";
                $resdor2 .= "<tr><td><span class='dor-disc-name'>$disc</span></td><td>" . implode('</td><td>', $arDor['TABLE']['BODY']) . "</td></tr>";
                $resdor2 .= '</table>';
            }
            $result['HTML'] = $resdor2;
            $result['JS'] = $reJs;

        } else { //если ошибка
            $result['HTML'] = '<div class="alert alert-danger">' . $listDisc["TextMessage"] . '</div>';
            $result['JS'] = $reJs;
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        die();
    }

    //ajax сохранение балов доработки
    $dorSave = $request->get('Discipline');
    if ($request->isAjaxRequest() && $dorSave) {
        $APPLICATION->RestartBuffer();

        $arParams = [
            'Date' => date('c')
        ];
        $arParams = array_merge($arParams, $_POST);
        $arRes = Prepod::saveDataDorab($arParams);
        echo json_encode($arRes, JSON_UNESCAPED_UNICODE);
        die();
    }
}

$cachePath = '/';
if ($request->get('datereq')) {
    $datereq = Lk::dateToDay($request->get('datereq'), '%Y-%m-%d');
    $datereqID = Lk::dateToDay($request->get('datereq'), '%Y-%m-%d');
} else {
    $datereq = date('c');
}
//расписание
$obCache = new CPHPCache;
$lifeTime = 0;//$arParams['CACHE_TIME'];  //TODO закешировать а может и не стоит)
$lifeTimeVed = $lifeTime;

if (!$datereqID) {
    $datereqID = date('Ymd');
}
$cacheIDSCHEDULE = 'cach' . strval($datereqID) . strval($USER->GetID());
$cacheID = 'cach' . strval($USER->GetID());
if ($obCache->InitCache($lifeTime, $cacheIDSCHEDULE, $cachePath)) {
    $arResult = $obCache->GetVars();
} elseif ($obCache->StartDataCache($lifeTime, $cacheIDSCHEDULE, $cachePath)) {
    $arDate = Lk::dateToMS($datereq, '%d.%m.%Y');
    $arResult["SCHEDULE"]['DATE_START'] = $arDate['monday'];
    $arResult["SCHEDULE"]['DATE_END'] = $arDate['sunday'];
    $arResult["SCHEDULE"]['SYS_DATE_NEXT'] = Lk::dateToDay($datereq, '%Y-%m-%d', '+1 week');
    $arResult["SCHEDULE"]['SYS_DATE_PREV'] = Lk::dateToDay($datereq, '%Y-%m-%d', '1 week ago');

    $entityDataClassSh = Lk::GetEntityDataClass(HL_SHEDULE_ID);
    $entityDataClassGr = Lk::GetEntityDataClass(HL_GROUPS_ID);

    $shOb = $entityDataClassSh::getList(['select' => ['*'], 'filter' => [
        'LOGIC' => 'AND',
        [
            '>=UF_DAY' => $arDate['monday'],
            '=UF_PREPOD_LOGIN' => trim($arResult["PREPOD_INFO"]['LOGIN']),
        ],
        [
            '<=UF_DAY' => $arDate['sunday'],
            '=UF_PREPOD_LOGIN' => trim($arResult["PREPOD_INFO"]['LOGIN']),
        ]
    ]]);
    while ($shRes = $shOb->fetch()) {
        $arGroupNames = [];
        $arFilterJgr = [];
        $arFilterJgrE = [];
        $strInst = '';
        $strForm = '';
        $intCurse = '';
        if (!empty($shRes['UF_LNK_GROUPS']) && is_array($shRes['UF_LNK_GROUPS']))
            foreach ($shRes['UF_LNK_GROUPS'] as $gId) {

                $obRes = $entityDataClassGr::getList(['select' => ['*'], 'filter' => ['ID' => intval($gId)]]);
                if ($resGr = $obRes->fetch()) {
                    $arGroupNames[trim($resGr['UF_XML_ID'])] = trim($resGr['UF_NAME']);
                    $strInst = trim($resGr['UF_INST']);

                    if ($resGr['UF_FORM'] == 1) {
                        $strForm = 'Очная';
                    } elseif ($resGr['UF_FORM'] == 2) {
                        $strForm = 'Заочная';
                    } elseif ($resGr['UF_FORM'] == 3) {
                        $strForm = 'Очно-Заочная';
                    }
                    $intCurse = intval($resGr['UF_COURSE']);


                    $arFilterJgr['Состав'][] = ['GUIDГруппы' => $resGr['UF_XML_ID']];
                    $arFilterJgrE['List'][] = ['GUID' => $resGr['UF_XML_ID']];
                }

            }

        $StrGroupNames = implode('; ', $arGroupNames);

        $arResult['ITEMS'][$shRes['UF_N_PARA']][$shRes['UF_DAY']->format('N')][] = array(
            'id' => trim($shRes['UF_XML_ID']),
            'date' => $shRes['UF_DAY']->format('d.m.Y'),
            'dateExp' => $shRes['UF_DAY']->format('c'),
            'ndate' => $shRes['UF_DAY']->format('N'),
            'timeStart' => $shRes['UF_START'],
            'timeEnd' => $shRes['UF_END'],
            'subject' => $shRes['UF_DISCIPLIN'],
            'subjectGuid' => $shRes['UF_GUID_DISCIPLIN'],
            'place' => $shRes['UF_AUDIT'],
            'view' => $shRes['UF_TYPE'],
            'rpd' => $shRes['UF_RPD'],
            'rpdGuid' => $shRes['UF_GUID_RPD'],
            'korpus' => $shRes['UF_KORPUS'],
            'teacher' => $shRes['UF_PREPOD'],
            'teacherGuid' => $shRes['UF_GUID_PREPOD'],
            'group' => $StrGroupNames,
            'semestr' => $shRes['UF_SEMESTR'],
            'naborYearGuid' => $shRes['UF_GUID_NABOR_YEAR'],
            'naborYear' => $shRes['UF_NABOR_YEAR'],
            'uchYear' => $shRes['UF_UCH_YEAR'],
            'uchYearGuid' => $shRes['UF_GUID_UCH_YEAR'],
            'potok' => $shRes['UF_POTOK'],
            'potokGuid' => $shRes['UF_GUID_POTOK'],
            'inst' => $strInst,
            'form' => $strForm,
            'curse' => $intCurse,
            'arFilterJgrE1' => $arFilterJgrE,
            'arFilterJgrE2' => [ //получить занятие
                'Date' => $shRes['UF_DAY']->format('c'),
                'Semester' => $shRes['UF_SEMESTR'],
                'Theme' => '',
                'Discipline' => [
                    'Name' => $shRes['UF_DISCIPLIN'],
                    'GUID' => $shRes['UF_GUID_DISCIPLIN'] ? $shRes['UF_GUID_DISCIPLIN'] : Lk::$null
                ],
                'LoadType' => [
                    'Name' => $shRes['UF_TYPE']
                ],
                'STime' => [
                    'Val' => $shRes['UF_START']
                ],
                'ETime' => [
                    'Val' => $shRes['UF_END']
                ],
                'Teacher' => [
                    'Name' => $shRes['UF_PREPOD'],
                    'GUID' => $shRes['UF_GUID_PREPOD'] ? $shRes['UF_GUID_PREPOD'] : Lk::$null
                ],
                'RPD' => [
                    'Name' => $shRes['UF_RPD'],
                    'GUID' => $shRes['UF_GUID_RPD'] ? $shRes['UF_GUID_RPD'] : Lk::$null
                ],
                'AcademicYear' => [
                    'Name' => $shRes['UF_UCH_YEAR'],
                    'GUID' => $shRes['UF_GUID_UCH_YEAR'] ? $shRes['UF_GUID_UCH_YEAR'] : Lk::$null
                ],
                'Flow' => [
                    'Name' => $shRes['UF_POTOK'] ?? '',
                    'GUID' => $shRes['UF_GUID_POTOK'] ? $shRes['UF_GUID_POTOK'] : Lk::$null
                ]
            ],
            'arFilterJgr1' => $arFilterJgr,
            'arFilterJgr2' => [ //получить занятие
                'ДатаЗанятия' => $shRes['UF_DAY']->format('c'),
                'Семестр' => $shRes['UF_SEMESTR'],
                'ТемаЗанятий' => '',
                'Дисциплина' => [
                    'Наименование' => $shRes['UF_DISCIPLIN'],
                    'GUIDДисциплины' => $shRes['UF_GUID_DISCIPLIN']
                ],
                'ВидНагрузки' => [
                    'Наименование' => $shRes['UF_TYPE']
                ],
                'ВремяНачалаПары' => [
                    'Значение' => $shRes['UF_START']
                ],
                'ВремяОкончанияПары' => [
                    'Значение' => $shRes['UF_END']
                ],
                'Преподаватель' => [
                    'Наименование' => $shRes['UF_PREPOD'],
                    'GUIDПреподавателя' => $shRes['UF_GUID_PREPOD']
                ],
                'РПД' => [
                    'Наименование' => $shRes['UF_RPD'],
                    'GUID' => $shRes['UF_GUID_RPD']
                ],
                'УчебныйГод' => [
                    'Наименование' => $shRes['UF_UCH_YEAR'],
                    'GUID' => $shRes['UF_GUID_UCH_YEAR']
                ],
                'Поток' => [
                    'Наименование' => $shRes['UF_POTOK'] ?? '',
                    'GUID' => $shRes['UF_GUID_POTOK'] ?? Lk::$null
                ]
            ]
        );
        $arResult['RINGS'][$shRes['UF_N_PARA']] = $shRes['UF_START'] . '-' . $shRes['UF_END'];
    }
    $obCache->EndDataCache($arResult);
}
//открытие журнала
if (!$request->isAjaxRequest() && $request->get('opwin')) {
    foreach ($arResult['ITEMS'] as &$arTemp) {
        foreach ($arTemp as &$arTemp2) {
            foreach ($arTemp2 as &$arLec) {

                if (trim($request->get('opwin')) === $arLec['id'] && $arLec['arFilterJgrE1'] && $arLec['arFilterJgrE2']) {
                    $arLec['JOURNAL'] = Prepod::getLectionE($arLec['arFilterJgrE1'], $arLec['arFilterJgrE2']);
                }

                if (strlen($arLec['rpdGuid']) > 1 && trim($arLec['rpdGuid']) !== Lk::$null && stripos('Ошибка', strval($arLec['rpd'])) === false) {
                    $arLec['RPD'] = Prepod::getStructRpdE(trim($arLec['rpdGuid']));
                } else {
                    $arLec['ERROR'] = 'Ошибка. Не заполнена/Не совпадает РПД';
                }

                if (!$arLec['potok']) {
                    $arLec['ERROR'] = 'Ошибка. Не заполнен поток групп';
                }
                //период редактирование -3 дня
                $date['lection'] = date('U', strtotime($arLec['date']));
                $date['start'] = time();
                $date['end'] = date('U', strtotime('-4 day'));
                if (!($date['lection'] >= $date['end'] && $date['lection'] <= $date['start'])) {
//                    $arLec['ERROR'] = 'Период редактирования недоступен';
                }
            }
        }
    }
}

//открытие сводной таблицы
if (!$request->isAjaxRequest() && $request->get('opwin') && $request->get('svod') === 'Y') {
    function cmpsvod($a, $b)
    {
        return strcmp(strtotime($a["Day"]), strtotime($b["Day"]));
    }

    foreach ($arResult['ITEMS'] as $arTempSvod) {
        foreach ($arTempSvod as $arTemp2Svod) {
            foreach ($arTemp2Svod as $arLecSvod) {
                if (trim($request->get('opwin')) === $arLecSvod['id'] && $arLecSvod['arFilterJgrE1'] && $arLecSvod['arFilterJgrE2']) {
                    $arTemp3Svod = Prepod::getSvodTableE($arLecSvod['arFilterJgrE1'], $arLecSvod['arFilterJgrE2'])['MatrixList'];
                    $arResult['SVOD']['HEADER'] = [];
                    $arResult['SVOD']['ITEMS'] = [];
                    $arTempMod = [];
                    $arTempMaxStud = [];
                    foreach ($arTemp3Svod as $fake => $item) {
                        $arTempMod[$item['Module']['Name']] = $item['Module']['MaxBall'];
                        $arResult['SVOD']['ITEMS'][$item['Module']['Name']][$item['Student']['Name'] . '__' . $item['Student']['GUID']][] = $item;
                    }

                    $arResult['SVOD']['MAXBALL'] = array_sum($arTempMod);
                    ksort($arResult['SVOD']['ITEMS']);
                    foreach ($arResult['SVOD']['ITEMS'] as $module => $arStudent) {
                        ksort($arResult['SVOD']['ITEMS'][$module]);
                    }
                    //выравнивание по датам
                    foreach ($arResult['SVOD']['ITEMS'] as &$arStudentDay) {

                        foreach ($arStudentDay as &$arItemsSort) {
                            usort($arItemsSort, 'cmpsvod');
                        }
                    }

                    foreach ($arResult['SVOD']['ITEMS'] as $module => $arStudent) {
                        $s = 0;
                        foreach ($arStudent as $student => $arItems) {

                            foreach ($arItems as $arItem) {
                                if ($s === 0) {
                                    $arResult['SVOD']['HEADER'][$module][] = ['date' => date('d.m.Y', strtotime($arItem['Day'])), 'tema' => trim($arItem['ItemTheme']['Name'])];
                                }
                            }
                            $s++;
                        }
                    }
                }
            }
        }
    }
}

//старый журнал Удалить
//ведомости
$itemInpage = 5;
$page = intval($request->get('page'));
if ($page < 1) {
    $page = 1;
}
$tempResult = Prepod::getVedomostiForPrepod($arResult["ID"]);
if ($tempResult['НомерВедомости']) {
    $tempResult = [$tempResult];
}
$arResult['PAGIN'] = intval(ceil(count($tempResult) / $itemInpage));
$arResult['CUR_PAGIN'] = $page;
$arResult['CUR_PAGE'] = $APPLICATION->GetCurPage();

$cacheIDVED = 'cach' . strval($USER->GetID()) . $page . $itemInpage;
//сброс кеша при сохранении ведомости
if ($request->get('nved')) {
    $lifeTimeVed = 1;
}
if ($obCache->InitCache($lifeTimeVed, $cacheIDVED, $cachePath)) {
    $arResult["Ведомости"] = $obCache->GetVars();
} elseif ($obCache->StartDataCache($lifeTimeVed, $cacheIDVED, $cachePath)) {

    $arResult["Ведомости"] = $prepod->getVedomostRpdPagin($page, $itemInpage, $tempResult);
    if ($arResult["Ведомости"]['НомерВедомости']) {
        $arResult["Ведомости"] = [$arResult["Ведомости"]];
    }

    foreach ($arResult["Ведомости"] as &$arVed) {
        if ($arVed['СписокСтудентов']['УИД']) {
            $arVed['СписокСтудентов'] = [$arVed['СписокСтудентов']];
        }
        if ($arVed['СписокОценок']['УИДСтудента']) {
            $arVed['СписокОценок'] = [$arVed['СписокОценок']];
        }

        if ($arVed['РабочаяПрограммаДисциплины']['СписокМодулей']['УИД']) {
            $arVed['РабочаяПрограммаДисциплины']['СписокМодулей'] = [$arVed['РабочаяПрограммаДисциплины']['СписокМодулей']];
        }
        foreach ($arVed['РабочаяПрограммаДисциплины']['СписокМодулей'] as &$arModule) {
            if ($arModule['СписокТем']['УИД']) {
                $arModule['СписокТем'] = [$arModule['СписокТем']];
            }
            foreach ($arModule['СписокТем'] as &$arTem) {
                if ($arTem['СписокЗанятий']['УИД']) {
                    $arTem['СписокЗанятий'] = [$arTem['СписокЗанятий']];
                }
            }
        }
    }
    if (!($arResult["Ведомости"][0])) {
        $arResult["Ведомости"] = false;
    }

    $obCache->EndDataCache($arResult["Ведомости"]);
}

processAjax();
$this->IncludeComponentTemplate();