<?
use Terminal\Schedule;
use Bitrix\Main\Application;

$request = Application::getInstance()->getContext()->getRequest();
/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @var $arParams
 * @var $arResult
 * @var CBitrixComponent $this
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
define("NO_KEEP_STATISTIC", true);
CJSCore::Init(array('popup', 'date'));
$arResult = [];


$obCache = new CPHPCache;
$lifeTime = 3600 * 24;
$cachePath = '/';
$cacheID = 'inst123';

if ($obCache->InitCache($lifeTime, $cacheID, $cachePath)) {

	$arResult['INST'] = $obCache->GetVars()['INST'];

} elseif ($obCache->StartDataCache($lifeTime, $cacheID, $cachePath)) {

	$arResult['INST'] = Schedule::getListGroups();

	$obCache->EndDataCache(['INST' => $arResult['INST']]);
}
$form = intval($request->get('form'));
$course = intval($request->get('course'));
$inst = trim(urldecode($request->get('inst')));
if ($form && $course) {

	foreach ($arResult['INST'] as $instkey => $arList) {
		foreach ($arList as $key => $arGr) {
			if (intval($arGr['UF_COURSE']) !== $course or intval($arGr['UF_FORM']) !== $form) {
				unset($arResult['INST'][$instkey][$key]);
			}
		}
		if (empty($arResult['INST'][$instkey])) {
			unset($arResult['INST'][$instkey]);
		}
		if ($inst && trim(strtolower($inst))!==trim(strtolower($instkey))) {
			unset($arResult['INST'][$instkey]);
		}
	}
}
$groupGuid = $request->get('guid');
if ($groupGuid) {
	$arResult["SCHEDULE"]['YEAR'] = Schedule::getkListScheduleBitrix('Y');
	$arDate = Terminal\Common::dateToMS(false, '%d.%m.%Y');
	$arResult["SCHEDULE"]['DATE_START'] = $arDate['monday'];
	$arResult["SCHEDULE"]['DATE_END'] = $arDate['sunday'];

	$entityDataClassSh = Terminal\Common::GetEntityDataClass(HL_SHEDULE_ID);
	$entityDataClassGr = Terminal\Common::GetEntityDataClass(HL_GROUPS_ID);
	$groupHlId = $entityDataClassGr::getList(['select' => ['ID'], 'filter' => ['=UF_XML_ID' => trim($groupGuid)]])->fetch()['ID'];
	$shOb = $entityDataClassSh::getList(['select' => ['*'], 'filter' => [
		'LOGIC' => 'AND',
		[
			'>=UF_DAY' => $arDate['monday'],
			'UF_LNK_GROUPS' => $groupHlId,
		],
		[
			'<=UF_DAY' => $arDate['sunday'],
			'UF_LNK_GROUPS' => $groupHlId,
		]
	]]);
	while ($shRes = $shOb->fetch()) {
		$arResult["SCHEDULE"]['ITEMS'][$shRes['UF_N_PARA']][$shRes['UF_DAY']->format('N')][] = array(
			'date' => $shRes['UF_DAY']->format('d.m.Y'),
			'ndate' => $shRes['UF_DAY']->format('N'),
			'timeStart' => $shRes['UF_START'],
			'timeEnd' => $shRes['UF_END'],
			'subject' => $shRes['UF_DISCIPLIN'],
			'place' => $shRes['UF_AUDIT'],
			'view' => $shRes['UF_TYPE'],
			'korpus' => $shRes['UF_KORPUS'],
			'teacher' => $shRes['UF_PREPOD'],
		);
		$arResult["SCHEDULE"]['RINGS'][$shRes['UF_N_PARA']] = $shRes['UF_START'] . '-' . $shRes['UF_END'];
	}
	foreach ($arResult["SCHEDULE"]['ITEMS'] as $nlect => $arDay) {
		foreach ($arDay as $nday => $arTempLect) {
			if (count($arTempLect) === 1) {
				$arResult["SCHEDULE"]['ITEMS'][$nlect][$nday] = $arTempLect[0];
			}
		}
	}
	ksort($arResult["SCHEDULE"]['ITEMS']);
	ksort($arResult["SCHEDULE"]['RINGS']);
}


$this->IncludeComponentTemplate();