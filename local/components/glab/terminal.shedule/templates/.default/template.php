<?
/**
 * @global CMain $APPLICATION
 * @var array $arParams
 * @var array $arResult
 */

if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
	die();
use Bitrix\Main\Page\Asset;
use Bitrix\Main\Application;

$request = Application::getInstance()->getContext()->getRequest();
$step = intval($request->get("step"));
$getCourse = intval($request->get("course"));
$getForm = intval($request->get("form"));
$getPage = intval($request->get("page"));
$getNameGroup = urldecode($request->get("name"));

$id = 123;

global $APPLICATION;
$ruWeek = array('ПН', 'ВТ', 'СР', 'ЧТ', 'ПТ', 'СБ', 'ВC');
$rootShedule = '/terminal/schedule/index.php';
$courses = [
	1 => '1-й курс',
	2 => '2-й курс',
	3 => '3-й курс',
	4 => '4-й курс',
	5 => '5-й курс',
];
$ochns = [
	1 => 'Очная',
	2 => 'Заочная',
	3 => 'Очно-Заочная',
];

if (intval($step) === 1):?>
	<!--курсы-->
	<section class="msal-main msal-secondary-main msal-department-section">
		<div class="msal-main__text">
			<h1 class="msal-main__text-title">Выберите курс</h1>
		</div>
		<div class="msal-main__department-sections">
			<div class="department-sections">
				<ul class="department-sections__items">
									<? foreach ($courses as $cv => $course): ?>
					  <li>
						  <a href="<?= $APPLICATION->GetCurPageParam("step=" . strval($step + 1) . '&course=' . $cv, array("step", 'course')); ?>"><?= $course ?></a>
					  </li>
									<? endforeach; ?>
				</ul>
			</div>
		</div>
	</section>
<? endif;
if (intval($step) === 2): ?>
	<!--формы обучения-->
	<section class="msal-main msal-secondary-main msal-department-section">
		<div class="msal-main__text">
			<h1 class="msal-main__text-title">Выберите форму обучения</h1>
		</div>
		<div class="msal-main__department-sections">
			<div class="department-sections">
				<ul class="department-sections__items">
									<? foreach ($ochns as $fv => $type): ?>
					  <li>
						  <a href="<?= $APPLICATION->GetCurPageParam("step=" . strval($step + 1) . '&form=' . $fv . '&page=1', array("step", 'form', 'page')); ?>"><?= $type ?></a>
					  </li>
									<? endforeach; ?>
				</ul>
			</div>
		</div>
	</section>
<? endif;
if (intval($step) === 3): ?>
	<!--институты-->
	<section class="msal-main msal-secondary-main msal-department-section">
		<div class="msal-main__text">
			<h1 class="msal-main__text-title">Выберите институт</h1>
		</div>
		<div class="msal-main__department-sections">
			<div class="department-sections">
				<ul class="department-sections__items">
									<?
									$datastep = 3;
									$inpage = 8;
									$in = 0;
									$start = $inpage * intval($getPage) - $inpage;
									$end = 8 * intval($getPage);
									$pagecount = ceil(count($arResult['INST']) / $inpage);

									$pageLeft = $APPLICATION->GetCurPageParam("page=" . strval($getPage - 1), array("page"));
									if ($getPage - 1 <= 0) {
										$pageLeft = '#';
									}
									$pageRight = $APPLICATION->GetCurPageParam("page=" . strval($getPage + 1), array("page"));
									if ($getPage + 1 > $pagecount) {
										$pageRight = '#';
									}

									foreach ($arResult['INST'] as $inst => $arGroup):
										$in++;
										if ($start > $in or $in > $end) {
											continue;
										}

										if ($inst): ?>
						<li>
							<a href="<?= $APPLICATION->GetCurPageParam("step=" . strval($step + 1) . '&inst=' . $inst . '&page=1', array("step", 'page', 'inst')); ?>"><?= $inst ?></a>
						</li>
										<? endif;
									endforeach; ?>

				</ul>
			</div>
		</div>

        <?if($pagecount>1):?>
		<div class="msal-main__navigation">
			<div class="msal-navigation__nav-prev">
				<a href="<?= $pageLeft ?>"><img src="<?= SITE_TEMPLATE_PATH ?>/images/arr-prev.png" alt=""></a></div>
			<ul class="msal-navigation__list">
							<?

							for ($i = 1; $i <= $pagecount; $i++):?>
				  <li class="msal-navigation__list-item <?= ($i == $getPage) ? ' active ' : ''; ?>">
										<? if ($i != $getPage): ?>
						<a href="<?= $APPLICATION->GetCurPageParam("page=" . strval($i), array("page")); ?>"><?= $i ?></a>
										<? else: ?><?= $i ?><? endif; ?>
				  </li>
							<? endfor; ?>
			</ul>
			<div class="msal-navigation__nav-next">
				<a href="<?= $pageRight ?>"><img src="<?= SITE_TEMPLATE_PATH ?>/images/arr-next.png" alt=""></a></div>
		</div>
        <?endif;?>
	</section>
<? endif;
if (intval($step) === 4): ?>
	<!--группы-->
	<section class="msal-main msal-secondary-main msal-department-section">
		<div class="msal-main__text">
			<h1 class="msal-main__text-title">Выберите группу</h1>
		</div>
		<div class="msal-main__department-sections">
			<div class="department-sections">
				<ul class="department-sections__items">
									<?
									$instName = urldecode($_GET['inst']);
									$datastep = 4;
									$inpage = 9;
									$in = 0;
									$start = $inpage * intval($getPage) - $inpage;
									$end = $inpage * intval($getPage);
									$pagecount = ceil(count($arResult['INST'][$instName]) / $inpage);

									$pageLeft = $APPLICATION->GetCurPageParam("page=" . strval($getPage - 1), array("page"));
									if ($getPage - 1 <= 0) {
										$pageLeft = '#';
									}
									$pageRight = $APPLICATION->GetCurPageParam("page=" . strval($getPage + 1), array("page"));
									if ($getPage + 1 > $pagecount) {
										$pageRight = '#';
									}

									foreach ($arResult['INST'][$instName] as $arGroup):

										$in++;
										if ($start > $in or $in > $end) {
											continue;
										}
										if ($arGroup['UF_XML_ID']): ?>
						<li><a href="<?=$APPLICATION->GetCurPageParam("step=" . strval($step + 1) . '&guid=' . $arGroup['UF_XML_ID']. '&name='.$arGroup['UF_NAME'], array("step", 'page', 'guid','name')); ?>"><?= $arGroup['UF_NAME'] ?></a></li>
										<? endif;
									endforeach; ?>

				</ul>
			</div>
		</div>
    <?if($pagecount>1):?>
		<div class="msal-main__navigation">
			<div class="msal-navigation__nav-prev"><a href="<?= $pageLeft ?>"><img
						src="<?= SITE_TEMPLATE_PATH ?>/images/arr-prev.png" alt=""></a></div>
			<ul class="msal-navigation__list">
							<?

							for ($i = 1; $i <= $pagecount; $i++):?>
				  <li class="msal-navigation__list-item <?= ($i == $getPage) ? ' active ' : ''; ?>">
										<? if ($i != $getPage): ?>
						<a class="jspagin"
						   href="<?= $APPLICATION->GetCurPageParam("page=" . strval($i), array("page")); ?>"><?= $i ?></a>
										<? else: ?><?= $i ?><? endif; ?>
				  </li>
							<? endfor; ?>
			</ul>
			<div class="msal-navigation__nav-next"><a href="<?= $pageRight ?>"><img
						src="<?= SITE_TEMPLATE_PATH ?>/images/arr-next.png" alt=""></a></div>
		</div>
        <?endif;?>
	</section>
<? endif;
if (intval($step) === 5):?>
	<!--расписание-->
	<section class="msal-main msal-secondary-main msal-schedule-section">
		<div class="msal-main__text">
			<p class="msal-main__text-title">Расписание</p>
			<p class="msal-main__text-subtitle"><i><h2><?= $arResult['SCHEDULE']['DATE_START'] . " - " . $arResult['SCHEDULE']['DATE_END']; ?>, группа <?=$getNameGroup?></h2></i></p>
		</div>
		<div class="schedule-section scroll-pane">
			<table class="schedule-table">


			<tr>
			<th></th>
						<? foreach ($ruWeek as $day): ?>
				<th><?= $day ?></th>
						<? endforeach; ?>
			</tr>


						<? $rows = 8; // количество строк, tr
						$cols = 8;
						for ($tr = 1; $tr <= $rows; $tr++):
							?>
							<?= '</tr>' ?><? for ($td = 1; $td <= $cols; $td++): ?>

							<?
							if ($td != 1) {
								//если два заниятия
								if ($arResult["SCHEDULE"]['ITEMS'][$tr][$td - 1][1]) {
									echo '<td class="schedule-table__full-cell">';
									foreach ($arResult["SCHEDULE"]['ITEMS'][$tr][$td - 1] as $ki => $navibor) {
										if ($ki !== 0) {
											echo '<hr>';
										}
										echo $navibor['view'] . '<br>';
										echo $navibor['subject'] . '<br>';
										echo $navibor['teacher'] . '<br>';
										echo $navibor['place'] . '<br>';
									}
								} else {
									echo $arResult["SCHEDULE"]['ITEMS'][$tr][$td - 1]['subject']? '<td class="schedule-table__full-cell">':'<td>';
									echo $arResult["SCHEDULE"]['ITEMS'][$tr][$td - 1]['view'] . '<br>';
									echo $arResult["SCHEDULE"]['ITEMS'][$tr][$td - 1]['subject'] . '<br>';
									echo $arResult["SCHEDULE"]['ITEMS'][$tr][$td - 1]['teacher'] . '<br>';
									echo $arResult["SCHEDULE"]['ITEMS'][$tr][$td - 1]['place'] . '<br>';
									echo '</td>';
								}
							} elseif ($td === 1) {
								echo '<td>';
								echo "$tr - я пара <br>";
								echo '<b>'.$arResult["SCHEDULE"]['RINGS'][$tr]. '</b>';
								echo '</td>';
							}
							?>

						<? endfor; ?>
							<?= '</tr>' ?><? endfor; ?>

			</table>
			<?/*
			<table class="schedule-table">
			<tr>
			<th>&nbsp;</th>
						<? foreach ($ruWeek as $weekDay): ?>
				<th><?= $weekDay ?></th>
						<? endforeach; ?>
			</tr>
			<tr>
			<tr>
			<td>1-я пара <b>8:00 - 9:40</b></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			</tr>
			<tr>
			<td>2-я пара <b>8:00 - 9:40</b></td>
			<td></td>
			<td class="schedule-table__full-cell">История и теория древнеримского...</td>
			<td></td>
			<td></td>
			<td class="schedule-table__full-cell">История и теория древнеримского...</td>
			<td></td>
			<td></td>
			</tr>
			<tr>
			<td>3-я пара <b>8:00 - 9:40</b></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			</tr>
			<tr>
			<td>4-я пара <b>8:00 - 9:40</b></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td class="schedule-table__full-cell">История и теория древнеримского...</td>
			<td></td>
			<td></td>
			</tr>
			<tr>
			<td>5-я пара <b>8:00 - 9:40</b></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td class="schedule-table__full-cell">История и теория древнеримского...</td>
			<td></td>
			<td></td>
			</tr>
			<tr>
			<td>6-я пара <b>8:00 - 9:40</b></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			</tr>
			<tr>
			<td>7-я пара <b>8:00 - 9:40</b></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td class="schedule-table__full-cell">История и теория древнеримского...</td>
			<td></td>
			</tr>
			</table>*/?>
		</div>
	</section>
<? endif; ?>