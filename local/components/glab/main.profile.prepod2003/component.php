<?
/**
 * @global CMain $APPLICATION
 * @global CUser $USER
 * @var CBitrixComponent $this
 * @var CBitrixComponent $arParams
 */
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true)
    die();

$this->setFrameMode(false);
define("NO_KEEP_STATISTIC", true);
CJSCore::Init(array('popup', 'date'));

$context = \Bitrix\Main\Context::getCurrent();
$request = $context->getRequest();
$prepod = new Prepod();

$arResult["ID"] = intval($USER->GetID());
$arResult["PREPOD_INFO"] = $prepod->getPrepodInfo();

if (!$arResult["PREPOD_INFO"]["LOGIN"] || $arResult["ID"] <= 0) {
    if ($arResult["ID"] > 0) {
        echo "<p class='alert alert-success'>Ошибка значения login " . $arResult["PREPOD_INFO"]["LOGIN"] . "</p>";
        global $USER;
        $USER->Logout();
    }
    $APPLICATION->ShowAuthForm("");
    return;
}

function processAjax()
{
    $context = \Bitrix\Main\Context::getCurrent();
    $request = $context->getRequest();
    global $APPLICATION;

    //листание расписания
    if ($request->isAjaxRequest() && $request->get('curDate')) {
        $APPLICATION->RestartBuffer();
        $now = date('c');
        if ($request->get('curDate')) {
            $now = $request->get('curDate');
        }
        $obDate = new Lk();
        $reult = $obDate::dateToMS($now, '%d.%m.%Y');
        echo json_encode($reult);
        exit;
    }
    //сохранение балов за занятие
    if ($request->isAjaxRequest() && $request->get('save')) {
        $APPLICATION->RestartBuffer();
        $arRes = $request->get('save');
        $arRParams = [];
        if (is_array($arRes) && !empty($arRes)) {
            foreach ($arRes as $item) {
                $arRParams[] = [
                    'Дисциплина' => [
                        'Наименование' => trim($item['subject']) ?? '',
                        'GUID' => trim($item['subjectGuid']) ?? Lk::$null,
                    ],
                    'ВидНагрузки' => [
                        'Наименование' => trim($item['view']) ?? '',
                    ],
                    'Преподаватель' => [
                        'Наименование' => trim($item['teacher']) ?? '',
                        'GUID' => trim($item['teacherGuid']) ?? Lk::$null,
                    ],
                    'РПД' => [
                        'Наименование' => trim($item['rpd']) ?? '',
                        'GUID' => trim($item['rpdGuid']) ?? Lk::$null,
                    ],
                    'Модуль' => [
                        'Наименование' => trim($item['module']) ?? '',
                        'GUID' => trim($item['moduleGuid']) ?? Lk::$null,
                    ],
                    'Студент' => [
                        'Наименование' => trim($item['stname']) ?? '',
                        'GUID' => trim($item['stuid']) ?? Lk::$null,
                    ],
                    'ВремяНачалаПары' => [
                        'Значение' => trim($item['timeStart']) ?? '',
                    ],
                    'ВремяОкончанияПары' => [
                        'Значение' => trim($item['timeEnd']) ?? '',
                    ],
                    'ТемаЗанятий' => [
                        'Наименование' => trim($item['tema']) ?? '',
                        'МаксимальныйБалл' => intval($item['temaMaxBall'])
                    ],
                    'УчебныйГод' => [
                        'Наименование' => trim($item['uchYear']) ?? '',
                        'GUID' => trim($item['uchYearGuid']) ?? Lk::$null,
                    ],
                    'Группа'=>[
                        'Наименование' => trim($item['groupName']) ?? '',
                        'GUID' => trim($item['groupGuid']) ?? Lk::$null,
                    ],
                    'Поток' => [
                        'Наименование' => trim($item['potok']) ?? '',
                        'GUID' =>trim($item['potokGuid']) ?? Lk::$null
                    ],
                    'Балл' => intval($item['ball']),
                    'Семестр' => intval($item['semestr']),
                    'ЗанятиеПропущено' => intval($item['nn']),
                    'БаллДоработки' => intval($item['dorBall']), //TODO добавить
                    'ДатаЗанятия' => $item['date'],
                ];
            }
        }

        if (is_array($arRParams) && !empty($arRParams)) {
            $answer = Prepod::saveJournal($arRParams);
            echo json_encode($answer, JSON_UNESCAPED_UNICODE);
        }
        exit;
    }

    //обновление суммы балов студента
    if ($request->isAjaxRequest() && $request->get('updateBall')) {
        $APPLICATION->RestartBuffer();
        $par1 = $request->get('par1');
        $par2 = $request->get('par2');
        $result = false;
        if ($par1 && $par2) {
            $tempResult = Prepod::getPrepodJournal($par1, $par2);
            if(is_array($tempResult) && !empty($tempResult)){
                foreach ($tempResult as $arGroup){
                    foreach ($arGroup as $student){
                        $result[trim($student['Студент']['GUIDСтудента'])] = intval($student['СуммаБаллов']);
                    }
                }
            }
        }
        echo json_encode($result, JSON_UNESCAPED_UNICODE);
        exit;
    }
}

$cachePath = '/';
if ($request->get('datereq')) {
    $datereq = Lk::dateToDay($request->get('datereq'), '%Y-%m-%d');
    $datereqID = Lk::dateToDay($request->get('datereq'), '%Y-%m-%d');
} else {
    $datereq = date('c');
}
//расписание
$obCache = new CPHPCache;
$lifeTime = 0;//$arParams['CACHE_TIME'];  //TODO закешировать а может и не стоит)
$lifeTimeVed = $lifeTime;

if (!$datereqID) {
    $datereqID = date('Ymd');
}
$cacheIDSCHEDULE = 'cach' . strval($datereqID) . strval($USER->GetID());
$cacheID = 'cach' . strval($USER->GetID());
if ($obCache->InitCache($lifeTime, $cacheIDSCHEDULE, $cachePath)) {
    $arResult = $obCache->GetVars();
} elseif ($obCache->StartDataCache($lifeTime, $cacheIDSCHEDULE, $cachePath)) {
    $arDate = Lk::dateToMS($datereq, '%d.%m.%Y');
    $arResult["SCHEDULE"]['DATE_START'] = $arDate['monday'];
    $arResult["SCHEDULE"]['DATE_END'] = $arDate['sunday'];
    $arResult["SCHEDULE"]['SYS_DATE_NEXT'] = Lk::dateToDay($datereq, '%Y-%m-%d', '+1 week');
    $arResult["SCHEDULE"]['SYS_DATE_PREV'] = Lk::dateToDay($datereq, '%Y-%m-%d', '1 week ago');

    $entityDataClassSh = Lk::GetEntityDataClass(HL_SHEDULE_ID);
    $entityDataClassGr = Lk::GetEntityDataClass(HL_GROUPS_ID);

    $shOb = $entityDataClassSh::getList(['select' => ['*'], 'filter' => [
        'LOGIC' => 'AND',
        [
            '>=UF_DAY' => $arDate['monday'],
            '=UF_PREPOD_LOGIN' => trim($arResult["PREPOD_INFO"]['LOGIN']),
        ],
        [
            '<=UF_DAY' => $arDate['sunday'],
            '=UF_PREPOD_LOGIN' => trim($arResult["PREPOD_INFO"]['LOGIN']),
        ]
    ]]);
    while ($shRes = $shOb->fetch()) {
        $arGroupNames = [];
        $arFilterJgr = [];
        $strInst = '';
        $strForm = '';
        $intCurse = '';
        if (!empty($shRes['UF_LNK_GROUPS']) && is_array($shRes['UF_LNK_GROUPS']))
            foreach ($shRes['UF_LNK_GROUPS'] as $gId) {

                $obRes = $entityDataClassGr::getList(['select' => ['*'], 'filter' => ['ID' => intval($gId)]]);
                if ($resGr = $obRes->fetch()) {
                    $arGroupNames[trim($resGr['UF_XML_ID'])] = trim($resGr['UF_NAME']);
                    $strInst = trim($resGr['UF_INST']);

                    if ($resGr['UF_FORM'] == 1) {
                        $strForm = 'Очная';
                    } elseif ($resGr['UF_FORM'] == 2) {
                        $strForm = 'Заочная';
                    } elseif ($resGr['UF_FORM'] == 3) {
                        $strForm = 'Очно-Заочная';
                    }
                    $intCurse = intval($resGr['UF_COURSE']);


                    $arFilterJgr['Состав'][] = ['GUIDГруппы' => $resGr['UF_XML_ID']];
                }

            }

        $StrGroupNames = implode('; ', $arGroupNames);

        $arResult['ITEMS'][$shRes['UF_N_PARA']][$shRes['UF_DAY']->format('N')][] = array(
            'id' => trim($shRes['UF_XML_ID']),
            'date' => $shRes['UF_DAY']->format('d.m.Y'),
            'dateExp' => $shRes['UF_DAY']->format('c'),
            'ndate' => $shRes['UF_DAY']->format('N'),
            'timeStart' => $shRes['UF_START'],
            'timeEnd' => $shRes['UF_END'],
            'subject' => $shRes['UF_DISCIPLIN'],
            'subjectGuid' => $shRes['UF_GUID_DISCIPLIN'],
            'place' => $shRes['UF_AUDIT'],
            'view' => $shRes['UF_TYPE'],
            'rpd' => $shRes['UF_RPD'],
            'rpdGuid' => $shRes['UF_GUID_RPD'],
            'korpus' => $shRes['UF_KORPUS'],
            'teacher' => $shRes['UF_PREPOD'],
            'teacherGuid' => $shRes['UF_GUID_PREPOD'],
            'group' => $StrGroupNames,
            'semestr' => $shRes['UF_SEMESTR'],
            'naborYearGuid' => $shRes['UF_GUID_NABOR_YEAR'],
            'naborYear' => $shRes['UF_NABOR_YEAR'],
            'uchYear' => $shRes['UF_UCH_YEAR'],
            'uchYearGuid' => $shRes['UF_GUID_UCH_YEAR'],
            'potok' => $shRes['UF_POTOK'],
            'potokGuid' => $shRes['UF_GUID_POTOK'],
            'inst' => $strInst,
            'form' => $strForm,
            'curse' => $intCurse,
            'arFilterJgr1' => $arFilterJgr,
            'arFilterJgr2' => [ //получить занятие
                'ДатаЗанятия' => $shRes['UF_DAY']->format('c'),
                'Семестр' => $shRes['UF_SEMESTR'],
                'ТемаЗанятий' => '',
                'Дисциплина' => [
                    'Наименование' => $shRes['UF_DISCIPLIN'],
                    'GUIDДисциплины' => $shRes['UF_GUID_DISCIPLIN']
                ],
                'ВидНагрузки' => [
                    'Наименование' => $shRes['UF_TYPE']
                ],
                'ВремяНачалаПары' => [
                    'Значение' => $shRes['UF_START']
                ],
                'ВремяОкончанияПары' => [
                    'Значение' => $shRes['UF_END']
                ],
                'Преподаватель' => [
                    'Наименование' => $shRes['UF_PREPOD'],
                    'GUIDПреподавателя' => $shRes['UF_GUID_PREPOD']
                ],
                'РПД' => [
                    'Наименование' => $shRes['UF_RPD'],
                    'GUID' => $shRes['UF_GUID_RPD']
                ],
                'УчебныйГод' => [
                    'Наименование' => $shRes['UF_UCH_YEAR'],
                    'GUID' => $shRes['UF_GUID_UCH_YEAR']
                ],
                'Поток' => [
                    'Наименование' => $shRes['UF_POTOK'] ?? '',
                    'GUID' => $shRes['UF_GUID_POTOK'] ?? Lk::$null
                ]
            ]
        );
        $arResult['RINGS'][$shRes['UF_N_PARA']] = $shRes['UF_START'] . '-' . $shRes['UF_END'];
    }
    $obCache->EndDataCache($arResult);
}
//открытие журнала
if (!$request->isAjaxRequest() && $request->get('opwin')) {

    $idlec = trim($request->get('opwin'));
    foreach ($arResult['ITEMS'] as &$arTemp) {
        foreach ($arTemp as &$arTemp2) {
            foreach ($arTemp2 as &$arLec) {
                if ($idlec === $arLec['id'] && $arLec['arFilterJgr1'] && $arLec['arFilterJgr2']) {

                    $arLec['JOURNAL'] = Prepod::getPrepodJournal($arLec['arFilterJgr1'], $arLec['arFilterJgr2']);
//                    printr(count($arLec['JOURNAL']['ИППУ15-01Д-НБ']),1);
                }

                if (strlen($arLec['rpdGuid']) > 1 && trim($arLec['rpdGuid']) !== Lk::$null && stripos('Ошибка',strval($arLec['rpd']))===false) {
                    $arLec['RPD'] = Prepod::getStructRPD(trim($arLec['rpdGuid']));
                } else {
                    $arLec['ERROR'] = 'Ошибка. Не заполнена/Не совпадает РПД';
                }

                if(!$arLec['potok']){
                    $arLec['ERROR'] = 'Ошибка. Не заполнен поток групп';
                }
                //период редактирование -3 дня
                $date['lection'] = date('U', strtotime($arLec['date']));
                $date['start'] = time();
                $date['end'] = date('U', strtotime('-4 day'));
                if (!($date['lection'] >= $date['end'] && $date['lection'] <= $date['start'])) {
//                    $arLec['ERROR'] = 'Период редактирования недоступен';
                }
            }
        }
    }
}


//старый журнал Удалить
//ведомости
$itemInpage = 5;
$page = intval($request->get('page'));
if ($page < 1) {
    $page = 1;
}
$tempResult = Prepod::getVedomostiForPrepod($arResult["ID"]);
if ($tempResult['НомерВедомости']) {
    $tempResult = [$tempResult];
}
$arResult['PAGIN'] = intval(ceil(count($tempResult) / $itemInpage));
$arResult['CUR_PAGIN'] = $page;
$arResult['CUR_PAGE'] = $APPLICATION->GetCurPage();

$cacheIDVED = 'cach' . strval($USER->GetID()) . $page . $itemInpage;
//сброс кеша при сохранении ведомости
if ($request->get('nved')) {
    $lifeTimeVed = 1;
}
if ($obCache->InitCache($lifeTimeVed, $cacheIDVED, $cachePath)) {
    $arResult["Ведомости"] = $obCache->GetVars();
} elseif ($obCache->StartDataCache($lifeTimeVed, $cacheIDVED, $cachePath)) {

    $arResult["Ведомости"] = $prepod->getVedomostRpdPagin($page, $itemInpage, $tempResult);
    if ($arResult["Ведомости"]['НомерВедомости']) {
        $arResult["Ведомости"] = [$arResult["Ведомости"]];
    }

    foreach ($arResult["Ведомости"] as &$arVed) {
        if ($arVed['СписокСтудентов']['УИД']) {
            $arVed['СписокСтудентов'] = [$arVed['СписокСтудентов']];
        }
        if ($arVed['СписокОценок']['УИДСтудента']) {
            $arVed['СписокОценок'] = [$arVed['СписокОценок']];
        }

        if ($arVed['РабочаяПрограммаДисциплины']['СписокМодулей']['УИД']) {
            $arVed['РабочаяПрограммаДисциплины']['СписокМодулей'] = [$arVed['РабочаяПрограммаДисциплины']['СписокМодулей']];
        }
        foreach ($arVed['РабочаяПрограммаДисциплины']['СписокМодулей'] as &$arModule) {
            if ($arModule['СписокТем']['УИД']) {
                $arModule['СписокТем'] = [$arModule['СписокТем']];
            }
            foreach ($arModule['СписокТем'] as &$arTem) {
                if ($arTem['СписокЗанятий']['УИД']) {
                    $arTem['СписокЗанятий'] = [$arTem['СписокЗанятий']];
                }
            }
        }
    }
    if (!($arResult["Ведомости"][0])) {
        $arResult["Ведомости"] = false;
    }

    $obCache->EndDataCache($arResult["Ведомости"]);
}

processAjax();
$this->IncludeComponentTemplate();