<?php
AddEventHandler("main", "OnAfterUserAuthorize", Array("MyClass", "OnAfterUserAuthorizeHandler"));

class MyClass
{
    function OnAfterUserAuthorizeHandler($arUser)
    {
        if($arUser['user_fields']['LOGIN'] && $arUser['user_fields']['ID']){
                $guid = Student::getStudentGuid($arUser['user_fields']['LOGIN']);

                if ($guid) {
                    $groupGuid['UF_GROUP_GUID'] = $guid["UF_GROUP_GUID"];
                    $groupGuid['UF_STUDENT_GUID'] = $guid["UF_STUDENT_GUID"];
                    $groupGuid['UF_STUDENT_GROUP'] = $guid["UF_STUDENT_GROUP"];
                    $groupGuid['UF_STUDENT_STAR'] = intval($guid['UF_STUDENT_STAR']);
                    $groupGuid['UF_ACTIVE'] = $guid['UF_ACTIVE'];

                    $user = new CUser;
                    $fields = Array(
                        "UF_GROUP_GUID" => trim($groupGuid['UF_GROUP_GUID']),
                        "UF_STUDENT_GUID" => trim($groupGuid['UF_STUDENT_GUID']),
                        "UF_STUDENT_GROUP" => trim($groupGuid['UF_STUDENT_GROUP']),
                        "UF_STUDENT_STAR" => intval($groupGuid['UF_STUDENT_STAR']),
                    );
                    global $USER;
                    $arGroups = $USER->GetUserGroupArray();

                    if(in_array(ID_STUNENT_GROUP,$arGroups)){
                        if(strtolower(trim($groupGuid['UF_ACTIVE']))=='учится'){
                            $fields['ACTIVE'] = 'Y';
                        }else{
                            $fields['ACTIVE'] = 'N';
                        }
                        $user->Update($arUser['user_fields']['ID'], $fields);
                    }else{
                        $fields = ['ACTIVE'=> 'Y'] ;
                        $user->Update($arUser['user_fields']['ID'], $fields);
                    }
                }

        }

    }
}