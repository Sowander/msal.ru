<?php
/**
 * Created by PhpStorm.
 * User: Bondarenko Georg
 * Date: 24.09.2015
 * Time: 11:19
 */

class BITCron {

    const EVENT_IBLOCK_ID = 6;
    const ARCHIVE_XML_ID = 14;

    /** Move old events to archive */
    public static function MoveOldEventsToArchive()
    {
        if(CModule::IncludeModule('iblock'))
        {
            $rs_old_events = self::GetOldEvents();
            while($obj_old_event = $rs_old_events->GetNextElement())
            {
                $event_id = $obj_old_event->GetFields()["ID"];
	            self::MoveEventsToArchive($event_id);
            }
        }
    }

    /** Check witch elements is "old" (event is end)
     * and return CResult object with this elements
     * @return CResult
     */
    public static function GetOldEvents()
    {
        $dateCur = date("Y-m-d");
        $arSelect = Array("ID", "IBLOCK_ID","PROPERTY_*");
        $arFilter = Array(
            "IBLOCK_ID"             =>  intval(self::EVENT_IBLOCK_ID),
            array(
                "<=PROPERTY_UF_END_EVENT"   =>  $dateCur,
                "!PROPERTY_UF_END_EVENT"    =>  self::ARCHIVE_XML_ID
            ),
            "!PROPERTY_UF_IS_ARCHIVE" =>  true
        );
        $res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        return $res;
    }

    /** Change events field UF_SUBJECTS to archive id subject
     * @return bool
     */
    public static function MoveEventsToArchive($eventId)
    {
        $update_result = CIBlockElement::SetPropertyValuesEx($eventId, false, array(UF_IS_ARCHIVE => self::ARCHIVE_XML_ID));
        return $update_result;
    }

} 