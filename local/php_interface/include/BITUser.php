<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 24.12.2015
 * Time: 18:39
 */

class BITUser
{
	function FixExportFromCSV(&$arFields)
	{
		define("LOG_FILENAME", $_SERVER["DOCUMENT_ROOT"]."/log.txt");
		$ar_fix_fields = array(
			"UF_DISCIPLINES",
			'UF_EDUCATION',
			'UF_ACADEMIC_DEGREE',
			'UF_ACADEMIC_TITLE',
			'UF_SPECIALITY',
			'UF_EXERITIES',
			'UF_RETRAINING' ,
			'UF_AWARDS',
			'UF_LANGUAGE'
		);
		foreach($arFields as $field_key=>$field_value)
		{
			if(in_array($field_key, $ar_fix_fields))
			{
				if(!is_array($field_value))
				{
					$arFields[$field_key] = explode("$", $field_value);
				}
			}
		}
		AddMessage2Log($arFields, "2");
	}
}