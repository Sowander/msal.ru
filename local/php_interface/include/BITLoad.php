<?php
/**
 * Created by PhpStorm.
 * User: Bondarenko Georg
 * Date: 31.07.2015
 * Time: 14:47
 */


use Bitrix\Highloadblock as HL;

class BITLoad {

    /***/
    public function InitLanguageSiteParams()
    {
        $GLOBALS["arSiteLanguageParams"] = self::GetCurrentLanguageAllParams();
        $GLOBALS["arAllLanguage"] = array();
        if(CModule::IncludeModule("bit_hl"))
        {
            $bit_lang = new \BIT\ORM\BITLang();
            $cur_lang = $bit_lang::getList(array(
                "select" => array('*'),
                "filter" => array()
            ));
            while($ar_cur_lang = $cur_lang->fetch())
            {
                $GLOBALS["arAllLanguage"][$ar_cur_lang["UF_XML_ID"]] = $ar_cur_lang["ID"];
            }
        }
    }

    public function InitBreadcrumbsNameExclusion()
    {
        $GLOBALS["breadcrumbsNameExclusion"] = array(
            "content"
        );
        if(strtoupper(LANGUAGE_ID) == "EN")
        {
            $GLOBALS["breadcrumbsNameExclusion"][] = "Главная";
        }
    }

    public  function InitBreadcrumbsChange()
    {
        $GLOBALS["breadcrumbsChange"] = array(
            "Справочник"  =>  "/handbook/?THEMES_LIST=Y"
        );
    }

    /** Prewev news and events filter */
    public function InitFilterByPreviewNewsAndEventsInLeftBar()
    {
        if(isset($GLOBALS["arSiteLanguageParams"]) &&
            !empty($GLOBALS["arSiteLanguageParams"]) &&
            isset($GLOBALS["arSiteLanguageParams"]["UF_XML_ID"]) &&
            strlen($GLOBALS["arSiteLanguageParams"]["UF_XML_ID"])>0
        )
        {
            $GLOBALS["arNewsAndEventsPreFilter"] = array(
                "PROPERTY_UF_LANGUAGE" => $GLOBALS["arSiteLanguageParams"]["UF_XML_ID"]
            );
        }
        else
        {
            $GLOBALS["arNewsAndEventsPreFilter"] = array(
                "PROPERTY_UF_LANGUAGE" => $GLOBALS["arFilterLanguage"]["PROPERTY_UF_LANGUAGE"]
            );
            if(strlen($GLOBALS["arNewsAndEventsPreFilter"]["PROPERTY_UF_LANGUAGE"])<=0)
            {
                $GLOBALS["arNewsAndEventsPreFilter"] = array(
                    "PROPERTY_UF_LANGUAGE" => self::GetLanguageXMLId()
                );
            }
        }
        $GLOBALS["arNewsAndEventsPreFilter"]["!PROPERTY_UF_IS_IMPOTANT"] = false;
    }

    /** All events list filter */
    public function InitFilterEventsAllList()
    {
        if(isset($GLOBALS["arSiteLanguageParams"]) &&
            !empty($GLOBALS["arSiteLanguageParams"]) &&
            isset($GLOBALS["arSiteLanguageParams"]["UF_XML_ID"]) &&
            strlen($GLOBALS["arSiteLanguageParams"]["UF_XML_ID"])>0
        )
        {
            $GLOBALS["arFilterListNewsAll"] = array(
                "UF_LANGUAGE" => $GLOBALS["arSiteLanguageParams"]["UF_XML_ID"]
            );
        }
        else
        {
            $GLOBALS["arFilterListNewsAll"] = array(
                "UF_LANGUAGE" => $GLOBALS["arFilterLanguage"]["PROPERTY_UF_LANGUAGE"]
            );
            if(strlen($GLOBALS["arFilterListNewsAll"]["UF_LANGUAGE"])<=0)
            {
                $GLOBALS["arFilterListNewsAll"] = array(
                    "UF_LANGUAGE" => self::GetLanguageXMLId()
                );
            }
        }
    }

    /**
     * Init site language global filter
     *
     */
    public function InitLanguageSiteFilter()
    {
        if(isset($GLOBALS["arSiteLanguageParams"]) &&
            !empty($GLOBALS["arSiteLanguageParams"]) &&
            isset($GLOBALS["arSiteLanguageParams"]["UF_XML_ID"]) &&
            strlen($GLOBALS["arSiteLanguageParams"]["UF_XML_ID"])>0
        )
        {
            $GLOBALS["arFilterLanguage"] = array(
                "PROPERTY_UF_LANGUAGE" => $GLOBALS["arSiteLanguageParams"]["UF_XML_ID"]
            );
        }
        else
        {
            $GLOBALS["arFilterLanguage"] = array(
                "PROPERTY_UF_LANGUAGE" => self::GetLanguageXMLId()
            );
        }
    }

    /**
     *  Get current language xml_id
     */
    public static function GetLanguageXMLId()
    {
        $lang_xml = false;
        if(CModule::IncludeModule("bit_hl"))
        {
            $bit_lang = new \BIT\ORM\BITLang();
            $cur_lang = $bit_lang::getList(array(
                "select" => array('UF_XML_ID'),
                "filter" => array(
                    "UF_LANG_ID" => LANGUAGE_ID
                )
            ));
            if($cur_lang_xml = $cur_lang->fetch())
            {
                $lang_xml = $cur_lang_xml["UF_XML_ID"];
            }
        }
        return $lang_xml;
    }

    public static function GetCurrentLanguageAllParams()
    {
        $ar_cur_lang = false;
        if(CModule::IncludeModule("bit_hl"))
        {
            $bit_lang = new \BIT\ORM\BITLang();
            $cur_lang = $bit_lang::getList(array(
                "select" => array('*'),
                "filter" => array(
                    "UF_LANG_ID" => LANGUAGE_ID
                )
            ));
            if($cur_lang_xml = $cur_lang->fetch())
            {
                $ar_cur_lang = $cur_lang_xml;
            }
        }
        return $ar_cur_lang;
    }

    public function SetAdminDateTimeFix()
    {
        global $MAIN_LANGS_ADMIN_CACHE;
        pre_dump($MAIN_LANGS_ADMIN_CACHE);
        echo CSite::GetDateFormat("SHORT");
    }

}
