<?php
/**
 * Created by PhpStorm.
 * User: Bondarenko Georg
 * Date: 20.08.2015
 * Time: 10:23
 */

if(isset($_GET["SELECT_STRUCTURE"]) && $_GET["SELECT_STRUCTURE"]=="Y")
{
        $request_data = $_REQUEST;
        $cur_lang = $request_data["UF_LANGUAGE"];
        unset($request_data["SELECT_STRUCTURE"]);
        unset($request_data["UF_LANGUAGE"]);
        if(!empty($request_data))
        {
            /** Include bitrix prolod */
            require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

            if(\CModule::IncludeModule("bit_hl"))
            {
                $response_data = array();
                $ar_section_structure = \BIT\ORM\BITTabsList::GetTabsByArraySectionsID($request_data,$cur_lang);
                foreach($ar_section_structure as $structure_section)
                {
                    $response_data[] = array(
                        "UF_SECTION_NAME"   =>  $structure_section["UF_SECTION_NAME"],
                        "SECTION_ID"        =>  $structure_section["SECTION_ID"],
                        "UF_TABS"           =>  empty($structure_section["UF_TABS"]) ? json_encode(array()) : $structure_section["UF_TABS"]
                    );
                }
                die(json_encode($response_data));
            }
        }
}

die("OK");

?>