/**
 * (c) Bit24, http://studiobit.ru
 * User: Bondarenko Georg
 * Date: 27.06.2016 9:56
 */

var Tools = function()
{

	this.arrMessages =
	{
		standart: "Уверены, что хотите это выполнить?",
		del_tab: "Уверены, что хотите удалить вкладку?",
	}

	this.confirmFormSend = function(form, messageCode)
	{
		var message = "";
		if(!messageCode)
		{
			message = this.arrMessages.standart;
		}
		else
		{
			message = this.arrMessages[messageCode];
		}
		if(confirm(message))
		{
			form.submit();
		}
		else
		{
			if (messageCode == "del_tab") {
				disableAllDelFields();
			}
			return false;
		}
	}

	this.onSubmitFormHandler = function(event, thisElem, message)
	{
		event = event || window.event;

		if (event.preventDefault)
		{
			event.preventDefault();
		}
		else
		{
			event.returnValue = false;
		}
		var objTool = new Tools();
		return objTool.confirmFormSend(thisElem, message);
	}

}

clikedSubmitIntutValue = false;

window.onload = function () {
	var submitDel = "del_tab";
	var innerForm = document.getElementById("form_element_3_form");
	if(innerForm)
	{
		innerForm.addEventListener("submit", function(event)
		{
			if(clikedSubmitIntutValue == submitDel)
			{
				clikedSubmitIntutValue = false;
				var objTool = new Tools();
				return objTool.onSubmitFormHandler(event, this, submitDel)
			}
		});
	}
	var eventForm = document.getElementById("form_element_6_form");
	if(eventForm)
	{
		eventForm.addEventListener("submit", function(event)
		{
			if(clikedSubmitIntutValue == submitDel)
			{
				clikedSubmitIntutValue = false;
				var objTool = new Tools();
				return objTool.onSubmitFormHandler(event, this, submitDel)
			}
		});
	}
}

function enableCurHidden(submitBtn) {
	clikedSubmitIntutValue = 'del_tab';

	submitBtn.nextElementSibling.removeAttribute("disabled");
}

function disableAllDelFields() {
	var elements = document.getElementById("table_tabs").querySelectorAll('input[type="hidden"]');
	if (elements.length > 0) {
		for (var i = 0; i < elements.length; ++i) {
			if (typeof elements[i] == "object") {
				elements[i].setAttribute("disabled", "disabled");
			}
		}
	}
}