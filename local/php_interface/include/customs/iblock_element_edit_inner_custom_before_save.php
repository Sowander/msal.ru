<?php
/**
 * Created by PhpStorm.
 * User: Bondarenko Georg
 * Date: 10.08.2015
 * Time: 14:54
 */

if($REQUEST_METHOD=="POST" && strlen($Update)>0 && $view!="Y" && (!$error) && empty($dontsave))
{

    /**
     *  error example
     *  $error = new _CIBlockError(2, "DESCRIPTION_REQUIRED", "Введите текст статьи");
     * @arError - error array, witch consist of error messages. This array will check from end of script.
    */
    $arErrors = array();
    /**
     *  Find tabs to delete
    */
    $arrKeysDelete = array("del_tab_id_");
    $tabsForDelete = get_compare_like_keys($_POST, $arrKeysDelete);
    /**
     *  Delete tabs
    */
    if(!empty($tabsForDelete))
    {
        foreach($tabsForDelete as $del_tab)
        {
            $delete_result = \BIT\ORM\BITTabsList::delete($del_tab["HL_ID"]);
            if(!$delete_result->isSuccess())
            {
                $arErrors[] = $delete_result->getErrorMessages();
            }
            $_POST["apply"] = "Применить";
        }
    }

    /**
     *  Find tabs to add new
     */
    $arrKeysAdd = array("HL_ID_0_TYPE_COUNT_", "HL_ID_0_NAME_COUNT_");
    $tabsForAdd = get_compare_like_keys($_POST, $arrKeysAdd);
    $arNewTabsFields = array();
    $arChangeFieldsToCreate = array(
        "HL_ID_0_TYPE_COUNT_"   =>  "UF_TAB_TYPE_ID",
        "HL_ID_0_NAME_COUNT_"   =>  "UF_NAME"
    );
    /**
     *  Add tabs
     */
    if(!empty($tabsForAdd))
    {
        foreach($tabsForAdd as $hashTab)
        {
            $hash = substr($hashTab["VALUE"], 0, strlen($hashTab["VALUE"])-strlen($hashTab["HL_ID"]));
            $arNewTabsFields[$hashTab["HL_ID"]][$arChangeFieldsToCreate[$hash]] = $_POST[$hashTab["VALUE"]];
            if(
                !$arNewTabsFields[$hashTab["HL_ID"]]["UF_IBLOCK_ELEMENT_ID"] ||
                !isset($arNewTabsFields[$hashTab["HL_ID"]]["UF_IBLOCK_ELEMENT_ID"])  ||
                strlen($arNewTabsFields[$hashTab["HL_ID"]]["UF_IBLOCK_ELEMENT_ID"])<=0
            )
            {
                $arNewTabsFields[$hashTab["HL_ID"]]["UF_IBLOCK_ELEMENT_ID"] = $_POST["ID"];
            }
            if(
                (!$arNewTabsFields[$hashTab["HL_ID"]]["UF_XML_ID"] ||
                !isset($arNewTabsFields[$hashTab["HL_ID"]]["UF_XML_ID"]) ||
                strlen($arNewTabsFields[$hashTab["HL_ID"]]["UF_XML_ID"])<=0) &&
                (
                $arChangeFieldsToCreate[$hash] == "UF_NAME"
                )
            )
            {
                $arParams = array("replace_space"=>"-","replace_other"=>"-");
                $trans = Cutil::translit($_POST[$hashTab["VALUE"]],"ru",$arParams);
                $rand = randString(5);
                $arNewTabsFields[$hashTab["HL_ID"]]["UF_XML_ID"] = $trans.rand(10,99).$rand;
            }
        }
        /**
         *  Adding new tabs in HL entity
        */
        foreach($arNewTabsFields as $ar_tab_fields)
        {
            $add_new_result = \BIT\ORM\BITTabsList::add($ar_tab_fields);
            if(!$add_new_result->isSuccess())
            {
                $arErrors[] = $add_new_result->getErrorMessages();
            }
        }
    }

    $arrKeysUpdate = array(
        "UF_HTML_TEXT_HLID_",
        "UF_NAME_HLID_",
        "UF_SORT_HLID_",
        "UF_SHOW_TYPE_HLID_",
        "UF_STRUCTURE_BAYAN_HLID_",
        "UF_EMPLOYEES_HLID_",
        "UF_GALLERY_HLID_",
        "UF_GALLERY_ISSET_HLID_",
        "UF_VIDEO_HLID_",
        "UF_VIDEO_ISSET_HLID_",
        "UF_LIST_HLID_",
        "STRUCTUREID_"
    );
    $tabsForUpdate = get_compare_like_keys($_POST, $arrKeysUpdate);
    $fileUpdate = get_compare_like_keys($_FILES, $arrKeysUpdate);
    $tabsForUpdate = array_merge($tabsForUpdate, $fileUpdate);
    $arChangeFieldsToUpdate = array(
        "UF_NAME_HLID_"             =>  "UF_NAME",
        "UF_SORT_HLID_"             =>  "UF_SORT",
        "UF_SHOW_TYPE_HLID_"        =>  "UF_SHOW_TYPE",
        "UF_STRUCTURE_BAYAN_HLID_"  =>  "UF_STRUCTURE_BAYAN",
        "UF_HTML_TEXT_HLID_"        =>  "UF_HTML_TEXT",
        "UF_EMPLOYEES_HLID_"        =>  "UF_EMPLOYEES",
        "UF_GALLERY_HLID_"          =>  "UF_GALLERY",
        "UF_GALLERY_ISSET_HLID_"    =>  "UF_GALLERY_ISSET",
        "UF_VIDEO_HLID_"            =>  "UF_VIDEO",
        "UF_VIDEO_ISSET_HLID_"      =>  "UF_VIDEO_ISSET",
        "UF_LIST_HLID_"             =>  "UF_LIST",
        "_STRUCTUREID_"             =>  "UF_STRUCTURE"
    );
    $arUpdateTabsFields = array();
    /**
     *  Update tabs
     */
    if(!empty($tabsForUpdate))
    {
        foreach($tabsForUpdate as $hashTab)
        {
            $hash = substr($hashTab["VALUE"], 0, strlen($hashTab["VALUE"])-strlen($hashTab["HL_ID"]));
            $arUpdateTabsFields[$hashTab["HL_ID"]][$arChangeFieldsToUpdate[$hash]] = $_POST[$hashTab["VALUE"]];
            /** Photos gallery fix */
            if($hash == "UF_GALLERY_HLID_")
            {
                $arUpdateTabsFields[$hashTab["HL_ID"]][$arChangeFieldsToUpdate[$hash]] = $_FILES[$hashTab["VALUE"]];
            }
            /** UF_LIST Fix */
            if($hash == "UF_LIST_HLID_")
            {
                $ar_uf_list_for_update = fix_uf_list_array_to_standart($hashTab["VALUE"],$hashTab["HL_ID"]);
                if($ar_uf_list_for_update)
                {
                    $arUpdateTabsFields[$ar_uf_list_for_update["HL_ID"]][$arChangeFieldsToUpdate[$hash]][$ar_uf_list_for_update["ID"]][$ar_uf_list_for_update["UF_FIELD"]] = $ar_uf_list_for_update["UF_VALUE"];
                }
                unset($ar_uf_list_for_update);
            }
            /** UF_STRUCTURE Fix */
            if(strpos($hashTab["VALUE"], "_STRUCTUREID_") !== false)
            {
                $temp_structure_data = fix_uf_structure_array_to_standart($hashTab["VALUE"], $_POST);
                $temp_hl_id = $temp_structure_data["HLID"];
                unset($temp_structure_data["HLID"]);
                $arUpdateTabsFields[$temp_hl_id][$arChangeFieldsToUpdate["_STRUCTUREID_"]][$temp_structure_data["ID"]] = $temp_structure_data;
                unset($temp_structure_data);
            }
            if
            (
                !$arUpdateTabsFields[$hashTab["HL_ID"]]["ID"] ||
                !isset($arUpdateTabsFields[$hashTab["HL_ID"]]["ID"]) ||
                strlen($arUpdateTabsFields[$hashTab["HL_ID"]]["ID"]) <=0
            )
            {
                $arUpdateTabsFields[$hashTab["HL_ID"]]["ID"] = $hashTab["HL_ID"];
            }
        }

        foreach($arUpdateTabsFields as $ar_tab_fields)
        {
            /** This is hack, which is associated with field UF_LIST  */
            if(intval($ar_tab_fields["ID"])<=0 || !isset($ar_tab_fields["ID"]) || strlen(intval($ar_tab_fields["ID"])) != strlen($ar_tab_fields["ID"]))
            {
                continue;
            }
            $tab_id = $ar_tab_fields["ID"];
            unset($ar_tab_fields["ID"]);
            $ar_employees_id = array();

            /** Employess */
            foreach($ar_tab_fields["UF_EMPLOYEES"] as $str_employee_id)
            {
                if(intval($str_employee_id) > 0)
                {
                    $ar_employees_id[] = intval($str_employee_id);
                }
            }
            $ar_tab_fields["UF_EMPLOYEES"] = serialize($ar_employees_id);

            /** Stricture sections bayan */
	        $ar_struct_bayan_id = array();
            foreach($ar_tab_fields["UF_STRUCTURE_BAYAN"] as $str_struct_bayan_id)
            {
                if(intval($str_struct_bayan_id) > 0)
                {
                    $ar_struct_bayan_id[] = intval($str_struct_bayan_id);
                }
            }
            $ar_tab_fields["UF_STRUCTURE_BAYAN"] = serialize($ar_struct_bayan_id);

            /** Photo*/
            $ar_gallery_photo_id = array();
            /** New add photos */
            foreach($ar_tab_fields["UF_GALLERY"]["tmp_name"] as $ar_gallery_photo)
            {
                $ar_image = CFile::MakeFileArray($ar_gallery_photo);
                $gallery_photo_id = CFile::SaveFile($ar_image, "main");
                if(intval($gallery_photo_id) > 0)
                {
                    $ar_gallery_photo_id[] = $gallery_photo_id;
                }
            }
            /** Old (is isset) photos */
            if(is_array($ar_tab_fields["UF_GALLERY_ISSET"]))
            {
                foreach($ar_tab_fields["UF_GALLERY_ISSET"] as $isset_imgs)
                {
                    if(intval($isset_imgs) > 0)
                    {
                        $ar_gallery_photo_id[] = intval($isset_imgs);
                    }
                }
            }
            else
            {
                if(intval($ar_tab_fields["UF_GALLERY_ISSET"])>0)
                {
                    $ar_gallery_photo_id[] = intval($ar_tab_fields["UF_GALLERY_ISSET"]);
                }
            }
            if(!empty($ar_gallery_photo_id))
            {
                $ar_tab_fields["UF_GALLERY"] = serialize($ar_gallery_photo_id);
            }
            else
            {
                unset($ar_tab_fields["UF_GALLERY"]);
            }
            unset($ar_tab_fields["UF_GALLERY_ISSET"]);
            /** Video */
            foreach($ar_tab_fields["UF_VIDEO_ISSET"] as $isset_video)
            {
                $ar_tab_fields["UF_VIDEO"][] = $isset_video;
            }
            unset($ar_tab_fields["UF_VIDEO_ISSET"]);
            $ar_video_str = array();
            foreach($ar_tab_fields["UF_VIDEO"] as $str_video)
            {
                if(strlen($str_video)>0)
                {
                    $ar_video_str[] = $str_video;
                }
            }
            if(!empty($ar_video_str))
            {
                $ar_tab_fields["UF_VIDEO"] = base64_encode(serialize($ar_video_str));
            }
            else
            {
                unset($ar_tab_fields["UF_VIDEO"]);
            }

            /** UF_LIST fix */
            $ar_tab_uf_list = array();
            if(!empty($ar_tab_fields["UF_LIST"]))
            {
                foreach($ar_tab_fields["UF_LIST"] as $list_id=>$ar_list_obj)
                {
                    /** new list obj */
                    if(stripos($list_id,"new") !== false)
                    {
                        /** add new list obj */
                        if($new_list_id = \BIT\ORM\BITListObj::add($ar_list_obj))
                        {
                            if(intval($new_list_id->GetId())>0)
                            {
                                $ar_tab_uf_list[] = intval($new_list_id->GetId());
                            }
                        }
                    }
                    else
                    {
                        /** update list obj */
                        $update_list_obj = \BIT\ORM\BITListObj::update($list_id,$ar_list_obj);
                        if(!$update_list_obj->isSuccess())
                        {
                            $arErrors[] = $update_list_obj->getErrorMessages();
                        }
                        $ar_tab_uf_list[] = intval($list_id);
                    }
                }
            }
            $ar_tab_fields["UF_LIST"] = serialize($ar_tab_uf_list);


            /** UF_STRUCTURE fix */
            $ar_tab_uf_structure = array();
            if(!empty($ar_tab_fields["UF_STRUCTURE"]))
            {
                foreach($ar_tab_fields["UF_STRUCTURE"] as $tab_structure_obj)
                {
                    if(isset($tab_structure_obj["ID"]))
                    {
                        $ar_temp_struct_tabs_list = array();
                        foreach($tab_structure_obj["UF_VALUES"] as $struct_tab_val)
                        {
                            if(intval($struct_tab_val)>0)
                            {
                                $ar_temp_struct_tabs_list[] = $struct_tab_val;
                            }
                        }
                        $tab_structure_obj["UF_VALUES"] = $ar_temp_struct_tabs_list;
                        unset($ar_temp_struct_tabs_list);
                        /**
                         *  Find element by section id and language
                        */
                        $iblock_id_to_save_structure = \BIT\ORM\BITStructureList::GetElementBySectionAndLanguage($tab_structure_obj["UF_SECTION_ID"],$_POST["PROP"][5][15]["VALUE"]);
                        if(!$iblock_id_to_save_structure)
                        {
                            $iblock_id_to_save_structure = NULL;
                        }
                        $lang_val_iblock = "ae2ogrFC";
                        foreach($_POST["PROP"][5] as $ar_prop)
                        {
                            if($ar_prop["VALUE"] == "ae2ogrFC" || $ar_prop["VALUE"] == "8slHBJtK")
                            {
                                $lang_val_iblock = $ar_prop["VALUE"];
                            }
                        }
                        $ar_update_structure = array(
                            "UF_LANGUAGE"               =>  $lang_val_iblock,
                            "UF_IBLOCK_SECTION_ID"      =>  $tab_structure_obj["UF_SECTION_ID"],
                            "UF_TABS"                   =>  serialize($tab_structure_obj["UF_VALUES"]),
                            "UF_IBLOCK_ELEMENT_ID"      =>  $iblock_id_to_save_structure
                        );
                        if(intval($tab_structure_obj["ID"])>0 && strlen($tab_structure_obj["ID"]) == strlen(intval($tab_structure_obj["ID"])))
                        {
                            $update_structure = \BIT\ORM\BITStructureList::update(intval($tab_structure_obj["ID"]),$ar_update_structure);
                            if(!$update_structure->isSuccess())
                            {
                                $arErrors[] = $update_structure->getErrorMessages();
                            }
                            else
                            {
                                $ar_tab_uf_structure[] = intval($tab_structure_obj["ID"]);
                            }
                            unset($update_structure);
                        }
                        else {
                            $add_new_structure = \BIT\ORM\BITStructureList::add($ar_update_structure);
                            if(intval($add_new_structure->GetID())>0)
                            {
                                $ar_tab_uf_structure[] = intval($add_new_structure->GetID());
                            }
                        }
                    }

                }

            }
            $ar_tab_fields["UF_STRUCTURE"] = serialize($ar_tab_uf_structure);

            $update_result = \BIT\ORM\BITTabsList::update($tab_id, $ar_tab_fields);

            if(!$update_result->isSuccess())
            {
                $arErrors[] = $update_result->getErrorMessages();
            }
        }
    }

    /** Check errors */
    if(!empty($arErrors))
    {
        $str_error_text = "";
        $str_error_code = "DESCRIPTION_REQUIRED";
        foreach($arErrors as $one_error)
        {
            $str_error_text = $str_error_text.$one_error."<br>";
        }
        $error = new _CIBlockError(2, $str_error_code, $str_error_text);
//        pre_dump($arErrors);
//        die();
    }
}

/**
 *  Returns array(list) witch consist of values like $arCompareKeys consist of
 * and consist of $arSearch
 */
function get_compare_like_keys($arSearch, $arCompareKeys)
{
    $arKeys = array_keys($arSearch);
    $ar_return_keys = array();
    foreach($arKeys as $key)
    {
        if($exist_key = get_like_val_in_array($key, $arCompareKeys))
        {
            $ar_return_keys = array_merge($ar_return_keys, $exist_key);
        }
    }
    return $ar_return_keys;
}

/**
 *  Return array, witch consist of values like $valSearch
 *  This values exist in $arrSearch (search in $arrSearch array)
 */
function get_like_val_in_array($valSearch, $arrSearch)
{
    $ar_return_vals = array();
    foreach($arrSearch as $like_val)
    {
        if(strpos($valSearch,$like_val)!==false)
        {
            $ar_return_vals[] = array(
                "VALUE" =>  $valSearch,
                "HL_ID" =>  str_replace($like_val, "", $valSearch)
            );
        }
    }
    if(empty($ar_return_vals))
    {
        return false;
    }
    else
    {
        return $ar_return_vals;
    }
}

/**
 *  Fix UF_LIST array to standart
 *  Compare by HL ID and LIST ID
 *  return array(
 *  "ID"        =>  list object id or new_$uf_list_new_counter
 *  "HL_ID"     =>  highload id (tab)
 *  "UF_FIELD"  =>  field name (UF_NAME or UF_TEXT)
 *  "UF_VALUE"  =>  field value (value of UF_NAME or UF_TEXT)
 * )
 */
function fix_uf_list_array_to_standart($fieldFromPostArName, $fieldHash)
{
    static $uf_list_new_counter = 0;
    $ar_return_list_fields = array();
    $arFieldsToExplode = array("UF_NAME","UF_TEXT");
    foreach($arFieldsToExplode as $field_to_explode)
    {
        if(strripos($fieldHash, $field_to_explode) !== false)
        {
            $pieces = explode("_".$field_to_explode."_LISTID_", $fieldHash);
            $ar_return_list_fields["HL_ID"] = $pieces[0];
            if(intval($pieces[1])>0 && strlen($pieces[1]) == strlen(intval($pieces[1])))
            {
                $ar_return_list_fields["ID"] = $pieces[1];
            }
            else
            {
                $ar_return_list_fields["ID"] = "new".$uf_list_new_counter;
            }
            $ar_return_list_fields["UF_FIELD"] = $field_to_explode;
            $ar_return_list_fields["UF_VALUE"] = $_POST[$fieldFromPostArName];
            break;
        }
    }
    if(empty($ar_return_list_fields))
    {
        $ar_return_list_fields = false;
    }
    else
    {
        $uf_list_new_counter++;
    }
    return $ar_return_list_fields;
}

/**
 *  Fix Structure array to standart
 *  Compare by HL ID and STRUCTURE ID
 *  return array(
 *  "ID"            =>  structure object id or new_$counter
 *  "HL_ID"         =>  highload id (tab)
 *  "UF_SECTION_ID" =>  section id (iblock section)
 *  "UF_VALUE"      =>  values in list
 * )
*/
function fix_uf_structure_array_to_standart($fieldFromPostArName, $postValues)
{
    static $uf_structure_new_counter = 0;
    $ar_return_structure_fields = array();
    $arFieldsToExplode = array("HLID_","_STRUCTUREID_","_SECTIONID_");
    $arFieldsToReturn = array(
        "HLID_"             =>  "HLID",
        "_STRUCTUREID_"     =>  "STRUCTUREID",
        "_SECTIONID_"       =>  "SECTIONID"
    );
    $temp_split_str = explode("HLID_", $fieldFromPostArName);
    $temp_split_str = explode("_STRUCTUREID_", $temp_split_str[1]);
    $ar_return_structure_fields["HLID"] = $temp_split_str[0];
    $temp_split_str = explode("_SECTIONID_", $temp_split_str[1]);
    if(intval($temp_split_str[0] > 0))
    {
        $ar_return_structure_fields["ID"] = $temp_split_str[0];
    }
    else
    {
        $ar_return_structure_fields["ID"] = "new".$uf_structure_new_counter;
        $uf_structure_new_counter++;
    }
    $ar_return_structure_fields["UF_SECTION_ID"] = $temp_split_str[1];
    foreach($postValues[$fieldFromPostArName] as $val)
    {
        $ar_return_structure_fields["UF_VALUES"][] = intval($val);
    }
    return $ar_return_structure_fields;

}

?>