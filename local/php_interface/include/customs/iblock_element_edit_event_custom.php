<?php
/**
 * Created by PhpStorm.
 * User: Bondarenko Georg
 * Date: 10.08.2015
 * Time: 11:56
 */

use Bitrix\Main\Loader;
use Bitrix\Iblock;
//////////////////////////
//START of the custom form
//////////////////////////

/* Logic section START */

if(CModule::IncludeModule("bit_hl"))
{
    $ar_tabs_types_temp = \BIT\ORM\BITTypeTab::GetAllTypes();
    $ar_tabs_types = array();
    $ar_tabs_list = \BIT\ORM\BITTabsList::GetListByIBElementID($ID);
    foreach($ar_tabs_types_temp as $tab_type_temp)
    {
        if(in_array(6,$tab_type_temp["UF_IBLOCK_SHOW"]))
        {
            $ar_tabs_types[$tab_type_temp["ID"]] = $tab_type_temp;
        }
    }
}
/** Get show types */
$obEnum = new CUserFieldEnum;
$rsEnum = $obEnum->GetList(array(), array("USER_FIELD_ID" => 185));
$arShowListTypes = array();
while($arEnum = $rsEnum->GetNext()){
	$arShowListTypes[] = $arEnum;
}
unset($obEnum);
unset($rsEnum);
unset($arEnum);
/** show types and */
/* Logic section END */

/*  Reinit tabs START  */

unset($tabControl);

foreach($ar_tabs_list as $key=>$uf_tab)
{
    $aTabs[] = array(
        'DIV'   => 'tab'.$key,
        'TAB'   => $uf_tab["UF_NAME"],
        'ICON'  => '',
        'TITLE' => $uf_tab["UF_NAME"],
    );
}
$tabControl->tabs = $aTabs;
$tabControl = new CAdminForm("form_element_".$IBLOCK_ID, $aTabs, !$bPropertyAjax, $denyAutosave);
/** This is deleting "do not need" tabs */
unset($tabControl->tabs[2]);

$customTabber = new CAdminTabEngine("OnAdminIBlockElementEdit", array("ID" => $ID, "IBLOCK"=>$arIBlock, "IBLOCK_TYPE"=>$arIBTYPE));
$tabControl->AddTabs($customTabber);
$temp_tabs = $tabControl->tabs;
unset($tabControl->tabs);
foreach($temp_tabs as $temp_cur_tab)
{
    $tabControl->tabs[] = $temp_cur_tab;
}
/* Reinit tabs END */

//We have to explicitly call calendar and editor functions because
//first output may be discarded by form settings

$tabControl->BeginPrologContent();
CJSCore::Init(array('date'));

if(COption::GetOptionString("iblock", "use_htmledit", "Y")=="Y" && $bFileman)
{
    //TODO:This dirty hack will be replaced by special method like calendar do
    echo '<div style="display:none">';
    CFileMan::AddHTMLEditorFrame(
        "SOME_TEXT",
        "",
        "SOME_TEXT_TYPE",
        "text",
        array(
            'height' => 450,
            'width' => '100%'
        ),
        "N",
        0,
        "",
        "",
        $arIBlock["LID"]
    );
    echo '</div>';
}

if($arTranslit["TRANSLITERATION"] == "Y")
{
    CJSCore::Init(array('translit'));
    ?>
    <script type="text/javascript">
        var linked=<?if($bLinked) echo 'true'; else echo 'false';?>;
        function set_linked()
        {
            linked=!linked;

            var name_link = document.getElementById('name_link');
            if(name_link)
            {
                if(linked)
                    name_link.src='/bitrix/themes/.default/icons/iblock/link.gif';
                else
                    name_link.src='/bitrix/themes/.default/icons/iblock/unlink.gif';
            }
            var code_link = document.getElementById('code_link');
            if(code_link)
            {
                if(linked)
                    code_link.src='/bitrix/themes/.default/icons/iblock/link.gif';
                else
                    code_link.src='/bitrix/themes/.default/icons/iblock/unlink.gif';
            }
            var linked_state = document.getElementById('linked_state');
            if(linked_state)
            {
                if(linked)
                    linked_state.value='Y';
                else
                    linked_state.value='N';
            }
        }
        var oldValue = '';
        function transliterate()
        {
            if(linked)
            {
                var from = document.getElementById('NAME');
                var to = document.getElementById('CODE');
                if(from && to && oldValue != from.value)
                {
                    BX.translit(from.value, {
                        'max_len' : <?echo intval($arTranslit['TRANS_LEN'])?>,
                        'change_case' : '<?echo $arTranslit['TRANS_CASE']?>',
                        'replace_space' : '<?echo $arTranslit['TRANS_SPACE']?>',
                        'replace_other' : '<?echo $arTranslit['TRANS_OTHER']?>',
                        'delete_repeat_replace' : <?echo $arTranslit['TRANS_EAT'] == 'Y'? 'true': 'false'?>,
                        'use_google' : <?echo $arTranslit['USE_GOOGLE'] == 'Y'? 'true': 'false'?>,
                        'callback' : function(result){to.value = result; setTimeout('transliterate()', 250); }
                    });
                    oldValue = from.value;
                }
                else
                {
                    setTimeout('transliterate()', 250);
                }
            }
            else
            {
                setTimeout('transliterate()', 250);
            }
        }
        transliterate();
    </script>
<?
}
?>
    <script type="text/javascript">
        var InheritedPropertiesTemplates = new JCInheritedPropertiesTemplates(
            '<?echo $tabControl->GetName()?>_form',
            '/bitrix/admin/iblock_templates.ajax.php?ENTITY_TYPE=E&IBLOCK_ID=<?echo intval($IBLOCK_ID)?>&ENTITY_ID=<?echo intval($ID)?>'
        );
        BX.ready(function(){
            setTimeout(function(){
                InheritedPropertiesTemplates.updateInheritedPropertiesTemplates(true);
            }, 1000);
        });
    </script>
<?
$tabControl->EndPrologContent();

$tabControl->BeginEpilogContent();

echo bitrix_sessid_post();
echo GetFilterHiddens("find_");?>
    <input type="hidden" name="linked_state" id="linked_state" value="<?if($bLinked) echo 'Y'; else echo 'N';?>">
    <input type="hidden" name="Update" value="Y">
    <input type="hidden" name="from" value="<?echo htmlspecialcharsbx($from)?>">
    <input type="hidden" name="WF" value="<?echo htmlspecialcharsbx($WF)?>">
    <input type="hidden" name="return_url" value="<?echo htmlspecialcharsbx($return_url)?>">
<?if($ID>0 && !$bCopy)
{
    ?><input type="hidden" name="ID" value="<?echo $ID?>"><?
}
if ($bCopy)
{
    ?><input type="hidden" name="copyID" value="<? echo $ID; ?>"><?
}
if ($bCatalog)
    CCatalogAdminTools::showFormParams();
?>
    <input type="hidden" name="IBLOCK_SECTION_ID" value="<?echo intval($IBLOCK_SECTION_ID)?>">
    <input type="hidden" name="TMP_ID" value="<?echo intval($TMP_ID)?>">
<?
$tabControl->EndEpilogContent();

$customTabber->SetErrorState($bVarsFromForm);

$arEditLinkParams = array(
    "find_section_section" => intval($find_section_section)
);
if ($bAutocomplete)
{
    $arEditLinkParams['lookup'] = $strLookup;
}

$tabControl->Begin(array(
    "FORM_ACTION" => "/bitrix/admin/".CIBlock::GetAdminElementEditLink($IBLOCK_ID, null, $arEditLinkParams)
));

$tabControl->BeginNextFormTab();
if($ID > 0 && !$bCopy)
{
    $p = CIblockElement::GetByID($ID);
    $pr = $p->ExtractFields("prn_");
}
else
{
    $pr = array();
}
$tabControl->AddCheckBoxField("ACTIVE", GetMessage("IBLOCK_FIELD_ACTIVE").":", false, array("Y","N"), $str_ACTIVE=="Y");
$tabControl->BeginCustomField("ACTIVE_FROM", GetMessage("IBLOCK_FIELD_ACTIVE_PERIOD_FROM"), $arIBlock["FIELDS"]["ACTIVE_FROM"]["IS_REQUIRED"] === "Y");
?>
    <tr id="tr_ACTIVE_FROM">
        <td><?echo $tabControl->GetCustomLabelHTML()?>:</td>
        <td><?echo CAdminCalendar::CalendarDate("ACTIVE_FROM", $str_ACTIVE_FROM, 19, true)?></td>
    </tr>
<?
$tabControl->EndCustomField("ACTIVE_FROM", '<input type="hidden" id="ACTIVE_FROM" name="ACTIVE_FROM" value="'.$str_ACTIVE_FROM.'">');
$tabControl->BeginCustomField("ACTIVE_TO", GetMessage("IBLOCK_FIELD_ACTIVE_PERIOD_TO"), $arIBlock["FIELDS"]["ACTIVE_TO"]["IS_REQUIRED"] === "Y");
?>
    <tr id="tr_ACTIVE_TO">
        <td><?echo $tabControl->GetCustomLabelHTML()?>:</td>
        <td><?echo CAdminCalendar::CalendarDate("ACTIVE_TO", $str_ACTIVE_TO, 19, true)?></td>
    </tr>

<?
$tabControl->EndCustomField("ACTIVE_TO", '<input type="hidden" id="ACTIVE_TO" name="ACTIVE_TO" value="'.$str_ACTIVE_TO.'">');

if($arTranslit["TRANSLITERATION"] == "Y")
{
    $tabControl->BeginCustomField("NAME", GetMessage("IBLOCK_FIELD_NAME").":", true);
    ?>
    <tr id="tr_NAME">
        <td><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td style="white-space: nowrap;">
            <input type="text" size="50" name="NAME" id="NAME" maxlength="255" value="<?echo $str_NAME?>"><image id="name_link" title="<?echo GetMessage("IBEL_E_LINK_TIP")?>" class="linked" src="/bitrix/themes/.default/icons/iblock/<?if($bLinked) echo 'link.gif'; else echo 'unlink.gif';?>" onclick="set_linked()" />
        </td>
    </tr>
    <?
    $tabControl->EndCustomField("NAME",
        '<input type="hidden" name="NAME" id="NAME" value="'.$str_NAME.'">'
    );

    $tabControl->BeginCustomField("CODE", GetMessage("IBLOCK_FIELD_CODE").":", $arIBlock["FIELDS"]["CODE"]["IS_REQUIRED"] === "Y");
    ?>
    <tr id="tr_CODE">
        <td><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td style="white-space: nowrap;">
            <input type="text" size="50" name="CODE" id="CODE" maxlength="255" value="<?echo $str_CODE?>"><image id="code_link" title="<?echo GetMessage("IBEL_E_LINK_TIP")?>" class="linked" src="/bitrix/themes/.default/icons/iblock/<?if($bLinked) echo 'link.gif'; else echo 'unlink.gif';?>" onclick="set_linked()" />
        </td>
    </tr>
    <?
    $tabControl->EndCustomField("CODE",
        '<input type="hidden" name="CODE" id="CODE" value="'.$str_CODE.'">'
    );
}
else
{
    $tabControl->AddEditField("NAME", GetMessage("IBLOCK_FIELD_NAME").":", true, array("size" => 50, "maxlength" => 255), $str_NAME);
    $tabControl->AddEditField("CODE", GetMessage("IBLOCK_FIELD_CODE").":", $arIBlock["FIELDS"]["CODE"]["IS_REQUIRED"] === "Y", array("size" => 20, "maxlength" => 255), $str_CODE);
}

if (
    $arShowTabs['sections']
    && $arIBlock["FIELDS"]["IBLOCK_SECTION"]["DEFAULT_VALUE"]["KEEP_IBLOCK_SECTION_ID"] === "Y"
)
{
    $arDropdown = array();
    if ($str_IBLOCK_ELEMENT_SECTION)
    {
        $sectionList = CIBlockSection::GetList(
            array("left_margin"=>"asc"),
            array("=ID"=>$str_IBLOCK_ELEMENT_SECTION),
            false,
            array("ID", "NAME")
        );
        while ($section = $sectionList->Fetch())
            $arDropdown[$section["ID"]] = $section["NAME"];
    }
    $tabControl->BeginCustomField("IBLOCK_ELEMENT_SECTION_ID", GetMessage("IBEL_E_MAIN_IBLOCK_SECTION_ID").":", false);
    ?>
    <tr id="tr_IBLOCK_ELEMENT_SECTION_ID">
        <td class="adm-detail-valign-top"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td>
            <div id="RESULT_IBLOCK_ELEMENT_SECTION_ID">
                <select name="IBLOCK_ELEMENT_SECTION_ID" id="IBLOCK_ELEMENT_SECTION_ID" onchange="InheritedPropertiesTemplates.updateInheritedPropertiesValues(false, true)">
                    <?foreach($arDropdown as $key => $val):?>
                        <option value="<?echo $key?>" <?if ($str_IBLOCK_SECTION_ID == $key) echo 'selected'?>><?echo $val?></option>
                    <?endforeach?>
                </select>
            </div>
            <script>
                window.ipropTemplates[window.ipropTemplates.length] = {
                    "ID": "IBLOCK_ELEMENT_SECTION_ID",
                    "INPUT_ID": "IBLOCK_ELEMENT_SECTION_ID",
                    "RESULT_ID": "RESULT_IBLOCK_ELEMENT_SECTION_ID",
                    "TEMPLATE": ""
                };
                window.ipropTemplates[window.ipropTemplates.length] = {
                    "ID": "CODE",
                    "INPUT_ID": "CODE",
                    "RESULT_ID": "",
                    "TEMPLATE": ""
                };
                window.ipropTemplates[window.ipropTemplates.length] = {
                    "ID": "XML_ID",
                    "INPUT_ID": "XML_ID",
                    "RESULT_ID": "",
                    "TEMPLATE": ""
                };
            </script>
        </td>
    </tr>
    <?
    $tabControl->EndCustomField("IBLOCK_ELEMENT_SECTION_ID",
        '<input type="hidden" name="IBLOCK_ELEMENT_SECTION_ID" id="IBLOCK_ELEMENT_SECTION_ID" value="'.$str_IBLOCK_SECTION_ID.'">'
    );
}

if(COption::GetOptionString("iblock", "show_xml_id", "N")=="Y")
    $tabControl->AddEditField("XML_ID", GetMessage("IBLOCK_FIELD_XML_ID").":", $arIBlock["FIELDS"]["XML_ID"]["IS_REQUIRED"] === "Y", array("size" => 20, "maxlength" => 255, "id" => "XML_ID"), $str_XML_ID);

$tabControl->AddEditField("SORT", GetMessage("IBLOCK_FIELD_SORT").":", $arIBlock["FIELDS"]["SORT"]["IS_REQUIRED"] === "Y", array("size" => 7, "maxlength" => 10), $str_SORT);

if(!empty($PROP)):
    if ($arIBlock["SECTION_PROPERTY"] === "Y" || defined("CATALOG_PRODUCT"))
    {
        $arPropLinks = array("IBLOCK_ELEMENT_PROP_VALUE");
        if(is_array($str_IBLOCK_ELEMENT_SECTION) && !empty($str_IBLOCK_ELEMENT_SECTION))
        {
            foreach($str_IBLOCK_ELEMENT_SECTION as $section_id)
            {
                foreach(CIBlockSectionPropertyLink::GetArray($IBLOCK_ID, $section_id) as $PID => $arLink)
                    $arPropLinks[$PID] = "PROPERTY_".$PID;
            }
        }
        else
        {
            foreach(CIBlockSectionPropertyLink::GetArray($IBLOCK_ID, 0) as $PID => $arLink)
                $arPropLinks[$PID] = "PROPERTY_".$PID;
        }
        $tabControl->AddFieldGroup("IBLOCK_ELEMENT_PROPERTY", GetMessage("IBLOCK_ELEMENT_PROP_VALUE"), $arPropLinks, $bPropertyAjax);
    }

    $tabControl->AddSection("IBLOCK_ELEMENT_PROP_VALUE", GetMessage("IBLOCK_ELEMENT_PROP_VALUE"));

    foreach($PROP as $prop_code=>$prop_fields):
        $prop_values = $prop_fields["VALUE"];
        $tabControl->BeginCustomField("PROPERTY_".$prop_fields["ID"], $prop_fields["NAME"], $prop_fields["IS_REQUIRED"]==="Y");
        ?>
        <?if($prop_fields["ID"] == 18):?>
        <?if(intval($ID)>0):?>
            <tr id="tr_PROPERTY_<?echo $prop_fields["ID"];?>" class="heading">
                <td colspan="2" style="background-color: #f5f9f9;">
                    <?echo $prop_fields["NAME"];?>:
                </td>
            </tr>
            <tr id="tabs_elements">
                <td colspan="2" style="text-align: center;">
                    <table class="internal" id="hlb_directory_table" style="margin: 0 auto;">
                        <tbody id="table_tabs">
                        <tr class="heading">
                            <td>Название</td>
                            <td>Тип</td>
                            <td style="vertical-align: top;">
                                <input type="button" value="Добавить вкладку" id="addNewTR">
                                <script>
                                    BX.ready(function(){
                                        var counter = 0;
                                        BX.bind(BX('addNewTR'), 'click', function() {
                                            addEmptyTabConf();
                                        });
                                        function addEmptyTabConf()
                                        {
                                            $str = '<td><input type="text" name="HL_ID_0_NAME_COUNT_'+counter+'" value="" size="35" maxlength="255" style="width:90%"></td>';
                                            $str += '<td><select name="HL_ID_0_TYPE_COUNT_'+counter+'" style="margin-bottom:3px">';
                                            //$str += '<option value="" selected="">(не установлено)</option>';
                                            <?foreach($ar_tabs_types as $tab_type):?>
                                            $str += '<option value="<?=$tab_type["ID"]?>" <?if($tab_type["ID"] == 1):?> selected=""<?endif;?>><?=$tab_type["UF_NAME"]?></option>';
                                            <?endforeach;?>
                                            $str += '</select></td><td></td>';
                                            console.log($str);
                                            var node = document.createElement("TR");
                                            node.innerHTML = $str;
                                            node.setAttribute('id', "hlbl_property_tr_0");
                                            var element = document.getElementById("table_tabs");
                                            element.appendChild(node);
                                            counter++;
                                        }
                                    });

                                </script>
                            </td>
                        </tr>
                        <!-- Isset tabs -->
                        <?foreach($ar_tabs_list as $uf_tab):?>
                            <tr id="hlbl_property_tr_<?=$uf_tab["ID"]?>">
                            <td style="vertical-align: top;">
                                <span><?=$uf_tab["UF_NAME"]?></span>
                            </td>
                            <td style="vertical-align: top;">
                                <span><?=$ar_tabs_types[$uf_tab["UF_TAB_TYPE_ID"]]["UF_NAME"]?></span>

                            </td>
                            <td style="vertical-align: top;">
                                <input type="submit" class="button" name="del_tab_id_<?=$uf_tab["ID"]?>" id="del_tab" value="Удалить вкладку"
                                       onclick="enableCurHidden(this);">
                                <input type="hidden" name="del_tab_id_<?=$uf_tab["ID"]?>" value="Y" disabled="disabled"/>
                            </td>
                        </tr>
                        <?endforeach;?>
                        </tbody>
                    </table>
                </td>
            </tr>
        <?endif;?>
        <?elseif($prop_fields["ID"] == 19 || $prop_fields["ID"] == 21):?>
            <tr id="tr_PROPERTY_<?echo $prop_fields["ID"];?>"<?if ($prop_fields["PROPERTY_TYPE"]=="F"):?> class="adm-detail-file-row"<?endif?>>
                <td class="adm-detail-valign-top" width="40%"><?if($prop_fields["HINT"]!=""):
                        ?><span id="hint_<?echo $prop_fields["ID"];?>"></span><script type="text/javascript">BX.hint_replace(BX('hint_<?echo $prop_fields["ID"];?>'), '<?echo CUtil::JSEscape($prop_fields["HINT"])?>');</script>&nbsp;<?
                    endif;?><?echo $tabControl->GetCustomLabelHTML();?>:</td>
                <td width="60%">
                    <?
                    $ar_all_persone_fields = \BIT\ORM\BITEventPersonList::getMap();
                    $excluded_field = \BIT\ORM\BITEventPersonList::GetExecludedFields();
                    ?>
                    <select name="<?='PROP['.$prop_fields["ID"].'][]'?>" size="<?=count($ar_all_persone_fields)?>" multiple="" onchange="onSectionChanged()">
                        <?foreach($ar_all_persone_fields as $field_key=>$person_field):?>
                            <?if(!$person_field["primary"] && !in_array($field_key, $excluded_field)):?>
                                <option
                                    value="<?=$field_key?>"
                                    <?if(in_array($field_key,$prop_fields["VALUE"])):?>selected=""<?endif;?>>
                                    <?=$person_field["NAME"]?>
                                </option>
                            <?endif;?>
                        <?endforeach;?>
                    </select>
                </td>
            </tr>
        <?elseif($prop_fields["ID"] == 17 && intval($ID)<=0):?>
		    <tr id="tr_PROPERTY_17">
			    <td class="adm-detail-valign-top adm-detail-content-cell-l" width="40%">
				    <span class="adm-required-field">Язык</span>:</td>
			    <td width="60%" class="adm-detail-content-cell-r">
				    <table cellpadding="0" cellspacing="0" border="0" class="nopadding" width="100%" id="tb7195fd7094d305b4987af966f093b6a7"><tbody>
					    <tr><td><select name="PROP[17][n0][VALUE]">
								    <option value="8slHBJtK">Eng [2]</option>
								    <option value="ae2ogrFC" selected>Рус [1]</option>
							    </select></td></tr></tbody>
				    </table></td>
		    </tr>
	    <?else:?>
            <tr id="tr_PROPERTY_<?echo $prop_fields["ID"];?>"<?if ($prop_fields["PROPERTY_TYPE"]=="F"):?> class="adm-detail-file-row"<?endif?>>
                <td class="adm-detail-valign-top" width="40%"><?if($prop_fields["HINT"]!=""):
                        ?><span id="hint_<?echo $prop_fields["ID"];?>"></span><script type="text/javascript">BX.hint_replace(BX('hint_<?echo $prop_fields["ID"];?>'), '<?echo CUtil::JSEscape($prop_fields["HINT"])?>');</script>&nbsp;<?
                    endif;?><?echo $tabControl->GetCustomLabelHTML();?>:</td>
                <td width="60%"><?_ShowPropertyField('PROP['.$prop_fields["ID"].']', $prop_fields, $prop_fields["VALUE"], (($historyId <= 0) && (!$bVarsFromForm) && ($ID<=0) && (!$bPropertyAjax)), $bVarsFromForm||$bPropertyAjax, 50000, $tabControl->GetFormName(), $bCopy);?></td>
            </tr>
        <?endif;?>
        <?
        $hidden = "";
        if(!is_array($prop_fields["~VALUE"]))
            $values = Array();
        else
            $values = $prop_fields["~VALUE"];
        $start = 1;
        foreach($values as $key=>$val)
        {
            if($bCopy)
            {
                $key = "n".$start;
                $start++;
            }

            if(is_array($val) && array_key_exists("VALUE",$val))
            {
                $hidden .= _ShowHiddenValue('PROP['.$prop_fields["ID"].']['.$key.'][VALUE]', $val["VALUE"]);
                $hidden .= _ShowHiddenValue('PROP['.$prop_fields["ID"].']['.$key.'][DESCRIPTION]', $val["DESCRIPTION"]);
            }
            else
            {
                $hidden .= _ShowHiddenValue('PROP['.$prop_fields["ID"].']['.$key.'][VALUE]', $val);
                $hidden .= _ShowHiddenValue('PROP['.$prop_fields["ID"].']['.$key.'][DESCRIPTION]', "");
            }
        }
        $tabControl->EndCustomField("PROPERTY_".$prop_fields["ID"], $hidden);
    endforeach;?>
<?endif;

if (!$bAutocomplete)
{
    $rsLinkedProps = CIBlockProperty::GetList(array(), array(
        "PROPERTY_TYPE" => "E",
        "LINK_IBLOCK_ID" => $IBLOCK_ID,
        "ACTIVE" => "Y",
        "FILTRABLE" => "Y",
    ));
    $arLinkedProp = $rsLinkedProps->GetNext();
    if($arLinkedProp)
    {
        $tabControl->BeginCustomField("LINKED_PROP", GetMessage("IBLOCK_ELEMENT_EDIT_LINKED"));
        ?>
        <tr class="heading" id="tr_LINKED_PROP">
            <td colspan="2"><?echo $tabControl->GetCustomLabelHTML();?></td>
        </tr>
        <?
        do {
            $elements_name = CIBlock::GetArrayByID($arLinkedProp["IBLOCK_ID"], "ELEMENTS_NAME");
            if(strlen($elements_name) <= 0)
                $elements_name = GetMessage("IBLOCK_ELEMENT_EDIT_ELEMENTS");
            ?>
            <tr id="tr_LINKED_PROP<?echo $arLinkedProp["ID"]?>">
                <td colspan="2"><a href="<?echo htmlspecialcharsbx(CIBlock::GetAdminElementListLink($arLinkedProp["IBLOCK_ID"], array('set_filter'=>'Y', 'find_el_property_'.$arLinkedProp["ID"]=>$ID, 'find_section_section' => -1)))?>"><?echo CIBlock::GetArrayByID($arLinkedProp["IBLOCK_ID"], "NAME").": ".$elements_name?></a></td>
            </tr>
        <?
        } while ($arLinkedProp = $rsLinkedProps->GetNext());
        $tabControl->EndCustomField("LINKED_PROP", "");
    }
}

$tabControl->BeginNextFormTab();
$tabControl->BeginCustomField("PREVIEW_PICTURE", GetMessage("IBLOCK_FIELD_PREVIEW_PICTURE"), $arIBlock["FIELDS"]["PREVIEW_PICTURE"]["IS_REQUIRED"] === "Y");
if($bVarsFromForm && !array_key_exists("PREVIEW_PICTURE", $_REQUEST) && $arElement)
	$str_PREVIEW_PICTURE = intval($arElement["PREVIEW_PICTURE"]);
?>
	<tr id="tr_PREVIEW_PICTURE" class="adm-detail-file-row">
		<td width="40%" class="adm-detail-valign-top"><?echo $tabControl->GetCustomLabelHTML()?>:</td>
		<td width="60%">
			<?if($historyId > 0):?>
				<?echo CFileInput::Show("PREVIEW_PICTURE", $str_PREVIEW_PICTURE, array(
					"IMAGE" => "Y",
					"PATH" => "Y",
					"FILE_SIZE" => "Y",
					"DIMENSIONS" => "Y",
					"IMAGE_POPUP" => "Y",
					"MAX_SIZE" => array(
						"W" => COption::GetOptionString("iblock", "detail_image_size"),
						"H" => COption::GetOptionString("iblock", "detail_image_size"),
					),
				));
				?>
			<?else:?>
				<?
				if (class_exists('\Bitrix\Main\UI\FileInput', true))
				{
					echo \Bitrix\Main\UI\FileInput::createInstance(array(
							"name" => "PREVIEW_PICTURE",
							"description" => true,
							"upload" => true,
							"allowUpload" => "I",
							"medialib" => true,
							"fileDialog" => true,
							"cloud" => true,
							"delete" => true,
							"maxCount" => 1
						))->show($str_PREVIEW_PICTURE);
				}
				else
				{
					echo CFileInput::Show("PREVIEW_PICTURE", ($ID > 0 && !$bCopy? $str_PREVIEW_PICTURE: 0),
						array(
							"IMAGE" => "Y",
							"PATH" => "Y",
							"FILE_SIZE" => "Y",
							"DIMENSIONS" => "Y",
							"IMAGE_POPUP" => "Y",
							"MAX_SIZE" => array(
								"W" => COption::GetOptionString("iblock", "detail_image_size"),
								"H" => COption::GetOptionString("iblock", "detail_image_size"),
							),
						), array(
							'upload' => true,
							'medialib' => true,
							'file_dialog' => true,
							'cloud' => true,
							'del' => true,
							'description' => true,
						)
					);
				}
				?>
			<?endif?>
		</td>
	</tr>
<?
$tabControl->EndCustomField("PREVIEW_PICTURE", "");
$tabControl->BeginCustomField("PREVIEW_TEXT", GetMessage("IBLOCK_FIELD_PREVIEW_TEXT"), $arIBlock["FIELDS"]["PREVIEW_TEXT"]["IS_REQUIRED"] === "Y");
?>
	<tr class="heading" id="tr_PREVIEW_TEXT_LABEL">
		<td colspan="2"><?echo $tabControl->GetCustomLabelHTML()?></td>
	</tr>
	<?if($ID && $PREV_ID && $bWorkflow):?>
	<tr id="tr_PREVIEW_TEXT_DIFF">
		<td colspan="2">
			<div style="width:95%;background-color:white;border:1px solid black;padding:5px">
				<?echo getDiff($prev_arElement["PREVIEW_TEXT"], $arElement["PREVIEW_TEXT"])?>
			</div>
		</td>
	</tr>
	<?elseif(COption::GetOptionString("iblock", "use_htmledit", "Y")=="Y" && $bFileman):?>
	<tr id="tr_PREVIEW_TEXT_EDITOR">
		<td colspan="2" align="center">
			<?CFileMan::AddHTMLEditorFrame(
			"PREVIEW_TEXT",
			$str_PREVIEW_TEXT,
			"PREVIEW_TEXT_TYPE",
			$str_PREVIEW_TEXT_TYPE,
			array(
				'height' => 450,
				'width' => '100%'
			),
			"N",
			0,
			"",
			"",
			$arIBlock["LID"],
			true,
			false,
			array(
				'toolbarConfig' => CFileman::GetEditorToolbarConfig("iblock_".(defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1 ? 'public' : 'admin')),
				'saveEditorKey' => $IBLOCK_ID,
				'hideTypeSelector' => $arIBlock["FIELDS"]["PREVIEW_TEXT_TYPE_ALLOW_CHANGE"]["DEFAULT_VALUE"] === "N",
			)
			);?>
		</td>
	</tr>
	<?else:?>
	<tr id="tr_PREVIEW_TEXT_TYPE">
		<td><?echo GetMessage("IBLOCK_DESC_TYPE")?></td>
		<td><input type="radio" name="PREVIEW_TEXT_TYPE" id="PREVIEW_TEXT_TYPE_text" value="text"<?if($str_PREVIEW_TEXT_TYPE!="html")echo " checked"?>> <label for="PREVIEW_TEXT_TYPE_text"><?echo GetMessage("IBLOCK_DESC_TYPE_TEXT")?></label> / <input type="radio" name="PREVIEW_TEXT_TYPE" id="PREVIEW_TEXT_TYPE_html" value="html"<?if($str_PREVIEW_TEXT_TYPE=="html")echo " checked"?>> <label for="PREVIEW_TEXT_TYPE_html"><?echo GetMessage("IBLOCK_DESC_TYPE_HTML")?></label></td>
	</tr>
	<tr id="tr_PREVIEW_TEXT">
		<td colspan="2" align="center">
			<textarea cols="60" rows="10" name="PREVIEW_TEXT" style="width:100%"><?echo $str_PREVIEW_TEXT?></textarea>
		</td>
	</tr>
	<?endif;
$tabControl->EndCustomField("PREVIEW_TEXT",
	'<input type="hidden" name="PREVIEW_TEXT" value="'.$str_PREVIEW_TEXT.'">'.
	'<input type="hidden" name="PREVIEW_TEXT_TYPE" value="'.$str_PREVIEW_TEXT_TYPE.'">'
);

$tabControl->BeginNextFormTab();
?>
<?
$tabControl->BeginCustomField("IPROPERTY_TEMPLATES_ELEMENT_META_TITLE", GetMessage("IBEL_E_SEO_META_TITLE"));
?>
    <tr class="adm-detail-valign-top">
        <td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%"><?echo IBlockInheritedPropertyInput($IBLOCK_ID, "ELEMENT_META_TITLE", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))?></td>
    </tr>
<?
$tabControl->EndCustomField("IPROPERTY_TEMPLATES_ELEMENT_META_TITLE",
    IBlockInheritedPropertyHidden($IBLOCK_ID, "ELEMENT_META_TITLE", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))
);
?>
<?
$tabControl->BeginCustomField("IPROPERTY_TEMPLATES_ELEMENT_META_KEYWORDS", GetMessage("IBEL_E_SEO_META_KEYWORDS"));
?>
    <tr class="adm-detail-valign-top">
        <td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%"><?echo IBlockInheritedPropertyInput($IBLOCK_ID, "ELEMENT_META_KEYWORDS", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))?></td>
    </tr>
<?
$tabControl->EndCustomField("IPROPERTY_TEMPLATES_ELEMENT_META_KEYWORDS",
    IBlockInheritedPropertyHidden($IBLOCK_ID, "ELEMENT_META_KEYWORDS", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))
);
?>
<?
$tabControl->BeginCustomField("IPROPERTY_TEMPLATES_ELEMENT_META_DESCRIPTION", GetMessage("IBEL_E_SEO_META_DESCRIPTION"));
?>
    <tr class="adm-detail-valign-top">
        <td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%"><?echo IBlockInheritedPropertyInput($IBLOCK_ID, "ELEMENT_META_DESCRIPTION", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))?></td>
    </tr>
<?
$tabControl->EndCustomField("IPROPERTY_TEMPLATES_ELEMENT_META_DESCRIPTION",
    IBlockInheritedPropertyHidden($IBLOCK_ID, "ELEMENT_META_DESCRIPTION", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))
);
?>
<?
$tabControl->BeginCustomField("IPROPERTY_TEMPLATES_ELEMENT_PAGE_TITLE", GetMessage("IBEL_E_SEO_ELEMENT_TITLE"));
?>
    <tr class="adm-detail-valign-top">
        <td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%"><?echo IBlockInheritedPropertyInput($IBLOCK_ID, "ELEMENT_PAGE_TITLE", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))?></td>
    </tr>
<?
$tabControl->EndCustomField("IPROPERTY_TEMPLATES_ELEMENT_PAGE_TITLE",
    IBlockInheritedPropertyHidden($IBLOCK_ID, "ELEMENT_PAGE_TITLE", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))
);
?>
<?
$tabControl->AddSection("IPROPERTY_TEMPLATES_ELEMENTS_PREVIEW_PICTURE", GetMessage("IBEL_E_SEO_FOR_ELEMENTS_PREVIEW_PICTURE"));
$tabControl->BeginCustomField("IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_ALT", GetMessage("IBEL_E_SEO_FILE_ALT"));
?>
    <tr class="adm-detail-valign-top">
        <td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%"><?echo IBlockInheritedPropertyInput($IBLOCK_ID, "ELEMENT_PREVIEW_PICTURE_FILE_ALT", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))?></td>
    </tr>
<?
$tabControl->EndCustomField("IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_ALT",
    IBlockInheritedPropertyHidden($IBLOCK_ID, "ELEMENT_PREVIEW_PICTURE_FILE_ALT", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))
);
?>
<?
$tabControl->BeginCustomField("IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_TITLE", GetMessage("IBEL_E_SEO_FILE_TITLE"));
?>
    <tr class="adm-detail-valign-top">
        <td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%"><?echo IBlockInheritedPropertyInput($IBLOCK_ID, "ELEMENT_PREVIEW_PICTURE_FILE_TITLE", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))?></td>
    </tr>
<?
$tabControl->EndCustomField("IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_TITLE",
    IBlockInheritedPropertyHidden($IBLOCK_ID, "ELEMENT_PREVIEW_PICTURE_FILE_TITLE", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))
);
?>
<?
$tabControl->BeginCustomField("IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_NAME", GetMessage("IBEL_E_SEO_FILE_NAME"));
?>
    <tr class="adm-detail-valign-top">
        <td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%"><?echo IBlockInheritedPropertyInput($IBLOCK_ID, "ELEMENT_PREVIEW_PICTURE_FILE_NAME", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))?></td>
    </tr>
<?
$tabControl->EndCustomField("IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_NAME",
    IBlockInheritedPropertyHidden($IBLOCK_ID, "ELEMENT_PREVIEW_PICTURE_FILE_NAME", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))
);
?>
<?
$tabControl->AddSection("IPROPERTY_TEMPLATES_ELEMENTS_DETAIL_PICTURE", GetMessage("IBEL_E_SEO_FOR_ELEMENTS_DETAIL_PICTURE"));
$tabControl->BeginCustomField("IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_ALT", GetMessage("IBEL_E_SEO_FILE_ALT"));
?>
    <tr class="adm-detail-valign-top">
        <td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%"><?echo IBlockInheritedPropertyInput($IBLOCK_ID, "ELEMENT_DETAIL_PICTURE_FILE_ALT", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))?></td>
    </tr>
<?
$tabControl->EndCustomField("IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_ALT",
    IBlockInheritedPropertyHidden($IBLOCK_ID, "ELEMENT_DETAIL_PICTURE_FILE_ALT", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))
);
?>
<?
$tabControl->BeginCustomField("IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_TITLE", GetMessage("IBEL_E_SEO_FILE_TITLE"));
?>
    <tr class="adm-detail-valign-top">
        <td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%"><?echo IBlockInheritedPropertyInput($IBLOCK_ID, "ELEMENT_DETAIL_PICTURE_FILE_TITLE", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))?></td>
    </tr>
<?
$tabControl->EndCustomField("IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_TITLE",
    IBlockInheritedPropertyHidden($IBLOCK_ID, "ELEMENT_DETAIL_PICTURE_FILE_TITLE", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))
);
?>
<?
$tabControl->BeginCustomField("IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_NAME", GetMessage("IBEL_E_SEO_FILE_NAME"));
?>
    <tr class="adm-detail-valign-top">
        <td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%"><?echo IBlockInheritedPropertyInput($IBLOCK_ID, "ELEMENT_DETAIL_PICTURE_FILE_NAME", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))?></td>
    </tr>
<?
$tabControl->EndCustomField("IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_NAME",
    IBlockInheritedPropertyHidden($IBLOCK_ID, "ELEMENT_DETAIL_PICTURE_FILE_NAME", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))
);
?>
<?
$tabControl->AddSection("SEO_ADDITIONAL", GetMessage("IBLOCK_EL_TAB_MO"));
$tabControl->BeginCustomField("TAGS", GetMessage("IBLOCK_FIELD_TAGS").":", $arIBlock["FIELDS"]["TAGS"]["IS_REQUIRED"] === "Y");
?>
    <tr id="tr_TAGS">
        <td><?echo $tabControl->GetCustomLabelHTML()?><br><?echo GetMessage("IBLOCK_ELEMENT_EDIT_TAGS_TIP")?></td>
        <td>
            <?if(Loader::includeModule('search')):
                $arLID = array();
                $rsSites = CIBlock::GetSite($IBLOCK_ID);
                while($arSite = $rsSites->Fetch())
                    $arLID[] = $arSite["LID"];
                echo InputTags("TAGS", htmlspecialcharsback($str_TAGS), $arLID, 'size="55"');
            else:?>
                <input type="text" size="20" name="TAGS" maxlength="255" value="<?echo $str_TAGS?>">
            <?endif?>
        </td>
    </tr>
<?
$tabControl->EndCustomField("TAGS",
    '<input type="hidden" name="TAGS" value="'.$str_TAGS.'">'
);

?>



<?if($arShowTabs['sections']):
    $tabControl->BeginNextFormTab();

    $tabControl->BeginCustomField("SECTIONS", GetMessage("IBLOCK_SECTION"), $arIBlock["FIELDS"]["IBLOCK_SECTION"]["IS_REQUIRED"] === "Y");
    ?>
    <tr id="tr_SECTIONS">
    <?if($arIBlock["SECTION_CHOOSER"] != "D" && $arIBlock["SECTION_CHOOSER"] != "P"):?>

        <?$l = CIBlockSection::GetTreeList(Array("IBLOCK_ID"=>$IBLOCK_ID), array("ID", "NAME", "DEPTH_LEVEL"));?>
        <td width="40%" class="adm-detail-valign-top"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%">
            <select name="IBLOCK_SECTION[]" size="14" multiple onchange="onSectionChanged()">
                <option value="0"<?if(is_array($str_IBLOCK_ELEMENT_SECTION) && in_array(0, $str_IBLOCK_ELEMENT_SECTION))echo " selected"?>><?echo GetMessage("IBLOCK_UPPER_LEVEL")?></option>
                <?
                while($ar_l = $l->GetNext()):
                    ?><option value="<?echo $ar_l["ID"]?>"<?if(is_array($str_IBLOCK_ELEMENT_SECTION) && in_array($ar_l["ID"], $str_IBLOCK_ELEMENT_SECTION))echo " selected"?>><?echo str_repeat(" . ", $ar_l["DEPTH_LEVEL"])?><?echo $ar_l["NAME"]?></option><?
                endwhile;
                ?>
            </select>
        </td>

    <?elseif($arIBlock["SECTION_CHOOSER"] == "D"):?>
        <td width="40%" class="adm-detail-valign-top"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%">
        <table class="internal" id="sections">
        <?
        if(is_array($str_IBLOCK_ELEMENT_SECTION))
        {
            $i = 0;
            foreach($str_IBLOCK_ELEMENT_SECTION as $section_id)
            {
                $rsChain = CIBlockSection::GetNavChain($IBLOCK_ID, $section_id);
                $strPath = "";
                while($arChain = $rsChain->Fetch())
                    $strPath .= $arChain["NAME"]."&nbsp;/&nbsp;";
                if(strlen($strPath) > 0)
                {
                    ?><tr>
                    <td nowrap><?echo $strPath?></td>
                    <td>
                        <input type="button" value="<?echo GetMessage("MAIN_DELETE")?>" OnClick="deleteRow(this)">
                        <input type="hidden" name="IBLOCK_SECTION[]" value="<?echo intval($section_id)?>">
                    </td>
                    </tr><?
                }
                $i++;
            }
        }
        ?>
        <tr>
            <td>
                <script type="text/javascript">
                    function deleteRow(button)
                    {
                        var my_row = button.parentNode.parentNode;
                        var table = document.getElementById('sections');
                        if(table)
                        {
                            for(var i=0; i<table.rows.length; i++)
                            {
                                if(table.rows[i] == my_row)
                                {
                                    table.deleteRow(i);
                                    onSectionChanged();
                                }
                            }
                        }
                    }
                    function addPathRow()
                    {
                        var table = document.getElementById('sections');
                        if(table)
                        {
                            var section_id = 0;
                            var html = '';
                            var lev = 0;
                            var oSelect;
                            while(oSelect = document.getElementById('select_IBLOCK_SECTION_'+lev))
                            {
                                if(oSelect.value < 1)
                                    break;
                                html += oSelect.options[oSelect.selectedIndex].text+'&nbsp;/&nbsp;';
                                section_id = oSelect.value;
                                lev++;
                            }
                            if(section_id > 0)
                            {
                                var cnt = table.rows.length;
                                var oRow = table.insertRow(cnt-1);

                                var i=0;
                                var oCell = oRow.insertCell(i++);
                                oCell.innerHTML = html;

                                var oCell = oRow.insertCell(i++);
                                oCell.innerHTML =
                                    '<input type="button" value="<?echo GetMessage("MAIN_DELETE")?>" OnClick="deleteRow(this)">'+
                                    '<input type="hidden" name="IBLOCK_SECTION[]" value="'+section_id+'">';
                                onSectionChanged();
                            }
                        }
                    }
                    function find_path(item, value)
                    {
                        if(item.id==value)
                        {
                            var a = Array(1);
                            a[0] = item.id;
                            return a;
                        }
                        else
                        {
                            for(var s in item.children)
                            {
                                if(ar = find_path(item.children[s], value))
                                {
                                    var a = Array(1);
                                    a[0] = item.id;
                                    return a.concat(ar);
                                }
                            }
                            return null;
                        }
                    }
                    function find_children(level, value, item)
                    {
                        if(level==-1 && item.id==value)
                            return item;
                        else
                        {
                            for(var s in item.children)
                            {
                                if(ch = find_children(level-1,value,item.children[s]))
                                    return ch;
                            }
                            return null;
                        }
                    }
                    function change_selection(name_prefix, prop_id,value,level,id)
                    {
                        var lev = level+1;
                        var oSelect;

                        while(oSelect = document.getElementById(name_prefix+lev))
                        {
                            jsSelectUtils.deleteAllOptions(oSelect);
                            jsSelectUtils.addNewOption(oSelect, '0', '(<?echo GetMessage("MAIN_NO")?>)');
                            lev++;
                        }

                        oSelect = document.getElementById(name_prefix+(level+1))
                        if(oSelect && (value!=0||level==-1))
                        {
                            var item = find_children(level,value,window['sectionListsFor'+prop_id]);
                            for(var s in item.children)
                            {
                                var obj = item.children[s];
                                jsSelectUtils.addNewOption(oSelect, obj.id, obj.name, true);
                            }
                        }
                        if(document.getElementById(id))
                            document.getElementById(id).value = value;
                    }
                    function init_selection(name_prefix, prop_id, value, id)
                    {
                        var a = find_path(window['sectionListsFor'+prop_id], value);
                        change_selection(name_prefix, prop_id, 0, -1, id);
                        for(var i=1;i<a.length;i++)
                        {
                            if(oSelect = document.getElementById(name_prefix+(i-1)))
                            {
                                for(var j=0;j<oSelect.length;j++)
                                {
                                    if(oSelect[j].value==a[i])
                                    {
                                        oSelect[j].selected=true;
                                        break;
                                    }
                                }
                            }
                            change_selection(name_prefix, prop_id, a[i], i-1, id);
                        }
                    }
                    var sectionListsFor0 = {id:0,name:'',children:Array()};

                    <?
                    $rsItems = CIBlockSection::GetTreeList(Array("IBLOCK_ID"=>$IBLOCK_ID), array("ID", "NAME", "DEPTH_LEVEL"));
                    $depth = 0;
                    $max_depth = 0;
                    $arChain = array();
                    while($arItem = $rsItems->GetNext())
                    {
                        if($max_depth < $arItem["DEPTH_LEVEL"])
                        {
                            $max_depth = $arItem["DEPTH_LEVEL"];
                        }
                        if($depth < $arItem["DEPTH_LEVEL"])
                        {
                            $arChain[]=$arItem["ID"];

                        }
                        while($depth > $arItem["DEPTH_LEVEL"])
                        {
                            array_pop($arChain);
                            $depth--;
                        }
                        $arChain[count($arChain)-1] = $arItem["ID"];
                        echo "sectionListsFor0";
                        foreach($arChain as $i)
                            echo ".children['".intval($i)."']";

                        echo " = { id : ".$arItem["ID"].", name : '".CUtil::JSEscape($arItem["NAME"])."', children : Array() };\n";
                        $depth = $arItem["DEPTH_LEVEL"];
                    }
                    ?>
                </script>
                <?
                for($i = 0; $i < $max_depth; $i++)
                    echo '<select id="select_IBLOCK_SECTION_'.$i.'" onchange="change_selection(\'select_IBLOCK_SECTION_\',  0, this.value, '.$i.', \'IBLOCK_SECTION[n'.$key.']\')"><option value="0">('.GetMessage("MAIN_NO").')</option></select>&nbsp;';
                ?>
                <script type="text/javascript">
                    init_selection('select_IBLOCK_SECTION_', 0, '', 0);
                </script>
            </td>
            <td><input type="button" value="<?echo GetMessage("IBLOCK_ELEMENT_EDIT_PROP_ADD")?>" onClick="addPathRow()"></td>
        </tr>
        </table>
        </td>

    <?else:?>
        <td width="40%" class="adm-detail-valign-top"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%">
            <table id="sections" class="internal">
                <?
                if(is_array($str_IBLOCK_ELEMENT_SECTION))
                {
                    $i = 0;
                    foreach($str_IBLOCK_ELEMENT_SECTION as $section_id)
                    {
                        $rsChain = CIBlockSection::GetNavChain($IBLOCK_ID, $section_id);
                        $strPath = "";
                        while($arChain = $rsChain->GetNext())
                            $strPath .= $arChain["NAME"]."&nbsp;/&nbsp;";
                        if(strlen($strPath) > 0)
                        {
                            ?><tr>
                            <td><?echo $strPath?></td>
                            <td>
                                <input type="button" value="<?echo GetMessage("MAIN_DELETE")?>" OnClick="deleteRow(this)">
                                <input type="hidden" name="IBLOCK_SECTION[]" value="<?echo intval($section_id)?>">
                            </td>
                            </tr><?
                        }
                        $i++;
                    }
                }
                ?>
            </table>
            <script type="text/javascript">
                function deleteRow(button)
                {
                    var my_row = button.parentNode.parentNode;
                    var table = document.getElementById('sections');
                    if(table)
                    {
                        for(var i=0; i<table.rows.length; i++)
                        {
                            if(table.rows[i] == my_row)
                            {
                                table.deleteRow(i);
                                onSectionChanged();
                            }
                        }
                    }
                }
                function InS<?echo md5("input_IBLOCK_SECTION")?>(section_id, html)
                {
                    var table = document.getElementById('sections');
                    if(table)
                    {
                        if(section_id > 0 && html)
                        {
                            var cnt = table.rows.length;
                            var oRow = table.insertRow(cnt-1);

                            var i=0;
                            var oCell = oRow.insertCell(i++);
                            oCell.innerHTML = html;

                            var oCell = oRow.insertCell(i++);
                            oCell.innerHTML =
                                '<input type="button" value="<?echo GetMessage("MAIN_DELETE")?>" OnClick="deleteRow(this)">'+
                                '<input type="hidden" name="IBLOCK_SECTION[]" value="'+section_id+'">';
                            onSectionChanged();
                        }
                    }
                }
            </script>
            <input name="input_IBLOCK_SECTION" id="input_IBLOCK_SECTION" type="hidden">
            <input type="button" value="<?echo GetMessage("IBLOCK_ELEMENT_EDIT_PROP_ADD")?>..." onClick="jsUtils.OpenWindow('/bitrix/admin/iblock_section_search.php?lang=<?echo LANGUAGE_ID?>&amp;IBLOCK_ID=<?echo $IBLOCK_ID?>&amp;n=input_IBLOCK_SECTION&amp;m=y', 600, 500);">
        </td>
    <?endif;?>
    </tr>
    <input type="hidden" name="IBLOCK_SECTION[]" value="">
    <script type="text/javascript">
        function onSectionChanged()
        {
            var form = BX('<?echo CUtil::JSEscape($tabControl->GetFormName())?>');
            var url = '<?echo CUtil::JSEscape($APPLICATION->GetCurPageParam())?>';
            <?if($arIBlock["SECTION_PROPERTY"] === "Y" || defined("CATALOG_PRODUCT")):?>
            var groupField = new JCIBlockGroupField(form, 'tr_IBLOCK_ELEMENT_PROPERTY', url);
            groupField.reload();
            <?endif?>
            <?if($arIBlock["FIELDS"]["IBLOCK_SECTION"]["DEFAULT_VALUE"]["KEEP_IBLOCK_SECTION_ID"] === "Y"):?>
            InheritedPropertiesTemplates.updateInheritedPropertiesValues(false, true);
            <?endif?>
            return;
        }
    </script>
    <?
    $hidden = "";
    if(is_array($str_IBLOCK_ELEMENT_SECTION))
        foreach($str_IBLOCK_ELEMENT_SECTION as $section_id)
            $hidden .= '<input type="hidden" name="IBLOCK_SECTION[]" value="'.intval($section_id).'">';
    $tabControl->EndCustomField("SECTIONS", $hidden);
endif;?>
<?
/*  CUSTOM TABS START  */
//pre_dump($ar_tabs_list);
foreach($ar_tabs_list as $key=>$cus_tab)
{
    $name = "TAB_ID_".$cus_tab['ID'];
    $tab_name = "TAB_ENTINY_".$cus_tab['ID']."_ID";
    $edit_field_name = "";
    $tabControl->BeginNextFormTab();
    $tabControl->BeginCustomField($name, $tab_name, false);
    ?>
    <tr id="tr_<?=$tab_name?>">
        <td class="adm-detail-content-cell-l">
            <span class="adm-required-field">Название вкладки:</span>
        </td>
        <td style="white-space: nowrap;" class="adm-detail-content-cell-r">
            <?$edit_field_name = "UF_NAME_HLID_".$cus_tab["ID"];?>
            <input type="text" size="50" name="<?=$edit_field_name?>" maxlength="255" value="<?=$cus_tab["UF_NAME"]?>">
        </td>
    </tr>
    <tr>
        <td class="adm-detail-content-cell-l">
            <span class="">Сортировка вкладки:</span>
        </td>
        <td style="white-space: nowrap;" class="adm-detail-content-cell-r">
            <?$edit_field_sort = "UF_SORT_HLID_".$cus_tab["ID"];?>
            <input type="text" size="50" name="<?=$edit_field_sort?>" maxlength="255" value="<?=$cus_tab["UF_SORT"]?>">
        </td>
    </tr>
	<tr>
		<td class="adm-detail-content-cell-l">
			<span class="">Вариант отображения вкладки:</span>
		</td>
		<td style="white-space: nowrap;" class="adm-detail-content-cell-r">
			<?$edit_field_show_type = "UF_SHOW_TYPE_HLID_".$cus_tab["ID"];?>
			<select name="<?=$edit_field_show_type?>">
				<?foreach($arShowListTypes as $show_type_tab_tmp):?>
					<option
						value="<?=$show_type_tab_tmp["ID"]?>"
					    <?if($cus_tab["UF_SHOW_TYPE"]==$show_type_tab_tmp["ID"]):?>
					        selected
					    <?elseif($show_type_tab_tmp["DEF"] == "Y" && intval($show_type_tab_tmp["ID"])<=0):?>
							selected
						<?endif;?>
						>
						<?=$show_type_tab_tmp["VALUE"]?>
						</option>
				<?endforeach?>
			</select>
		</td>
	</tr>
    <?
    /**
    * Switch tab type
    *
    *   TEXT
    */?>
    <?if($ar_tabs_types[$cus_tab["UF_TAB_TYPE_ID"]]["UF_CODE"] == "TEXT"):?>
    <?$edit_field_name = "UF_HTML_TEXT_HLID_".$cus_tab["ID"];?>
    <tr class="heading" id="tr_TEXT_EDITOR_NAME_HL_<?=$cus_tab["ID"]?>">
        <td colspan="2" align="center">
            <span>Контент (text/html):</span>
        </td>
    </tr>
    <tr id="tr_UF_HTML_TEXT_HLID_<?=$cus_tab["ID"]?>">
        <td colspan="2" align="center">
            <?CFileMan::AddHTMLEditorFrame(
                $edit_field_name,
                $cus_tab['UF_HTML_TEXT'],
                "UF_HTML_TEXT_TYPE",
                'html',
                array(
                    'height' => 450,
                    'width' => '100%'
                ),
                "N",
                0,
                "",
                "",
                $arIBlock["LID"],
                false,
                false,
                array(
                    'toolbarConfig' => CFileman::GetEditorToolbarConfig("iblock_".(defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1 ? 'public' : 'admin')),
                    'saveEditorKey' => $cus_tab['ID'],
                )
            );
            ?>
        </td>
    </tr>
    <?endif;?>
    <?/**   GALLERY   */?>
    <?if($ar_tabs_types[$cus_tab["UF_TAB_TYPE_ID"]]["UF_CODE"] == "GALLERY"):?>
    <table  class="internal" style="margin: 0 auto;">
    <tr class="heading">
        <td align="center">
            <span>Фото:</span>
        </td>
        <td align="center">
            <span>Видео:</span>
        </td>
    </tr>
    <tr>
    <td style="vertical-align: top;">
        <?$edit_field_name = "UF_GALLERY_HLID_".$cus_tab["ID"];?>
    <table  class="internal" style="margin: 0 auto;" >
        <tr id="add_new_photo_hlid_<?=$cus_tab["ID"]?>_count_0">
            <td></td>
            <td>
                <span class="adm-input-file">
                    <span id="file_upload_name_0">Добавить фото</span>
                    <input type="file" class="adm-designed-file add_photo" name="<?=$edit_field_name?>[]" onchange="addNewInputFile(this);">
                </span>
            </td>
        </tr>
        <script>

            var input_photo_counter = 0;
            function addNewInputFile(input)
            {
                var str_name = input.value;
                var str_id = "file_upload_name_"+input_photo_counter;
                var elem = document.getElementById(str_id);
                elem.innerText = str_name;
                this.value=null;
                var parent_id = "add_new_photo_hlid_<?=$cus_tab["ID"]?>_count_"+input_photo_counter;
                console.log(parent_id);
                var parent = document.getElementById(parent_id);
                input_photo_counter++;
                var html = '<tr id="add_new_photo_hlid_<?=$cus_tab["ID"]?>_count_'+input_photo_counter+'"><td></td><td><span class="adm-input-file"><span id="file_upload_name_'+input_photo_counter+'">Добавить фото</span>';
                html += '<input type="file" class="adm-designed-file add_photo" name="<?=$edit_field_name?>[]" onchange="addNewInputFile(this);">';
                html += '</span></td></tr>';
                parent.insertAdjacentHTML('beforebegin', html);

                return false;
            }
        </script>
        <?$edit_field_name = "UF_GALLERY_ISSET_HLID_".$cus_tab["ID"]."[]";?>
        <?foreach($cus_tab["UF_GALLERY"] as $photo):?>
            <?if($photo > 0):?>
                <tr id="TR_PHOTO_HLID_<?=$cus_tab["ID"]?>_ID_<?=$photo?>">
                    <td>
                        <?echo CFileInput::Show($edit_field_name, $photo, array(
                            "IMAGE" => "Y",
                            "PATH" => "Y",
                            "FILE_SIZE" => "Y",
                            "DIMENSIONS" => "Y",
                            "IMAGE_POPUP" => "Y",
                            "MAX_SIZE" => array(
                                "W" => COption::GetOptionString("iblock", "detail_image_size"),
                                "H" => COption::GetOptionString("iblock", "detail_image_size"),
                            ),
                        ));
                        ?>
                    </td>
                    <td>
                        <input type="button" value="Удалить" id="delete_hlid_<?=$cus_tab["ID"]?>_photo_<?=$photo?>">
                        <input type="hidden" name="<?=$edit_field_name?>" value="<?=$photo?>">
                        <script>
                            BX.bind(BX('delete_hlid_<?=$cus_tab["ID"]?>_photo_<?=$photo?>'), 'click', function() {
                                console.log(<?=$photo?>);
                                var element = document.getElementById("TR_PHOTO_HLID_<?=$cus_tab["ID"]?>_ID_<?=$photo?>");
                                element.outerHTML = "";
                                delete element;
                            });
                        </script>
                    </td>
                </tr>
            <?endif?>
        <?endforeach;?>
    </table>
    </td>
    <td style="vertical-align: top;">
        <?$edit_field_name = "UF_VIDEO_HLID_".$cus_tab["ID"];?>
        <table  class="internal" style="margin: 0 auto;">
            <tr id="add_new_video_hlid_<?=$cus_tab["ID"]?>">
                <td></td>
                <td>
                <span class="adm-input-file">
                    <span>Добавить видео</span>
                </span>
                </td>
            </tr>
            <script>
                BX.bind(BX('add_new_video_hlid_<?=$cus_tab["ID"]?>'), 'click', function() {
                    var parent = document.getElementById("add_new_video_hlid_<?=$cus_tab["ID"]?>");
                    var html = '<tr><td style="white-space: nowrap;" class="adm-detail-content-cell-r" colspan="2">';
                    html += '<input type="text" size="50" name="<?=$edit_field_name?>[]" maxlength="255" value="">';
                    html += "</td></tr>";
                    parent.insertAdjacentHTML('afterend', html);
                });
            </script>
            <tr>
                <td style="white-space: nowrap;" class="adm-detail-content-cell-r" colspan="2">
                    <input type="text" size="50" name="<?=$edit_field_name?>[]" maxlength="255" value="">
                </td>
            </tr>
            <script>
                BX.bind(BX('delete_hlid_<?=$cus_tab["ID"]?>_video_<?=$video_count?>'), 'click', function() {
                    var element = document.getElementById("TR_VIDEO_HLID_<?=$cus_tab["ID"]?>_ID_<?=$video_count?>");
                    element.outerHTML = "";
                    delete element;
                });
            </script>
            <?$edit_field_name = "UF_VIDEO_ISSET_HLID_".$cus_tab["ID"]."[]";?>
            <?$video_count = 0;?>
            <?foreach($cus_tab["UF_VIDEO"] as $video):?>
                <?if(strlen($video) > 0):?>
                    <tr id="TR_VIDEO_HLID_<?=$cus_tab["ID"]?>_ID_<?=$video_count?>">
                        <td>
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:player",
                                "",
                                Array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "PLAYER_TYPE" => "",
                                    "USE_PLAYLIST" => "N",
                                    "PATH" => $video,
                                    "PROVIDER" => "",
                                    "STREAMER" => "",
                                    "WIDTH" => "400",
                                    "HEIGHT" => "300",
                                    "PREVIEW" => "",
                                    "FILE_TITLE" => "",
                                    "FILE_DURATION" => "",
                                    "FILE_AUTHOR" => "",
                                    "FILE_DATE" => "",
                                    "FILE_DESCRIPTION" => "",
                                    "AUTOSTART" => "N",
                                    "REPEAT" => "none",
                                    "VOLUME" => "70",
                                    "SKIN_PATH" => "",
                                    "SKIN" => "",
                                    "CONTROLBAR" => "",
                                    "WMODE" => "",
                                    "LOGO" => "",
                                    "LOGO_LINK" => "",
                                    "LOGO_POSITION" => "",
                                    "PLUGINS" => array(""),
                                    "ADDITIONAL_FLASHVARS" => "",
                                    "WMODE_WMV" => "",
                                    "SHOW_CONTROLS" => "N",
                                    "SHOW_DIGITS" => "N",
                                    "CONTROLS_BGCOLOR" => "",
                                    "CONTROLS_COLOR" => "",
                                    "CONTROLS_OVER_COLOR" => "",
                                    "SCREEN_COLOR" => "",
                                    "MUTE" => "N",
                                    "ADVANCED_MODE_SETTINGS" => "N",
                                    "PLAYER_ID" => "",
                                    "BUFFER_LENGTH" => "",
                                    "DOWNLOAD_LINK" => "",
                                    "DOWNLOAD_LINK_TARGET" => "",
                                    "ADDITIONAL_WMVVARS" => "",
                                    "ALLOW_SWF" => "N"
                                )
                            );?>
                        </td>
                        <td>
                            <input type="button" value="Удалить" id="delete_hlid_<?=$cus_tab["ID"]?>_video_<?=$video_count?>">
                            <input type="hidden" name="<?=$edit_field_name?>" value="<?=$video?>">
                            <script>
                                BX.bind(BX('delete_hlid_<?=$cus_tab["ID"]?>_video_<?=$video_count?>'), 'click', function() {
                                    var element = document.getElementById("TR_VIDEO_HLID_<?=$cus_tab["ID"]?>_ID_<?=$video_count?>");
                                    element.outerHTML = "";
                                    delete element;
                                });
                            </script>
                        </td>
                    </tr>
                    <?$video_count++;?>
                <?endif?>
            <?endforeach;?>
        </table>
    </td>
    </tr>
    <?endif;?>
    <?/**   VISITORS   */?>
    <?if($ar_tabs_types[$cus_tab["UF_TAB_TYPE_ID"]]["UF_CODE"] == "VISITORS"):?>
        <?$edit_field_name = "UF_VISITORS_HLID_".$cus_tab["ID"];?>
        <table id="datatable_visitor" class="internal" style="margin: 0 auto;" >
            <tr id="add_visitor_list_hlid_<?=$cus_tab["ID"]?>_count_0">
                <td>ID</td>
                <td>Имя</td>
                <td>Место работы</td>
                <td>Должность</td>
                <td>Наиминование субъекта РФ</td>
                <td>Почтовый адрес</td>
                <td>Контактный телефон</td>
                <td>Email</td>
                <td>Тема доклада</td>
                <td>Наиминование тезисов</td>
                <td>Прикрепаляемые файлы</td>
                <td>Участие в дискуссиях</td>
                <td>Статус посещения</td>
                <td>Подтверждение</td>
                <td>Удалить</td>
            </tr>
            <?foreach($cus_tab["UF_EVENT_PERSON"] as $visitor):?>
                <?if($visitor["UF_IS_VISITOR"] && isset($visitor["UF_IS_VISITOR"]) && ($visitor["UF_IS_VISITOR"] == "Y" || $visitor["UF_IS_VISITOR"] == 1)):?>
                    <?$edit_field_name = "UF_VISITOR_".$visitor["ID"]."_HLID_".$cus_tab["ID"];?>
                    <tr id="<?=$edit_field_name?>">
                        <td>
                            <?=$visitor["ID"]?>
                            <input type="hidden" name="UF_VISITORS_HLID_<?=$cus_tab["ID"]?>[]" value="<?=$visitor["ID"]?>">
                        </td>
                        <td>
	                        <?if(strlen($visitor["UF_PERSON_NAME"])>1):?>
		                        <?=$visitor["UF_PERSON_NAME"]?>
	                        <?else:?>
		                        -
							<?endif;?>
                        </td>
                        <td>
	                        <?if(strlen($visitor["UF_JOB"])>1):?>
		                        <?=$visitor["UF_JOB"]?>
	                        <?else:?>
		                        -
	                        <?endif;?>
                        </td>
                        <td>
	                        <?if(strlen($visitor["UF_POST"])>1):?>
		                        <?=$visitor["UF_POST"]?>
	                        <?else:?>
		                        -
	                        <?endif;?>
                        </td>
                        <td>
	                        <?if(strlen($visitor["UF_STATE"])>1):?>
		                        <?=$visitor["UF_STATE"]?>
	                        <?else:?>
		                        -
	                        <?endif;?>
                        </td>
                        <td>
	                        <?if(strlen($visitor["UF_POST_MAIL"])>1):?>
		                        <?=$visitor["UF_POST_MAIL"]?>
	                        <?else:?>
		                        -
	                        <?endif;?>
                        </td>
                        <td>
	                        <?if(strlen($visitor["UF_CONTACT_PHONE"])>1):?>
		                        <?=$visitor["UF_CONTACT_PHONE"]?>
	                        <?else:?>
		                        -
	                        <?endif;?>
                        </td>
	                    <td>
		                    <?if(strlen($visitor["UF_EMAIL"])>1):?>
			                    <?=$visitor["UF_EMAIL"]?>
			                    <?else:?>
			                    -
		                    <?endif;?>
	                    </td>
                        <td>
	                        <?if(strlen($visitor["UF_TOPIC"])>1):?>
		                        <?=$visitor["UF_TOPIC"]?>
	                        <?else:?>
		                        -
	                        <?endif;?>
                        </td>
                        <td>
	                        <?if(count($visitor["UF_THESES_NAMES"])>0):?>
		                        <ul>
		                        <?foreach($visitor["UF_THESES_NAMES"] as $theme):?>
			                        <?if(strlen($theme)>0):?>
				                        <li><?=$theme?></li>
			                        <?endif;?>
		                        <?endforeach;?>
		                        <?unset($theme);?>
		                        </ul>
	                        <?else:?>
		                        -
	                        <?endif;?>
                        </td>
                        <td>

	                        <?foreach($visitor["UF_FILES"] as $tmp_file):?>
		                        <?
		                        $rs_file = CFile::GetByID($tmp_file);
		                        $ar_file = $rs_file->Fetch();
		                        $temp_width = 100;
		                        $temp_height = $temp_width/$ar_file["WIDTH"]*$ar_file["HEIGHT"];
		                        ?>
		                        <span class="adm-input-file-exist-cont">
                                    <?if($ar_file["CONTENT_TYPE"] == "image/jpeg" || $ar_file["CONTENT_TYPE"] == "image/png"):?>
	                                    <?=CFile::ShowImage($ar_file["ID"], $temp_width, $temp_height, "border=0", "", true);?>
                                    <?else:?>
	                                    <?$temp_path = CFile::GetPath($ar_file["ID"]);?>
	                                    <a
		                                    class="adm-input-file-name"
		                                    href="<?=$temp_path;?>"
		                                    >
		                                    <?=$temp_path;?>
	                                    </a>
                                    <?endif;?>
                                </span>
		                        <br>
	                        <?endforeach;?>
	                        <?unset($tmp_file);?>
                        </td>
                        <td>
	                        <?if(strlen($visitor["UF_IN_DISCUSSIONS"])>1):?>
		                        <?=$visitor["UF_IN_DISCUSSIONS"]?>
	                        <?else:?>
		                        -
	                        <?endif;?>
                        </td>
                        <td>
	                        <?if(strlen($visitor["UF_VISIT_STATUS"])>1):?>
		                        <?=$visitor["UF_VISIT_STATUS"]?>
	                        <?else:?>
		                        -
	                        <?endif;?>
                        </td>
	                    <td style="vertical-align: top;">
		                    <select name="UF_VISITOR_<?=$visitor["ID"]?>_UF_IS_CONF_RAPPORT" style="margin-bottom:3px" value="<?=$rapporteur["UF_IS_CONF_RAPPORT"]?>">
			                    <option value="0"
				                    <?if($visitor["UF_IS_CONF_RAPPORT"]==0):?> selected<?endif;?>
				                    >Нет</option>
			                    <option value="1"
				                    <?if($visitor["UF_IS_CONF_RAPPORT"]==1):?> selected<?endif;?>
				                    >Да</option>
		                    </select>
	                    </td>
                        <td>
                            <input type="button" size="50" name="<?=$edit_field_name?>" value="Удалить" id="DEL_<?=$edit_field_name?>">
                            <script>
                                BX.ready(function(){
                                    BX.bind(BX('DEL_<?=$edit_field_name?>'), 'click', function() {
                                        var element = document.getElementById("<?=$edit_field_name?>");
                                        element.outerHTML = "";
                                        delete element;
                                    });
                                });
                            </script>
                        </td>
                    </tr>
                <?endif;?>
            <?endforeach;?>
			</table>
			<table>
			<script src="/local/templates/.default/excellentexport.js"></script>
			<link href="/local/templates/.default/exelexportbuttons.css" rel="stylesheet">
			<tr>
				<td>
					<a
						download="visitors_exel.xls"
						class="export-buttons"
						href="#"
						onclick="return ExcellentExport.excel(this, 'datatable_visitor', 'Посетители');"
						>
						Выгрузить <br>
						в Excel
					</a>
				</td>
			</tr>
    <?endif;?>
    <?/**   RAPPORTEURS   */?>
    <?if($ar_tabs_types[$cus_tab["UF_TAB_TYPE_ID"]]["UF_CODE"] == "RAPPORTEURS"):?>
        <?$edit_field_name = "UF_RAPPORTEURS_HLID_".$cus_tab["ID"];?>
        <table id="datatable_rapporteurs" class="internal" style="margin: 0 auto;" >
            <tr id="add_visitor_list_hlid_<?=$cus_tab["ID"]?>_count_0">
	            <td>ID</td>
	            <td>Имя</td>
	            <td>Место работы</td>
	            <td>Должность</td>
	            <td>Наиминование субъекта РФ</td>
	            <td>Почтовый адрес</td>
	            <td>Контактный телефон</td>
	            <td>Email</td>
	            <td>Тема доклада</td>
	            <td>Наиминование тезисов</td>
	            <td>Прикрепаляемые файлы</td>
	            <td>Участие в дискуссиях</td>
	            <td>Статус посещения</td>
	            <td>Подтверждённый докладчик</td>
	            <td>Удалить</td>
            </tr>
            <?foreach($cus_tab["UF_EVENT_PERSON"] as $rapporteur):?>
                <?if($rapporteur["UF_IS_RAPPORTEUR"] && isset($rapporteur["UF_IS_RAPPORTEUR"]) && ($rapporteur["UF_IS_RAPPORTEUR"] == "Y" || $rapporteur["UF_IS_RAPPORTEUR"] == 1)):?>
                    <?$edit_field_name = "UF_RAPPORTEUR_".$rapporteur["ID"]."_HLID_".$cus_tab["ID"];?>
                    <tr id="<?=$edit_field_name?>">
                        <td style="vertical-align: top;">
                            <?=$rapporteur["ID"]?>
                            <input type="hidden" name="UF_RAPPORTEURS_HLID_<?=$cus_tab["ID"]?>[]" value="<?=$rapporteur["ID"]?>">
                        </td>
	                    <td>
		                    <?if(strlen($rapporteur["UF_PERSON_NAME"])>1):?>
			                    <?=$rapporteur["UF_PERSON_NAME"]?>
		                    <?else:?>
			                    -
		                    <?endif;?>
	                    </td>
	                    <td>
		                    <?if(strlen($rapporteur["UF_JOB"])>1):?>
			                    <?=$rapporteur["UF_JOB"]?>
		                    <?else:?>
			                    -
		                    <?endif;?>
	                    </td>
	                    <td>
		                    <?if(strlen($rapporteur["UF_POST"])>1):?>
			                    <?=$rapporteur["UF_POST"]?>
		                    <?else:?>
			                    -
		                    <?endif;?>
	                    </td>
	                    <td>
		                    <?if(strlen($rapporteur["UF_STATE"])>1):?>
			                    <?=$rapporteur["UF_STATE"]?>
		                    <?else:?>
			                    -
		                    <?endif;?>
	                    </td>
	                    <td>
		                    <?if(strlen($rapporteur["UF_POST_MAIL"])>1):?>
			                    <?=$rapporteur["UF_POST_MAIL"]?>
		                    <?else:?>
			                    -
		                    <?endif;?>
	                    </td>
	                    <td>
		                    <?if(strlen($rapporteur["UF_CONTACT_PHONE"])>1):?>
			                    <?=$rapporteur["UF_CONTACT_PHONE"]?>
		                    <?else:?>
			                    -
		                    <?endif;?>
	                    </td>
	                    <td>
		                    <?if(strlen($rapporteur["UF_EMAIL"])>1):?>
			                    <?=$rapporteur["UF_EMAIL"]?>
		                    <?else:?>
			                    -
		                    <?endif;?>
	                    </td>
	                    <td>
		                    <?if(strlen($rapporteur["UF_TOPIC"])>1):?>
			                    <?=$rapporteur["UF_TOPIC"]?>
		                    <?else:?>
			                    -
		                    <?endif;?>
	                    </td>
	                    <td>
		                    <?if(count($rapporteur["UF_THESES_NAMES"])>0):?>
			                    <ul>
				                    <?foreach($rapporteur["UF_THESES_NAMES"] as $theme):?>
					                    <?if(strlen($theme)>0):?>
						                    <li><?=$theme?></li>
					                    <?endif;?>
				                    <?endforeach;?>
				                    <?unset($theme);?>
			                    </ul>
		                    <?else:?>
			                    -
		                    <?endif;?>
	                    </td>
                        <td style="vertical-align: top;">
                            <?foreach($rapporteur["UF_FILES"] as $tmp_file):?>
                                <?
                                $rs_file = CFile::GetByID($tmp_file);
                                $ar_file = $rs_file->Fetch();
                                $temp_width = 100;
                                $temp_height = $temp_width/$ar_file["WIDTH"]*$ar_file["HEIGHT"];
                                ?>
                                <span class="adm-input-file-exist-cont">
                                    <?if($ar_file["CONTENT_TYPE"] == "image/jpeg" || $ar_file["CONTENT_TYPE"] == "image/png"):?>
                                        <?=CFile::ShowImage($ar_file["ID"], $temp_width, $temp_height, "border=0", "", true);?>
                                    <?else:?>
                                        <?$temp_path = CFile::GetPath($ar_file["ID"]);?>
                                        <a
                                            class="adm-input-file-name"
                                            href="<?=$temp_path;?>"
                                            >
                                            <?=$temp_path;?>
                                        </a>
                                    <?endif;?>
                                </span>
                                <br>
                            <?endforeach;?>
                        </td>
	                    <td>
		                    <?if(strlen($rapporteur["UF_IN_DISCUSSIONS"])>1):?>
			                    <?=$rapporteur["UF_IN_DISCUSSIONS"]?>
		                    <?else:?>
			                    -
		                    <?endif;?>
	                    </td>
	                    <td>
		                    <?if(strlen($rapporteur["UF_VISIT_STATUS"])>1):?>
			                    <?=$rapporteur["UF_VISIT_STATUS"]?>
		                    <?else:?>
			                    -
		                    <?endif;?>
	                    </td>
                        <td style="vertical-align: top;">
                            <select name="UF_RAPPORTEUR_<?=$rapporteur["ID"]?>_UF_IS_CONF_RAPPORT" style="margin-bottom:3px" value="<?=$rapporteur["UF_IS_CONF_RAPPORT"]?>">
                                <option value="0"
                                    <?if($rapporteur["UF_IS_CONF_RAPPORT"]==0):?> selected<?endif;?>
                                        >Нет</option>
                                <option value="1"
                                    <?if($rapporteur["UF_IS_CONF_RAPPORT"]==1):?> selected<?endif;?>
                                    >Да</option>
                            </select>
                        </td>
                        <td style="vertical-align: top;">
                            <input type="button" size="50" name="<?=$edit_field_name?>" value="Удалить" id="DEL_<?=$edit_field_name?>">
                            <script>
                                BX.ready(function(){
                                    BX.bind(BX('DEL_<?=$edit_field_name?>'), 'click', function() {
                                        var element = document.getElementById("<?=$edit_field_name?>");
                                        element.outerHTML = "";
                                        delete element;
                                    });
                                });
                            </script>
                        </td>
                    </tr>
                <?endif;?>
            <?endforeach;?>
        </table>
	<table>
	<script src="/local/templates/.default/excellentexport.js"></script>
	<link href="/local/templates/.default/exelexportbuttons.css" rel="stylesheet">
	<tr>
		<td>
			<a
				download="rapporteurs_exel.xls"
				class="export-buttons"
				href="#"
				onclick="return ExcellentExport.excel(this, 'datatable_rapporteurs', 'Докладчики');"
				>
				Выгрузить <br>
				в Excel
			</a>
		</td>
	</tr>
    <?endif;?>
    <?
    $tabControl->EndCustomField($name,'');
}

/*  CUSTOM TABS END  */


if($arShowTabs['edit_rights']):
    $tabControl->BeginNextFormTab();
    if($ID > 0)
    {
        $obRights = new CIBlockElementRights($IBLOCK_ID, $ID);
        $htmlHidden = '';
        foreach($obRights->GetRights() as $RIGHT_ID => $arRight)
            $htmlHidden .= '
				<input type="hidden" name="RIGHTS[][RIGHT_ID]" value="'.htmlspecialcharsbx($RIGHT_ID).'">
				<input type="hidden" name="RIGHTS[][GROUP_CODE]" value="'.htmlspecialcharsbx($arRight["GROUP_CODE"]).'">
				<input type="hidden" name="RIGHTS[][TASK_ID]" value="'.htmlspecialcharsbx($arRight["TASK_ID"]).'">
			';
    }
    else
    {
        $obRights = new CIBlockSectionRights($IBLOCK_ID, $MENU_SECTION_ID);
        $htmlHidden = '';
    }

    $tabControl->BeginCustomField("RIGHTS", GetMessage("IBEL_E_RIGHTS_FIELD"));
    IBlockShowRights(
        'element',
        $IBLOCK_ID,
        $ID,
        GetMessage("IBEL_E_RIGHTS_SECTION_TITLE"),
        "RIGHTS",
        $obRights->GetRightsList(),
        $obRights->GetRights(array("count_overwrited" => true, "parents" => $str_IBLOCK_ELEMENT_SECTION)),
        false, /*$bForceInherited=*/($ID <= 0) || $bCopy
    );
    $tabControl->EndCustomField("RIGHTS", $htmlHidden);
endif;

$bDisabled =
    ($view=="Y")
    || ($bWorkflow && $prn_LOCK_STATUS=="red")
    || (
        (($ID <= 0) || $bCopy)
        && !CIBlockSectionRights::UserHasRightTo($IBLOCK_ID, $MENU_SECTION_ID, "section_element_bind")
    )
    || (
        (($ID > 0) && !$bCopy)
        && !CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $ID, "element_edit")
    )
    || (
        $bBizproc
        && !$canWrite
    )
;

if (!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1):
    ob_start();
    ?>
    <input <?if ($bDisabled) echo "disabled";?> type="submit" class="adm-btn-save" name="save" id="save" value="<?echo GetMessage("IBLOCK_EL_SAVE")?>">
    <? if (!$bAutocomplete)
{
    ?><input <?if ($bDisabled) echo "disabled";?> type="submit" class="button" name="apply" id="apply" value="<?echo GetMessage('IBLOCK_APPLY')?>"><?
}
    ?>
    <input <?if ($bDisabled) echo "disabled";?> type="submit" class="button" name="dontsave" id="dontsave" value="<?echo GetMessage("IBLOCK_EL_CANC")?>">
    <? if (!$bAutocomplete)
{
    ?><input <?if ($bDisabled) echo "disabled";?> type="submit" class="adm-btn-add" name="save_and_add" id="save_and_add" value="<?echo GetMessage("IBLOCK_EL_SAVE_AND_ADD")?>"><?
}
    $buttons_add_html = ob_get_contents();
    ob_end_clean();
    $tabControl->Buttons(false, $buttons_add_html);
elseif(!$bPropertyAjax && $nobuttons !== "Y"):

    $wfClose = "{
		title: '".CUtil::JSEscape(GetMessage("IBLOCK_EL_CANC"))."',
		name: 'dontsave',
		id: 'dontsave',
		action: function () {
			var FORM = this.parentWindow.GetForm();
			FORM.appendChild(BX.create('INPUT', {
				props: {
					type: 'hidden',
					name: this.name,
					value: 'Y'
				}
			}));
			this.disableUntilError();
			this.parentWindow.Submit();
		}
	}";
    $save_and_add = "{
		title: '".CUtil::JSEscape(GetMessage("IBLOCK_EL_SAVE_AND_ADD"))."',
		name: 'save_and_add',
		id: 'save_and_add',
		className: 'adm-btn-add',
		action: function () {
			var FORM = this.parentWindow.GetForm();
			FORM.appendChild(BX.create('INPUT', {
				props: {
					type: 'hidden',
					name: 'save_and_add',
					value: 'Y'
				}
			}));

			this.parentWindow.hideNotify();
			this.disableUntilError();
			this.parentWindow.Submit();
		}
	}";
    $cancel = "{
		title: '".CUtil::JSEscape(GetMessage("IBLOCK_EL_CANC"))."',
		name: 'cancel',
		id: 'cancel',
		action: function () {
			BX.WindowManager.Get().Close();
			if(window.reloadAfterClose)
				top.BX.reload(true);
		}
	}";
    $tabControl->ButtonsPublic(array(
        '.btnSave',
        ($ID > 0 && $bWorkflow? $wfClose: $cancel),
        $save_and_add,
    ));
endif;

$tabControl->Show();

if (
    (!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1)
    && CIBlockRights::UserHasRightTo($IBLOCK_ID, $IBLOCK_ID, "iblock_edit")
    && !$bAutocomplete
)
{

    echo
    BeginNote(),
    GetMessage("IBEL_E_IBLOCK_MANAGE_HINT"),
        ' <a href="/bitrix/admin/iblock_edit.php?type='.htmlspecialcharsbx($type).'&amp;lang='.LANGUAGE_ID.'&amp;ID='.$IBLOCK_ID.'&amp;admin=Y&amp;return_url='.urlencode("/bitrix/admin/".CIBlock::GetAdminElementEditLink($IBLOCK_ID, $ID, array("WF" => ($WF=="Y"? "Y": null), "find_section_section" => intval($find_section_section), "return_url" => (strlen($return_url)>0? $return_url: null)))).'">',
    GetMessage("IBEL_E_IBLOCK_MANAGE_HINT_HREF"),
    '</a>',
    EndNote()
    ;
}


$APPLICATION->AddHeadScript('/local/php_interface/include/customs/element_edit_form_functions.js');
//////////////////////////
//END of the custom form
//////////////////////////