<?php
/**
 * Created by PhpStorm.
 * User: Bondarenko Georg
 * Date: 10.08.2015
 * Time: 11:56
 */

use Bitrix\Main\Loader;
use Bitrix\Iblock;
//////////////////////////
//START of the custom form
//////////////////////////


/* Logic section START */

if(CModule::IncludeModule("bit_hl"))
{
    $ar_tabs_types_temp = \BIT\ORM\BITTypeTab::GetAllTypes();
    $ar_tabs_types = array();
    $ar_tabs_list = \BIT\ORM\BITTabsList::GetListByIBElementID($ID);
    foreach($ar_tabs_types_temp as $tab_type_temp)
    {
        if(in_array(3,$tab_type_temp["UF_IBLOCK_SHOW"]))
        {
            $ar_tabs_types[$tab_type_temp["ID"]] = $tab_type_temp;
        }
    }
}
$arUsers = array();
$rs_user = CUser::GetList(
    ($by="ID"),
    ($order="desc"),
    array("GROUPS_ID"=>5), //, "UF_LANGUAGE"=>$GLOBALS["arAllLanguage"][array_shift($PROP["UF_LANGUAGE"]["VALUE"])], "!UF_LANGUAGE"=>false),
    array("SELECT"=>array("UF_*"))
);
while($ar_user = $rs_user->Fetch())
{
    $arUsers[$ar_user["ID"]] = $ar_user;
}
/** Get show types */
$obEnum = new CUserFieldEnum;
$rsEnum = $obEnum->GetList(array(), array("USER_FIELD_ID" => 185));
$arShowListTypes = array();
while($arEnum = $rsEnum->GetNext()){
	$arShowListTypes[] = $arEnum;
}
unset($obEnum);
unset($rsEnum);
unset($arEnum);
/** show types and */
/* Logic section END */

/*  Reinit tabs START  */

unset($tabControl);

foreach($ar_tabs_list as $key=>$uf_tab)
{
    $aTabs[] = array(
        'DIV'   => 'tab'.$key,
        'TAB'   => $uf_tab["UF_NAME"],
        'ICON'  => '',
        'TITLE' => $uf_tab["UF_NAME"],
    );
}
$tabControl->tabs = $aTabs;
$tabControl = new CAdminForm("form_element_".$IBLOCK_ID, $aTabs, !$bPropertyAjax, $denyAutosave);
/** This is deleting "do not need" tabs */
unset($tabControl->tabs[2]);
unset($tabControl->tabs[1]);

$customTabber = new CAdminTabEngine("OnAdminIBlockElementEdit", array("ID" => $ID, "IBLOCK"=>$arIBlock, "IBLOCK_TYPE"=>$arIBTYPE));
$tabControl->AddTabs($customTabber);
$temp_tabs = $tabControl->tabs;
unset($tabControl->tabs);
foreach($temp_tabs as $temp_cur_tab)
{
    $tabControl->tabs[] = $temp_cur_tab;
}
//pre_dump($tabControl->tabs);
/* Reinit tabs END */

//We have to explicitly call calendar and editor functions because
//first output may be discarded by form settings

$tabControl->BeginPrologContent();
CJSCore::Init(array('date'));

if(COption::GetOptionString("iblock", "use_htmledit", "Y")=="Y" && $bFileman)
{
    //TODO:This dirty hack will be replaced by special method like calendar do
    echo '<div style="display:none">';
    CFileMan::AddHTMLEditorFrame(
        "SOME_TEXT",
        "",
        "SOME_TEXT_TYPE",
        "text",
        array(
            'height' => 450,
            'width' => '100%'
        ),
        "N",
        0,
        "",
        "",
        $arIBlock["LID"]
    );
    echo '</div>';
}

if($arTranslit["TRANSLITERATION"] == "Y")
{
    CJSCore::Init(array('translit'));
    ?>
    <script type="text/javascript">
        var linked=<?if($bLinked) echo 'true'; else echo 'false';?>;
        function set_linked()
        {
            linked=!linked;

            var name_link = document.getElementById('name_link');
            if(name_link)
            {
                if(linked)
                    name_link.src='/bitrix/themes/.default/icons/iblock/link.gif';
                else
                    name_link.src='/bitrix/themes/.default/icons/iblock/unlink.gif';
            }
            var code_link = document.getElementById('code_link');
            if(code_link)
            {
                if(linked)
                    code_link.src='/bitrix/themes/.default/icons/iblock/link.gif';
                else
                    code_link.src='/bitrix/themes/.default/icons/iblock/unlink.gif';
            }
            var linked_state = document.getElementById('linked_state');
            if(linked_state)
            {
                if(linked)
                    linked_state.value='Y';
                else
                    linked_state.value='N';
            }
        }
        var oldValue = '';
        function transliterate()
        {
            if(linked)
            {
                var from = document.getElementById('NAME');
                var to = document.getElementById('CODE');
                if(from && to && oldValue != from.value)
                {
                    BX.translit(from.value, {
                        'max_len' : <?echo intval($arTranslit['TRANS_LEN'])?>,
                        'change_case' : '<?echo $arTranslit['TRANS_CASE']?>',
                        'replace_space' : '<?echo $arTranslit['TRANS_SPACE']?>',
                        'replace_other' : '<?echo $arTranslit['TRANS_OTHER']?>',
                        'delete_repeat_replace' : <?echo $arTranslit['TRANS_EAT'] == 'Y'? 'true': 'false'?>,
                        'use_google' : <?echo $arTranslit['USE_GOOGLE'] == 'Y'? 'true': 'false'?>,
                        'callback' : function(result){to.value = result; setTimeout('transliterate()', 250); }
                    });
                    oldValue = from.value;
                }
                else
                {
                    setTimeout('transliterate()', 250);
                }
            }
            else
            {
                setTimeout('transliterate()', 250);
            }
        }
        transliterate();
    </script>
<?
}
?>
    <script type="text/javascript">
        var InheritedPropertiesTemplates = new JCInheritedPropertiesTemplates(
            '<?echo $tabControl->GetName()?>_form',
            '/bitrix/admin/iblock_templates.ajax.php?ENTITY_TYPE=E&IBLOCK_ID=<?echo intval($IBLOCK_ID)?>&ENTITY_ID=<?echo intval($ID)?>'
        );
        BX.ready(function(){
            setTimeout(function(){
                InheritedPropertiesTemplates.updateInheritedPropertiesTemplates(true);
            }, 1000);
        });
    </script>
<?
$tabControl->EndPrologContent();

$tabControl->BeginEpilogContent();

echo bitrix_sessid_post();
echo GetFilterHiddens("find_");?>
    <input type="hidden" name="linked_state" id="linked_state" value="<?if($bLinked) echo 'Y'; else echo 'N';?>">
    <input type="hidden" name="Update" value="Y">
    <input type="hidden" name="from" value="<?echo htmlspecialcharsbx($from)?>">
    <input type="hidden" name="WF" value="<?echo htmlspecialcharsbx($WF)?>">
    <input type="hidden" name="return_url" value="<?echo htmlspecialcharsbx($return_url)?>">
<?if($ID>0 && !$bCopy)
{
    ?><input type="hidden" name="ID" value="<?echo $ID?>"><?
}
if ($bCopy)
{
    ?><input type="hidden" name="copyID" value="<? echo $ID; ?>"><?
}
if ($bCatalog)
    CCatalogAdminTools::showFormParams();
?>
    <input type="hidden" name="IBLOCK_SECTION_ID" value="<?echo intval($IBLOCK_SECTION_ID)?>">
    <input type="hidden" name="TMP_ID" value="<?echo intval($TMP_ID)?>">
<?
$tabControl->EndEpilogContent();

$customTabber->SetErrorState($bVarsFromForm);

$arEditLinkParams = array(
    "find_section_section" => intval($find_section_section)
);
if ($bAutocomplete)
{
    $arEditLinkParams['lookup'] = $strLookup;
}

$tabControl->Begin(array(
    "FORM_ACTION" => "/bitrix/admin/".CIBlock::GetAdminElementEditLink($IBLOCK_ID, null, $arEditLinkParams)
));

$tabControl->BeginNextFormTab();
if($ID > 0 && !$bCopy)
{
    $p = CIblockElement::GetByID($ID);
    $pr = $p->ExtractFields("prn_");
}
else
{
    $pr = array();
}
$tabControl->AddCheckBoxField("ACTIVE", GetMessage("IBLOCK_FIELD_ACTIVE").":", false, array("Y","N"), $str_ACTIVE=="Y");
$tabControl->BeginCustomField("ACTIVE_FROM", GetMessage("IBLOCK_FIELD_ACTIVE_PERIOD_FROM"), $arIBlock["FIELDS"]["ACTIVE_FROM"]["IS_REQUIRED"] === "Y");
?>
    <tr id="tr_ACTIVE_FROM">
        <td><?echo $tabControl->GetCustomLabelHTML()?>:</td>
        <td><?echo CAdminCalendar::CalendarDate("ACTIVE_FROM", $str_ACTIVE_FROM, 19, true)?></td>
    </tr>
<?
$tabControl->EndCustomField("ACTIVE_FROM", '<input type="hidden" id="ACTIVE_FROM" name="ACTIVE_FROM" value="'.$str_ACTIVE_FROM.'">');
$tabControl->BeginCustomField("ACTIVE_TO", GetMessage("IBLOCK_FIELD_ACTIVE_PERIOD_TO"), $arIBlock["FIELDS"]["ACTIVE_TO"]["IS_REQUIRED"] === "Y");
?>
    <tr id="tr_ACTIVE_TO">
        <td><?echo $tabControl->GetCustomLabelHTML()?>:</td>
        <td><?echo CAdminCalendar::CalendarDate("ACTIVE_TO", $str_ACTIVE_TO, 19, true)?></td>
    </tr>

<?
$tabControl->EndCustomField("ACTIVE_TO", '<input type="hidden" id="ACTIVE_TO" name="ACTIVE_TO" value="'.$str_ACTIVE_TO.'">');

if($arTranslit["TRANSLITERATION"] == "Y")
{
    $tabControl->BeginCustomField("NAME", GetMessage("IBLOCK_FIELD_NAME").":", true);
    ?>
    <tr id="tr_NAME">
        <td><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td style="white-space: nowrap;">
            <input type="text" size="50" name="NAME" id="NAME" maxlength="255" value="<?echo $str_NAME?>"><image id="name_link" title="<?echo GetMessage("IBEL_E_LINK_TIP")?>" class="linked" src="/bitrix/themes/.default/icons/iblock/<?if($bLinked) echo 'link.gif'; else echo 'unlink.gif';?>" onclick="set_linked()" />
        </td>
    </tr>
    <?
    $tabControl->EndCustomField("NAME",
        '<input type="hidden" name="NAME" id="NAME" value="'.$str_NAME.'">'
    );

    $tabControl->BeginCustomField("CODE", GetMessage("IBLOCK_FIELD_CODE").":", $arIBlock["FIELDS"]["CODE"]["IS_REQUIRED"] === "Y");
    ?>
    <tr id="tr_CODE">
        <td><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td style="white-space: nowrap;">
            <input type="text" size="50" name="CODE" id="CODE" maxlength="255" value="<?echo $str_CODE?>"><image id="code_link" title="<?echo GetMessage("IBEL_E_LINK_TIP")?>" class="linked" src="/bitrix/themes/.default/icons/iblock/<?if($bLinked) echo 'link.gif'; else echo 'unlink.gif';?>" onclick="set_linked()" />
        </td>
    </tr>
    <?
    $tabControl->EndCustomField("CODE",
        '<input type="hidden" name="CODE" id="CODE" value="'.$str_CODE.'">'
    );
}
else
{
    $tabControl->AddEditField("NAME", GetMessage("IBLOCK_FIELD_NAME").":", true, array("size" => 50, "maxlength" => 255), $str_NAME);
    $tabControl->AddEditField("CODE", GetMessage("IBLOCK_FIELD_CODE").":", $arIBlock["FIELDS"]["CODE"]["IS_REQUIRED"] === "Y", array("size" => 20, "maxlength" => 255), $str_CODE);
}

if (
    $arShowTabs['sections']
    && $arIBlock["FIELDS"]["IBLOCK_SECTION"]["DEFAULT_VALUE"]["KEEP_IBLOCK_SECTION_ID"] === "Y"
)
{
    $arDropdown = array();
    if ($str_IBLOCK_ELEMENT_SECTION)
    {
        $sectionList = CIBlockSection::GetList(
            array("left_margin"=>"asc"),
            array("=ID"=>$str_IBLOCK_ELEMENT_SECTION),
            false,
            array("ID", "NAME")
        );
        while ($section = $sectionList->Fetch())
            $arDropdown[$section["ID"]] = $section["NAME"];
    }
    $tabControl->BeginCustomField("IBLOCK_ELEMENT_SECTION_ID", GetMessage("IBEL_E_MAIN_IBLOCK_SECTION_ID").":", false);
    ?>
    <tr id="tr_IBLOCK_ELEMENT_SECTION_ID">
        <td class="adm-detail-valign-top"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td>
            <div id="RESULT_IBLOCK_ELEMENT_SECTION_ID">
                <select name="IBLOCK_ELEMENT_SECTION_ID" id="IBLOCK_ELEMENT_SECTION_ID" onchange="InheritedPropertiesTemplates.updateInheritedPropertiesValues(false, true)">
                    <?foreach($arDropdown as $key => $val):?>
                        <option value="<?echo $key?>" <?if ($str_IBLOCK_SECTION_ID == $key) echo 'selected'?>><?echo $val?></option>
                    <?endforeach?>
                </select>
            </div>
            <script>
                window.ipropTemplates[window.ipropTemplates.length] = {
                    "ID": "IBLOCK_ELEMENT_SECTION_ID",
                    "INPUT_ID": "IBLOCK_ELEMENT_SECTION_ID",
                    "RESULT_ID": "RESULT_IBLOCK_ELEMENT_SECTION_ID",
                    "TEMPLATE": ""
                };
                window.ipropTemplates[window.ipropTemplates.length] = {
                    "ID": "CODE",
                    "INPUT_ID": "CODE",
                    "RESULT_ID": "",
                    "TEMPLATE": ""
                };
                window.ipropTemplates[window.ipropTemplates.length] = {
                    "ID": "XML_ID",
                    "INPUT_ID": "XML_ID",
                    "RESULT_ID": "",
                    "TEMPLATE": ""
                };
            </script>
        </td>
    </tr>
    <?
    $tabControl->EndCustomField("IBLOCK_ELEMENT_SECTION_ID",
        '<input type="hidden" name="IBLOCK_ELEMENT_SECTION_ID" id="IBLOCK_ELEMENT_SECTION_ID" value="'.$str_IBLOCK_SECTION_ID.'">'
    );
}

if(COption::GetOptionString("iblock", "show_xml_id", "N")=="Y")
    $tabControl->AddEditField("XML_ID", GetMessage("IBLOCK_FIELD_XML_ID").":", $arIBlock["FIELDS"]["XML_ID"]["IS_REQUIRED"] === "Y", array("size" => 20, "maxlength" => 255, "id" => "XML_ID"), $str_XML_ID);

$tabControl->AddEditField("SORT", GetMessage("IBLOCK_FIELD_SORT").":", $arIBlock["FIELDS"]["SORT"]["IS_REQUIRED"] === "Y", array("size" => 7, "maxlength" => 10), $str_SORT);

if(!empty($PROP)):
    if ($arIBlock["SECTION_PROPERTY"] === "Y" || defined("CATALOG_PRODUCT"))
    {
        $arPropLinks = array("IBLOCK_ELEMENT_PROP_VALUE");
        if(is_array($str_IBLOCK_ELEMENT_SECTION) && !empty($str_IBLOCK_ELEMENT_SECTION))
        {
            foreach($str_IBLOCK_ELEMENT_SECTION as $section_id)
            {
                foreach(CIBlockSectionPropertyLink::GetArray($IBLOCK_ID, $section_id) as $PID => $arLink)
                    $arPropLinks[$PID] = "PROPERTY_".$PID;
            }
        }
        else
        {
            foreach(CIBlockSectionPropertyLink::GetArray($IBLOCK_ID, 0) as $PID => $arLink)
                $arPropLinks[$PID] = "PROPERTY_".$PID;
        }
        $tabControl->AddFieldGroup("IBLOCK_ELEMENT_PROPERTY", GetMessage("IBLOCK_ELEMENT_PROP_VALUE"), $arPropLinks, $bPropertyAjax);
    }

    $tabControl->AddSection("IBLOCK_ELEMENT_PROP_VALUE", GetMessage("IBLOCK_ELEMENT_PROP_VALUE"));

    foreach($PROP as $prop_code=>$prop_fields):
        $prop_values = $prop_fields["VALUE"];
        $tabControl->BeginCustomField("PROPERTY_".$prop_fields["ID"], $prop_fields["NAME"], $prop_fields["IS_REQUIRED"]==="Y");
        ?>
        <?if($prop_fields["ID"] == 9):?>
        <?if(intval($ID)>0):?>
            <tr id="tr_PROPERTY_<?echo $prop_fields["ID"];?>" class="heading">
            <td colspan="2" style="background-color: #f5f9f9;">
                <?echo $prop_fields["NAME"];?>:
            </td>
        </tr>
            <tr id="tabs_elements">
                <td colspan="2" style="text-align: center;">
                    <table class="internal" id="hlb_directory_table" style="margin: 0 auto;">
                        <tbody id="table_tabs">
                        <tr class="heading">
                            <td>Название</td>
                            <td>Тип</td>
                            <td style="vertical-align: top;">
                                <input type="button" value="Добавить вкладку" id="addNewTR">
                                <script>
                                    BX.ready(function(){
                                        var counter = 0;
                                        BX.bind(BX('addNewTR'), 'click', function() {
                                            addEmptyTabConf();
                                        });
                                        function addEmptyTabConf()
                                        {
                                            $str = '<td><input type="text" name="HL_ID_0_NAME_COUNT_'+counter+'" value="" size="35" maxlength="255" style="width:90%"></td>';
                                            $str += '<td><select name="HL_ID_0_TYPE_COUNT_'+counter+'" style="margin-bottom:3px">';
                                            //$str += '<option value="" selected="">(не установлено)</option>';
                                            <?foreach($ar_tabs_types as $tab_type):?>
                                            $str += '<option value="<?=$tab_type["ID"]?>" <?if($tab_type["ID"] == 1):?> selected=""<?endif;?>><?=$tab_type["UF_NAME"]?></option>';
                                            <?endforeach;?>
                                            $str += '</select></td><td></td>';
                                            console.log($str);
                                            var node = document.createElement("TR");
                                            node.innerHTML = $str;
                                            node.setAttribute('id', "hlbl_property_tr_0");
                                            var element = document.getElementById("table_tabs");
                                            element.appendChild(node);
                                            counter++;
                                        }
                                    });

                                </script>
                            </td>
                        </tr>
                        <!-- Isset tabs -->
                        <?foreach($ar_tabs_list as $uf_tab):?>
                            <tr id="hlbl_property_tr_<?=$uf_tab["ID"]?>">
                            <td style="vertical-align: top;">
                                <span><?=$uf_tab["UF_NAME"]?></span>
                            </td>
                            <td style="vertical-align: top;">
                                <span><?=$ar_tabs_types[$uf_tab["UF_TAB_TYPE_ID"]]["UF_NAME"]?></span>

                            </td>
                            <td style="vertical-align: top;">
                                <input type="submit" class="button"
                                       name="del_tab_id_<?=$uf_tab["ID"]?>" id="del_tab" value="Удалить вкладку"
                                       onclick="enableCurHidden(this);"
                                >
                                <input type="hidden" name="del_tab_id_<?=$uf_tab["ID"]?>" value="Y" disabled="disabled"/>
                            </td>
                        </tr>
                        <?endforeach;?>
                        </tbody>
                    </table>
                </td>
            </tr>
        <?endif;?>
	    <?elseif($prop_fields["ID"] == 5 && intval($ID)<=0):?>
	    <tr id="tr_PROPERTY_5">
		    <td class="adm-detail-valign-top adm-detail-content-cell-l" width="40%"><span class="adm-required-field">Язык</span>:</td>
		    <td width="60%" class="adm-detail-content-cell-r">
			    <table cellpadding="0" cellspacing="0" border="0" class="nopadding" width="100%" id="tbf4c19a15121fb29465a56d30b4490249"><tbody><tr><td>
						    <select name="PROP[5][n0][VALUE]">
							    <option value="8slHBJtK">Eng [2]</option>
							    <option value="ae2ogrFC" selected="">Рус [1]</option>
						    </select></td></tr></tbody></table></td>
	    </tr>
	    <?else:?>
        <!--tr id="tr_PROPERTY_<?echo $prop_fields["ID"];?>"<?if ($prop_fields["PROPERTY_TYPE"]=="F"):?> class="adm-detail-file-row"<?endif?>>
                <td class="adm-detail-valign-top" width="40%"><?if($prop_fields["HINT"]!=""):
                        ?><span id="hint_<?echo $prop_fields["ID"];?>"></span><script type="text/javascript">BX.hint_replace(BX('hint_<?echo $prop_fields["ID"];?>'), '<?echo CUtil::JSEscape($prop_fields["HINT"])?>');</script>&nbsp;<?
                    endif;?><?echo $tabControl->GetCustomLabelHTML();?>:</td>
                <td width="60%"><?_ShowPropertyField('PROP['.$prop_fields["ID"].']', $prop_fields, $prop_fields["VALUE"], (($historyId <= 0) && (!$bVarsFromForm) && ($ID<=0) && (!$bPropertyAjax)), $bVarsFromForm||$bPropertyAjax, 50000, $tabControl->GetFormName(), $bCopy);?></td>
            </tr-->
        <?endif;?>
        <?
        $hidden = "";
        if(!is_array($prop_fields["~VALUE"]))
            $values = Array();
        else
            $values = $prop_fields["~VALUE"];
        $start = 1;
        foreach($values as $key=>$val)
        {
            if($bCopy)
            {
                $key = "n".$start;
                $start++;
            }

            if(is_array($val) && array_key_exists("VALUE",$val))
            {
                $hidden .= _ShowHiddenValue('PROP['.$prop_fields["ID"].']['.$key.'][VALUE]', $val["VALUE"]);
                $hidden .= _ShowHiddenValue('PROP['.$prop_fields["ID"].']['.$key.'][DESCRIPTION]', $val["DESCRIPTION"]);
            }
            else
            {
                $hidden .= _ShowHiddenValue('PROP['.$prop_fields["ID"].']['.$key.'][VALUE]', $val);
                $hidden .= _ShowHiddenValue('PROP['.$prop_fields["ID"].']['.$key.'][DESCRIPTION]', "");
            }
        }
        $tabControl->EndCustomField("PROPERTY_".$prop_fields["ID"], $hidden);
    endforeach;?>
<?endif;

if (!$bAutocomplete)
{
    $rsLinkedProps = CIBlockProperty::GetList(array(), array(
        "PROPERTY_TYPE" => "E",
        "LINK_IBLOCK_ID" => $IBLOCK_ID,
        "ACTIVE" => "Y",
        "FILTRABLE" => "Y",
    ));
    $arLinkedProp = $rsLinkedProps->GetNext();
    if($arLinkedProp)
    {
        $tabControl->BeginCustomField("LINKED_PROP", GetMessage("IBLOCK_ELEMENT_EDIT_LINKED"));
        ?>
        <tr class="heading" id="tr_LINKED_PROP">
            <td colspan="2"><?echo $tabControl->GetCustomLabelHTML();?></td>
        </tr>
        <?
        do {
            $elements_name = CIBlock::GetArrayByID($arLinkedProp["IBLOCK_ID"], "ELEMENTS_NAME");
            if(strlen($elements_name) <= 0)
                $elements_name = GetMessage("IBLOCK_ELEMENT_EDIT_ELEMENTS");
            ?>
            <tr id="tr_LINKED_PROP<?echo $arLinkedProp["ID"]?>">
                <td colspan="2"><a href="<?echo htmlspecialcharsbx(CIBlock::GetAdminElementListLink($arLinkedProp["IBLOCK_ID"], array('set_filter'=>'Y', 'find_el_property_'.$arLinkedProp["ID"]=>$ID, 'find_section_section' => -1)))?>"><?echo CIBlock::GetArrayByID($arLinkedProp["IBLOCK_ID"], "NAME").": ".$elements_name?></a></td>
            </tr>
        <?
        } while ($arLinkedProp = $rsLinkedProps->GetNext());
        $tabControl->EndCustomField("LINKED_PROP", "");
    }
}


$tabControl->BeginNextFormTab();
?>
<?
$tabControl->BeginCustomField("IPROPERTY_TEMPLATES_ELEMENT_META_TITLE", GetMessage("IBEL_E_SEO_META_TITLE"));
?>
    <tr class="adm-detail-valign-top">
        <td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%"><?echo IBlockInheritedPropertyInput($IBLOCK_ID, "ELEMENT_META_TITLE", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))?></td>
    </tr>
<?
$tabControl->EndCustomField("IPROPERTY_TEMPLATES_ELEMENT_META_TITLE",
    IBlockInheritedPropertyHidden($IBLOCK_ID, "ELEMENT_META_TITLE", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))
);
?>
<?
$tabControl->BeginCustomField("IPROPERTY_TEMPLATES_ELEMENT_META_KEYWORDS", GetMessage("IBEL_E_SEO_META_KEYWORDS"));
?>
    <tr class="adm-detail-valign-top">
        <td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%"><?echo IBlockInheritedPropertyInput($IBLOCK_ID, "ELEMENT_META_KEYWORDS", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))?></td>
    </tr>
<?
$tabControl->EndCustomField("IPROPERTY_TEMPLATES_ELEMENT_META_KEYWORDS",
    IBlockInheritedPropertyHidden($IBLOCK_ID, "ELEMENT_META_KEYWORDS", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))
);
?>
<?
$tabControl->BeginCustomField("IPROPERTY_TEMPLATES_ELEMENT_META_DESCRIPTION", GetMessage("IBEL_E_SEO_META_DESCRIPTION"));
?>
    <tr class="adm-detail-valign-top">
        <td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%"><?echo IBlockInheritedPropertyInput($IBLOCK_ID, "ELEMENT_META_DESCRIPTION", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))?></td>
    </tr>
<?
$tabControl->EndCustomField("IPROPERTY_TEMPLATES_ELEMENT_META_DESCRIPTION",
    IBlockInheritedPropertyHidden($IBLOCK_ID, "ELEMENT_META_DESCRIPTION", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))
);
?>
<?
$tabControl->BeginCustomField("IPROPERTY_TEMPLATES_ELEMENT_PAGE_TITLE", GetMessage("IBEL_E_SEO_ELEMENT_TITLE"));
?>
    <tr class="adm-detail-valign-top">
        <td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%"><?echo IBlockInheritedPropertyInput($IBLOCK_ID, "ELEMENT_PAGE_TITLE", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))?></td>
    </tr>
<?
$tabControl->EndCustomField("IPROPERTY_TEMPLATES_ELEMENT_PAGE_TITLE",
    IBlockInheritedPropertyHidden($IBLOCK_ID, "ELEMENT_PAGE_TITLE", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))
);
?>
<?
$tabControl->AddSection("IPROPERTY_TEMPLATES_ELEMENTS_PREVIEW_PICTURE", GetMessage("IBEL_E_SEO_FOR_ELEMENTS_PREVIEW_PICTURE"));
$tabControl->BeginCustomField("IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_ALT", GetMessage("IBEL_E_SEO_FILE_ALT"));
?>
    <tr class="adm-detail-valign-top">
        <td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%"><?echo IBlockInheritedPropertyInput($IBLOCK_ID, "ELEMENT_PREVIEW_PICTURE_FILE_ALT", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))?></td>
    </tr>
<?
$tabControl->EndCustomField("IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_ALT",
    IBlockInheritedPropertyHidden($IBLOCK_ID, "ELEMENT_PREVIEW_PICTURE_FILE_ALT", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))
);
?>
<?
$tabControl->BeginCustomField("IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_TITLE", GetMessage("IBEL_E_SEO_FILE_TITLE"));
?>
    <tr class="adm-detail-valign-top">
        <td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%"><?echo IBlockInheritedPropertyInput($IBLOCK_ID, "ELEMENT_PREVIEW_PICTURE_FILE_TITLE", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))?></td>
    </tr>
<?
$tabControl->EndCustomField("IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_TITLE",
    IBlockInheritedPropertyHidden($IBLOCK_ID, "ELEMENT_PREVIEW_PICTURE_FILE_TITLE", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))
);
?>
<?
$tabControl->BeginCustomField("IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_NAME", GetMessage("IBEL_E_SEO_FILE_NAME"));
?>
    <tr class="adm-detail-valign-top">
        <td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%"><?echo IBlockInheritedPropertyInput($IBLOCK_ID, "ELEMENT_PREVIEW_PICTURE_FILE_NAME", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))?></td>
    </tr>
<?
$tabControl->EndCustomField("IPROPERTY_TEMPLATES_ELEMENT_PREVIEW_PICTURE_FILE_NAME",
    IBlockInheritedPropertyHidden($IBLOCK_ID, "ELEMENT_PREVIEW_PICTURE_FILE_NAME", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))
);
?>
<?
$tabControl->AddSection("IPROPERTY_TEMPLATES_ELEMENTS_DETAIL_PICTURE", GetMessage("IBEL_E_SEO_FOR_ELEMENTS_DETAIL_PICTURE"));
$tabControl->BeginCustomField("IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_ALT", GetMessage("IBEL_E_SEO_FILE_ALT"));
?>
    <tr class="adm-detail-valign-top">
        <td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%"><?echo IBlockInheritedPropertyInput($IBLOCK_ID, "ELEMENT_DETAIL_PICTURE_FILE_ALT", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))?></td>
    </tr>
<?
$tabControl->EndCustomField("IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_ALT",
    IBlockInheritedPropertyHidden($IBLOCK_ID, "ELEMENT_DETAIL_PICTURE_FILE_ALT", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))
);
?>
<?
$tabControl->BeginCustomField("IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_TITLE", GetMessage("IBEL_E_SEO_FILE_TITLE"));
?>
    <tr class="adm-detail-valign-top">
        <td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%"><?echo IBlockInheritedPropertyInput($IBLOCK_ID, "ELEMENT_DETAIL_PICTURE_FILE_TITLE", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))?></td>
    </tr>
<?
$tabControl->EndCustomField("IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_TITLE",
    IBlockInheritedPropertyHidden($IBLOCK_ID, "ELEMENT_DETAIL_PICTURE_FILE_TITLE", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))
);
?>
<?
$tabControl->BeginCustomField("IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_NAME", GetMessage("IBEL_E_SEO_FILE_NAME"));
?>
    <tr class="adm-detail-valign-top">
        <td width="40%"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%"><?echo IBlockInheritedPropertyInput($IBLOCK_ID, "ELEMENT_DETAIL_PICTURE_FILE_NAME", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))?></td>
    </tr>
<?
$tabControl->EndCustomField("IPROPERTY_TEMPLATES_ELEMENT_DETAIL_PICTURE_FILE_NAME",
    IBlockInheritedPropertyHidden($IBLOCK_ID, "ELEMENT_DETAIL_PICTURE_FILE_NAME", $str_IPROPERTY_TEMPLATES, "E", GetMessage("IBEL_E_SEO_OVERWRITE"))
);
?>
<?
$tabControl->AddSection("SEO_ADDITIONAL", GetMessage("IBLOCK_EL_TAB_MO"));
$tabControl->BeginCustomField("TAGS", GetMessage("IBLOCK_FIELD_TAGS").":", $arIBlock["FIELDS"]["TAGS"]["IS_REQUIRED"] === "Y");
?>
    <tr id="tr_TAGS">
        <td><?echo $tabControl->GetCustomLabelHTML()?><br><?echo GetMessage("IBLOCK_ELEMENT_EDIT_TAGS_TIP")?></td>
        <td>
            <?if(Loader::includeModule('search')):
                $arLID = array();
                $rsSites = CIBlock::GetSite($IBLOCK_ID);
                while($arSite = $rsSites->Fetch())
                    $arLID[] = $arSite["LID"];
                echo InputTags("TAGS", htmlspecialcharsback($str_TAGS), $arLID, 'size="55"');
            else:?>
                <input type="text" size="20" name="TAGS" maxlength="255" value="<?echo $str_TAGS?>">
            <?endif?>
        </td>
    </tr>
<?
$tabControl->EndCustomField("TAGS",
    '<input type="hidden" name="TAGS" value="'.$str_TAGS.'">'
);

?>
<?if($arShowTabs['sections']):
    $tabControl->BeginNextFormTab();

    $tabControl->BeginCustomField("SECTIONS", GetMessage("IBLOCK_SECTION"), $arIBlock["FIELDS"]["IBLOCK_SECTION"]["IS_REQUIRED"] === "Y");
    ?>
    <tr id="tr_SECTIONS">
    <?if($arIBlock["SECTION_CHOOSER"] != "D" && $arIBlock["SECTION_CHOOSER"] != "P"):?>

        <?$l = CIBlockSection::GetTreeList(Array("IBLOCK_ID"=>$IBLOCK_ID), array("ID", "NAME", "DEPTH_LEVEL"));?>
        <td width="40%" class="adm-detail-valign-top"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%">
            <select name="IBLOCK_SECTION[]" size="14" multiple onchange="onSectionChanged()">
                <option value="0"<?if(is_array($str_IBLOCK_ELEMENT_SECTION) && in_array(0, $str_IBLOCK_ELEMENT_SECTION))echo " selected"?>><?echo GetMessage("IBLOCK_UPPER_LEVEL")?></option>
                <?
                while($ar_l = $l->GetNext()):
                    ?><option value="<?echo $ar_l["ID"]?>"<?if(is_array($str_IBLOCK_ELEMENT_SECTION) && in_array($ar_l["ID"], $str_IBLOCK_ELEMENT_SECTION))echo " selected"?>><?echo str_repeat(" . ", $ar_l["DEPTH_LEVEL"])?><?echo $ar_l["NAME"]?></option><?
                endwhile;
                ?>
            </select>
        </td>

    <?elseif($arIBlock["SECTION_CHOOSER"] == "D"):?>
        <td width="40%" class="adm-detail-valign-top"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%">
        <table class="internal" id="sections">
        <?
        if(is_array($str_IBLOCK_ELEMENT_SECTION))
        {
            $i = 0;
            foreach($str_IBLOCK_ELEMENT_SECTION as $section_id)
            {
                $rsChain = CIBlockSection::GetNavChain($IBLOCK_ID, $section_id);
                $strPath = "";
                while($arChain = $rsChain->Fetch())
                    $strPath .= $arChain["NAME"]."&nbsp;/&nbsp;";
                if(strlen($strPath) > 0)
                {
                    ?><tr>
                    <td nowrap><?echo $strPath?></td>
                    <td>
                        <input type="button" value="<?echo GetMessage("MAIN_DELETE")?>" OnClick="deleteRow(this)">
                        <input type="hidden" name="IBLOCK_SECTION[]" value="<?echo intval($section_id)?>">
                    </td>
                    </tr><?
                }
                $i++;
            }
        }
        ?>
        <tr>
            <td>
                <script type="text/javascript">
                    function deleteRow(button)
                    {
                        var my_row = button.parentNode.parentNode;
                        var table = document.getElementById('sections');
                        if(table)
                        {
                            for(var i=0; i<table.rows.length; i++)
                            {
                                if(table.rows[i] == my_row)
                                {
                                    table.deleteRow(i);
                                    onSectionChanged();
                                }
                            }
                        }
                    }
                    function addPathRow()
                    {
                        var table = document.getElementById('sections');
                        if(table)
                        {
                            var section_id = 0;
                            var html = '';
                            var lev = 0;
                            var oSelect;
                            while(oSelect = document.getElementById('select_IBLOCK_SECTION_'+lev))
                            {
                                if(oSelect.value < 1)
                                    break;
                                html += oSelect.options[oSelect.selectedIndex].text+'&nbsp;/&nbsp;';
                                section_id = oSelect.value;
                                lev++;
                            }
                            if(section_id > 0)
                            {
                                var cnt = table.rows.length;
                                var oRow = table.insertRow(cnt-1);

                                var i=0;
                                var oCell = oRow.insertCell(i++);
                                oCell.innerHTML = html;

                                var oCell = oRow.insertCell(i++);
                                oCell.innerHTML =
                                    '<input type="button" value="<?echo GetMessage("MAIN_DELETE")?>" OnClick="deleteRow(this)">'+
                                    '<input type="hidden" name="IBLOCK_SECTION[]" value="'+section_id+'">';
                                onSectionChanged();
                            }
                        }
                    }
                    function find_path(item, value)
                    {
                        if(item.id==value)
                        {
                            var a = Array(1);
                            a[0] = item.id;
                            return a;
                        }
                        else
                        {
                            for(var s in item.children)
                            {
                                if(ar = find_path(item.children[s], value))
                                {
                                    var a = Array(1);
                                    a[0] = item.id;
                                    return a.concat(ar);
                                }
                            }
                            return null;
                        }
                    }
                    function find_children(level, value, item)
                    {
                        if(level==-1 && item.id==value)
                            return item;
                        else
                        {
                            for(var s in item.children)
                            {
                                if(ch = find_children(level-1,value,item.children[s]))
                                    return ch;
                            }
                            return null;
                        }
                    }
                    function change_selection(name_prefix, prop_id,value,level,id)
                    {
                        var lev = level+1;
                        var oSelect;

                        while(oSelect = document.getElementById(name_prefix+lev))
                        {
                            jsSelectUtils.deleteAllOptions(oSelect);
                            jsSelectUtils.addNewOption(oSelect, '0', '(<?echo GetMessage("MAIN_NO")?>)');
                            lev++;
                        }

                        oSelect = document.getElementById(name_prefix+(level+1))
                        if(oSelect && (value!=0||level==-1))
                        {
                            var item = find_children(level,value,window['sectionListsFor'+prop_id]);
                            for(var s in item.children)
                            {
                                var obj = item.children[s];
                                jsSelectUtils.addNewOption(oSelect, obj.id, obj.name, true);
                            }
                        }
                        if(document.getElementById(id))
                            document.getElementById(id).value = value;
                    }
                    function init_selection(name_prefix, prop_id, value, id)
                    {
                        var a = find_path(window['sectionListsFor'+prop_id], value);
                        change_selection(name_prefix, prop_id, 0, -1, id);
                        for(var i=1;i<a.length;i++)
                        {
                            if(oSelect = document.getElementById(name_prefix+(i-1)))
                            {
                                for(var j=0;j<oSelect.length;j++)
                                {
                                    if(oSelect[j].value==a[i])
                                    {
                                        oSelect[j].selected=true;
                                        break;
                                    }
                                }
                            }
                            change_selection(name_prefix, prop_id, a[i], i-1, id);
                        }
                    }
                    var sectionListsFor0 = {id:0,name:'',children:Array()};

                    <?
                    $rsItems = CIBlockSection::GetTreeList(Array("IBLOCK_ID"=>$IBLOCK_ID), array("ID", "NAME", "DEPTH_LEVEL"));
                    $depth = 0;
                    $max_depth = 0;
                    $arChain = array();
                    while($arItem = $rsItems->GetNext())
                    {
                        if($max_depth < $arItem["DEPTH_LEVEL"])
                        {
                            $max_depth = $arItem["DEPTH_LEVEL"];
                        }
                        if($depth < $arItem["DEPTH_LEVEL"])
                        {
                            $arChain[]=$arItem["ID"];

                        }
                        while($depth > $arItem["DEPTH_LEVEL"])
                        {
                            array_pop($arChain);
                            $depth--;
                        }
                        $arChain[count($arChain)-1] = $arItem["ID"];
                        echo "sectionListsFor0";
                        foreach($arChain as $i)
                            echo ".children['".intval($i)."']";

                        echo " = { id : ".$arItem["ID"].", name : '".CUtil::JSEscape($arItem["NAME"])."', children : Array() };\n";
                        $depth = $arItem["DEPTH_LEVEL"];
                    }
                    ?>
                </script>
                <?
                for($i = 0; $i < $max_depth; $i++)
                    echo '<select id="select_IBLOCK_SECTION_'.$i.'" onchange="change_selection(\'select_IBLOCK_SECTION_\',  0, this.value, '.$i.', \'IBLOCK_SECTION[n'.$key.']\')"><option value="0">('.GetMessage("MAIN_NO").')</option></select>&nbsp;';
                ?>
                <script type="text/javascript">
                    init_selection('select_IBLOCK_SECTION_', 0, '', 0);
                </script>
            </td>
            <td><input type="button" value="<?echo GetMessage("IBLOCK_ELEMENT_EDIT_PROP_ADD")?>" onClick="addPathRow()"></td>
        </tr>
        </table>
        </td>

    <?else:?>
        <td width="40%" class="adm-detail-valign-top"><?echo $tabControl->GetCustomLabelHTML()?></td>
        <td width="60%">
            <table id="sections" class="internal">
                <?
                if(is_array($str_IBLOCK_ELEMENT_SECTION))
                {
                    $i = 0;
                    foreach($str_IBLOCK_ELEMENT_SECTION as $section_id)
                    {
                        $rsChain = CIBlockSection::GetNavChain($IBLOCK_ID, $section_id);
                        $strPath = "";
                        while($arChain = $rsChain->GetNext())
                            $strPath .= $arChain["NAME"]."&nbsp;/&nbsp;";
                        if(strlen($strPath) > 0)
                        {
                            ?><tr>
                            <td><?echo $strPath?></td>
                            <td>
                                <input type="button" value="<?echo GetMessage("MAIN_DELETE")?>" OnClick="deleteRow(this)">
                                <input type="hidden" name="IBLOCK_SECTION[]" value="<?echo intval($section_id)?>">
                            </td>
                            </tr><?
                        }
                        $i++;
                    }
                }
                ?>
            </table>
            <script type="text/javascript">
                function deleteRow(button)
                {
                    var my_row = button.parentNode.parentNode;
                    var table = document.getElementById('sections');
                    if(table)
                    {
                        for(var i=0; i<table.rows.length; i++)
                        {
                            if(table.rows[i] == my_row)
                            {
                                table.deleteRow(i);
                                onSectionChanged();
                            }
                        }
                    }
                }
                function InS<?echo md5("input_IBLOCK_SECTION")?>(section_id, html)
                {
                    var table = document.getElementById('sections');
                    if(table)
                    {
                        if(section_id > 0 && html)
                        {
                            var cnt = table.rows.length;
                            var oRow = table.insertRow(cnt-1);

                            var i=0;
                            var oCell = oRow.insertCell(i++);
                            oCell.innerHTML = html;

                            var oCell = oRow.insertCell(i++);
                            oCell.innerHTML =
                                '<input type="button" value="<?echo GetMessage("MAIN_DELETE")?>" OnClick="deleteRow(this)">'+
                                '<input type="hidden" name="IBLOCK_SECTION[]" value="'+section_id+'">';
                            onSectionChanged();
                        }
                    }
                }
            </script>
            <input name="input_IBLOCK_SECTION" id="input_IBLOCK_SECTION" type="hidden">
            <input type="button" value="<?echo GetMessage("IBLOCK_ELEMENT_EDIT_PROP_ADD")?>..." onClick="jsUtils.OpenWindow('/bitrix/admin/iblock_section_search.php?lang=<?echo LANGUAGE_ID?>&amp;IBLOCK_ID=<?echo $IBLOCK_ID?>&amp;n=input_IBLOCK_SECTION&amp;m=y', 600, 500);">
        </td>
    <?endif;?>
    </tr>
    <input type="hidden" name="IBLOCK_SECTION[]" value="">
    <script type="text/javascript">
        function onSectionChanged()
        {
            var form = BX('<?echo CUtil::JSEscape($tabControl->GetFormName())?>');
            var url = '<?echo CUtil::JSEscape($APPLICATION->GetCurPageParam())?>';
            <?if($arIBlock["SECTION_PROPERTY"] === "Y" || defined("CATALOG_PRODUCT")):?>
            var groupField = new JCIBlockGroupField(form, 'tr_IBLOCK_ELEMENT_PROPERTY', url);
            groupField.reload();
            <?endif?>
            <?if($arIBlock["FIELDS"]["IBLOCK_SECTION"]["DEFAULT_VALUE"]["KEEP_IBLOCK_SECTION_ID"] === "Y"):?>
            InheritedPropertiesTemplates.updateInheritedPropertiesValues(false, true);
            <?endif?>
            return;
        }
    </script>
    <?
    $hidden = "";
    if(is_array($str_IBLOCK_ELEMENT_SECTION))
        foreach($str_IBLOCK_ELEMENT_SECTION as $section_id)
            $hidden .= '<input type="hidden" name="IBLOCK_SECTION[]" value="'.intval($section_id).'">';
    $tabControl->EndCustomField("SECTIONS", $hidden);
endif;?>

<?

if($arShowTabs['edit_rights']):
	$tabControl->BeginNextFormTab();
	if($ID > 0)
	{
		$obRights = new CIBlockElementRights($IBLOCK_ID, $ID);
		$htmlHidden = '';
		foreach($obRights->GetRights() as $RIGHT_ID => $arRight)
			$htmlHidden .= '
				<input type="hidden" name="RIGHTS[][RIGHT_ID]" value="'.htmlspecialcharsbx($RIGHT_ID).'">
				<input type="hidden" name="RIGHTS[][GROUP_CODE]" value="'.htmlspecialcharsbx($arRight["GROUP_CODE"]).'">
				<input type="hidden" name="RIGHTS[][TASK_ID]" value="'.htmlspecialcharsbx($arRight["TASK_ID"]).'">
			';
	}
	else
	{
		$obRights = new CIBlockSectionRights($IBLOCK_ID, $MENU_SECTION_ID);
		$htmlHidden = '';
	}

	$tabControl->BeginCustomField("RIGHTS", GetMessage("IBEL_E_RIGHTS_FIELD"));
	IBlockShowRights(
		'element',
		$IBLOCK_ID,
		$ID,
		GetMessage("IBEL_E_RIGHTS_SECTION_TITLE"),
		"RIGHTS",
		$obRights->GetRightsList(),
		$obRights->GetRights(array("count_overwrited" => true, "parents" => $str_IBLOCK_ELEMENT_SECTION)),
		false, /*$bForceInherited=*/($ID <= 0) || $bCopy
	);
	$tabControl->EndCustomField("RIGHTS", $htmlHidden);
endif;
?>

<?
/*  CUSTOM TABS START  */
//$ar_tabs_list = array_reverse($ar_tabs_list);
foreach($ar_tabs_list as $key=>$cus_tab)
{
    $name = "TAB_ID_".$cus_tab['ID'];
    $tab_name = "TAB_ENTINY_".$cus_tab['ID']."_ID";
    $edit_field_name = "";
    $tabControl->BeginNextFormTab();
    $tabControl->BeginCustomField($name, $tab_name, false);
    ?>
    <tr id="tr_<?=$tab_name?>">
        <td class="adm-detail-content-cell-l">
            <span class="adm-required-field">Название вкладки:</span>
        </td>
        <td style="white-space: nowrap;" class="adm-detail-content-cell-r">
            <?$edit_field_name = "UF_NAME_HLID_".$cus_tab["ID"];?>
            <input type="text" size="50" name="<?=$edit_field_name?>" maxlength="255" value="<?=$cus_tab["UF_NAME"]?>">
         </td>
    </tr>
    <tr>
        <td class="adm-detail-content-cell-l">
            <span class="">Сортировка вкладки:</span>
        </td>
        <td style="white-space: nowrap;" class="adm-detail-content-cell-r">
            <?$edit_field_sort = "UF_SORT_HLID_".$cus_tab["ID"];?>
            <input type="text" size="50" name="<?=$edit_field_sort?>" maxlength="255" value="<?=$cus_tab["UF_SORT"]?>">
        </td>
    </tr>
	<tr>
		<td class="adm-detail-content-cell-l">
			<span class="">Вариант отображения вкладки:</span>
		</td>
		<td style="white-space: nowrap;" class="adm-detail-content-cell-r">
			<?$edit_field_show_type = "UF_SHOW_TYPE_HLID_".$cus_tab["ID"];?>
			<select name="<?=$edit_field_show_type?>">
				<?foreach($arShowListTypes as $show_type_tab_tmp):?>
					<option
						value="<?=$show_type_tab_tmp["ID"]?>"
					    <?if($cus_tab["UF_SHOW_TYPE"]==$show_type_tab_tmp["ID"]):?>
					        selected
					    <?elseif($show_type_tab_tmp["DEF"] == "Y" && intval($show_type_tab_tmp["ID"])<=0):?>
							selected
						<?endif;?>
						>
						<?=$show_type_tab_tmp["VALUE"]?>
						</option>
				<?endforeach?>
			</select>
		</td>
	</tr>
    <?
    /**
    * Switch tab type
    *
    *   TEXT
    */?>
    <?if($ar_tabs_types[$cus_tab["UF_TAB_TYPE_ID"]]["UF_CODE"] == "TEXT"):?>
    <?$edit_field_name = "UF_HTML_TEXT_HLID_".$cus_tab["ID"];?>
    <tr class="heading" id="tr_TEXT_EDITOR_NAME_HL_<?=$cus_tab["ID"]?>">
        <td colspan="2" align="center">
            <span>Контент (text/html):</span>
        </td>
    </tr>
    <tr id="tr_UF_HTML_TEXT_HLID_<?=$cus_tab["ID"]?>">
        <td colspan="2" align="center">
            <?CFileMan::AddHTMLEditorFrame(
                $edit_field_name,
                $cus_tab['UF_HTML_TEXT'],
                "UF_HTML_TEXT_TYPE",
                'html',
                array(
                    'height' => 450,
                    'width' => '100%'
                ),
                "N",
                0,
                "",
                "",
                $arIBlock["LID"],
                false,
                false,
                array(
                    'toolbarConfig' => CFileman::GetEditorToolbarConfig("iblock_".(defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1 ? 'public' : 'admin')),
                    'saveEditorKey' => $cus_tab['ID'],
                )
            );
            ?>
        </td>
    </tr>
    <?endif;?>
    <?/**   LIST   */?>
    <?if($ar_tabs_types[$cus_tab["UF_TAB_TYPE_ID"]]["UF_CODE"] == "LIST"):?>
    <tr class="heading" id="tr_UF_LIST_HL_<?=$cus_tab["ID"]?>">
        <td align="center">
            <span>Название:</span>
        </td>
        <td align="center">
            <span>Контент:</span>
        </td>
        <td align="center">
            <span>
                <input type="button" class="button"  id="ADD_LIST_HLID_<?=$cus_tab["ID"]?>_LISTID_0" value="Добавить">
                <script>
                    <?$edit_field_name = "UF_LIST_HLID_".$cus_tab["ID"]."_UF_TEXT_LISTID_NEW0"?>
                    BX.ready(function(){
                        var new_list_<?=$cus_tab["ID"]?>_element_counter = 0;
                        BX.bind(BX('ADD_LIST_HLID_<?=$cus_tab["ID"]?>_LISTID_0'), 'click', function() {
                            var parent = document.getElementById("ADD_LIST_HLID_<?=$cus_tab["ID"]?>_LISTID_0").parentNode.parentNode.parentNode;
                            var new_elem = '<tr id="UF_LIST_HLID_<?=$cus_tab["ID"]?>_LISTID_NEW'+new_list_<?=$cus_tab["ID"]?>_element_counter+'">';
                            new_elem += '<td style="white-space: nowrap;border-top: 2px solid #e0e8ea;border-left: 4px solid #e0e8ea;border-bottom: 2px solid #e0e8ea;" align="center" class="adm-detail-content-cell-r">';
                            new_elem += '<input type="text" size="50" name="UF_LIST_HLID_<?=$cus_tab["ID"]?>_UF_NAME_LISTID_NEW'+new_list_<?=$cus_tab["ID"]?>_element_counter+'" maxlength="255" value=""></td>';
                            new_elem += '<td style="border-top: 2px solid #e0e8ea;border-bottom: 2px solid #e0e8ea;" align="center">';
                            new_elem += '<span>Текстовое поле появится после сохранения элемента.</span>';
                            new_elem += '</td><td style="border-top: 2px solid #e0e8ea;border-bottom: 2px solid #e0e8ea;border-right: 4px solid #e0e8ea;" align="center">';
                            new_elem += '<input type="button" class="button" onclick="var element = this.parentNode.parentNode;element.outerHTML = new String();delete element;"  id="DEL_LIST_HLID_<?=$cus_tab["ID"]?>_LISTID_NEW'+new_list_<?=$cus_tab["ID"]?>_element_counter+'" value="Удалить">';
                            new_elem += '</td></tr>';
                            parent.insertAdjacentHTML('afterend', new_elem);
                            new_list_<?=$cus_tab["ID"]?>_element_counter++;
                            var new_id_del = 'DEL_LIST_HLID_<?=$cus_tab["ID"]?>_LISTID_NEW'+new_list_<?=$cus_tab["ID"]?>_element_counter;
                            BX.bind(BX(new_id_del), 'click', function() {
                                var element = this.parentNode.parentNode;
                                element.outerHTML = "";
                                delete element;
                            });
                        });
                    });
                </script>
            </span>
        </td>
    </tr>
    <?foreach($cus_tab["UF_LIST"] as $ar_list_obj):?>
        <?$edit_field_name = "UF_LIST_HLID_".$cus_tab["ID"]."_UF_TEXT_LISTID_".$ar_list_obj["ID"]?>
        <tr id="UF_LIST_HLID_<?=$cus_tab["ID"]?>_LISTID_<?=$ar_list_obj["ID"]?>">
            <td style="
            white-space: nowrap;
            border-top: 2px solid #e0e8ea;
            border-left: 4px solid #e0e8ea;
            border-bottom: 2px solid #e0e8ea;
            "
                width="10%"
                align="center" class="adm-detail-content-cell-r">
                <input type="text" size="50" name="UF_LIST_HLID_<?=$cus_tab["ID"]?>_UF_NAME_LISTID_<?=$ar_list_obj["ID"]?>" maxlength="255" value="<?=$ar_list_obj["UF_NAME"]?>">
            </td>
            <td style="
            border-top: 2px solid #e0e8ea;
            border-bottom: 2px solid #e0e8ea;
            width: 1000px;
            display: inline-block;
            "
                width="80%" align="center">
                <?CFileMan::AddHTMLEditorFrame(
                    $edit_field_name,
                    $ar_list_obj["UF_TEXT"],
                    "UF_HTML_TEXT_TYPE",
                    'html',
                    array(
                        'height' => 50,
                        'width' => '80%'
                    ),
                    "N",
                    0,
                    "",
                    "",
                    $arIBlock["LID"],
                    false,
                    false,
                    array(
                        'toolbarConfig' => CFileman::GetEditorToolbarConfig("iblock_".(defined('BX_PUBLIC_MODE') && BX_PUBLIC_MODE == 1 ? 'public' : 'admin')),
                        'saveEditorKey' => $cus_tab['ID'],
                    )
                );
                ?>
            </td>
            <td style="
            border-top: 2px solid #e0e8ea;
            border-bottom: 2px solid #e0e8ea;
            border-right: 4px solid #e0e8ea;
            "
                width="10%" align="center">
                <input type="button" class="button"  id="DEL_LIST_HLID_<?=$cus_tab["ID"]?>_LISTID_<?=$ar_list_obj["ID"]?>" value="Удалить">
                <script>
                    BX.ready(function(){
                        BX.bind(BX('DEL_LIST_HLID_<?=$cus_tab["ID"]?>_LISTID_<?=$ar_list_obj["ID"]?>'), 'click', function() {
                            var element = document.getElementById("UF_LIST_HLID_<?=$cus_tab["ID"]?>_LISTID_<?=$ar_list_obj["ID"]?>");
                            element.outerHTML = "";
                            delete element;
                        });
                    });
                </script>
            </td>
        </tr>
    <?endforeach;?>
    <?endif;?>
    <?/**   EMPLOYEES   */?>
    <?if($ar_tabs_types[$cus_tab["UF_TAB_TYPE_ID"]]["UF_CODE"] == "EMPLOYEES"):?>
    <?$edit_field_name = "UF_EMPLOYEES_HLID_".$cus_tab["ID"]."[]";?>
    <tr class="heading" id="tr_UF_EMPLOYEES_NAME_HL_<?=$cus_tab["ID"]?>">
        <td colspan="2" align="center">
            <span>Сотрудники:</span>
        </td>
    </tr>
    <tr>
    <table class="internal" style="margin: 0 auto;">
        <tbody id="table_users_hlid_<?=$cus_tab["ID"]?>">
            <tr class="heading">
                <td>ФИО</td>
                <td>
                    <input type="button" class="button" id="add_user_hlid_<?=$cus_tab["ID"]?>" value="Добавить">
                    <script>
                        BX.ready(function(){
                            var counter = 0;
                            BX.bind(BX('add_user_hlid_<?=$cus_tab["ID"]?>'), 'click', function() {
                                addUser();
                            });
                            function addUser()
                            {
                                $str = '<td colspan="2"><select name="<?=$edit_field_name?>" multiple="multiple" size="10" style="margin-bottom:3px;width:350px;">';
                                $str += '<option value="" selected="">(не установлено)</option>';
                                <?foreach($arUsers as $ar_user):?>
                                $str += '<option value="<?=$ar_user["ID"]?>"> <?=$ar_user["LAST_NAME"]?> <?=$ar_user["NAME"]?> <?=$ar_user["SECOND_NAME"]?> - <span style="color:green;font-size;.8em;"><?=$ar_user["PERSONAL_PROFESSION"]?></span></option>';
                                <?endforeach;?>
                                $str += '</select></td><td></td>';
                                console.log($str);
                                var node = document.createElement("TR");
                                node.innerHTML = $str;
                                node.setAttribute('id', "hlbl_property_tr_0");
                                var element = document.getElementById("table_users_hlid_<?=$cus_tab["ID"]?>");
                                element.appendChild(node);
                                counter++;
                            }
                        });

                    </script>
                </td>
            </tr>
            <?if($cus_tab["UF_EMPLOYEES"] && !empty($cus_tab["UF_EMPLOYEES"]) && isset($cus_tab["UF_EMPLOYEES"])):?>
                <?foreach($cus_tab["UF_EMPLOYEES"] as $ar_employee):?>
                    <tr id="user_<?=$ar_employee["ID"]?>_row">
                        <td>
                            <a href="/bitrix/admin/user_edit.php?lang=ru&ID=<?=$ar_employee["ID"]?>" target="_blank">
                                <span style="color:green;font-size;.8em;"><?=$ar_employee["PERSONAL_PROFESSION"]?></span><br />
								<?=$ar_employee["LAST_NAME"]?> <?=$ar_employee["NAME"]?> <?=$ar_employee["SECOND_NAME"]?>
                            </a>
                            <input type="hidden" name="<?=$edit_field_name?>" value="<?=$ar_employee["ID"]?>">
                        </td>
                        <td>
                            <input type="button" class="button" id="del_user_<?=$ar_employee["ID"]?>" value="Удалить">
                            <script>
                                BX.ready(function(){
                                    var counter = 0;
                                    BX.bind(BX('del_user_<?=$ar_employee["ID"]?>'), 'click', function() {
                                        var element = document.getElementById("user_<?=$ar_employee["ID"]?>_row");
                                        element.outerHTML = "";
                                        delete element;
                                    });
                                });

                            </script>
                        </td>
                    </tr>
                <?endforeach;?>
            <?endif;?>
        </tbody>
    </tr>
    <?endif;?>
    <?/**   GALLERY   */?>
    <?if($ar_tabs_types[$cus_tab["UF_TAB_TYPE_ID"]]["UF_CODE"] == "GALLERY"):?>
    <table  class="internal" style="margin: 0 auto;">
    <tr class="heading">
        <td align="center">
            <span>Фото:</span>
        </td>
        <td align="center">
            <span>Видео:</span>
        </td>
    </tr>
    <tr>
    <td style="vertical-align: top;">
        <?$edit_field_name = "UF_GALLERY_HLID_".$cus_tab["ID"];?>
    <table  class="internal" style="margin: 0 auto;" >
        <tr id="add_new_photo_hlid_<?=$cus_tab["ID"]?>_count_0">
            <td></td>
            <td>
                <span class="adm-input-file">
                    <span id="file_upload_name_0">Добавить фото</span>
                    <input type="file" class="adm-designed-file add_photo" name="<?=$edit_field_name?>[]" onchange="addNewInputFile(this);">
                </span>
            </td>
        </tr>
        <script>

            var input_photo_counter = 0;
            function addNewInputFile(input)
            {
                var str_name = input.value;
                var str_id = "file_upload_name_"+input_photo_counter;
                var elem = document.getElementById(str_id);
                elem.innerText = str_name;
                this.value=null;
                var parent_id = "add_new_photo_hlid_<?=$cus_tab["ID"]?>_count_"+input_photo_counter;
                console.log(parent_id);
                var parent = document.getElementById(parent_id);
                input_photo_counter++;
                var html = '<tr id="add_new_photo_hlid_<?=$cus_tab["ID"]?>_count_'+input_photo_counter+'"><td></td><td><span class="adm-input-file"><span id="file_upload_name_'+input_photo_counter+'">Добавить фото</span>';
                html += '<input type="file" class="adm-designed-file add_photo" name="<?=$edit_field_name?>[]" onchange="addNewInputFile(this);">';
                html += '</span></td></tr>';
                parent.insertAdjacentHTML('beforebegin', html);

                return false;
            }
        </script>
        <?$edit_field_name = "UF_GALLERY_ISSET_HLID_".$cus_tab["ID"]."[]";?>
        <?foreach($cus_tab["UF_GALLERY"] as $photo):?>
            <?if($photo > 0):?>
                <tr id="TR_PHOTO_HLID_<?=$cus_tab["ID"]?>_ID_<?=$photo?>">
                    <td>
                        <?echo CFileInput::Show($edit_field_name, $photo, array(
                            "IMAGE" => "Y",
                            "PATH" => "Y",
                            "FILE_SIZE" => "Y",
                            "DIMENSIONS" => "Y",
                            "IMAGE_POPUP" => "Y",
                            "MAX_SIZE" => array(
                                "W" => COption::GetOptionString("iblock", "detail_image_size"),
                                "H" => COption::GetOptionString("iblock", "detail_image_size"),
                            ),
                        ));
                        ?>
                    </td>
                    <td>
                        <input type="button" value="Удалить" id="delete_hlid_<?=$cus_tab["ID"]?>_photo_<?=$photo?>">
                        <input type="hidden" name="<?=$edit_field_name?>" value="<?=$photo?>">
                        <script>
                            BX.bind(BX('delete_hlid_<?=$cus_tab["ID"]?>_photo_<?=$photo?>'), 'click', function() {
                                console.log(<?=$photo?>);
                                var element = document.getElementById("TR_PHOTO_HLID_<?=$cus_tab["ID"]?>_ID_<?=$photo?>");
                                element.outerHTML = "";
                                delete element;
                            });
                        </script>
                    </td>
                </tr>
            <?endif?>
        <?endforeach;?>
    </table>
    </td>
    <td style="vertical-align: top;">
        <?$edit_field_name = "UF_VIDEO_HLID_".$cus_tab["ID"];?>
        <table  class="internal" style="margin: 0 auto;">
            <tr id="add_new_video_hlid_<?=$cus_tab["ID"]?>">
                <td></td>
                <td>
                <span class="adm-input-file">
                    <span>Добавить видео</span>
                </span>
                </td>
            </tr>
            <script>
                BX.bind(BX('add_new_video_hlid_<?=$cus_tab["ID"]?>'), 'click', function() {
                    var parent = document.getElementById("add_new_video_hlid_<?=$cus_tab["ID"]?>");
                    var html = '<tr><td style="white-space: nowrap;" class="adm-detail-content-cell-r" colspan="2">';
                    html += '<input type="text" size="50" name="<?=$edit_field_name?>[]" maxlength="255" value="">';
                    html += "</td></tr>";
                    parent.insertAdjacentHTML('afterend', html);
                });
            </script>
            <tr>
                <td style="white-space: nowrap;" class="adm-detail-content-cell-r" colspan="2">
                    <input type="text" size="50" name="<?=$edit_field_name?>[]" maxlength="255" value="">
                </td>
            </tr>
            <script>
                BX.bind(BX('delete_hlid_<?=$cus_tab["ID"]?>_video_<?=$video_count?>'), 'click', function() {
                    var element = document.getElementById("TR_VIDEO_HLID_<?=$cus_tab["ID"]?>_ID_<?=$video_count?>");
                    element.outerHTML = "";
                    delete element;
                });
            </script>
            <?$edit_field_name = "UF_VIDEO_ISSET_HLID_".$cus_tab["ID"]."[]";?>
            <?$video_count = 0;?>
            <?foreach($cus_tab["UF_VIDEO"] as $video):?>
                <?if(strlen($video) > 0):?>
                    <tr id="TR_VIDEO_HLID_<?=$cus_tab["ID"]?>_ID_<?=$video_count?>">
                        <td>
                            <?$APPLICATION->IncludeComponent(
                                "bitrix:player",
                                "",
                                Array(
                                    "COMPONENT_TEMPLATE" => ".default",
                                    "PLAYER_TYPE" => "",
                                    "USE_PLAYLIST" => "N",
                                    "PATH" => $video,
                                    "PROVIDER" => "",
                                    "STREAMER" => "",
                                    "WIDTH" => "400",
                                    "HEIGHT" => "300",
                                    "PREVIEW" => "",
                                    "FILE_TITLE" => "",
                                    "FILE_DURATION" => "",
                                    "FILE_AUTHOR" => "",
                                    "FILE_DATE" => "",
                                    "FILE_DESCRIPTION" => "",
                                    "AUTOSTART" => "N",
                                    "REPEAT" => "none",
                                    "VOLUME" => "70",
                                    "SKIN_PATH" => "",
                                    "SKIN" => "",
                                    "CONTROLBAR" => "",
                                    "WMODE" => "",
                                    "LOGO" => "",
                                    "LOGO_LINK" => "",
                                    "LOGO_POSITION" => "",
                                    "PLUGINS" => array(""),
                                    "ADDITIONAL_FLASHVARS" => "",
                                    "WMODE_WMV" => "",
                                    "SHOW_CONTROLS" => "N",
                                    "SHOW_DIGITS" => "N",
                                    "CONTROLS_BGCOLOR" => "",
                                    "CONTROLS_COLOR" => "",
                                    "CONTROLS_OVER_COLOR" => "",
                                    "SCREEN_COLOR" => "",
                                    "MUTE" => "N",
                                    "ADVANCED_MODE_SETTINGS" => "N",
                                    "PLAYER_ID" => "",
                                    "BUFFER_LENGTH" => "",
                                    "DOWNLOAD_LINK" => "",
                                    "DOWNLOAD_LINK_TARGET" => "",
                                    "ADDITIONAL_WMVVARS" => "",
                                    "ALLOW_SWF" => "N"
                                )
                            );?>
                        </td>
                        <td>
                            <input type="button" value="Удалить" id="delete_hlid_<?=$cus_tab["ID"]?>_video_<?=$video_count?>">
                            <input type="hidden" name="<?=$edit_field_name?>" value="<?=$video?>">
                            <script>
                                BX.bind(BX('delete_hlid_<?=$cus_tab["ID"]?>_video_<?=$video_count?>'), 'click', function() {
                                    var element = document.getElementById("TR_VIDEO_HLID_<?=$cus_tab["ID"]?>_ID_<?=$video_count?>");
                                    element.outerHTML = "";
                                    delete element;
                                });
                            </script>
                        </td>
                    </tr>
                    <?$video_count++;?>
                <?endif?>
            <?endforeach;?>
        </table>
    </td>
    </tr>
    <?endif;?>
    <?/**   STRUCTURE_BAYAN   */?>
    <?if($ar_tabs_types[$cus_tab["UF_TAB_TYPE_ID"]]["UF_CODE"] == "STRUCTURE_BAYAN"):?>
        <table  class="internal" style="margin: 0 auto;">
            <tbody>
                <tr class="heading">
                    <td align="center">
                        <span>Изменить выбранные структурные элементы:</span>
                    </td>
                    <td align="center">
                        <span>Изменить выбранные структурные вкладки:</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table  class="internal" style="margin: 0 auto;">
                            <tbody>
                                <tr class="heading">
                                    <td align="center">
                                        <span>Структура:</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?$l = CIBlockSection::GetTreeList(Array("IBLOCK_ID"=>$IBLOCK_ID), array("ID", "NAME", "DEPTH_LEVEL"));?>
                                        <select
                                            name="UF_STRUCTURE_BAYAN_HLID_<?=$cus_tab["ID"]?>[]"
                                            size="14"
                                            multiple
                                            id="UF_STRUCTURE_BAYAN_HLID_<?=$cus_tab["ID"]?>"
                                            >
                                            <option value="0"<?if(is_array($str_IBLOCK_ELEMENT_SECTION) && in_array(0, $str_IBLOCK_ELEMENT_SECTION))echo " selected"?>><?echo GetMessage("IBLOCK_UPPER_LEVEL")?></option>
                                            <?
                                            while($ar_l = $l->GetNext()):
                                                ?>
                                                <option
                                                    value="<?echo $ar_l["ID"]?>"
                                                    <?if(in_array($ar_l["ID"],$cus_tab["UF_STRUCTURE_BAYAN"]))echo " selected"?>
                                                    >
                                                        <?echo str_repeat(" . ", $ar_l["DEPTH_LEVEL"])?>
                                                        <?echo $ar_l["NAME"]?>
                                                </option><?
                                            endwhile;
                                            ?>
                                        </select>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                </tr>
    <?endif;?>
    <?/**   STRUCTURE   */?>
    <?if($ar_tabs_types[$cus_tab["UF_TAB_TYPE_ID"]]["UF_CODE"] == "STRUCTURE"):?>
        <table  class="internal" style="margin: 0 auto;">
            <tbody>
                <tr class="heading">
                    <td align="center">
                        <span>Изменить выбранные структурные элементы:</span>
                    </td>
                    <td align="center">
                        <span>Изменить выбранные структурные вкладки:</span>
                    </td>
                </tr>
                <tr>
                    <td>
                        <table  class="internal" style="margin: 0 auto;">
                            <tbody>
                                <tr class="heading">
                                    <td align="center">
                                        <span>Структура:</span>
                                    </td>
                                    <td align="center">
                                        <span>Применить:</span>
                                    </td>
                                </tr>
                                <tr>
                                    <td>
                                        <?$l = CIBlockSection::GetTreeList(Array("IBLOCK_ID"=>$IBLOCK_ID), array("ID", "NAME", "DEPTH_LEVEL"));?>
                                        <select
                                            name="AJAX_STRUCTURE_SECTION_HLID_<?=$cus_tab["ID"]?>[]"
                                            size="14"
                                            multiple onchange="onSectionChanged()"
                                            id="AJAX_STRUCTURE_SECTION_HLID_<?=$cus_tab["ID"]?>"
                                            >
                                            <option value="0"<?if(is_array($str_IBLOCK_ELEMENT_SECTION) && in_array(0, $str_IBLOCK_ELEMENT_SECTION))echo " selected"?>><?echo GetMessage("IBLOCK_UPPER_LEVEL")?></option>
                                            <?
                                            while($ar_l = $l->GetNext()):
                                                ?><option value="<?echo $ar_l["ID"]?>"<?if(is_array($str_IBLOCK_ELEMENT_SECTION) && in_array($ar_l["ID"], $str_IBLOCK_ELEMENT_SECTION))echo " selected"?>><?echo str_repeat(" . ", $ar_l["DEPTH_LEVEL"])?><?echo $ar_l["NAME"]?></option><?
                                            endwhile;
                                            ?>
                                        </select>
                                    </td>
                                    <td>
                                        <input type="button" value="Применить" id="add_ajax_scructure_hlid_<?=$cus_tab["ID"]?>">
                                        <script>
                                            function getSelectValues(select)
                                            {
                                                var result = [];
                                                var options = select && select.options;
                                                var opt;

                                                for (var i=0, iLen=options.length; i<iLen; i++) {
                                                    opt = options[i];

                                                    if (opt.selected) {
                                                        result.push(opt.value || opt.text);
                                                    }
                                                }
                                                return result;
                                            }
                                            BX.bind(BX('add_ajax_scructure_hlid_<?=$cus_tab["ID"]?>'), 'click', function() {
                                                var el = document.getElementById("AJAX_STRUCTURE_SECTION_HLID_<?=$cus_tab["ID"]?>");
                                                send_ajax_struct(getSelectValues(el), <?=$cus_tab["ID"]?>);
                                            });
                                            function send_ajax_struct(selectStructureArray , cus_tab)
                                            {
                                                var current_tab = cus_tab;
                                                var select_data = {
                                                    "data":  selectStructureArray
                                                };
                                                console.log(select_data);
                                                BX.ajax.post(
                                                    '<?=explode('/home/bitrix/www', __DIR__)[1]."/return_ajax_structure_tabs.php?SELECT_STRUCTURE=Y&UF_LANGUAGE=".array_shift(array_values($PROP["UF_LANGUAGE"]["VALUE"]))?>',
                                                    selectStructureArray,
                                                    function(data){
                                                        var response_data = JSON.parse(data);
                                                        console.log(response_data);
                                                        for (element of response_data )
                                                        {
                                                            logArrayElements(element, current_tab)
                                                        }
//                                                        response_data.forEach(logArrayElements(current_tab));
                                                    }
                                                );
                                                delete current_tab;
                                            }
                                            function logArrayElements(element, current_tab) {
                                                console.log(current_tab);
                                                var html_obj = new HTMLCheckboxTR(current_tab);
                                                if(html_obj.SetSectionID(element.SECTION_ID))
                                                {
                                                    html_obj.SetSectionName(element.UF_SECTION_NAME);
                                                    for(var key in element.UF_TABS)
                                                    {
                                                        if (element.UF_TABS.hasOwnProperty(key))
                                                        {
                                                            html_obj.PushTab(element.UF_TABS[key]["ID"], element.UF_TABS[key]["UF_NAME"]);
                                                        }
                                                    }
                                                    html_obj.InsertTRInTable();
                                                }


                                            }
                                            var HTMLCheckboxTR = function(cus_tab)
                                            {
                                                console.log(cus_tab);
                                                this.SECTION_ID         = "";
                                                this.UF_SECTION_NAME    = "";
                                                this.UF_TABS            = [];
                                                this.HTML_TR = "";
                                                this.SetSectionID = function(id)
                                                {
                                                    if(!this.CheckTRBySectionId(id))
                                                    {
                                                        this.SECTION_ID = id;
                                                        return this.SECTION_ID;
                                                    }
                                                    else
                                                    {
                                                        return false;
                                                    }
                                                }
                                                this.CheckTRBySectionId = function(section_id)
                                                {
                                                    var isset = document.getElementsByClassName("STRUCTURELIST_HLID_"+cus_tab+"_SECTIONID_"+section_id);
                                                    if(isset.length >0)
                                                    {
                                                        return isset;
                                                    }
                                                    else
                                                    {
                                                        return false;
                                                    }
                                                }
                                                this.SetSectionName = function(name)
                                                {
                                                    this.UF_SECTION_NAME = name;
                                                }
                                                this.PushTab = function(id, name)
                                                {
                                                    if ((typeof id != 'undefined') && (typeof name != 'undefined'))
                                                    {
                                                        this.UF_TABS.push({id: id, name: name});
                                                    }
                                                }
                                                this.MakeNewTR = function()
                                                {
                                                    this.HTML_TR += '<tr><td align="center">';
                                                    this.HTML_TR += '<span id="HLID_'+cus_tab+'_STRUCTUREID_0_SECTIONID_'+this.SECTION_ID+'" class="STRUCTURELIST_HLID_<?=$cus_tab["ID"]?>_SECTIONID_'+this.SECTION_ID+'">';
                                                    this.HTML_TR += this.UF_SECTION_NAME+'</span></td><td>';
                                                    if(this.UF_TABS.length > 0)
                                                    {
                                                        for(var i in this.UF_TABS)
                                                        {
                                                            this.HTML_TR += '<input type="checkbox"';
                                                            this.HTML_TR += 'name="HLID_'+cus_tab+'_STRUCTUREID_0_SECTIONID_'+this.SECTION_ID+'[]"';
                                                            this.HTML_TR += 'value="'+this.UF_TABS[i]["id"]+'"';
                                                            this.HTML_TR += '>'+this.UF_TABS[i]["name"]+'<br>';
                                                        }
                                                    }
                                                    else
                                                    {
                                                        this.HTML_TR += '<span>У этой структуры вкладки отсутствуют.</span>';
                                                        this.HTML_TR += '<input type="hidden" name="HLID_'+cus_tab+'_STRUCTUREID_0_SECTIONID_'+this.SECTION_ID+'[]" >';
                                                    }
                                                    this.HTML_TR += '</td></tr>';
                                                }
                                                this.GetNewTR = function()
                                                {
                                                    this.MakeNewTR();
                                                    return this.HTML_TR;
                                                }
                                                this.InsertTRInTable = function()
                                                {
                                                    var parent = document.getElementById("head_sections_tabs_list_hlid_"+cus_tab);
                                                    parent.insertAdjacentHTML('afterend', this.GetNewTR());
                                                }
                                            }
                                        </script>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </td>
                    <td style="vertical-align: top;">
                        <table  class="internal" style="margin: 0 auto;">
                            <tbody>
                                <tr class="heading" id="head_sections_tabs_list_hlid_<?=$cus_tab["ID"]?>">
                                    <td align="center">
                                        <span>Структурные элементы:</span>
                                    </td>
                                    <td align="center">
                                        <span>Выбранные вкладки:</span>
                                    </td>
                                </tr>
                                <?foreach($cus_tab["UF_STRUCTURE"] as $structure_element):?>
                                    <tr>
                                        <td align="center">
                                            <span
                                                id="HLID_<?=$cus_tab["ID"]?>_STRUCTUREID_<?=$structure_element["ID"]?>_SECTIONID_<?=$structure_element["UF_SECTION_ID"]?>"
                                                class="STRUCTURELIST_HLID_<?=$cus_tab["ID"]?>_SECTIONID_<?=$structure_element["UF_SECTION_ID"]?>"
                                            >
                                                <?=$structure_element["UF_SECTION_NAME"]?>
                                            </span>
                                        </td>
                                        <td>
                                            <?if(!empty($structure_element["UF_TABS"])):?>
                                                <?foreach($structure_element["UF_TABS"] as $structure_tab):?>
                                                    <input
                                                        type="checkbox"
                                                        name="HLID_<?=$cus_tab["ID"]?>_STRUCTUREID_<?=$structure_element["ID"]?>_SECTIONID_<?=$structure_element["UF_SECTION_ID"]?>[]"
                                                        value="<?=$structure_tab["ID"]?>"
                                                        <?if(in_array($structure_tab["ID"],$structure_element["UF_SELECTED_TABS"])):?>checked<?endif;?>
                                                    ><?=$structure_tab["UF_NAME"]?><br>
                                                <?endforeach;?>
                                            <?else:?>
                                                <span>У этой структуры вкладки отсутствуют.</span>
                                                <input type="hidden" name="HLID_<?=$cus_tab["ID"]?>_STRUCTUREID_<?=$structure_element["ID"]?>_SECTIONID_<?=$structure_element["UF_SECTION_ID"]?>[]" >
                                            <?endif;?>
                                        </td>
                                    </tr>
                                <?endforeach;?>
                            </tbody>
                        </table>
                    </td>
                </tr>
    <?endif;?>
    <?
    $tabControl->EndCustomField($name,'');
}

/*  CUSTOM TABS END  */



$bDisabled =
    ($view=="Y")
    || ($bWorkflow && $prn_LOCK_STATUS=="red")
    || (
        (($ID <= 0) || $bCopy)
        && !CIBlockSectionRights::UserHasRightTo($IBLOCK_ID, $MENU_SECTION_ID, "section_element_bind")
    )
    || (
        (($ID > 0) && !$bCopy)
        && !CIBlockElementRights::UserHasRightTo($IBLOCK_ID, $ID, "element_edit")
    )
    || (
        $bBizproc
        && !$canWrite
    )
;

if (!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1):
    ob_start();
    ?>
    <input <?if ($bDisabled) echo "disabled";?> type="submit" class="adm-btn-save" name="save" id="save" value="<?echo GetMessage("IBLOCK_EL_SAVE")?>">
    <? if (!$bAutocomplete)
{
    ?><input <?if ($bDisabled) echo "disabled";?> type="submit" class="button" name="apply" id="apply" value="<?echo GetMessage('IBLOCK_APPLY')?>"><?
}
    ?>
    <input <?if ($bDisabled) echo "disabled";?> type="submit" class="button" name="dontsave" id="dontsave" value="<?echo GetMessage("IBLOCK_EL_CANC")?>">
    <? if (!$bAutocomplete)
{
    ?><input <?if ($bDisabled) echo "disabled";?> type="submit" class="adm-btn-add" name="save_and_add" id="save_and_add" value="<?echo GetMessage("IBLOCK_EL_SAVE_AND_ADD")?>"><?
}
    $buttons_add_html = ob_get_contents();
    ob_end_clean();
    $tabControl->Buttons(false, $buttons_add_html);
elseif(!$bPropertyAjax && $nobuttons !== "Y"):

    $wfClose = "{
		title: '".CUtil::JSEscape(GetMessage("IBLOCK_EL_CANC"))."',
		name: 'dontsave',
		id: 'dontsave',
		action: function () {
			var FORM = this.parentWindow.GetForm();
			FORM.appendChild(BX.create('INPUT', {
				props: {
					type: 'hidden',
					name: this.name,
					value: 'Y'
				}
			}));
			this.disableUntilError();
			this.parentWindow.Submit();
		}
	}";
    $save_and_add = "{
		title: '".CUtil::JSEscape(GetMessage("IBLOCK_EL_SAVE_AND_ADD"))."',
		name: 'save_and_add',
		id: 'save_and_add',
		className: 'adm-btn-add',
		action: function () {
			var FORM = this.parentWindow.GetForm();
			FORM.appendChild(BX.create('INPUT', {
				props: {
					type: 'hidden',
					name: 'save_and_add',
					value: 'Y'
				}
			}));

			this.parentWindow.hideNotify();
			this.disableUntilError();
			this.parentWindow.Submit();
		}
	}";
    $cancel = "{
		title: '".CUtil::JSEscape(GetMessage("IBLOCK_EL_CANC"))."',
		name: 'cancel',
		id: 'cancel',
		action: function () {
			BX.WindowManager.Get().Close();
			if(window.reloadAfterClose)
				top.BX.reload(true);
		}
	}";
    $tabControl->ButtonsPublic(array(
        '.btnSave',
        ($ID > 0 && $bWorkflow? $wfClose: $cancel),
        $save_and_add,
    ));
endif;

$tabControl->Show();

if (
    (!defined('BX_PUBLIC_MODE') || BX_PUBLIC_MODE != 1)
    && CIBlockRights::UserHasRightTo($IBLOCK_ID, $IBLOCK_ID, "iblock_edit")
    && !$bAutocomplete
)
{

    echo
    BeginNote(),
    GetMessage("IBEL_E_IBLOCK_MANAGE_HINT"),
        ' <a href="/bitrix/admin/iblock_edit.php?type='.htmlspecialcharsbx($type).'&amp;lang='.LANGUAGE_ID.'&amp;ID='.$IBLOCK_ID.'&amp;admin=Y&amp;return_url='.urlencode("/bitrix/admin/".CIBlock::GetAdminElementEditLink($IBLOCK_ID, $ID, array("WF" => ($WF=="Y"? "Y": null), "find_section_section" => intval($find_section_section), "return_url" => (strlen($return_url)>0? $return_url: null)))).'">',
    GetMessage("IBEL_E_IBLOCK_MANAGE_HINT_HREF"),
    '</a>',
    EndNote()
    ;
}

$APPLICATION->AddHeadScript('/local/php_interface/include/customs/element_edit_form_functions.js');
//////////////////////////
//END of the custom form
//////////////////////////