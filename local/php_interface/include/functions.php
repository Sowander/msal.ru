<?
global $USER;
session_start();

//function getSoapClient($URL = false){
function getSoapClient($URL = '', $XML_USER_ID = 0, $STEP = 0){
    //echo "0";
    //шлем инфу в 1С
    if($URL){
        //echo "1";
        if(!function_exists('is_soap_fault')){
            ShowError("Не настроен web сервер. Не найден модуль php-soap.");
            if($STEP && $XML_USER_ID){
                saveToLog2("getSoapClient - Не настроен web сервер. Не найден модуль php-soap.\n", intval($XML_USER_ID), $STEP);
            }
            //print 'Не настроен web сервер. Не найден модуль php-soap.';
            return false;
        }
        try{
            $client = new SoapClient($URL,
                array(
                    "login" => "Avrobus",
                    "password" => "avrobus",
                    'soap_version' => SOAP_1_2,
                    'trace' => true,
                    'exceptions' => true,
                    'cache_wsdl' => WSDL_CACHE_NONE
                )
            );
        }catch(SoapFault $e){
            ShowError("Ошибка подключения или внутренняя ошибка сервера. Не удалось связаться с базой 1С.");
            if($STEP && $XML_USER_ID){
                saveToLog2("getSoapClient - Ошибка подключения или внутренняя ошибка сервера. Не удалось связаться с базой 1С.\n", intval($XML_USER_ID), $STEP);
            }
            //trigger_error('Ошибка подключения или внутренняя ошибка сервера. Не удалось связаться с базой 1С.', E_ERROR);
            //echo "<pre>".print_r($e, 1)."</pre>";
            return false;
        }
        if(is_soap_fault($client)){
            ShowError("Ошибка подключения или внутренняя ошибка сервера. Не удалось связаться с базой 1С.");
            if($STEP && $XML_USER_ID){
                saveToLog2("getSoapClient - Ошибка подключения или внутренняя ошибка сервера. Не удалось связаться с базой 1С.\n", intval($XML_USER_ID), $STEP);
            }
            //trigger_error('Ошибка подключения или внутренняя ошибка сервера. Не удалось связаться с базой 1С.', E_ERROR);
            return false;
        }else{
            return $client;
        }
    }else{
        ShowError("Не заполнено поле URL.");
        if($STEP && $XML_USER_ID){
            saveToLog2("getSoapClient - Не заполнено поле URL.\n", intval($XML_USER_ID), $STEP);
        }
        return false;
    }
}

function isEmptyArr1C($arr = array(), $step = 0, $hideErrFl = false){
    global $USER;
    if(intval($step)>0){
        $step = 'step_'.$step.'.php';
    }else{
        $step = 'index.php';
    }
    //проверка на пустой ответ из 1С
    if(intval($arr["IDMain"]) == 0){
        if(!isset($_SESSION['enrollee']['EMPTY_1C'])){
            $_SESSION['enrollee']['EMPTY_1C'] = 0;
        }
        if(intval($_SESSION['enrollee']['EMPTY_1C']) < 5){
            $_SESSION['enrollee']['EMPTY_1C']++;
            sleep(5);
            if($hideErrFl){
                LocalRedirect('/enrollee/'.$step);
            }else{
                LocalRedirect('/enrollee/'.$step.'?msg=wrngs');
            }
            die();
        }else{
            unset($_SESSION['enrollee']['EMPTY_1C']);
            $USER->Logout();
            $err = urlencode("Большая нагрузка на сервис. Попробуйте авторизоваться еще раз.");
            LocalRedirect('/enrollee/?err='.$err);
            die();
        }
    }
}


//основные данные
function getAbitMainFldsToMaindata($XML_USER_ID = 0, $STEP = 0){
    $arFields1C = array();
    $clientRez = getSoapClient(WSABITURS_URL, intval($XML_USER_ID), $STEP);
    if($clientRez){
        $paramsRez = array("ID" => intval($XML_USER_ID));
        try{
            $rez = $clientRez->ПолучитьСписокАбитуриентов($paramsRez);
            $arFields1C = json_decode(json_encode($rez->return->Состав->Абитуриент), TRUE);
            isEmptyArr1C($arFields1C, $STEP);
            /*
            if(intval($arFields1C["IDMain"]) == 0){
                sleep(5);
                //header('Location: /enrollee/step_'.$STEP.'.php?msg=wrngs', true, 301);
                LocalRedirect('/enrollee/step_'.$STEP.'.php?msg=wrngs');
                die();
            }
            */
            if($STEP){
                saveToLog2("Абитуриент\n".print_r($arFields1C,1), intval($XML_USER_ID), $STEP);
            }
        }catch (Exception $e){
            ShowError('Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n");
            if($STEP){
                saveToLog2('Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n", intval($XML_USER_ID), $STEP);
            }
        }
    }
    $arResult = array(
        'Online' => true,

        //'Email' => $arFields1C["ЭлПочта"], //Email
        //'DocType' => $arFields1C["ПаспортВид"], //Удостоверение личности - тип документа
        //'Series' => $arFields1C["ПаспортСерия"], //Удостоверение личности - серия документа
        //'Number' => $arFields1C["ПаспортНомер"], //Удостоверение личности - номер документа

        //'DateOfIssue' => $arFields1C["ПаспортДата"], //Удостоверение личности - Дата выдачи
        'DepartmentCode' => $arFields1C["ПаспортКодПодразделения"],//Удостоверение личности - Код подразделения
        'Department' => $arFields1C["ПаспортВыдан"],//Удостоверение личности - Кем выдан
        'DocType2' => $arFields1C["ПаспортВид2"], //Тип удостоверения личности №2 - тип документа
        'Series2' => $arFields1C["ПаспортСерия2"], //ЕГЭ - серия
        'Number2' => $arFields1C["ПаспортНомер2"], //ЕГЭ - номер
        //'DateOfIssue2' => $arFields1C["ПаспортДата2"], //ЕГЭ - дата
        //[ПаспортВыдан2] =>
        //[ПаспортКодПодразделения2] =>
        'Name' => $arFields1C["Имя"],   //Имя
        'Family' => $arFields1C["Фамилия"],    //Фамилию
        'Surname' => $arFields1C["Отчество"],  //Отчество
        'DateOfBirth' => $arFields1C["ДатаРождения"], //Дата рождения
        'Sex' => '0', //Пол
        'NeedHostel' => '0', //нуждаюсь в общежитии
        'Foreigner' => '0', //иностранный гражданин
        'PlaceOfBirth' => $arFields1C["МестоРождения"], //Место рождения
        'Patriality' => $arFields1C["Гражданство"], //Гражданство
        'RegionFIS' => $arFields1C["РегионДляФИС"],  //Регион (для ФИС)
        'PlaceTypeFIS' => $arFields1C["ТипНаселенногоПунктаДляФИС"], //Тип населенного пункта
        'IndexR' => $arFields1C["ИндексР"], //Адрес регистрации - Индекс
        'CityR' => $arFields1C["ГородР"], //Адрес регистрации - Город
        'RegionR' => $arFields1C["РегионР"], //Адрес регистрации - Регион
        'LocalityR' => $arFields1C["НасПунктР"], //Адрес регистрации - Населенный пункт
        'StreetR' => $arFields1C["УлицаР"], //Адрес регистрации - Улица
        'HouseR' => $arFields1C["ДомР"], //Адрес регистрации - Дом
        'FlatR' => $arFields1C["КвартираР"], //Адрес регистрации - Квартира
        //КорпусР
        'IndexL' => $arFields1C["ИндексП"], //Адрес проживания - Индекс
        'CityL' => $arFields1C["ГородП"], //Адрес проживания - Город
        'RegionL' => $arFields1C["РегионП"], //Адрес проживания - Регион
        'LocalityL' => $arFields1C["НасПунктП"], //Адрес проживания - Населенный пункт
        'StreetL' => $arFields1C["УлицаП"], //Адрес проживания - Улица
        'HouseL' => $arFields1C["ДомП"], //Адрес проживания - Дом
        'FlatL' => $arFields1C["КвартираП"], //Адрес проживания - Квартира
        //КорпусП
        'TelContact' => $arFields1C["ТелефонКонтактный"], //Телефон контактный
        'TelHome' => $arFields1C["ТелефонДомашний"], //Телефон домашний
        'TelWork' => $arFields1C["ТелефонСлужебный"], //Телефон служебный
        'MaritalStatus' => $arFields1C["СемейноеПоложение"], //Семейное положение
        'VisitedCourses' => $arFields1C["ПосещениеПодготовительныхКурсов"],
        //CoursesOrganisation - ЗаведениеПодготовительныхКурсов
        'Workplace' => $arFields1C["МестоРаботы"], //Место работы
        'Workpost' => $arFields1C["Должность"], //Трудоустройство/должность
        'RequestStatus' => $arFields1C["СтатусЗаявки"],
        /*
                [ОтношениеКВоинскойСлужбе] =>
                [ВидДокументаВС] =>
                [ДокументВСНомер] =>
                [ДокументВСДата] => 0001-01-01
                [ДокументВСМестоВыдачи] =>
                [Военкомат] =>
                [ВоинскоеЗвание] =>
                [КатегорияЗапаса] =>
                [КатегорияГодностиКВоеннойСлужбе] =>
                [СтатусЗаявки]
        */
    );
    //Пол
    if($arFields1C["Пол"] == "Мужской"){
        $arResult['Sex'] = 1;
    }
    //нуждаюсь в общежитии
    if($arFields1C["НужноОбщежитие"]){
        $arResult['NeedHostel'] = 1;
    }
    //иностранный гражданин
    if($arFields1C["ИностранныйГражданин"]){
        $arResult['Foreigner'] = 1;
    }
    return $arResult;
}
//состав семьи
function getAbitFamilyFldsToMaindata($XML_USER_ID = 0, $STEP = 0){
    $arFields1C = array();
    $arResult = array();
    $arBuf = array();
    $clientRez = getSoapClient(WSABITURS_URL, intval($XML_USER_ID), $STEP);
    if($clientRez){
        $paramsRez = array("ID" => intval($XML_USER_ID));
        try{
            $rez = $clientRez->ПолучитьСписокАбитуриентов($paramsRez);
            $arFieldsAb1C = json_decode(json_encode($rez->return->Состав->Абитуриент), TRUE);
            isEmptyArr1C($arFieldsAb1C, $STEP);
            /*
            if(intval($arFieldsAb1C["IDMain"]) == 0){
                sleep(5);
                //header('Location: /enrollee/step_'.$STEP.'.php?msg=wrngs', true, 301);
                LocalRedirect('/enrollee/step_'.$STEP.'.php?msg=wrngs');
                die();
            }
            */
            $arFields1C = json_decode(json_encode($rez->return->Состав->СоставСемьи),TRUE);
            if($STEP){
                saveToLog2("СоставСемьи\n".print_r($arFields1C,1), intval($XML_USER_ID), $STEP);
            }
            if(is_array($arFields1C[0])){
                $arBuf = $arFields1C;
            }elseif(is_array($arFields1C)){
                $arBuf[0] = $arFields1C;
            }else{
                $arBuf = array();
            }
            $arFields1C = $arBuf;
        }catch (Exception $e){
            ShowError('Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n");
            if($STEP){
                saveToLog2('Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n", intval($XML_USER_ID), $STEP);
            }
        }
    }
    foreach($arFields1C as $arItem){
        $arResult[] = array(
            'ID'    => "",
            'RelationType'  => $arItem["СтепеньРодства"],
            'FIO' => $arItem["ФИО"],
            'DateOfBirth' => $arItem["ДатаРождения"],
            'WorkPlace' => $arItem["МестоРаботы"],
            'Tel' => $arItem["Телефон"],
            'Address' => $arItem["Адрес"]
        );
    }
    return $arResult;
}

//Иностранные Языки
function getAbitLangFldsToMaindata($XML_USER_ID = 0, $STEP = 0){
    $arFields1C = array();
    $arResult = array();
    $arBuf = array();
    $clientRez = getSoapClient(WSABITURS_URL, intval($XML_USER_ID), $STEP);
    if($clientRez){
        $paramsRez = array("ID" => intval($XML_USER_ID));
        try{
            $rez = $clientRez->ПолучитьСписокАбитуриентов($paramsRez);
            $arFieldsAb1C = json_decode(json_encode($rez->return->Состав->Абитуриент), TRUE);
            isEmptyArr1C($arFieldsAb1C, $STEP);
            /*
            if(intval($arFieldsAb1C["IDMain"]) == 0){
                sleep(5);
                //header('Location: /enrollee/step_'.$STEP.'.php?msg=wrngs', true, 301);
                LocalRedirect('/enrollee/step_'.$STEP.'.php?msg=wrngs');
                die();
            }
            */
            $arFields1C = json_decode(json_encode($rez->return->Состав->ИностранныеЯзыки),TRUE);
            if($STEP){
                saveToLog2("ИностранныеЯзыки\n".print_r($arFields1C,1), intval($XML_USER_ID), $STEP);
            }
            if(is_array($arFields1C[0])){
                $arBuf = $arFields1C;
            }elseif(is_array($arFields1C)){
                $arBuf[0] = $arFields1C;
            }else{
                $arBuf = array();
            }
            $arFields1C = $arBuf;
        }catch (Exception $e){
            ShowError('Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n");
            if($STEP){
                saveToLog2('Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n", intval($XML_USER_ID), $STEP);
            }
        }
    }
    foreach($arFields1C as $arItem){
        $arResult[] = array(
            "ID"    => "",
            "Language" => $arItem["ИностранныйЯзык"],
            "IsMain" => ($arItem["Основной"]) ? true : false,
            //"" => $arItem["check_language"]
        );
    }
    return $arResult;
}
//Файлы абитуриента ($fl=true - на выходе массив с ключами тип докомента)
function getAbitFilesToMaindata($XML_USER_ID = 0, $fl=false, $STEP = 0){
    $arFields1C = array();
    $arResult = array();
    $arBuf = array();
    $clientRez = getSoapClient(WSABITURS_URL, intval($XML_USER_ID), $STEP);
    if($clientRez){
        $paramsRez = array("ID" => intval($XML_USER_ID));
        try{
            $rez = $clientRez->ПолучитьСписокАбитуриентов($paramsRez);
            $arFieldsAb1C = json_decode(json_encode($rez->return->Состав->Абитуриент), TRUE);
            isEmptyArr1C($arFieldsAb1C, $STEP);
            /*
            if(intval($arFieldsAb1C["IDMain"]) == 0){
                sleep(5);
                //header('Location: /enrollee/step_'.$STEP.'.php?msg=wrngs', true, 301);
                LocalRedirect('/enrollee/step_'.$STEP.'.php?msg=wrngs');
                die();
            }
            */
            $arFields1C = json_decode(json_encode($rez->return->Состав->ФайлыАбитуриента), TRUE);
            if($STEP){
                saveToLog2("ФайлыАбитуриента\n".print_r($arFields1C,1), intval($XML_USER_ID), $STEP);
            }
            if(is_array($arFields1C[0])){
                $arBuf = $arFields1C;
            }elseif(is_array($arFields1C)){
                $arBuf[0] = $arFields1C;
            }else{
                $arBuf = array();
            }
            $arFields1C = $arBuf;
        }catch (Exception $e){
            ShowError('Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n");
            if($STEP){
                saveToLog2('Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n", intval($XML_USER_ID), $STEP);
            }
        }
    }
    foreach($arFields1C as $arItem){
        if($fl){
            $arResult[$arItem["ТипДокумента"]] = array(
                "ID"        =>	"",
                "File"      =>	"",
                "FilePath"  =>	$arItem["ПутьКФайлу"],
                "FileType"  =>  $arItem["ТипДокумента"],
                "DateChange"=>  $arItem["ДатаИзменения"],
            );
        }else{
            $arResult[] = array(
                "ID"        =>	"",
                "File"      =>	"",
                "FilePath"  =>	$arItem["ПутьКФайлу"],
                "FileType"  =>  $arItem["ТипДокумента"],
                "DateChange"=>  $arItem["ДатаИзменения"],
            );
        }

    }
    return $arResult;
}
//Предоставленные документы ($fl=true - на выходе массив с ключами вид докомента)
function getAbitDocsToMaindata($XML_USER_ID = 0, $fl=false, $STEP = 0){
    $arFields1C = array();
    $arResult = array();
    $arBuf = array();
    $clientRez = getSoapClient(WSABITURS_URL, intval($XML_USER_ID), $STEP);
    if($clientRez){
        $paramsRez = array("ID" => intval($XML_USER_ID));
        try{
            $rez = $clientRez->ПолучитьСписокАбитуриентов($paramsRez);
            $arFieldsAb1C = json_decode(json_encode($rez->return->Состав->Абитуриент), TRUE);
            isEmptyArr1C($arFieldsAb1C, $STEP);
            /*
            if(intval($arFieldsAb1C["IDMain"]) == 0){
                sleep(5);
                //header('Location: /enrollee/step_'.$STEP.'.php?msg=wrngs', true, 301);
                LocalRedirect('/enrollee/step_'.$STEP.'.php?msg=wrngs');
                die();
            }
            */
            $arFields1C = json_decode(json_encode($rez->return->Состав->ПоданныеДокументы), TRUE);
            if($STEP){
                saveToLog2("ПоданныеДокументы\n".print_r($arFields1C,1), intval($XML_USER_ID), $STEP);
            }
            if(is_array($arFields1C[0])){
                $arBuf = $arFields1C;
            }elseif(is_array($arFields1C)){
                $arBuf[0] = $arFields1C;
            }else{
                $arBuf = array();
            }
            $arFields1C = $arBuf;
        }catch (Exception $e){
            ShowError('Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n");
            if($STEP){
                saveToLog2('Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n", intval($XML_USER_ID), $STEP);
            }
        }
    }
    foreach($arFields1C as $arItem){
        if($fl){
            $arResult[$arItem["ВидДокумента"]] = array(
                "ID" => "",
                "Code" => $arItem["ВидДокумента"], //Документ об образовании - тип
                "Serial" => $arItem["Серия"], //Серия документа
                "Number" => $arItem["Номер"], //Номер документа
                "Date" => $arItem["Дата"], //Дата
            );
        }else{
            $arResult[] = array(
                "ID" => "",
                "Code" => $arItem["ВидДокумента"], //Документ об образовании - тип
                "Serial" => $arItem["Серия"], //Серия документа
                "Number" => $arItem["Номер"], //Номер документа
                "Date" => $arItem["Дата"], //Дата
            );
        }

    }
    return $arResult;
}

//Заявления документы
function getAbitAppToMaindata($XML_USER_ID = 0, $STEP = 0){
    $arFields1C = array();
    $arResult = array();
    $arBuf = array();
    $clientRez = getSoapClient(WSABITURS_URL, intval($XML_USER_ID), $STEP);
    if($clientRez){
        $paramsRez = array("ID" => intval($XML_USER_ID));
        try{
            $rez = $clientRez->ПолучитьСписокАбитуриентов($paramsRez);
            $arFieldsAb1C = json_decode(json_encode($rez->return->Состав->Абитуриент), TRUE);
            isEmptyArr1C($arFieldsAb1C, $STEP);
            /*
            if(intval($arFieldsAb1C["IDMain"]) == 0){
                sleep(5);
                //header('Location: /enrollee/step_'.$STEP.'.php?msg=wrngs', true, 301);
                LocalRedirect('/enrollee/step_'.$STEP.'.php?msg=wrngs');
                die();
            }
            */
            $arFields1C = json_decode(json_encode($rez->return->Состав->Заявления->Заявление), TRUE);
            if($STEP){
                saveToLog2("Заявления\n".print_r($arFields1C,1), intval($XML_USER_ID), $STEP);
            }
            if(is_array($arFields1C[0])){
                $arBuf = $arFields1C;
            }elseif(is_array($arFields1C)){
                $arBuf[0] = $arFields1C;
            }else{
                $arBuf = array();
            }
            $arFields1C = $arBuf;
        }catch (Exception $e){
            ShowError('Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n");
            if($STEP){
                saveToLog2('Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n", intval($XML_USER_ID), $STEP);
            }
        }
    }
    foreach($arFields1C as $arItem){
        $arResult[] = array(
            "ID"        =>	"",
            "GUIDKG" => $arItem["GUIDКГ"],
            "Level" => ""/*$arItem["УровеньОбразования"]*/,
            "Specialty" => $arItem["GUIDНП"],
            "Request" => "",
            "RequestPath" => $arItem["ПутьКФайлу"],
            "Priority" => $arItem["СогласиеНаЗачисление"],
            "DateChange" => $arItem["ДатаИзменения"],
            "Status" => $arItem["RequestStatus"],
            "GUIDProfile" => $arItem["GUIDПрофиль"],
            //"Status" => ($arItem["RequestStatus"] == "На доработке") ? '' : $arItem["RequestStatus"]
        );
    }
    return $arResult;
}

//Сведения о ЕГЭ
function getAbitEgeFldsToMaindataTEST($XML_USER_ID = 0, $STEP = 0){
    $arFields1C = array();
    $arResult = array();
    $arBuf = array();

    isEmptyArr1C($arFields1C, $STEP);

    /*
    $clientRez = getSoapClient(WSABITURS_URL);
    if($clientRez){
        $paramsRez = array("ID" => intval($XML_USER_ID));
        try{
            $rez = $clientRez->ПолучитьСписокАбитуриентов($paramsRez);
            $arFieldsAb1C = json_decode(json_encode($rez->return->Состав->Абитуриент), TRUE);
            isEmptyArr1C($arFieldsAb1C, $STEP);

            $arFields1C = json_decode(json_encode($rez->return->Состав->РезультатыЕГЭ), TRUE);
            if($STEP){
                saveToLog2("РезультатыЕГЭ\n".print_r($arFields1C,1), intval($XML_USER_ID), $STEP);
            }
            if(is_array($arFields1C[0])){
                $arBuf = $arFields1C;
            }elseif(is_array($arFields1C)){
                $arBuf[0] = $arFields1C;
            }else{
                $arBuf = array();
            }
            $arFields1C = $arBuf;
        }catch (Exception $e){
            ShowError('Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n");
            if($STEP){
                saveToLog2('Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n", intval($XML_USER_ID), $STEP);
            }
        }
    }

    */
    foreach($arFields1C as $arItem){
        $arResult[] = array(
            "ID"        =>	"",
            "Subject"   =>  $arItem["Дисциплина"],
            "Mark"      =>  $arItem["Отметка"]
        );
    }

    return $arResult;
}



//Сведения о ЕГЭ
function getAbitEgeFldsToMaindata($XML_USER_ID = 0, $STEP = 0){
    $arFields1C = array();
    $arResult = array();
    $arBuf = array();
    $clientRez = getSoapClient(WSABITURS_URL);
    if($clientRez){
        $paramsRez = array("ID" => intval($XML_USER_ID));
        try{
            $rez = $clientRez->ПолучитьСписокАбитуриентов($paramsRez);
            $arFieldsAb1C = json_decode(json_encode($rez->return->Состав->Абитуриент), TRUE);
            isEmptyArr1C($arFieldsAb1C, $STEP);
            /*
            if(intval($arFieldsAb1C["IDMain"]) == 0){
                sleep(5);
                //header('Location: /enrollee/step_'.$STEP.'.php?msg=wrngs', true, 301);
                LocalRedirect('/enrollee/step_'.$STEP.'.php?msg=wrngs');
                die();
            }
            */
            $arFields1C = json_decode(json_encode($rez->return->Состав->РезультатыЕГЭ), TRUE);
            if($STEP){
                saveToLog2("РезультатыЕГЭ\n".print_r($arFields1C,1), intval($XML_USER_ID), $STEP);
            }
            if(is_array($arFields1C[0])){
                $arBuf = $arFields1C;
            }elseif(is_array($arFields1C)){
                $arBuf[0] = $arFields1C;
            }else{
                $arBuf = array();
            }
            $arFields1C = $arBuf;
        }catch (Exception $e){
            ShowError('Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n");
            if($STEP){
                saveToLog2('Выброшено исключение. Метод "ПолучитьСписокАбитуриентов". '.$e->getMessage()."\n", intval($XML_USER_ID), $STEP);
            }
        }
    }
    foreach($arFields1C as $arItem){
        if($arItem["Дисциплина"] == "Русский язык"){
            $arItem["Дисциплина"] = "Русский";
        }
        $arResult[] = array(
            "ID"        =>	"",
            "Subject"   =>  $arItem["Дисциплина"],
            "Mark"      =>  $arItem["Отметка"]
        );
    }
    return $arResult;
}

function checkPostField($Fld_1C, $Fld_POST, $POST = array(), &$arResult = array()){
    if(!empty($POST) && !empty($arResult)){
        if(isset($POST[$Fld_POST])&&!empty($POST[$Fld_POST])){
            $arResult[$Fld_1C] = $POST[$Fld_POST];
        }
    }
}

function dismount($object) {
    $reflectionClass = new ReflectionClass(get_class($object));
    $array = array();
    foreach ($reflectionClass->getProperties() as $property) {
        $property->setAccessible(true);
        $array[$property->getName()] = $property->getValue($object);
        $property->setAccessible(false);
    }
    return $array;
}

function object_to_array($data)
{
    if (is_array($data) || is_object($data))
    {
        $result = array();
        foreach ($data as $key => $value)
        {
            $result[$key] = object_to_array($value);
        }
        return $result;
    }
    return $data;
}

function saveToLog($str,$uid=0,$step=0){
    $file = $_SERVER["DOCUMENT_ROOT"]."/enrollee/log/".$uid."_".$step.".txt";
    file_put_contents($file, print_r($str,1)."\n".date("Y-m-d\TH:i:s\Z", time())."\n========\n", FILE_APPEND | LOCK_EX);
}

function saveToLog2($str,$uid=0,$step=0){
    $file = $_SERVER["DOCUMENT_ROOT"]."/enrollee/log2/".$uid."_".$step.".txt";
    file_put_contents($file, print_r($str,1)."\n".date("Y-m-d\TH:i:s\Z", time())."\n========\n\n", FILE_APPEND | LOCK_EX);
}

function saveToLog3($str,$uid=0,$step=''){
    $file = $_SERVER["DOCUMENT_ROOT"]."/enrollee/logreg/".$uid."_".$step.".txt";
    file_put_contents($file, print_r($str,1)."\n".date("Y-m-d\TH:i:s\Z", time())."\n========\n\n", FILE_APPEND | LOCK_EX);
}
function getNormalArr($arr=array()){
    $arRez = array();
    if(is_array($arr[0])){
        $arRez = $arr;
    }elseif(is_array($arr)){
        $arRez[0] = $arr;
    }else{
        $arRez = array();
    }
    return $arRez;
}
?>