<?php
///**
// * Created by PhpStorm.
// * User: Bondarenko Georg
// * Date: 31.07.2015
// * Time: 14:57
// */
//
//use Bitrix\Main\EventManager;
//use Bitrix\Main\Loader;
//
///**
// * Page load
// */
//AddEventHandler("main", "OnBeforeProlog",  Array("BITLoad", "InitLanguageSiteParams"));
//AddEventHandler("main", "OnBeforeProlog",  Array("BITLoad", "InitLanguageSiteFilter"));
////AddEventHandler("main", "OnBeforeProlog",  Array("BITLoad", "SetAdminDateTimeFix"));
///** Filters news and events */
//AddEventHandler("main", "OnBeforeProlog",  Array("BITLoad", "InitFilterByPreviewNewsAndEventsInLeftBar"));
//AddEventHandler("main", "OnBeforeProlog",  Array("BITLoad", "InitFilterEventsAllList"));
///** Breadcrumbs */
//AddEventHandler("main", "OnBeforeProlog",  Array("BITLoad", "InitBreadcrumbsNameExclusion"));
//AddEventHandler("main", "OnBeforeProlog",  Array("BITLoad", "InitBreadcrumbsChange"));
///** Crons */
//AddEventHandler("main", "OnBeforeProlog",  Array("BITCron", "MoveOldEventsToArchive"));
//
//AddEventHandler("iblock", "OnBeforeIBlockElementUpdate", array("BitNews", "onBeforeUpdateHandler"));
//AddEventHandler("iblock", "OnBeforeIBlockElementAdd", array("BitNews", "onBeforeUpdateHandler"));
//
//
//Loader::includeModule("bit_hl");
//$eventManager = EventManager::getInstance();
//$eventManager->addEventHandler('', 'BITTabsListOnAfterUpdate', '\BIT\ORM\BITTabsList::OnAfterUpdateHandler');
//$eventManager->addEventHandler('bit.orm', 'BITTabsListOnAfterUpdate', '\BIT\ORM\BITTabsList::OnAfterUpdateHandler');
//$eventManager->addEventHandler('bit_hl', 'onAfterDetailTextUpdate', '\Firstbit\Soap\Structure::onAfterDetailTextUpdateHandler');