<?php

/**
 * (c) Bit24, http://studiobit.ru
 * User: Bondarenko Georg
 * Date: 17.06.2016 10:05
 */
class BitIp
{

	const IPV4_MASK = '[0-9]{1,3}(\.[0-9]{1,3}){3}';
	const IPV6_MASK = '[0-9a-fA-F]{1,4}(\:[0-9a-fA-F]{1,4}){7}';

	const AR_PRIVATE_IPS = array(
		"10.0.0.0/8",
		"172.16.0.0/12",
		"192.168.0.0/16",
	);

	private $ip = "";

	function __construct($string = "")
	{
		if(strlen($string) <= 0 )
		{
			$string = $_SERVER['REMOTE_ADDR'];
		}
		if(is_string($string) && static::validate($string))
		{
			$this->ip = $string;
		}
	}

	public static function validate($strIp)
	{
		return preg_match("/^(".static::IPV4_MASK."|".static::IPV6_MASK.")\$/", trim($strIp));
	}

	public function isPrivate()
	{
		return (bool)static::matchCidr($this->ip, static::AR_PRIVATE_IPS);
	}

	public static function matchCidr($addr, $cidr) {
		$output = false;
		if ( is_array($cidr) ) {

			foreach ( $cidr as $cidrlet ) {
				if ( static::matchCidr($addr, $cidrlet) ) {
					$output = true;
					break;
				}
			}
		} else {
			list($ip, $mask) = explode('/', $cidr);
			$mask = 0x00000000ffffffff << (32 - $mask);
			$output = ((ip2long($addr) & $mask) == (ip2long($ip) & $mask));
		}
		return $output;
	}


}

