<?php
/**
 * (c) Bit24, http://studiobit.ru
 * User: Bondarenko Georg
 * Date: 05.08.2016 14:28
 */

namespace Firstbit\Soap;

/**
 * Класс для работы с мероприятиями при обмене по SOAP
 * Class Event
 * @package Firstbit\Soap
 */
class Event
{

	/**
	 * ИД инфоблока
	 */
	const IBLOCK_ID = 6;

	const DEFAULT_SORT = 500;

	const DEFAULT_PREVIEW_TEXT_TYPE = "html";

	/**
	 * Внешний ключ дефолтного значения языка
	 */
	const DEFAULT_LANG_XML_ID = "ae2ogrFC";

	/**
	 * @var int ИД
	 */
	protected $id = 0;
	/**
	 * @var string
	 */
	protected $xmlId = "";
	/**
	 * @var bool Флаг активности
	 */
	protected $active = true;
	/**
	 * @var string Название
	 */
	protected $name = "";
	/**
	 * @var string Дата начала активности
	 */
	protected $dateActiveFrom = "";
	/**
	 * @var string Дата завершения активности
	 */
	protected $dateActiveTo = "";
	/**
	 * @var int Индекс сортировки
	 */
	protected $sort = 500;
	/**
	 * @var string Изображение анонса в формате base64-строки
	 */
	protected $previewPicture = "";
	/**
	 * @var string Расширение изображения
	 */
	protected $previewPictureFormat = "";
	/**
	 * @var int ИД изображения
	 */
	protected $previewPictureArray = array();
	/**
	 * @var string Текст анонса
	 */
	protected $previewText = "";
	/**
	 * @var string Детальный текст
	 */
	protected $detailText = "";
	/**
	 * @var array Массив ИД тематик
	 */
	protected $subjects = array();
	/**
	 * @var array Массив ИД целевых групп
	 */
	protected $targetGroup = array();
	/**
	 * @var bool Флаг "Важные мероприятия"
	 */
	protected $isImportant = false;
	/**
	 * @var bool Флаг "Архивное"
	 */
	protected $isArchive = false;
	/**
	 * @var array Внешние ключи структур, где необходимо отобразить мероприятия
	 */
	protected $department = array();
	/**
	 * @var string Дата начала проведения
	 */
	protected $dateStart = "";
	/**
	 * @var string Дата завершения проведения
	 */
	protected $dateEnd = "";
	protected $code = "";

	/**
	 * Существует ли элемент на сайте
	 * @var bool
	 */
	protected $existInSite = false;

	/**
	 * Event constructor.
	 *
	 * @param $arFields
	 */
	public function __construct($arFields)
	{
		$this->checkFields($arFields);
		$this->setXmlId($arFields["XML_ID"]);
		$this->setActive($arFields["ACTIVE"]);
		$this->setName($arFields["NAME"]);
		$this->setDateActiveFrom($arFields["DATE_ACTIVE_FROM"]);
		$this->setDateActiveTo($arFields["DATE_ACTIVE_TO"]);
		$this->setSort($arFields["SORT"]);
		$this->setPreviewPicture($arFields["PREVIEW_PICTURE"]);
		$this->setPreviewPictureFormat($arFields["PREVIEW_PICTURE_FORMAT"]);
		$this->setPreviewText($arFields["PREVIEW_TEXT"]);
		$this->setDetailText($arFields["DETAIL_TEXT"]);
		$this->setSubjects($arFields["SUBJECTS"]);
		$this->setTargetGroup($arFields["TARGET_GROUP"]);
		$this->setIsImportant($arFields["IS_IMPORTANT"]);
		$this->setIsArchive($arFields["IS_ARCHIVE"]);
		$this->setDepartment($arFields["DEPARTMENT"]);
		$this->setDateStart($arFields["DATE_START"]);
		$this->setDateEnd($arFields["DATE_END"]);
		$this->setCode($arFields["CODE"]);
	}

	/**
	 * @param $arFields
	 *
	 * @throws SoapException
	 */
	protected function checkFields($arFields)
	{
		if(!empty($arFields))
		{
			if(!$arFields["XML_ID"] || strlen($arFields["XML_ID"]) <= 0)
			{
				throw new SoapException("NO_XML_ID");
			}
			if(!$arFields["NAME"] || strlen($arFields["NAME"]) <= 0)
			{
				throw new SoapException("NO_NAME");
			}
			if(!$arFields["PREVIEW_PICTURE"] || strlen($arFields["PREVIEW_PICTURE"]) <= 0)
			{
				throw new SoapException("NO_PREVIEW_PICTURE");
			}
			if(!$arFields["PREVIEW_PICTURE_FORMAT"] || strlen($arFields["PREVIEW_PICTURE_FORMAT"]) <= 0)
			{
				throw new SoapException("NO_PREVIEW_PICTURE_FORMAT");
			}
		}
	}

	/**
	 * Вернет поля по внешнему ключу
	 * @param $xmlId
	 *
	 * @return array|bool
	 */
	public static function getByXmlId($xmlId)
	{
		$return = false;
		if(strlen($xmlId) > 0)
		{
			$arFields = \CIBlockElement::GetList(
				array(),
				array(
					"XML_ID"    =>  $xmlId
				)
			)->fetch();
			if($arFields)
			{
				$return = $arFields;
			}
		}
		return $return;
	}

	/**
	 * Преобразует дату из формата soap в объект DateTime
	 * @param $soapDateTime
	 *
	 * @return \DateTime
	 */
	protected static function prepareDate($soapDateTime)
	{
		if($soapDateTime)
		{
			$soapDateTime = new \DateTime($soapDateTime);
		}
		else
		{
			$soapDateTime = new \DateTime();
		}
		return $soapDateTime;
	}

	/**
	 * Добавляет или обновляет элемент и вкладку детального описания
	 * @throws SoapException
	 */
	public function addOrUpdate()
	{
		$this->createPreviewPicture();
		$arFields = $this->prepareArFields();
		if($this->existInSite)
		{
			$this->update($arFields);
		}
		else
		{
			$this->add($arFields);
		}
		if($this->id)
		{
			Tab::addOrUpdateDetail($this->id, $this->detailText);
		}
	}

	/**
	 * Добавляет элемент мероприятия
	 * @param $arFields
	 *
	 * @return bool|int
	 * @throws SoapException
	 */
	public function add($arFields)
	{
		$return = false;
		if($arFields)
		{
			$objIblockEl = new \CIBlockElement;
			$this->id = $objIblockEl->Add($arFields);
			if(!$this->id)
			{
				throw new SoapException($objIblockEl->LAST_ERROR);
			}
			$return =  $this->id;
		}
		return $return;
	}

	/**
	 * Обновляет элемент мероприятия
	 * @param $arFields
	 *
	 * @return bool|int
	 * @throws SoapException
	 */
	public function update($arFields)
	{
		$return = false;
		if($arFields)
		{
			$objIblockEl = new \CIBlockElement;
			if(!$objIblockEl->Update($this->id, $arFields))
			{
				throw new SoapException($objIblockEl->LAST_ERROR);
			}
			$return =  $this->id;
		}
		return $return;
	}

	/**
	 * Удаляет мероприятие по внешнему ключу и удаляет все его вкладки
	 * @param $xmlId
	 * @param bool $deactivate - флаг деактивации вместо удаления
	 *
	 * @return bool
	 * @throws SoapException
	 */
	public static function deleteByXmlId($xmlId, $deactivate = true)
	{
		$return = false;
		$arFields = static::getByXmlId($xmlId);
		if($arFields && intval($arFields["ID"]) > 0)
		{
			if($deactivate)
			{
				$return = static::deactivate(intval($arFields["ID"]));
			}
			else
			{
				$return = static::delete(intval($arFields["ID"]));
				if($return)
				{
					Tab::deleteByElement(intval($arFields["ID"]));
				}
			}
		}
		return $return;
	}

	/**
	 * Удаляет мероприятие
	 * @param $id
	 *
	 * @return bool
	 * @throws SoapException
	 */
	public static function delete($id)
	{
		$return = false;
		$id = (int)$id;
		if($id > 0)
		{
			$res = \CIBlockElement::Delete($id);
			if(!$res)
			{
				throw new SoapException("DELETE_ERROR");
			}
			$return = true;
		}
		return $return;
	}

	/**
	 * Деактивация мероприятия
	 * @param $id
	 *
	 * @return bool
	 * @throws SoapException
	 */
	public static function deactivate($id)
	{
		$return = false;
		$id = (int)$id;
		if($id > 0)
		{
			$objIblockEl = new \CIBlockElement;
			if(!$objIblockEl->Update($id, array(
				"ACTIVE"    =>  "N"
			)))
			{
				throw new SoapException($objIblockEl->LAST_ERROR);
			}
			$return = true;
		}
		return $return;
	}

	/**
	 * Подготавливает поля мероприятия
	 * @return array
	 */
	protected function prepareArFields()
	{
		$fields = array(
			"IBLOCK_ID"         =>  static::IBLOCK_ID,
			"XML_ID"            =>  $this->xmlId,
			"ACTIVE"            =>  $this->active,
			"NAME"              =>  $this->name,
			"CODE"              =>  (strlen($this->code) > 0) ? $this->code : \Cutil::translit($this->name,"ru",array("replace_space"=>"-","replace_other"=>"-")),
			"DATE_ACTIVE_FROM"  =>  $this->dateActiveFrom->format("d.m.Y"),
			"DATE_ACTIVE_TO"    =>  $this->dateActiveTo->format("d.m.Y"),
			"SORT"              =>  $this->sort,
			"PREVIEW_PICTURE"   =>  $this->previewPictureArray,
			"PREVIEW_TEXT"      =>  $this->previewText,
			"PREVIEW_TEXT_TYPE" =>  static::DEFAULT_PREVIEW_TEXT_TYPE,
			"PROPERTY_VALUES"   =>  array(
				"UF_SUBJECTS"      =>  $this->subjects,
				"UF_TARGET_GROUP"  =>  $this->targetGroup,
				"UF_DEPARTMENT"    =>  $this->department,
				"UF_LANGUAGE"      =>  static::DEFAULT_LANG_XML_ID,
			)
		);
		if ($this->isImportant)
		{
			$fields["PROPERTY_VALUES"]["UF_IS_IMPORTANT"] = $this->isImportant;
		}
		if ($this->isArchive)
		{
			$fields["PROPERTY_VALUES"]["UF_IS_ARCHIVE"] = $this->isArchive;
		}
		if($this->dateStart)
		{
			$fields["PROPERTY_VALUES"]["UF_START_EVENT"] = $this->dateStart->format("d.m.Y");
		}
		if($this->dateEnd)
		{
			$fields["PROPERTY_VALUES"]["UF_END_EVENT"] = $this->dateEnd->format("d.m.Y");
		}

		return $fields;
	}

	/**
	 * Сохраняет изображение анонса
	 */
	protected function createPreviewPicture()
	{
		if(strlen($this->previewPicture) && strlen($this->previewPictureFormat))
		{
			$this->previewPictureArray = File::makeFileArray($this->previewPicture, $this->xmlId, $this->previewPictureFormat);
		}
	}


	/**
	 * @param string $xmlId
	 */
	public function setXmlId($xmlId)
	{
		$this->xmlId = $xmlId;
		$arFields = static::getByXmlId($this->xmlId);
		if($arFields)
		{
			$this->id = $arFields["ID"];
			$this->existInSite = true;
		}
	}

	/**
	 * @param boolean $active
	 */
	public function setActive($active)
	{
		if($active)
		{
			$active = "Y";
		}
		else
		{
			$active = "N";
		}
		$this->active = $active;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @param string $dateActiveFrom
	 */
	public function setDateActiveFrom($dateActiveFrom)
	{
		$this->dateActiveFrom = static::prepareDate($dateActiveFrom);
	}

	/**
	 * @param string $dateActiveTo
	 */
	public function setDateActiveTo($dateActiveTo)
	{
		$this->dateActiveTo = static::prepareDate($dateActiveTo);
	}

	/**
	 * @param int $sort
	 */
	public function setSort($sort)
	{
		$sort = (int)$sort;
		if($sort <= 0)
		{
			$sort = static::DEFAULT_SORT;
		}
		$this->sort = $sort;
	}

	/**
	 * @param string $previewPicture
	 */
	public function setPreviewPicture($previewPicture)
	{
		$this->previewPicture = $previewPicture;
	}

	/**
	 * @param string $previewPictureFormat
	 */
	public function setPreviewPictureFormat($previewPictureFormat)
	{
		$this->previewPictureFormat = $previewPictureFormat;
	}

	/**
	 * @param string $previewText
	 */
	public function setPreviewText($previewText)
	{
		$this->previewText = htmlspecialchars_decode(htmlspecialcharsEx($previewText));
	}

	/**
	 * @param string $detailText
	 */
	public function setDetailText($detailText)
	{
		$this->detailText = htmlspecialchars_decode(htmlspecialcharsEx($detailText));
	}

	/**
	 * @param array $subjects
	 */
	public function setSubjects($subjects)
	{
		$this->subjects = $subjects;
	}

	/**
	 * @param array $targetGroup
	 * @throws \Exception
	 */
	public function setTargetGroup($targetGroup)
	{
		if ($targetGroup)
		{
			if (!\Bitrix\Main\Loader::includeModule("bit_hl"))
			{
				throw new \Exception("MODULE 'bit_hl' IS NOT INSTALLED");
			}

			$targetGroupXmlIds = array();
			$res = \BIT\ORM\BitTargetGroups::getList(array(
				"filter" => array("=ID" => $targetGroup),
				"select" => array("UF_XML_ID")
			));
			while ($item = $res->fetch())
			{
				$targetGroupXmlIds[] = $item["UF_XML_ID"];
			}

			if ($targetGroupXmlIds)
			{
				$targetGroup = $targetGroupXmlIds;
			}
		}

		$this->targetGroup = $targetGroup;
	}

	/**
	 * @param boolean $isImportant
	 */
	public function setIsImportant($isImportant)
	{
		$enumId = $isImportant;
		if ($isImportant && \Bitrix\Main\Loader::includeModule("iblock"))
		{
			$res = \CIBlockPropertyEnum::GetList(
				array(),
				array("IBLOCK_ID" => self::IBLOCK_ID, "CODE" => "UF_IS_IMPORTANT", "XML_ID" => "Y")
			);
			if ($item = $res->fetch())
			{
				$enumId = $item["ID"];
			}
		}

		$this->isImportant = $enumId;
	}

	/**
	 * @param boolean $isArchive
	 */
	public function setIsArchive($isArchive)
	{
		$enumId = $isArchive;
		if ($isArchive && \Bitrix\Main\Loader::includeModule("iblock"))
		{
			$res = \CIBlockPropertyEnum::GetList(
				array(),
				array("IBLOCK_ID" => self::IBLOCK_ID, "CODE" => "UF_IS_ARCHIVE", "XML_ID" => "Y")
			);
			if ($item = $res->fetch())
			{
				$enumId = $item["ID"];
			}
		}

		$this->isArchive = $enumId;
	}

	/**
	 * @param array $department
	 */
	public function setDepartment($department)
	{
		$arDepartment = array();
		foreach($department as $depXmlId)
		{
			if(strlen($depXmlId) > 0)
			{
				$arDep = Structure::getByXmlId($depXmlId);
				if(intval($arDep["ID"]) > 0)
				{
					$arDepartment[] = $arDep["ID"];
				}
			}

		}
		$this->department = $arDepartment;
	}

	/**
	 * @param string $dateStart
	 */
	public function setDateStart($dateStart)
	{
		if(strlen($dateStart) > 0)
		{
			$this->dateStart = static::prepareDate($dateStart);
		}
		else
		{
			$this->dateStart = false;
		}
	}

	/**
	 * @param string $dateEnd
	 */
	public function setDateEnd($dateEnd)
	{
		if(strlen($dateEnd) > 0)
		{
			$this->dateEnd = static::prepareDate($dateEnd);
		}
		else
		{
			$this->dateEnd = false;
		}
	}

	/**
	 * @param string $code
	 */
	public function setCode($code)
	{
		$this->code = $code;
	}


}