<?php
/**
 * (c) Bit24, http://studiobit.ru
 * User: Bondarenko Georg
 * Date: 04.08.2016 15:41
 */

namespace Firstbit\Soap;
use \Bitrix\Main\Security\Random;
use Bitrix\Main\Diag\Debug;

/**
 * Class TeachingStaff
 * @package Firstbit\Soap
 */
class TeachingStaff
{

	/**
	 * Язык по-умолчанию
	 */
	const DEFAULT_LANG = 1;
	/**
	 * ИД группы "Профессорско-преподавательский состав"
	 */
	const EMPLOYEE_GROUP_ID = 5;

	/**
	 * @var int ИД
	 */
	protected $id = 0;
	/**
	 * @var string Внешний ключ
	 */
	protected $xmlId = "";
	/**
	 * @var string Флаг активности (Y|N)
	 */
	protected $active = "";
	/**
	 * @var string Имя
	 */
	protected $name = "";
	/**
	 * @var string Фамилия
	 */
	protected $lastName = "";
	/**
	 * @var string Отчество
	 */
	protected $secondName = "";
	/**
	 * @var string E-mail адрес
	 */
	protected $email = "";
	/**
	 * @var string Имя входа (логин)
	 */
	protected $login = "";
	/**
	 * @var string Фотография
	 */
	protected $personalPhoto = "";
	/**
	 * @var string Расширение файла фотографии
	 */
	protected $personalPhotoFormat = "";
	/**
	 * @var array Массив файла для сохранения
	 */
	protected $personalPhotoArray = array();
	/**
	 * @var string Дополнительные заметки
	 */
	protected $personalNotes = "";
	/**
	 * @var array Профессия
	 */
	protected $personalProfession = array();
	/**
	 * @var array Преподаваемые дисциплины
	 */
	protected $disciplines = array();
	/**
	 * @var array Образование
	 */
	protected $education = array();
	/**
	 * @var array Ученая степень
	 */
	protected $academicDegree = array();
	/**
	 * @var array Ученое звание
	 */
	protected $academicTitle = array();
	/**
	 * @var array Специальность
	 */
	protected $speciality = array();
	/**
	 * @var array Квалификация
	 */
	protected $exerities = array();
	/**
	 * @var array Повышения квалификации и (или) профессиональная переподготовка
	 */
	protected $retraining = array();
	/**
	 * @var array Награды, звания
	 */
	protected $awards = array();
	/**
	 * @var string Общий стаж
	 */
	protected $totalExperience = "";
	/**
	 * @var string Стаж по специальности
	 */
	protected $cpec = "";
	/**
	 * @var array Департамент
	 */
	protected $department = array();

	/**
	 * @var array Департамент (служеб.)
	 */
	protected $departmentUtil = array();

	/**
	 * @var bool Флаг присутствия на сайте
	 */
	protected $existInSite = false;

	/**
	 * TeachingStaff constructor.
	 *
	 * @param $arFields
	 */
	public function __construct($arFields)
	{
		$this->checkFields($arFields);
		$this->setXmlId($arFields["XML_ID"]);
		$this->setActive($arFields["ACTIVE"]);
		$this->setName($arFields["NAME"]);
		$this->setLastName($arFields["LAST_NAME"]);
		$this->setsecondName($arFields["SECOND_NAME"]);
		$this->setEmail($arFields["EMAIL"]);
		$this->setLogin($arFields["LOGIN"]);
		$this->setPersonalPhoto($arFields["PERSONAL_PHOTO"]);
		$this->setPersonalPhotoFormat($arFields["PERSONAL_PHOTO_FORMAT"]);
		$this->setPersonalNotes($arFields["PERSONAL_NOTES"]);
		$this->setPersonalProfession($arFields["PERSONAL_PROFESSION"]);
		$this->setDisciplines($arFields["DISCIPLINES"]);
		$this->setEducation($arFields["EDUCATION"]);
		$this->setAcademicDegree($arFields["ACADEMIC_DEGREE"]);
		$this->setAcademicTitle($arFields["ACADEMIC_TITLE"]);
		$this->setSpeciality($arFields["SPECIALITY"]);
		$this->setExerities($arFields["EXERITIES"]);
		$this->setRetraining($arFields["RETRAINING"]);
		$this->setAwards($arFields["AWARDS"]);
		$this->setTotalExperience($arFields["TOTAL_EXPERIENCE"]);
		$this->setCpec($arFields["CPEC"]);
		$this->setDepartment($arFields["DEPARTMENT"]);
	}

	/**
	 * Проверка полей
	 * @param $arFields
	 *
	 * @throws SoapException
	 */
	protected function checkFields($arFields)
	{
		if(!empty($arFields))
		{
			if(!$arFields["XML_ID"] || strlen($arFields["XML_ID"]) <= 0)
			{
				throw new SoapException("NO_XML_ID");
			}
			if(!$arFields["NAME"] || strlen($arFields["NAME"]) <= 0)
			{
				throw new SoapException("NO_NAME");
			}
			if(!$arFields["LAST_NAME"] || strlen($arFields["LAST_NAME"]) <= 0)
			{
				throw new SoapException("NO_LAST_NAME");
			}
			if(!$arFields["SECOND_NAME"] || strlen($arFields["SECOND_NAME"]) <= 0)
			{
				throw new SoapException("NO_SECOND_NAME");
			}
			if(!$arFields["EMAIL"] || strlen($arFields["EMAIL"]) <= 0)
			{
				throw new SoapException("NO_EMAIL");
			}
			if(!check_email($arFields["EMAIL"]))
			{
				throw new SoapException("NOT_VALID_EMAIL");
			}
			if(!$arFields["LOGIN"] || strlen($arFields["LOGIN"]) <= 0)
			{
				throw new SoapException("NO_LOGIN");
			}
			if((strlen($arFields["PERSONAL_PHOTO"]) > 0) && (!$arFields["PERSONAL_PHOTO_FORMAT"] || strlen($arFields["PERSONAL_PHOTO_FORMAT"]) <= 0))
			{
				throw new SoapException("NO_PERSONAL_PHOTO_FORMAT");
			}
		}
	}

	/**
	 * Добавление или обновление сотрудника
	 * @throws SoapException
	 */
	public function addOrUpdate()
	{
		$this->createPhoto();
		$arFields = $this->prepareArFields();
		if($this->existInSite)
		{
			$this->update($arFields);
		}
		else
		{
			$this->add($arFields);
		}
	}

	/**
	 * Добавление сотрудника
	 * @param $arFields
	 *
	 * @return bool|int
	 * @throws SoapException
	 */
	public function add($arFields)
	{
		$user = new \CUser;
		$this->id = $user->Add($arFields);
		if (intval($this->id) <= 0)
		{
			throw new SoapException($user->LAST_ERROR);
		}
		else
		{
			$this->setEmployeeGroup();
			return $this->id;
		}
	}

	/**
	 * Обновление сотрудника
	 * @param $arFields
	 *
	 * @return int
	 * @throws SoapException
	 */
	public function update($arFields)
	{
		$user = new \CUser;
		$user->Update($this->id, $arFields);
		if(strlen($user->LAST_ERROR) > 0)
		{
			throw new SoapException($user->LAST_ERROR);
		}
		else
		{
			$this->setEmployeeGroup();
			return $this->id;
		}
	}

	/**
	 * Удаление по внешнему ключу
	 * @param $xmlId
	 *
	 * @return bool
	 * @throws SoapException
	 */
	public static function deleteByXmlId($xmlId)
	{
		$return = false;
		$arFields = static::getByXmlId($xmlId);
		if($arFields && intval($arFields["ID"]) > 0)
		{
			$return = static::delete(intval($arFields["ID"]));
		}
		return $return;
	}

	/**
	 * Удаление
	 * @param $id
	 *
	 * @return bool
	 * @throws SoapException
	 */
	public static function delete($id)
	{
		$return = false;
		$id = (int)$id;
		if($id > 0)
		{
			$res = \CUser::Delete($id);
			if(!$res)
			{
				throw new SoapException("DELETE_ERROR");
			}
			$return = true;
		}
		return $return;
	}

	/**
	 * Устанавливает пользователю группу "Профессорско-преподавательский состав"
	 */
	public function setEmployeeGroup()
	{
		if(intval($this->id) > 0)
		{
			$arGroups = \CUser::GetUserGroup($this->id);
			if(!in_array(static::EMPLOYEE_GROUP_ID, $arGroups))
			{
				\CUser::SetUserGroup(
					$this->id,
					array_merge($arGroups, array(static::EMPLOYEE_GROUP_ID))
				);
			}
		}
	}

	/**
	 * Вернет поля сотрудника по XML_ID
	 * @param $xmlId
	 *
	 * @return bool
	 */
	public static function getByXmlId($xmlId)
	{
		$return = false;
		if(strlen($xmlId) > 0)
		{
			$rsUser = \CUser::GetList(($by="timestamp_x"), ($order="desc"), array(
				"XML_ID"    =>  $xmlId
			));
			if($arUser = $rsUser->fetch())
			{
				$return = $arUser;
			}
		}
		return $return;
	}

	/**
	 * Создаёт массив фото из base64-строки
	 */
	protected function createPhoto()
	{
		if(strlen($this->personalPhoto))
		{
			$this->personalPhotoArray = File::makeFileArray($this->personalPhoto, $this->xmlId, $this->personalPhotoFormat);
		}
	}

	/**
	 * Подготавливает поля для сохранения
	 * @return array
	 */
	protected function prepareArFields()
	{
		return array(
			"XML_ID"                =>  $this->xmlId,
			"LOGIN"                 =>  $this->login,
			"PASSWORD"              =>  md5(Random::getString(4)).password_hash(Random::getString(4)),
			"ACTIVE"                =>  $this->active,
			"NAME"                  =>  $this->name,
			"LAST_NAME"             =>  $this->lastName,
			"SECOND_NAME"           =>  $this->secondName,
			"EMAIL"                 =>  $this->email,
			"PERSONAL_PHOTO"        =>  $this->personalPhotoArray,
			"PERSONAL_NOTES"        =>  $this->personalNotes,
			"UF_PROFESSION"         =>  $this->personalProfession,
			"UF_DISCIPLINES"        =>  $this->disciplines,
			"UF_EDUCATION"          =>  $this->education,
			"UF_ACADEMIC_DEGREE"    =>  $this->academicDegree,
			"UF_ACADEMIC_TITLE"     =>  $this->academicTitle,
			"UF_SPECIALITY"         =>  $this->speciality,
			"UF_EXERITIES"          =>  $this->exerities,
			"UF_RETRAINING"         =>  $this->retraining,
			"UF_AWARDS"             =>  $this->awards,
			"UF_TOTAL_EXPERIENCE"   =>  $this->totalExperience,
			"UF_CPEC"               =>  $this->cpec,
			"UF_DEPARTMENT"         =>  $this->department,
			"UF_DEP_UTIL"           =>  $this->departmentUtil,
			"UF_LANGUAGE"           =>  static::DEFAULT_LANG
		);
	}

	/**
	 * @param string $xmlId
	 */
	protected function setXmlId($xmlId)
	{
		$this->xmlId = $xmlId;
		$arFields = static::getByXmlId($this->xmlId);
		if($arFields)
		{
			$this->id = $arFields["ID"];
			$this->existInSite = true;
		}
	}

	/**
	 * @param string $active
	 */
	protected function setActive($active)
	{
		if($active != "N")
		{
			$active = "Y";
		}
		$this->active = $active;
	}

	/**
	 * @param string $name
	 */
	protected function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @param string $lastName
	 */
	protected function setLastName($lastName)
	{
		$this->lastName = $lastName;
	}

	/**
	 * @param string $secondName
	 */
	protected function setSecondName($secondName)
	{
		$this->secondName = $secondName;
	}

	/**
	 * @param string $email
	 */
	protected function setEmail($email)
	{
		$this->email = $email;
	}

	/**
	 * @param string $login
	 */
	protected function setLogin($login)
	{
		$this->login = $login;
	}

	/**
	 * @param string $personalPhoto
	 */
	protected function setPersonalPhoto($personalPhoto)
	{
		$this->personalPhoto = $personalPhoto;
	}

	/**
	 * @param string $personalPhotoFormat
	 */
	protected function setPersonalPhotoFormat($personalPhotoFormat)
	{
		$this->personalPhotoFormat = $personalPhotoFormat;
	}

	/**
	 * @param string $personalNotes
	 */
	public function setPersonalNotes($personalNotes)
	{
		$this->personalNotes = $personalNotes;
	}

	/**
	 * @param array $personalProfession
	 */
	public function setPersonalProfession($personalProfession)
	{
		$this->personalProfession = $personalProfession;
	}

	/**
	 * @param array $disciplines
	 */
	public function setDisciplines($disciplines)
	{
		$this->disciplines = $disciplines;
	}

	/**
	 * @param array $education
	 */
	public function setEducation($education)
	{
		$this->education = $education;
	}

	/**
	 * @param array $academicDegree
	 */
	public function setAcademicDegree($academicDegree)
	{
		$this->academicDegree = $academicDegree;
	}

	/**
	 * @param array $academicTitle
	 */
	public function setAcademicTitle($academicTitle)
	{
		$this->academicTitle = $academicTitle;
	}

	/**
	 * @param array $speciality
	 */
	public function setSpeciality($speciality)
	{
		$this->speciality = $speciality;
	}

	/**
	 * @param array $exerities
	 */
	public function setExerities($exerities)
	{
		$this->exerities = $exerities;
	}

	/**
	 * @param array $retraining
	 */
	public function setRetraining($retraining)
	{
		$this->retraining = $retraining;
	}

	/**
	 * @param array $awards
	 */
	public function setAwards($awards)
	{
		$this->awards = $awards;
	}

	/**
	 * @param string $totalExperience
	 */
	public function setTotalExperience($totalExperience)
	{
		$this->totalExperience = $totalExperience;
	}

	/**
	 * @param string $cpec
	 */
	public function setCpec($cpec)
	{
		$this->cpec = $cpec;
	}

	/**
	 * @param array $department - массив внешних ключей департаментов
	 */
	public function setDepartment($department)
	{
		$arDepartment = array();
		foreach($department as $depXmlId)
		{
			if(strlen($depXmlId) > 0)
			{
				$arDep = Structure::getByXmlId($depXmlId);
				if(intval($arDep["ID"]) > 0)
				{
					$arDepartment[] = $arDep["ID"];
				}
			}

		}
		$this->departmentUtil = $arDepartment;
		$this->department = array_unique($arDepartment);
	}



}