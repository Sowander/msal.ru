<?php
/**
 * (c) Bit24, http://studiobit.ru
 * User: Bondarenko Georg
 * Date: 10.08.2016 16:26
 */

namespace Firstbit\Soap;
use Bitrix\Main\Diag\Debug;


/**
 * Class Phonebook
 * @package Firstbit\Soap
 */
class Phonebook
{

	/**
	 * ИД инфоблока
	 */
	const IBLOCK_ID = 16;
	/**
	 * Символьный код поля типа должности
	 */
	const POSITION_TYPE_FIELD_CODE  =   "POSITION_TYPE";

	/**
	 * @var int ИД контакта
	 */
	protected $id = 0;
	/**
	 * @var string Внешний код
	 */
	protected $xmlId = "";
	/**
	 * @var string Фамилия имя отчество
	 */
	protected $fio  = "";
	/**
	 * @var string Должность
	 */
	protected $position = "";
	/**
	 * @var string Подразделение
	 */
	protected $department = "";
	/**
	 * @var string Электронная почта
	 */
	protected $email = "";
	/**
	 * @var string Телефон
	 */
	protected $phone = "";
	/**
	 * @var string Тип должности
	 */
	protected $positionType = "OTHER_EMPLOYEES";
	/**
	 * @var bool Флаг присутствия на сайте
	 */
	protected $existInSite = false;


	/**
	 * Phonebook constructor.
	 *
	 * @param $arFields
	 */
	public function __construct($arFields)
	{
		Debug::writeToFile($arFields, "", "/log.txt");
		$this->checkFields($arFields);
		$this->setXmlId($arFields["XML_ID"]);
		$this->setFio($arFields["FIO"]);
		$this->setPosition($arFields["POSITION"]);
		$this->setDepartment($arFields["DEPARTMENT"]);
		$this->setEmail($arFields["EMAIL"]);
		$this->setPhone($arFields["PHONE"]);
		$this->setPositionType($arFields["POSITION_TYPE"]);
		if($arRow = static::getByXmlId($this->xmlId))
		{
			$this->id = $arRow["ID"];
			$this->existInSite = true;
		}
	}

	/**
	 * Проверяет поля
	 * @param $arFields
	 *
	 * @throws SoapException
	 */
	protected function checkFields($arFields)
	{
		if(!empty($arFields))
		{
			if(!$arFields["XML_ID"] || strlen($arFields["XML_ID"]) <= 0)
			{
				throw new SoapException("NO_XML_ID");
			}
			if(!$arFields["FIO"] || strlen($arFields["FIO"]) <= 0)
			{
				throw new SoapException("NO_FIO");
			}
			if(!$arFields["DEPARTMENT"] || strlen($arFields["DEPARTMENT"]) <= 0)
			{
				throw new SoapException("NO_DEPARTMENT");
			}
			if(strlen($arFields["EMAIL"]) > 0 && !check_email($arFields["EMAIL"]))
			{
				throw new SoapException("NOT_VALID_EMAIL");
			}
		}
		else
		{
			throw new SoapException("NO_DATA");
		}
	}

	/**
	 * Добавляет или обновляет контакт
	 * @throws SoapException
	 */
	public function addOrUpdate()
	{
		$arFields = $this->prepareArFields();
		if($this->existInSite)
		{
			$this->update($arFields);
		}
		else
		{
			$this->add($arFields);
		}
	}

	/**
	 * Подготавливает поля контакта
	 * @return array
	 */
	protected function prepareArFields()
	{
		return array(
			"IBLOCK_ID" =>  static::IBLOCK_ID,
			"XML_ID"    =>  $this->xmlId,
			"NAME"      =>  $this->fio,
			"CODE"      =>  \Cutil::translit($this->fio,"ru",array("replace_space"=>"-","replace_other"=>"-")),
			"PROPERTY_VALUES"   =>  array(
				"DEPARTMENT"    =>  $this->department,
				"POSITION"      =>  $this->position,
				"EMAIL"         =>  $this->email,
				"PHONE"         =>  $this->phone,
				"POSITION_TYPE" =>  $this->positionType,
			)
		);
	}

	/**
	 * Добавляет контакт
	 * @param $arFields
	 *
	 * @throws SoapException
	 * @throws \Bitrix\Main\LoaderException
	 */
	public function add($arFields)
	{
		if(\Bitrix\Main\Loader::includeModule('iblock'))
		{
			$iblockElement = new \CIBlockElement();
			$res = $iblockElement->Add($arFields);
			if(!$res)
			{
				throw new SoapException($iblockElement->LAST_ERROR);
			}
		}
	}

	/**
	 * Обновляет контакт
	 * @param $arFields
	 *
	 * @throws SoapException
	 * @throws \Bitrix\Main\LoaderException
	 */
	public function update($arFields)
	{
		if(\Bitrix\Main\Loader::includeModule('iblock'))
		{
			$iblockElement = new \CIBlockElement();
			$res = $iblockElement->Update($this->id, $arFields);
			if(!$res)
			{
				throw new SoapException($iblockElement->LAST_ERROR);
			}
		}
	}

	/**
	 * Вернет контакт по внешнему ключу
	 * @param $xmlId
	 *
	 * @return bool
	 */
	public static function getByXmlId($xmlId)
	{
		$return = false;
		$rsPhonebook = \CIBlockElement::GetList(
			array("SORT"=>"ID"),
			array("IBLOCK_ID" => static::IBLOCK_ID, "XML_ID" => $xmlId),
			false,
			array(),
			false
		);
		if($arPhonebook = $rsPhonebook->GetNext())
		{
			$return = $arPhonebook;
		}
		return $return;
	}


	/**
	 * Удаляет по внешнему ключу
	 * @param $xmlId
	 *
	 * @return bool
	 * @throws SoapException
	 */
	public static function deleteByXmlId($xmlId)
	{
		$return = false;
		$arFields = static::getByXmlId($xmlId);
		if($arFields && intval($arFields["ID"]) > 0)
		{
			$return = static::delete(intval($arFields["ID"]));
		}
		return $return;
	}

	/**
	 * Удаляет контакт
	 * @param $id - идентификатор
	 *
	 * @return bool
	 * @throws SoapException
	 */
	public static function delete($id)
	{
		$return = false;
		$id = (int)$id;
		if($id > 0)
		{
			$res = \CIBlockElement::Delete($id);
			if(!$res)
			{
				throw new SoapException("DELETE_ERROR");
			}
			$return = true;
		}
		return $return;
	}

	/**
	 * Вернет список внешних ключей контактов
	 * @return array
	 */
	public static function getXmlIdList()
	{
		$return = array();
		$rsPhonebook = \CIBlockElement::GetList(
			array("SORT"=>"ID"),
			array("IBLOCK_ID" => static::IBLOCK_ID),
			false,
			array(),
			array("XML_ID", "IBLOCK_ID")
		);
		while($arPhonebook = $rsPhonebook->GetNext())
		{
			$return[] = $arPhonebook["XML_ID"];
		}
		return $return;
	}



	/**
	 * @param $xmlId
	 *
	 * @throws SoapException
	 */
	public function setXmlId($xmlId)
	{
		if(strlen($xmlId) > 0)
		{
			$this->xmlId = $xmlId;
		}
		else
		{
			throw new SoapException("NO_XML_ID");
		}
	}

	/**
	 * @param string $fio
	 */
	public function setFio($fio)
	{
		$this->fio = $fio;
	}

	/**
	 * @param string $position
	 */
	public function setPosition($position)
	{
		$this->position = $position;
	}

	/**
	 * @param string $department
	 */
	public function setDepartment($department)
	{
		$arDep = Structure::getByXmlId($department);
		if(intval($arDep["ID"]) > 0)
		{
			$this->department = $arDep["ID"];
		}
	}

	/**
	 * @param string $email
	 */
	public function setEmail($email)
	{
		$this->email = $email;
	}

	/**
	 * @param string $phone
	 */
	public function setPhone($phone)
	{
		$this->phone = $phone;
	}

	/**
	 * @param string $positionType
	 */
	public function setPositionType($positionType)
	{
		if(strlen($positionType) > 0)
		{
			$this->positionType = $positionType;
		}
		if(is_string($this->positionType) && strlen($this->positionType) > 0)
		{
			$propertyEnums = \CIBlockPropertyEnum::GetList(
				array("DEF"=>"DESC", "SORT"=>"ASC"),
				array(
					"IBLOCK_ID"=>static::IBLOCK_ID,
				    "CODE"=>static::POSITION_TYPE_FIELD_CODE,
				    "XML_ID"=>$this->positionType
				)
			);
			if($enumFields = $propertyEnums->GetNext())
			{
				$this->positionType = intval($enumFields["ID"]);
			}
		}
	}



}