<?php
/**
 * (c) Bit24, http://studiobit.ru
 * User: Bondarenko Georg
 * Date: 22.09.2016 15:26
 */

namespace Firstbit\Soap\Client;


use Bitrix\Main\Diag\Debug;

class SharePoint extends Base
{
	/**
	 * Путь до wsdl-файла
	 * @var string
	 */
	protected $wsdl = "http://213.79.127.46:4545/UnitInfoService.svc?wsdl";

	protected $login = 'bt\unitserviceclient';

	protected $password = 'P@ssword';

	/**
	 * Обновляет описание структуры (института, кафедры)
	 * @param string $xmlId
	 * @param string $html
	 *
	 * @return bool
	 * @throws \SoapFault
	 */
	public function updateStructureDescription($xmlId, $html)
	{
		$res = $this->soapClient->UpdateDescription(array(
			"id"    =>  $xmlId,
			"text"  =>  $html,
		))->UpdateDescriptionResult;
		if($res != "success")
		{
			throw new \SoapFault("ERROR_FROM_SHAREPOINT", $res);
		}
		return true;
	}

}