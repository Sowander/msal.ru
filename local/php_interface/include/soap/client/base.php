<?php
/**
 * (c) Bit24, http://studiobit.ru
 * User: Bondarenko Georg
 * Date: 22.09.2016 15:25
 */

namespace Firstbit\Soap\Client;


class Base
{
	/**
	 * Путь до wsdl-файла
	 * @var string
	 */
	protected $wsdl = "";

	protected $login = "";

	protected $password = "";

	/**
	 * Объект класса SoapClient
	 * @note По-умолчанию - false
	 * @var bool
	 */
	protected $soapClient = false;

	const DEFAULT_OPTIONS = array('soap_version'   => SOAP_1_1, 'cache_wsdl' => WSDL_CACHE_NONE);

	/**
	 * SoapClient constructor.
	 *
	 * @param string     $wsdl - путь до wsdl-файла, который переопределит поле объекта
	 * @param array|null $options
	 */
	public function __construct ($wsdl = "", $options = null)
	{
		if(!empty($options))
		{
			if(strlen($options["login"]) > 0)
			{
				$this->login = $options["login"];
			}
			if(strlen($options["password"]) > 0)
			{
				$this->password = $options["password"];
			}
		}
		if(strlen($this->login) > 0)
		{
			if(empty($options) || !isset($options))
			{
				$options = array();
			}
			$options["login"] = $this->login;
			$options["password"] = $this->password;
		}
		if(strlen($wsdl) > 0)
		{
			$this->setWsdl($wsdl);
		}
		return $this->setSoapClient($options);

	}

	/**
	 * @return string
	 */
	public function getWsdl()
	{
		return $this->wsdl;
	}

	/**
	 * @param string $wsdl
	 */
	public function setWsdl($wsdl)
	{
		$this->wsdl = $wsdl;
	}

	/**
	 * @return bool|\SoapClient - Вернет объект клвсса SoapClient или false
	 */
	public function getSoapClient()
	{
		return $this->soapClient;
	}

	/**
	 * @param null $options
	 *
	 * @return bool|\SoapClient - Вернет объект клвсса SoapClient или false
	 */
	public function setSoapClient($options = null)
	{
		if(!isset($options))
		{
			$options = array();
		}
		$options = array_merge($options, static::DEFAULT_OPTIONS);
		$this->soapClient = new \SoapClient($this->getWsdl(), $options);
		return $this->soapClient;
	}

	/**
	 * @return array
	 */
	public function getLastRequestAndResponse()
	{
		return array(
			$this->soapClient->__getLastRequest(),
			$this->soapClient->__getLastResponse(),
		);
	}
}