<?php
/**
 * (c) Bit24, http://studiobit.ru
 * User: Bondarenko Georg
 * Date: 08.08.2016 11:49
 */

namespace Firstbit\Soap;


use BIT\ORM\BITTabsList;

/**
 * Класс для работы с вкладками по SOAP
 * Class Tab
 * @package Firstbit\Soap
 */
class Tab
{

	/**
	 * Добавляет или обновляет вкладку с детальным описанием по ИД элемента
	 * @param $elementId
	 * @param $text
	 * @param $createEmpty
	 */
	public static function addOrUpdateDetail($elementId, $text, $createEmpty = false)
	{
		$arTab = BITTabsList::getDetailByElementId($elementId);
		if(strlen($text) > 0 || $createEmpty)
		{
			if($arTab && !$createEmpty)
			{
				BITTabsList::updateDetail($arTab["ID"], $elementId, $text);
			}
			else
			{
				BITTabsList::addDetail($elementId, $text);
			}
		}
		elseif($arTab)
		{
			BITTabsList::delete($arTab["ID"]);
		}
	}

	/**
	 * Удаляет вкладки по ИД элемента
	 * @param $elementId
	 *
	 * @throws \Exception
	 */
	public static function deleteByElement($elementId)
	{
		$elementId = (int)$elementId;
		if($elementId > 0)
		{
			$arTabs = BITTabsList::getByElementId($elementId);
			foreach($arTabs as $arTab)
			{
				BITTabsList::delete($arTab["ID"]);
			}
		}
	}

	/**
	 * Добавляет или обновляет вкладку с консультацией по ИД элемента
	 * @param $elementId
	 * @param $tableData
	 */
	public static function addOrUpdateTableData($elementId, $tableData)
	{
		$arTab = BITTabsList::getTableDataByElementId($elementId);
		if($arTab)
		{
			BITTabsList::updateTableData($arTab["ID"], $elementId, $tableData);
		}
		else
		{
			BITTabsList::addTableData($elementId, $tableData);
		}
	}
}