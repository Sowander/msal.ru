<?php
/**
 * (c) Bit24, http://studiobit.ru
 * User: Bondarenko Georg
 * Date: 03.08.2016 11:52
 */

namespace Firstbit\Soap;
use Bitrix\Main\Diag\Debug;

/**
 * Класс для работы с SOAP-запросом
 * Class MsalService
 * @package Firstbit\Soap
 */
class MsalService
{

	/**
	 * Поле в котором содержется авторизация
	 */
	const HEADER_AUTH_VALUE_NAME = "Authorization";

	/**
	 * Метод проводит аутентификацию и авторизацию пользователя SOAP
	 * @throws SoapException
	 */
	protected function checkAuth()
	{
		$authResult = false;
		if($arAuth = $this->getAuthData())
		{
			global $USER;
			if (!is_object($USER))
			{
				$USER = new \CUser();
			}
			$authResult = $USER->Login($arAuth["LOGIN"], $arAuth["PASSWORD"], "N");
		}
		if($authResult !== true)
		{
			header('HTTP/1.0 401 Unauthorized');
			throw new SoapException("Access denied");
		}
	}

	/**
	 * Возвращает данные для автоирзации из заготовка
	 * @return array|bool
	 */
	protected function getAuthData()
	{
		$return = false;
		$data = base64_decode(explode(" ", getallheaders()[static::HEADER_AUTH_VALUE_NAME])[1]);
		if(strlen($data) > 0)
		{
			$data = explode(":", $data);
			$return = array(
				"LOGIN"     =>  $data[0],
				"PASSWORD"  =>  $data[1]
			);
		}
		return $return;
	}

	public function Action($header)
	{
	}

	/**
	 * Добавляет или обновляет структуру (кафедру/институт)
	 * @param $params - объект параметров запроса
	 *
	 * @return array
	 */
	public function addOrUpdateStructure($params)
	{
		try
		{
			$this->checkAuth();
			$xmlId = $params->XML_ID;
			$name = $params->NAME;
			$type = $params->TYPE;
			if(strlen($xmlId) <= 0)
			{
				throw new SoapException("NO_XML");
			}
			if(strlen($name) <= 0)
			{
				throw new SoapException("NO_NAME");
			}
			if(strlen($type) <= 0)
			{
				throw new SoapException("NO_TYPE");
			}
			$objStructure = new Structure(
				$xmlId,
				$name,
				$type,
				$params->PARENT_XML_ID,
				(is_array($params->PHONES->item)) ? $params->PHONES->item : array($params->PHONES->item),
				(is_array($params->TABLE_DATA->item)) ? $params->TABLE_DATA->item : array($params->TABLE_DATA->item)
			);
			$objStructure->addOrUpdate();
			return array("status"=>"OK", "message"=>"");
		}
		catch(SoapException $e)
		{
			return array("status"=>"ERROR","message"=>$e->getMessage());
		}
		catch(\Exception $e)
		{
			return array("status"=>"ERROR","message"=>"SCRIPT_ERROR");
		}
	}

	/**
	 * Удаляет структуры
	 * @param $params - объект параметров запроса
	 *
	 * @return array
	 */
	public function removeStructure($params)
	{
		try
		{
			$this->checkAuth();
			$arXmlIds = (is_array($params->XML_ID->item)) ? $params->XML_ID->item : array($params->XML_ID->item);
			if(empty($arXmlIds) || !$arXmlIds)
			{
				throw new SoapException("NO_XML_ID");
			}
			$strErrors = "";
			foreach($arXmlIds as $xmlId)
			{
				try
				{
					Structure::deleteByXmlId($xmlId);
				}
				catch(\Exception $e)
				{
					$strErrors .= $e->getMessage()."\n";
					continue;
				}
			}
			return array(
				"status"    =>  (strlen($strErrors)>0) ? "WARNING" : "OK",
				"message"   =>  $strErrors
			);
		}
		catch(SoapException $e)
		{
			return array("status"=>"ERROR","message"=>$e->getMessage());
		}
		catch(\Exception $e)
		{
			Debug::dumpToFile($e->getMessage(), "", "/log.txt");
			return array("status"=>"ERROR","message"=>"SCRIPT_ERROR");
		}
	}

	/**
	 * Добавляет или обновляет сотрудника
	 * @param $params
	 *
	 * @return array
	 */
	public function addOrUpdateUser($params)
	{
		try
		{
			$this->checkAuth();
			$objTeachingStaff = new TeachingStaff(array(
				"XML_ID"                =>  $params->XML_ID,
				"ACTIVE"                =>  $params->ACTIVE,
				"NAME"                  =>  $params->NAME,
				"LAST_NAME"             =>  $params->LAST_NAME,
				"SECOND_NAME"           =>  $params->SECOND_NAME,
				"EMAIL"                 =>  $params->EMAIL,
				"LOGIN"                 =>  $params->LOGIN,
				"PERSONAL_PHOTO"        =>  $params->PERSONAL_PHOTO,
				"PERSONAL_PHOTO_FORMAT" =>  $params->PERSONAL_PHOTO_FORMAT,
				"PERSONAL_NOTES"        =>  $params->PERSONAL_NOTES,
				"PERSONAL_PROFESSION"   =>  (is_array($params->PERSONAL_PROFESSION->item)) ? $params->PERSONAL_PROFESSION->item : array($params->PERSONAL_PROFESSION->item),
				"DISCIPLINES"           =>  (is_array($params->DISCIPLINES->item)) ? $params->DISCIPLINES->item : array($params->DISCIPLINES->item),
				"EDUCATION"             =>  (is_array($params->EDUCATION->item)) ? $params->EDUCATION->item : array($params->EDUCATION->item),
				"ACADEMIC_DEGREE"       =>  (is_array($params->ACADEMIC_DEGREE->item)) ? $params->ACADEMIC_DEGREE->item : array($params->ACADEMIC_DEGREE->item),
				"ACADEMIC_TITLE"        =>  (is_array($params->ACADEMIC_TITLE->item)) ? $params->ACADEMIC_TITLE->item : array($params->ACADEMIC_TITLE->item),
				"SPECIALITY"            =>  (is_array($params->SPECIALITY->item)) ? $params->SPECIALITY->item : array($params->SPECIALITY->item),
				"EXERITIES"             =>  (is_array($params->EXERITIES->item)) ? $params->EXERITIES->item : array($params->EXERITIES->item),
				"RETRAINING"            =>  (is_array($params->RETRAINING->item)) ? $params->RETRAINING->item : array($params->RETRAINING->item),
				"AWARDS"                =>  (is_array($params->AWARDS->item)) ? $params->AWARDS->item : array($params->AWARDS->item),
				"TOTAL_EXPERIENCE"      =>  $params->TOTAL_EXPERIENCE,
				"CPEC"                  =>  $params->CPEC,
				"DEPARTMENT"            =>  (is_array($params->DEPARTMENT->item)) ? $params->DEPARTMENT->item : array($params->DEPARTMENT->item)
			));
			$objTeachingStaff->addOrUpdate();
			return array("status"=>"OK", "message"=>"");
		}
		catch(SoapException $e)
		{
			return array("status"=>"ERROR","message"=>$e->getMessage());
		}
		catch(\Exception $e)
		{
			Debug::dumpToFile($e->getMessage(), "", "/log.txt");
			return array("status"=>"ERROR","message"=>"SCRIPT_ERROR");
		}
	}

	/**
	 * Удаляет сотрудников
	 * @param $params
	 *
	 * @return array
	 */
	public function removeUser($params)
	{
		try
		{
			$this->checkAuth();
			$arXmlIds = (is_array($params->XML_ID->item)) ? $params->XML_ID->item : array($params->XML_ID->item);
			if(empty($arXmlIds) || !$arXmlIds)
			{
				throw new SoapException("NO_XML_ID");
			}
			$strErrors = "";
			foreach($arXmlIds as $xmlId)
			{
				try
				{
					TeachingStaff::deleteByXmlId($xmlId);
				}
				catch(\Exception $e)
				{
					$strErrors .= $e->getMessage()."\n";
					continue;
				}
			}
			return array(
				"status"    =>  (strlen($strErrors)>0) ? "WARNING" : "OK",
				"message"   =>  $strErrors
			);
		}
		catch(SoapException $e)
		{
			return array("status"=>"ERROR","message"=>$e->getMessage());
		}
		catch(\Exception $e)
		{
			Debug::dumpToFile($e->getMessage(), "", "/log.txt");
			return array("status"=>"ERROR","message"=>"SCRIPT_ERROR");
		}
	}

	/**
	 * Добавляет/обновляет мероприятие, а также добавляет/обновляет вкладку с описанием
	 * @param $params
	 *
	 * @return array
	 */
	public function addOrUpdateEvent($params)
	{
		try
		{
			$this->checkAuth();
			$objEvent = new Event(array(
				"XML_ID"                    =>  $params->XML_ID,
				"ACTIVE"                    =>  $params->ACTIVE,
				"NAME"                      =>  $params->NAME,
				"DATE_ACTIVE_FROM"          =>  $params->DATE_ACTIVE_FROM,
				"DATE_ACTIVE_TO"            =>  $params->DATE_ACTIVE_TO,
				"SORT"                      =>  $params->SORT,
				"PREVIEW_PICTURE"           =>  $params->PREVIEW_PICTURE,
				"PREVIEW_PICTURE_FORMAT"    =>  $params->PREVIEW_PICTURE_FORMAT,
				"PREVIEW_TEXT"              =>  $params->PREVIEW_TEXT,
				"DETAIL_TEXT"               =>  $params->DETAIL_TEXT,
				"SUBJECTS"                  =>  (is_array($params->SUBJECTS->item)) ? $params->SUBJECTS->item : array($params->SUBJECTS->item),
				"TARGET_GROUP"              =>  (is_array($params->TARGET_GROUP->item)) ? $params->TARGET_GROUP->item : array($params->TARGET_GROUP->item),
				"IS_IMPORTANT"              =>  $params->IS_IMPORTANT,
				"IS_ARCHIVE"                =>  $params->IS_ARCHIVE,
				"DEPARTMENT"                =>  (is_array($params->DEPARTMENT->item)) ? $params->DEPARTMENT->item : array($params->DEPARTMENT->item),
				"DATE_START"                =>  $params->DATE_START,
				"DATE_END"                  =>  $params->DATE_END
			));
			$objEvent->addOrUpdate();
			return array("status"=>"OK", "message"=>"");
		}
		catch(SoapException $e)
		{
			return array("status"=>"ERROR","message"=>$e->getMessage());
		}
		catch(\Exception $e)
		{
			Debug::dumpToFile($e->getMessage(), "", "/log.txt");
			return array("status"=>"ERROR","message"=>"SCRIPT_ERROR");
		}
	}

	/**
	 * Удаляет мероприятие и все его вкладки
	 * @param $params
	 *
	 * @return array
	 */
	public function removeEvent($params)
	{
		try
		{
			$this->checkAuth();
			$arXmlIds = (is_array($params->XML_ID->item)) ? $params->XML_ID->item : array($params->XML_ID->item);
			if(empty($arXmlIds) || !$arXmlIds)
			{
				throw new SoapException("NO_XML_ID");
			}
			$strErrors = "";
			foreach($arXmlIds as $xmlId)
			{
				try
				{
					Event::deleteByXmlId($xmlId);
				}
				catch(\Exception $e)
				{
					$strErrors .= $e->getMessage()."\n";
					continue;
				}
			}
			return array(
				"status"    =>  (strlen($strErrors)>0) ? "WARNING" : "OK",
				"message"   =>  $strErrors
			);
		}
		catch(SoapException $e)
		{
			return array("status"=>"ERROR","message"=>$e->getMessage());
		}
		catch(\Exception $e)
		{
			Debug::dumpToFile($e->getMessage(), "", "/log.txt");
			return array("status"=>"ERROR","message"=>"SCRIPT_ERROR");
		}
	}

	/**
	 * Добавляет илил обновляет контакт
	 * @param $params
	 *
	 * @return array
	 */
	public function addOrUpdatePhonebook($params)
	{
		try
		{
			$this->checkAuth();
			$objPhonebook = new Phonebook(array(
				"XML_ID"        =>  $params->XML_ID,
				"FIO"           =>  $params->FIO,
				"POSITION"      =>  $params->POSITION,
				"DEPARTMENT"    =>  $params->DEPARTMENT,
				"EMAIL"         =>  $params->EMAIL,
				"PHONE"         =>  $params->PHONE,
				"POSITION_TYPE" =>  $params->POSITION_TYPE,
			));
			$objPhonebook->addOrUpdate();
			return array("status"=>"OK", "message"=>"");
		}
		catch(SoapException $e)
		{
			return array("status"=>"ERROR","message"=>$e->getMessage());
		}
		catch(\Exception $e)
		{
			Debug::dumpToFile($e->getMessage(), "", "/log.txt");
			return array("status"=>"ERROR","message"=>"SCRIPT_ERROR");
		}
	}

	/**
	 * Удаляет контакт
	 * @param $params
	 *
	 * @return array
	 */
	public function removePhonebook($params)
	{
		try
		{
			$this->checkAuth();
			$arXmlIds = (is_array($params->XML_ID->item)) ? $params->XML_ID->item : array($params->XML_ID->item);
			if(empty($arXmlIds) || !$arXmlIds)
			{
				throw new SoapException("NO_XML_ID");
			}
			$strErrors = "";
			foreach($arXmlIds as $xmlId)
			{
				try
				{
					Phonebook::deleteByXmlId($xmlId);
				}
				catch(\Exception $e)
				{
					$strErrors .= $e->getMessage()."\n";
					continue;
				}
			}
			return array(
				"status"    =>  (strlen($strErrors)>0) ? "WARNING" : "OK",
				"message"   =>  $strErrors
			);
		}
		catch(SoapException $e)
		{
			return array("status"=>"ERROR","message"=>$e->getMessage());
		}
		catch(\Exception $e)
		{
			Debug::dumpToFile($e->getMessage(), "", "/log.txt");
			return array("status"=>"ERROR","message"=>"SCRIPT_ERROR");
		}
	}

	/**
	 * Вернет список внешних ключей имеющихся контактов на сайте
	 * @return array
	 */
	public function getPhonebookXmlIdList()
	{
		try
		{
			$this->checkAuth();
			return array(
				"status"    =>  "OK",
				"list"=>array(
					"XML_ID"    => Phonebook::getXmlIdList()
				)
			);
		}
		catch(SoapException $e)
		{
			return array("status"=>"ERROR","message"=>$e->getMessage());
		}
		catch(\Exception $e)
		{
			Debug::dumpToFile($e->getMessage(), "", "/log.txt");
			return array("status"=>"ERROR","message"=>"SCRIPT_ERROR");
		}
	}

}