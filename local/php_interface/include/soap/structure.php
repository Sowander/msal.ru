<?php
/**
 * (c) Bit24, http://studiobit.ru
 * User: Bondarenko Georg
 * Date: 03.08.2016 13:07
 */

namespace Firstbit\Soap;
use Bitrix\Main\Diag\Debug;


/**
 * Класс для работы со структурами (кафедры институты) при обмене
 * Class Structure
 * @package Firstbit\Soap
 */
class Structure
{

	/**
	 * ИД инфоблока
	 */
	const IBLOCK_ID = 3;
	/**
	 * Тип "Кафедры"
	 */
	const TYPE_DEPARTMENT = "DEPARTMENT";
	/**
	 * Тип "Инсттитуты"
	 */
	const TYPE_INSTITUTE = "INSTITUTE";
	/**
	 * XML_ID раздела "Кафедры"
	 */
	const DEPARTMENT_ROOT_XML_ID = "747daf3d-0485-4dc0-b94b-aee705a6caa2";
	/**
	 * XML_ID раздела "Инсттитуты"
	 */
	const INSTITUTE_ROOT_XML_ID = "e15334dd-b502-422e-82b9-bcb50db5586c";

	/**
	 * Дефолтное значения поля "Где отобразить мероприятия"
	 */
	const DEFAULT_UF_EVENT_SHOW_IN = 5;
	/**
	 * Дефолтное значения поля "Где отобразить новости"
	 */
	const DEFAULT_UF_NEWS_SHOW_IN = 7;
	/**
	 * Дефолтное значения поля "Где отобразить важную информацию"
	 */
	const DEFAULT_UF_IMP_INFO_SHOW_IN = 9;
	/**
	 * Дефолтное значение языка
	 */
	const DEFAULT_LANG  =   "ae2ogrFC";

	/**
	 * Поля объекта
	*/

	/**
	 * @var int ИД
	 */
	private $id = 0;
	/**
	 * @var string внешний ключ
	 */
	private $xmlId = "";
	/**
	 * @var string Название
	 */
	private $name = "";
	/**
	 * @var string Тип структуры (кафедра или институт)
	 */
	private $type = "";
	/**
	 * @var bool XML_ID родителя
	 */
	private $parentXmlId = false;
	/**
	 * @var array Список внешних ключей телефонного справочника
	 */
	private $phones = array();
	/**
	 * @var array Табличные данные расписания консультаций
	 */
	private $tableData = array();
	/**
	 * @var bool Идентификатор родителя
	 */
	private $parentId = false;

	/**
	 * @var bool Флаг существования на сайте
	 */
	private $existInSite = false;

	/**
	 * Structure constructor.
	 *
	 * @param string $xmlId
	 * @param string $name
	 * @param string $type
	 * @param bool  $parentXmlId
	 * @param array $phones
	 * @param array $tableData
	 */
	public function __construct($xmlId, $name, $type, $parentXmlId = false, $phones = array(), $tableData = array())
	{
		$this->setType($type);
		$this->setXmlId($xmlId);
		$this->setName($name);
		$this->setParentXmlId($parentXmlId);
		$this->setParent();
		$this->setPhones($phones);
		$this->setTableData($tableData);
		if($arRow = static::getByXmlId($this->getXmlId()))
		{
			$this->id = $arRow["ID"];
			$this->existInSite = true;
		}
	}

	/**
	 * Вернет структуру по внешнему ключу
	 * @param $xmlId
	 *
	 * @return bool
	 */
	public static function getByXmlId($xmlId)
	{
		$return = false;
		$rsStructures = \CIBlockSection::GetList(
			array("SORT"=>"ID"),
			array("IBLOCK_ID" => static::IBLOCK_ID, "XML_ID" => $xmlId),
			false,
			array(),
			false
		);
		if($arStructure = $rsStructures->GetNext())
		{
			$return = $arStructure;
		}
		return $return;
	}

	/**
	 * Добавляет или обновляет структуру, если такая есть на сайте
	 * @throws SoapException
	 */
	public function addOrUpdate()
	{
		$arFields = $this->prepareArFields();
		if($this->existInSite)
		{
			$this->update($arFields);
		}
		else
		{
			$this->add($arFields);
		}
		$this->createElemWithTabs();
	}

	/**
	 * Добавляет структуру
	 * @param $arFields - поля
	 *
	 * @throws SoapException
	 */
	public function add($arFields)
	{
		if(\Bitrix\Main\Loader::includeModule('iblock'))
		{
			$iblockSection = new \CIBlockSection;
			$this->id = $iblockSection->Add($arFields);
			if(!$this->id)
			{
				throw new SoapException($iblockSection->LAST_ERROR);
			}
		}
	}

	/**
	 * Обновляет структуру
	 * @param $arFields - поля
	 *
	 * @throws SoapException
	 */
	public function update($arFields)
	{
		if(\Bitrix\Main\Loader::includeModule('iblock'))
		{
			$iblockSection = new \CIBlockSection;
			$res = $iblockSection->Update($this->id, $arFields);
			if(!$res)
			{
				throw new SoapException($iblockSection->LAST_ERROR);
			}
		}
	}

	/**
	 * Удаляет по внешнему ключу
	 * @param string $xmlId
	 * @param bool $deactivate - флаг деактивации вместо удаления
	 *
	 * @return bool
	 * @throws SoapException
	 */
	public static function deleteByXmlId($xmlId, $deactivate = true)
	{
		$return = false;
		$arFields = static::getByXmlId($xmlId);
		if($arFields && intval($arFields["ID"]) > 0)
		{
			if($deactivate)
			{
				$return = static::deactivate(intval($arFields["ID"]));
			}
			else
			{
				$return = static::delete(intval($arFields["ID"]));
			}
		}
		return $return;
	}

	/**
	 * Удаляет структуру
	 * @param $id - идентификатор
	 *
	 * @return bool
	 * @throws SoapException
	 */
	public static function delete($id)
	{
		$return = false;
		$id = (int)$id;
		if($id > 0)
		{
			$res = \CIBlockSection::Delete($id);
			if(!$res)
			{
				throw new SoapException("DELETE_ERROR");
			}
			$return = true;
		}
		return $return;
	}

	/**
	 * Деактивирует структуру
	 * @param $id
	 *
	 * @return bool
	 * @throws SoapException
	 */
	public static function deactivate($id)
	{
		$return = false;
		$id = (int)$id;
		if($id > 0)
		{
			if(\Bitrix\Main\Loader::includeModule('iblock'))
			{
				$iblockSection = new \CIBlockSection;
				$res = $iblockSection->Update($id, array(
					"ACTIVE"    =>  "N"
				));
				if(!$res)
				{
					throw new SoapException($iblockSection->LAST_ERROR);
				}
			}
			$return = true;
		}
		return $return;
	}

	/**
	 * Подготавливает поля для обновления или добавления
	 * @return array
	 */
	protected function prepareArFields()
	{
		/** @todo tableData */
		return array(
			"IBLOCK_ID" =>  static::IBLOCK_ID,
			"XML_ID"    =>  $this->getXmlId(),
			"NAME"      =>  $this->getName(),
			"CODE"      =>  \Cutil::translit($this->getName(),"ru",array("replace_space"=>"-","replace_other"=>"-")),
			"IBLOCK_SECTION_ID" =>  $this->parentId,
			"UF_NAME_RU"=>  $this->getName(),
			"UF_EVENT_SHOW_IN"  =>  static::DEFAULT_UF_EVENT_SHOW_IN,
			"UF_NEWS_SHOW_IN"  =>  static::DEFAULT_UF_NEWS_SHOW_IN,
			"UF_IMP_INFO_SHOW_IN"  =>  static::DEFAULT_UF_IMP_INFO_SHOW_IN,
		);
	}

	/**
	 * Создаст элемент с вкладками внутри раздела
	 * @throws SoapException
	 */
	protected function createElemWithTabs()
	{
		$elemId = $this->getContentElemId();
		if(!$elemId || $elemId <= 0)
		{
			$elemId = $this->createElem();
		}
		Tab::addOrUpdateDetail($elemId, "", true);
		Tab::addOrUpdateTableData($elemId, $this->getTableData());
	}

	/**
	 * Вернет контентный-элемент
	 * @return bool|int
	 */
	protected function getContentElemId()
	{
		$return = false;
		$arFields = \CIBlockElement::GetList(
			array(),
			array(
				"IBLOCK_ID"         =>  static::IBLOCK_ID,
				"SECTION_ID"        =>  $this->id,
				"PROPERTY_UF_LANGUAGE"  =>  static::DEFAULT_LANG
			)
		)->fetch();
		if($arFields)
		{
			$return = (int)$arFields["ID"];
		}
		return $return;
	}

	/**
	 * Создаст контентный элемент
	 * @return bool
	 * @throws SoapException
	 */
	protected function createElem()
	{
		$objIblockEl = new \CIBlockElement;
		$return = $objIblockEl->Add(array(
			"IBLOCK_ID"             =>  static::IBLOCK_ID,
			"IBLOCK_SECTION_ID"     =>  $this->id,
			"NAME"      =>  $this->getName(),
			"CODE"      =>  \Cutil::translit($this->getName(),"ru",array("replace_space"=>"-","replace_other"=>"-")),
			"PROPERTY_VALUES"   =>  array(
				"UF_LANGUAGE"  =>  static::DEFAULT_LANG
			)
		));
		if(!$return)
		{
			throw new SoapException($objIblockEl->LAST_ERROR);
		}
		return $return;
	}

	/**
	 * Обработчик события на изменение детального описания структуры
	 * @param $event
	 */
	public static function onAfterDetailTextUpdateHandler($event)
	{
		try
		{
			$arParams = $event->getParameters();
			$xmlId = static::getSectionXmlById($arParams["IBLOCK_ELEMENT_ID"]);
			if($xmlId)
			{
				$objSharePointSoapClient = new Client\SharePoint();
				$objSharePointSoapClient->updateStructureDescription($xmlId, $arParams["HTML_TEXT"]);
			}
		}
		catch(\Exception $e)
		{
			Debug::dumpToFile(array(
				"id"        =>  ($arParams["ID"]) ? $arParams["ID"] : "",
				"xmlId"     =>  ($xmlId) ? $xmlId : "",
				"text"      =>  ($arParams["HTML_TEXT"]) ? $arParams["HTML_TEXT"] : "",
				"error"     =>  $e->getMessage(),
			), "", "log.txt");
		}
	}

	/**
	 * @param $id
	 *
	 * @return bool
	 */
	public static function getSectionXmlById($id)
	{
		$xmlId = false;
		$id = (int)\CIBlockElement::GetByID($id)->fetch()["IBLOCK_SECTION_ID"];
		if($arSection = \CIBlockSection::GetByID($id)->GetNext())
		{
			$xmlId = $arSection["XML_ID"];
		}
		return $xmlId;
	}


	/**
	 * @return mixed
	 */
	public function getXmlId()
	{
		return $this->xmlId;
	}

	/**
	 * @param $xmlId
	 *
	 * @throws SoapException
	 */
	public function setXmlId($xmlId)
	{
		if(strlen($xmlId) > 0)
		{
			$this->xmlId = $xmlId;
		}
		else
		{
			throw new SoapException("NO_XML_ID");
		}
	}

	/**
	 * @return string
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param string $name
	 */
	public function setName($name)
	{
		$this->name = $name;
	}

	/**
	 * @return string
	 */
	public function getType()
	{
		return $this->type;
	}

	/**
	 * @param $type
	 *
	 * @throws SoapException
	 */
	public function setType($type)
	{
		if($type == static::TYPE_DEPARTMENT || $type == static::TYPE_INSTITUTE)
		{
			$this->type = $type;
		}
		else
		{
			throw new SoapException("NOT_VALID_TYPE");
		}
	}

	/**
	 * @return boolean
	 */
	public function getParentXmlId()
	{
		return $this->parentXmlId;
	}

	/**
	 * @param boolean $parentXmlId
	 */
	public function setParentXmlId($parentXmlId)
	{
		if(strlen($parentXmlId) > 0)
		{
			$arStruct = static::getByXmlId($parentXmlId);
			if(!$arStruct)
			{
				$parentXmlId = false;
			}
		}
		if(!$parentXmlId || strlen($parentXmlId) <= 0)
		{
			switch($this->type)
			{
				case static::TYPE_DEPARTMENT:
					$parentXmlId = static::DEPARTMENT_ROOT_XML_ID;
					break;
				case static::TYPE_INSTITUTE:
					$parentXmlId = static::INSTITUTE_ROOT_XML_ID;
					break;
			}
		}
		$this->parentXmlId = $parentXmlId;
	}

	/**
	 * @return boolean
	 */
	public function getParentId()
	{
		return $this->parentId;
	}

	/**
	 *
	 */
	public function setParent()
	{
		$arParent = static::getByXmlId($this->parentXmlId);
		$this->parentId = $arParent["ID"];
	}

	/**
	 * @return array
	 */
	public function getPhones()
	{
		return $this->phones;
	}

	/**
	 * @param array $phones
	 */
	public function setPhones($phones)
	{
		$this->phones = $phones;
	}

	/**
	 * @return array
	 */
	public function getTableData()
	{
		return $this->tableData;
	}

	/**
	 * @param array $tableData
	 */
	public function setTableData($tableData)
	{
		$this->tableData = $tableData;
	}



}