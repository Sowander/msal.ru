<?php
/**
 * (c) Bit24, http://studiobit.ru
 * User: Bondarenko Georg
 * Date: 04.08.2016 18:17
 */

namespace Firstbit\Soap;
use \Bitrix\Main\Security\Random;


/**
 * Класс для работы с файлами при SOAP-обмене
 * Class File
 * @package Firstbit\Soap
 */
class File
{

	/**
	 * Расширение по-умолчанию
	 */
	const DEFAULT_EXTENSION = "jpg";
	const DEFAULT_DIR_RIGHTS = 0775;
	/**
	 * Директория для врменного сохранения файла
	 */
	const TMP_DIR = "/soap/tmp/";
	/**
	 * Поддиректория для сохранения файла
	 */
	const SAVE_DIR = "soap";

	/**
	 * Сохранения файла
	 * @param string $base64String - файл в формате base64-строки
	 * @param string $prefix - префикс для файла (чаще всего будет спользоваться внешний ключ элемента)
	 * @param string $fileExt - расширение файла
	 *
	 * @return bool|int - ид файла или false, если не удалось сохранить
	 */
	public static function save($base64String, $prefix = "", $fileExt = "")
	{
		$return = false;
		try
		{
			if(strlen($base64String) > 0)
			{
				$fileName = self::makeTmpFileName($prefix, $fileExt);
				$bxFile = self::makeFileArrayEx($base64String, $fileName);
				$fileId = \CFile::SaveFile($bxFile, static::SAVE_DIR);
				unlink($fileName);
				$return = ($fileId > 0) ? $fileId : false;
			}
		}
		catch(\Exception $e)
		{
			$return = false;
		}
		return $return;
	}

	/**
	 * Формирует массив файла
	 * @param string $base64String
	 * @param string $prefix
	 * @param string $fileExt
	 *
	 * @return mixed
	 */
	public static function makeFileArray($base64String, $prefix = "", $fileExt = "")
	{
		$fileName = self::makeTmpFileName($prefix, $fileExt);
		return self::makeFileArrayEx($base64String, $fileName);
	}


	/**
	 * Проверяет директорию на существование и создает, если нету
	 * @param string $fileName
	 */
	final protected function checkDirectories($fileName){
		$dirName = dirname($fileName);
		if (!is_dir($dirName))
		{
			mkdir($dirName, static::DEFAULT_DIR_RIGHTS, true);
		}
	}

	/**
	 * Записывает в файл
	 * @param string $fileName
	 * @param string $fileData
	 */
	final protected function saveFileFromBase64($fileName, $fileData){
		$fileData = base64_decode(preg_replace('#^data:image/\w+;base64,#i', '', $fileData));
		file_put_contents($fileName, $fileData);
	}

	/**
	 * @param $prefix
	 * @param $fileExt
	 *
	 * @return string
	 */
	protected static function makeTmpFileName($prefix, $fileExt)
	{
		if (strlen($fileExt) <= 0)
		{
			$fileExt = static::DEFAULT_EXTENSION;
		}
		$fileName = $_SERVER['DOCUMENT_ROOT'] . static::TMP_DIR . $prefix . Random::getString(22) . "." . $fileExt;

		return $fileName;
	}

	/**
	 * @param $base64String
	 * @param $fileName
	 *
	 * @return mixed
	 */
	final protected static function makeFileArrayEx($base64String, $fileName)
	{
		self::checkDirectories($fileName);
		self::saveFileFromBase64($fileName, $base64String);
		return \CFile::MakeFileArray($fileName);
	}


}