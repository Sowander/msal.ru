<?php

/**
 * (c) Bit24, http://studiobit.ru
 * User: Bondarenko Georg
 * Date: 21.06.2016 16:37
 */

/**
 * Базовый класс для подключения по FTP
 * Class BitFtp
 */
class BitFtp
{

	/**
	 * Логин
	 * @var string
	 */
	protected $login = "";

	/**
	 * Пароль
	 * @var string
	 */
	protected $password = "";

	/**
	 * Хост
	 * @var string
	 */
	protected $host = "";

	/**
	 * Идентификатор соединения
	 * @var bool|resource
	 */
	protected $connId = false;

	/**
	 * BitFtp constructor.
	 *
	 * @param string $host
	 * @param string $login
	 * @param string $password
	 */
	function __construct($host = "", $login = "", $password = "")
	{
		if(strlen($host) > 0)
		{
			$this->host = $host;
		}
		if(strlen($login) > 0)
		{
			$this->login = $login;
		}
		if(strlen($password) > 0)
		{
			$this->password = $password;
		}
		if(strlen($this->host) > 0 && strlen($this->login) > 0 && strlen($this->password) > 0)
		{
			$this->connId = ftp_connect($this->host);
			ftp_login($this->connId, $this->login, $this->password);
			ftp_pasv($this->connId, true);
		}
	}

	/**
	 * Завершает соединение по ftp
	 */
	public function disconnect()
	{
		ftp_close($this->connId);
	}

	/**
	 *
	 */
	function __destruct()
	{
		$this->disconnect();
	}

	/**
	 * Сохраняет файл на удаленном сервере
	 * @param string $localFilePath - локальный путь до файла
	 * @param string $remoteFilePath - удаленный путь до файла
	 *
	 * @return string|bool - путь до сохраненного файла или false
	 */
	public function filePut($localFilePath = "", $remoteFilePath = "")
	{
		if(strlen($localFilePath) > 0 && strlen($remoteFilePath) > 0)
		{
			$fileSize = 0;
			$putCount = 0;
			$localFileSize = filesize($localFilePath);
			while(intval($fileSize) != intval($localFileSize) && $putCount < 5)
			{
				ftp_put($this->connId, $remoteFilePath, $localFilePath, FTP_BINARY);
				$fileSize = ftp_size($this->connId, $remoteFilePath);
				$putCount++;
			}
			if (intval($fileSize) == intval($localFileSize))
			{
				return $remoteFilePath;
			}
			else
			{
				return false;
			}
		}
		return false;
	}

}

/**
 * Класс для работы с сервером 1С по FTP
 * Class Ftp1C
 */
class Ftp1C extends BitFtp
{
	/**
	 * Логин
	 * @var string
	 */
	protected $login = "bitrix1";

	/**
	 * Пароль
	 * @var string
	 */
	protected $password = "bitrix1";

	/**
	 * Хост
	 * @var string
	 */
	protected $host = "10.0.0.51";

	/**
	 * Путь к директории на удаленном сервере, где будет сохнанен файл
	 */
	const REMOTE_FILE_PATH = '\\public\\';

	/**
	 * Ftp1C constructor.
	 */
	function __construct()
	{
		parent::__construct($this->host, $this->login, $this->password);
	}

	/**
	 * Сохраняет файл на удаленно сервере
	 * @param string $localFilePath - локальный путь до файла
	 * @param string $subDirName - имя поддиректории
	 * @param string $fileName - имя файла
	 *
	 * @return string|bool - путь до сохраненного файла или false
	 */
	public function filePut($localFilePath = "", $subDirName = "folder\\", $fileName = "")
	{
		if(strlen($localFilePath) > 0)
		{
			if(strlen($fileName) <= 0)
			{
				$fileName = $this->returnFileName($localFilePath);
			}
			$resMakeDir = $this->makeRemoteDir($subDirName);
			if($fileName && $resMakeDir)
			{
				return parent::filePut($localFilePath, $resMakeDir.$fileName);
			}
		}
		return false;
	}

	/**
	 * Вернет имя файла по его пути
	 * @param string $filePath - путь до файла
	 *
	 * @return string|bool - имя файла или false
	 */
	private function returnFileName($filePath = "")
	{
		if(strlen($filePath) > 0)
		{
			$pathParts = pathinfo($filePath);
			if($pathParts)
			{
				return $pathParts["basename"];
			}
		}
		return false;
	}

	/**
	 * Создает директорию, если она не существует
	 * @param $subDirName - имя поддиректории
	 *
	 * @return string|bool - путь до директории или false
	 */
	protected function makeRemoteDir($subDirName)
	{
		if (!ftp_nlist($this->connId, static::REMOTE_FILE_PATH . $subDirName))
		{
			$resMakeDir = ftp_mkdir($this->connId, static::REMOTE_FILE_PATH . $subDirName);
		}
		else
		{
			$resMakeDir = static::REMOTE_FILE_PATH . $subDirName;
		}
		return $resMakeDir;
}

}