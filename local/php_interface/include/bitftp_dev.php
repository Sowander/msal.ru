<?php

/**
 * (c) Bit24, http://studiobit.ru
 * User: Bondarenko Georg
 * Date: 21.06.2016 16:37
 */

/**
 * Базовый класс для подключения по FTP
 * Class BitFtp
 */
class BitFtpDev
{
	const LINUX = 1;
	const MS_WINDOWS = 2;

	protected $serverOS = self::LINUX;

	/**
	 * Логин
	 * @var string
	 */
	protected $login = "";

	/**
	 * Пароль
	 * @var string
	 */
	protected $password = "";

	/**
	 * Хост
	 * @var string
	 */
	protected $host = "";

	/**
	 * Порт
	 * @var int
	 */
	protected $port = 21;

	/**
	 * Идентификатор соединения
	 * @var bool|resource
	 */
	protected $connId = false;

	/**
	 * BitFtp constructor.
	 *
	 * @param string $host
	 * @param int $port
	 * @param string $login
	 * @param string $password
	 * @param int $serverOS
	 */
	function __construct($host = "", $login = "", $password = "", $serverOS = self::LINUX, $port = 21)
	{
		if(strlen($host) > 0)
		{
			$this->host = $host;
		}
		if(intval($port) > 0)
		{
			$this->port = $port;
		}
		if(strlen($login) > 0)
		{
			$this->login = $login;
		}
		if(strlen($password) > 0)
		{
			$this->password = $password;
		}
		if(intval($serverOS) > 0)
		{
			$this->serverOS = $serverOS;
		}
		if(strlen($this->host) > 0 && strlen($this->login) > 0 && strlen($this->password) > 0)
		{
			$this->connId = ftp_connect($this->host, $this->port);
			ftp_login($this->connId, $this->login, $this->password);
			ftp_pasv($this->connId, true);
		}
	}

	/**
	 * Завершает соединение по ftp
	 */
	public function disconnect()
	{
		ftp_close($this->connId);
	}

	/**
	 *
	 */
	function __destruct()
	{
		$this->disconnect();
	}

	/**
	 * Сохраняет файл на удаленном сервере
	 * @param string $localFilePath - локальный путь до файла
	 * @param string $remoteFilePath - удаленный путь до файла
	 *
	 * @return string|bool - путь до сохраненного файла или false
	 */
	public function filePut($localFilePath = "", $remoteFilePath = "")
	{
		$remoteFilePath = static::formatSlashes($remoteFilePath);
		if(strlen($localFilePath) > 0 && strlen($remoteFilePath) > 0)
		{
			$fileSize = 0;
			$putCount = 0;
			$localFileSize = filesize($localFilePath);
			while(intval($fileSize) != intval($localFileSize) && $putCount < 5)
			{
				ftp_put($this->connId, $remoteFilePath, $localFilePath, FTP_BINARY);
				$fileSize = ftp_size($this->connId, $remoteFilePath);
				$putCount++;
			}
			if (intval($fileSize) == intval($localFileSize))
			{
				return $remoteFilePath;
			}
			else
			{
				return false;
			}
		}
		return false;
	}

	/**
	 * Удаляет файл на удаленном сервере
	 * @param string $remoteFilePath - удаленный путь до файла
	 *
	 * @return string|bool - путь до сохраненного файла или false
	 */
	public function fileDelete($remoteFilePath = "")
	{
		$remoteFilePath = static::formatSlashes($remoteFilePath);
		if(strlen($remoteFilePath) > 0)
		{
			return ftp_delete($this->connId, $remoteFilePath);
		}
		return false;
	}

	protected function formatSlashes($path)
	{
		$search = "";
		$replace = "";
		switch($this->serverOS) {
			case self::LINUX:
				$search = "\\";
				$replace = "/";
				break;
			case self::MS_WINDOWS:
				$search = "/";
				$replace = "\\";
				break;
		}

		return str_replace($search, $replace, $path);
	}
}

/**
 * Класс для работы с сервером 1С по FTP
 * Class Ftp1C
 */
class Ftp1CDev extends BitFtpDev
{
	/**
	 * ОС сервера
	 * @var string
	 */
	protected $serverOS = self::MS_WINDOWS;

	/**
	 * Логин
	 * @var string
	 */
	protected $login = "bitrix1";

	/**
	 * Пароль
	 * @var string
	 */
	protected $password = "bitrix1";

	/**
	 * Хост
	 * @var string
	 */
	protected $host = "10.0.0.51";

	/**
	 * Порт
	 * @var int
	 */
	protected $port = 21;

	/**
	 * Путь к директории на удаленном сервере, где будет сохнанен файл
	 */
	const REMOTE_FILE_PATH = '/public/pk2018/';

	/**
	 * Ftp1C constructor.
	 */
	function __construct()
	{
		parent::__construct($this->host, $this->login, $this->password, $this->serverOS, $this->port);
	}

	/**
	 * Сохраняет файл на удаленно сервере
	 * @param string $localFilePath - локальный путь до файла
	 * @param string $subDirName - имя поддиректории
	 * @param string $fileName - имя файла
	 *
	 * @return string|bool - путь до сохраненного файла или false
	 */
	public function filePut($localFilePath = "", $subDirName = "folder/", $fileName = "")
	{
		if(strlen($localFilePath) > 0)
		{
			if(strlen($fileName) <= 0)
			{
				$fileName = $this->returnFileName($localFilePath);
			}
			$resMakeDir = $this->makeRemoteDir($subDirName);
			if($fileName && $resMakeDir)
			{
				return parent::filePut($localFilePath, $resMakeDir.$fileName);
			}
		}
		return false;
	}

	/**
	 * Удаляет файл на удаленно сервере
	 * @param string $subDirName - имя поддиректории
	 * @param string $fileName - имя файла
	 *
	 * @return string|bool - путь до сохраненного файла или false
	 */
	public function fileDelete($subDirName = "folder/", $fileName = "")
	{
		if(strlen($fileName) > 0)
		{
			$resDir = static::REMOTE_FILE_PATH . $subDirName;
			if($fileName && $resDir)
			{
				return parent::fileDelete($resDir.$fileName);
			}
		}
		return false;
	}

	public function fileReplace($curFilePath, $newFilePath, $fileName)
	{
		if (strlen($curFilePath) > 0 && strlen($newFilePath) > 0)
		{
			$resMakeDir = $this->makeRemoteDir($newFilePath);
			if($fileName && $resMakeDir)
			{
				$curFilePath = static::formatSlashes($curFilePath);
				$resMakeDir = static::formatSlashes($resMakeDir);
				return ftp_rename($this->connId, $curFilePath . $fileName, $resMakeDir . $fileName);
			}
		}
		return false;
	}

	public function dirReplace($curDirPath, $newDirPath)
	{
		if (strlen($curDirPath) > 0 && strlen($newDirPath) > 0)
		{
			$curDirPath = static::formatSlashes($curDirPath);
			$newDirPath = static::formatSlashes(static::REMOTE_FILE_PATH . $newDirPath);
			return ftp_rename($this->connId, $curDirPath, $newDirPath);
		}
		return false;
	}

	/**
	 * Вернет имя файла по его пути
	 * @param string $filePath - путь до файла
	 *
	 * @return string|bool - имя файла или false
	 */
	private function returnFileName($filePath = "")
	{
		if(strlen($filePath) > 0)
		{
			$pathParts = pathinfo($filePath);
			if($pathParts)
			{
				return $pathParts["basename"];
			}
		}
		return false;
	}

	/**
	 * Создает директорию, если она не существует
	 * @param $subDirName - имя поддиректории
	 *
	 * @return string|bool - путь до директории или false
	 */
	protected function makeRemoteDir($subDirName)
	{
		$resMakeDir = static::formatSlashes(static::REMOTE_FILE_PATH . $subDirName);
		$nlistRes = ftp_nlist($this->connId, static::REMOTE_FILE_PATH);
		foreach ($nlistRes as &$path)
		{
			$path = static::formatSlashes($path);
		}
		unset($path);
		$dirWithoutSlash = preg_replace('#[\/\\\]$#', "", $resMakeDir);
		if (empty($nlistRes) || !in_array($dirWithoutSlash, $nlistRes))
		{
			$resMakeDir = ftp_mkdir($this->connId, $resMakeDir);
		}

		return $resMakeDir;
}

}