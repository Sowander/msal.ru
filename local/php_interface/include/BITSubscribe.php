<?php
/**
 * Created by PhpStorm.
 * User: Bondarenko Georg
 * Date: 10.09.2015
 * Time: 16:11
 */

/**
 * This class need in subscribe templates
 * Consist from methods, witch get "result" data
*/
class BITSubscribe
{

    const NEWS_IBLOCK_ID = 8;
    const EVENTS_IBLOCK_ID = 6;

    /**
     * Method return array with news data
     * Elements get from tomorow, if no time range
     * @param int $subjectId - идентификатор тематики
     * @param string $iblockCode - код инфоблока
     * @param array $arTimeRange - массив диапазона дат
     * @return array - новости
     */
    public static function GetNewContentTodayBySubject($subjectId, $iblockCode = "news", $arTimeRange = array())
    {
        if($iblockCode == "news")
        {
            $iblock_id = self::NEWS_IBLOCK_ID;
        }
        elseif($iblockCode == "events")
        {
            $iblock_id = self::EVENTS_IBLOCK_ID;
        }
        $arResultNews = array();
        if (CModule::IncludeModule("iblock")) {
            $ar_select = Array(
	            "ID",
	            "NAME",
	            "DATE_CREATE",
	            "DATE_ACTIVE_FROM",
	            "TIMESTAMP_X",
	            "DETAIL_PAGE_URL",
	            "PROPERTY_UF_SUBJECTS",
                "PROPERTY_UF_START_EVENT"
            );
	        $last_date = time() - (86400 * 1);
            $ar_filter = Array(
                "IBLOCK_ID" => intval($iblock_id),
                "ACTIVE" => "Y",
                "PROPERTY_UF_SUBJECTS" => $subjectId,
                ">TIMESTAMP_X" => array(false, ConvertTimeStamp($last_date, "FULL")),
            );
	        if(!empty($arTimeRange) && intval($arTimeRange["FROM"]) > 0 && intval($arTimeRange["TO"]) > 0)
	        {
		        $ar_filter[">TIMESTAMP_X"] = array(false, ConvertTimeStamp($arTimeRange["FROM"], "FULL"));
		        $ar_filter["<=TIMESTAMP_X"] = array(false, ConvertTimeStamp($arTimeRange["TO"], "FULL"));
	        }
            $rsNews = CIBlockElement::GetList(Array(), $ar_filter, false, false, $ar_select);
            while ($arNews = $rsNews->GetNextElement()) {
                $fields = $arNews->GetFields();
	            $fields["TYPE_CODE"]  = $iblockCode;
	            if(strtotime($fields["DATE_CREATE"])<=$last_date)
	            {
		            $fields["OLD"]  = "Y";
	            }
                $arResultNews[] = $fields;
                unset($fields);
                unset($properties);
            }
        }
        return $arResultNews;
    }

    public static function ReturnSubscribeNewsTemplate($arNewsData)
    {
        $str_template = "";
        if (!empty($arNewsData)) {
            $str_template .= '<table cellpadding="0" cellspacing="10" border="0">
            <tbody>
                <tr>
                    <td>Новость</td>
                    <td>Дата</td>
                    <td>Тематика</td>
                </tr>';
            foreach ($arNewsData as $news) {
                $str_template .= '<tr>';
                $str_template .= '<td>' . $news["NAME"] . '</td>';
                $str_template .= '<td>' . $news["DATE_CREATE"] . '</td>';
                $str_template .= '<td>' . $news["PROPERTY_UF_SUBJECTS_VALUE"] . '</td>';
                $str_template .= '</tr>';
            }
            $str_template .= '</tbody></table>';
        }
        return $str_template;
    }
}