<?php

/**
 * (c) Bit24, http://studiobit.ru
 * User: Bondarenko Georg
 * Date: 30.08.2016 10:48
 */
class BitNews
{

	/**
	 * ИД инфоблока нововстей
	 */
	const IBLOCK_ID = 8;
	/**
	 * ИД поля департамента у новости
	 */
	const DEPARTMENT_FIELD_ID = 77;

	/**
	 * @param $arFields
	 */
	public static function onBeforeUpdateHandler(&$arFields)
	{
		if($arFields["IBLOCK_ID"] == static::IBLOCK_ID)
		{
			static::autoSetAuthor($arFields);
		}
	}

	/**
	 * Автооопределение департаметра новости по автору
	 * @param $arFields
	 */
	protected static function autoSetAuthor(&$arFields)
	{
		$dep = $arFields["PROPERTY_VALUES"][static::DEPARTMENT_FIELD_ID];
		$curVal = array_shift($dep);
		if(intval($curVal['VALUE']) == 0 && count($arFields["PROPERTY_VALUES"][static::DEPARTMENT_FIELD_ID]) == 1)
		{
			global $USER;
			$arUserFields = \CUser::GetList(
				($by="id"),
				($order="desc"),
				array(
					"ID"    =>  $USER->GetID()
				),
				array(
					"SELECT"    =>  array("UF_DEPARTMENT")
				)
			)->fetch();
			if(is_array($arUserFields["UF_DEPARTMENT"]))
			{
				array_push($arFields["PROPERTY_VALUES"][static::DEPARTMENT_FIELD_ID], intval(array_shift($arUserFields["UF_DEPARTMENT"])));
			}
			elseif(intval($arUserFields["UF_DEPARTMENT"]) > 0)
			{
				array_push($arFields["PROPERTY_VALUES"][static::DEPARTMENT_FIELD_ID], intval($arUserFields["UF_DEPARTMENT"]));
			}
		}
	}
}