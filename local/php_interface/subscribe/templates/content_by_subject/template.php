<?
/**
 * Created by PhpStorm.
 * User: Bondarenko Georg
 * Date: 10.08.2015
 * Time: 15:00
 */
if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();

global $SUBSCRIBE_TEMPLATE_RESULT;
global $SUBSCRIBE_TEMPLATE_RUBRIC;
global $USER;
global $APPLICATION;
$SAVED_USER = $USER;
$USER = new CUser;

$SUBSCRIBE_TEMPLATE_RESULT = 0;
$ar_return = array();
$ar_return = $APPLICATION->IncludeComponent(
	"bit:custom.news_for_subscribe",
	"",
	Array(
		"RUBRIC_ID"    =>  $ID
	)
);
$SUBSCRIBE_TEMPLATE_RUBRIC = $ar_return;
$SUBSCRIBE_TEMPLATE_RESULT++;

$USER = $SAVED_USER;
if($SUBSCRIBE_TEMPLATE_RUBRIC && isset($SUBSCRIBE_TEMPLATE_RUBRIC) && !empty($SUBSCRIBE_TEMPLATE_RUBRIC) && $SUBSCRIBE_TEMPLATE_RESULT)
{
    return $SUBSCRIBE_TEMPLATE_RUBRIC;
}
?>