<?php
/**
 * Created by PhpStorm.
 * User: Bondarenko Georg
 * Date: 31.07.2015
 * Time: 14:45
 */
//define('BX_COMPRESSION_DISABLED',true);
//
//ini_set('soap.wsdl_cache_enabled', 0 );
//ini_set('soap.wsdl_cache_ttl', 0);

$arExclusionDirs = array(
	$_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/customs",
	$_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/cron",
	$_SERVER["DOCUMENT_ROOT"] . "/local/php_interface/include/soap"
);

BITIncludeFile(__DIR__ . "/include/", $arExclusionDirs);


Bitrix\Main\Loader::registerAutoLoadClasses(null, array(
	'Firstbit\Soap\MsalService' => '/local/php_interface/include/soap/msalservice.php',
	'Firstbit\Soap\Structure' => '/local/php_interface/include/soap/structure.php',
	'Firstbit\Soap\SoapException' => '/local/php_interface/include/soap/soapexception.php',
	'Firstbit\Soap\TeachingStaff' => '/local/php_interface/include/soap/teachingstaff.php',
	'Firstbit\Soap\File' => '/local/php_interface/include/soap/file.php',
	'Firstbit\Soap\Event' => '/local/php_interface/include/soap/event.php',
	'Firstbit\Soap\Tab' => '/local/php_interface/include/soap/tab.php',
	'Firstbit\Soap\Phonebook' => '/local/php_interface/include/soap/phonebook.php',
	'Firstbit\Soap\Client\Base' => '/local/php_interface/include/soap/client/base.php',
	'Firstbit\Soap\Client\SharePoint' => '/local/php_interface/include/soap/client/sharepoint.php',
	'\Lk' => '/personal/Lk.php',
	'\Import1C' => '/personal/Import1C.php',
	'\Progress' => '/personal/Progress.php',
	'\Ring' => '/personal/Ring.php',
	'\Schedule' => '/personal/Schedule.php',
	'\Student' => '/personal/Student.php',
	'\Prepod' => '/personal/Prepod.php',
	'\Terminal\Common' => '/terminal/Common.php',
	'\Terminal\Schedule' => '/terminal/Schedule.php',
	'\Terminal\News' => '/terminal/News.php',
));

function BITIncludeFile($dir, $arExclusionDirs = array())
{
	if (is_dir($dir) && !in_array($dir, $arExclusionDirs)) {
		$_dir = opendir($dir);

		while ($_ = readdir($_dir)) {
			if ($_ == "." || $_ == "..") continue;

			$_ = $dir . "/" . $_;
			$_ = str_replace("//", "/", $_);

			if (is_dir($_)) {
				BITIncludeFile($_, $arExclusionDirs);
			} elseif (is_file($_)) {
				if (!stristr($_, '.php')) continue;
				@include_once($_);
			}
		}
		closedir($_dir);
	} else {
		return false;
	}
}

function printr($ar, $adm = false)
{
	global $USER;
	$userId = $USER->GetID();
	if ($USER->IsAdmin() or $adm == 1 or $userId == 3304 or $userId == 35290) {
		if (is_array($ar) or is_object($ar)) {
			echo '<pre>';
			print_r($ar);
			echo '</pre>';
		} else {
			echo '<pre>';
			var_dump($ar);
			echo '</pre>';
		}
	}
}