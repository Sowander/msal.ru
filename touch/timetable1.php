<?


$f = file_get_contents('data/instituti.csv.html');

$f = explode('
',$f);
foreach($f as $v)
{
	$tmp = explode('#',$v);
	$list[] = array('name'=>$tmp[0]);
}



function cmp($a, $b)
{
    if ($a[name] == $b[name]) {
        return 0;
    }
    return ($a < $b) ? -1 : 1;
}

#$a = array(3, 2, 5, 6, 1);

usort($list, "cmp");



$pagecount = ceil(count($list)/8);
if(!$_GET[page]) $_GET[page]=1;

$curlist = array_slice($list, (8*($_GET[page]-1)) ,8);


?><!DOCTYPE html>
<html lang="en">
<head>
    <title>МГЮА. Расписание</title>
    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/main.css">   
</head>
<body>
    <div class="msal-page">
        <header class="msal-header msal-secondary-header">
            <div class="msal-header__logo">
                <div class="msal-logo">
                    <a href="/touch/"><img class="msal-logo__img" src="img/logo-sm.png"/></a>
                </div>
            </div>
        </header>
        <section class="msal-main msal-secondary-main msal-department-section">
            <div class="msal-main__text">
                <h1 class="msal-main__text-title">Выберите институт</h1>
            </div>
            <div class="msal-main__department-sections">
        		<div class="department-sections">
        			<ul class="department-sections__items">
                        <?foreach($curlist as $v){?>
						<li><a href="./timetable2.php?i=<?=$v[name]?>"><?=$v[name]?></a></li>
						<?}?>
                       
        			</ul>
        		</div>
        	</div>

            <div class="msal-main__navigation">
                <div class="msal-navigation__nav-prev"><a href="#"><img src="img/arr-prev.png" alt=""></a></div>
                 <ul class="msal-navigation__list">
					<?for($i=1;$i<=$pagecount;$i++){?>
                    <li class="msal-navigation__list-item <?if($i==$_GET[page]) echo ' active ';?>"><?if($i!=$_GET[page]){?><a href="./timetable1.php?page=<?=$i?>"><?}?><?=$i?><?if($i!=$_GET[page]){?></a><?}?></li>
					<?}?>
                </ul>
                <div class="msal-navigation__nav-next"><a href="#"><img src="img/arr-next.png" alt=""></a></div>
            </div>

        </section>
        <footer class="msal-footer msal-secondary-footer">
            <div class="msal-footer__btns">
                <a onclick="javascript:history.back(); return false;" class="msal-footer__btns-left">ВЕРНУТЬСЯ НАЗАД</a>
                <a href="/touch/" class="msal-footer__btns-right">ВЕРНУТЬСЯ НА ГЛАВНУЮ</a>
            </div>
        </footer>
    </div>
</body>
</html>