<?

$f = file_get_contents('data/instituti.csv.html');

$f = explode('
',$f);
foreach($f as $v)
{
	$tmp = explode('#',$v);
	$instList[] = $tmp[0];
}



$f = file_get_contents('data/gruppi.csv.html');

$f = explode('
',$f);
foreach($f as $v)
{
	$tmp = explode('#',$v);
	$list[] = $tmp[0];
}

$from = 0;

//находим наш инст
foreach($list as $k=>$v)
{
	if($v==$_GET[i])
	{
		$from = $k;
	}
	
	if($from > 0 and in_array($v,$instList) and $v!=$_GET[i] )
	{
		$to = $k;
		//echo $v;
		break;
	}
}

#echo '-'.$from.'-'.$to.'-';


//берем года
foreach($list as $k=>$v)
{
	if( $k>=$from and $k<$to and strpos( $v,'/20' ))
	{
		$yearList[$v] = $v;
	}
}


foreach($list as $k=>$v)
{
	if( $k>=$from and $k<$to and strpos( $v,'/20' ))
	{
		//$yearList[$v] = $v;
		$curYear = $v;
	}
	
	if( $k>=$from and $k<$to and !strpos( $v,'/20' ))
	{
		$groups[ $curYear ][] = $v;
	}
}




$pagecount = ceil(count($groups[ $curYear ])/12);
if(!$_GET[page]) $_GET[page]=1;

$curgroups = array_slice($groups[ $curYear ], (12*($_GET[page]-1)) ,12);



?><!DOCTYPE html>
<html lang="en">
<head>
    <title>МГЮА. Расписание</title>
    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/main.css">   
</head>
<body>
    <div class="msal-page">
        <header class="msal-header msal-secondary-header">
            <div class="msal-header__logo">
                <div class="msal-logo">
                    <a href="/touch/"><img class="msal-logo__img" src="img/logo-sm.png"/></a>
                </div>
            </div>
        </header>
        <section class="msal-main msal-secondary-main msal-department-section">
            <div class="msal-main__text">
                <h1 class="msal-main__text-title">Выберите группу</h1>
            </div>
            <div class="msal-main__department-sections">
        		<div class="department-sections">
        			<ul class="department-sections__items">
                        <?foreach($curgroups as $v){?>
						<li><a href="schedule.html"><?=$v?></a></li>
						<?}?>
                       
        			</ul>
        		</div>
        	</div>

            <div class="msal-main__navigation">
                <div class="msal-navigation__nav-prev"><a href="#"><img src="img/arr-prev.png" alt=""></a></div>
                 <ul class="msal-navigation__list">
					<?for($i=1;$i<=$pagecount;$i++){?>
                    <li class="msal-navigation__list-item <?if($i==$_GET[page]) echo ' active ';?>"><?if($i!=$_GET[page]){?><a href="./timetable3.php?i=<?=$_GET[i]?>&y=<?=$v?>&page=<?=$i?>"><?}?><?=$i?><?if($i!=$_GET[page]){?></a><?}?></li>
					<?}?>
                </ul>
                <div class="msal-navigation__nav-next"><a href="#"><img src="img/arr-next.png" alt=""></a></div>
            </div>

        </section>
        <footer class="msal-footer msal-secondary-footer">
            <div class="msal-footer__btns">
                <a onclick="javascript:history.back(); return false;" class="msal-footer__btns-left">ВЕРНУТЬСЯ НАЗАД</a>
                <a href="/touch/" class="msal-footer__btns-right">ВЕРНУТЬСЯ НА ГЛАВНУЮ</a>
            </div>
        </footer>
    </div>
</body>
</html>