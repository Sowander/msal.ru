<?


$f = file_get_contents('data/news.csv.html');

$f = explode('
',$f);
foreach($f as $v)
{
	$tmp = explode('#',$v);
	$list[] = array('name'=>$tmp[0],'img'=>$tmp[1],'date'=>$tmp[2],'detail'=>$tmp[3]);
}
unset($list[0]);



/*
$pagecount = ceil(count($list)/8);
if(!$_GET[page]) $_GET[page]=1;

$curlist = array_slice($list, (8*($_GET[page]-1)) ,8);
*/
?><!DOCTYPE html>
<html lang="en">
<head>
    <title>МГЮА. Календарь новостей</title>
    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">    
    <!-- styles needed by jScrollPane -->
    <link rel="stylesheet" type="text/css" href="css/jquery.jscrollpane.css" media="all" />
    <link rel="stylesheet" type="text/css" href="css/main.css">   
</head>
<body>
    <div class="msal-page">
        <header class="msal-header msal-secondary-header">
            <div class="msal-header__logo">
                <div class="msal-logo">
                    <a href="/touch/"><img class="msal-logo__img" src="img/logo-sm.png"/></a>
                </div>
            </div>
        </header>
        <section class="msal-main msal-secondary-main msal-news-section">
            <div class="msal-main__text">
                <h1 class="msal-main__text-title">Календарь новостей</h1>
            </div>
            <div class="msal-main__news-sections">
                <div class="news-sections">
					<?foreach($list as $v)if($v['date']){?>
                    <div class="news-sections__item">
                       
                        <div class="news__item-date"><?=$v['date']?></div>
                        
                        <div class="news__item-text">
                            <div class="news__item-text-preview">
                                <div class="news__item-image"><img src="<?=$v['img']?>" alt=""></div>
                                <p><?=$v['name']?></p>
                            </div> 
                            <div class="news__item-text-full scroll-pane">
                                <div class="news__item-image"><img src="<?=$v['img']?>" alt=""></div>
                                <p><?=$v['detail']?></p>
                            </div>
                        </div>
                        <div class="news__item-link"><a href="#">читать весь текст новости</a></div>
                    </div>
                   <?}?>
                 
                </div>
        	</div>

        </section>
        <footer class="msal-footer msal-secondary-footer">
            <div class="msal-footer__btns">
                <button class="msal-footer__btns-left">ВЕРНУТЬСЯ К НОВОСТЯМ</button>
                <a href="/touch/" class="msal-footer__btns-right">ВЕРНУТЬСЯ НА ГЛАВНУЮ</a>
            </div>
        </footer>
    </div>

	
	
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
    </script>
    <!-- the mousewheel plugin - optional to provide mousewheel support -->
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>
    <!-- the jScrollPane script -->
    <script type="text/javascript" src="js/jquery.jscrollpane.min.js"></script>

    <script>
        $(document).ready(function () {
            
            $(".msal-footer__btns-left").click(function(){
                $(".msal-page").removeClass("single-new-open");
                $(".news-sections__item").removeClass("close-new");
                $(".news-sections__item").removeClass("open-new");
                $(".news__item-link").show();

            });
            $(".news__item-link").click(function(){
                $(".msal-page").addClass("single-new-open");
                $(".news-sections__item").addClass("close-new");
                $(this).parents(".news-sections__item").addClass("open-new");
                $(this).hide();

                $('.scroll-pane').jScrollPane({
                    showArrows: true,
                    verticalDragMinHeight: 0
                });
            });
        });
            
    </script>
</body>
</html>