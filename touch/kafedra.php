<!DOCTYPE html>
<html lang="en">
<head>
    <title>МГЮА. <?=$_GET[name]?></title>
    <meta charset="utf-8">
    <link href="//fonts.googleapis.com/css?family=PT+Sans:400,700&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">
    <!-- styles needed by jScrollPane -->
    <link rel="stylesheet" type="text/css" href="css/jquery.jscrollpane.css" media="all" />
    <link rel="stylesheet" type="text/css" href="css/main.css">   
	<style>
	.teacher-section>div {width:100% !important; max-width:100%}
	</style>
</head>
<body>
    <div class="msal-page">
        <header class="msal-header msal-secondary-header">
            <div class="msal-header__logo">
                <div class="msal-logo">
                    <a href="/touch/"><img class="msal-logo__img" src="img/logo-sm.png"/></a>
                </div>
            </div>
        </header>
        <section class="msal-main msal-secondary-main msal-teacher-section">
            <div class="msal-main__text">
                <h1 class="msal-main__text-title" style="text-align: left; font-size:40px"><?=$_GET[name]?></h1>
            </div>
            <div class="msal-main__teacher-section">
                <div class="teacher-section">
                    <div class="teacher-information scroll-pane" >
                		<div class="teacher-information-content">
								<p style="text-align: justify;"></p>
<div style="text-align: left;">
 <p>Заведующий кафедрой: <b><?=$_GET[chief]?></b></p>
 <p>Телефон: <b><?=$_GET[tel]?> # <?=$_GET[dob]?></b></p>
 <br>
 <h2>Подробная информация публикуется</h2>
                            <br><br><br><br><br>
                        </div>
                    </div>
        		</div>
        	</div>

        </section>
        <footer class="msal-footer msal-secondary-footer">
            <div class="msal-footer__btns">
                <a onclick="javascript:history.back(); return false;" class="msal-footer__btns-left">ВЕРНУТЬСЯ НАЗАД</a>
                <a href="/touch/" class="msal-footer__btns-right">ВЕРНУТЬСЯ НА ГЛАВНУЮ</a>
            </div>
        </footer>
    </div>

    

    <!-- latest jQuery direct from google's CDN -->
    <script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
    </script>
    <!-- the mousewheel plugin - optional to provide mousewheel support -->
    <script type="text/javascript" src="js/jquery.mousewheel.js"></script>
    <!-- the jScrollPane script -->
    <script type="text/javascript" src="js/jquery.jscrollpane.min.js"></script>

    <script>
        $(document).ready(function () {
            $('.scroll-pane').jScrollPane({
                showArrows: true,
                verticalDragMinHeight: 0
            });
        });
    </script>
</body>
</html>