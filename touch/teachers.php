<?


$f = file_get_contents('data/teachers.csv.html');

$f = explode('
',$f);
foreach($f as $v)
{
	$tmp = explode(';',$v);
	$list[] = array('name'=>trim($tmp[0]),'pos'=>$tmp[1],'kaf'=>$tmp[2]);
}


function cmp($a, $b)
{
    if ($a[name] == $b[name]) {
        return 0;
    }
    return ($a < $b) ? -1 : 1;
}

#$a = array(3, 2, 5, 6, 1);

usort($list, "cmp");

if(!$_GET[a]) $_GET[a]=1;

$alp[1] = array('А','Б','В','Г');
$alp[2] = array('Д','Е','Ё','Ж','З','И','Й');
$alp[3] = array('К','Л','М','Н');
$alp[4] = array('О','П','Р','С','Т','У');
$alp[5] = array('Ф','Х','Ц','Ч');
$alp[6] = array('Ш','Щ');
$alp[7] = array('Э','Ю','Я');


#print_r($list);

foreach($list as $k=>$v)
{
	

	
	$fl = mb_substr($v[name],0,1,'utf-8');
	
	#	print_r($alp[$_GET[a]]);
#print_r($fl);
	
	
	if( in_array( $fl,$alp[$_GET[a]]) )
	{
		$curlist[] = $v;
	}
}


?><!DOCTYPE html>
<html lang="en">
<head>
    <title>МГЮА. Преподаватели</title>
    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/main.css">   
</head>
<body>
    <div class="msal-page">
        <header class="msal-header msal-secondary-header">
            <div class="msal-header__logo">
                <div class="msal-logo">
                    <a href="/touch/"><img class="msal-logo__img" src="img/logo-sm.png"/></a>
                </div>
            </div>
        </header>
        <section class="msal-main msal-secondary-main msal-teachers-section">
            <div class="msal-main__text">
                <h1 class="msal-main__text-title">Преподаватели</h1>
                <h2 class="msal-main__text-subtitle">Выберите первую букву фамилии</h2>
            </div>
            <div class="msal-main__teachers-sections">
                <div class="teachers-alphabet">
                    <ul class="teachers-alphabet__items">
                        <li ><a href="?a=1">А-Г</a></li>                        
                        <li><a href="?a=2">Д-И</a></li>                        
                        <li><a href="?a=3">К-Н</a></li>                        
                        <li><a href="?a=4">О-У</a></li>                        
                        <li><a href="?a=5">Ф-Ч</a></li>                        
                        <li><a href="?a=6">Ш-Щ</a></li>                        
                        <li><a href="?a=7">Э-Я</a></li>                        
                    </ul>
                </div>
        		<div class="teachers-sections">
        			<ul class="teachers-sections__items">
                       
                        <?foreach($curlist as $v){?>
						<li><a href="./teacher.php?name=<?=$v[name]?>&pos=<?=$v['pos']?>&kaf=<?=$v[kaf]?>"><?=$v[name]?></a></li>
						<?}?>
                       
        			
        			</ul>
        		</div>
        	</div>

           

        </section>
        <footer class="msal-footer msal-secondary-footer">
            <div class="msal-footer__btns">
                <a onclick="javascript:history.back(); return false;" class="msal-footer__btns-left">ВЕРНУТЬСЯ НАЗАД</a>
                <a href="/touch/" class="msal-footer__btns-right">ВЕРНУТЬСЯ НА ГЛАВНУЮ</a>
            </div>
        </footer>
    </div>
</body>
</html>