<!DOCTYPE html>
<html lang="en">
<head>
    <title>МГЮА</title>
    <meta charset="utf-8">
    <link href="https://fonts.googleapis.com/css?family=PT+Sans:400,700&amp;subset=cyrillic,cyrillic-ext" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="css/main.css">   
</head>
<body>
	<div class="msal-page">
	    <header class="msal-header msal-homepage-header">
	        <div class="msal-header__logo">
	            <div class="msal-logo">
	                <a href="/touch/"><img class="msal-logo__img" src="img/logo.png"/></a>
	            </div>
	        </div>
	    </header>
	    <section class="msal-main">
	        <div class="msal-main__text">
	        	<p class="msal-main__text-title">Добрый день!</p>
	        	<p class="msal-main__text-subtitle">Выберите интересующий вас пункт меню, и следуйте подсказкам на экране</p>
	        </div>
	        <div class="msal-main__homepage-sections">
	    		<div class="homepage-sections">
	    			<div class="homepage-sections__item">
	    				<a href="timetable1.php">Расписание</a>
	    			</div>
	    			<div class="homepage-sections__item">
	    				<a href="teachers.php">Преподаватели</a>
	    			</div>
	    			<div class="homepage-sections__item">
	    				<a href="kafedry.php">Кафедры</a>
	    			</div>
	    			<div class="homepage-sections__item">
	    				<a href="news.php">Новости</a>
	    			</div>
	    		</div>
	    	</div>
	    </section>
	</div>
</body>
</html>