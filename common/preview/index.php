<?php

	if (file_exists(dirname((dirname(dirname(__FILE__)))).'/data/config/params.php')){
		include_once dirname((dirname(dirname(__FILE__)))).'/data/config/params.php';
	}
	else {
		include_once( dirname((dirname(dirname(__FILE__)))) . "/data/lib/lib_abstract.php" );
		 include_once dirname((dirname(dirname(__FILE__)))).'/data/config/config.php';
	}
	include_once params::$params['common_data_server']['value'].'/lib/op/init.inc.php';

	function error404($location='/common/medialib/static/img/404.png'){
		die();
		$location=($location)?$location:'/common/medialib/static/img/404.png';
		header('Location: '.$location);
		die();
	}
	
	$preview=rbcc5::getObject('PREVIEW',array('NAME'=>request::String('preview')));
	
	if (!$preview) error404();
	
	try {
		$img=img::loadFromFile(params::$params['common_htdocs_server']['value'].request::String('img'));
		$resized=$img->resize($preview->WIDTH,$preview->HEIGHT,$preview->ALIGN,$preview->VALIGN,$preview->BACKGROUND,$preview->CUT,$preview->SCALE);
		
		$resized->display($img->mime);
		$resized->save(params::$params['common_htdocs_server']['value'].'preview/'.$preview->NAME.'/'.request::String('img'));
	}	
	catch (img_resize_scale_exception $e){				
		header('Location: /common/'.request::String('img'));
	}
	catch (Exception $e){
		error404();	
	}
	
?>
