<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Личный кабинет");


?>
<script src='https://www.google.com/recaptcha/api.js'></script>
<style>
.left_sidebar {
    min-height: 30px;
}
#FORM2-captchaContainer {
	margin: 0px 19.6%;
    background-color: #501619;
    border-radius: 5px;}
	p {margin:.3em;}
</style>
<div style="width:1024px;">
<br /><br /><p><b>Уважаемые коллеги!</b></p>
<p>Нам очень важно знать и оперативно реагировать на возможные трудности, с которыми Вы можете столкнуться при работе в личном кабинете. Воспользуйтесь формой обратной связи, чтобы сообщить о проблеме, с которой Вы столкнулись.</p>
<p>Мы также будем очень Вам благодарны, если получим обратную связь с пожеланиями по развитию функциональных модулей личного кабинета - чего не хватает, что можно было бы улучшить, где надо бы иначе и т.п.</p>
<p>Отправьте нам сообщение посредством формы обратной связи и мы обязательно учтем Ваши пожелания при дальнейшей разработке личных кабинетов.</p>
<div style="margin:0 auto;width:50%;">
<h3><center>Форма обратной связи</center></h3>
<?$APPLICATION->IncludeComponent(
	"slam:easyform",
	"",
	Array(
		"CAPTCHA_ERROR" => "Обязательное поле",
		"CAPTCHA_KEY" => "6Lf7TnQUAAAAAJPbI0xSson5BgW_hRoVGLBEV4hA",
		"CAPTCHA_SECRET_KEY" => "6Lf7TnQUAAAAAOIGCOygx_ftVdAgZcvg6ydS8NrD",
		"CAPTCHA_TITLE" => "Мы хотим убедиться, что сообщение отправляется не роботом, пожалуйста, пройдите проверку:",
		"CATEGORY_ACCEPT_IBLOCK_FIELD" => "FORM_ACCEPT",
		"CATEGORY_ACCEPT_TITLE" => "Согласие на обработку данных",
		"CATEGORY_ACCEPT_TYPE" => "accept",
		"CATEGORY_ACCEPT_VALIDATION_ADDITIONALLY_MESSAGE" => "",
		"CATEGORY_ACCEPT_VALIDATION_MESSAGE" => "Обязательное поле",
		"CATEGORY_ACCEPT_VALUE" => "Я ознакомлен с <a href=\"https://msal.ru/content/ob-universitete/lokalnye-normativnye-akty/?hash=tab3012\" target=\"_blank\">Политикой информационной безопасности МГЮА и даю согласие на обработку моих персональныx данных</a>",
		"CATEGORY_DOCS_DROPZONE_INCLUDE" => "Y",
		"CATEGORY_DOCS_FILE_EXTENSION" => "doc, docx, xls, xlsx, txt, rtf, pdf, png, jpeg, jpg, gif",
		"CATEGORY_DOCS_FILE_MAX_SIZE" => "20971520",
		"CATEGORY_DOCS_IBLOCK_FIELD" => "FORM_DOCS",
		"CATEGORY_DOCS_TITLE" => "Приложите файлы при необходимости",
		"CATEGORY_DOCS_TYPE" => "file",
		"CATEGORY_EMAIL_IBLOCK_FIELD" => "FORM_EMAIL",
		"CATEGORY_EMAIL_PLACEHOLDER" => "".CUser::GetEmail()."",
		"CATEGORY_EMAIL_TITLE" => "Ваш E-mail",
		"CATEGORY_EMAIL_TYPE" => "email",
		"CATEGORY_EMAIL_VALIDATION_ADDITIONALLY_MESSAGE" => "data-bv-emailaddress-message=\"E-mail введен некорректно\"",
		"CATEGORY_EMAIL_VALIDATION_MESSAGE" => "Обязательное поле",
		"CATEGORY_EMAIL_VALUE" => "".CUser::GetEmail()."",
		"CATEGORY_MESSAGE_IBLOCK_FIELD" => "PREVIEW_TEXT",
		"CATEGORY_MESSAGE_PLACEHOLDER" => "Воспользуйтесь формой обратной связи, чтобы сообщить о проблеме, с которой Вы столкнулись. Мы также будем очень Вам благодарны, если получим обратную связь с пожеланиями по развитию функциональных модулей личного кабинета - чего не хватает, что можно было бы улучшить, где надо бы иначе и т.п.",
		"CATEGORY_MESSAGE_TITLE" => "Сообщение",
		"CATEGORY_MESSAGE_TYPE" => "textarea",
		"CATEGORY_MESSAGE_VALIDATION_ADDITIONALLY_MESSAGE" => "",
		"CATEGORY_MESSAGE_VALIDATION_MESSAGE" => "Обязательное поле",
		"CATEGORY_MESSAGE_VALUE" => "",
		"CATEGORY_PHONE_IBLOCK_FIELD" => "FORM_PHONE",
		"CATEGORY_PHONE_INPUTMASK" => "Y",
		"CATEGORY_PHONE_INPUTMASK_TEMP" => "+7 (999) 999-9999",
		"CATEGORY_PHONE_PLACEHOLDER" => "",
		"CATEGORY_PHONE_TITLE" => "Мобильный телефон",
		"CATEGORY_PHONE_TYPE" => "tel",
		"CATEGORY_PHONE_VALIDATION_ADDITIONALLY_MESSAGE" => "",
		"CATEGORY_PHONE_VALUE" => "",
		"CATEGORY_TITLE_IBLOCK_FIELD" => "NAME",
		"CATEGORY_TITLE_PLACEHOLDER" => "".CUser::GetLastName()." ".CUser::GetFirstName()."",
		"CATEGORY_TITLE_TITLE" => "Ваше ФИО",
		"CATEGORY_TITLE_TYPE" => "text",
		"CATEGORY_TITLE_VALIDATION_ADDITIONALLY_MESSAGE" => "",
		"CATEGORY_TITLE_VALIDATION_MESSAGE" => "Обязательное поле",
		"CATEGORY_TITLE_VALUE" => "".CUser::GetLastName()." ".CUser::GetFirstName()."",
		"CREATE_IBLOCK" => "",
		"CREATE_SEND_MAIL" => "",
		"DISPLAY_FIELDS" => array("TITLE","EMAIL","PHONE","MESSAGE","DOCS","ACCEPT",""),
		"EMAIL_BCC" => "dmastenko@msal.ru",
		"EMAIL_TO" => "pvryabov@msal.ru",
		"ENABLE_SEND_MAIL" => "Y",
		"ERROR_TEXT" => "Произошла ошибка. Сообщение не отправлено.",
		"EVENT_MESSAGE_ID" => array("49"),
		"FIELDS_ORDER" => "TITLE,EMAIL,PHONE,MESSAGE,DOCS,ACCEPT",
		"FORM_AUTOCOMPLETE" => "Y",
		"FORM_ID" => "FORM2",
		"FORM_NAME" => "Обратная связь по работе ЛК",
		"FORM_SUBMIT_VALUE" => "Отправить",
		"FORM_SUBMIT_VARNING" => "Нажимая на кнопку \"#BUTTON#\", вы даете согласие на обработку <a target=\"_blank\" href=\"https://msal.ru/content/ob-universitete/lokalnye-normativnye-akty/?hash=tab3012\">персональных данных</a>",
		"HIDE_ASTERISK" => "N",
		"HIDE_FIELD_NAME" => "N",
		"HIDE_FORMVALIDATION_TEXT" => "N",
		"IBLOCK_ID" => "",
		"IBLOCK_TYPE" => "-",
		"INCLUDE_BOOTSRAP_JS" => "Y",
		"MAIL_SUBJECT_ADMIN" => "Обратная связь по ЛК",
		"OK_TEXT" => "Ваше сообщение успешно отправлено.
Спасибо за Ваш отзыв!",
		"REPLACE_FIELD_FROM" => "N",
		"REQUIRED_FIELDS" => array("TITLE","MESSAGE","ACCEPT"),
		"SEND_AJAX" => "Y",
		"SHOW_MODAL" => "N",
		"TITLE_SHOW_MODAL" => "Спасибо!",
		"USE_BOOTSRAP_CSS" => "Y",
		"USE_BOOTSRAP_JS" => "Y",
		"USE_CAPTCHA" => "Y",
		"USE_FORMVALIDATION_JS" => "Y",
		"USE_IBLOCK_WRITE" => "N",
		"USE_INPUTMASK_JS" => "Y",
		"USE_JQUERY" => "N",
		"USE_MODULE_VARNING" => "Y",
		"WIDTH_FORM" => "500px",
		"WRITE_MESS_FILDES_TABLE" => "Y",
		"_CALLBACKS" => ""
	)
);?>
</div></div>
<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>