var userAgent = navigator.userAgent.toLowerCase();
// Figure out what browser is being used
var browser = {
	version: (userAgent.match( /.+(?:rv|it|ra|ie)[\/: ]([\d.]+)/ ) || [])[1],
	safari: /webkit/.test( userAgent ),
	opera: /opera/.test( userAgent ),
	msie: /msie/.test( userAgent ) && !/opera/.test( userAgent ),
	msie6: /msie 6/.test( userAgent ) && !/opera/.test( userAgent ),
	mozilla: /mozilla/.test( userAgent ) && !/(compatible|webkit)/.test( userAgent )
};

var keyCode = {up:38,down:40,left:37,right:39,esc:27,enter:13,pgUp:33,pgDown:34,home:36,end:35};

DOMReady = (function(ie){
 var d = document;
 return ie ? function(c){
   var n = d.firstChild,
    f = function(){
     try{
      c(n.doScroll('left'));
     }catch(e){
      setTimeout(f, 10);
     }
    }; f()
  } : 
  /webkit|safari|khtml/i.test(navigator.userAgent) ? function(c){
   var f = function(){
     /loaded|complete/.test(d.readyState) ? c() : setTimeout(f, 10)
    }; f()
    
  } : 
  function(c){
   d.addEventListener("DOMContentLoaded", c, false);
  }
})(/*@cc_on 1@*/);

function DOM(id, debug){
	var element=null;
	if (!id) element=(browser.mozilla)?window:document;
	else if (typeof(id)=='object'){
		if (id.isDOM) return id
		element=id;		
	}
	else element=document.getElementById(id);
	
	if (!element) return null;
	element.isDOM=true;
	element.listen= function(name, f, useCapture){
		var observer= function(e){
			e=op.event(e, name);
			if (!f(e)){
				e.stop();				
			}
		}
		if (name=='scroll'){
			//it's fucken scroll event:
			if ( element.addEventListener ){
				element.addEventListener( (browser.mozilla ? 'DOMMouseScroll' : 'mousewheel'), observer, false);
			}
			else{
				element.onmousewheel = observer;
			}
		}
		else {
			if (element.addEventListener) {
			  element.addEventListener(name, observer, useCapture);
			} else if (element.attachEvent) {
			  element.attachEvent('on' + name, observer);
			}
		}
	};

	element.parent= function(tagName){
		var n=element;
		while (n.tagName!=tagName){
			n= n.parentNode;
			if (!n) return null;
		}
		return n;
	}
	
	element.first= function(tagName, className){
		var list= element.getElementsByTagName(tagName);
		if (className){
			var re= new RegExp('\\b'+className+'\\b');
			for (i=0; i<list.length; i++) if (re.test(list[i].className)) return DOM(list[i]);
		}
		else if(list[0]){
			 DOM(list[0]);			
		}
		else return null;
	}	
	
	element.hide= function(){
		element.style.display='none';
	}
	
	element.show= function(){
		element.style.display='block';
	}
	
	element.attachClass= DOM.attachClass;	
	element.detachClass= DOM.detachClass;
	
	element.create=DOM.create;		
	element.clean=DOM.clear;	
	element.getPosition=DOM.getPosition;
	element.getChildNodes=DOM.getChildNodes;
	return element;
}

DOM.getChildNodes= function(params){
	var ret=[];
	for (var i=0; i<this.childNodes.length; i++){
		if (this.childNodes[i].nodeType==1){
			if (params&&params.groupBy){
				ret[this.childNodes[i][params.groupBy]]=DOM(this.childNodes[i]);
			}
			else {
				ret[ret.length]=DOM(this.childNodes[i]);
			}
		}
	}
	return ret;
}

DOM.create= function(obj){
	var ret=DOM(document.createElement(obj.tagName));	
	
	if (obj){
		for (key in obj){
			if (key=='tagName'||key=='childNodes'||key=='insertBefore'){
				
			}
			else if (key=='style'){
				for (s in obj[key]){
					ret[key][s]=obj[key][s];
				}
			}
			else if (key.substr(0,2)=='on'){
				ret.listen(key.substr(2), obj[key]);
			}
			else if (key=='rel'||key=='rev'){
				ret.setAttribute(key, obj[key]);
			}
			else {
				ret[key]=obj[key];
			}
		}		
	}
	if (obj.insertBefore){
		this.insertBefore(ret, obj.insertBefore);
	}
	else {
		this.appendChild(ret);
	}
	if (obj.childNodes){		
		for (var i=0; i<childNodes.length; i++){
			ret.create(childNodes[i]);
		}
	}
	return ret;
}


DOM.clear= function(){
	while (this.firstChild) this.removeChild(this.firstChild);
}

DOM.attachClass= function(className){
	var re= new RegExp('\\b'+className+'\\b');
	if (!re.test(this.className)){
		this.className+=' '+className;			
	}
}

DOM.detachClass= function(className){
	var re= new RegExp('\\b'+className+'\\b');
	if (this.className){
		this.className=this.className.replace(re, '');
	}	
}

DOM.getPosition= function(){
	p=this;
	ret={left:0, top:0};
	while (p){
		if (p.style.position=='absolute'){			
			ret= {left:parseInt(p.style.left)+document.body.parentNode.clientLeft, top:parseInt(p.style.top)+document.body.parentNode.clientTop};			
			break;
		}
		p=p.parentNode;
	}		
	ret.left+=this.offsetLeft;
	ret.top+=this.offsetTop;
	return ret;
}

var op = {
	element: DOM,	
	event: function(e, name){
		if (e= (e)?e:window.event){
			//���������� �������:
			e.stop= function(){
				if (!e.preventDefault) {
					e.cancelBubble = true;
					e.returnValue = false;
				}
				else {
					e.preventDefault();
				}
			};
			
			if (!e.pageX&&!e.pageY&&e.clientX&&e.clientY){
			
				e.pageX=e.clientX;
				e.pageY=e.clientY;				
				if (document.body && typeof document.body.scrollTop != 'undefined') {
					e.pageX += document.body.scrollLeft;
					e.pageY += document.body.scrollTop;
				}
				if (document.body.parentNode && typeof document.body.parentNode.scrollTop != 'undefined') {
					e.pageX += document.body.parentNode.scrollLeft;
					e.pageY += document.body.parentNode.scrollTop;
				}
			}
			if (name=='scroll'){
				if (e&&e.detail) e.scrollDelta = -e.detail;
				else if (window.event.wheelDelta) e.scrollDelta = window.event.wheelDelta; 
			}
			e.getTarget= function(tagName){				
				t=(e.explicitOriginalTarget)?e.explicitOriginalTarget:e.srcElement;
				if (!t) return null;
				while (t.nodeType!=1){
					t=t.parentNode;
					if (!t) return null;
				}
				if (!tagName) return DOM(t);
				tagName=tagName.toUpperCase();
				while (t.tagName!=tagName){
					t=t.parentNode;
					if (!t) return null;
				}
				return DOM(t);
			}
			return e;
		}		
		else return null;
	},	
	locked:[],
	lock: function(id){
		if (!id) return true;
		if (op.locked[id]) return false;
		op.locked[id]=true;
		return true;
	},
	locked: function(id){
		return (op.locked[id])?true:false;
	},
	free: function(id){		
		if (id){
			var ret=(op.locked[id])?false:true;
			op.locked[id]=false;
			return ret;
		}
		
	},
	
	query: function(url, callback, POST){

		var xmlHttp = false;
		/*@cc_on @*/
		/*@if (@_jscript_version >= 5)
		var ver=["MSXML2.XMLHttp.5.0", "MSXML2.XMLHttp.4.0","MSXML2.XMLHttp.3.0", "MSXML2.XMLHttp","Microsoft.XMLHttp"];
		for (i=0; i<ver.length; i++){
			try { //
              var xmlHttp = new ActiveXObject(ver[i]);              
              break;
            } catch (oError) {
              //do nothing
            }
		}
		@end @*/
		
		if (!xmlHttp && typeof XMLHttpRequest != 'undefined') {
		  xmlHttp = new XMLHttpRequest(); 
		}
		if (POST){			
			xmlHttp.open("POST", url, true);
		}
		else {
			POST=null;
			xmlHttp.open("GET", url, true);
		}
		if (xmlHttp.overrideMimeType){			
			xmlHttp.overrideMimeType('text/xml');
		}		
		
		xmlHttp.onreadystatechange = function(){
			
			if (xmlHttp.readyState == 4) {				
				var result=xmlHttp.responseXML;
				
				if (result.documentElement){					
					result=result.documentElement;
				}
				else if (xmlHttp.responseStream) {					
					result.load(xmlHttp.responseStream);	
					
				}
				else result=null;	
				
				callback(op.xml.factory(result));
			}
		};
		if (POST){
			xmlHttp.setRequestHeader('Content-Type', 'application/x-www-form-urlencoded')
			xmlHttp.send(op.urlEncode(POST));
		}
		else {
			
			xmlHttp.send(null);
		}
		
	},
	urlEncode: function(data, prefix){		
		if (typeof(data)=='string') return encodeURIComponent(data);		
		if (!data) return null;
		var fullKey=null;
		ret='';
		for (key in data){
			fullKey=(prefix)?prefix+'['+key+']':key;
			if (ret) ret+='&';
			if (typeof(data[key])=='object'){
				ret+=op.urlEncode(data[key], fullKey)
			}
			else {
				ret+=fullKey+'='+encodeURIComponent(data[key]);
			}
		}
		return ret;
	},
	select: function(path, element){
		if (!element) element=document.body;
		var t=element.getElementsByTagName(path)[0];
		if (t){		
			return (t.text)?t.text:t.textContent;
		}
		else return '';
	},	
	animate: function(f, sleep, id){
		if (!op.lock(id)) return false;
		var timeout=null;
		timeout= setInterval(function(){
			if (!f()){
				op.free(id); 
				clearTimeout(timeout);
			}
		}, sleep);
		return true;
	},
	animateRange: function(f, callback, from, to, delta, timeout, id){		
		return op.animate(function(){
			if (from>to) from-=delta;			
			else from+=delta;		
			var ret;
			
			if (from==to||Math.abs(from-to)<delta){
				ret=false;
				from=to;
			}
			else ret=true;
			
			f(from);
			if (!ret){
				if (callback) callback();
			}
			return ret;
		}, timeout, id);
	},
	
	
	create: function (type, id, properties, parent, childNodes){
		var ret= DOM(document.createElement(type));	
		if (id) ret.id=id;
		if (properties){
			for (key in properties){
				if (key=='class'||key=='className'){
					ret.className=properties[key];
				}
				else if (key=='innerHTML'||key=='html'){
					ret.innerHTML=properties[key];
				}
				else if (key.substr(0,2)=='on'){					
					ret.listen(key.substr(2), properties[key]);
				}
				else if (key=='style'){					
					for (k in properties[key]){
						ret[key][k]=properties[key][k];
					}					
				}
				else if (key=='rel'||key=='rev'){
					ret.setAttribute(key, properties[key]);
				}
				else {
					ret[key]=properties[key];
				}
			}
		}	
		
		if (childNodes){			
			for (var i=0; i<childNodes.length; i++){
				ret.appendChild(childNodes[i]);
			}
		}
		if (parent){			
			parent.appendChild(ret);
		}
		return ret;
	},
	location: function(url, data){
		if (window._iframe_callback){
			window._iframe_callback(data);
		}
		document.location.href=url;
	},
	nextID:0,
	getNextID: function(){
		op.nextID++;
		return op.nextID;
	},
	flash: function(display){	
		var objects=(document.getElementsByTagName('object'));
		if (!objects.length) objects=document.embeds;
		for (var i=0; i<objects.length; i++){
			if (this.doNotHideFlash){
				objects[i].style.display=(display=='hidden')?'none':'';
			}
			else if (browser.opera){
				objects[i].style.display=(display=='hidden')?'none':'';
			}
			else {
				objects[i].style.visibility=display;
			}
		}
		
		var objects=(document.getElementsByTagName('select'));
		for (var i=0; i<objects.length; i++){
			objects[i].style.visibility=display;
		}
	},	
	position: {
		//
		rectangle: function(position){
		 
			var screen=op.position.screen();
			
			var ret={
				width:this._toInt(position.width, screen.width),
				height:this._toInt(position.height, screen.height)
			};
			if (position.top=='center'||position.top=='middle'){
				ret.top=screen.top+parseInt((screen.height-position.height)/2);
			}
			else{
				ret.top=screen.top+op.position._toInt(position.top, screen.top);
			}
			if (position.left=='center'||position.left=='middle'){
				ret.left=screen.left+parseInt((screen.width-position.width)/2);
			}
			else {
				ret.left=screen.left+op.position._toInt(position.top, screen.width);
			}	
			
			return ret;
		},
		screen: function(){			
			return {
				top:	document.body.parentNode.scrollTop,
				left:	document.body.parentNode.scrollLeft,
				width:	document.body.parentNode.clientWidth,
				height: document.body.parentNode.clientHeight,
				offsetWidth: document.body.parentNode.clientWidth,
				offsetHeight: Math.max(document.body.clientHeight,(browser.msie)?document.documentElement.clientHeight:window.innerHeight)
			};
		},
		_toInt: function(pos, rel){
			var m;
			if (m=/^(\d+)%$/.exec(pos)){
				return parseInt(rel*m[1]/100);
			}			
			return pos;
		},
		zIndex: 100,
		align:{left:'left',center:'center',right:'right',bottom:'bottom'},
		pos: {
			nearCursor:{left:-1,top:-1}
		},
		move: function(el, to, pos, scroll){
			el.style.position='absolute';
			el.style.zIndex=op.position.zIndex;
			var left= to.pageX;
			var top= to.pageY;
			if (pos.left) left-=pos.left;
			if (pos.top) top-=pos.top;
			if (pos.align){
				switch (pos.align){
					case op.position.align.right:
						left-=el.offsetWidth;
					break;
					case op.position.center:
						left-=Math.ceil(el.offsetWidth/2);
					break;
				}
			}
			if (pos.valign){
				switch (pos.valign){
					case op.position.align.bottom:
						top+=el.offsetHeight;
					break;
					case op.position.align.center:
						top+=Math.ceil(el.offsetHeight/2)
					break;
				}
			}
			if (top<0) top=0;
			if (left<0) left=0;
			
			el.style.top=top+'px';
			el.style.left=left+'px';		
			if (!scroll) scroll={top:false,bottom:false,left:false,right:false};
			if (scroll.top){			
				if (to.pageY<document.body.scrollTop+scroll.top){				
					document.body.scrollTop=document.body.scrollTop-scroll.top;
				}		
				else if (to.pageY>document.body.scrollTop+document.body.clientHeight-scroll.top){				
					document.body.scrollTop=document.body.scrollTop+scroll.top;
				}
				
			}
					
		},	
		calculate: function(element){
			return {
				top:element.offsetTop,
				bottom:element.offsetTop+element.offsetHeight,
				left:element.offsetLeft,
				right:element.offsetLeft+element.offsetHeight
			};
		}
	}
	
};
function setElementOpacity(elem, nOpacity)
{	  
  if (document.body.filters){ // Internet Exploder 5.5+
    opacityProp = 'filter';	
  }
  else{ if (typeof document.body.style.opacity == 'string') // CSS3 compliant (Moz 1.7+, Safari 1.2+, Opera 9, IE7)
    opacityProp = 'opacity';
  else if (typeof document.body.style.MozOpacity == 'string') // Mozilla 1.6 Firefox 0.8 
    opacityProp = 'MozOpacity';
  else if (typeof document.body.style.KhtmlOpacity == 'string') // Konqueror 3.1, Safari 1.1
    opacityProp = 'KhtmlOpacity';
  }
  if (!elem || !opacityProp) return; //
  
  if (opacityProp=="filter")  // Internet Exploder 5.5+
  {
    nOpacity *= 100;
	
    //
    if (elem.filters){
    	var oAlpha = elem.filters['DXImageTransform.Microsoft.alpha'] || elem.filters.alpha;
    }
    else oAlpha=null;
    if (oAlpha) oAlpha.opacity = nOpacity;
    else elem.style.filter += "progid:DXImageTransform.Microsoft.Alpha(opacity="+nOpacity+")";
  }
  else //
    elem.style[opacityProp] = nOpacity;
}


DOMReady(function(){
	if (window._iframe_callback){
		DOM(null).attachEvent('keypress', function(e){
			if (e.keyCode==keyCode.esc) window._iframe_callback();
			return true;
		});
	}
});

var str={
	cut: function(str, length){
		if (str.length<=length) return str;
		else return str.substr(0, length-1)+'&#133;';
	}
};

String.prototype.stripTags= function(){
	return this.replace(/<(.*?)>/,'');
}

String.prototype.cut= function(length){
	if (this.length<=length) return this;
	return this.substr(0, length)+'&#133;';
}

op.dict= function(key, defaultValue){
	return defaultValue;
}

op.loader= function(param){
	
	this.callback=param.callback;
	this.id=(param.id)?param.id:op.getNextID();
	this.div=op.create('div', 'op_loader_container_'+this.id, {className:'loading'}, (param.parent)?param.parent:document.body);
	this.target=op.create((param.type=='img')?'img':'iframe', 'op_loader_'+this.id, {style:{display:'none'}, src:param.src}, this.div);
	var self=this;
	
	this.target.listen('load', function(){			
		self.div.detachClass('loading');
		if (param.callback){
			if (param.callback(self.target)){
				self.target.show();
			}			
		}
		else {
			self.target.show();
		}		
	});
}