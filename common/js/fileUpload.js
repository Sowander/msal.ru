function fileUpload(id){
	this.id=id;
	this.reWin = /.*\\(.*)$/;	
	this.reUnix = /.*\/(.*)/;
	var self=this;
	if (id!=false){
		DOMReady(function(){			
			var list= DOM(id).getElementsByTagName('input');
			for (var i=0; i<list.length; i++){				
				if (list[i].type=='file') self.initInput(DOM(list[i]))
			}
		});
	}
}

fileUpload.prototype.initInput= function(input){
	var self=this;
	input.listen('change', function(e){		
		var value = input.value.replace(self.reWin, '$1').replace(self.reUnix, '$1');		
		var lbl=input.parentNode.getElementsByTagName('label')[0];
		
		if (value){
			lbl.innerHTML=value;
		}
		else {
			lbl.innerHTML=lbl.getAttribute('rel');
		}
		
	});
}