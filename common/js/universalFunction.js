/**
 * Функция создания элемента по заданным параметрам
 * 
 * @type string
 * @param typeE тип элемента
 * 
 * @type string
 * @param idE id который будет у созданного элемента
 * 
 * @type string
 * @param classE класс, который будет присвоен созданному элементу
 * 
 * @return htmlelement
 */
function mas_CreateElem(typeE, idE, classE)
{
  var elem = document.createElement(typeE);
  if(idE != '')
  {
    elem.getAttribute('id');
    elem.id = idE;
  }
  if(classE != '')
  {
    elem.getAttribute('class');
    elem.className = classE;
  }

  document.body.appendChild(elem);
  return elem;
}

/**
 * Функция удалени элемента по id
 * 
 * @type string
 * @param id id удаляемого элемента
 * 
 * @return void
 */
function mas_removeEl(id)
{
  if(document.getElementById(id))
  {
    var remObj = document.getElementById(id);
    document.body.removeChild(remObj);
  }
}
/**
 * Функция нахождения координат заданного элемента на странице
 * 
 * @type htmlelement
 * @param elem элемент положение которого надо найти
 * 
 * @return object
 */
function getOffset(elem) 
{
  if (elem.getBoundingClientRect) 
  {
    return getOffsetRect(elem)
  } 
  else 
  {
    return getOffsetSum(elem)
  }
}

/**
 * Функция нахождения координат заданного элемента на странице
 * 
 * @type htmlelement
 * @param elem элемент положение которого надо найти
 * 
 * @return object
 */
function getOffsetSum(elem) 
{
  var top=0, left=0
  while(elem) 
  {
    top = top + parseInt(elem.offsetTop)
    left = left + parseInt(elem.offsetLeft)
    elem = elem.offsetParent
  }
  return {top: top, left: left}
}

/**
 * Функция нахождения координат заданного элемента на странице
 * 
 * @type htmlelement
 * @param elem элемент положение которого надо найти
 * 
 * @return object
 */
function getOffsetRect(elem) 
{
    var box = elem.getBoundingClientRect()

    var body = document.body
    var docElem = document.documentElement

    var scrollTop = window.pageYOffset || docElem.scrollTop || body.scrollTop
    var scrollLeft = window.pageXOffset || docElem.scrollLeft || body.scrollLeft

    var clientTop = docElem.clientTop || body.clientTop || 0
    var clientLeft = docElem.clientLeft || body.clientLeft || 0

    var top  = box.top +  scrollTop - clientTop
    var left = box.left + scrollLeft - clientLeft

    return {top: Math.round(top), left: Math.round(left)}
}
