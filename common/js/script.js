/* Author:

*/

$(function(){
		
		
	if ( $('.gallery').length > 0) {
		$('.gallery ul').jcarousel(
			{
				wrap:'circular',
				scroll: 1
			}
	
		);
		$('.gallery a').fancybox();
	}
	
	$('div.collapse .teaser').hide();
	
	$('div.collapse .tickedIn a').click(function(){
		$(this).parent().next().slideToggle();
		return false;
	});
	
	
	$('ul.collapse .tickedIn a').click(function(){
		var $teaser = $(this).parent().next();
		$teaser.slideToggle();
		/* 
		if($teaser.children().length && $teaser.children().get(0).tagName=='OL'){
			$teaser.children().eq(0).slideToggle();
		}else{
			
			$teaser.slideToggle();
		} */
		return false;
	});
	
		
	$('#sItems li').fadeTo(0,.0);
	if($('#slider').length > 0) {
	$('#slider').ant1ucem_slider({item:'li.slide', method: 'fade', 
		onSlide: function(d){
			
			$('#sControls li').removeClass('active').eq(d.currentSlide).addClass('active');
		},
		extra:function(d){
			d.slideTo(0);
			$('#sControls li').click(function(){
				d.slideTo($(this).index());
			})
		}
	})
	}

	

	var d = document,
		elClasses = '.cols3 li',
		index = getEqualHeight(elClasses);
	autoSliderHeight(d.getElementById('navSeconadry'), d.getElementById('slider'), d.getElementById('sItems'));
	
	window.onresize = function() {
			setEqualHeight(elClasses, index);
		}
	//console.log(index);
	
	if ($.browser.msie && $.browser.version=="6.0") {
		
		$.getScript('/common/js/DD_belatedPNG-0.0.8a.min.js', function() {
			DD_belatedPNG.fix('img');
		});
	}
	
	$('div.navSub h2.folder:not(.active)').next().hide();
	$('div.navSub h2.folder').click(function(){
		$('div.navSub h2.folder:not(.active)').next().slideUp();
		$(this).next().stop().slideToggle();
		return false;
	});
	
	//раскрывание бокового меню
	$(".navBox li.more a").click(function(){
		console.log($(this).attr('rel'));
		if ($(this).attr('rel')) {
			$('li.' + $(this).attr('rel')).show();
		}
		$(this).parent('li.more').hide(); 
		return false;
	})

})


function autoSliderHeight(navSecondary,bxSlider, ulSlider) {
	if (!bxSlider) return false;
	
	var bxHeight = Math.max((navSecondary.offsetHeight - 0), (bxSlider.offsetHeight - 0));
		tmp1 = bxSlider.childNodes[0],
		tmp2 = bxSlider.childNodes[1],
		navPaddingH = (parseInt(getElementComputedStyle(navSecondary,'padding-top'))) + (parseInt(getElementComputedStyle(navSecondary,'padding-bottom'))),
		navMarginH = 0;
	
	if ($.browser.msie && $.browser.version=="6.0") {
		navMarginH = parseInt(navSecondary.currentStyle.marginTop) + parseInt(navSecondary.currentStyle.marginBottom);
	}
	
	var bxGradient = (document.getElementsByClassName == undefined) ? tmp2 : bxSlider.getElementsByClassName('module')[0];
	
	bxSlider.style.height = (bxHeight - navMarginH) + 'px';
	bxGradient.style.height = (bxHeight - navMarginH) + 'px';
	navSecondary.style.height = (bxHeight-navPaddingH - navMarginH) + 'px';

	/* box and items widths normalization */

}
 
function getEqualHeight(elems) {
	
	if (typeof elems !="object") {
		var el =  $(elems).get(0);
		
	}
	
	if(el===undefined) return;
	
	var bxHeight = el.offsetHeight,
		maxHeight = 0,
		flag = 0,
		i = 0;
	
	while(el = el.nextSibling){
		if (el.nodeType === 1) {
			i += 1;
			bxHeight = el.offsetHeight;
			maxHeight = Math.max(bxHeight, maxHeight);
			flag = (bxHeight === maxHeight) ? i : 0; 
		}
	}
	$(elems + ':not(:eq(' + i + '))').children('div.wrap').css('height',((maxHeight-0) + 'px'));
	/*if (i !== 0) {
		$(elems + ':eq(' + i + ')').children('div.wrap').css('height','auto');
	}*/
	return i;
}

function setEqualHeight(elems, index) {
	var maxHeight = $(elems + ':eq(' + index + ')').height() - 2; // 2px is a border;
	$(elems + ':not(:eq(' + index + '))').children('div.wrap').css('height',((maxHeight-0) + 'px'));
}

function getElementComputedStyle(elem, prop)
{
  if (typeof elem!="object") elem = document.getElementById(elem);
  
   if (document.defaultView && document.defaultView.getComputedStyle)
  {
    if (prop.match(/[A-Z]/)) prop = prop.replace(/([A-Z])/g, "-$1").toLowerCase();
    return document.defaultView.getComputedStyle(elem, "").getPropertyValue(prop);
  }
  
  if (elem.currentStyle)
  {
    var i;
    while ((i=prop.indexOf("-"))!=-1) prop = prop.substr(0, i) + prop.substr(i+1,1).toUpperCase() + prop.substr(i+2);
    return elem.currentStyle[prop];
  }
  return "";
}
