function dndOrder(id, skipInit){
	this.id= id;
	this.draggable=null;
	this.lastEvent=null;
	if (!skipInit){
		var self=this;	
		DOMReady(function(){
			self.init();		
		});
	}
}

dndOrder.prototype.init= function(){
	this.ul=DOM(this.id);
	this.li=[];
	
	var childNodes= op.list.childNodes(this.ul);
	this.total=childNodes.length;
	for (i=0; i<this.total; i++){
		this.initItem(childNodes[i]);			
	}
	this.total=this.li.length;
	var self=this;
	DOM(null).listen('mousemove',function(e){
		if (self.dnd){			
			op.position.move(self.getDraggable(), e, op.position.pos.nearCursor, {top:20});
		}
	});
}

dndOrder.prototype.initItem= function(li){	
	var self=this;
	
	li.controls= op.list.groupByClass(li.getElementsByTagName('a'));
	op.list.listenAssoc(li.controls, {
		up: function(e){self.move(li, li.getOrderNumber()-1);},
		down: function(e){self.move(li, li.getOrderNumber()+1);},
		first: function(e){self.move(li, 0);}
	});
	
	li.controls.refresh= function(){
		if (li.getOrderNumber()==0){
			if (li.controls.up) li.controls.up.attachClass('disabled');
			if (li.controls.first) li.controls.first.attachClass('disabled');
		}
		else {
			if (li.controls.up) li.controls.up.detachClass('disabled');
			if (li.controls.first) li.controls.first.detachClass('disabled');
		}
		if (li.getOrderNumber()==self.total-1){
			if (li.controls.down) li.controls.down.attachClass('disabled');
		}
		else {
			if (li.controls.down) li.controls.down.detachClass('disabled');
		}
	};
	
	li.inputs= op.list.groupByClass(li.getElementsByTagName('input'));
	var onChange=function(e){
		if (!op.str.isNumeric(li.inputs.order.value)){
			li.input.value=li.getOrderNumber()+1;
			return false;
		}
		if (!self.move(li, li.inputs.order.value-1)){
			li.setOrderNumber(li.getOrderNumber());
		}
	}
	if (li.inputs.set){
		li.inputs.setSet.listen('click', onChange);
	}
	else if (li.inputs.order){
		li.inputs.order.listen('change', onChange);
	}		
	li.getOrderNumber=function(){
		return li.orderNumber;
	};
	li.setOrderNumber=function(value){
		li.orderNumber=value;
		li.inputs.order.value=value+1; 
		li.controls.refresh();
	};
	li.setOrderNumber(this.li.length);
	li.listen('mousedown', function(e){
		t=e.getTarget();		
		if (t&&(t.tagName=='INPUT'||t.tagName=='A'||t.tagName=='LABEL')){			
			return true;		
		}
		self.dnd=li;
		li.attachClass('moved');		
		op.position.move(self.getDraggable(li), e, op.position.pos.nearCursor);		
	});
	li.listen('mouseup',function(){		
		if (self.dnd){			
			self.move(self.dnd, li.getOrderNumber());
			self.stopDragging();
		}		
	});
	DOM(null).listen('mouseup', function(){		
		self.stopDragging();
	});
	this.li[li.getOrderNumber()]=li;	
}

dndOrder.prototype.stopDragging= function(){
	if (this.dnd){
		this.getDraggable().hide();
		this.getDraggable().innerHTML='';
		this.dnd.detachClass('moved');
		this.dnd=null;
	}
}

dndOrder.prototype.getDraggable= function(li){	
	if (!this.draggable){				
		this.draggable=op.create('li', this.id+'draggable', {className:'draggable'}, this.ul);		
	}
	
	if (li){		
		this.draggable.innerHTML=li.innerHTML;
		this.draggable.style.display='block';
	}	
	return this.draggable;
}

dndOrder.prototype.move= function(element, position){
	var _position=element.getOrderNumber();
	
	if (_position==position) return false;
	if (position>=this.li.length) return false;
	
	var elements=[];
	for (i=0; i<Math.min(_position,position);i++){
		this.li[i].setOrderNumber(i);
		elements[elements.length]=this.li[i];
	}
	var max=0;
	var tail_i=0;
	
	if (_position>position){
		element.setOrderNumber(elements.length);
		elements[elements.length]=element;
		for (i=position; i<_position; i++){
			this.li[i].setOrderNumber(elements.length);
			elements[elements.length]=this.li[i];
		}
		tail_i=_position+1;
	}
	else{
		
		for (i=_position+1; i<=position; i++){
			this.li[i].setOrderNumber(elements.length);
			elements[elements.length]=this.li[i];			
		}
		element.setOrderNumber(elements.length);
		elements[elements.length]=element;
		tail_i=position+1;
	}
	for (i=tail_i; i<this.li.length; i++){
		this.li[i].setOrderNumber(elements.length);
		elements[elements.length]=this.li[i];
	}
	
	this.li=elements;
	if (this.li[position+1]){
		this.ul.insertBefore(element, this.li[position+1]);
	}
	else {
		this.ul.appendChild(element);
	}
}