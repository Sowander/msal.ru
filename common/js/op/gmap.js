op.gmap= function(div, options){
	this.div=div;
	this.markers=[];
	//options:
	//this.markerOptions= new GInfoWindowOptions();	
	this.b= new GLatLngBounds();	
}

op.gmap.prototype.addMarker= function(li, icon){	
	var coord= li.getAttribute('rel').split(',');
	if (coord[0]=='0'&&coord[1]=='0') return 0;
	return this.addPoint(new GLatLng(coord[0], coord[1]),li, icon);
}
op.gmap.prototype.addPoint= function(point, li, icon){
	this.b.extend(point);
	var marker= new GMarker(point, icon);
	if (li) marker.bindInfoWindow(li);//, this.markerOptions);
	this.markers[this.markers.length]=marker;	
	return 1;
}

op.gmap.prototype.display= function(center, zoom){
	this.map= new GMap2(this.div);
	this.map.enableDoubleClickZoom();
	this.map.enableScrollWheelZoom();
	this.map.addControl(new GSmallMapControl());
	//1. zoom it:
	if (!center) center=this.b.getCenter();
	if (!zoom) zoom=this.map.getBoundsZoomLevel(this.b);
	this.map.setCenter(center, zoom);
	for (var i=0; i<this.markers.length; i++){
		this.map.addOverlay(this.markers[i]);
	}
	
}

op.gmap.gui= function(mapID, lists, icons){
	
	DOMReady(function(){
		var map=DOM(mapID);
		//calculate map size:
		if (!map.style.width){
			map.style.width='100%';	
			map.style.height=Math.ceil(parseInt(map.clientWidth)*0.75)+'px';
		}
		//get list:
		
		var gm=new op.gmap(map);
		var total=0;
		for (var j=0; j<lists.length; j++){				
			var ul=DOM(lists[j]);
			if (ul){	
				var li=ul.getChildNodes();			
				for (var i=0; i<li.length; i++){
					var icon=null;
					if (li[i].getAttribute('rel')){
						if (icons[li[i].className]) icon=icons[li[i].className];
						total+=gm.addMarker(li[i],icon);					
					}
				}
			}
		}
		
		if (total>0){		
			gm.display();		
			DOM('lbl').show();			
		}
		else {
			DOM(mapID).hide();
		}
//alert(total);
		return total;
	});
	
}

op.gmap.displayItem= function(div, item, icon){
	if (item.point[0]=='0'&&item.point[1]=='0') return;
	DOMReady(function(){
		var gmap= new op.gmap(DOM(div));
		gmap.addPoint(new GLatLng(item.point[0], item.point[1]), null, icon);	
		gmap.display(new GLatLng(item.center[0], item.center[1]), item.zoom);
//		var marker= new GMarker(new GLatLng(item.point[0], item.point[1]));
//		gmap.map.setCenter(new GLatLng(item.center[0], item.center[1]), item.zoom);
//		gmap.map.addOverlay(marker);
	});
}
