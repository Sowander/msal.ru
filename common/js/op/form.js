op.form= function(id){
	this.id=id;
	this.errors=[];
	op.form.forms[id]=this;
	var self=this;
	DOMReady(function(){self.init();});
}

op.form.forms=[];

op.form.prototype.init= function(){
	this.form=DOM(this.id);
	var inputs=this.form.getElementsByTagName('input');
	for (var i=inputs.length-1; i>=0; i--){
		if (inputs[i].type=='submit') this.submit=inputs[i];
	}
}

op.form.getForm= function(input){
	return op.form.forms[input.form.id];
}

op.form.input= function(id, handlers, currentError){
	this.id=id;
	this.firstCall=true;	
	var self=this;
	DOMReady(function(){
		self.input=DOM(id);
		self.form=op.form.getForm(self.input);
		self.throwError= function(code){
			self.form.throwError(self, code);
		}
		if (currentError){
			self.form.throwError(self, currentError);
			self.firstCall=false;
		}
		self.setValid=function(){
			self.form.setValid(self);
		}		
		//listen while print:
		if (handlers.keyup){
			self.input.listen('keyup', function(e){handlers.keyup(self, self.input);});
		}
		//listen when changed:
		if (handlers.change){
			self.input.listen('change',function(e){handlers.change(self, self.input);});
			//self.input.listen('focus',function(e){handlers.change(self, self.input);});
			handlers.change(self, self.input);				
		}
		self.firstCall=false;
	});
}

op.form.fieldset= function(id, handlers){
	this.id=id;
	var self=this;
	DOMReady(function(){
		//load fieldset:
		self.fieldset=DOM(id);
		//load inputs:
		self.inputs=self.fieldset.getElementsByTagName('input');
		//throw error:
		self.throwError= function(code){
			self.form.throwError(self, code);
		}
		this.firstCall=true;
		for (var i=0; i<self.inputs.length; i++){
			//foreach inputs in fieldset attach listeners:
			if (!self.form) self.form=DOM(self.inputs[i]);
			//listen while print:
			if (handlers.keyup){
				self.inputs[i].listen('keyup', function(e){handlers.keyup(self, inputs[i]);});
			}
			//listen when changed:
			if (handlers.change){
				self.inputs[i].listen('change',function(e){handlers.change(self, inputs[i]);});
				self.inputs[i].listen('focus',function(e){handlers.change(self, inputs[i]);});
			}			
		}	
		if (handler.onchange) handler.onchange(self);
		self.firstCall=false;
	});
}

op.form.prototype.throwError= function(input, code){	
	if (code!=null){
		//disable last error:
		if (this.errors[input.id]&&this.errors[input.id].hide){
			this.errors[input.id].hide();
		}
		//get new error:
		var errorMessage=DOM('error_'+input.id+'_'+code);
		if(!errorMessage) errorMessage=DOM('error_'+input.id);
		if(!errorMessage){
			errorMessage=true;
		}
		else{
			//display error:
			errorMessage.show();
		}
		//remember error:		
		this.errors[input.id]=errorMessage;
		//mark as invalid:
		if (input.fieldset){ 			
			input.fieldset.attachClass('invalid');
		}
		else{			
			DOM(input.input.parentNode).attachClass('invalid');
		}
	}
	else {
		this.errors[input.id]=true;
	}
	if (this.submit) this.submit.disabled=true;
}

op.form.prototype.setValid= function(input){
	//hide error:
	if (this.errors[input.id]&&this.errors[input.id].hide){
		this.errors[input.id].hide();
	}
	//remove current error:
	this.errors[input.id]=false;
	//unmark input:
	if (input.fieldset){ 
		input.fieldset.detachClass('invalid');
	}
	else{		
		DOM(input.input.parentNode).detachClass('invalid');
	}
	//refresh:	
	for (key in this.errors){
		if (this.errors[key]) return;
	}
	//enable submit:
	if (this.submit) this.submit.disabled=false;
}

op.form.nonempty= function(id, preg, currentError){
	var validate= function(form, input){
		if (!input.value.length){			
			return form.throwError((form.firstCall)?null:'empty');
		}
		if (preg){
			if (!preg.test(input.value)){
				return form.throwError('invalid');
			}
		}
		form.setValid();
	}
	input= new op.form.input(id,{change:validate,keyup:validate},currentError);
}
