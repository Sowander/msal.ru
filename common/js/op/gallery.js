op.gallery= function(){
	
}

op.gallery.settings=[];
op.gallery.once=[];
op.gallery.find= function(settings){
	op.gallery.once[settings.id]=true;
	
	var className;
	if (settings.className){
		className=new RegExp('\b'+settings.className+'\b');
	}
	else {
		className=/\bmedialib\b/;
	}
	DOMReady(function(){
		if (!op.gallery.once[settings.id]) return;
		op.gallery.once[settings.id]=false;
		var source=(settings.id)?DOM(settings.id):document.body;
		
		var list=source.getElementsByTagName((settings.tagName)?settings.tagName:'div');	
		for (i=0; i<list.length; i++){
			
			if (className.test(list[i].className)){					
				//check view mode:
			var galleryInfo=list[i].className.split(/\s+/);				
				if (op.gallery[galleryInfo[1]]){						
					var ctrl= new op.gallery[galleryInfo[1]](list[i]);
				}
			}
		}
		
	});
}

op.gallery.popup= function(id, settings){	
	this.links=[];
	this.alt=[]
	this.el=DOM(id);
	this.settings=(settings)?settings:[];
	var a=this.el.getElementsByTagName('a');	
	for (var i=0; i<a.length; i++){		
		if (this.settings.hrefAttribute){
			this.addLink(a[i].getAttribute(this.settings.hrefAttribute));
		}
		else {
			this.addLink(a.href)
		}	
		this.initLink(DOM(a[i]));
	}		
	
}

op.gallery.popup.prototype.initLink= function(a){
	var self=this;	
	a.listen('click', function(){
		if (self.settings.hrefAttribute){
			self.open(a.getAttribute(self.settings.hrefAttribute));
		}
		else {
			self.open(a.href)
		}		
	});
}

op.gallery.popup.prototype.addLink= function(href){
	
	for (var i=0; i<this.links.length; i++){		
		if (href==this.links[i]) return;
	}	
	index=this.links.length;
	this.links[index]=href;	
	return index;
}

op.gallery.popup.prototype.open= function(src){
	var self=this;
	var justOpened= op.layer.safeOpen({
		settings:{opacity: 0.8},
		onclose: function(){
			self.image.hide();
			self.layer.style.width='';
			self.layer.style.height='';
		}
	});	
	var self=this;
	if (!this.layer){
		this.layer=this.el.create({tagName:'div',className:'loading gallery_layer'});
		this.layer.hide();
		this.image=this.layer.create({
			tagName:'img', 
			onclick:function(){
				self.next(self.image.xsrc);
			},
			onload: function(){
				
				self.layer.detachClass('loading');
				self.layer.style.width=self.image.width;
				self.layer.style.height=self.image.height;
					
				var s=self.settings;
				if (self.settings.width){
					s.width=self.settings.width;
					s.height=self.settings.height;
				}
				else {
					self.image.style.display='';
					s.width=self.image.width;
					s.height=self.image.height;
				}
				s.align='center';
				s.valign='center';
				op.layer.resize(s);
				self.image.style.display='';
				
			},
			style:{display:'none'}
		});		
	}
	this.next= function(href){		
		for (var i=0; i<this.links.length; i++){		
			if (href==this.links[i]) break;
		}		
		if (this.links[i+1]) this.open(this.links[i+1]);
		else op.layer.close();
	}
	this.layer.attachClass('loading');
	if (justOpened){				
		op.layer.add(this.layer);
		this.layer.show();				
		op.layer.resize({width:400, height:400});
	}
	this.image.xsrc=src;
	this.image.src=src;
}


op.gallery.gallery =function(el, settings){
	this.settings=settings;
	this.addLink=op.gallery.popup.prototype.addLink;
	this.links=[];
	this.open=op.gallery.popup.prototype.open;
	this.el=DOM(el);
	//init popup links:
	this.popupLinks=[];
	var obj=this.el.getChildNodes({groupBy:'tagName'});	
		
	var a=obj.DL.getElementsByTagName('a');
	
	
	this.dt=DOM(obj.DL.getElementsByTagName('dt')[0]);
	this.dt.a=this.dt.getChildNodes()[0];
	
	this.initImage(DOM(obj.DL.getElementsByTagName('img')[0]));
	for (var i=0; i<a.length; i++){		
		this.popupLink(DOM(a[i]));
	}
	//ul:
	var li=obj.UL.getElementsByTagName('li');
	var img=null;
	var a=null;
	for (var i=0; i<li.length; i++){
		img=li[i].getElementsByTagName('img')[0];
		a=li[i].getElementsByTagName('a');
		for (var j=0; j<a.length; j++){		
			if (/\bcurrent\b/.test(li[i].className)){
				this.currentLi=DOM(li[i]);
			}
			this.lblLink(DOM(a[j]),img, DOM(li[i]));
		}
	}
}

op.gallery.gallery.prototype.initImage= function(img){
	this.img=img;
	
	var self=this;
	this.img.counter=0;
	this.img.firstLoad=true;
	this.img.locked=false;
	
	this.img.listen('load', function(){
		self.dt.style.width=Math.max(self.img.width, self.dt.a.clientWidth);
		self.displayTitle();		
		self.hideDt(2000);
	});
	this.img.listen('mouseover', function(){	
		self.displayTitle();
	});
	this.img.listen('mouseout', function(){
		self.hideDt(500);
	});	
	this.dt.listen('mouseout', function(){
		self.hideDt(500);
	})	
	this.dt.listen('mouseover', function(){
		self.displayTitle();
	})
	
}

op.gallery.gallery.prototype.displayTitle= function(){
	if (!this.img.alt){
		this.dt.hide();
		return;
	}
	this.dt.a.innerHTML=this.img.alt;
	if (this.timeout){
		clearTimeout(this.timeout);
		this.timeout=null;
		return;
	}
	setElementOpacity(this.dt,0.8);
	this.dt.show();
}

op.gallery.gallery.prototype.hideDt= function(timeout){
	var self=this;
	self.timeout= setTimeout(
		function(){
			self.dt.hide();
			self.timeout=null;
		},timeout);	
}

op.gallery.gallery.prototype.popupLink= function(a){
	this.addLink(a.href);
	var self=this;
	
	a.listen('click', function(){		
		self.open(a.href);
	});
	this.popupLinks[this.popupLinks.length]=a;
}

op.gallery.gallery.prototype.lblLink= function(a, img, li){
	var self=this;
	this.addLink(a.href);
	a.listen('click', function(e){				
		self.img.alt=img.alt;
		if (self.currentLi) self.currentLi.detachClass('current');
		self.currentLi=li;
		li.attachClass('current');
		for (var i=0; i<self.popupLinks.length; i++){
			self.popupLinks[i].href=a.href;
		}
		self.img.src=a.getAttribute('rel');
	});
}