op.layer= {
	fader:null,
	div:null,	
	loading:null,
	iframe:null,
	
	isOpened:false,
	lastID:null,
	counter:0,
	
	defaultSettings:{
		width:'50%',
		height:'50%',
		top:'center',
		left:'center',
		heightSpeed:{step:20, timeout:2},		
		opacity:0.8,
		opacitySpeed:10		
	},
	callback: function(data, url){
		
		if (window._iframe_callback){
			window._iframe_callback(data);
			return false;
		}
		else if(url){
			if (window.opener){
				window.opener.location.href=url;
				return false;
			}						
		}
		return true;
	},
	clearHash: function(){
		document.location.hash='';
	},
	displayIframe: function(src, callback, param, onClose){
		param=op.layer.getParam(param);
		op.layer.openLayer(param, src, onClose);
		op.layer.iframe=op.create('iframe', 'system_layer_iframe_'+op.layer.counter,{			
			src:src,
			onload:function(){				
				if (op.layer.lastID!=src) return;				
				op.layer.loading.hide();
				if (op.layer.firstLoad){
					op.layer.firstLoad=false;
					op.layer._positionateDiv(param);					
				}				
				op.layer.iframe.contentWindow._iframe_callback= function(data){					
					if (callback){
						callback(data);
					}
					op.layer.hide();
				}
				//listen ctrl+->			
				if(op.layer.iframe.contentWindow.op.layer.navNext){
					DOM(null).listen('keypress', function(e){
						if (e.ctrlKey&&e.keyCode==keyCode.right){
							op.layer.iframe.contentWindow.op.layer.navNext();
						}
						return true;
					});
					op.lock('ctrl.'+keyCode.right);
				}
			}
		}, op.layer.div);		
		return false;
	},
	openLayer: function(param, id, onClose){
		op.layer._init();		
		//refresh params:		
		
		op.layer._refresh(id);
		op.layer.isOpened=true;		
		op.layer._displayFader(param);						
		op.layer.counter++;
		op.layer.firstLoad=true;
		op.layer.onClose=onClose;
	},	
	getParam: function(param){
		if (!param) param=[];
		for (key in op.layer.defaultSettings){
			if (!param[key]) param[key]=op.layer.defaultSettings[key];
		}
		return param;
	},
	_refresh: function(id){
		op.layer.lastID=id;
		while (op.layer.div.firstChild){			
			op.layer.div.removeChild(op.layer.div.firstChild);
		}
		op.layer.div.hide();		
	},	
	_positionateDiv:function(param, callback){		
		if (perc=/^(\d+)%$/.exec(param.width)){
			width=Math.ceil(document.body.clientWidth*parseInt(perc[1])/100);
		}
		else {
			width=parseInt(param.width);
		}
		if (perc=/^(\d+)%$/.exec(param.height)){
			height=Math.ceil(document.body.clientHeight*parseInt(perc[1])/100);
		}
		else {
			height=parseInt(param.height);
		}
		
		op.layer.div.style.overflow='hidden';
		if (op.layer.iframe){
			op.layer.iframe.style.width=width+'px';
			op.layer.iframe.style.height=height+'px';
		}
		
		var scrollTop=document.documentElement.scrollTop;
		var scrollLeft=document.documentElement.scrollLeft;
		
		var top=(param.top=='center')?Math.ceil((document.body.clientHeight-height)/2):param.top;
		top+=scrollTop;
		var left=(param.left=='center')?Math.ceil((document.body.clientWidth-width)/2):param.left+scrollLeft;
		left+=scrollLeft;
						
		op.layer.div.style.top=top+'px';
		op.layer.div.style.left=left+'px';
		op.layer.div.style.width=width+'px';
		
		var startHeight=(op.layer.firstLoad)?0:op.layer.div.clientHeight;
		op.layer.div.style.height=startHeight+'px';
		
		if (param.heightSpeed){
			//op.layer.div.style.height='0px';
			if (op.layer.iframe){
				op.layer.iframe.style.overflow='hidden';
			}
			
			op.animateRange(function(v){op.layer.div.style.height=v+'px';}, function(){
				if (op.layer.iframe) op.layer.iframe.style.overflow='';
				if (callback) callback();
			}, startHeight, height, param.heightSpeed.step, param.heightSpeed.timeout, 'system_layer_div');
		}
		else {
			op.layer.div.style.height=height+'px';
		}
		op.layer.div.show();
		
	},
	_init:function(){
		if (op.layer.div) return;		
		op.layer.div=op.create('div','system_layer_div',{
			className:	'layer', 
			style:		{position:'absolute', zIndex:200}			
		},
		document.body);
				
		
		op.layer.loading=op.create('div','system_layer_loading',{className:'loading'},document.body);
		
		var _=DOM(null);
		_.listen('resize',op.layer.onResize);		
		_.listen('keypress',function(e){
			if (e.keyCode==keyCode.esc){
				op.layer.hide();
				return false;
			}
			return true;
		});
		
		op.layer.fader=op.create('div','system_layer_fader',{		
			className:'fader',
			onclick:function(e){if (e.getTarget()==op.layer.fader) op.layer.hide();}
		},document.body);		
		op.layer.fader.style.position='absolute';
		op.layer.fader.style.zIndex=100;
		
	},
	hide:function(){
		op.layer.isOpened=false;
		op.layer._hideFader();
		op.layer.div.hide();
		op.free('ctrl.'+keyCode.right);
		if (op.layer.onClose){
			op.layer.onClose();
			op.layer.onClose=null;
		}		
	},
	onResize:function(){
		if (!op.layer.isOpened) return;
		op.layer._positionateFader();
	},
	/*fader:*/
	_lastBodyOverflow:null,
	_lastDisplayedElements:[],
	_displayFader: function(param){										
		//hide SELECTs and OBJECTs:
		document.body.style.overflow='hidden';
		op.layer._lastDisplayedElements=[];
		var topElements;
		topElements=document.getElementsByTagName('select');
		for (i=0; i<topElements.length; i++){
			if (topElements[i].style.display!='none'){
				
				op.layer._lastDisplayedElements[op.layer._lastDisplayedElements.length]={
					object:topElements[i],
					display:topElements[i].style.display
				}
				topElements[i].style.display='none';
			}
		}
		topElements=document.getElementsByTagName('object');
		for (i=0; i<topElements.length; i++){
			if (topElements[i].style.display!='none'){
				topElements[i].style.display='none';
				op.layer._lastDisplayedElements[op.layer._lastDisplayedElements.length]=topElements[i];
			}
		}
		op.layer._positionateFader();
		
		if (param.opacitySpeed){
			setElementOpacity(op.layer.fader, 0);			
			op.animateRange(
				function(value){
					setElementOpacity(op.layer.fader, value);
				}, 
				null,
				0, param.opacity, 0.1, param.opacitySpeed, 'system_layer_fader');
		}
		else {
			setElementOpacity(op.layer.fader, param.opacity);
		}
		op.layer.fader.show();
	},
	_positionateFader: function(){		
		
		op.layer.fader.style.top=document.documentElement.scrollTop+'px';
		op.layer.fader.style.width=document.body.clientWidth+'px';
		op.layer.fader.style.height=document.body.clientHeight+'px'	
		
		
	},
	_hideFader: function(){
		document.body.style.overflow='';
		op.layer._lastBodyOverflow=null;		
		for (i=0; i<op.layer._lastDisplayedElements.length; i++){
			op.layer._lastDisplayedElements[i].object.style.display=op.layer._lastDisplayedElements[i].display;
		}
		op.free('system_layer_fader');
		op.layer.fader.hide();
	}
}