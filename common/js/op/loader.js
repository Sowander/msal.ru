op.loader= function(param){
		
		this.callback=param.callback;
		this.id=(param.id)?param.id:op.getNextID();
		this.div=op.create('div', 'op_loader_container_'+this.id, {className:'loading'}, (param.parent)?param.parent:document.body);
		this.target=op.create((param.type=='img')?'img':'iframe', 'op_loader_'+this.id, {style:{display:'none'}, src:param.src}, this.div);
		var self=this;
		
		this.target.listen('load', function(){			
			self.div.detachClass('loading');
			if (param.callback){
				if (param.callback(self.target)){
					self.target.show();
				}			
			}
			else {
				self.target.show();
			}		
		});
}

op.loader.prototype.reload= function(){
	
}