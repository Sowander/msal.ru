op.window= function(param){
	if (!param.id) param.id=op.getNextID();
	this.id=param.id;
	var self=this;
			
	this.window=op.create(
		'div', 
		'op_window_'+param.id, 
		{
			className:'op_window',			
			style:{zIndex:op.window.zIndex++, display:'none'},			
		},	document.body
	);
	
	this.nav=this.window.create({
		tagName:'div',
		className:'panel',
		onmousedown: function(e){
			self.focus();
			self.drag={
				left: parseInt(self.window.style.left)-e.pageX,
				top:  parseInt(self.window.style.top)-e.pageY
			}
		},
		onmousemove: function(e){
			if (!self.drag) return;
			console.dir(self.drag);
			left=self.drag.left+e.pageX;
			top=self.drag.top+e.pageY;
			self.window.style.left=left+'px';
			self.window.style.top=top+'px';
		},
		onmouseup: function(e){
			self.drag=null;
		}
	});
	this.nav.create({tagName:'h3',innerHTML:param.title});
	this.nav.create({tagName:'a', className:'close', innerHTML:'close',onclick:function(){self.close();}});
	
	if (param.type=='div'){
		this.window.appendChild(param.div);
		this.show(param.div);
	}
	else if (param.type='html'){		
		this.show(this.window.create({tagName:'div',innerHTML:param.html}));
	}
}

op.window.prototype.show=function(el){
	if (!op.window.counter) op.flash('hidden');
	
	el.show();
	//this.window.style.visibility='hidden';
	this.window.style.display='block';
	this.window.style.position='absolute';		
	var pos=op.position.rectangle({width:el.clientWidth, height:el.clientHeight, top:'center',left:'center'});
	pos.left+=op.window.settings.left*op.window.counter;
	pos.top	+=op.window.settings.top*op.window.counter;
	op.window.counter++;
	for (key in pos) this.window.style[key]=pos[key]+'px';
	this.window.style.visibility='visible';
	
}

op.window.prototype.focus= function(){
	if (op.window.current!=this.id){
		this.window.style.zIndex=op.window.zIndex++;
		op.window.current=this.id;
	}
}

op.window.prototype.close= function(){
	this.window.hide();
	op.window.counter-=1;
	if (!op.window.counter){
		op.flash('visible');
	}
}

op.window.settings={left:20, top:20};

op.window.counter=0;
op.window.zIndex=100;

op.window.init= function(){
	
}
