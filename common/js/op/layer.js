op.layer={
	settings:{
		width:'50%',
		height:'50%',
		top:'center',
		left:'center',
		animateHeight:null,
		animateWidth:null,
		opacity:{opacity:0.8, start:0, delta:0.1, timeout:100},
		loadingHeight:100,
		positionateClose:true,
		displayFader:true
	},
	displayIframe: function(src, callback, settings, onready){
		op.layer.open({callback:callback,settings:settings,id:src});
		op.layer.loading(settings);
		var iframe=op.create('iframe', src,{
			onload:function(){
				op.layer.loaded();				
				if (iframe.justLoaded) return;
				iframe.justLoaded=true;
				try {
					op.layer.iframe_callback= function(data){					
						if (callback){
							callback(data);
						}					
						op.layer.close();
					};					
				}
				catch (e){
					
				}
				op.layer.resize(settings, function(){					
					iframe.style.width=op.layer.div.style.width;
					iframe.style.height=op.layer.div.style.height;					
					op.layer.add(iframe);
					iframe.show();					
					if (onready) onready();
				})
			},
			onkeypress:function(e){				
				if (e.keyCode==keyCode.esc){
					op.layer.close();
				}
				return true;
			},
			src:src,
			style:{display:'none'}
		}, document.body);										
	},
	loading: function(s){
		if (!op.layer._loading){
			op.layer._loading= op.create('div','op_layer_loading',{className:'loading',style:{height:op.layer.settings.loadingHeight}});
		}
		var settings={
			width:800,
			height:op.layer.settings.loadingHeight,
			animateHeight:false,
			animateWidth:false
		};		
		op.layer.resize(settings);
		op.layer.add(op.layer._loading);
	},
	loaded: function(){
		if (op.layer._loading) op.layer._loading.hide();
	},
	open: function(arg){
		op.layer.closeLock=true;		
		op.layer.close();
		op.layer.id=(arg.id)?arg.id:'op_layer';		
		op.layer.onclose=arg.onclose;		
		op.layer._init();
		settings=(arg.settings)?arg.settings:this.settings;
		if (this.settings.displayFader){
			op.layer.fader.show(settings.opacity, arg.onready);
		}
		else if (arg.onready) {
			arg.onready();
		}
		if (this.closeMessage&&this.settings.displayFader){			
			this.closeMessage.style.top=this.fader.style.top;			
			this.closeMessage.style.right='0px';
			this.closeMessage.show();			
		}
	},
	safeOpen: function(arg){
		if (!op.layer.id){
			op.layer.open(arg);
			return true;
		}
		else {
			return false;
		}
	},
	close: function(result){		
		if (!op.layer.id) return; //not opened		
		if (op.layer.onclose) op.layer.onclose(result);		
		//clear div:
		for (i=0; i<this.div.childNodes.lenght; i++){
			this.div.childNodes[i].style.display='none';
		}
		this.div.hide();
		if (this.fader) this.fader.hide();
		if (this.closeMessage) this.closeMessage.hide();
		
		op.layer.id=null;
		if (this.closeMessage){
			this.closeMessage.hide();
		}
	},
	pregPerc: /^(\d+)%/,
	resize: function(settings, callback){
		op.layer.lastSettings=settings;//remember		
		var animateWidth=(settings.animateWidth==undefined)?this.settings.animateWidth:settings.animateWidth;
		var animateHeight=(settings.animateHeight==undefined)?this.settings.animateHeight:settings.animateHeight;
		var animateOpacity=(settings.animateOpacity==undefined)?this.settings.animateOpacity:settings.animateOpacity;

		
		if (!settings) settings=this.settings;
		if (!settings.top) 	settings.top='center';
		if (!settings.left) settings.left='center';
		var pos= op.position.rectangle(settings);
	
		var left=pos.left;
		var top=pos.top;
		var width=pos.width;
		var height=pos.height;
		
//		var width=(settings.width)?settings.width:this.settings.width;
//		if (m=this.pregPerc.exec(width)){
//			width=Math.ceil(m[1]*document.body.parentNode.clientWidth/100);
//		}
//		var height=(settings.height)?settings.height:this.settings.height;
//		if (m=this.pregPerc.exec(height)){
//			height=Math.ceil(m[1]*document.body.parentNode.clientHeight/100);
//		}
//		var top=(settings.top)?settings.top:this.settings.top;
//		
//		if (m=this.pregPerc.exec(top)){
//			top=Math.ceil(m[1]*document.body.parentNode.clientHeight/100);
//		}
//		else if (top=='center'){			
//			top=Math.ceilL((document.body.clientHeight-height)/2);
//		}
//		else if (top=='top'){
//			top=0;
//		}
//		else if (top=='bottom'){
//			top=document.body.parentNode.clientHeight-height;
//		}
//		
//		top+=(document.body.parentNode.scrollTop)?document.body.parentNode.scrollTop:document.body.scrollTop;
//		
//		var left=(settings.left)?settings.left:this.settings.left;
//		if (m=this.pregPerc.exec(left)){
//			left=Math.ceil(m[1]*document.body.parentNode.clientWidth/100);
//		}
//		else if (left=='center'){			
//			left=Math.ceil(((!browser.msie)?document.body.parentNode.clientWidth:document.body.clientWidth-width)/2);
//			alert(document.body.parentNode.clientWidth+':'+document.body.clientWidth+'??'+width+'='+left);
//			
//		}
//		else if (left=='left'){
//			left=0;
//		}
//		else if (left=='right'){
//			left=(!browser.msie)?document.body.parentNode.clientWidth:document.body.clientWidth-width;
//		}	
//		left+=document.body.parentNode.scrollLeft;
		
		//animate:
		var animate= (animateWidth)?1:0+(animateHeight)?1:0;
		if (animateWidth){			
			op.animateRange(
				function(value){
					op.layer.div.style.width=value+'px';
				}, 
				function(){
					animate-=1; if (animate==0&&callback) callback();
				}, 
				op.layer.div.clientWidth, 
				width, 
				animateWidth.step, 
				animateWidth.timeout				
			);
			if (parseInt(op.layer.div.style.left)){
				//animate left:
				op.animateRange(
					function(value){
						op.layer.div.style.left=value+'px';
					}, 
					function(){
						animate-=1; if (animate==0&&callback) callback();
					}, 
					parseInt(op.layer.div.style.left), 
					left, 
					Math.round(animateHeight.step/2), 
					animateWidth.timeout				
				);
				animate+=1;
			}
			else {
				op.layer.div.style.left=left+'px';
			}
		}
		else{
			op.layer.div.style.width=width+'px';
			op.layer.div.style.left=left+'px';
		}
		
		if (animateHeight){
			op.animateRange(
				function(value){
					op.layer.div.style.height=value+'px';
				}, 
				function(){
					animate-=1; if (animate==0&&callback) callback();
				}, 
				op.layer.div.clientHeight, 
				height, 
				animateHeight.step, 
				animateHeight.timeout			
			);
			if (parseInt(op.layer.div.style.top)){
				//animate left:
				op.animateRange(
					function(value){
						op.layer.div.style.top=value+'px';
					}, 
					function(){
						animate-=1; if (animate==0&&callback) callback();
					}, 
					parseInt(op.layer.div.style.top), 
					top, 
					Math.round(animateHeight.step/2), 
					animateHeight.timeout				
				);
				animate+=1;
			}
			else {
				op.layer.div.style.top=top+'px';
			}
		}
		else{
			
			if (animateOpacity){
				setElementOpacity(op.layer.div,0.1);
				op.animateRange(
					function(value){
						setElementOpacity(op.layer.div,value);
					}, 
					function(){
						if(callback) callback();
					}, 
					0.1, 
					1, 
					animateOpacity.step, 
					animateOpacity.timeout			
				);
			}
			op.layer.div.style.height=height+'px';
			op.layer.div.style.top=top+'px';
		}
		op.layer.div.show();
		
		if (this.settings.positionateClose){
			if (this.closeMessage){
				this.closeMessage.style.visibility='hidden';
				this.closeMessage.show();
				closeTop=top-this.closeMessage.clientHeight;
				closeLeft=left+width-this.closeMessage.clientWidth;
				
				this.closeMessage.style.top=closeTop+'px';
				this.closeMessage.style.left=closeLeft+'px';				
				this.closeMessage.style.visibility='visible';
			}
		}
		if (!animate&&callback) callback();		
	},
	add: function(obj, nav){
		for (i=0; i<this.div.childNodes.length; i++){
			this.div.childNodes[i].style.display='none';
		}
		this.div.appendChild(obj);		
		obj.style.display='block';		
	},
//private		
	_init:function(){		
		var doc=DOM(null);
		if (this.div) return;//init once
		//������� ������ � ������� ���������� ������:
		this.div=op.create('div','system_layer_div',{
			className:	'layer', 
			style:		{position:'absolute', zIndex:210,display:'none'}			
		},		
		document.body);
		//�������� ��������� �� ������� � �������� ������:
		this.div.isTargetOf= function(e){
			var t=e.getTarget();			
			while (t){
				if (t.id==this.id) return true;
				t=t.parentNode;
			}
			return false;
		}
		//�������� (������������ ��� ��������)
		this.div.hide= function(){
			this.style.display='none';
			this.style.height='0px';
			this.style.width='0px';		
			this.style.top='0px';
			this.style.left='0px';
		}

		if (this.settings.displayFader){
			//create fader and attach functions (if fader is turned on)
			this.fader= op.create('div','op.layer.fader',{
				className:	'fader', 
				onclick: 	function(e){self.close();},
				style: 		{position:'absolute', zIndex:200,display:'none'}			
			},document.body);
			//show:
			this.fader.show=function(opacity, callback){
				//hide all top elements:
				this._hideMe=[];
				var t=['select','object'];
				var i=0; var h=null;
				op.layer._flash('hidden');			
				//calculate postion:
				//document.body.style.overflow='hidden';
				var screen=op.position.screen();
				
				this.style.top=0;
				this.style.left=0;
				this.style.width=screen.offsetWidth+'px';
				this.style.height=screen.offsetHeight+'px';
				this.style.display='block';
										
				if (!opacity) opacity=op.layer.settings.opacity;			
				
				if (typeof(opacity)=='object'){
					var self=this;
					op.layer.lastOpacity=opacity.opacity;
					setElementOpacity(this, opacity.start);
					op.animateRange(function(o){setElementOpacity(self, o);}, callback, opacity.start, opacity.opacity, opacity.delta, opacity.timeout, '');
				}
				else {
					op.layer.lastOpacity=opacity;
					setElementOpacity(this, opacity);
					if (callback) callback();
				}
			}
			//hide:
			this.fader.hide= function(){			
				//show all hided elements:
				op.layer._flash('');
				//document.body.style.overflow='';
				this.style.display='none';
			}
			
		}
		else {
			//����� ��� ����� ����� ����� ������� ���������� ����:
			DOM(document.body).listen('click', function(e){				
				if (op.layer.id&&!op.layer.div.isTargetOf(e)){
					if (browser.msie||!op.layer.closeLock){
						op.layer.close();
					}
					else {
						op.layer.closeLock=false;
					}
				}
				return true;
			})
		}		
		
		//on resize:	
		var onResize= function(e){			
			if(op.layer.id){
				if (op.layer.settings.displayFader) op.layer.fader.show(op.layer.lastOpacity);
				op.layer.resize(op.layer.lastSettings);
			}
		}
		//use pure javascript for window:
		if (window.addEventListener) {
			window.addEventListener('resize', onResize, false);
		} else if (window.attachEvent) {
			window.attachEvent('onresize', onResize);
		}
		
		var self=this;
		doc.listen('keyup',function(e){
			if (e.keyCode==keyCode.esc){
				self.close();
			}
			return true;
		});
		
		if (this.settings.closeMessage){
			this.closeMessage=op.create('div', 'op_layer_close', {
				style:{
					zIndex:200,
					position:'absolute',
					display:'none'					
				},
				className:'layer_close',
				onclick:function(){self.close();},
				innerHTML:this.settings.closeMessage
			}, document.body);
		}
	},	
	id: null,
	_flash: function(display){
		op.flash(display);			
	}
}