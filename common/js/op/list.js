op.list={
	childNodes:function(el, filter){
		var ret=[];
		if (!el||!el.childNodes) return false;
	
		for (var i=0; i<el.childNodes.length; i++){			
			if (el.childNodes[i].nodeType==1&&(!filter||filter(el.childNodes[i]))) ret[ret.length]=DOM(el.childNodes[i]);			
		}
		return ret;
	},
	byTagName:function(tagName, filter){
		var list=DOM(el).getElementsByTagName(tagName);
		var ret=[];
		for (var i=0; i<list.length; i++){
			if (!filter||!filter(list[i])) ret[ret.length]=DOM(list[i]);
		}
		return ret;
	},
	filter: function(query){
		var className=null;
		if (query.className){
			className=new RegExp('\\b'+className+'\\b');
		}
		return function(el){
			if (query.tagName&&query.tagName!=el.tagName) return false;
			if (className&&!className.test(el.className)) return false;
			return true;
		}
	},
	groupByClass:function(list){
		return op.list.groupBy(list, 'className', true);
	},
	groupByID:function(list){
		return op.list.groupBy(list, 'id', true);
	},
	groupBy:function(list, field, distinct){
		var ret=[];
		for (var i=0; i<list.length; i++){
			key=list[i][field];
			if (distinct){
				ret[key]=DOM(list[i]);
			}
			else {
				if (!ret[key]) ret[key]=[];
				ret[key][ret[key].length]=DOM(list[i]);
			}
		}
		return ret;
	},
	listenAssoc:function(list, listeners, event){
		if (!event) event='click';
		for (key in listeners){
			if (list[key]) list[key].listen(event, listeners[key]);
		}
	}
};