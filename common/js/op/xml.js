op.xml={
	parseIntoStruct: function(element){
		if (!element) return null;
		var ret=[];
		
		for (var i=0; i<element.childNodes.length; i++){
			if (element.childNodes[i].nodeType==1){				
				ret[element.childNodes[i].tagName]=(element.childNodes[i].text)?element.childNodes[i].text:element.childNodes[i].textContent;		
			}
		}
		
		return ret;
	},
	parseIntoList: function(element){
		if (!element) return null;
		var ret=[];		
		for (var i=0; i<element.childNodes.length; i++){
			if (element.childNodes[i].nodeType==1){				
				ret[ret.length]=op.xml.parseIntoStruct(element.childNodes[i]);
			}			
		}
		return ret;
	},
	selectElement: function(res, tagName){		
		return ret= res.getElementsByTagName(tagName)[0];		
	},
	selectString: function(res, tagName){		
		var ret=op.xml.selectElement(res, tagName);
		if (ret) return (ret.text)?ret.text:ret.textContent;
		else return '';
	},
	factory: function(result){
		if (!result) return null;
		return {
			result:result,
			selectElement:function(path){					
				return op.xml.selectElement(result, path);
			},
			selectString:function(path){
				return op.xml.selectString(result, path);
			},
			selectStruct:function(path){
				return op.xml.parseIntoStruct(op.xml.selectElement(result, path));
			},
			selectList:function(path){
				return op.xml.parseIntoList(op.xml.selectElement(result, path));
			}
		};
	}
};