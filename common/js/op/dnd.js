op.dnd= function(settings){
	
	for (key in settings){
		this[key]=settings[key];
	}
	this.buffer=null;
	var self=this;	
	this.init();
}

op.dnd.prototype.init= function(){
	
	this.marker=DOM('_dndMarker');
	if (!this.marker){		
		this.marker=op.create('div','_dndMarker');	
		document.body.appendChild(this.marker);
	}
	
	this.active=false;
	this.dst=DOM(this.id);
	
	var self=this;	
	this.dst.listen('mouseover', function(){
		self.active=true;
		console.log('mouseover');
		if (self.buffer) self.marker.attachClass('dropme');
	});
	this.dst.listen('mouseout', function(){
		self.active=false;		
		if (self.buffer) self.marker.detachClass('dropme');
	});
	
	DOM(null).listen('mouseup', function(){
		console.log('mouseup', self.active);
		if (self.buffer){
			if (self.active){
				self.callback(self.buffer)
			}
			self.buffer=null;
			self.marker.hide();
		}
	});
	DOM(null).listen('mousemove', function(e){
		if (self.buffer) self.moveMarker(e);
	});
}

op.dnd.prototype.draggable= function(el, obj, innerHTML){
	var self=this;
	el.listen('mousedown', function(e){
		self.buffer=obj;
		self.marker.innerHTML=innerHTML;
		self.moveMarker(e);
	});
}

op.dnd.prototype.moveMarker= function(e){
	if (!this.buffer) return;
	this.marker.style.display='block';
	this.marker.style.left=e.pageX+1;
	this.marker.style.top=e.pageY+1;
}