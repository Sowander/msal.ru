$(document).ready(function() {
 
    // ������������ �����
    var originalFontSize = $('body').css('font-size');
 
    // ��������� cookie
    var cookieSettings = {
        expires: 7,
        path: '/',
        name: 'zoomText'    
    };
 
    // ���� ���� cookie - ��������� ��������� ������
    if($.cookie != undefined && $.cookie(cookieSettings.name) != null) {
        var size = $.cookie(cookieSettings.name);
        $('body').css({fontSize: size + (size.indexOf('px') != -1 ? '' : 'px')}); // IE fix
    }
 
    // ����� ������
    $('.resetFont').click(function() {
        $('body').css('font-size', originalFontSize);
        $.cookie != undefined ? $.cookie(cookieSettings.name, originalFontSize, cookieSettings) : '';
        return false;
    });
 
    // ���������� ������
    $('.incFont').click(function() {
        var newFontSizeNum = (parseFloat($('body').css('font-size'), 10) * 1.2);
        var newFontSizeStr = newFontSizeNum.toString().slice(0, 4);
        $('body').css('font-size', newFontSizeStr);
        $.cookie != undefined ? $.cookie(cookieSettings.name, newFontSizeStr, cookieSettings) : '';
        return false;
    });
 
    // ���������� ������
    $('.decFont').click(function() {
        var newFontSizeNum = (parseFloat($('body').css('font-size'), 10) * 0.8);
        var newFontSizeStr = newFontSizeNum.toString().slice(0, 4);
        $('body').css('font-size', newFontSizeStr);
        $.cookie != undefined ? $.cookie(cookieSettings.name, newFontSizeStr, cookieSettings) : '';
        return false;
    });
 
});