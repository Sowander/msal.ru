//include_once("/common/js/errorReporting.js");
include_once("/common/js/universalFunction.js");
//include_once("/common/js/AjaxClass.js");

$(window).load(function () {
	var regEmail = /([a-zA-Z0-9\-\.]{1,})=######=([a-zA-Z0-9\-\.]{1,})\.([a-zA-Z]{2,6})/g;
    jQuery("a[href^='mailto:']").parent().each(function () {
		jQuery(this).html(jQuery(this).html().replace(regEmail, '$1@$2.$3'));
    });
});

browserName = navigator.appName;
browserVer = parseInt(navigator.appVersion);
var agent = navigator.userAgent;
var ua = navigator.userAgent.toLowerCase();
var msie_ver = ua.charAt(ua.indexOf('msie')+5);


if (ua.indexOf('opera') != '-1') {
	document.writeln("<link rel=stylesheet type=text/css href=\"/common/css/op.css\">");
}

if (ua.indexOf('msie') != '-1') { 
	document.writeln("<link rel=stylesheet type=text/css href=\"/common/css/ie_all.css\">");
}
if ((ua.indexOf('msie') != '-1') && (msie_ver == 5)) { // IE 7.0
	document.writeln("<link rel=stylesheet type=text/css href=\"/common/css/ie5_5.css\">");
}
if ((ua.indexOf('msie') != '-1') && (msie_ver == 6)) { // IE 7.0
	document.writeln("<link rel=stylesheet type=text/css href=\"/common/css/ie6.css\">");
}
if ((ua.indexOf('msie') != '-1') && (msie_ver == 7)) { // IE 7.0
	document.writeln("<link rel=stylesheet type=text/css href=\"/common/css/ie7.css\">");
}


if ((ua.indexOf('msie') != '-1') && (msie_ver == 8)) {  // IE 8.0
	document.writeln("<link rel=stylesheet type=text/css href=\"/common/css/ie8.css\">");
}

if ((ua.indexOf('msie') != '-1') && (msie_ver == 9)) {  // IE 9.0
	document.writeln("<link rel=stylesheet type=text/css href=\"/common/css/ie9.css\">");
}


if ((ua.indexOf('msie') != '-1') && (msie_ver < 9)) { 
	document.writeln("<link rel=stylesheet type=text/css href=\"/common/css/ie.css\">");
}


if ((browserVer >3) &&(browserName == "Netscape")) {
	document.writeln("<link rel=stylesheet type=text/css href=\"/common/css/nn.css\">");
}




function include( filename ) {	// The include() statement includes and evaluates the specified file.
	// 
	// +   original by: mdsjack (http://www.mdsjack.bo.it)
	// +   improved by: Legaev Andrey
	// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   improved by: Michael White (http://crestidg.com)
	// %		note 1: Force Javascript execution to pause until the file is loaded. Usually causes failure if the file never loads. ( Use sparingly! )

	var js = document.createElement('script');
	js.setAttribute('type', 'text/javascript');
	js.setAttribute('src', filename);
	js.setAttribute('defer', 'defer');
	document.getElementsByTagName('HEAD')[0].appendChild(js);

	// save include state for reference by include_once
	var cur_file = {};
	cur_file[window.location.href] = 1;

	if (!window.php_js) window.php_js = {};
	if (!window.php_js.includes) window.php_js.includes = cur_file;
	if (!window.php_js.includes[filename]) {
		window.php_js.includes[filename] = 1;
	} else {
		window.php_js.includes[filename]++;
	}

	return window.php_js.includes[filename];
}

function include_once( filename ) {	// The include_once() statement includes and evaluates the specified file during
	// the execution of the script. This is a behavior similar to the include()
	// statement, with the only difference being that if the code from a file has
	// already been included, it will not be included again.  As the name suggests, it
	// will be included just once.
	// 
	// +   original by: Legaev Andrey
	// +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
	// +   improved by: Michael White (http://crestidg.com)

	var cur_file = {};
	cur_file[window.location.href] = 1;

	if (!window.php_js) window.php_js = {};
	if (!window.php_js.includes) window.php_js.includes = cur_file;
	if (!window.php_js.includes[filename]) {
		return this.include(filename);
	} else{
		return window.php_js.includes[filename];
	}
}



$(document).ready(function(){
	$(".div_click").click(function(){
   $.colorbox({inline:true, href:"#div_open", width:"620px"});
 }); 
})


/*=== ��������� ���� select ====*/
$(document).ready(function() {
	$('select').eq(1).css('width','50px')
});
/*=============================*/



/*=== ���� ��� ������ ====*/
(function($) {
	$(function() {

		$('ul.tabs').delegate('li:not(.current)', 'click', function() {
			$(this).addClass('current').siblings().removeClass('current')
				.parents('div.section').find('div.box').hide().eq($(this).index()).fadeIn(150);
		})

	})
})(jQuery)
/*========================*/
