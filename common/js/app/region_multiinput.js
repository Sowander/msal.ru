function region_multiinput(id, url){
	this.id=id;
	this.url=url;
	var self=this;
	DOMReady(function(){		
		self.select=DOM(self.id+'_country');	
		self.select.listen('change', function(){self.select.form.submit();});
		self.search=DOM(self.id+'_search');		
		self.areas=DOM(self.id+'_areas');
		
		self.search.listen('keyup', function(e){			
			var lbl= self.areas.getElementsByTagName('label');
			var len= self.search.value.length;
			for (var i=0; i<lbl.length; i++){
				if (lbl[i].innerHTML.substr(0,len).toLowerCase()==self.search.value.toLowerCase()){
					lbl[i].parentNode.style.display='block';
				}
				else {
					lbl[i].parentNode.style.display='none';
				}
			}
		});
		
		if (browser.msie6) return;
		
		self.selected=DOM(self.areas.parentNode).create({tagName:'ul',id:self.id+'_selected',className:'selected'});
		self.li=[];
		var li=self.areas.getElementsByTagName('li');
		for (var i=0; i<li.length; i++){
			self.li[i]=self.initLi(li[i]);
		}	
		
		for (i=0; i<self.li.length; i++){			
			if (self.li[i].input.checked){				
				self.selected.appendChild(self.li[i]);
			}
		}
	});
}

region_multiinput.prototype.initLi= function(li){	
	li.input=DOM(li.getElementsByTagName('input')[0]);
	if (browser.msie) li.input.style.width='0px';
	else li.input.hide();
	
	var self=this;
	
	li.input.listen((browser.msie)?'focus':'change', function(){
		
		if (li.input.checked){
			self.selected.appendChild(li);
		}
		else {
			self.areas.appendChild(li);
		}
		return true;
	});
	return li;
}

region_multiinput.prototype.init= function(){
	
}