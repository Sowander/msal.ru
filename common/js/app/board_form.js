function board_form(area, type){
	
	this.area=area;
	this.type=type;
	var self=this;
	DOMReady(function(){
		var inherit=DOM('region_inherit_'+self.area);
		if (inherit){
			var geo=DOM('geo_'+self.area);
			if (inherit.checked) geo.hide();			
			inherit.listen('change', function(){
				if (inherit.checked) geo.hide();
				else geo.show();
			});
		}
		var href=/^(?:(?:https?|ftp|telnet):\/\/(?:[a-z0-9_-]{1,32}(?::[a-z0-9_-]{1,32})?@)?)?(?:(?:[a-z0-9-]{1,128}\.)+(?:com|net|org|mil|edu|arpa|gov|biz|info|aero|inc|name|[a-z]{2})|(?!0)(?:(?!0[^.]|255)[0-9]{1,3}\.){3}(?!0|255)[0-9]{1,3})(?:\/[a-z0-9.,_@%&?+=\~/-]*)?(?:#[^ '\"&<>]*)?$/i;
		self.form= new op.form('item_form_'+area,[
op.form.input('title_'+area, /^(.{2,50})$/),
op.form.input('body_'+area, /^(.{2,1000})$/),
op.form.input('href_'+area, href),
op.form.checkboxes('sector_'+area),
op.form.input('form_'+area)
		]);
	});
}