function inlineTree(id){	
	var self=this;
	this.id=id;
	this.state=[];	
	DOMReady(function(){
		if (self.tree) return;
		self.readTree();
		self.hideTree();
	
	});
}

inlineTree.prototype.readTree= function(){
	this.ul=DOM(this.id);
	li=this.ul.getChildNodes();
	this.tree=[];
	for (var i=0; i<li.length; i++){
		var rootID=li[i].getAttribute('rel');
		if (!this.tree[rootID]) this.tree[rootID]=[];		
		this.tree[rootID][this.tree[rootID].length]=this.initLi(li[i]);
	}
	
}

inlineTree.prototype.initLi= function(li){	
	var childs=li.getChildNodes();
	var self=this;
	for (var i=0; i<childs.length; i++){
		if (childs[i].tagName=='INPUT'&&childs[i].className){
			li[childs[i].className]=childs[i];
		}
		else if(childs[i].className=='expand'){
			li.expand=childs[i];
			li.expand.listen('click', function(e){
				self.expand(li);
			});
		}
	}
	if (li.checkAll){
		li.checkAll.listen('click', function(){
			self.checkR(li);
		})
		li.deselectAll.listen('click',function(){
			self.deselectR(li);
		});
	}
	return li;
}

inlineTree.prototype.checkR= function(li){	
	li.checkbox.checked=true;
	li.style.display='block';
	if (this.tree[li.id]){
		this.state[li.id]=true;
		for (var i=0; i<this.tree[li.id].length; i++){
			this.checkR(this.tree[li.id][i]);
		}
	}
}

inlineTree.prototype.deselectR= function(li){	
	li.checkbox.checked=false;
	
	if (this.tree[li.id]){
		this.state[li.id]=false;
		for (var i=0; i<this.tree[li.id].length; i++){
			this.deselectR(this.tree[li.id][i]);
			this.tree[li.id][i].hide();
		}
	}
}

inlineTree.prototype.expand= function(li, display){
	if (display){
		this.state[li.id]=(display=='block');		
	}
	else {
		this.state[li.id]=!(this.state[li.id]);
		display=(this.state[li.id])?'block':'none';
	}
	
	if (!this.tree[li.id]) return;
	for (var i=0; i<this.tree[li.id].length; i++){
		this.tree[li.id][i].style.display=display;
	}
}

inlineTree.prototype.hideTree= function(){
	for (var key in this.tree){
		if (key==0) continue;
		checked=false;
		for (var i=0; i<this.tree[key].length; i++){
			checked|=this.tree[key][i].checkbox.checked;
		}
		this.state[key]=checked;
		if (!checked){
			this.expand(DOM(key), 'none')
		}
	}
}