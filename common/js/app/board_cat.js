function board_cat(id, settings){
	this.id=id;
	this.tree=[];
	this.li=[];
	var self=this;	
	DOMReady(function(){
		self.init();
	});
}
board_cat.prototype.init= function(){
	this.ul= DOM(this.id);
	this.ul.attachClass('list');
	this.selected= DOM(this.ul.parentNode).create({tagName:'ul',className:'selected'});
	this.initTree(this.ul,0);
}

board_cat.prototype.initTree= function(ul, rootID){
	this.tree[rootID]=ul;
	ret=false;
	ul.li=this.tree[rootID].getChildNodes();	
	for (var i=0; i<ul.li.length; i++){		
		ret|=this.initLi(ul.li[i]);
	}
	return ret;
}

board_cat.prototype._expandTree= function(id, close){
	if (this.tree[id]){		
		if (close){	
			this.tree[id].hide();
			this.li[id].obj.LABEL.className='folder';		
		}
		else {
			this.tree[id].show();
			this.li[id].obj.LABEL.className='expanded';
		}
	}	
}

board_cat.prototype.initLi= function(li){
	li.obj=li.getChildNodes({groupBy:'tagName'});
	this.li[li.obj.INPUT.value]=li;
	
	var ret=li.obj.INPUT.checked;
	if (li.obj.UL){
		ret|=this.initTree(li.obj.UL, li.obj.INPUT.value)
	}
	if (li.obj.UL){
		var more=li.obj.UL.create({tagName:'li', className:'another'});
		more.appendChild(li.obj.INPUT);
		more.create({tagName:'label', title:li.obj.LABEL.innerHTML, htmlFor:li.obj.INPUT.id, innerHTML: op.dict('board_cat_another','Другое')});
		li.obj.INPUT.listen('click', function(){
		
			if (li.obj.INPUT.checked){
				if (!more.input){
					more.input=more.create({
						tagName:'input', 						
						title:op.dict('board_my', 'Ваша рубрика'),
						className:'another',
						onclick:function(){
							if (more.input.value==more.input.title) more.input.value='';
						},
						id:'my_'+this.id+li.obj.INPUT.value,
						name:'my_'+this.id+'['+li.obj.INPUT.value+']'
					});
				}
				else {
					more.input.show();
				}
				more.input.focus();
			}
			else {
				if (more.input) more.input.hide();
			}
			return true;
		})
		li.obj.LABEL.htmlFor='';	
		var self=this;
		li.obj.LABEL.listen('click', function(){			
			self._expandTree(li.obj.INPUT.value, (li.obj.LABEL.className=='expanded')?true:false);
		});
		
		this._expandTree(li.obj.INPUT.value, (ret)?false:true);
	}
	
}