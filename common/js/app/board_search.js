function board_search(area){
	this.area=area;
	var self=this;
	this.formState=[];
	DOMReady(function(){self.init();});
}

board_search.prototype._get= function(id){
	if (this[id]) return this[id];
	this[id]=DOM(id+'_'+this.area);
	return this[id];
}

board_search.prototype.init= function(){	
	var self=this;	
	this._get('form').attachClass('js');	
	this.announces=this._get('announce').getChildNodes();
	//submit listen:
	this._get('real_submit').hide();
	this._get('submit').listen('click', function(){self._get('form').submit();});	
	//labels:
	var labels=this._get('form_status').getElementsByTagName('label');	
	for (var i=0; i<labels.length; i++){
		this.initLabel(labels[i]);
	}
	this._get('expandText').listen('click', function(){self.expand();})
	var search=this._get('search');
	search.listen('keyup', function(){
		self.refreshForm('search', (search.value.length>3));
		return true;
	})
}

board_search.prototype.expand= function(){
	for (var i=0; i<this.announces.length; i++){
		this.announces[i].show();
	}
	this._get('expandText').hide();
}

board_search.prototype.initLabel= function(lbl){
	lbl=DOM(lbl);
	var self=this;
	lbl.listen('click', function(){
		if (lbl.selected){
			lbl.selected.checked=false;
			lbl.innerHTML=lbl.title;
			lbl.detachClass('search');
			self.refreshForm(lbl.htmlFor, false)
		}
		for (var i=0; i<self.announces.length; i++){
			if (self.announces[i].id!=lbl.htmlFor) self.announces[i].hide();
			else self.announces[i].show();
		}
		self._get('expandText').show();
	});
	
	if (lbl.getAttribute('rev')){
		lbl.maxInList=parseInt(lbl.getAttribute('rev'));
		lbl.numeric=lbl.getAttribute('rel').split(';');
		lbl.getNumericTpl= function(count){
			var ten=count%100;
			var one=count%10;
			if (ten>10&&ten<20) return lbl.numeric[2];
			if (one==1) return lbl.numeric[0];
			if (one>1&&one<5) return lbl.numeric[1];
			return lbl.numeric[2];
		}
	}
	
	var inputs= DOM(lbl.htmlFor).getElementsByTagName('input');
	for (var i=0; i<inputs.length; i++){
		if (!inputs[i].title) inputs[i].title=inputs[i].parentNode.getElementsByTagName('label')[0].innerHTML;
		if (inputs[i].type=='radio'){
			DOM(inputs[i]).listen((browser.msie)?'click':'change', this.onSelectRadio(DOM(inputs[i]), lbl));
		}
		else if (inputs[i].type=='checkbox'){
			DOM(inputs[i]).listen((browser.msie)?'click':'change', this.onSelectCheckbox(inputs, lbl));
		}
	}
}

board_search.prototype.onSelectRadio= function(input,lbl){
	var self=this;	
	return function(){
		if (input.checked){
			lbl.innerHTML=input.title;
			lbl.selected=input;
			lbl.attachClass('search');
		}
		else{
			lbl.innerHTML=lbl.title;
			lbl.detachClass('search');
		}
		self.refreshForm(lbl.htmlFor, input.checked);
		self.expand();
		return true;
	}
}

board_search.prototype.onSelectCheckbox= function(inputs, lbl){
	var self=this;
	return function(){
		var checked=[];
		for (var i=0; i<inputs.length; i++){
			if (inputs[i].checked){
				checked[checked.length]=inputs[i];
			}
		}
		if (checked.length==0){
			lbl.innerHTML=lbl.title;
			lbl.detachClass('search');			
		}
		else if (checked.length>lbl.maxInList){
			lbl.innerHTML=lbl.getNumericTpl(checked.length).replace('$count',checked.length);
			lbl.attachClass('search');
		}
		else {
			lbl.attachClass('search');
			lbl.innerHTML='';
			for (var i=0; i<checked.length; i++){
				if (lbl.innerHTML) lbl.innerHTML+=', ';
				lbl.innerHTML+=checked[i].title;
			}
		}
		self.refreshForm(lbl.htmlFor, checked.length)
		return true;
	}
}

board_search.prototype.refreshForm= function(lbl, value){
	if (lbl) this.formState[lbl]=value;
	var enabled=false;
	for (var uid in this.formState){
		if (this.formState[uid]){
			enabled=true;
			break;
		}
	}	
	this._get('submit').disabled=(!enabled);	
}

