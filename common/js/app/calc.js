function calc(form, callback){
	this.callback=callback;
	this.errorTpl='[$title]';
	this.preg=/^\d+$/;
	this.stripSpaces=/\s+/g;
	
	var self=this;
	DOMReady(function(){
		self.form=DOM(form);
		var inputs=self.form.getElementsByTagName('input');
		self.inputs=[];
		for (var i=0; i<inputs.length; i++){
			if (inputs[i].type=='text'||inputs[i].type=='checkbox') self.inputs[self.inputs.length]=inputs[i];
		}
		var selects=self.form.getElementsByTagName('select');
		for (var i=0; i<selects.length; i++){
			self.inputs[self.inputs.length]=selects[i];
		}
		
		self.form.listen('submit', function(e){			
			self.calculate();
		});
	});
}

calc.prototype.calculate= function(){
	var q=[];
	var value=0;
	
	for (var i=0; i<this.inputs.length; i++){
		var input=this.inputs[i];
		var name=input.getAttribute('rel');
		
		if (input.tagName=='INPUT'&&input.type=='text'){
			value=input.value.replace(this.stripSpaces,'');	
			if (!this.preg.test(value)){
				return this.throwError(this.inputs[i]);
			}		
			q[name]=parseInt(value);
		}
		else if (input.tagName=='INPUT'&&input.type=='checkbox'){
			if (this.inputs[i].checked){
				q[name]=input.value;
			}
			else {
				q[name]=input.getAttribute('rev');
			}
		}
		else {
			q[name]=parseInt(input.options[input.selectedIndex].getAttribute('rel'));			
		}
	}	
	this.printResult(this.callback(q));
	return false;
}

calc.prototype.throwError= function(i){
	alert(this.errorTpl.replace('[$title]',i.title));
	i.focus();
	i.select();
	return false;
}

calc.prototype.printResult=function(result){
	DOM(this.form.id+'_result').show();
	DOM(this.form.id+'_result_value').innerHTML=result;
}