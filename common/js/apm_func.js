// Категории ТС
var kateg_id_srts_pts = new Array();
kateg_id_srts_pts["A"] = ["L"];
kateg_id_srts_pts["B"] = ["M1", "N1"];
kateg_id_srts_pts["C"] = ["N2", "N3"];
kateg_id_srts_pts["D"] = ["M2", "M3"];
kateg_id_srts_pts["E"] = ["O1", "O2", "O3", "O4"];

browserName = navigator.appName;
browserVer = parseInt(navigator.appVersion);
var agent = navigator.userAgent;
var ua = navigator.userAgent.toLowerCase();
var msie_ver = ua.charAt(ua.indexOf('msie')+5);


if (ua.indexOf('msie') != '-1') {
	document.writeln("<link rel=stylesheet type=text/css href=\"/common/css/ie_all.css\">");
}
if ((ua.indexOf('msie') != '-1') && (msie_ver == 6)) { // IE 7.0
	document.writeln("<link rel=stylesheet type=text/css href=\"/common/css/ie6.css\">");
}
if ((ua.indexOf('msie') != '-1') && (msie_ver == 7)) { // IE 7.0
	document.writeln("<link rel=stylesheet type=text/css href=\"/common/css/ie7.css\">");
}

if ((ua.indexOf('msie') != '-1') && (msie_ver < 8)) {
	document.writeln("<link rel=stylesheet type=text/css href=\"/common/css/ie.css\">");
}


$(document).ready(
 function()
 {
 	$("tr.td_white").mouseover(function() {
 		$(this).addClass("over");
 	});

 	$("tr.td_white").mouseout(function() {
 		$(this).removeClass("over");
 	});

 	$("tr.td_white:odd").addClass("alt");



	$(".nav_but_1").hover(function(){
        if ($(this).hasClass('disabled')) {
            return;
        }
        $(this).addClass('nav_but_2');
    }, function(){
        if ($(this).hasClass('disabled')) {
            return;
        }
        $(this).removeClass('nav_but_2');
    });

	$(".nav_but_1").click(function(){
        if ($(this).hasClass('disabled') || $(this).hasClass('nonclickable')) {
            return;
        }
        $(this).toggleClass('nav_but_3');
        // установка значения инпута
        var parameter_id = $(this).attr("id").replace("button_table_item_", '');
        /*var inp = $("#button_table_input_"+parameter_id);
        var checked = 0;
        if (inp.val() == 0) {*/
        if (getTableParameter()) {
            checked = 1;
        } else {
            checked = 0;
        }

        //inp.val(checked);

        //showZakluchenie();
        switchParameter(parameter_id);
	});


	$(".nav-13").hover(function(){
        $('.nav_menu_list').slideDown();
    }, function(){
        $('.nav_menu_list').hide();
    });


	$(".nav_menu_list div").hover(function(){
        $(this).addClass('nav_menu_sel');
    }, function(){
        $(this).removeClass('nav_menu_sel');

    });

	$(".nav-13").click(function(){
            return false;
	});
	$(".nav-13 a").click(function(){
            return false;
	});


/*
	$(".menu_search").click(function(){
        $("div.menu_view").slideToggle();
        $(this).toggleClass('sel');
		return false;
    });
*/

 })

// переключение чекбокса Отсутствует ВИН
function vinOtsutstvuetChange(element)
{
    if (jQuery(element).is(':checked')) {
        // включён
        jQuery("#VIN").attr("disabled", "disabled").attr("readonly", "readonly");
		jQuery("#VIN").removeClass('error');
		$('#ST_VIN').html('');
		jQuery("#VIN").unmask();
		//jQuery('#VIN').valid();
		//$('#GOD').focus();
    } else {
        // отключён
        jQuery("#VIN").removeAttr("disabled").removeAttr("readonly");
		$('#ST_VIN').html('*');
		$("#VIN").unmask();
		$.mask.definitions['%']='[0-9A-Za-zБГДЁЖЗИЙЛПФЦЧШЩЪЫЬЭЮЯбгдёжзийлпфцчшщъыьэюя]';
		$("#VIN").unmask().mask('%%%%%%%%%%%%%%%%%')
    }
}

// переключение чекбокса Отсутствует кузов
function kuzovOtsutstvuetChange(element)
{
    if (jQuery(element).is(':checked')) {
        // включён
        jQuery("#NOMER_KUZOVA").attr("disabled", "disabled").attr("readonly", "readonly");
    } else {
        // отключён
        jQuery("#NOMER_KUZOVA").removeAttr("disabled").removeAttr("readonly");
    }
}

// переключение чекбокса Отсутствует кузов
function ramaOtsutstvuetChange(element)
{
    if (jQuery(element).is(':checked')) {
        // включён
        jQuery("#NOMER_RAMY").attr("disabled", "disabled").attr("readonly", "readonly");
    } else {
        // отключён
        jQuery("#NOMER_RAMY").removeAttr("disabled").removeAttr("readonly");
    }
}

// переключение чекбокса номер рамы
function ramOtsutstvuetChange(element)
{
    if (jQuery(element).is(':checked')) {
        // включён
        jQuery("#NOMER_RAMY_ID").attr("disabled", "disabled").attr("readonly", "readonly");
    } else {
        // отключён
        jQuery("#NOMER_RAMY_ID").removeAttr("disabled").removeAttr("readonly");
    }
}

//показываем доп пункт при выборе прицепов
function showDopPunkt()
{
	if (
		(
			$('#KATEG_ID').val() == 'O1' ||
			$('#KATEG_ID').val() == 'O2' ||
			$('#KATEG_ID').val() == 'O3' ||
			$('#KATEG_ID').val() == 'O4'
		) &&
				!$('#TIP_TORMOZ_SISTEMY').find('#not_drag').size() )
	{
		$('#TIP_TORMOZ_SISTEMY').append('<option id="not_drag" value="4" >Без тормозной системы</option>');
	} else if
		(
			$('#KATEG_ID').val() != 'O1' ||
			$('#KATEG_ID').val() != 'O2' ||
			$('#KATEG_ID').val() != 'O3' ||
			$('#KATEG_ID').val() != 'O4'
		)
	{
		//$('#TIP_TORMOZ_SISTEMY').val('').change();
		$('#TIP_TORMOZ_SISTEMY').find('#not_drag').remove();
	}
    // выставляем Категорию ТС (ОКП)
    $('#KATEG_SRTS_PTS_ID').val(
        kategIdSrtsPtsToOkp($('#KATEG_ID').val())
    );
}

// функция делает select неактивным/активным
function disableSelect(element, disabled)
{
    if (disabled) {
        element.focus(function() {
            this.defaultIndex=this.selectedIndex;
        });
        element.change(function() {
            this.selectedIndex=this.defaultIndex;
        });
    } else {
        element.removeAttr('onfocus').removeAttr('onchange');
    }
}

// Получение Категории ОКП по Категории СРТС или ПТС
function kategIdSrtsPtsToOkp(srts_pts)
{
    var kateg_id = '';

    if (!srts_pts) {
        return;
    }

    for (kateg_id in kateg_id_srts_pts) {

        for (kateg_id_srts_pts_key in kateg_id_srts_pts[kateg_id]) {
            if (kateg_id_srts_pts[kateg_id][kateg_id_srts_pts_key] == srts_pts) {
                return kateg_id;
            }
        }
    }

    return '';
}