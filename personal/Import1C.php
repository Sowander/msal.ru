<?php
/**
 * Created by PhpStorm.
 * User: Sowander
 * Date: 21.12.2018
 * Time: 19:25
 */
require_once('Lk.php');
use Bitrix\Main\Type\DateTime;
use Bitrix\Main\Diag\Debug;

class Import1C extends Lk
{

    private static function getDate()
    {
        $date = [];
        $date['start'] = strtotime("-4 month monday");
        $date['end'] = strtotime('+4 month sunday');
        return $date;
    }

    private static function getDateWeek()
    {
        $date = [];
        $date['start'] = strtotime("this week monday");
        $date['end'] = strtotime('this week sunday');
        return $date;
    }

    /*
     * @deprecated
     * */
    static function imoprtAll()
    {
        $arDate = self::getDate();
        $arGroup = [];
        if (!empty($arDate['start']) && !empty($arDate['end'])) {
            $arItems = self::getNotGroupSchedule(date('c', $arDate['start']), date('c', $arDate['end']));

            foreach ($arItems as $item) {
                foreach ($item['Группы']['Состав'] as $group) {
                    $arGroup[trim($group['Группа']['GUIDГруппы'])] = [
                        'name' => trim($group['Группа']['Значение']),
                        'course' => intval($group['Курс']),
                        'form' => intval($group['ФормаОбучения']),
                        'inst' => trim($group['Институт']),
                    ];
                }
            }
        }
        //добавляем нгруппы если надо
        if (is_array($arGroup) && !empty($arGroup)) {
            foreach ($arGroup as $guid => $arGr) {
                $entityDataClass = self::GetEntityDataClass(HL_GROUPS_ID);
                $tempGuid = $entityDataClass::GetList(['select' => ['*'], 'filter' => ['UF_XML_ID' => trim($guid)]])->fetch()['UF_NAME'];
                if (!$tempGuid) {
                    $tempGuid = self::addToHL(HL_GROUPS_ID, [
                        'UF_NAME' => $arGr['name'],
                        'UF_XML_ID' => $guid,
                        'UF_INST' => $arGr['inst'],
                        'UF_FORM' => $arGr['form'],
                        'UF_COURSE' => $arGr['course'],
                    ]);
                }
            }
        }
        //удаляем все расписания и добавляем
        if (!empty($arItems) && is_array($arItems))
            self::importAllShedule($arItems);
    }

    /**
     * @deprecated
     */
    static function importAllShedule($arItems = false)
    {
        $arDelIds = [];
        $entityDataClassSh = self::GetEntityDataClass(HL_SHEDULE_ID);
        $entityDataClassGr = self::GetEntityDataClass(HL_GROUPS_ID);
        $tempOb = $entityDataClassSh::GetList(['select' => ['ID']]);
        while ($tempRes = $tempOb->fetch()) {
            $arDelIds[] = $tempRes['ID'];
        }
        if (is_array($arItems) && !empty($arItems) && is_array($arDelIds) && !empty($arDelIds)) {
            foreach ($arDelIds as $rid) {
                $entityDataClassSh::delete($rid);
            }
        }

        if (is_array($arItems) && !empty($arItems))
            foreach ($arItems as $item) {
                $arParams = [];
                $arGroup = [];
                $idShedule = hash('md5', json_encode($item, JSON_UNESCAPED_UNICODE));
                foreach ($item['Группы']['Состав'] as $group) {
                    $arGroup[] = $entityDataClassGr::getList(['select' => ['ID'], 'filter' => ['UF_XML_ID' => trim($group['Группа']['GUIDГруппы'])]])->fetch()['ID'];
                }
                $arGroup = array_unique($arGroup);
                $dt = new DateTime();
                $startTime = explode(':', trim($item['ВремяНачалаПары']['Значение']));
                $npara = self::getNumberLection(intval($startTime[0]));
                if ($npara > 0) {
                    $arParams = [
                        'UF_XML_ID' => $idShedule,
                        'UF_LNK_GROUPS' => $arGroup,
                        'UF_N_PARA' => intval($npara),
                        'UF_PREPOD_LOGIN' => trim($item['ЛогинАД']),
                        'UF_TYPE' => trim($item['ВидНагрузки']['Наименование']),
                        'UF_GUID_KORPUS' => trim($item['Корпус']['GUIDКорпуса']),
                        'UF_KORPUS' => trim($item['Корпус']['Наименование']),
                        'UF_GUID_AUDIT' => trim($item['Аудитория']['GUIDАудитории']),
                        'UF_AUDIT' => trim($item['Аудитория']['Наименование']),
                        'UF_GUID_PREPOD' => trim($item['Преподаватель']['GUIDПреподавателя']),
                        'UF_PREPOD' => trim($item['Преподаватель']['Наименование']),
                        'UF_GUID_DISCIPLIN' => trim($item['Дисциплина']['GUIDДисциплины']),
                        'UF_DISCIPLIN' => trim($item['Дисциплина']['Наименование']),
                        'UF_START' => trim($item['ВремяНачалаПары']['Значение']),
                        'UF_END' => trim($item['ВремяОкончанияПары']['Значение']),
                        'UF_DAY' => date('d.m.Y', strtotime(trim($item['День']['Значение']))),
                        'UF_CHANGE' => $dt->toString(),
                        'UF_UCH_YEAR' => trim($item['УчебныйГод']['Наименование']),
                        'UF_GUID_UCH_YEAR' => trim($item['УчебныйГод']['GUIDУчебногоГодаРасписания']),
                        'UF_NABOR_YEAR' => trim($item['ГодНабора']['Наименование']),
                        'UF_GUID_NABOR_YEAR' => trim($item['ГодНабора']['GUIDУчебногоГодаРасписания']),
                        'UF_SEMESTR' => intval($item['Семестр']),
                        'UF_RPD' => trim($item['РПД']['Наименование']),
                        'UF_GUID_RPD' => trim($item['РПД']['GUID']),
                        'UF_GUID_POTOK' => trim($item['Поток']['GUID']),
                        'UF_POTOK' => trim($item['Поток']['Наименование']),
                    ];
                    $id = self::addToHL(HL_SHEDULE_ID, $arParams);
                }
            }

    }

    /**
     * @deprecated
     */
    static function imoprtWeek()
    {
        $arDate = self::getDateWeek();
        $arGroup = [];
//        $arDate['start'] = strtotime("14.02.2019");
//        $arDate['end'] = strtotime('14.02.2019');
//        printr(date('c', $arDate['start']));
//        printr(date('c', $arDate['end']));

        # echo print_r(date('c', $arDate['start']),1);
        # echo print_r(date('c', $arDate['end']),1);

        if (!empty($arDate['start']) && !empty($arDate['end'])) {
            $arItems = self::getNotGroupSchedule(date('c', $arDate['start']), date('c', $arDate['end']));
            if (is_array($arItems) && !empty($arItems)) {
                foreach ($arItems as $item) {
                    foreach ($item['Группы']['Состав'] as $group) {
                        $arGroup[trim($group['Группа']['GUIDГруппы'])] = [
                            'name' => trim($group['Группа']['Значение']),
                            'course' => intval($group['Курс']),
                            'form' => intval($group['ФормаОбучения']),
                            'inst' => trim($group['Институт']),
                        ];
                    }
                }
            }
        }
        //добавляем нгруппы если надо
        if (is_array($arGroup) && !empty($arGroup)) {
            foreach ($arGroup as $guid => $arGr) {
                $entityDataClass = self::GetEntityDataClass(HL_GROUPS_ID);
                $tempGuid = $entityDataClass::GetList(['select' => ['*'], 'filter' => ['UF_XML_ID' => trim($guid)]])->fetch()['UF_NAME'];
                if (!$tempGuid) {
                    $tempGuid = self::addToHL(HL_GROUPS_ID, [
                        'UF_NAME' => $arGr['name'],
                        'UF_XML_ID' => $guid,
                        'UF_INST' => $arGr['inst'],
                        'UF_FORM' => $arGr['form'],
                        'UF_COURSE' => $arGr['course'],
                    ]);
                }
            }
        }
        //удаляем расписание за неделю -2 1 и добавляем

        self::importWeekShedule($arItems);
    }

    /**
     * @deprecated
     * @param bool $arItems
     *
     */
    static function importWeekShedule($arItems = false)
    {
        $arDelIds = [];
        $entityDataClassSh = self::GetEntityDataClass(HL_SHEDULE_ID);
        $entityDataClassGr = self::GetEntityDataClass(HL_GROUPS_ID);
        $arTimeStamp = self::getDateWeek();
        $tempOb = $entityDataClassSh::getList(['select' => ['*'], 'filter' => [
            'LOGIC' => 'AND',
            [
                '>=UF_DAY' => date('d.m.Y', $arTimeStamp['start']),
            ],
            [
                '<=UF_DAY' => date('d.m.Y', $arTimeStamp['end']),
            ]
        ]]);
        while ($tempRes = $tempOb->fetch()) {
            $arDelIds[] = $tempRes['ID'];
        }
        if (is_array($arItems) && !empty($arItems) && is_array($arDelIds) && !empty($arDelIds)) {
            foreach ($arDelIds as $rid) {
                $entityDataClassSh::delete($rid);
            }
        }

        if (is_array($arItems) && !empty($arItems))
            foreach ($arItems as $item) {
                $arParams = [];
                $arGroup = [];
                $idShedule = hash('md5', json_encode($item, JSON_UNESCAPED_UNICODE));
                foreach ($item['Группы']['Состав'] as $group) {
                    $arGroup[] = $entityDataClassGr::getList(['select' => ['ID'], 'filter' => ['UF_XML_ID' => trim($group['Группа']['GUIDГруппы'])]])->fetch()['ID'];
                }

//				$arGroup = array_unique($arGroup);
                $dt = new DateTime();
                $startTime = explode(':', trim($item['ВремяНачалаПары']['Значение']));
                $npara = self::getNumberLection(intval($startTime[0]));

                if ($npara > 0) {
                    $arParams = [
                        'UF_XML_ID' => $idShedule,
                        'UF_LNK_GROUPS' => $arGroup,
                        'UF_N_PARA' => intval($npara),
                        'UF_PREPOD_LOGIN' => trim($item['ЛогинАД']),
                        'UF_TYPE' => trim($item['ВидНагрузки']['Наименование']),
                        'UF_GUID_KORPUS' => trim($item['Корпус']['GUIDКорпуса']),
                        'UF_KORPUS' => trim($item['Корпус']['Наименование']),
                        'UF_GUID_AUDIT' => trim($item['Аудитория']['GUIDАудитории']),
                        'UF_AUDIT' => trim($item['Аудитория']['Наименование']),
                        'UF_GUID_PREPOD' => trim($item['Преподаватель']['GUIDПреподавателя']),
                        'UF_PREPOD' => trim($item['Преподаватель']['Наименование']),
                        'UF_GUID_DISCIPLIN' => trim($item['Дисциплина']['GUIDДисциплины']),
                        'UF_DISCIPLIN' => trim($item['Дисциплина']['Наименование']),
                        'UF_START' => trim($item['ВремяНачалаПары']['Значение']),
                        'UF_END' => trim($item['ВремяОкончанияПары']['Значение']),
                        'UF_DAY' => date('d.m.Y', strtotime(trim($item['День']['Значение']))),
                        'UF_CHANGE' => $dt->toString(),
                        'UF_UCH_YEAR' => trim($item['УчебныйГод']['Наименование']),
                        'UF_GUID_UCH_YEAR' => trim($item['УчебныйГод']['GUIDУчебногоГодаРасписания']),
                        'UF_NABOR_YEAR' => trim($item['ГодНабора']['Наименование']),
                        'UF_GUID_NABOR_YEAR' => trim($item['ГодНабора']['GUIDУчебногоГодаРасписания']),
                        'UF_SEMESTR' => intval($item['Семестр']),
                        'UF_RPD' => trim($item['РПД']['Наименование']),
                        'UF_GUID_RPD' => trim($item['РПД']['GUID']),
                        'UF_GUID_POTOK' => trim($item['Поток']['GUID']),
                        'UF_POTOK' => trim($item['Поток']['Наименование']),
                    ];
                    $id = self::addToHL(HL_SHEDULE_ID, $arParams);
                }
            }

    }

    /**
     * @deprecated
     * @param $dateStart
     * @param $dateEnd
     * @return array
     *
     */
    static function getNotGroupSchedule($dateStart, $dateEnd)
    {
        $function = 'ПолучитьРасписаниеБезГруппы';
        $wsdl = 'http://' . self::$host . '/UV/ws/wsreportschedulegroup.1cws?wsdl';
        $arResult = [];
        $params = array(
            'ДатаС' => $dateStart, 'ДатаПо' => $dateEnd
        );
        $arTemp = self::getSoapClient($wsdl, $function, $params);
        if (is_array($arTemp) && !empty($arTemp))
            foreach ($arTemp as $item) {
                if (!empty($item)) {
                    $tempItem = self::stdClassInArray($item);
                    if ($tempItem['Группы']['Состав']['Группа']) {
                        $tempItem['Группы']['Состав'] = [$tempItem['Группы']['Состав']];
                    }
                    $arResult[] = $tempItem;
                }
            }
        return $arResult;
    }

    /**
     * добавление чегонить в HL
     * @param $idHiloadBlock
     * @param $arParams
     * @return array|\Bitrix\Main\Entity\AddResult|\Bitrix\Main\ORM\Data\AddResult|bool|int
     */
    private static function addToHL($idHiloadBlock, $arParams)
    {
        $entityDataClass = self::GetEntityDataClass($idHiloadBlock);
        $result = 0;
        if ($entityDataClass) {
            $result = $entityDataClass::add($arParams);
            $id = $result->getId();
            if ($id) {
                $result = $id;
            }
        }
        return $result;
    }

    static function imoprtAllE()
    {
        $arDate = self::getDate();
        $arGroup = [];
        if (!empty($arDate['start']) && !empty($arDate['end'])) {
            $arItems = self::getNotGroupScheduleE(date('c', $arDate['start']), date('c', $arDate['end']));

            foreach ($arItems as $item) {
                foreach ($item['Groups']['List'] as $group) {
                    $arGroup[trim($group['Group']['GUID'])] = [
                        'name' => trim($group['Group']['Name']),
                        'course' => intval($group['Course']),
                        'form' => intval($group['FormStudy']),
                        'inst' => trim($group['Faculty']),
                    ];
                }
            }
        }
        //добавляем нгруппы если надо
        if (is_array($arGroup) && !empty($arGroup)) {
            foreach ($arGroup as $guid => $arGr) {
                $entityDataClass = self::GetEntityDataClass(HL_GROUPS_ID);
                $tempGuid = $entityDataClass::GetList(['select' => ['*'], 'filter' => ['UF_XML_ID' => trim($guid)]])->fetch()['UF_NAME'];
                if (!$tempGuid) {
                    $tempGuid = self::addToHL(HL_GROUPS_ID, [
                        'UF_NAME' => $arGr['name'],
                        'UF_XML_ID' => $guid,
                        'UF_INST' => $arGr['inst'],
                        'UF_FORM' => $arGr['form'],
                        'UF_COURSE' => $arGr['course'],
                    ]);
                }
            }
        }
        //удаляем все расписания и добавляем
        if (!empty($arItems) && is_array($arItems))
            self::importAllSheduleE($arItems);
    }

    /**
     * @param bool $arItems
     */
    static function importAllSheduleE($arItems = false)
    {
        $arDelIds = [];
        $entityDataClassSh = self::GetEntityDataClass(HL_SHEDULE_ID);
        $entityDataClassGr = self::GetEntityDataClass(HL_GROUPS_ID);
        $tempOb = $entityDataClassSh::GetList(['select' => ['ID']]);
        while ($tempRes = $tempOb->fetch()) {
            $arDelIds[] = $tempRes['ID'];
        }
        if (is_array($arItems) && !empty($arItems) && is_array($arDelIds) && !empty($arDelIds)) {
            foreach ($arDelIds as $rid) {
                $entityDataClassSh::delete($rid);
            }
        }

        if (is_array($arItems) && !empty($arItems))
            foreach ($arItems as $item) {
                $arParams = [];
                $arGroup = [];
                $idShedule = hash('md5', json_encode($item, JSON_UNESCAPED_UNICODE));
                foreach ($item['Groups']['List'] as $group) {
                    $arGroup[] = $entityDataClassGr::getList(['select' => ['ID'], 'filter' => ['UF_XML_ID' => trim($group['Group']['GUID'])]])->fetch()['ID'];
                }
                $arGroup = array_unique($arGroup);
                $dt = new DateTime();
                $startTime = explode(':', trim($item['STime']['Val']));
                $npara = self::getNumberLection(intval($startTime[0]));
                if ($npara > 0) {
                    $arParams = [
                        'UF_XML_ID' => $idShedule,
                        'UF_LNK_GROUPS' => $arGroup,
                        'UF_N_PARA' => intval($npara),
                        'UF_PREPOD_LOGIN' => trim($item['LoginAD']),
                        'UF_TYPE' => trim($item['LoadType']['Name']),
                        'UF_GUID_KORPUS' => trim($item['Corps']['GUID']),
                        'UF_KORPUS' => trim($item['Corps']['Name']),
                        'UF_GUID_AUDIT' => trim($item['Auditory']['GUID']),
                        'UF_AUDIT' => trim($item['Auditory']['Name']),
                        'UF_GUID_PREPOD' => trim($item['Teacher']['GUID']),
                        'UF_PREPOD' => trim($item['Teacher']['Name']),
                        'UF_GUID_PREPOD_OR' => trim($item['TeacherOriginal']['GUID']),
                        'UF_PREPOD_OR' => trim($item['TeacherOriginal']['Name']),
                        'UF_GUID_DISCIPLIN' => trim($item['Discipline']['GUID']),
                        'UF_DISCIPLIN' => trim($item['Discipline']['Name']),
                        'UF_START' => trim($item['STime']['Val']),
                        'UF_END' => trim($item['ETime']['Val']),
                        'UF_DAY' => date('d.m.Y', strtotime(trim($item['Day']['Val']))),
                        'UF_CHANGE' => $dt->toString(),
                        'UF_UCH_YEAR' => trim($item['AcademicYear']['Name']),
                        'UF_GUID_UCH_YEAR' => trim($item['AcademicYear']['GUID']),
                        'UF_NABOR_YEAR' => trim($item['YearSet']['Name']),
                        'UF_GUID_NABOR_YEAR' => trim($item['YearSet']['GUID']),
                        'UF_SEMESTR' => intval($item['Semester']),
                        'UF_RPD' => trim($item['RPD']['Name']),
                        'UF_GUID_RPD' => trim($item['RPD']['GUID']),
                        'UF_GUID_POTOK' => trim($item['Flow']['GUID']),
                        'UF_POTOK' => trim($item['Flow']['Name']),
                    ];
                    $id = self::addToHL(HL_SHEDULE_ID, $arParams);
                }
            }

    }

    /**
     *
     */
    static function imoprtWeekE()
    {
        $arDate = self::getDateWeek();
        $arGroup = [];

        if (!empty($arDate['start']) && !empty($arDate['end'])) {
            $arItems = self::getNotGroupScheduleE(date('c', $arDate['start']), date('c', $arDate['end']));
            if (is_array($arItems) && !empty($arItems)) {
                foreach ($arItems as $item) {
                    foreach ($item['Groups']['List'] as $group) {
                        $arGroup[trim($group['Group']['GUID'])] = [
                            'name' => trim($group['Group']['Name']),
                            'course' => intval($group['Course']),
                            'form' => intval($group['FormStudy']),
                            'inst' => trim($group['Faculty']),
                        ];
                    }
                }
            }
        }
        //добавляем нгруппы если надо
        if (is_array($arGroup) && !empty($arGroup)) {
            foreach ($arGroup as $guid => $arGr) {
                $entityDataClass = self::GetEntityDataClass(HL_GROUPS_ID);
                $tempGuid = $entityDataClass::GetList(['select' => ['*'], 'filter' => ['UF_XML_ID' => trim($guid)]])->fetch()['UF_NAME'];
                if (!$tempGuid) {
                    $tempGuid = self::addToHL(HL_GROUPS_ID, [
                        'UF_NAME' => $arGr['name'],
                        'UF_XML_ID' => $guid,
                        'UF_INST' => $arGr['inst'],
                        'UF_FORM' => $arGr['form'],
                        'UF_COURSE' => $arGr['course'],
                    ]);
                }
            }
        }
        //удаляем расписание за неделю -2 1 и добавляем

        self::importWeekSheduleE($arItems);
    }

    /**
     * @param bool $arItems
     */
    static function importWeekSheduleE($arItems = false)
    {
        $arDelIds = [];
        $entityDataClassSh = self::GetEntityDataClass(HL_SHEDULE_ID);
        $entityDataClassGr = self::GetEntityDataClass(HL_GROUPS_ID);
        $arTimeStamp = self::getDateWeek();
        $tempOb = $entityDataClassSh::getList(['select' => ['*'], 'filter' => [
            'LOGIC' => 'AND',
            [
                '>=UF_DAY' => date('d.m.Y', $arTimeStamp['start']),
            ],
            [
                '<=UF_DAY' => date('d.m.Y', $arTimeStamp['end']),
            ]
        ]]);
        while ($tempRes = $tempOb->fetch()) {
            $arDelIds[] = $tempRes['ID'];
        }
        if (is_array($arItems) && !empty($arItems) && is_array($arDelIds) && !empty($arDelIds)) {
            foreach ($arDelIds as $rid) {
                $entityDataClassSh::delete($rid);
            }
        }

        if (is_array($arItems) && !empty($arItems))
            foreach ($arItems as $item) {
                $arParams = [];
                $arGroup = [];
                $idShedule = hash('md5', json_encode($item, JSON_UNESCAPED_UNICODE));
                foreach ($item['Groups']['List'] as $group) {
                    $arGroup[] = $entityDataClassGr::getList(['select' => ['ID'], 'filter' => ['UF_XML_ID' => trim($group['Group']['GUID'])]])->fetch()['ID'];
                }

//				$arGroup = array_unique($arGroup);
                $dt = new DateTime();
                $startTime = explode(':', trim($item['STime']['Val']));
                $npara = self::getNumberLection(intval($startTime[0]));

                if ($npara > 0) {
                    $arParams = [
                        'UF_XML_ID' => $idShedule,
                        'UF_LNK_GROUPS' => $arGroup,
                        'UF_N_PARA' => intval($npara),
                        'UF_PREPOD_LOGIN' => trim($item['LoginAD']),
                        'UF_TYPE' => trim($item['LoadType']['Name']),
                        'UF_GUID_KORPUS' => trim($item['Corps']['GUID']),
                        'UF_KORPUS' => trim($item['Corps']['Name']),
                        'UF_GUID_AUDIT' => trim($item['Auditory']['GUID']),
                        'UF_AUDIT' => trim($item['Auditory']['Name']),
                        'UF_GUID_PREPOD' => trim($item['Teacher']['GUID']),
                        'UF_PREPOD' => trim($item['Teacher']['Name']),
                        'UF_GUID_PREPOD_OR' => trim($item['TeacherOriginal']['GUID']),
                        'UF_PREPOD_OR' => trim($item['TeacherOriginal']['Name']),
                        'UF_GUID_DISCIPLIN' => trim($item['Discipline']['GUID']),
                        'UF_DISCIPLIN' => trim($item['Discipline']['Name']),
                        'UF_START' => trim($item['STime']['Val']),
                        'UF_END' => trim($item['ETime']['Val']),
                        'UF_DAY' => date('d.m.Y', strtotime(trim($item['Day']['Val']))),
                        'UF_CHANGE' => $dt->toString(),
                        'UF_UCH_YEAR' => trim($item['AcademicYear']['Name']),
                        'UF_GUID_UCH_YEAR' => trim($item['AcademicYear']['GUID']),
                        'UF_NABOR_YEAR' => trim($item['YearSet']['Name']),
                        'UF_GUID_NABOR_YEAR' => trim($item['YearSet']['GUID']),
                        'UF_SEMESTR' => intval($item['Semester']),
                        'UF_RPD' => trim($item['RPD']['Name']),
                        'UF_GUID_RPD' => trim($item['RPD']['GUID']),
                        'UF_GUID_POTOK' => trim($item['Flow']['GUID']),
                        'UF_POTOK' => trim($item['Flow']['Name']),
                    ];
                    $id = self::addToHL(HL_SHEDULE_ID, $arParams);
                }
            }

    }

    /**
     * новая wsdl
     * @param $dateStart
     * @param $dateEnd
     * @return array
     */
    static function getNotGroupScheduleE($dateStart, $dateEnd)
    {
        $function = 'ПолучитьРасписаниеБезГруппы';
        $wsdl = 'http://' . self::$OldHost . '/UV/ws/wsreportsjournal.1cws?wsdl';
        $arResult = [];
        $params = array(
            'ДатаС' => $dateStart, 'ДатаПо' => $dateEnd
        );
        $arTemp = self::getSoapClient($wsdl, $function, $params, 'List');
        if (is_array($arTemp) && !empty($arTemp))
            foreach ($arTemp as $item) {
                if (!empty($item)) {
                    $tempItem = self::stdClassInArray($item);
                    if ($tempItem['Groups']['List']['Group']) {
                        $tempItem['Groups']['List'] = [$tempItem['Groups']['List']];
                    }
                    $arResult[] = $tempItem;
                }
            }
        return $arResult;
    }

    static function updatePreopd($json)
    {
        $result = false;
        $time = date('d.m.Y-H:i:s');
        $arParams = json_decode($json, true, JSON_UNESCAPED_UNICODE);
        if ($arParams['ЗаменыППС']['ДисциплинаУИД']) {
            $arParams['ЗаменыППС'] = [$arParams['ЗаменыППС']];
        }
        if (json_last_error() === JSON_ERROR_NONE && is_array($arParams) && !empty($arParams)) {
            $entityDataClass = self::GetEntityDataClass(HL_SHEDULE_ID);
            $dt = new DateTime();
            foreach ($arParams['ЗаменыППС'] as $arOneParam) {
                $filter = [
                    'UF_GUID_POTOK' => trim($arOneParam['ПотокУИД']),
                    'UF_START' => trim($arOneParam['ВремяНачала']),
                    'UF_GUID_DISCIPLIN' => trim($arOneParam['ДисциплинаУИД']),
                    'UF_DAY' => trim(date('d.m.Y', strtotime(trim($arOneParam['День'])))),
                ];
                $arTempIds[] = $elementID = $entityDataClass::GetList([
                        'select' => ['*'],
                        'filter' => $filter
                    ]
                )->fetch()['ID'];
                if (intval($elementID) > 2) {
                    $params['UF_PREPOD'] = trim($arOneParam['ПреподавательПослеФИО']);
                    $params['UF_GUID_PREPOD'] = trim($arOneParam['ПреподавательПослеУИД']);
                    $params['UF_PREPOD_LOGIN'] = trim($arOneParam['ПреподавательПослеЛогинАД']);
                    $params['UF_CHANGE'] = $dt->toString();
                    $res = $entityDataClass::update(intval($elementID),$params);
                    if(!$res->isSuccess()){
                        $result['RESULT'] = $time . 'ОШИБКА Не удалось обновить препода ID-'.$elementID;
                        $result['JSON'] = $json;
                        Debug::writeToFile($result, '', SH_LOG);
                    }
                }
            }
            if (count($arParams['ЗаменыППС']) !== count($arTempIds)) {
                $result['RESULT'] = $time . 'ОШИБКА Не все преподаватели были заменены';
                $result['JSON'] = $json;
                Debug::writeToFile($result, '', SH_LOG);
                throw new Exception($result['RESULT']);
            }
        } else {
            $result['RESULT'] = 'ОШИБКА '.$time . Lk::getJsonError();
            $result['JSON'] = $json;
            Debug::writeToFile($result, '', SH_LOG);
            throw new Exception($result['RESULT']);
        }

    }

}