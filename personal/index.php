<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");?><?
/* <img src="http://mir-kryma.ru/data/images/techwork.jpg" alt="">*/
global $USER,$APPLICATION;
$APPLICATION->SetTitle("Личный кабинет");

$isPrepod = in_array(ID_PREPOD_GROUP, $USER->GetUserGroupArray());
if($isPrepod){
    $APPLICATION->IncludeComponent(
        "glab:main.profile.prepod",
        "",
        Array(
            "CHECK_RIGHTS" => "N",
            "SEND_INFO" => "N",
            "SET_TITLE" => "Y",
            "USER_PROPERTY" => array(),
            "USER_PROPERTY_NAME" => "",
            "CACHE_TIME" => 3600
        )
    );
}else{
    $APPLICATION->IncludeComponent(
        "glab:main.profile.student",
        "",
        Array(
            "CHECK_RIGHTS" => "N",
            "SEND_INFO" => "N",
            "SET_TITLE" => "Y",
            "USER_PROPERTY" => array(),
            "USER_PROPERTY_NAME" => "",
            "CACHE_TIME" => 3600
        )
    );

}
?>
<style>
    .form-loader {
        display: block;
    }
</style>
<script>
    $(document).ready(function ($) {
        $(window).load(function () {
            $('.form-loader').fadeOut('slow', function () {});

        });
    });
</script>


<script>
    $('[name="form_auth"]').submit(function () {
        $('.form-loader').fadeIn(300);
    });


    <?if($_GET[t]){?>
    $(".t<?=$_GET[t]?>").trigger('click');
    <?}?>

</script>
<?require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>
