<?php
include($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
use Bitrix\Main\Loader;
use Bitrix\Highloadblock\HighloadBlockTable as HLBT;

class Lk
{
	protected static $login = 'webservices';
	protected static $password = 'WebTest17';
	public static $host = '10.0.0.53';
	public static $OldHost = '10.0.0.51';
	protected static $logpath = '/logs/';
	protected static $ListScheduleIblockID = 17;
	protected static $ListRingIblockID = 18;
	public static $null = '00000000-0000-0000-0000-000000000000';

	static function setBitrixSystems($error = false)
	{
		if ($error !== false) {
			error_reporting(E_ALL);
			ini_set('display_errors', 'On');
		}
		set_time_limit(0);
		require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
	}


	static function getSoapClient($wsd, $functionName, $params = NULL, $obj = false)
	{
		if (!function_exists('is_soap_fault')) {
			print 'Не настроен web сервер. Не найден модуль php-soap.';
			return false;
		}

		try {
			//кеширование wsdl
			$cache = static::wsdlCahe(1);
			$client = new SoapClient($wsd,
				array("login" => self::$login,
					"password" => self::$password,
					'soap_version' => SOAP_1_2,
					'trace' => true,
					'exceptions' => 1,
					'cache_wsdl' => $cache
				)
			);
		} catch (SoapFault $e) {
			trigger_error('Ошибка подключения или внутренняя ошибка сервера. Не удалось связаться с базой 1С.', E_ERROR);
			var_dump($e);
		}
		if (is_soap_fault($client)) {
			trigger_error('Ошибка подключения или внутренняя ошибка сервера. Не удалось связаться с базой 1С.', E_ERROR);
			return false;
		}

		if ($obj === false) {
			$obj = 'Состав';
		}

		if ($obj === null) {
			$result = $client->$functionName($params)->return;
		} else {
			$result = $client->$functionName($params)->return->$obj;
		}
		return $result;
	}

	static function wsdlCahe($bool)
	{
		$timeout = 600;
		$time = 24*3600;
		ini_set('default_socket_timeout', intval($timeout));
		ini_set('soap.wsdl_cache_enabled', intval($bool));
		if($bool){
			ini_set('soap.wsdl_cache_ttl', intval($time));
		}
		return intval($bool);
	}

	static function dateToMS($curDate = false, $format = false, $plus = false)
	{
		setlocale(LC_TIME, 'ru_RU.UTF-8');

		if ($curDate === false) {
			$now = strtotime("this monday");
		} else {
			$now = strtotime($curDate);
		}


		if ($format === false) {
			$date['monday'] = strftime('%F', strtotime('this week monday', $now));
			$date['sunday'] = strftime('%F', strtotime('this week sunday', $now));
		} else {
			$date['monday'] = strftime($format, strtotime('this week monday', $now));
			$date['sunday'] = strftime($format, strtotime('this week sunday', $now));
		}
		if ($plus !== false) {
			$date['monday'] = strftime($format, strtotime($plus . ' week monday', $now));
			$date['sunday'] = strftime($format, strtotime($plus . ' week sunday', $now));
		}

		return $date;
	}


	static function dateToDay($date = false, $format = false, $plus = false)
	{
		setlocale(LC_TIME, 'ru_RU.UTF-8');
		if ($format === false) {
			$format = '%A';
		}
		if ($date === false) {
			$day = strftime($format);
		} else {
			$day = strftime($format, strtotime($date));
		}
		if ($plus !== false && $format !== false) {
			$date = strtotime($date);
			$day = strftime($format, strtotime($plus, $date));
		}

		return $day;
	}

	static function getYearGuidBitrix()
	{
		self::setBitrixSystems();

		$arResult = NULL;
		$arSelect = Array("ID", "NAME", "ACTIVE", "PROPERTY_SCH_YEAR_GUID");
		$arFilter = Array("IBLOCK_ID" => IntVal(self::$ListScheduleIblockID), 'ACTIVE' => 'Y');
		$res = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNext()) {
			$arResult = $ob['PROPERTY_SCH_YEAR_GUID_VALUE'];
		}
		return $arResult;
	}

	static function getParamVedomosti($nlist)
	{
		$arResult = false;
		$function = 'ПолучитьПараметрыВедомости';
		$wsdl = 'http://' . self::$host . '/UV/ws/wsreportattved.1cws?wsdl';

		$params = array('НомерВедомости' => trim($nlist));
		$obProgress = self::getSoapClient($wsdl, $function, $params);
		return $obProgress;
	}

	static function stdClassInArray($object)
	{
		return json_decode(json_encode($object), true);
	}

	/**
	 * Функция получения экземпляра класса: для HL блоков
	 * @param $HlBlockId
	 * @return \Bitrix\Main\Entity\DataManager|bool
	 */
	static function GetEntityDataClass($HlBlockId)
	{
		Loader::includeModule("highloadblock");
		if (empty($HlBlockId) || $HlBlockId < 1) {
			return false;
		}
		$hlblock = HLBT::getById($HlBlockId)->fetch();
		$entity = HLBT::compileEntity($hlblock);
		$entityDataClass = $entity->getDataClass();
		return $entityDataClass;
	}

	/**
	 * преобразование даты из битрикс объекта в строку
	 * @param $date
	 * @param $mask
	 * @return string | array
	 */
	public static function getDateFromOb($date, $mask)
	{
		if ($date instanceof \Bitrix\Main\Type\Date or $date instanceof \Bitrix\Main\Type\DateTime)
			return $date->format($mask);

		return [];
	}


	static function getNumberLection($timeStart)
	{
		switch ($timeStart) {
			case 9:
				return 1;
				break;
			case 10:
				return 2;
				break;
			case 11:
				return 2;
				break;
			case 12:
				return 3;
				break;
			case 13:
				return 4;
				break;
			case 14:
				return 4;
				break;
			case 15:
				return 5;
				break;
			case 16:
				return 6;
				break;
			case 17:
				return 6;
				break;
			case 18:
				return 7;
				break;
			case 19:
				return 7;
				break;
			case 20:
				return 8;
				break;
			default:
				return false;
		}
	}

	static function getNumberLectionPrepod($timeStart)
	{
		switch ($timeStart) {
			case 9:
				return 1;
				break;
			case 10:
				return 2;
				break;
			case 12:
				return 3;
				break;
			case 13:
				return 4;
				break;
			case 14:
				return 4;
				break;
			case 15:
				return 5;
				break;
			case 16:
				return 6;
				break;
			case 17:
				return 6;
				break;
			case 18:
				return 7;
				break;
			case 19:
				return 7;
				break;
			case 20:
				return 8;
				break;
			default:
				return false;
		}
	}

	static function getJsonError(){
        switch (json_last_error()) {
            case JSON_ERROR_DEPTH:
                return ' - Достигнута максимальная глубина стека';
                break;
            case JSON_ERROR_STATE_MISMATCH:
                echo ' - Некорректные разряды или несоответствие режимов';
                break;
            case JSON_ERROR_CTRL_CHAR:
                return ' - Некорректный управляющий символ';
                break;
            case JSON_ERROR_SYNTAX:
                return ' - Синтаксическая ошибка, некорректный JSON';
                break;
            case JSON_ERROR_UTF8:
                return ' - Некорректные символы UTF-8, возможно неверно закодирован';
                break;
            default:
                return ' - Неизвестная ошибка';
                break;
        }
    }

}