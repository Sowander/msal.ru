<?php

/**
 * Created by PhpStorm.
 * User: Sowander
 * Date: 20.02.2018
 * Time: 15:20
 */
use Bitrix\Main\Diag\Debug;
use Bitrix\Main\Loader;

require_once('Lk.php');

class Ring extends Lk
{
    static function getListRing()
    {
        $function = 'ПолучитьСписокПлановЗвонков';
        $wsdl = 'http://' . self::$host . '/UV/ws/wsreportschedulegroup.1cws?wsdl';
        return self::getSoapClient($wsdl, $function);
    }

    static function getkListRingBitrix($active = false)
    {
        self::setBitrixSystems();
	    Loader::includeModule('iblock');
        $arResult = NULL;
        $arSelect = Array("ID", "NAME", "ACTIVE", "PROPERTY_PLAN_GUID");
        $arFilter = Array("IBLOCK_ID" => IntVal(self::$ListRingIblockID),'ACTIVE'=>$active);
        $res = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
        while ($ob = $res->GetNext()) {
            $arResult[] = $ob;
        }
        return $arResult;
    }

    static function setListRing()
    {

        self::setBitrixSystems();
        $ob1C = self::getListRing();
        $arBitrix = self::getkListRingBitrix();

        global $USER;
        $el = new \CIBlockElement;
        foreach ($ob1C as $ob) {

            unset($arLoadProductArray);
            unset($PROP);
            unset($PRODUCT_ID);
            unset($ACTIVE);

            $PROP['PLAN_GUID'] = trim($ob->ПланЗвонков->GUIDПланаЗвонков);



            foreach ($arBitrix as $arItem) {
                if (trim($arItem['PROPERTY_PLAN_GUID_VALUE']) == trim($ob->ПланЗвонков->GUIDПланаЗвонков)) {
                    $PRODUCT_ID = $arItem['ID'];
                    $ACTIVE = $arItem['ACTIVE'];
                }
            }
            $arLoadProductArray = Array(
                "MODIFIED_BY" => $USER->GetID(), // элемент изменен текущим пользователем
                "IBLOCK_SECTION_ID" => false,
                "IBLOCK_ID" => self::$ListRingIblockID,
                "PROPERTY_VALUES" => $PROP,
                "NAME" => trim($ob->ПланЗвонков->Наименование),
                "ACTIVE" => $ACTIVE? $ACTIVE :'N',
            );
            if ($PRODUCT_ID) { //Обновим элемент
                if (!$res = $el->Update($PRODUCT_ID, $arLoadProductArray)) {
                    Debug::writeToFile($el->LAST_ERROR . '/n', '', self::$logpath.'/logs/setListRingErrorUpdate.log');
                }
            } else { //Добавим элемент
                if (!$PRODUCT_ID = $el->Add($arLoadProductArray)) {
                    Debug::writeToFile($el->LAST_ERROR . '/n', '', self::$logpath.'/logs/setListRingErrorAdd.log');
                }
            }
        }
    }

}