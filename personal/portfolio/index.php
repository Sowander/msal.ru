<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");


$APPLICATION->SetTitle("Портфолио");
use Bitrix\Main\Page\Asset;

Asset::getInstance()->addString("<script src='https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js'></script>");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/bootstrap.min.css");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . "/js/bootstrap.min.js");

require_once($_SERVER["DOCUMENT_ROOT"] . '/personal/Lk.php');
require_once($_SERVER["DOCUMENT_ROOT"] . '/personal/Student.php');
$Student = new Student();
$arPortfolio = Student::getStudentPortfolio();
?>
    <table class="table table-bordered">
        <tr>
            <td>Тип</td>
            <td>Вид</td>
            <td>Значение</td>
        </tr>
        <? foreach ($arPortfolio as $title => $arItem): ?>
            <tr>
                <td rowspan="<?= count($arItem) + 1 ?>"><?= $title ?></td>
            </tr>
            <? foreach ($arItem as $type => $value): ?>
                <tr>
                    <td><?= $type ?></td>
                    <td><?= $value ?></td>
                </tr>
            <? endforeach; ?>
        <? endforeach; ?>
    </table>


    <ul class="nav nav-tabs" role="tablist">
        <li><a href="/personal/?t=home" class="frmldr">Расписание</a></li>
        <li><a href="/personal/?t=profile" class="frmldr">Успеваемость</a></li>
        <li class="active"><a href="#">Портфолио</a></li>
        <li>
            <a href="<?= 'http://' . $_SERVER['HTTP_HOST'] . substr($_SERVER['REQUEST_URI'], 0, strpos($_SERVER['REQUEST_URI'], '?')) . '?logout=yes' ?>">Выход</a>
        </li>
    </ul>


    <div class="inner_page_content">
        <? $APPLICATION->IncludeComponent(
            "bitrix:iblock.element.add",
            "portfolio",
            array(
                "AJAX_MODE" => "N",
                "AJAX_OPTION_ADDITIONAL" => "",
                "AJAX_OPTION_HISTORY" => "N",
                "AJAX_OPTION_JUMP" => "Y",
                "AJAX_OPTION_STYLE" => "Y",
                "ALLOW_DELETE" => "N",
                "ALLOW_EDIT" => "Y",
                "CUSTOM_TITLE_DATE_ACTIVE_FROM" => "",
                "CUSTOM_TITLE_DATE_ACTIVE_TO" => "",
                "CUSTOM_TITLE_DETAIL_PICTURE" => "",
                "CUSTOM_TITLE_DETAIL_TEXT" => "",
                "CUSTOM_TITLE_IBLOCK_SECTION" => "",
                "CUSTOM_TITLE_NAME" => "Название работы",
                "CUSTOM_TITLE_PREVIEW_PICTURE" => "",
                "CUSTOM_TITLE_PREVIEW_TEXT" => "",
                "CUSTOM_TITLE_TAGS" => "",
                "DEFAULT_INPUT_SIZE" => "30",
                "DETAIL_TEXT_USE_HTML_EDITOR" => "N",
                "ELEMENT_ASSOC" => "CREATED_BY",
                "GROUPS" => array(
                    0 => "1",
                    1 => "3",
                    2 => "4",
                    3 => "5",
                    4 => "6",
                    5 => "7",
                    6 => "8",
                    7 => "9",
                    8 => "10",
                    9 => "11",
                    10 => "12",
                    11 => "13",
                    12 => "14",
                    13 => "15",
                    14 => "16",
                    15 => "17",
                    16 => "18",
                    17 => "19",
                    18 => "20",
                    19 => "21",
                    20 => "22",
                ),
                "IBLOCK_ID" => "19",
                "IBLOCK_TYPE" => "personaloffice",
                "LEVEL_LAST" => "N",
                "MAX_FILE_SIZE" => "0",
                "MAX_LEVELS" => "100000",
                "MAX_USER_ENTRIES" => "100000",
                "NAV_ON_PAGE" => "10",
                "PREVIEW_TEXT_USE_HTML_EDITOR" => "N",
                "PROPERTY_CODES" => array(
                    0 => "83",
                    1 => "84",
                    2 => "85",
                    3 => "86",
                    4 => "87",
                    5 => "NAME",
                ),
                "PROPERTY_CODES_REQUIRED" => array(
                    0 => "83",
                    1 => "84",
                    2 => "NAME",
                ),
                "RESIZE_IMAGES" => "N",
                "SEF_MODE" => "N",
                "STATUS" => "ANY",
                "STATUS_NEW" => "N",
                "USER_MESSAGE_ADD" => "",
                "USER_MESSAGE_EDIT" => "",
                "USE_CAPTCHA" => "N",
                "COMPONENT_TEMPLATE" => "portfolio"
            ),
            false
        ); ?>
    </div>


<? /*$APPLICATION->IncludeComponent(
	"bitrix:news.list",
	"portfolio",
	Array(
		"ACTIVE_DATE_FORMAT" => "d.m.Y",
		"ADD_SECTIONS_CHAIN" => "Y",
		"AJAX_MODE" => "N",
		"AJAX_OPTION_ADDITIONAL" => "",
		"AJAX_OPTION_HISTORY" => "N",
		"AJAX_OPTION_JUMP" => "N",
		"AJAX_OPTION_STYLE" => "Y",
		"CACHE_FILTER" => "N",
		"CACHE_GROUPS" => "Y",
		"CACHE_TIME" => "36000000",
		"CACHE_TYPE" => "A",
		"CHECK_DATES" => "Y",
		"DETAIL_URL" => "",
		"DISPLAY_BOTTOM_PAGER" => "Y",
		"DISPLAY_DATE" => "Y",
		"DISPLAY_NAME" => "Y",
		"DISPLAY_PICTURE" => "Y",
		"DISPLAY_PREVIEW_TEXT" => "Y",
		"DISPLAY_TOP_PAGER" => "N",
		"FIELD_CODE" => array("",""),
		"FILTER_NAME" => "arPortfolio",
		"HIDE_LINK_WHEN_NO_DETAIL" => "N",
		"IBLOCK_ID" => "19",
		"IBLOCK_TYPE" => "user_data",
		"INCLUDE_IBLOCK_INTO_CHAIN" => "Y",
		"INCLUDE_SUBSECTIONS" => "Y",
		"MESSAGE_404" => "",
		"NEWS_COUNT" => "20",
		"PAGER_BASE_LINK_ENABLE" => "N",
		"PAGER_DESC_NUMBERING" => "N",
		"PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
		"PAGER_SHOW_ALL" => "N",
		"PAGER_SHOW_ALWAYS" => "N",
		"PAGER_TEMPLATE" => "modern",
		"PAGER_TITLE" => "Новости",
		"PARENT_SECTION" => "",
		"PARENT_SECTION_CODE" => "",
		"PREVIEW_TRUNCATE_LEN" => "",
		"PROPERTY_CODE" => array("DATE","TYPE","LINKS","FILES"),
		"SET_BROWSER_TITLE" => "Y",
		"SET_LAST_MODIFIED" => "N",
		"SET_META_DESCRIPTION" => "Y",
		"SET_META_KEYWORDS" => "Y",
		"SET_STATUS_404" => "N",
		"SET_TITLE" => "Y",
		"SHOW_404" => "N",
		"SORT_BY1" => "ACTIVE_FROM",
		"SORT_BY2" => "SORT",
		"SORT_ORDER1" => "DESC",
		"SORT_ORDER2" => "ASC",
		"STRICT_SECTION_CHECK" => "N"
	)
);//*/ ?>

    <script>
        $(function () {
            $(".frmldr").click(function () {
                $('.form-loader').fadeIn(300);
            });
        });
    </script>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>