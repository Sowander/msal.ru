<?php

/**
 * Created by PhpStorm.
 * User: Sowander
 * Date: 20.02.2018
 * Time: 18:30
 */
require_once('Lk.php');

class Student extends Lk
{

    static function getStudentGuid($login)
    {
        $function = 'ПолучитьСписокСтудентовГрупп';

        $wsdl = 'http://' . self::$host . '/UV/ws/wsspravinfo.1cws?wsdl';

        $res = false;
        $re = self::getSoapClient($wsdl, $function, array("НаДату" => date('c', time()), 'НомерСтуденческого' => str_replace('s', '', $login)));
        if (count($re) > 1) {
            foreach ($re as $arStudent) {
                if ($arStudent->ГруппаСтудентаДляСвязи->Основная) {
                    $res["UF_STUDENT_GUID"] = $arStudent->СтудентДляСвязи->GUIDСтудентаДляСвязи;
                    $res["UF_GROUP_GUID"] = $arStudent->ГруппаСтудентаДляСвязи->GUIDГруппыДляСвязи;
                    $res["UF_STUDENT_GROUP"] = $arStudent->ГруппаСтудентаДляСвязи->Наименование;
                    $res["UF_STUDENT_STAR"] = intval($arStudent->СтудентДляСвязи->СтаростаГруппы);
                    $res["UF_ACTIVE"] = $arStudent->СтудентДляСвязи->Состояние;
                }
            }
        } else {
            $res["UF_STUDENT_GUID"] = $re->СтудентДляСвязи->GUIDСтудентаДляСвязи;
            $res["UF_GROUP_GUID"] = $re->ГруппаСтудентаДляСвязи->GUIDГруппыДляСвязи;
            $res["UF_STUDENT_GROUP"] = $re->ГруппаСтудентаДляСвязи->Наименование;
            $res["UF_STUDENT_STAR"] = intval($re->СтудентДляСвязи->СтаростаГруппы);
            $res["UF_ACTIVE"] = $re->СтудентДляСвязи->Состояние;

        }
        return $res;

    }

    static function getStudentPortfolio()
    {
        global $USER;
        $result = false;
        $id = $USER->GetID();
        $arInfo = self::getStudentInfo($id);
        $login = $arInfo["LOGIN"];

        $function = 'ПолучитьСписокСтудентов';
        $wsdl = 'http://' . self::$host . '/UV/ws/wsportfoliostud.1cws?wsdl';

        $temp = self::getSoapClient($wsdl, $function, array('НомерСтуденческого' => str_replace('s', '', $login)), "Студент");

        $result["COMMON"]["FOTO"] = '<img src="" width="200" height="100" alt="">';
        $result["COMMON"]["FIO"] = $temp->Фамилия . ' ' . $temp->Имя . ' ' . $temp->Отчество;
        $result["COMMON"]["DATE_BIRTH"] = $temp->ДатаРождения;
        $result["COMMON"]["EMAIL"] = $temp->EMail;
        $result["COMMON"]["PHONE"] = $temp->Телефон;
        $result["COMMON"]["KURS"] = $temp->Курс;
        $result["Сведения о местах обучения"]["Институт"] = $temp->Институт;
        $result["Сведения о местах обучения"]["Специальность"] = $temp->Специальность;
        $result["Сведения о местах обучения"]["Профиль"] = $temp->Профиль;
        $result["Сведения о местах обучения"]["Форма Обучения"] = $temp->ФормаОбучения;
        $result["Сведения о местах обучения"]["Уровень Образования"] = $temp->УровеньОбразования;
        $result["Сведения о местах обучения"]["Год Поступления"] = $temp->ГодПоступления;
        $result["Сведения о местах обучения"]["Курс"] = $temp->Курс;
        $result["Сведения о местах обучения"]["Срок Обучения"] = $temp->СрокОбучения;
        $result["Сведения о местах обучения"]["Дата Выдачи Диплома"] = $temp->ДатаВыдачиДиплома;
        $result["Подтвержденный документ об образовании"]["Наименование документа"] = $temp->ДокументОбОбразовании->ВидДокумента;
        $result["Подтвержденный документ об образовании"]["Серия"] = $temp->ДокументОбОбразовании->Серия;
        $result["Подтвержденный документ об образовании"]["Номер"] = $temp->ДокументОбОбразовании->Номер;
        $result["Подтвержденный документ об образовании"]["Дата Выдачи"] = $temp->ДокументОбОбразовании->ДатаВыдачи;
        $result["Подтвержденный документ об образовании"]["Уровень Образования"] = $temp->ДокументОбОбразовании->УровеньОбразования;
        $result["Подтвержденный документ об образовании"]["Базовое Образование"] = $temp->ДокументОбОбразовании->БазовоеОбразование;
        $result["Подтвержденный документ об образовании"]["Средний Балл"] = $temp->ДокументОбОбразовании->СреднийБалл;
        $result["Подтвержденный документ об образовании"]["Место Выдачи"] = $temp->ДокументОбОбразовании->МестоВыдачи;
        $result["Подтвержденный документ об образовании"]["Специальность"] = $temp->ДокументОбОбразовании->Специальность;
        $result["Подтвержденный документ об образовании"]["Где Выдан"] = $temp->ДокументОбОбразовании->ГдеВыдан;
//        $result["Документ Об Образовании"]  = $temp->ДокументОбОбразовании;

        return $result;
    }
    static function getStudentFull($login = false)
    {
        if($login === false){
            global $USER;
            $id = $USER->GetID();
            $arInfo = self::getStudentInfo($id);
            $login = $arInfo["LOGIN"];
        }

        $function = 'ПолучитьСписокСтудентов';
        $wsdl = 'http://' . self::$host . '/UV/ws/wsportfoliostud.1cws?wsdl';

        $temp = self::getSoapClient($wsdl, $function, array('НомерСтуденческого' => str_replace('s', '', $login)), "Студент");
        $result = self::stdClassInArray($temp);
        return $result;
    }

    static function getStudentFullGroup($login = false)
    {
        if($login === false){
            global $USER;
            $id = $USER->GetID();
            $arInfo = self::getStudentInfo($id);
            $login = $arInfo["LOGIN"];
        }
        $function = 'ПолучитьСписокСтудентовГрупп';
        $wsdl = 'http://' . self::$host . '/UV/ws/wsspravinfo.1cws?wsdl';
        $temp = self::getSoapClient($wsdl, $function, array("НаДату" => date('c', time()),'НомерСтуденческого' => str_replace('s', '', $login)));
        $result = self::stdClassInArray($temp);
        return $result;

    }


    function getStudentInfo($id = false)
    {
        self::setBitrixSystems();

        $groupGuid = '';

        global $USER;
        if ($id == false) {
            $id = $USER->GetID();
        }
        $filter = Array("ID" => $id);
        $by = "id";
        $order = 'asc';
        $select = array('SELECT' => array('LOGIN', 'UF_GROUP_GUID', 'UF_STUDENT_GUID', 'UF_STUDENT_GROUP',"UF_STUDENT_STAR","UF_STUDENT_KURS", 'UF_STUDENT_FAK', 'NAME', 'LAST_NAME', 'SECOND_NAME', 'PERSONAL_PHOTO'));
        $rsUsers = \CUser::GetList($by, $order, $filter, $select);
        while ($arUser = $rsUsers->Fetch()) {
            $groupGuid['UF_GROUP_GUID'] = trim($arUser['UF_GROUP_GUID']);
            $groupGuid['UF_STUDENT_GUID'] = trim($arUser['UF_STUDENT_GUID']);
            $groupGuid['UF_STUDENT_GROUP'] = trim($arUser['UF_STUDENT_GROUP']);
            $groupGuid['UF_STUDENT_FAK'] = trim($arUser['UF_STUDENT_FAK']);
            $groupGuid['UF_STUDENT_STAR'] = trim($arUser['UF_STUDENT_STAR']);
            $groupGuid['LOGIN'] = trim($arUser['LOGIN']);
            $groupGuid['FIO'] = trim($arUser['LAST_NAME']) . ' ' . trim($arUser['NAME']) . ' ' . trim($arUser['SECOND_NAME']);
            $groupGuid['PERSONAL_PHOTO'] = CFile::GetPath(intval($arUser['PERSONAL_PHOTO']));
        }

        return $groupGuid;
    }

    static function getCurProgress($studentGuid)
    {
        $function = 'ПолучитьУспеваемость';
        $wsdl = 'http://' . self::$host . '/UV/ws/wsreportkartochka.1cws?wsdl';
        $obRes = self::getSoapClient($wsdl, $function, array("НаДату" => date('c', time()), 'Студент' => $studentGuid));
        $res = self::stdClassInArray($obRes);
        return $res;
    }

    static function getCurProgressOnline($studentGuid,$nk)
    {
        $function = 'РасширеннаяИнфоромацияОВедомостяхСтудента';
        $wsdl = 'http://' . self::$host . '/UV/ws/vedomostirpd.1cws?wsdl';
        $obRes = self::getSoapClient($wsdl, $function, array('УИДСтудента' => $studentGuid,'НомерКурса'=>$nk), null);
        $res = self::stdClassInArray($obRes);
        return $res;
    }

}