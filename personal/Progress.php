<?php

/**
 * Created by PhpStorm.
 * User: Sowander
 * Date: 27.02.2018
 * Time: 12:20
 */

require_once('Lk.php');

class Progress extends Lk
{
    static function getOneProgress()
    {

        $arResult = false;
        $function = 'ПолучитьУспеваемость';
        $wsdl = 'http://' . self::$host . '/UV/ws/wsreportkartochka.1cws?wsdl';
        $obStudent = new Student();
        $arStudentInfo = $obStudent->getStudentInfo();
        $strStudentGuid = $arStudentInfo['UF_STUDENT_GUID'];
        $params = array('НаДату' => date('c'), 'Студент' => $strStudentGuid);
        $arAllRPDModulesGuid = array();


        $obProgress = self::getSoapClient($wsdl, $function, $params);
        foreach ($obProgress as $val) {
            $arResult['ITEMS'][] = array(
                'SEMESTR' => $val->Семестр->Наименование,
                'SUBJECT' => $val->Дисциплина->Наименование,
                'GUID_SUBJECT' => $val->Дисциплина->GUIDДисциплины,
                'VIEW' => $val->ВидНагрузки->Наименование,
                'COURSE' => $val->Курс->Наименование,
                'VALUE' => $val->Оценка->Наименование,
                'NLIST' => $val->НомерВедомости->Наименование,
                'FIO' => $val->Студент->Наименование,
                'BALS' => '',
                'PREPOD' => ''
            );
        }
        $arAllPoints = self::getPoints();

        if ($strStudentGuid)
            $arAllRPDModulesGuid = self::getRPDModulesGuid($strStudentGuid);
        $arPoints = array();


        foreach ($arAllPoints as &$value) {
            if (strlen($value['MODULE']) > 4)
                $arPoints[$value['GUID_SUBJECT'] . $value['SEMESTR']] += $value['BALS'];
            $arResult["MODULES"][$value['GUID_SUBJECT']][] = array(
                'MODULE' => $value['MODULE'],
                'POINTS' => $value['BALS'],
                'GUID_SUBJECT' => $value['GUID_SUBJECT'],
                'GUID_MODULE' => $value['GUID_MODULE'],
                'SUBJECT' => $value['SUBJECT'],
                'SEMESTR' => $value['SEMESTR'],
                'RPD' => in_array($value['MODULE'], $arAllRPDModulesGuid) && !empty($value['MODULE']) ? self::getPointsRpd(array_search($value['MODULE'], $arAllRPDModulesGuid), $strStudentGuid, $value['GUID_SUBJECT']) : '',
            );
        }
        foreach ($arResult['ITEMS'] as &$value) {
            $value['PREPOD'] = self::getParamVedomosti($value['NLIST'])->Преподаватель->Наименование;
            foreach ($arPoints as $key => $val) {
                if ($key == $value['GUID_SUBJECT'] . $value['SEMESTR']) {
                    $value['BALS'] = $val;
                }
            }
        }
        return $arResult;
    }


    static function getRPDModulesGuid($strStudentGuid)
    {
        $arResult = false;
        $function = 'ПолучитьМодулиРПДСтудента';
        $wsdl = 'http://' . self::$host . '/UV/ws/wsreportreiting.1cws?wsdl';
        $params = array('GUIDСтудента' => $strStudentGuid);

        $obProgress = self::getSoapClient($wsdl, $function, $params);
        foreach ($obProgress as $val) {
            $arResult[trim($val->МодульРПДСтудента->GUIDМодуляРПД)] = trim($val->МодульРПДСтудента->Наименование);

        }
        return $arResult;
    }


    static function getPoints()
    {
        $arResult = false;
        $function = 'ПолучитьБаллы';
        $wsdl = 'http://' . self::$host . '/UV/ws/wsreportreiting.1cws?wsdl';
        $obStudent = new Student();
        $arStudentInfo = $obStudent->getStudentInfo();
        $strStudentGuid = $arStudentInfo['UF_STUDENT_GUID'];

        $params = array('Студент' => $strStudentGuid);

        $obProgress = self::getSoapClient($wsdl, $function, $params);
        foreach ($obProgress as $val) {


            $arResult[] = array(
                'SEMESTR' => $val->Семестр->Наименование,
                'SUBJECT' => $val->Дисциплина->Наименование,
                'GUID_SUBJECT' => $val->Дисциплина->GUIDДисциплины,
                'GUID_MODULE' => $val->Модуль->GUIDМодуля,
                'NLIST' => $val->НомерВедомости->Наименование,
                'FIO' => $val->Студент->Наименование,
                'MODULE' => $val->Модуль->Наименование,
                'BALS' => $val->СуммаБаллов->Значение,
            );

        }

        return $arResult;


    }

    static function getPointsRpd($modGuid, $strStudentGuid, $subGuid)
    {
        $arResult = false;
        $function = 'ПолучитьМодулиРПД';
        $wsdl = 'http://' . self::$host . '/UV/ws/wsreportreiting.1cws?wsdl';


        $params = array('GUIDМодуля' => trim($modGuid), 'GUIDСтудента' => trim($strStudentGuid), 'GUIDДисциплины' => trim($subGuid));

        $obProgress = self::getSoapClient($wsdl, $function, $params);

        foreach ($obProgress as $val) {
            if (strlen($val->Студент->Наименование)>0)
                $arResult[] = array(
                    'GUID_SUBJECT' => $val->Дисциплина->GUIDДисциплины,
                    'MODULE' => $val->РабочаяПрограммаДисциплины->СписокМодулей->Наименование,
                    'SEMESTR' => $val->Семестр->Наименование,
                    "MAX_POINTS" => $val->РабочаяПрограммаДисциплины->СписокМодулей->МаксимальныйБалл,
                    "TEMA" => $val->РабочаяПрограммаДисциплины->СписокМодулей->СписокТем->Наименование,
                    "TEMA_POINTS" => $val->РабочаяПрограммаДисциплины->СписокМодулей->СписокТем->БаллПоТеме,
                    "UID_MODULE" => $val->РабочаяПрограммаДисциплины->СписокМодулей->УИД,
                );
        }
        return $arResult;

    }
}