<?php

/**
 * Created by PhpStorm.
 * User: Sowander
 * Date: 20.02.2018
 * Time: 15:14
 */
use Bitrix\Main\Diag\Debug;
use Bitrix\Main\Loader;

require_once('Lk.php');

class Schedule extends Lk
{
	static function getListSchedule()
	{
		$function = 'ПолучитьСписокРасписаний';
		$wsdl = 'http://' . self::$host . '/UV/ws/wsreportschedulegroup.1cws?wsdl';
		return self::getSoapClient($wsdl, $function);
	}

	static function getkListScheduleBitrix($active = false)
	{
		self::setBitrixSystems();
		Loader::includeModule('iblock');
		$arResult = NULL;
		$arSelect = Array("ID", "NAME", "ACTIVE", "PROPERTY_SCH_GUID");
		$arFilter = Array("IBLOCK_ID" => IntVal(self::$ListScheduleIblockID), 'ACTIVE' => $active);
		$res = \CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		while ($ob = $res->GetNext()) {
			$arResult[] = $ob;
		}
		return $arResult;
	}

	static function setListSchedule()
	{

		self::setBitrixSystems();
		$ob1C = self::getListSchedule();
		$arBitrix = self::getkListScheduleBitrix();

		global $USER;
		$el = new \CIBlockElement;
		foreach ($ob1C as $ob) {

			unset($arLoadProductArray);
			unset($PROP);
			unset($PRODUCT_ID);
			unset($ACTIVE);

			$PROP['SCH_GUID'] = trim($ob->Расписание->GUIDРасписания);
			$PROP['SCH_YEAR_NAME'] = trim($ob->УчебныйГодРасписания->Наименование);
			$PROP['SCH_YEAR_GUID'] = trim($ob->УчебныйГодРасписания->GUIDУчебногоГодаРасписания);
			$PROP['SCH_RING_NAME'] = trim($ob->ПланЗвонковПоУмолчаниюРасписания->Наименование);
			$PROP['SCH_RING_GUID'] = trim($ob->ПланЗвонковПоУмолчаниюРасписания->GUIDПланаЗвонковПоУмолчаниюРасписания);

			foreach ($arBitrix as $arItem) {
				if (trim($arItem['PROPERTY_SCH_GUID_VALUE']) == trim($ob->Расписание->GUIDРасписания)) {
					$PRODUCT_ID = $arItem['ID'];
					$ACTIVE = $arItem['ACTIVE'];
				}
			}
			$arLoadProductArray = Array(
				"MODIFIED_BY" => $USER->GetID(), // элемент изменен текущим пользователем
				"IBLOCK_SECTION_ID" => false,
				"IBLOCK_ID" => self::$ListScheduleIblockID,
				"PROPERTY_VALUES" => $PROP,
				"NAME" => trim($ob->Расписание->Наименование),
				"ACTIVE" => $ACTIVE ? $ACTIVE : 'N',
			);
			if ($PRODUCT_ID) { //Обновим элемент
				if (!$res = $el->Update($PRODUCT_ID, $arLoadProductArray)) {
					Debug::writeToFile($el->LAST_ERROR . '/n', '', self::$logpath . 'setListScheduleErrorUpdate.log');
				}
			} else { //Добавим элемент
				if (!$PRODUCT_ID = $el->Add($arLoadProductArray)) {
					Debug::writeToFile($el->LAST_ERROR . '/n', '', self::$logpath . 'setListScheduleErrorAdd.log');
				}
			}
		}
	}

	static function getOneSchedule($dateStart, $dateEnd, $params = false)
	{
		$function = 'ПолучитьРасписание';
		$wsdl = 'http://' . self::$host . '/UV/ws/wsreportschedulegroup.1cws?wsdl';


		$arRasp = self::getkListScheduleBitrix('Y');
		$obRing = new Ring();


		$arRing = $obRing::getkListRingBitrix('Y');
		$obStudent = new Student();
		$arGroup = $obStudent->getStudentInfo();

		#exit("99");


		$group = $arGroup['UF_GROUP_GUID'];


		$arResult = false;
		if ($params === false) {
			if (count($arRing) > 1) {
				foreach ($arRing as $ring) {
					$params = array(
						'Расписание' => $arRasp[0]['PROPERTY_SCH_GUID_VALUE'],
						'ПланЗвонков' => $ring['PROPERTY_PLAN_GUID_VALUE'],
						'Группа' => $group,
						'ДатаС' => $dateStart, 'ДатаПо' => $dateEnd
					);
					$temp[] = self::getSoapClient($wsdl, $function, $params);
				}
			} else {
				$params = array(
					'Расписание' => $arRasp[0]['PROPERTY_SCH_GUID_VALUE'],
					'ПланЗвонков' => $arRing[0]['PROPERTY_PLAN_GUID_VALUE'],
					'Группа' => $group,
					'ДатаС' => $dateStart, 'ДатаПо' => $dateEnd
				);

				$temp[] = self::getSoapClient($wsdl, $function, $params);
			}
		} else {
			$temp[] = self::getSoapClient($wsdl, $function, $params);
		}

		foreach ($temp as $ob) {
			foreach ($ob as $key => $item) {
				if ($item->День->Значение) {
					$arResult['ITEMS'][self::getNumberLection($item->ВремяНачалаПары->Значение)][self::dateToDay($item->День->Значение, '%u')][] = array(
						'date' => $item->День->Значение,
						'ndate' => self::dateToDay($item->День->Значение, '%u'),
						'timeStart' => $item->ВремяНачалаПары->Значение,
						'timeEnd' => $item->ВремяОкончанияПары->Значение,
						'subject' => $item->Дисциплина->Наименование,
						'place' => $item->Аудитория->Наименование,
						'view' => $item->ВидНагрузки->Наименование,
						'korpus' => $item->Корпус->Наименование,
						'teacher' => $item->Преподаватель->Наименование,
					);
					$arResult['RINGS'][self::getNumberLection($item->ВремяНачалаПары->Значение)] = $item->ВремяНачалаПары->Значение . '-' . $item->ВремяОкончанияПары->Значение;
				}

			}
		}
		foreach ($arResult['ITEMS'] as $nlect => $arDay) {
			foreach ($arDay as $nday => $arTempLect) {
				if (count($arTempLect) === 1) {
					$arResult['ITEMS'][$nlect][$nday] = $arTempLect[0];
				}
			}
		}
		ksort($arResult['ITEMS']);
		ksort($arResult['RINGS']);
		return $arResult;
	}

}