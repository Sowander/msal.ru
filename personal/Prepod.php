<?php

/**
 * Created by PhpStorm.
 * User: Sowander
 * Date: 20.02.2018
 * Time: 18:30
 */
require_once('Lk.php');

class Prepod extends Lk
{

    static function getPrepodGuid($login = false)
    {
        $function = 'ПолучитьГУИДПоЛогинуАД';
        $wsdl = 'http://' . self::$host . '/UV/ws/wsspravinfo.1cws?wsdl';
        $res = self::getSoapClient($wsdl, $function, array("ЛогинАД" => $login), null);
        return $res;

    }

    static function getVedomostiForPrepod($userID)
    {
        $result = false;
        $adLogin = self::getPrepodInfo($userID)['LOGIN'];

        if (strlen($adLogin) > 1) {
            $function = 'ПолучитьСписокВедомостейРПД';
            $wsdl = 'http://' . self::$host . '/UV/ws/vedomostirpd.1cws?wsdl';
            $obRes = self::getSoapClient($wsdl, $function, array("ЛогинПреподавателяВАД" => $adLogin), "Ведомости");
            $result = self::stdClassInArray($obRes);
        }
        return $result;
    }

    static function getOneVedomost($nVedomost)
    {

        $result = false;

        if (strlen($nVedomost) > 1) {
            $function = 'РасширеннаяИнфоромацияОВедомости';
            $wsdl = 'http://' . self::$host . '/UV/ws/vedomostirpd.1cws?wsdl';
            $obRes = self::getSoapClient($wsdl, $function, array("НомерВедомости" => $nVedomost), null);
            $result = self::stdClassInArray($obRes);
        }
        return $result;
    }

    public function getVedomostRpdPagin($page, $maxitem, $arResult)
    {

        if (is_array($arResult) && !empty($arResult)) {
            if ($arResult['НомерВедомости']) {
                $arResult = [$arResult];
            }
            foreach ($arResult as $k => $arValue) {
                if ($k < ($maxitem * $page) && $k >= ($maxitem * $page - $maxitem)) {
                    if ($arValue['НомерВедомости']) {
                        $arValue2 = self::getOneVedomost($arValue['НомерВедомости']);
                        $arResult[$k] = $arValue2;
                    }
                } else {
                    unset($arResult[$k]);
                }
            }
        }
        sort($arResult);
        return $arResult;
    }



    /**
     * @deprecated
     * @param bool $arGroupsGuid
     * @param bool $arParams
     * @return array
     * $arGroupsGuid = ['Состав' => [
     * ['GUIDГруппы' => '81e714f2-5a6c-11e8-812f-005056b32020'],
     * ['GUIDГруппы' => '0d657e8c-5a6d-11e8-812f-005056b32020'],
     * ]
     * ];
     * $arParams = [
     * 'ДатаЗанятия' => date('c'),
     * 'Дисциплина' => [
     * 'Наименование' => '',
     * 'GUIDДисциплины' => self::$null,
     * ],
     * 'ВидНагрузки' => [
     * 'Наименование' => ''
     * ],
     * 'ВремяНачалаПары' => [
     * 'Значение' => ''
     * ],
     * 'ВремяОкончанияПары' => [
     * 'Значение' => ''
     * ],
     * 'Преподаватель' => [
     * 'Наименование' => '',
     * 'GUIDПреподавателя' => self::$null,
     * ],
     * ];
     */
    public static function updateBall($nVedomost, $uidStuden, $uidLesson, $ball, $missed = 0)
    {
        $dateTime = date('c', time());
        if (strtolower($ball) === 'н') {
            $missed = 1;
            $ball = 0;
        } else {
            $ball = strval(intval($ball));
        }
        $function = 'ОбновитьОценкуВВедомости';
        $wsdl = 'http://' . self::$host . '/UV/ws/vedomostirpd.1cws?wsdl';
        $obRes = self::getSoapClient($wsdl, $function,
            [
                "НомерВедомости" => $nVedomost,
                "УИДСтудента" => $uidStuden,
                "УИДЗанятия" => $uidLesson,
                "Оценка" => $ball,
                "ДатаОценки" => $dateTime,
                "ПропустилЗанятие" => $missed
            ], null);
        $result = self::stdClassInArray($obRes);

        return $result;

    }

    /**
     * @deprecated
     * @param bool $arGroupsGuid
     * @param bool $arParams
     * @return array
     * $arGroupsGuid = ['Состав' => [
     * ['GUIDГруппы' => '81e714f2-5a6c-11e8-812f-005056b32020'],
     * ['GUIDГруппы' => '0d657e8c-5a6d-11e8-812f-005056b32020'],
     * ]
     * ];
     * $arParams = [
     * 'ДатаЗанятия' => date('c'),
     * 'Дисциплина' => [
     * 'Наименование' => '',
     * 'GUIDДисциплины' => self::$null,
     * ],
     * 'ВидНагрузки' => [
     * 'Наименование' => ''
     * ],
     * 'ВремяНачалаПары' => [
     * 'Значение' => ''
     * ],
     * 'ВремяОкончанияПары' => [
     * 'Значение' => ''
     * ],
     * 'Преподаватель' => [
     * 'Наименование' => '',
     * 'GUIDПреподавателя' => self::$null,
     * ],
     * ];
     */
    public static function updateBallDop($nVedomost, $uidStuden, $uidModule, $ball)
    {
        $dateTime = date('c', time());
        $function = 'ОбновитьОценкуДоработкиВВедомости';
        $wsdl = 'http://' . self::$host . '/UV/ws/vedomostirpd.1cws?wsdl';
        $obRes = self::getSoapClient($wsdl, $function,
            [
                "НомерВедомости" => $nVedomost,
                "УИДСтудента" => $uidStuden,
                "УИДМодуля" => $uidModule,
                "Оценка" => $ball,
                "ДатаОценки" => $dateTime,
            ], null);
        $result = self::stdClassInArray($obRes);
        return $result;

    }

    /**
     * @deprecated
     * @param bool $arGroupsGuid
     * @param bool $arParams
     * @return array
     * $arGroupsGuid = ['Состав' => [
     * ['GUIDГруппы' => '81e714f2-5a6c-11e8-812f-005056b32020'],
     * ['GUIDГруппы' => '0d657e8c-5a6d-11e8-812f-005056b32020'],
     * ]
     * ];
     * $arParams = [
     * 'ДатаЗанятия' => date('c'),
     * 'Дисциплина' => [
     * 'Наименование' => '',
     * 'GUIDДисциплины' => self::$null,
     * ],
     * 'ВидНагрузки' => [
     * 'Наименование' => ''
     * ],
     * 'ВремяНачалаПары' => [
     * 'Значение' => ''
     * ],
     * 'ВремяОкончанияПары' => [
     * 'Значение' => ''
     * ],
     * 'Преподаватель' => [
     * 'Наименование' => '',
     * 'GUIDПреподавателя' => self::$null,
     * ],
     * ];
     */
    public static function updateAllBall($nVedomost, $arBalls)
    {
        $function = 'ОбновитьВсеОценкиВВедомости';
        $wsdl = 'http://' . self::$host . '/UV/ws/vedomostirpd.1cws?wsdl';
        $obRes = self::getSoapClient($wsdl, $function,
            [
                "НомерВедомости" => $nVedomost,
                "ОценкиДляОбновления" => $arBalls,
            ],
            null
        );
        $result = self::stdClassInArray($obRes);
        return $result;

    }
    /**
     * @deprecated
     * @param bool $arGroupsGuid
     * @param bool $arParams
     * @return array
     * $arGroupsGuid = ['Состав' => [
     * ['GUIDГруппы' => '81e714f2-5a6c-11e8-812f-005056b32020'],
     * ['GUIDГруппы' => '0d657e8c-5a6d-11e8-812f-005056b32020'],
     * ]
     * ];
     * $arParams = [
     * 'ДатаЗанятия' => date('c'),
     * 'Дисциплина' => [
     * 'Наименование' => '',
     * 'GUIDДисциплины' => self::$null,
     * ],
     * 'ВидНагрузки' => [
     * 'Наименование' => ''
     * ],
     * 'ВремяНачалаПары' => [
     * 'Значение' => ''
     * ],
     * 'ВремяОкончанияПары' => [
     * 'Значение' => ''
     * ],
     * 'Преподаватель' => [
     * 'Наименование' => '',
     * 'GUIDПреподавателя' => self::$null,
     * ],
     * ];
     */
    public static function getPrepodShedule($logon, $date)
    {
        $function = 'ПолучитьРасписаниеПреподавателя';
        $wsdl = 'http://' . self::$host . '/UV/ws/wsreportschedulegroup.1cws?wsdl';
        $obRes = self::getSoapClient($wsdl, $function,
            [
                "ЛогинАД" => $logon,
                "Дата" => $date,
            ]
        );
        $result = self::stdClassInArray($obRes);
        return $result;
    }

    /*
     * НОВЫЙ ЖУРНАЛ ПРЕПОДАВАТЕЛЯ
     * */

    /**
     * @param bool $userID
     * @return bool
     */
    public function getPrepodInfo($userID = false)
    {
        self::setBitrixSystems();

        $arResult = false;

        global $USER;
        if ($userID == false) {
            $userID = $USER->GetID();
        }
        $isPrepod = in_array(ID_PREPOD_GROUP, CUser::GetUserGroup($userID));
        if ($isPrepod === true) {
            $filter = Array("ID" => $userID);
            $order = array('sort' => 'asc');
            $tmp = 'sort';
            $select = array('SELECT' => array('*'));
            $rsUsers = \CUser::GetList($order, $tmp, $filter, $select);
            while ($arUser = $rsUsers->Fetch()) {
                $arResult['LOGIN'] = trim($arUser['LOGIN']);
                $arResult['FIO'] = trim($arUser['LAST_NAME']) . ' ' . trim($arUser['NAME']) . ' ' . trim($arUser['SECOND_NAME']);
                $arResult['PERSONAL_PHOTO'] = CFile::GetPath(intval($arUser['PERSONAL_PHOTO']));
            }
        }
        return $arResult;
    }

    /**
     * @deprecated
     * @param bool $arGroupsGuid
     * @param bool $arParams
     * @return array
     * $arGroupsGuid = ['Состав' => [
     * ['GUIDГруппы' => '81e714f2-5a6c-11e8-812f-005056b32020'],
     * ['GUIDГруппы' => '0d657e8c-5a6d-11e8-812f-005056b32020'],
     * ]
     * ];
     * $arParams = [
     * 'ДатаЗанятия' => date('c'),
     * 'Дисциплина' => [
     * 'Наименование' => '',
     * 'GUIDДисциплины' => self::$null,
     * ],
     * 'ВидНагрузки' => [
     * 'Наименование' => ''
     * ],
     * 'ВремяНачалаПары' => [
     * 'Значение' => ''
     * ],
     * 'ВремяОкончанияПары' => [
     * 'Значение' => ''
     * ],
     * 'Преподаватель' => [
     * 'Наименование' => '',
     * 'GUIDПреподавателя' => self::$null,
     * ],
     * ];
     */
    public static function getPrepodJournal($arGroupsGuid = false, $arParams = false)
    {

        $result = [];
        $function = 'ПолучитьЗанятие';
        $wsdl = 'http://' . self::$host . '/UV/ws/wsreportschedulegroup.1cws?wsdl';
        $obRes = self::getSoapClient($wsdl, $function,
            [
                "Группы" => $arGroupsGuid,
                "ВхПараметры" => $arParams,
            ]
        );
        $arTemp = self::stdClassInArray($obRes);
        foreach ($arTemp as $stud) {
            $result[trim($stud['Группа']['Значение'])][] = $stud;
        }
        return $result;
    }

    /**
     * новый wsdl бывший getPrepodJournal()
     * @param bool $arGroupsGuid
     * @param bool $arParams
     * @return array
     */
    public static function getLectionE($arGroupsGuid = false, $arParams = false)
    {
        $result = [];
        $function = 'ПолучитьЗанятие';
        $wsdl = 'http://' . self::$OldHost . '/UV/ws/wsreportsjournal.1cws?wsdl';
        $obRes = self::getSoapClient($wsdl, $function,
            [
                "Группы" => $arGroupsGuid,
                "ВхПараметры" => $arParams,
            ],
        null
        );
        $arTemp = self::stdClassInArray($obRes);
        foreach ($arTemp['List'] as $stud) {
            $stud['EN'] = intval($arTemp['EngLanguage']);
            $result[trim($stud['Group']['Name'])][] = $stud;
        }

        return $result;
    }

    /**
     * @deprecated
     * @param bool $arGroupsGuid
     * @param bool $arParams
     * @return array
     */
    public static function getLectionT($arGroupsGuid = false, $arParams = false)
    {
        printr('ПолучитьЗанятие - входные параметры',1);
        printr($arGroupsGuid,1);
        printr($arParams,1);
        $result = [];
        $function = 'ПолучитьЗанятие';
        $wsdl = 'http://' . self::$OldHost . '/UV/ws/wsreportsjournal.1cws?wsdl';
        $obRes = self::getSoapClient($wsdl, $function,
            [
                "Группы" => $arGroupsGuid,
                "ВхПараметры" => $arParams,
            ],
            null
        );
        printr('ПолучитьЗанятие - результат',1);
        printr($obRes,1);
        $arTemp = self::stdClassInArray($obRes);
        foreach ($arTemp['List'] as $stud) {
            $stud['EN'] = intval($arTemp['EngLanguage']);
            $result[trim($stud['Group']['Name'])][] = $stud;
        }

        return $result;
    }

    /**
     * @deprecated
     * @param $arParams
     * @return array|mixed
     *    $arParams = ['СписокПараметров' => ['Состав' => [
     * [
     * 'Дисциплина' => [
     * 'Наименование' => 'Научно-исследовательский семинар № 2',
     * 'GUID' => '2099a658-fdb3-11e6-80de-005056b32020',
     * ],
     * 'ВидНагрузки' => [
     * 'Наименование' => 'Семинар',
     * ],
     * 'Преподаватель' => [
     * 'Наименование' => 'Петрова Инга Вадимовна',
     * 'GUID' => '708308be-6bac-11e5-ba04-0050569666d8'
     * ],
     * 'РПД' => [
     * 'Наименование' => 'рпд',
     * 'GUID' => '13596f43-ad1e-11e4-90a1-005056af2bad'
     * ],
     * 'Модуль' => [
     * 'Наименование' => 'рпд',
     * 'GUID' => '13596f43-ad1e-11e4-90a1-005056af2bad',
     * ],
     * 'Студент' => [
     * 'Наименование' => '',
     * 'GUID' => 'da685a5c-f3bf-11e8-813e-005056b32020'
     * ],
     * 'ВремяНачалаПары' => [
     * 'Значение' => '12:11'
     * ],
     * 'ВремяОкончанияПары' => [
     * 'Значение' => '12:12'
     * ],
     * 'ТемаЗанятий' => [
     * 'Наименование' => 'Размножение ежиков',
     * 'МаксимальныйБалл' => 123
     * ],
     * 'УчебныйГод' => [
     * 'Наименование' => '2018/2019',
     * 'GUID' => 'd7127a40-02ad-11e5-b137-005056af2bad'
     * ],
     * 'Балл' => 1,
     * 'Семестр' => 1,
     * 'ЗанятиеПропущено' => 0,
     * 'БаллДоработки' => 0,
     * 'ДатаЗанятия' => date('c'),
     * ],
     * ]]];
     */
    public static function saveJournal($arParams)
    {
        $result = [];
        if (is_array($arParams) && !empty($arParams)) {
            $arParams = ['СписокПараметров' => ['Состав' =>
                $arParams
            ]];
            $function = 'СохранитьДанныеЗанятия';
            $wsdl = 'http://' . self::$host . '/UV/ws/wsreportschedulegroup.1cws?wsdl';
            $obRes = self::getSoapClient($wsdl, $function, $arParams, null);
            $result = self::stdClassInArray($obRes);
        }
        return $result;
    }

    /**
     * новая wsdl, бывший saveJournal()
     * @param $arParams
     * @return array|mixed
     */
    public static function saveDataLectionE($arParams)
    {
        $result = [];
        if (is_array($arParams) && !empty($arParams)) {
            $arParams = ['СписокПараметров' => ['List' =>
                $arParams
            ]];
            $function = 'СохранитьДанныеЗанятия';
            $wsdl = 'http://' . self::$OldHost . '/UV/ws/wsreportsjournal.1cws?wsdl';
            $obRes = self::getSoapClient($wsdl, $function, $arParams, null);
            $result = self::stdClassInArray($obRes);
        }
        return $result;
    }

    /**
     * @param $rpdGuid
     * @return mixed
     * получить структуру рпд
     * @deprecated
     */
    public static function getStructRPD($rpdGuid)
    {
        $arParams = ['GUID' => strval($rpdGuid)];
        $function = 'ПолучитьСтруктуруРПД';
        $wsdl = 'http://' . self::$host . '/UV/ws/wsreportschedulegroup.1cws?wsdl';
        $obRes = self::getSoapClient($wsdl, $function, $arParams, null);
        $result = self::stdClassInArray($obRes);
        if ($result['РПД']['МодулиРПД']['Наименование']) {
            $result['РПД']['МодулиРПД'] = [$result['РПД']['МодулиРПД']];
        }
        foreach ($result['РПД']['МодулиРПД'] as &$arModule) {
            if ($arModule['ТемыМодуля']['Наименование']) {
                $arModule['ТемыМодуля'] = [$arModule['ТемыМодуля']];
            }
        }
        return $result['РПД'];
    }

    /**
     * новая wsdl
     * @param $rpdGuid
     * @return mixed
     */
    public static function getStructRpdE($rpdGuid)
    {
        $arParams = ['GUID' => strval($rpdGuid)];
        $function = 'ПолучитьСтруктуруРПД';
        $wsdl = 'http://' . self::$OldHost . '/UV/ws/wsreportsjournal.1cws?wsdl';
        $obRes = self::getSoapClient($wsdl, $function, $arParams, null);

        $result = self::stdClassInArray($obRes);

        if ($result['RPD']['Modules']['Name']) {
            $result['RPD']['Modules'] = [$result['RPD']['Modules']];
        }
        foreach ($result['RPD']['Modules'] as &$arModule) {
            if ($arModule['ItemsTheme']['Name']) {
                $arModule['ItemsTheme'] = [$arModule['ItemsTheme']];
            }
        }
        return $result['RPD'];
    }

    /**
     * @param $str
     * @return string
     * удаляем дебильные символы Привет MSWord
     */
    public static function removeWordSimbol($str)
    {
        return strval(preg_replace('/¶/', ' ', $str));
    }

    /**
     * @param bool $arGroupsGuid
     * @param bool $arParams
     * @return mixed
     */
    public static function  getSvodTableE($arGroupsGuid = false, $arParams = false)
    {
        $function = 'ПолучитьСводнуюТаблицуРезультатов';
        $wsdl = 'http://' . self::$OldHost . '/UV/ws/wsreportsjournal.1cws?wsdl';
        $obRes = self::getSoapClient(
            $wsdl,
            $function,
            [
                "Группы" => $arGroupsGuid,
                "ВхПараметры" => $arParams,
            ],
            Null
        );
//        printr($obRes,1);
        $result = self::stdClassInArray($obRes);

        return $result;
    }


    /**
     * список дисциплин для доработки
     * @param bool $nStunent
     * @param bool $loginPrepod
     * @return mixed
     */
    public static function  getListDiscDorab($nStunent = false, $loginPrepod = false)
    {
        if(!$nStunent or !$loginPrepod){
//            return false;
        }
        $function = 'ПолучитьСписокДисциплинНаДоработку';
        $wsdl = 'http://' . self::$OldHost . '/UV/ws/wsreportsjournal.1cws?wsdl';
        $obRes = self::getSoapClient(
            $wsdl,
            $function,
            [
               "НомерСтуденческого" => $nStunent,
               "LoginAD" => $loginPrepod,
            ],
            Null
        );
        $result = self::stdClassInArray($obRes);
        return $result;
    }

    public static function saveDataDorab($arParams = false){
        $arParams = ['СписокПараметров'=>['List'=>$arParams]];
        $function = 'СохранитьДанныеДоработок';
        $wsdl = 'http://' . self::$OldHost . '/UV/ws/wsreportsjournal.1cws?wsdl';
        $obRes = self::getSoapClient(
            $wsdl,
            $function,
            $arParams,
            Null
        );
        $result = self::stdClassInArray($obRes);
        return $result;
    }

}